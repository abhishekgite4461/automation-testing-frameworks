const memoize = require('memoizee');

const AppInfoFactory = require('../apps/appInfo.factory');
const AppInstaller = require('./appInstaller');
const DeviceState = require('./deviceState');
const envEnum = require('../enums/server.enum');
const Server = require('../enums/server.enum');
const logger = require('./logger');
const envAPi = require('./environmentApiClient');
const hsUtils = require('./headspin-utils');
const appVersionHelper = require('./appVersion.helper');
const AppInfo = require('../apps/appInfo.factory');
const {PerfectoRestClient} = require('./perfectoRestClient');
const deviceList = require('../wdio_config/perfectoDeviceList');

const perfectoRestClient = new PerfectoRestClient();
const deviceListForReboot = deviceList.ALL.iOS.concat(deviceList.ALL.Android);

const getAppType = () => browser.capabilities.appType;
const isBNGA = () => browser.capabilities.appType === 'BNGA';
const isMBNA = () => browser.capabilities.brand === 'MBNA';
const isRNGA = () => !isBNGA();

const testEnvironment = () => browser.capabilities.server;
const isUAT = () => browser.capabilities.isUAT;
const isBR = () => Server.fromString(testEnvironment()).testPhase === 'BR';
const isMFIT = () => Server.fromString(testEnvironment()).testPhase === 'MFIT';
const isAndroid = () => driver.isAndroid;
const isIOS = () => driver.isIOS;
const isRetailIOS = () => driver.isIOS && browser.capabilities.appType === 'RNGA';

const isPerfecto = () => browser.capabilities.provider[0] === 'perfecto';
const isHeadspin = () => browser.capabilities.provider[0] === 'headspin';
const isStub = () => browser.capabilities.server === 'STUB';
const isEnv = () => !isStub();

const getBrand = () => browser.capabilities.brand;
const deEnrolUser = (user, environment) => {
    if (isPerfecto()) {
        envAPi.deregisterAllDevices(environment, getBrand(), user, getAppType());
    }
};

// We use memoize to only execute the request once and remember the response.
// This only works because we have one session per process (and one device per session).
// This is a side effect of using the webdriverio test runner.
// We should preferably have a device helper object and take control of its life cycle.
const osVersion = memoize(
    () => {
        let version = '';
        if (!isPerfecto() && isHeadspin()) {
            version = hsUtils.getDeviceIfo(browser.capabilities.udid).os_version;
        } else if (isPerfecto()) {
            version = browser.handsetInfo.osVersion;
        }
        return version;
    }
);
const getAppName = memoize(() => AppInfoFactory.getAppInfo(browser.capabilities).name);
const windowHandleSize = memoize(() => browser.getWindowSize());
const automationInfrastructure = memoize(() => {
    const automationInfra = isPerfecto() ? browser.execute('mobile:handset:info', {property: 'automationInfrastructure'})
        : browser.capabilities.automationName;
    return automationInfra;
});
const mobileNumber = memoize(() => {
    const getPhoneFromHS = () => (isHeadspin() ? hsUtils.getDeviceIfo(browser.capabilities.udid).phone_number : '');
    const phoneNumber = isPerfecto() ? browser.execute('mobile:handset:info', {property: 'phoneNumber'}) : getPhoneFromHS();
    if (phoneNumber && phoneNumber.length > 0) return phoneNumber;
    return '';
});
const isMobileNumberRequired = memoize(() => {
    const envType = envEnum[testEnvironment()].testPhase;
    return (envType && ['UAT', 'BR', 'FOV', 'MFIT'].includes(envType));
});
const isXCUITest = () => isIOS() && (!isPerfecto() || (isPerfecto() && automationInfrastructure() === 'XCUITest'));
// For local execution it should be possible to get the deviceId with: browser.capabilities.udid
// However, we normally only run one device at a time locally, so it normally doesn't matter.
// If we implement local parallel execution we need to review this.
const deviceId = memoize(() => (isPerfecto() ? browser.execute('mobile:handset:info', {property: 'deviceId'}) : browser.capabilities.udid), {async: true});
const isGreaterThaniOS10 = memoize(() => parseFloat(osVersion().match(/(\d+\.\d+)/)[1]) > parseFloat('10.3'));
const isIOS9 = memoize(() => osVersion().match(/(\d+).*/)[1] === '9');
const isSmallScreenSize = memoize(() => windowHandleSize().height <= 1280);

function changeCapabilities(brand, environment) {
    browser.options.requestedCapabilities.w3cCaps.alwaysMatch.brand = brand;
    browser.capabilities.brand = brand;
    browser.capabilities.appPackage = AppInfo.getAppInfo(browser.capabilities).appPackage;
    browser.options.requestedCapabilities.w3cCaps.alwaysMatch.appPackage = AppInfo.getAppInfo(
        browser.capabilities
    ).appPackage;
    browser.capabilities.desired.appPackage = AppInfo.getAppInfo(browser.capabilities).appPackage;
    browser.capabilities.server = environment;
    delete require.cache[require.resolve('../screens/native/android.helper')];
}

function relaunchApp(useInstalledApp = browser.capabilities.useInstalledApp) {
    if (!isPerfecto()) {
        driver.launchApp();
    } else if (useInstalledApp) {
        const appInfo = AppInfoFactory.getAppInfo(browser.capabilities);
        browser.execute('mobile:application:close', {identifier: appInfo.appPackage});
        browser.pause(5000);
        browser.execute('mobile:application:open', {identifier: appInfo.appPackage});
    } else {
        const appInfo = AppInfoFactory.getAppInfo(browser.capabilities);
        try {
            browser.execute('mobile:application:close', {identifier: appInfo.appPackage});
        } catch (e) {
            // console.log(e.message);
        } finally {
            if (isPerfecto()) {
                browser.execute('mobile:application:open', {identifier: appInfo.appPackage});
            } else {
                browser.launch();
            }
        }
    }
}

function rebootDevice() {
    if (isPerfecto()) {
        perfectoRestClient.rebootAllDevices(deviceListForReboot);
    } else if (isHeadspin()) {
        hsUtils.restartDevice(browser.capabilities.udid);
    }
}

const perfectoAppInstall = (instrument, sensorInstrument) => {
    try {
        browser.execute('mobile:application:install', {
            file: browser.appURI,
            instrument: instrument ? 'instrument' : 'noinstrument',
            sensorInstrument: sensorInstrument ? 'sensor' : 'nosensor'
        });
    } catch (err) {
        logger.error(`Installation has failed with \n ${err.message}`);
        throw err;
    }
};

const installAppFromSpecificFolderInPerfecto = (folderName, instrument, sensorInstrument) => {
    if (!isPerfecto()) {
        throw new Error('Currently installAppBanVersion is only for Perfecto');
    }
    const appInfo = AppInfoFactory.getAppInfo(browser.capabilities);
    const file = `PRIVATE:applications/${folderName}/${appInfo.fileName()}`;
    browser.execute('mobile:application:install', {file, instrument, sensorInstrument});
};

const installAppBanVersion = (instrument = 'noinstrument', sensorInstrument = 'nosensor') => installAppFromSpecificFolderInPerfecto(
    appVersionHelper.getAppBanVersion(getAppType(), browser.capabilities.platformName.toUpperCase()),
    instrument,
    sensorInstrument
);

const installAppWarnVersion = (instrument = 'noinstrument', sensorInstrument = 'nosensor') => installAppFromSpecificFolderInPerfecto(
    appVersionHelper.getAppWarnVersion(getAppType(), browser.capabilities.platformName.toUpperCase()),
    instrument,
    sensorInstrument
);

/**
 * On Perfecto iOS, we have to uninstall and reinstall the app every time because
 * is no clean command.
 */
function ensureCleanAppInstall(appConfiguration, isEnrolled = false, forceInstall = false, accountTypes) {
    let firstLaunchAfterInstall = false;
    const appInfo = AppInfoFactory.getAppInfo(browser.capabilities);

    const appUnInstall = () => {
        try {
            browser.removeApp(appInfo.appPackage);
        } catch (err) {
            logger.error(err.message);
            if (!((err.message.indexOf('can\'t find bundleId on the device') === -1)
                || (err.message.indexOf('Cannot find identifier') === -1))) {
                throw new Error(`App uninstallation failed appInfo.appPackage:${appInfo.appPackage}`);
            }
        }
    };
    const deviceState = new DeviceState(deviceId());
    const appInstaller = new AppInstaller(deviceState, perfectoAppInstall, appUnInstall);
    const resetApp = (enrolledUser, accType) => {
        if (isAndroid()) {
            // Application will be reset
            browser.execute('mobile:application:clean', {identifier: appInfo.appPackage});
        } else if (accType !== 'max device enrolled') {
            // De-enrol user from backend except for user type max device enrolled
            deEnrolUser(enrolledUser, Server.fromString(browser.capabilities.server));
        }
    };
    if (isPerfecto()) {
        if (!isUAT()) {
            const installedApp = appInstaller.installApp(
                appConfiguration.instrument,
                appConfiguration.sensorInstrument,
                forceInstall
            );
            const enrolledUser = deviceState.getEnrolledUser();
            if (!isStub() && !isEnrolled && !installedApp && enrolledUser !== null) {
                resetApp(enrolledUser, accountTypes);
                deviceState.setEnrolledUser(null);
                firstLaunchAfterInstall = false;
            }
            if (installedApp) {
                firstLaunchAfterInstall = true;
            }
        }
        if (firstLaunchAfterInstall === true) {
            logger.info('Slower devices longer to launch App');
            browser.execute('mobile:application:open', {identifier: appInfo.appPackage});
            browser.pause(15000);
        } else {
            try {
                browser.execute('mobile:application:close', {identifier: appInfo.appPackage});
            } catch (e) {
                // We don't care about this exception, we just want to make sure that the application is closed.
            }
            browser.execute('mobile:application:open', {identifier: appInfo.appPackage});
        }
        browser.pause(5000);
    } else {
        browser.reset();
    }
}

module.exports = {
    isBNGA,
    isRNGA,
    isMBNA,
    getAppType,
    isUAT,
    isAndroid,
    isIOS,
    isIOS9,
    isPerfecto,
    isHeadspin,
    isStub,
    isEnv,
    isMobileNumberRequired,
    mobileNumber,
    relaunchApp,
    ensureCleanAppInstall,
    perfectoAppInstall,
    testEnvironment,
    windowHandleSize,
    getAppName,
    osVersion,
    isBR,
    isMFIT,
    deviceId,
    getBrand,
    rebootDevice,
    deEnrolUser,
    isGreaterThaniOS10,
    isRetailIOS,
    isXCUITest,
    isSmallScreenSize,
    installAppBanVersion,
    installAppWarnVersion,
    changeCapabilities
};
