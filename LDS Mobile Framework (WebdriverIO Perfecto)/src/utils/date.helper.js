class DateHelper {
    static getDayIndex(date) {
        let day = date.getDate();
        if (day < 10) {
            day = `0${day}`;
        }
        return day;
    }

    static getMonthString(month) {
        const expectedMonth = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        return expectedMonth[month];
    }

    static getFullMonthString(month) {
        const expectedMonth = ['January', 'February', 'March', 'April', 'May', 'June',
            'July', 'August', 'September', 'October', 'November', 'December'];
        return expectedMonth[month];
    }
}

module.exports = DateHelper;
