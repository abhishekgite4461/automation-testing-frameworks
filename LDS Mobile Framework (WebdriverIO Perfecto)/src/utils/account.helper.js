const DeviceHelper = require('./device.helper');

class AccountHelper {
    static isValidAmount(amt, type) {
        const amount = amt;
        let regex = /-?£?[-+]?[0-9.,]+\.[0-9]+?$|nil|NIL|N\/A/;
        if (type === 'loan') {
            regex = /^-£(\d+)(,(\d+))?(,(\d+))?(,(\d+))?(\.(\d+))?$/;
        }

        if (!regex.test(amount)) {
            throw new Error(`${amount} - Not a valid amount field!!`);
        }
    }

    static isNotApplicableAmount(amt) {
        const amount = amt;
        const regex = /^N\/A|Not Available/i;

        if (!regex.test(amount)) {
            throw new Error(`${amount} - Not a N/A amount field!!`);
        }
    }

    static isNegativeAmount(amt) {
        const amount = amt;
        const regex = /^-£[0-9.,]+.?[0-9.,]*$/;
        if (!regex.test(amount)) {
            throw new Error(`${amount} - Not a Negative amount field!!`);
        }
    }

    static isNilAmount(amt) {
        const amount = amt;
        const regex = /^Nil/i;

        if (!regex.test(amount)) {
            throw new Error(`${amount} - Not a NIL Amount`);
        }
    }

    static isValidAccountNumber(accNo) {
        // const regex = /^\d{8}$/;
        const regex = /^[\d\s]{15}|^\d{8}$|Account number\. \d{8}$/;
        if (!regex.test(accNo)) {
            throw new Error(`${accNo} - Not a valid AccountNumber!!`);
        }
    }

    static isValidSortCode(sortcode) {
        let sortCode = sortcode;
        const regex = /^(Sortcode\.)?\d{2}-?\d{2}-?\d{2}$/;

        if (DeviceHelper.isIOS()) {
            sortCode = sortCode.replace(/\s+/g, '');
        }
        if (!regex.test(sortCode)) {
            throw new Error(`${sortCode} - Not a valid Sort code!!`);
        }
    }

    static isValidInterestRate(rate) {
        const interestRate = rate;
        const regex = /^(\d*)(\.\d+%\s)gross$/;

        if (!regex.test(interestRate)) {
            throw new Error(`${interestRate} - Not a valid interest value!!`);
        }
    }

    static isValidInterestRateForIsa(rate) {
        const interestRate = rate;
        const regex = /^(\d*)(\.\d+%\s)tax free$|Not available/;

        if (!regex.test(interestRate)) {
            throw new Error(`${interestRate} - Not a valid interest value for ISA!!`);
        }
    }

    static isValidCreditCardAccountNumber(accNumber) {
        const regex = /^[0-9]{3,4}(\s?)[0-9]{4}(\s?)[0-9]{4}(\s?)[0-9]{3,4}$/;

        if (!regex.test(accNumber)) {
            throw new Error(`${accNumber} - Not a valid credit card account number!!`);
        }
    }

    static isValidCreditCardAvailableBalance(amt) {
        let amount = amt;
        const regex = /^£?\d{1,3}(,\d{3})*(\.\d{2})|nil|NIL|N\/A/g;

        if (DeviceHelper.isAndroid()) {
            if (amt.indexOf('cr') >= 0) {
                amount = amt.split('cr')[0].trim();
            }
        }

        if (!regex.test(amount)) {
            throw new Error(`${amount} - Not a valid credit card available balance!!`);
        }
    }

    static isnegativeCreditCardAvailableBalance(amt) {
        let amount = amt;
        const regex = /^-?£?\d{1,3}(,\d{3})*(\.\d{2})|nil|NIL|N\/A/g;

        if (DeviceHelper.isAndroid()) {
            if (amt.indexOf('cr') >= 0) {
                amount = amt.split('cr')[0].trim();
            }
        }

        if (!regex.test(amount)) {
            throw new Error(`${amount} - Not credit card available balance!!`);
        }
    }

    static isValidMortgageAccountNumber(accNumber) {
        let accountNumber = accNumber;
        const regex = /^[0-9]{14}$/;

        if (DeviceHelper.isIOS()) {
            accountNumber = accNumber.split('.')[1].trim();
        } else {
            accountNumber = accNumber.split('Ref:')[1].trim();
        }

        if (!regex.test(accountNumber)) {
            throw new Error(`${accountNumber} - Not a valid mortgage account number!!`);
        }
    }

    static isValidDate(date) {
        const regex = /^\d{2}(-|\s)[a-zA-Z]{3}(-|\s)\d{4}$|Not available|nil|NIL|N\/A/;
        if (!regex.test(date)) {
            throw new Error(`${date} - Not a valid date field!!`);
        }
    }

    static isValidLoanAccountNumber(accNumber) {
        let accountNumber = accNumber;
        const regex = /^[0-9]{12}$/;

        if (DeviceHelper.isAndroid()) {
            accountNumber = accNumber.split('Ref: ')[1].trim();
        }

        if (!regex.test(accountNumber)) {
            throw new Error(`${accountNumber} - Not a valid loan account number!!`);
        }
    }

    static isValidStatementDate(date) {
        const regex = /^\d{2} [a-zA-Z]{3} \d{4}$|N\/A/i;
        const actualDate = date.includes(',') ? date.toString().split(',')[1].trim() : date.toString().trim();

        if (!regex.test(actualDate)) {
            throw new Error(`${actualDate} - Not a valid date field!!`);
        }
    }

    static isValidStatementAmount(amt) {
        const regex = /^£(\d+)(,(\d+))?(,(\d+))?(,(\d+))?(\.(\d+))?$|nil|NIL|N\/A/i;
        const amount = amt.includes(',') ? amt.toString().split(',')[1].trim() : amt.toString().trim();

        if (!regex.test(amount)) {
            throw new Error(`${amount} - Not a valid amount field!!`);
        }
    }

    static isValidTextField(name) {
        const regex = /^[a-zA-Z\s\d.:]*$/;
        if (!regex.test(name)) {
            throw new Error(`${name} - Not a valid text field!!`);
        }
    }

    static isValidThirtyTwoDayNoticeAccountNumber(accNumber) {
        let accountNumber = accNumber;
        const regex = /[A-Z0-9 ]*$/;

        if (DeviceHelper.isIOS()) {
            accountNumber = accNumber.split('Account number. ')[1];
        }

        if (!regex.test(accountNumber)) {
            throw new Error(`${accountNumber} - Not a valid thirty two day notice account number!!`);
        }
    }

    static isLogoDetailsDisplayed(logoDetails) {
        const details = logoDetails;
        const regex = /Bank\s\w+|logo/;

        if (!regex.test(details)) {
            throw new Error(`${details} - Bank logo not displayed!!`);
        }
    }

    static isOverdraftLimitLabelDisplayed(label) {
        const overdraftLimit = label;
        const regex = /Overdraft\s\w+|limit/;

        if (!regex.test(overdraftLimit)) {
            throw new Error(`${overdraftLimit} - Overdraft Limit label is not displayed!!`);
        }
    }

    static isRemainingOverdraftLabelDisplayed(label) {
        const overdraftLimit = label;
        const regex = /Remaining\s\w+|overdraft/;

        if (!regex.test(overdraftLimit)) {
            throw new Error(`${overdraftLimit} - Remaining Overdraft label is not displayed!!`);
        }
    }

    static isValidRemainingOverdraftValue(amt) {
        const amount = amt.split('Remaining overdraft: ')[1];
        const regex = /-?£?[-+]?[0-9.,]+\.[0-9]+?$|nil|NIL|N\/A/;

        if (!regex.test(amount)) {
            throw new Error(`${amount} - Remaining Overdraft Value is not displayed!!`);
        }
    }
}

module.exports = AccountHelper;
