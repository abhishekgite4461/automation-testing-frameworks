const UserType = require('../enums/userType.enum');

module.exports = (accTypes) => {
    if (!accTypes) {
        throw new Error('No user account type specified.');
    }
    const accountTypes = accTypes.trim().toUpperCase();
    if (accountTypes.indexOf(' OR ') !== -1) {
        throw new Error(`Or is not supported in TDaaS 2.0: ${accountTypes}`);
    }
    const types = accountTypes.split(' AND ');

    const userTypes = types.map((type) => {
        const userType = UserType.fromString(type.trim().replace(/ /g, '_'));
        if (!userType) {
            throw new Error(`'${userType}' is not a valid account type. Please check userType.enum`);
        }
        return userType.toString();
    });
    return userTypes;
};
