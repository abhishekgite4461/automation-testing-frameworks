const fs = require('fs');

module.exports = class DeviceState {
    constructor(deviceID) {
        this.deviceID = deviceID;
        this.fileName = `./tmp/${this.deviceID}.json`;
    }

    // This method only exists to make unit tests run.
    get state() {
        return this.loadState();
    }

    loadState() {
        let state;
        try {
            state = JSON.parse(fs.readFileSync(this.fileName));
        } catch (e) {
            state = {
                appInstalled: false,
                instrument: false,
                sensorInstrument: false,
                enrolledUser: null
            };
        }
        return state;
    }

    saveState(state) {
        fs.writeFileSync(this.fileName, JSON.stringify(state));
    }

    isAppInstallationRequired(instrument, sensorInstrument) {
        const state = this.loadState();
        return !state.appInstalled
            || (state.instrument !== instrument && !state.instrument)
            || state.sensorInstrument !== sensorInstrument;
    }

    setAppInstalled(appInstalled, instrument, sensorInstrument) {
        const state = this.loadState();
        state.appInstalled = appInstalled;
        state.instrument = instrument;
        state.sensorInstrument = sensorInstrument;
        this.saveState(state);
    }

    setEnrolledUser(user) {
        const state = this.loadState();
        state.enrolledUser = user;
        this.saveState(state);
    }

    getEnrolledUser() {
        const state = this.loadState();
        return state.enrolledUser;
    }
};
