const request = require('superagent');
const logger = require('superagent-logger');

module.exports = class TDaaS2Client {
    constructor(
        username = process.env.TDAAS_USER,
        password = process.env.TDAAS_PASSWORD,
        group = process.env.TDAAS_GROUP,
        url = 'http://test-data-service.mobile.sbx.zone'
    ) {
        this.username = username;
        this.password = password;
        this.group = group;
        this.url = url;
    }

    verifyCredentials() {
        return request
            .get(`${this.url}/debug/auth`)
            .auth(this.username, this.password)
            .use(logger)
            .then((res) => res.text);
    }

    createReservation(environment, appType, brand, accountTypes, mobileNumber) {
        const params = {};
        params.environment = environment;
        params.appType = appType;
        params.brand = brand;
        params.accountTypes = accountTypes;
        params.mobileNumber = mobileNumber;
        params.group = this.group;
        return request
            .post(`${this.url}/customers/reservations`)
            .auth(this.username, this.password)
            .send(params)
            .use(logger)
            .then((res) => res.body);
    }

    deleteReservation(account) {
        const customerID = account._id;
        return request
            .delete(`${this.url}/customers/${customerID}/reservations`)
            .auth(this.username, this.password)
            .use(logger)
            .then(() => true);
    }

    deleteAllReservations() {
        return request
            .delete(`${this.url}/customers/reservations`)
            .auth(this.username, this.password)
            .use(logger)
            .then(() => true);
    }

    getAllCustomers() {
        return request
            .get(`${this.url}/customers`)
            .auth(this.username, this.password)
            .use(logger)
            .then((res) => res.body);
    }
};
