const denodify = require('denodeify');
const request = require('request');
const util = require('util');
const parseXml = denodify(require('xml2js').parseString);

const logger = require('./logger');
const {sleep, tryAtMost} = require('./utils');

const WAIT_TIME_TO_RECOVER = 180 * 1000;
const WAIT_TIME_TO_REBOOT = 300 * 1000;

const DEVICE_STATUS_CHECK_TRIES = 10;
const DEVICE_STATUS_CHECK_INTERVAL = 30 * 1000;

const DeviceInfo = {
    Manufacturer: 'manufacturer',
    Model: 'model',
    PhoneNumber: 'phoneNumber',
    DeviceID: 'deviceId',
    Resolution: 'resolution',
    ResolutionWidth: 'resolutionWidth',
    ResolutionHeight: 'resolutionHeight',
    OS: 'os',
    OSVersion: 'osVersion',
    OSBuild: 'osBuild',
    Firmware: 'firmware',
    Location: 'location',
    Distributor: 'distributor',
    Language: 'language',
    Imei: 'imei',
    NativeImei: 'nativeImei',
    WiFiMACAddress: 'wifiMacAddress',
    CradleID: 'cradleId',
    Status: 'status',
    InUse: 'inUse',
    Description: 'description',
    Position: 'position',
    Method: 'method',
    Rotation: 'rotation',
    OperabilityScore: 'operabilityRatingScore',
    AutomationInfrastructure: 'automationInfrastructure',
    Roles: 'roles',
    LastUsedBy: 'lastUsedBy'
};

class PerfectoRestClient {
    constructor(host = process.env.PERFECTO_HOST || 'lloyds.perfectomobile.com',
        user = process.env.PERFECTO_USER,
        pwd = process.env.PERFECTO_PWD,
        proxyHost = process.env.PROXY_HOST,
        proxyPort = process.env.PROXY_PORT) {
        if (host === undefined || user === undefined || pwd === undefined) {
            throw new Error('Host, user, and pwd mustn\'t be undefined');
        }

        this.host = host;
        this.user = user;
        this.pwd = pwd;

        if (proxyHost !== undefined && proxyPort !== undefined) {
            this.get = denodify(request.defaults({proxy: `http://${proxyHost}:${proxyPort}`}).get);
        } else {
            this.get = denodify(request.get);
        }
    }

    makeGetRequest(path, qs) {
        return this.get({
            url: `https://${this.host}/${path}`,
            qs: {...qs,
                user: this.user,
                password: this.pwd}
        })
            .then((result) => {
                if (result.statusCode !== 200 || result.body.errorMessage || (result.body.reason && result.body.reason !== 'Success')) {
                    logger.info(util.inspect(result.body));
                    throw new Error(`Perfecto request failed: ${util.inspect(result.body)}`);
                }
                // While the default is JSON, some endpoints only return XML!
                try {
                    return JSON.parse(result.body);
                } catch (SyntaxError) {
                    return parseXml(result.body);
                }
            });
    }

    executeCommand(executionId, deviceId, command, subcommand, additionalParams) {
        let params = {
            operation: 'command',
            command,
            subcommand,
            'param.deviceId': deviceId
        };
        if (additionalParams) {
            params = Object.assign(params, additionalParams);
        }
        return this.makeGetRequest(`services/executions/${executionId}`, params);
    }

    repositoryOperation(command, path) {
        const params = {
            operation: command
        };
        return this.makeGetRequest(`services/repositories/${path}`, params);
    }

    /* Opens the given device for script execution ONLY if Allocate Automatically
     * is turned OFF
     */
    openDevice(executionId, deviceId) {
        return this.executeCommand(executionId, deviceId, 'device', 'open');
    }

    closeDevice(executionId, deviceId) {
        return this.executeCommand(executionId, deviceId, 'device', 'close');
    }

    getDeviceInfo(executionId, deviceId, params) {
        return this.executeCommand(executionId, deviceId, 'device', 'info', params);
    }

    rebootDevice(executionId, deviceId) {
        return this.executeCommand(executionId, deviceId, 'device', 'reboot');
    }

    recoverDevice(executionId, deviceId) {
        return this.executeCommand(executionId, deviceId, 'device', 'recover');
    }

    listRepositoryItem(path) {
        return this.repositoryOperation('list', path);
    }

    deleteRepositoryItem(path) {
        return this.repositoryOperation('delete', path);
    }

    uninstallApp(executionId, deviceId, identifier) {
        return this.executeCommand(executionId, deviceId, 'application', 'uninstall', {'param.identifier': identifier});
    }

    uninstallAllApps(executionId, deviceId) {
        return this.executeCommand(executionId, deviceId, 'application', 'reset');
    }

    /* Opens a new session and returns an executionId (sessionId)
    */
    startExecution() {
        return this.makeGetRequest('services/executions', {operation: 'start'})
            .then((res) => res.executionId);
    }

    endExecution(executionId) {
        return this.makeGetRequest(`services/executions/${executionId}`, {operation: 'end'})
            .then((result) => {
                if (result.status !== 'Success') {
                    throw new Error(`Failed to end execution, got: ${result}`);
                }
            });
    }

    getAllDeviceIds() {
        return this.makeGetRequest('services/handsets', {operation: 'list'})
            .then((result) => result.handsets.handset
                .map((handset) => handset.deviceId)
                .map((deviceIdWrapper) => {
                    if (deviceIdWrapper.length !== 1) {
                        throw new Error(`Unexpected non-singleton device Id: ${deviceIdWrapper}`);
                    }
                    return deviceIdWrapper[0];
                }));
    }

    releaseDevice(deviceId) {
        return this.makeGetRequest(`services/handsets/${deviceId}`, {operation: 'release'});
    }

    getAllDeviceInfo(deviceId) {
        return this.withOpenSession((executionId) => this.getDeviceInfo(executionId, deviceId, {
            'param.property': 'all'
        })
            .then((result) => {
                const properties = result.returnValue.substring(1, result.returnValue.length - 1);
                let allProperties = properties.split(',')
                    .map((ele) => ele.trim());
                    // the below if logic is to slice the random number which appears after the modelCode in ios
                if ((/^[0-9]*$/).test(allProperties[allProperties.indexOf('modelCode') + 2])) {
                    allProperties = allProperties.slice(
                        0, allProperties.indexOf('modelCode') + 2
                    )
                        .concat(
                            allProperties.slice(
                                allProperties.indexOf('modelCode') + 3, allProperties.length
                            )
                        );
                }
                const propertyPair = {};
                for (let index = 0; index < allProperties.length; ++index) {
                    const key = allProperties[index].trim();
                    let value = allProperties[++index].trim();
                    if (value.startsWith('[') && !value.endsWith(']')) {
                        while (!allProperties[++index].endsWith(']')) {
                            value += (`,${allProperties[index]}`);
                        }
                        value += (`,${allProperties[index]}`);
                        Object.assign(propertyPair, {
                            [key]: value
                        });
                    } else {
                        Object.assign(propertyPair, {
                            [key]: value
                        });
                    }
                }
                return propertyPair;
            }));
    }

    getDeviceStatus(executionId, deviceId) {
        return this.getDeviceInfo(executionId, deviceId, {
            'param.property': 'status'
        })
            .then((result) => result.returnValue);
    }

    getDeviceReservations(deviceIds, startTimeMillis, endTimeMillis) {
        return this.makeGetRequest('services/reservations', {
            operation: 'list',
            resourceIds: deviceIds.join(','),
            startTime: startTimeMillis,
            endTime: endTimeMillis,
            admin: true
        })
            .then((result) => result.reservations.map((reservation) => ({
                startTime: reservation.startTime,
                endTime: reservation.endTime,
                status: reservation.status,
                created: reservation.created,
                lastModified: reservation.lastModified,
                reservedBy: reservation.reservedBy,
                deviceId: reservation.resourceId,
                id: reservation.id
            })));
    }

    getCurrentReservations(deviceIds, startTimeMilis, endTimeMilis) {
        return this.getDeviceReservations(deviceIds, startTimeMilis, endTimeMilis)
            .then((reservations) => reservations.filter((reservation) => reservation.status !== 'ENDED'));
    }

    makeDeviceReservations(deviceIds, startTimeMillis, endTimeMillis) {
        return this.makeGetRequest('services/reservations', {
            operation: 'create',
            resourceIds: deviceIds.join(','),
            startTime: startTimeMillis,
            endTime: endTimeMillis,
            admin: true
        })
            .then((result) => result.reservationIds);
    }

    removeDeviceReservations(deviceIds, startTimeMillis, endTimeMillis) {
        return this.getCurrentReservations(deviceIds, startTimeMillis, endTimeMillis)
            .then((reservations) => Promise.all(reservations
                .map((reservation) => this.makeGetRequest(`services/reservations/${reservation.id}`, {
                    operation: 'delete',
                    admin: true
                })
                    .then((status) => {
                        if (status === 'failure') {
                            return 'Perfecto returned a failure status when '
                                    + `removing reservation(id: ${reservation.id})`;
                        }
                        return null;
                    })
                    .catch((err) => `API failure removing device registration(id: ${reservation.id})  ${err}`))))
            .then((results) => results.filter((result) => (result !== null)));
    }

    withOpenSession(func) {
        let stashedError = null;
        let stashedResult = null;
        return this.startExecution()
            .then((id) => func(id)
                .then((result) => (stashedResult = result))
                .catch((err) => (stashedError = err))
                .then(() => this.endExecution(id)
                    .catch((recoveryErr) => logger.error(recoveryErr))
                    .then(() => {
                        if (stashedError) {
                            throw stashedError;
                        }
                        return stashedResult;
                    })));
    }

    withDeviceExecution(executionId, deviceId, func) {
        let stashedError = null;
        let stashedResult = null;
        return this.openDevice(executionId, deviceId)
            .then(() => func()
                .then((result) => (stashedResult = result))
                .catch((err) => (stashedError = err))
                .then(() => this.closeDevice(executionId, deviceId)
                    .catch((recoveryErr) => logger.error(recoveryErr))
                    .then(() => {
                        if (stashedError) {
                            throw stashedError;
                        }
                        return stashedResult;
                    })));
    }

    isDeviceConnected(executionId, deviceId) {
        return tryAtMost(DEVICE_STATUS_CHECK_TRIES, DEVICE_STATUS_CHECK_INTERVAL, () => {
            logger.info(`Checking status of device ${deviceId}`);
            return this.getDeviceStatus(executionId, deviceId)
                .then((status) => {
                    logger.info(`Device ${deviceId} status is '${status}'`);
                    if (status !== 'CONNECTED') {
                        throw Error(`Device ${deviceId} status is '${status}'`);
                    }
                    logger.info(`Device ${deviceId} is CONNECTED`);
                });
        });
    }

    /**
     * Recovers device identified by id. A session is opened followed by a new execution followed
     * by recover command. The recover command is run with a retry. After each recover command
     * it waits for sometime before it checks if the device is CONNECTED, if not it checks the
     * device status in a retry loop at regular intervals. The result is returned in the form
     * [null, error] or [result, null].
     */

    /* eslint-disable max-len */
    recoverDeviceWithRetry(id, reboot = false) {
        return Promise.resolve(id)
            .then((deviceId) => {
                logger.info(`Starting session for device ${deviceId}`);
                return this.withOpenSession((executionId) => {
                    logger.info(`Recovering device ${deviceId}`);
                    return this.recoverDevice(executionId, deviceId)
                        .then(() => {
                            logger.info(`Recover command issued, waiting ${WAIT_TIME_TO_RECOVER / (60 * 1000)} min to check status of device ${deviceId}`);
                            return sleep(WAIT_TIME_TO_RECOVER)()
                                .then(() => this.isDeviceConnected(executionId, deviceId));
                        })
                        .catch(() => {
                            if (!reboot) {
                                throw new Error(`Failed to recover device ${deviceId}`);
                            }
                            logger.info(`Failed to recover, now rebooting device ${deviceId}`);
                            return this.rebootDevice(executionId, deviceId)
                                .then(() => {
                                    logger.info(`Reboot command issued, waiting ${WAIT_TIME_TO_REBOOT / (60 * 1000)} min to check status of device ${deviceId}`);
                                    return sleep(WAIT_TIME_TO_REBOOT)()
                                        .then(() => this.withOpenSession((execId) => this.isDeviceConnected(execId, deviceId)));
                                });
                        });
                })
                    .then(
                        () => ({deviceId, isSuccess: true}),
                        (err) => ({deviceId, isSuccess: false, error: err})
                    );
            });
    }

    rebootDeviceWithRetry(id) {
        return Promise.resolve(id)
            .then((deviceId) => {
                logger.info(`Starting session for device ${deviceId}`);
                return this.withOpenSession((executionId) => {
                    logger.info(`Rebooting device ${deviceId}`);
                    return this.rebootDevice(executionId, deviceId)
                        .then(() => {
                            logger.info(`Reboot command issued, waiting ${WAIT_TIME_TO_REBOOT / (60 * 1000)} min to check status of device ${deviceId}`);
                            return sleep(WAIT_TIME_TO_REBOOT)()
                                .then(() => this.withOpenSession((execId) => this.isDeviceConnected(execId, deviceId)));
                        });
                });
            });
    }

    /**
     * Uninstalls a single named applications identified by the identifer on a device. The method opens a session for
     * the device and then ensues the command to uninstall all the application on the device.
     */
    uninstallApplication(id, appIdentifier) {
        return Promise.resolve(id)
            .then((deviceId) => {
                logger.info(`Starting session for device ${deviceId}`);
                return this.withOpenSession((executionId) => {
                    logger.info(`Uninstalling application ${appIdentifier} on device ${deviceId}`);
                    return this.uninstallApp(executionId, deviceId, appIdentifier)
                        .then(() => {
                            logger.info(`Command issued to uninstall application ${appIdentifier} on device ${deviceId}`);
                        })
                        .catch((err) => {
                            logger.info(`Failed to uninstall application on device ${deviceId} for following reasons:\n${err.message}`);
                        });
                })
                    .then(
                        () => ({deviceId, isSuccess: true}),
                        (err) => ({deviceId, isSuccess: false, error: err})
                    );
            });
    }

    /**
     * Uninstalls all third party applications on a device. This does not effect the pre-installed applications on the
     * device. In effect the device is reset to its factory state albeit all settings and configurations are maintained
     * on the device. The method opens a session for each device in the list and then ensues the command to uninstall
     * all third party apps on the device.
     */
    uninstallAllApplications(deviceIds) {
        return Promise.all(deviceIds.map((id) => Promise.resolve(id)
            .then((deviceId) => {
                logger.info(`Starting session for device ${deviceId}`);
                return this.withOpenSession((executionId) => {
                    logger.info(`Uninstalling all applications on device ${deviceId}`);
                    return this.uninstallAllApps(executionId, deviceId)
                        .then(() => {
                            logger.info(`Command issued to uninstall all third party applications on device ${deviceId}`);
                        })
                        .catch((err) => {
                            logger.info(`Failed to uninstall all applications on device ${deviceId} for following reasons:\n${err.message}`);
                            throw err;
                        });
                })
                    .then(
                        () => ({deviceId, isSuccess: true}),
                        (err) => ({deviceId, isSuccess: false, error: err})
                    );
            })));
    }

    /* eslint-enable max-len */

    /**
     * Recovers device identified by id. A session is opened, and a new
     * execution and then run recover. Returns result in the form [null, error]
     * or [result, null].
     */
    recoverAllDevices(deviceIds) {
        return Promise.all(deviceIds.map((deviceId) => this.recoverDeviceWithRetry(deviceId, true)));
    }

    rebootAllDevices(deviceIds) {
        return Promise.all(deviceIds.map((deviceId) => this.rebootDeviceWithRetry(deviceId, true)));
    }
}

module.exports = {PerfectoRestClient, DeviceInfo};
