const denodeify = require('denodeify');
const fs = require('fs');
const Gherkin = require('@cucumber/gherkin');
const {IdGenerator, messages} = require('@cucumber/messages');
const path = require('path');

const readFile = denodeify(fs.readFile);

module.exports = function (featureFilePath, failedScenarioName, extraTag) {
    const getRawFeatureData = function (fileLocation) {
        let filePath = fileLocation;
        filePath = path.join(__dirname, `../../${filePath}`);
        return readFile(filePath).then((raw) => raw.toString());
    };

    const parseRawDataToGherkin = function (rawFeatureDataArray) {
        const parser = new Gherkin.Parser(new Gherkin.AstBuilder(IdGenerator.incrementing()));
        return parser.parse(rawFeatureDataArray);
    };

    const generateNewFeatureFileData = function (parsedGherkinData) {
        return new Promise((resolve, reject) => {
            try {
                const newFeatureFileContentArray = [];
                const featureTemplateData = parsedGherkinData;
                const {children} = parsedGherkinData.feature;
                let backgroundPresent = false;
                featureTemplateData.feature.children.forEach((featureChild) => {
                    if (featureChild.background) {
                        const FeatureChild = [];
                        FeatureChild.push(featureChild);
                        featureTemplateData.feature.children = FeatureChild;
                        backgroundPresent = true;
                    }
                });
                if (!backgroundPresent) {
                    featureTemplateData.feature.children = [];
                }
                featureTemplateData.comments = [];
                children.forEach((featureChild, index) => {
                    if (!featureChild.background && backgroundPresent) {
                        featureTemplateData.feature.children[1] = (children[index]);
                        newFeatureFileContentArray.push(messages.GherkinDocument.toObject(featureTemplateData));
                    } else if (!backgroundPresent) {
                        featureTemplateData.feature.children[0] = (children[index]);
                        newFeatureFileContentArray.push(messages.GherkinDocument.toObject(featureTemplateData));
                    }
                });
                return resolve(newFeatureFileContentArray);
            } catch (e) {
                return reject(e);
            }
        });
    };

    const generateNewFeatureFile = function (featureParsedData, failedScenario, customTag, featureFileFolderPath) {
        return new Promise((resolve, reject) => {
            try {
                const spec = [];
                return featureParsedData.forEach((newFeatureParsedData) => {
                    const delimiter = '\n';
                    const stepDelimiter = '\t';
                    let rawFeatureData = '';
                    let scenarioName = '';
                    let featureTags = '';
                    newFeatureParsedData.feature.tags.forEach((tag) => {
                        featureTags = `${featureTags + tag.name} `;
                        featureTags += customTag;
                    });
                    featureTags = featureTags.toString();
                    rawFeatureData = rawFeatureData + featureTags + delimiter;
                    const featureKeyword = `${newFeatureParsedData.feature.keyword}: `;
                    const featureName = newFeatureParsedData.feature.name;
                    rawFeatureData = rawFeatureData + featureKeyword + featureName + delimiter;
                    let featureDescription = '';
                    if (newFeatureParsedData.feature.description) {
                        featureDescription = newFeatureParsedData.feature.description;
                    } else {
                        featureDescription = '\n';
                    }
                    rawFeatureData = rawFeatureData + featureDescription + delimiter;
                    newFeatureParsedData.feature.children.forEach((featureChild) => {
                        let featureBackground = '';
                        let featureScenario = '';
                        if (featureChild.background) {
                            featureBackground = `${delimiter + featureBackground}  ${featureChild.background.keyword}: `;
                            featureBackground = featureBackground + featureChild.background.name + delimiter;
                            featureChild.background.steps.forEach((step) => {
                                featureBackground += stepDelimiter;
                                featureBackground = step.keyword + step.text + delimiter;
                            });
                        }
                        if (featureChild.scenario) {
                            let scenarioTags = '';
                            if (featureChild.scenario.tags) {
                                featureChild.scenario.tags.forEach((tag) => {
                                    scenarioTags = `${scenarioTags + tag.name} `;
                                });
                                scenarioTags += customTag;
                                scenarioTags = `${delimiter}  ${scenarioTags.toString()}`;
                            }
                            featureScenario = featureScenario + scenarioTags + delimiter;
                            featureScenario = `${featureScenario}  ${featureChild.scenario.keyword}: `;
                            scenarioName = featureChild.scenario.name;
                            featureScenario = featureScenario + scenarioName + delimiter;
                            featureChild.scenario.steps.forEach((step) => {
                                featureScenario = featureScenario + stepDelimiter + step.keyword + step.text + delimiter;
                                if (step.dataTable) {
                                    step.dataTable.rows.forEach((dataTableValue) => {
                                        dataTableValue.cells.forEach((dataTableCellValue) => {
                                            let tableValue = '';
                                            tableValue = `${featureScenario + stepDelimiter}|${dataTableCellValue.value}`;
                                            featureScenario = tableValue;
                                        });
                                        featureScenario = `${featureScenario}|${delimiter}`;
                                    });
                                }
                            });
                            if (featureChild.scenario.keyword === 'Scenario Outline') {
                                featureChild.scenario.examples.forEach((examples) => {
                                    featureScenario += delimiter;
                                    featureScenario = `${featureScenario + stepDelimiter + examples.keyword}: `;
                                    featureScenario += delimiter;
                                    examples.tableHeader.cells.forEach((exampleHeader) => {
                                        featureScenario = `${featureScenario + stepDelimiter}|${exampleHeader.value}`;
                                    });
                                    featureScenario = `${featureScenario}|${delimiter}`;
                                    examples.tableBody.forEach((exampleBody) => {
                                        exampleBody.cells.forEach((exampleBodyValue) => {
                                            let exampleValue = '';
                                            exampleValue = `${featureScenario + stepDelimiter}|${exampleBodyValue.value}`;
                                            featureScenario = exampleValue;
                                        });
                                        featureScenario = `${featureScenario}|${delimiter}`;
                                    });
                                });
                            }
                        }
                        rawFeatureData = rawFeatureData + featureBackground + featureScenario;
                    });
                    if (scenarioName === failedScenario) {
                        let newFeatureFileName = `${featureName}_${scenarioName}`;
                        newFeatureFileName += customTag;
                        newFeatureFileName = newFeatureFileName.replace(/[^A-Z0-9]+/ig, '_');
                        let newFeatureFilePath = path.join(__dirname, featureFileFolderPath);
                        newFeatureFilePath += '/features/reRun';
                        newFeatureFilePath = `${newFeatureFilePath}/${newFeatureFileName}.feature`;
                        newFeatureFilePath = newFeatureFilePath.replace('//', '/');
                        spec.push(newFeatureFilePath);
                        fs.writeFileSync(newFeatureFilePath, rawFeatureData, 'utf8');
                    }
                    return resolve(spec);
                });
            } catch (e) {
                return reject(e);
            }
        });
    };

    getRawFeatureData(featureFilePath)
        .then((rawData) => parseRawDataToGherkin(rawData))
        .then((parsedGherkinData) => generateNewFeatureFileData(parsedGherkinData))
        .then((newFeatureParsedData) => generateNewFeatureFile(newFeatureParsedData, failedScenarioName, extraTag, '../'));
};
