const path = require('path');
const _ = require('lodash');
const rimraf = require('rimraf');
const fs = require('fs');
const logger = require('./logger');

const cartesianProduct = (...args) => Array.prototype.reduce.call(
    args, (acc, x) => _.flatten(acc.map((accElems) => x.map((xElems) => accElems.concat([xElems])))), [[]]
);

const envVariableExists = (name) => {
    if (!process.env[name]) {
        throw new Error(`Environment variable ${name} was not set`);
    }
};

// This function is only required until we move to Node 8 or higher
// Since this method is Promise based it should not be used with synchronous code.
// Use system-sleep or browser.pause for synchronous code.
const sleep = (ms) => function (...args) {
    return new Promise((resolve) => setTimeout(resolve.bind(null, args), ms));
};

const tryAtMost = (tries, retryInterval, func) => {
    const triesCount = tries - 1;
    logger.debug(`${tries - 1} retries left`);
    return func()
        .catch((err) => {
            if (triesCount > 0) {
                return sleep(retryInterval)()
                    .then(() => tryAtMost(triesCount, retryInterval, func));
            }
            return Promise.reject(err);
        });
};

const resetTmpDir = () => {
    const tmpDir = './tmp';
    rimraf.sync(tmpDir);
    fs.mkdirSync(tmpDir);
};

const maskFields = (objectToMask, ...fieldsToMask) => {
    let clonedObject = null;
    if (objectToMask) {
        clonedObject = {...objectToMask};
        if (fieldsToMask && fieldsToMask.length > 0) {
            for (let index = 0; index < fieldsToMask.length; index++) {
                if (clonedObject[fieldsToMask[index]]) {
                    clonedObject[fieldsToMask[index]] = '********';
                }
            }
        }
    }
    return clonedObject;
};

module.exports = {
    cartesianProduct,
    envVariableExists,
    sleep,
    tryAtMost,
    resetTmpDir,
    maskFields,
    rootDirname: path.join(__dirname, '..')
};
