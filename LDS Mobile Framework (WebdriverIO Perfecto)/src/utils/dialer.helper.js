const iOSDialPadPercentage = {
    1: '30%,31%',
    2: '50%,31%',
    3: '70%,31%',
    4: '30%,43%',
    5: '50%,43%',
    6: '70%,43%',
    7: '30%,60%',
    8: '50%,60%',
    9: '70%,60%',
    0: '50%,70%',
    Keypad: '50%,40%',
    Accept: '75%,82%',
    End: '50%,80%'
};

module.exports = {iOSDialPadPercentage};
