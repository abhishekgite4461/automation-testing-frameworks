const appBanVersion = {
    RNGA: {
        ANDROID: 'release_candidate_argentina',
        IOS: 'release_candidate_argentina'
    },
    BNGA: {
        ANDROID: 'release_candidate_argentina',
        IOS: 'release_candidate_argentina'
    }
};

const appWarnVersion = {
    RNGA: {
        ANDROID: 'release_candidate_argentina',
        IOS: 'release_candidate_argentina'
    },
    BNGA: {
        ANDROID: 'release_candidate_argentina',
        IOS: 'release_candidate_argentina'
    }
};

const getAppBanVersion = (appType, platform) => appBanVersion[appType][platform];

const getAppWarnVersion = (appType, platform) => appWarnVersion[appType][platform];

module.exports = {
    getAppBanVersion,
    getAppWarnVersion
};
