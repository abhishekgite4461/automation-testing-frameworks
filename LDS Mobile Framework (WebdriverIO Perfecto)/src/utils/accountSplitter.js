const UserType = require('../enums/userType.enum');

module.exports = (accTypes) => {
    if (!accTypes) {
        throw new Error('No user account type specified.');
    }
    const accountTypes = accTypes.trim();
    if (accountTypes.indexOf(' AND ') !== -1 && accountTypes.indexOf(' OR ') !== -1) {
        throw new Error(`Invalid combination of account type requested: ${accountTypes}`);
    }
    let conjunction = ' AND ';
    let types = accountTypes.split(conjunction);

    if (types.length === 1) {
        conjunction = ' OR ';
        types = accountTypes.split(conjunction);
    }
    const userTypes = types.map((type) => {
        const userType = UserType.fromString(type.trim().replace(/ /g, '_').toUpperCase());
        if (!userType) {
            throw new Error(`'${userType}' is not a valid account type. Please check userType.enum`);
        }
        return userType;
    });
    return userTypes.join(conjunction);
};
