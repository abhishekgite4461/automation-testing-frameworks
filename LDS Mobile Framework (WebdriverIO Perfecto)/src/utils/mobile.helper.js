class MobileHelper {
    static isWorkMobile(workMobileObject, mobile) {
        if (mobile.includes(workMobileObject)) {
            throw new Error('Not a valid work Mobile ');
        }
    }

    static isPersonalMobile(personalMobileObject, mobile) {
        if (mobile.includes(personalMobileObject)) {
            throw new Error('Not a valid personal Mobile ');
        }
    }

    static isHomeInterestRate(homeMobileObject, mobile) {
        if (mobile.includes(homeMobileObject)) {
            throw new Error('Not a valid Home Mobile!!');
        }
    }
}

module.exports = MobileHelper;
