const memoize = require('memoizee');
const request = require('sync-request');
const util = require('util');
const logger = require('./logger');

const baseUrl = 'https://lloydsbank-api.headspin.io';
const apiToken = process.env.HEADSPIN_AUTH_TOKEN;

const isHeadspin = memoize(() => browser.capabilities.provider[0] === 'headspin');
const isAndroid = memoize(() => browser.capabilities.platformName === 'Android');
const isIOS = memoize(() => browser.capabilities.platformName === 'iOS');
const deviceId = memoize(() => browser.capabilities.udid);

const makeRequestJsonBody = (requestMethod, path, body) => {
    logger.info(`Making headspin-api request ${requestMethod}\t${path}\n${util.inspect(body)}`);
    const options = {
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${apiToken}`
        }
    };
    if (body) {
        options.body = JSON.stringify(body);
    }
    const response = (() => request(requestMethod, `${baseUrl}/${path}`, options))();
    if (response.statusCode.toString()[0] !== '2') {
        throw new Error(`Request failed with statusCode: ${response.statusCode} : 
            ${util.inspect(response.body.toString())}`);
    }
    return JSON.parse(response.body.toString());
};

const makeRequestTextBody = (requestMethod, path, body) => {
    logger.info(`Making headspin-api request ${requestMethod}\t${path}\n${util.inspect(body)}`);
    const options = {
        headers: {
            'Content-Type': 'text/plain',
            Authorization: `Bearer ${apiToken}`
        }
    };
    if (body) {
        options.body = body;
    }
    const response = (() => request(requestMethod, `${baseUrl}/${path}`, options))();
    if (response.statusCode.toString()[0] !== '2') {
        throw new Error(`Request failed with statusCode: ${response.statusCode} : 
            ${util.inspect(response.body.toString())}`);
    }
    return JSON.parse(response.body.toString());
};

const getAutomationConfig = memoize(() => makeRequestJsonBody('GET', 'v0/devices/automation-config'));

const getAppiumUrl = (udid) => {
    const automationConfig = getAutomationConfig();
    const deviceNotFound = Symbol(`Device ${udid} is not found on Headspin.`);
    const getDeviceInfo = () => {
        for (const key in automationConfig) {
            if (automationConfig[key].capabilities.udid === udid) {
                return automationConfig[key].driver_url;
            }
        }
        return deviceNotFound;
    };
    const formatUrl = (deviceInfo) => {
        if (deviceInfo === deviceNotFound) {
            throw new Error(`Device ${udid} is not found in Headspin.`);
        }
        const startIndex = deviceInfo.indexOf('io:') + 3;
        return {
            host: deviceInfo.substring(8, startIndex - 1),
            port: Number(deviceInfo.substring(startIndex, startIndex + 4)),
            path: `/v0/${apiToken}/wd/hub`
        };
    };
    return formatUrl(getDeviceInfo());
};

const getWorkingDirectory = (udid = deviceId()) => {
    const automationConfig = getAutomationConfig();
    for (const key in automationConfig) {
        if (automationConfig[key].capabilities.udid === udid) {
            return automationConfig[key].working_dir;
        }
    }
    throw new Error(`The device ${udid} is not found, or an unexpected error in getWorkingDir.`);
};

const performAndroidShell = (command) => {
    if (!isHeadspin() || !isAndroid()) {
        throw Error('performAndroidShell is meant only for Android device on Headspin.');
    }
    const apiEndPoint = `v0/adb/${deviceId()}/shell`;
    return makeRequestTextBody('POST', apiEndPoint, command);
};

const closeIosPopUps = (udid = deviceId()) => {
    if (!isHeadspin() || !isIOS()) {
        throw new Error('closeIosPopUps is meant for IOS devices on Headspin only.');
    }
    return makeRequestJsonBody('POST', `v0/idevice/${udid}/poptap`);
};

const restartDevice = (udid = deviceId()) => {
    if (!isHeadspin()) {
        throw new Error('restartDevice is only meant for Headspin Devices');
    }
    const apiEndPoint = !isIOS() ? `v0/adb/${udid}/reboot` : `v0/idevice/${udid}/diagnostics/restart`;
    return makeRequestJsonBody('POST', apiEndPoint);
};

const getDeviceLogs = (udid = deviceId(), linesBack = 20) => {
    if (!isHeadspin()) {
        throw new Error('restartDevice is only meant for Headspin Devices');
    }
    const apiEndPoint = !isIOS() ? `v0/adb/${udid}/logcat[?n=${linesBack}]`
        : `v0/idevice/${udid}/syslog[?n=${linesBack}]`;
    return makeRequestJsonBody('GET', apiEndPoint);
};

const getConnectedDevices = memoize(() => makeRequestJsonBody('GET', 'v0/devices'));

const getDeviceIfo = (udid = deviceId()) => {
    const deviceNotFound = Symbol(`Device ${udid} is not found on Headspin.`);
    const connectedDevices = getConnectedDevices();
    const query = (key) => (!isIOS() ? connectedDevices.devices[key].serial === udid
        : connectedDevices.devices[key].device_id === udid);

    for (const iteration in connectedDevices.devices) {
        if (query(iteration)) {
            return connectedDevices.devices[iteration];
        }
    }
    return deviceNotFound;
};
module.exports = {
    getAppiumUrl,
    getWorkingDirectory,
    performAndroidShell,
    closeIosPopUps,
    restartDevice,
    getDeviceLogs,
    getDeviceIfo
};
