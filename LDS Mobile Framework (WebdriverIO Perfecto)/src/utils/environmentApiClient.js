const request = require('sync-request');
const util = require('util');
const sleep = require('system-sleep');
const localUtil = require('./utils');

const logger = require('./logger');

// You may need to change this to 10.59.151.81, depending on which network you want to use.
// 10.59.151.81 is the real IP address, and 159.34.181.86 is sandbox NAT'd IP.
// http://test-data-service.k8s.mobile.sbx.zone/v1 is a proxy that works on both networks.
const baseUrl = 'http://test-data-service.mobile.sbx.zone/v1';

const maxRetries = 2;
const RETRY_TIMEOUT = 20;

const backOffAndRetry = (func) => {
    const attempt = (retryCount) => {
        const socketTimeout = Symbol('socketTimeout');
        const remainingTries = retryCount - 1;
        let sleepTime = 0;
        let warnMessage = '';
        let attemptRetry = false;
        const response = (() => {
            try {
                const resp = func();
                logger.info(`Got response: ${resp.statusCode}`);
                return resp;
            } catch (err) {
                logger.error('nga-environment-api request failed', err);
                if (err.code === 'ETIMEDOUT') {
                    return socketTimeout;
                }
                throw err;
            }
        })();

        if (response === socketTimeout || response.statusCode === 503 || response.statusCode === 500) {
            warnMessage = 'nga-environment-api returned retryable error, waiting to retry';
            sleepTime = RETRY_TIMEOUT * 1000;
            attemptRetry = true;
        }
        logger.info(response);
        if (attemptRetry && remainingTries > 0) {
            logger.warn(warnMessage);
            sleep(sleepTime);
            return attempt(remainingTries);
        }
        return response;
    };
    return attempt(maxRetries);
};

const makeRequest = (verb, path, jsonRequestBody) => {
    logger.info(`Making nga-environment-api Request: ${verb} ${path}\n${util.inspect(jsonRequestBody)}`);

    const response = request(verb, `${baseUrl}/${path}`, {
        body: JSON.stringify(jsonRequestBody),
        headers: {'Content-Type': 'application/json'}
    });
    if (response.statusCode.toString()[0] !== '2') {
        throw new Error(`Request failed with ${response.statusCode}: ${util.inspect(response.body.toString())}`);
    }
    if (response.statusCode === 204) {
        return null;
    }
    const jsonResponse = JSON.parse(response.body.toString());
    logger.info(util.inspect(localUtil.maskFields(jsonResponse, 'password', 'memorableInformation')));
    return jsonResponse;
};

const post = (url, opts) => makeRequest('POST', url, opts);

const updateSwitchState = (environment, brand, name, state, global) => post('switch/state', {
    environment,
    brand,
    name,
    state: state ? 'on' : 'off',
    global
});

const updateSwitches = (environment, brand, appType, switches) => post('switch/states', {
    environment,
    brand,
    appType,
    switches
});

const deregisterDevice = (environment, brand, username, appType) => post('customer/device-deregister', {
    brand,
    username,
    environment: environment.name,
    appType
});

const deregisterAllDevices = (environment, brand, username, appType) => post('customer/device-deregister/all', {
    brand,
    username,
    environment: environment.name,
    appType
});

const verifyConnected = () => {
    logger.info('Verifying connection to nga-environment-api');
    const response = backOffAndRetry(() => request('GET', baseUrl));
    if (response.statusCode !== 200) {
        throw new Error('Unable to contact remote data repository');
    }
};

const modifyMobileNumber = (environment, appType, brand, username, mobileNumber) => post('customer/mobileNumber', {
    username,
    environment,
    appType,
    brand,
    mobileNumber
});

const phoneNumberConfig = (environment, appType, brand, numberOfDays) => post('customer/phoneNumberConfig', {
    environment,
    appType,
    brand,
    numberOfDays
});

const resetPassword = (environment, appType, brand, username, newPassword) => post('customer/resetPassword', {
    environment,
    appType,
    brand,
    username,
    newPassword
});

const fetchPaymentLimit = (environment, appType, brand, accountName) => post('account/paymentLimit', {
    environment,
    appType,
    brand,
    accountName
});

const updateEIA = (environment, brand, appType, username) => post('customer/updateCallState', {
    environment,
    brand,
    appType,
    username
});

const clearDeviceList = (environment, brand, appType, username, userIdList) => post('customer/clearDvcList', {
    environment,
    brand,
    appType,
    username,
    userIdList
});

const deregisterMandate = (environment, appType, brand, username) => post('customer/deregisterMandate', {
    environment,
    appType,
    brand,
    username
});

const setFuse = (environment, boxGrpNo) => post('customer/setFuse', {
    environment,
    boxGrpNo
});

const setAppSign = (environment, brand, appType, username) => post('customer/appSign', {
    environment,
    brand,
    appType,
    username
});

module.exports = {
    updateSwitchState,
    updateSwitches,
    deregisterDevice,
    verifyConnected,
    deregisterAllDevices,
    modifyMobileNumber,
    phoneNumberConfig,
    resetPassword,
    fetchPaymentLimit,
    updateEIA,
    clearDeviceList,
    deregisterMandate,
    setFuse,
    setAppSign
};
