class UserDetailsHelper {
    static generateRandomEmailAddress() {
        return `${Math.random().toString(36).substring(5)}@g.com`;
    }

    static generatePhoneExtensionNumber() {
        return `${Math.ceil(Math.random() * 1000000)}`;
    }
}

module.exports = UserDetailsHelper;
