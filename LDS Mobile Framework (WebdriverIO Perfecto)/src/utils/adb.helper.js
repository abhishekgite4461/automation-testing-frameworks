const DeviceHelper = require('./device.helper');
const headspinUtils = require('./headspin-utils');
const logger = require('./logger');

const cmd = () => (DeviceHelper.isPerfecto() ? 'mobile:handset:shell' : 'mobile:shell');
const params = (adbCommand, deviceId = undefined) => (deviceId === undefined || !DeviceHelper.isPerfecto()
    ? {command: adbCommand} : {command: adbCommand, handsetId: deviceId});

function executionHelper(adbCommand, deviceId = undefined) {
    let returnValue;
    if (DeviceHelper.isHeadspin()) {
        const output = headspinUtils.performAndroidShell(adbCommand);
        if (!output.status === 0) {
            const error = new Error(`Perform adb on HS returned statusCode: ${output.stdout}`);
            logger.error(error.message);
            throw error;
        }
        returnValue = output;
    } else {
        returnValue = browser.execute(cmd(), params(adbCommand, deviceId));
    }
    return returnValue;
}

// The returned object is a JSON object, parse accordingly to use it.
function getCallState(deviceId) {
    const adbCommand = 'dumpsys telephony.registry | grep mCallState';
    return executionHelper(adbCommand, deviceId).match(/.*(\d).*/)[1] === '1';
}

// The returned object is a JSON object, parse accordingly to use it.
function getOutgoingNumber(deviceId) {
    const adbCommand = 'dumpsys activity | grep -m 1 PHONE_NUMBER';
    return executionHelper(adbCommand, deviceId);
}

function makePhoneCall(phoneNumber, deviceId) {
    const adbCommand = `'am start -a android.intent.action.CALL -d tel:${phoneNumber}'`;
    return executionHelper(adbCommand, deviceId);
}

function hangPhoneCall(deviceId) {
    const adbCommand = 'input keyevent 6';
    return executionHelper(adbCommand, deviceId);
}

function inputText(str) {
    const adbCommand = `input text ${str}`;
    return executionHelper(adbCommand);
}

function inputKeyEvent(strKeyEvent) {
    const adbCommand = `input keyevent ${strKeyEvent}`;
    return executionHelper(adbCommand);
}

function deviceRotation(rotationId) {
    let adbCommand = 'settings put system accelerometer_rotation 0';
    executionHelper(adbCommand);
    adbCommand = `settings put system user_rotation ${rotationId}`;
    return executionHelper(adbCommand);
}

/**
 * This will return an array containing:
 * [apppackage, appActivity]
 *
 */
function getCurrentFocusApp() {
    const adbCommand = 'dumpsys window windows | grep -E mCurrentFocus';
    return executionHelper(adbCommand).match(/[a-zA-Z0-9_]*(\.[a-zA-Z0-9_]+)+[0-9a-zA-Z_]/g);
}

function openNotificationTray() {
    const adbCommand = 'cmd statusbar expand-notifications';
    executionHelper(adbCommand);
}

function getManufacturer() {
    const adbCommand = 'getprop ro.product.brand';
    return executionHelper(adbCommand);
}

function getAPNState() {
    const adbCommand = 'dumpsys telephony.registry | grep mServiceState';
    return executionHelper(adbCommand);
}

function getNotification(notificationSearch) {
    const adbCommand = `dumpsys notification --noredact | grep ${notificationSearch}`;
    return executionHelper(adbCommand);
}

function adbLogs() {
    const adbCommand = 'logcat -t 1000';
    return executionHelper(adbCommand);
}

module.exports = {
    getCallState,
    getOutgoingNumber,
    makePhoneCall,
    hangPhoneCall,
    inputText,
    inputKeyEvent,
    deviceRotation,
    getCurrentFocusApp,
    getManufacturer,
    openNotificationTray,
    getAPNState,
    getNotification,
    adbLogs
};
