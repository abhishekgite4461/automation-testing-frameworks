module.exports = class SwitchRequestFormatter {
    static format(onSwitches, offSwitches) {
        const switchMapper = (swtch, state) => ({
            alias: swtch.restApiName,
            name: swtch.ibcName,
            global: swtch.isGlobal,
            state
        });
        return onSwitches.map((swtch) => switchMapper(swtch, 'on'))
            .concat(offSwitches.map((swtch) => switchMapper(swtch, 'off')));
    }
};
