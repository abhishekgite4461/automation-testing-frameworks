const Future = require('fibers/future');

module.exports = function (promise) {
    return Future.fromPromise(promise).wait();
};
