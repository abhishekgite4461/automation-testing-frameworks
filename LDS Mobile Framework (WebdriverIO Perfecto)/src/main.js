const commandlineArgumentParser = require('./runner/commandlineArgumentParser');
const runner = require('./runner/runner');
const logger = require('./utils/logger');

const argv = commandlineArgumentParser.getArgv(process.argv);
argv.numberOfDaysSince = (() => {
    const fromSpecificDate = 1543623000000; // 1st Dec 2018
    const dayInMs = 24 * 60 * 60 * 1000;
    return Math.round((Date.now() - (new Date(fromSpecificDate))) / dayInMs);
})();

logger.info('Starting test runner', {argv});
runner.run(argv)
    .then((exitCodes) => {
        logger.info(`All wdio processes exited, exit codes were: ${exitCodes.join(', ')}`);
        process.exit(Math.max(...exitCodes.map((code) => parseInt(code, 10))));
    })
    .catch((err) => {
        logger.error(`Launcher failed to start the test --> ${err}`);
        process.exit(1);
    });
