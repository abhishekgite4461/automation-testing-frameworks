const {setDefinitionFunctionWrapper, setWorldConstructor, Before, After} = require('cucumber');

const Server = require('../../enums/server.enum');
const Logger = require('../../utils/logger');
const LogOffTask = require('../../tasks/logOff.task');
const Actions = require('../../pages/common/actions');
const DeviceState = require('../../utils/deviceState');
const DeviceHelper = require('../../utils/device.helper');

const AppConfigurationRepository = require('../../data/appConfiguration.repository');
const StubRepository = require('../../data/stub.repository');
const SwitchRepository = require('../../data/switch.repository');
const FeatureRepository = require('../../data/feature.repository');
const AccountRepository = require('../../data/accounts/account.repository');
const IBCClient = require('../../utils/environmentApiClient');
const SwitchRequestFormatter = require('../../utils/switchRequestFormatter');
const AbTestRepository = require('../../data/abTest.repository');
const DataConsentRepository = require('../../data/dataConsent.repository');
const RealTimeAlertsPage = require('../../pages/settings/realTimeAlerts.page');
const SavedAppDataRepository = require('../../data/savedAppData.repository.js');
const SavedData = require('../../enums/savedAppData.enum.js');

const tagsToOptimise = ['@team-statements', '@iOSObjectTreeOpt'];
const tagsForNftTimer = ['@performance'];
const isPerfectoiOSDevice = DeviceHelper.isPerfecto() && DeviceHelper.isIOS();

setWorldConstructor(function World({attach, parameters}) {
    this.attach = attach;
    this.parameters = parameters;
    this.appConfigurationRepository = new AppConfigurationRepository();
    this.stubRepository = new StubRepository();
    this.dataRepository = new AccountRepository();
    this.switchRepository = new SwitchRepository();
    this.featureRepository = new FeatureRepository();
    this.abTestRepository = new AbTestRepository();
    this.dataConsentRepository = new DataConsentRepository();
});

setDefinitionFunctionWrapper(function (fn, opts) {
    return fn.apply(this, opts);
});

Before(function (beforeScenarioData) {
    const scenario = beforeScenarioData.pickle;
    Logger.info(`Before ${scenario.name}`);
    this.appConfigurationRepository.set(scenario.tags);
    this.dataRepository.initialize(
        browser.config.cucumberOpts.dataRepository,
        browser.config.cucumberOpts.ensureMobileNumber,
        browser.config.cucumberOpts.byPassEia
    );
    // scenario.tags returns tags from both the Feature and the Scenario.
    const tags = scenario.tags.map((t) => t.name);
    const isStub = Server.fromString(browser.capabilities.server) === Server.STUB;
    this.switchRepository.set(tags, isStub, browser.capabilities.brand);
    this.featureRepository.set(tags, isStub);
    this.abTestRepository.set(tags, isStub);
    this.dataConsentRepository.set(tags, isStub);
    if (tags.some((t) => t.includes(tagsForNftTimer))) {
        SavedAppDataRepository.setData(SavedData.IS_NFT_TIMER_ENABLED, true);
    } else {
        SavedAppDataRepository.setData(SavedData.IS_NFT_TIMER_ENABLED, false);
    }
    if (isStub) {
        this.stubRepository.set(tags);
    }

    if (browser.config.cucumberOpts.toggleSwitches) {
        const switches = SwitchRequestFormatter.format(
            this.switchRepository.get().on,
            this.switchRepository.get().off
        );
        if (switches.length > 0) {
            IBCClient.updateSwitches(
                browser.config.cucumberOpts.server,
                browser.config.cucumberOpts.brand,
                browser.config.cucumberOpts.appType,
                switches
            );
        }
    }

    if (tags.some((t) => tagsToOptimise.includes(t)) && isPerfectoiOSDevice) {
        Actions.iOSObjectTreeOptimizationStart(30);
    }
});

After(function (result) {
    const tags = result.pickle.tags.map((x) => x.name);
    try {
        // skip log off action for unenrolled scenarios
        if ((Server.fromString(browser.capabilities.server) !== Server.STUB)
            && (!tags.some((t) => t === '@unEnrolled'))
            && (new DeviceState(DeviceHelper.deviceId()).getEnrolledUser())) {
            LogOffTask.logOffFromApp();
        }
        if (tags.some((t) => t === '@makeWifiOn')) {
            Actions.turnWifiOn();
        }
        if (tags.some((t) => t === '@resetDeviceLocation')) {
            Actions.resetDeviceLocation();
        }
        if (tags.some((t) => t === '@resetDeviceDate')) {
            new RealTimeAlertsPage().resetDeviceDate();
        }
        if (tags.some((t) => t === '@uninstallAppAfterRun')) {
            DeviceHelper.ensureCleanAppInstall(this.appConfigurationRepository.get(), false, true);
        }
        if (tags.some((t) => tagsToOptimise.includes(t)) && isPerfectoiOSDevice) {
            Actions.iOSObjectTreeOptimizationStop();
        }
    } catch (err) {
        Logger.info(err.stack);
        Logger.info(err.message);
        // Logger.info(`After hook failed for ${scenario.name} \n ${err.message}`);
    }
    this.dataRepository.unassign();
});
