/* eslint no-param-reassign: "off" */

const request = require('syncrequest');
const XML = require('pixl-xml');
const util = require('util');
const _ = require('lodash');
const sleep = require('system-sleep');

const {isDeviceConnected} = require('../scripts/deviceConnection');
const AppInfoFactory = require('../apps/appInfo.factory');
const config = require('./config');
const logger = require('../utils/logger');
const Platform = require('../enums/platform.enum');
const utils = require('../utils/utils');
const failureReason = require('./failurereason');
const {devicesNotForSanity} = require('./perfectoDeviceList');
const {PerfectoRestClient} = require('../utils/perfectoRestClient');
const runOnFailure = require('../utils/runOnFailure');

// Perfecto variables
utils.envVariableExists('PERFECTO_USER');
utils.envVariableExists('PERFECTO_PWD');

const perfectoHost = process.env.PERFECTO_HOST || 'lloyds.perfectomobile.com';
const perfectoUser = process.env.PERFECTO_USER;
const perfectoPwd = process.env.PERFECTO_PWD;
const perfectoToken = process.env.PERFECTO_TOKEN;

// Proxy setup
const proxyHost = process.env.PROXY_HOST;
const proxyPort = process.env.PROXY_PORT;
const proxyUser = process.env.PROXY_USER;
const proxyPwd = process.env.PROXY_PWD;
const randomDevice = Symbol('randomDevice');

const modifyPerfectoJobNumber = (numberOfDaysSince, jobNumber) => {
    const padFunction = (str, numberOfPaddings = 4, padStr = '0') => {
        while (str.length < numberOfPaddings) {
            str = padStr + str;
        }
        return str;
    };
    return jobNumber ? `${numberOfDaysSince}${padFunction(jobNumber.toString(), 4)}` : '0';
};

if (proxyHost && proxyPort) {
    let proxy = `${proxyHost}:${proxyPort}`;

    if (proxyUser && proxyPwd) {
        const encodedPassword = encodeURIComponent(proxyPwd)
            .replace(/!/g, '%21');
        proxy = `${proxyUser}:${encodedPassword}@${proxy}`;
    }

    process.env.http_proxy = `http://${proxy}`;
    process.env.https_proxy = `http://${proxy}`;
}

// Update config
config.hostname = perfectoHost;
config.port = 443;
config.protocol = 'https';
config.path = '/nexperience/perfectomobile/wd/hub';

// Some app installations take an incredible amount of time, especially when instrumented,
// so the default timeout of 90s is not enough.
config.connectionRetryTimeout = 6 * 60 * 1000;

// Perfecto reporting
let testRunning = false;
let failureReported = false;

// This is the label used on Perfecto's CI Dashboard.
let perfectoReportingJobName;
let perfectoReportingJobNumber;

// Tags injected via the command line, used for example by Jenkins to tag tests as being
// part of a certain nightly run.

const swallowErrors = (func, description) => {
    try {
        // func();
    } catch (err) {
        logger.error(description, err);
    }
};

async function releaseDevice(deviceId, count = 0) {
    const perfectoRestClient = new PerfectoRestClient();
    if (deviceId) {
        return perfectoRestClient.releaseDevice(deviceId)
            .then(() => logger.info(`released device ${deviceId}`))
            .catch(async (err) => {
                // perfecto device needs time to close hence adding 20 seconds timeout
                if (JSON.stringify(err.message)
                    .includes('status is CLOSING.')) {
                    await new Promise((resolve) => setTimeout(resolve, 20000));
                }
                if (count < 2 && !JSON.stringify(err.message)
                    .includes('device is not in use')) {
                    count += 1;
                    await releaseDevice(deviceId, count);
                    // after release wait for 5 seconds
                    await new Promise((resolve) => setTimeout(resolve, 5000));
                }
            });
    }
    return Promise.resolve();
}

const convertToStartCase = (conversionString) => _.startCase(_.toLower(conversionString));

const createPerfectoJobName = (args) => {
    let returnJobName = args.perfectoReportingJobName;
    if (args.preset === 'weeknightFunctionalStubsRnga' || args.preset === 'weeknightFunctionalStubsBnga') {
        returnJobName = `nga-aft-weeknightFunctionalStubs${args.platform === 'iOS' ? 'Ios' : 'Android'}${args.preset === 'weeknightFunctionalStubsBnga' ? 'Bnga' : ''}${convertToStartCase(args.brand)}`;
    }
    if (args.preset === 'weekendCompatibilityAndroid' || args.preset === 'weekendCompatibilityIos') {
        returnJobName = `nga-aft-weekendCompatibility${args.platform === 'iOS' ? 'Ios' : 'Android'}`;
    }
    return returnJobName;
};

const getHandsetInfo = (deviceId) => {
    const result = request.sync({
        url: `https://${perfectoHost}/services/handsets/${deviceId}`,
        qs: {
            operation: 'info',
            user: perfectoUser,
            password: perfectoPwd
        }
    });

    if (result.error || result.response.statusCode !== 200) {
        throw new Error(`Perfecto request failed: ${result}`);
    }

    return XML.parse(result.response.body);
};

/**
 * We don't pass capability.app here as this will cause the app to be installed by Perfecto
 * when the device is requested - so before the test starts.  This causes reporting issues
 * as we haven't yet called testStart (this happens in beforeScenario).  Instead, we
 * install the app in the first user step (Given I am a <blah> user).
 */
// Disable prefer-destructuring otherwise the linter will complain about destructing
// conf.cucumberOpts, which is impossible as perfectoReportingJobName etc are not defined inside the
// below method.
/* eslint prefer-destructuring: "off" */
const baseBeforeSession = config.beforeSession;
config.beforeSession = async function (conf, capability) {
    await releaseDevice(conf.cucumberOpts.deviceName);
    await isDeviceConnected(conf.cucumberOpts.deviceName);
    baseBeforeSession(conf, capability);

    perfectoReportingJobName = createPerfectoJobName(conf.cucumberOpts);
    perfectoReportingJobNumber = modifyPerfectoJobNumber(conf.cucumberOpts.numberOfDaysSince,
        conf.cucumberOpts.perfectoReportingJobNumber);
    // if XCUITest is not supported then default fallback automation strategy is Appium
    capability.automationName = 'XCUITest';
    capability.maxInstances = 1; // can only run one test at a time per device
    capability.deviceType = 'Mobile';
    capability.baseAppiumBehavior = 'YES';
    capability.simpleIsVisibleCheck = true;

    // Disabling video output should make the test runs a little bit quicker, but more
    // more importantly, we suspect that recording too much video is causing the perfecto
    // infrastructure to become overloaded, which in turn causes 503 and 504 errors in the
    // overnight runs.
    capability.outputVideo = conf.cucumberOpts.perfectoEnableVideo;
    capability.description = conf.cucumberOpts.perfectoDescription;
    capability.screenshotOnError = conf.cucumberOpts.perfectoScreenshotOnErrorOnly;
    capability.deviceVitals = conf.cucumberOpts.deviceVitals;
    capability.deviceVitalsIntervals = conf.cucumberOpts.deviceVitalsIntervals;

    // Adding below capabilities to islolate jenkins failures during driver creation
    capability['report.jobName'] = perfectoReportingJobName || 'Test Job';
    capability['report.jobNumber'] = perfectoReportingJobNumber || '0';
    capability['report.jobBranch'] = process.env.AFT_BRANCH || '0';

    // Adding below capabilities to add metadata in html report
    capability['cjson:metadata'] = {
        platform: {
            name: capability.platformName,
            version: getHandsetInfo(conf.cucumberOpts.deviceName).osVersion
        },
        app: {
            name: `${capability.appType}_${capability.brand}`,
            version: capability.appVersion
        }
    };

    if (perfectoToken) {
        capability.securityToken = perfectoToken;
    } else {
        capability.user = perfectoUser;
        capability.password = perfectoPwd;
    }

    if (conf.cucumberOpts.deviceName) {
        capability.deviceName = conf.cucumberOpts.deviceName;
    } else if (conf.cucumberOpts.preset === 'weekdaySanity') {
        const usableDevices = getRandomDeviceForSanity();
        if (usableDevices !== randomDevice) {
            capability.deviceName = usableDevices[capability.platformName].deviceId;
        }
    } else {
        capability.platformVersion = Platform.fromString(capability.platformName).osVersionRegex;
    }

    config.debugInfo(conf, capability);
    failureReported = false;

    // release device if it was blocked by previous execution
    if (capability.deviceName) {
        const options = {
            method: 'POST',
            url: `https://${perfectoHost}/services/handsets/${capability.deviceName}`,
            qs:
                {
                    operation: 'release',
                    user: perfectoUser,
                    password: perfectoPwd
                }
        };
        const result = request.sync(options);

        if ((result.error || result.response.statusCode !== 200)
            && !result.response.body.includes('device is not in use')) {
            throw new Error(`Perfecto request failed: ${result}`);
        }
    }
};

config.sessionFailure = (conf, capability, specs, retries, exception) => {
    const shouldRetry = retries < 3;
    logger.error(exception);
    logger.error(exception.stack);
    if (shouldRetry) {
        if ((exception.message.indexOf('device is in use') > -1)
            || exception.message.indexOf('Failed to obtain device with options') > -1) {
            logger.error('Session creation failed. Retrying after 2 minutes.');
            sleep(2 * 60 * 1000);
        }
    }
    return shouldRetry;
};

const baseBefore = config.before;
config.before = () => {
    baseBefore();

    // Some tests require knowledge of the OS version to decide which gestures to perform, so we
    // attach handset info to the session.
    browser.handsetInfo = getHandsetInfo(browser.capabilities.deviceName);

    logger.info(`Device ID: ${browser.handsetInfo.deviceId}`);
    logger.info(`Manufacturer: ${browser.handsetInfo.manufacturer}`);
    logger.info(`Model: ${browser.handsetInfo.model}`);
    logger.info(`Cradle ID: ${browser.handsetInfo.cradleId}`);

    // So that we know where to install the app in the DeviceHelper.
    browser.appURI = (() => {
        const {fileName} = AppInfoFactory.getAppInfo(browser.capabilities);
        if (browser.config.cucumberOpts.buildSubfolder) {
            return `PRIVATE:applications/${browser.config.cucumberOpts.buildSubfolder}/${fileName}`;
        }
        return `PRIVATE:applications/${fileName}`;
    })();
};

const baseBeforeScenario = config.beforeScenario;
config.beforeScenario = function (uri, feature, scenario) {
    baseBeforeScenario(uri, feature, scenario);
    if (browser.capabilities.deviceVitals) {
        const {name} = AppInfoFactory.getAppInfo(browser.capabilities);
        browser.execute('mobile:monitor:start', {
            sources: name,
            interval: browser.capabilities.deviceVitalsIntervals
        });
    }
    try {
        browser.execute('mobile:test:start', {
            name: scenario.name,
            jobName: perfectoReportingJobName,
            jobNumber: perfectoReportingJobNumber,
            jobBranch: process.env.AFT_BRANCH,
            tags: scenario.tags.map((t) => t.name.replace('@', ''))
                .concat([
                    browser.capabilities.brand.toLowerCase(),
                    browser.capabilities.server.toLowerCase(),
                    browser.capabilities.appVersion.toLowerCase()
                ]),
            customFields: []
        });
    } catch (err) {
        logger.info(err);
        logger.error(err);
    }

    browser.execute('mobile:step:start', {
        name: `Initialize Test for ${browser.capabilities.brand} on
        ${browser.capabilities.server} AFT Branch ${process.env.AFT_BRANCH}
        Release branch ${process.env.RELEASE_BRANCH}`
    });

    swallowErrors(
        () => browser.execute('mobile:logs:start', {}),
        'Unable to start collecting device logs'
    );
    testRunning = true;
};

config.beforeStep = (uri, feature, stepData) => {
    // testRunning = true; //remove this
    if (testRunning && stepData.step.text !== undefined) {
        browser.execute('mobile:step:start', {name: `${stepData.step.keyword}${stepData.step.text}`});
    }
};
config.afterStep = (uri, feature, scenario) => {
    // This slightly terrifying logic involving testRunning is here because afterStep is called
    // twice on test failure, but only the first call has the correct failure exception attached.
    // if (testRunning && stepResult.status !== 'passed') {
    if (testRunning && scenario.passed !== true) {
        swallowErrors(
            () => browser.screenshot(),
            'Unable to take screenshot on test failure'
        );

        // We want this to be as forgiving as possible. There should be no error style which will
        // produce an empty message.  In particular we need to be able to deal with chai style
        // errors and also native js errors.
        //
        // Perfecto are keen that the message property be on the first line as they use this to
        // categorise errors internally.
        // eslint-disable-next-line no-unused-vars
        const formatException = (e) => _.compact([
            e && e.message && e.message.trim() !== '' ? e.message.trim() : null,
            util.inspect(e)]).join('\n');

        testRunning = false;
        onScenarioComplete(
            false,
            formatException(scenario.error)
        );
    }
};

function getCategory(source) {
    let message = '';
    Object.keys(failureReason)
        .forEach((errorCategory) => {
            if (failureReason[errorCategory].some((element) => source.indexOf(element) >= 0)) {
                message = errorCategory.replace(/_/g, ' ');
            }
        });
    return message;
}

function categorizeError(status, errorMessage) {
    let message;
    if (status !== true) {
        try {
            message = getCategory(errorMessage);
            if (!message) {
                // message = getCategory(browser.getPageSource());
            }
        } catch (Err) {
            message = 'unclassified error';
        }
    }
    return message;
}

const baseAfterScenario = config.afterScenario;
config.afterScenario = async (uri, feature, scenario, result) => {
    if (browser.capabilities.deviceVitals) await browser.execute('mobile:monitor:stop', {});
    baseAfterScenario();
    if (testRunning) {
        const message = result.status !== 'passed' ? result.exception.stack : null;
        onScenarioComplete(result.status, message);
    }
    if (result.status !== 'passed') {
        let extraTags = `@${browser.config.cucumberOpts.platform.toString().toLowerCase()} `;
        extraTags += `@${browser.config.cucumberOpts.brand.toString().toLowerCase()} @reRun`;
        await runOnFailure(uri, scenario.name, extraTags);
    }
};

function onScenarioComplete(status, message) {
    swallowErrors(
        () => browser.execute('mobile:logs:stop', {}),
        'Unable to stop collecting device logs'
    );

    // execute when there is non null failure message or on first after step call
    if (message || !failureReported || status) {
        logger.info(`reporting message ${message} status ${status}`);
        // handle exception when perfecto fails with Unknown server error.
        for (let i = 0; i < 3; i++) {
            logger.info(`Attempt ${i} to update perfecto report`);
            try {
                browser.execute('mobile:test:end', {
                    success: status === 'passed',
                    failureDescription: message === undefined ? 'SCRIPT ERROR' : message,
                    failureReason: categorizeError(status, message)
                });
                break;
            } catch (err) {
                logger.error('Failed to update perfecto report, will retry after a minute');
                browser.pause(1000 * 60);
            }
        }
    } else {
        logger.info(`unreported message ${message} status ${status}`);
    }
    failureReported = true;
}

function getAvailableDevices() {
    const result = request.get.sync({
        url: `https://${perfectoHost}/services/handsets`,
        qs: {
            operation: 'list',
            inUse: false,
            status: 'connected',
            user: perfectoUser,
            password: perfectoPwd,
            description: 'Lloyds_FT',
            reservedTo: ''
        }
    });

    if (result.error || result.response.statusCode !== 200) {
        throw new Error(`Perfecto request failed: ${JSON.stringify(result)}`);
    }
    return XML.parse(result.response.body);
}

function getRandomDeviceForSanity() {
    const availableDevices = getAvailableDevices();
    try {
        const androidDevices = availableDevices.handset.filter((handset) => (handset.deviceId !== devicesNotForSanity[handset.os] && handset.os === 'Android' && handset.available === 'true' && handset.reserved === 'false'));
        const iOSDevices = availableDevices.handset.filter((handset) => (handset.deviceId !== devicesNotForSanity[handset.os] && handset.os === 'iOS' && handset.available === 'true' && handset.reserved === 'false'));
        return {
            Android: _.sample(androidDevices),
            iOS: _.sample(iOSDevices)
        };
    } catch (err) {
        logger.info(err.message);
        return randomDevice;
    }
}

exports.config = config;
