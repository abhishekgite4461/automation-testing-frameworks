/* eslint no-param-reassign: "off" */

const AppInfoFactory = require('../apps/appInfo.factory');
const Platform = require('../enums/platform.enum');
const config = require('./config');
const hsUtils = require('../utils/headspin-utils');
const localUtil = require('../utils/utils');

const baseBeforeSession = config.beforeSession;

config.beforeSession = (conf, capability) => {
    baseBeforeSession(conf, capability);
    localUtil.envVariableExists('HEADSPIN_AUTH_TOKEN');
    const deviceToSearch = conf.cucumberOpts.deviceName;
    const appInfo = AppInfoFactory.getAppInfo(capability);
    const directories = ['build-store'];
    if (config.cucumberOpts.buildSubfolder) {
        directories.push(config.cucumberOpts.buildSubfolder);
    }
    capability.deviceName = appInfo.name;
    capability.udid = deviceToSearch;
    const hsAppiumUrl = hsUtils.getAppiumUrl(deviceToSearch);
    conf.host = hsAppiumUrl.host;
    conf.port = hsAppiumUrl.port;
    conf.path = hsAppiumUrl.path;
    conf.protocol = 'https';
    capability.newCommandTimeout = 120;
    capability.runOnHS = true;
    capability.fullReset = false;
    capability.noReset = true;
    if (capability.platformName === Platform.Android.name) {
        capability.app = `/home/hs/appium_tests/latest/RNGA_${conf.cucumberOpts.brand}.3.apk`;
        capability.automationName = 'uiautomator2';
        capability.appPackage = appInfo.appPackage;
        capability.appActivity = appInfo.appActivity;
    } else if (capability.platformName === Platform.iOS.name) {
        hsUtils.closeIosPopUps(deviceToSearch);
        // capability.app = `/Users/lloydsbank/appium_tests/latest/RNGA_${conf.cucumberOpts.brand}.3.ipa`;
        capability.useNewWDA = false;
        capability.automationName = 'XCUITest';
        capability.bundleId = appInfo.appPackage;
    }
    config.debugInfo(conf, capability);
};

const baseBefore = config.before;
config.before = () => {
    baseBefore();
    // This information is required so that JSON reports can be generated successfully.
    // There is no easy API for retrieving device information for Appium, so use dummy
    // data.
    browser.handsetInfo = {
        deviceId: browser.capabilities.udid,
        os: browser.capabilities.platformName,
        osVersion: hsUtils.getDeviceIfo.os_version
    };
};

exports.config = config;
