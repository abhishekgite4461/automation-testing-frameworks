/* eslint no-param-reassign: "off" */

const {resolve} = require('path');
const AppInfoFactory = require('../apps/appInfo.factory');
const Platform = require('../enums/platform.enum');
const config = require('./config');

const baseBeforeSession = config.beforeSession;

config.beforeSession = (conf, capability) => {
    baseBeforeSession(conf, capability);
    const deviceToSearch = conf.cucumberOpts.deviceName;
    const appInfo = AppInfoFactory.getAppInfo(capability);
    const directories = ['build-store'];
    if (config.cucumberOpts.buildSubfolder) {
        directories.push(config.cucumberOpts.buildSubfolder);
    }
    capability.deviceName = appInfo.name;
    capability.udid = deviceToSearch;
    conf.host = '127.0.0.1';
    conf.port = 4723;
    conf.path = '/wd/hub';
    conf.services = ['appium'];
    capability.app = resolve(`${directories.join('/')}/${appInfo.fileName}`);
    capability.runOnHS = false;
    capability.fullReset = false;
    capability.noReset = true;
    if (capability.platformName === Platform.Android.name) {
        capability.udid = deviceToSearch;
        capability.automationName = 'uiautomator2';
        capability.appPackage = appInfo.appPackage;
        capability.appActivity = appInfo.appActivity;
        capability.fullReset = false;
        capability.noReset = true;
    } else if (capability.platformName === Platform.iOS.name) {
        capability.platformVersion = '11.3';
        capability.udid = deviceToSearch;
        capability.useNewWDA = false;
        capability.automationName = 'XCUITest';
        capability.bundleId = appInfo.appPackage;
    }
    config.debugInfo(conf, capability);
};

const baseBefore = config.before;
config.before = () => {
    baseBefore();
    // This information is required so that JSON reports can be generated successfully.
    // There is no easy API for retrieving device information for Appium, so use dummy
    // data.
    browser.handsetInfo = {
        deviceId: browser.capabilities.udid,
        os: browser.capabilities.platformName,
        osVersion: 'OS version unknown'
    };
};

exports.config = config;
