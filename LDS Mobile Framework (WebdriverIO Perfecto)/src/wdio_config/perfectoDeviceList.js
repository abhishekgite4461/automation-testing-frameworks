/* eslint-disable indent */
const _ = require('lodash');

const weeknightDeviceBreakdown = {
    // RNGA - Master SIT10-W
    weeknightFunctionalEnvironmentMaster: {
        Generic: {
            Android: {
                LDS: ['R58N33HPTEB'], // Samsung S10 +447384534999
                HFX: ['2AE1C2D8FC0B7ECE'], // Samsung S9 +447384535007
                BOS: ['R58N33HPTEB'], // Samsung S10 +447384534999
                MBNA: ['2AE1C2D8FC0B7ECE'] // Samsung S9 +447384535007

            },
            iOS: {
                LDS: ['00008020-000A74E90190003A'], // iPhone-XS Max +447384454633
                HFX: ['00008030-000D698222F9802E'], // iPhone-11 Pro Max +447384455516
                BOS: ['00008020-000A74E90190003A'], // iPhone-XS Max +447384454633
                MBNA: ['00008030-000D698222F9802E'] // iPhone-11 Pro Max +447384455516

            }
        },
        NonGeneric: {
            Android: {
                LDS: ['CE031713C239802A0D'], // Samsung S8 +441234567890
                HFX: ['2BD96BC4451C7ECE'], // Samsung S9 +442345678901
                BOS: ['CE031713C239802A0D'], // Samsung S8 +441234567890
                MBNA: ['2BD96BC4451C7ECE'] // Samsung S9 +442345678901

            },
            iOS: {
                LDS: ['64681E804EFBE513E7F0B2168973F54B0170B4BE'], // iPhone-7 Plus +445678901234
                HFX: ['00008020-000635363A68002E'], // iPhone-XS +446789012345
                BOS: ['64681E804EFBE513E7F0B2168973F54B0170B4BE'], // iPhone-7 Plus +445678901234
                MBNA: ['00008020-000635363A68002E'] // iPhone-XS  +446789012345

            }
        }
    },
    // RNGA - Master SIT02-B
    weeknightFunctionalMasterSIT02: {
        Android: {
            LDS: ['R58N33HPTEB'], // Samsung S10 +447384534999
            HFX: ['2AE1C2D8FC0B7ECE'], // Samsung S9 +447384535007
            BOS: ['CE011711F3F0A90F02'], // Samsung S8 +447384535005
            MBNA: ['FA69L0302226'] // Google Pixel +447415389804]
        },
        iOS: {
            LDS: ['00008020-000A74E90190003A'], // iPhone-XS Max +447384454633
            HFX: ['00008030-000D698222F9802E'], // iPhone-11 Pro Max +447384455516
            BOS: ['6DBB0E594F19F4BD7FADB211F1CCBF838CCDAD26'], // iPhone 6 +447931176880
            MBNA: ['44F7C6D3D96D10B03DA2317EF66ED4F89570EF30'] // iPhone 8 plus +447776527078
        }
    },
    // Bnga - Master SIT10-W
    weeknightFunctionalEnvironmentBnga: {
        Android: {
            LDS: [
                'CE011711F3F0A90F02' // Samsung S8 +447384535005
            ],
            BOS: [
                'FA69L0302226' // Google Pixel +447415389804
            ]
        },
        iOS: {
            LDS: [
                '6DBB0E594F19F4BD7FADB211F1CCBF838CCDAD26' // iPhone 6 +447931176880
            ],
            BOS: [
                '44F7C6D3D96D10B03DA2317EF66ED4F89570EF30' // iPhone 8 plus +447776527078
            ]
        }
    },
    // Release Job
    weeknightFunctionalEnvironment: {
        Android: {
            LDS: [
                'R38M60AHRDR', // Samsung Galaxy S10+ 10 +447384454584
                '2AF3B0BC221C7ECE' // Samsung Galaxy S9+ 8 +447384534999
            ],
            HFX: [
                'BPN7N19227001078' // Huawei P20 8.1.0 +447384535007
            ],
            BOS: [
                'CE12160C71BB3E1C01' // Samsung Galaxy S7 7 +447384535005
            ],
            MBNA: ['CE12160C4B71551701' // Samsung Galaxy S7 6.0.1 +447415389804
            ]
        },
        iOS: {
            LDS: [
                '00008030-00054C1411A3802E' // iPhone 11 pro 13.5.1 +447384454633
            ],
            HFX: [
                'C1F199813002793128A8FBD07B61FBC9AD5408A3' // iPhone-X 12.2 +447384455516
            ],
            BOS: [
                '85F28E9F650CF98AB2755C052FA4AFB49E2318E2' // iPhone-8 Plus 11.0.3 +447931176880
            ],
            MBNA: ['85F28E9F650CF98AB2755C052FA4AFB49E2318E2'] // iPhone 8Plus 11.0.3 +447384534985
        }
    },
    // Same set of device is used for stub and platinum
    weeknightFunctionalStubs: {
        BNGA: {
            Android: {
                LDS: ['2BD96BC4451C7ECE'], // Galaxy S9
                BOS: ['33003CDFA045B2FB'] // Galaxy Tab S2 9.7
            },
            iOS: {
                LDS: ['795DB256EF48CD7080CC0FB68F2B2000B25A0050'], // iPad Air 2
                BOS: ['5E4978D69E8B0933EF9139A47628F962AFEC2F44'] // iPad Pro
            }
        },
        RNGA: {
            Android: {
                LDS: ['CE031713C239802A0D'], // Google Pixel
                HFX: ['84B7N15A20002158'], //  Huawei Nexus 6p 6.0.1
                BOS: ['LCL7N18A22004263'], // Huawei P20 Pro
                MBNA: ['01E18D64CAC02C46'] // Nexus 5X
            },
            iOS: {
                LDS: ['F5319D894B77E0189824436E7037ACE77F7BB7D1'], //  Iphone 8 plus 11.0.3
                HFX: ['6DBB0E594F19F4BD7FADB211F1CCBF838CCDAD26'], // Iphone 6 11.2
                BOS: ['1E8C0432A5FBE3A3B29A07A4C0231045AF6DF8F5'], // iPhone 6S
                MBNA: ['EE18CA978B58BC453A33E8EAAB2F6A1CA56E2754'] // Iphone 6-PLus 11.1
            }
        }
    },
    weekendCompatibilityStubs: {
        Android: [
            'CE011711F3F0A90F02', // Galaxy S8+
            'CE031713C239802A0D', // Galaxy S8
            'CE12160C4B71551701', // Galaxy S7
            //  'CE11160BDC5BA52002', // Galaxy S7 Edge
            '11160B2A5A440102', // Galaxy S6
            'FA69L0302226', // Google Pixel
            '091609E889DB2F05', // Galaxy S6,temporary replacement for the below sony device untill 03084644 is resolved
            '2AF3B0BC221C7ECE',
            '01E18D64CAC02C46'
            // 'CB512CK6EK', // Sony
        ],
        iOS: [
            '6B0AA02848EAD9506269B431880B19D93FC9E897', // iPhone 7
            'C1F199813002793128A8FBD07B61FBC9AD5408A3', // iPhone-X 11.1.1
            '5CE9A0B5663B3A9067961D491F3E7DF0713762A9', // Iphone 6S 11.4
            '85F28E9F650CF98AB2755C052FA4AFB49E2318E2', // iPhone 8Plus 11.0.3
            '2B55E2CCE467CF0AC891C4352E554232C29603A6', // iPhone 7
            'F5319D894B77E0189824436E7037ACE77F7BB7D1', // iPhone 8
            '6DBB0E594F19F4BD7FADB211F1CCBF838CCDAD26', // iPhone 6
            // '0716B740194954DC47C1FB8DDF79C038749A26FB', // iPhone 5S
            'EE18CA978B58BC453A33E8EAAB2F6A1CA56E2754', // iPhone 6s plus
            '1E8C0432A5FBE3A3B29A07A4C0231045AF6DF8F5' // iPhone 6S
        ]
    },
    weeknightFunctionalEnvironmentPlatinumMaster: {
        Android: {
            HFX: [
                '2AE1C2D8FC0B7ECE' // Galaxy 8+
            ]
        },
        iOS: {
            HFX: [
                '6B0AA02848EAD9506269B431880B19D93FC9E897' // iPhone 7
            ]
        }
    },
    localExecution: {
        Android: {
            LDS: [
                'FA69L0301370'
            ],
            BOS: [
                'ce04171461f120f80d'
            ],
            HFX: [
                '84B7N15A20002158'
            ]
        }
    },
    // Bnga Iguana Android
    weeknightFunctionalEnvironmentBNGARelease: {
        Android: {
            LDS: [
                '84B7N15A20002158' //  Huawei Nexus 6p 6.0.1
            ],
            BOS: [
                'LCL7N18A22004263' // Huawei
            ]
        }
    }
};

const uatDeviceList = {
    weeknightUATRegression: {
        BNGA: {
            Android: {
                LDS: ['06157DF6CC325808'],
                BOS: ['021602280C215A8B']
            },
            iOS: {
                LDS: ['F496A2D5CF9F6B44FEE4E140C9C57197E1FB1639'],
                BOS: ['6B0AA02848EAD9506269B431880B19D93FC9E897']
            }
        },
        RNGA: {
            Android: {
                LDS: ['06157DF6CC325808'],
                BOS: ['021602280C215A8B'],
                HFX: ['CE12171CA911171D01'],
                MBNA: ['06157DF6CC325808']
            },
            iOS: {
                LDS: ['F496A2D5CF9F6B44FEE4E140C9C57197E1FB1639'],
                BOS: ['6B0AA02848EAD9506269B431880B19D93FC9E897'],
                HFX: ['DBF158DDA395B830D3E4FF774446B3B2A7C75B4B'],
                MBNA: ['F496A2D5CF9F6B44FEE4E140C9C57197E1FB1639']
            }
        }
    }
};

const devicesNotForSanity = {
    Android: [
        '1A74CD98',
        '5203F3E1EA8993DF',
        '3204A32171738151',
        '6689EC331DF2D4B6',
        '3204A32171738151',
        '0712531C',
        'FA69L0302226'
    ],
    iOS: [
    ]
};

// Combine all Android and iOS devices from above list
const flattenDeviceList = function (deviceObject) {
    return _(_.values(deviceObject))
        .map((x) => {
            let [returnAndroid, returnIos] = [{}, {}];
            if (!(x.Android && x.iOS)) {
                [returnAndroid, returnIos] = _(x)
                    .map((appType) => [appType.Android, appType.iOS])
                    .unzip()
                    .map((nestedDevices) => _(nestedDevices)
                        .flatMap((devices) => (typeof devices === 'object' ? _.values(devices) : devices))
                        .flatten()
                        .uniq()
                        .valueOf())
                    .valueOf();
            } else {
                [returnAndroid, returnIos] = [x.Android, x.iOS];
            }
            return [returnAndroid, returnIos];
        })
        .unzip()
        .map((nestedDevices) => _(nestedDevices)
            .flatMap((devices) => (typeof devices === 'object' ? _.values(devices) : devices))
            .flatten()
            .uniq()
            .compact()
            .valueOf())
        .valueOf();
};
const [Android, iOS] = flattenDeviceList(weeknightDeviceBreakdown);

const asDeviceList = (subList) => _(_.values(subList))
    .map((devices) => _.values(devices))
    .flattenDeep()
    .valueOf();

module.exports = _.merge({}, weeknightDeviceBreakdown, {
    weekend: {Android, iOS},
    // ALL must contain all devices which are used for automation, as it is used for recovery and
    // reservation of devices.
    ALL: {Android, iOS},
    devicesNotForSanity
}, {asDeviceList}, uatDeviceList, {flattenDeviceList}, {weeknightDeviceBreakdown});
