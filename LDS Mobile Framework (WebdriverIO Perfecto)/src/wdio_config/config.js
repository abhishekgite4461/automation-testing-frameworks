/* eslint no-param-reassign: "off" */

/**
 * This shared configuration is read by each webdriver io process.  There is a initial main thread
 * and one for each capability.  Therefore it is important not to do anything in this config which
 * is either:
 * * Required to be run only once, or
 * * Requires state to be shared across multiple threads.
 *
 * Setup and teardown tasks should be placed in onPrepare and onComplete respectively.
 *
 * NB: Exceptions thrown in beforeScenario, afterScenario, etc will be logged but will _not_ fail
 * the tests, so code which may fail should not be placed in here, but rather in the step
 * definitions themselves.
 */
const uuid = require('uuid');
const util = require('util');
const report = require('multiple-cucumber-html-reporter');

const runConfig = require('../enums/runConfig.enum');

const logger = require('../utils/logger');
const localUtils = require('../utils/utils');

// The cucumber reporters will only log to the console, so we have to override it so that
// everything will go through the winston logger.
/* eslint no-console: "off" */
console.debug = (...params) => logger.debug(util.format.apply(null, params));
console.info = (...params) => logger.info(util.format.apply(null, params));
console.log = (...params) => logger.info(util.format.apply(null, params));
console.warn = (...params) => logger.warn(util.format.apply(null, params));
console.error = (...params) => logger.error(util.format.apply(null, params));

const config = {
    sync: true,
    framework: 'cucumber',
    reporters: [
        ['cucumberjs-json', {
            jsonFolder: 'reports/cucumber/json',
            language: 'en'
        }
        ],
        ['allure', {
            outputDir: 'reports/allure',
            disableWebdriverStepsReporting: true,
            disableWebdriverScreenshotsReporting: true,
            useCucumberStepReporter: true
        }]
    ],
    logLevel: 'debug',
    outputDir: 'logs',
    coloredLogs: true,
    cucumberOpts: {
        timeout: 60000,
        failAmbiguousDefinitions: true,
        ignoreUndefinedDefinitions: false
    }, // will be overridden by custom test runner
    capabilities: [{}]
};

config.onPrepare = (conf) => {
    logger.info('onPrepare');
    conf.specs = conf.cucumberOpts.specs;
    conf.maxInstances = conf.cucumberOpts.maxInstances;
};

config.beforeSession = (conf, capability) => {
    const testId = uuid.v4();
    logger.info(`beforeSession testId: ${testId}`);

    capability.autoLaunch = true;
    capability.provider = conf.cucumberOpts.provider;
    capability.testId = testId;
    capability.platformName = conf.cucumberOpts.platform;
    capability.isUAT = conf.cucumberOpts.isUAT;
    capability.appType = conf.cucumberOpts.appType;
    capability.brand = conf.cucumberOpts.brand;
    capability.server = conf.cucumberOpts.server;
    capability.appVersion = conf.cucumberOpts.appVersion;
};

config.before = () => {
    const chai = require('chai');
    global.expect = chai.expect;
    global.assert = chai.assert;
    global.should = chai.should();
    logger.info('before');
};

config.beforeScenario = function (uri, feature, scenario) {
    logger.info(`beforeScenario ${scenario.name}`);
    browser.setTimeout({implicit: runConfig.TIMEOUTS.IMPLICIT()});
};

config.afterScenario = function () {
    logger.info('afterScenario');
};

config.afterSession = () => logger.info('afterSession');

config.debugInfo = (conf, capability) => {
    logger.info(`Prepared wdio configuration: 
  specs: ${conf.specs} 
  capabilities: ${util.inspect(localUtils.maskFields(capability, 'password', 'securityToken'))} 
    `);
};

config.onComplete = () => {
    report.generate({
        openReportInBrowser: true,
        disableLog: true,
        reportName: 'Mobile AFT',
        jsonDir: 'reports/cucumber/json',
        reportPath: 'reports/cucumber/html'
    });
};

module.exports = config;
