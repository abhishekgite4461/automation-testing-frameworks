const denodeify = require('denodeify');
const fs = require('fs');
const Gherkin = require('@cucumber/gherkin');
const {IdGenerator} = require('@cucumber/messages');

const readFile = denodeify(fs.readFile);
const parser = new Gherkin.Parser(new Gherkin.AstBuilder(IdGenerator.incrementing()));

function extractTags(raw) {
    const gherkinDocument = parser.parse(raw.toString());
    if (gherkinDocument.feature.keyword !== 'Feature') {
        throw new Error('Expected Gherkin document');
    }
    // Once the gherkin document is compiled, tags on the feature are combined
    // with those on the scenarios.
    const pickles = new Gherkin.compile(gherkinDocument, '', IdGenerator.incrementing());

    return {
        switchTags: gherkinDocument.feature.tags
            .map((tag) => tag.name)
            .filter((tag) => (tag.startsWith('@sw'))),
        tags: pickles.map((pickle) => pickle.tags.map((tag) => tag.name))
    };
}

function parseFeatureFile(featureFile) {
    return readFile(featureFile).then((raw) => {
        try {
            return {...extractTags(raw), name: featureFile};
        } catch (err) {
            throw new Error(`Failed to parse ${featureFile}, error was: ${err}\n${err.stack}`);
        }
    });
}

module.exports = (featureFiles) => Promise.all(featureFiles.map((featureFile) => parseFeatureFile(featureFile)));
