/*
 * This has been added to allow us to run tests in parallel, grouped by which switch states they
 * require. Between groups, if --toggleSwitches is set, we make requests to the IBC API to set
 * the correct switch state for the next batch of feature files. By default, webdriver.io does
 * not allow the level of parallelisation that we require to do this so we launch many instances
 * of wdio from here, each running the tests for one configuration (brand, platform, device,
 * server, etc.).
 */

const cucumberHtmlReporter = require('multiple-cucumber-html-reporter');
const denodeify = require('denodeify');
const fs = require('fs');
const Launcher = require('@wdio/cli').default;
const path = require('path');
const readdir = denodeify(require('recursive-readdir'));
const util = require('util');
const request = require('superagent');
const admZip = require('adm-zip');

const allure = require('allure-commandline');
const logger = require('../utils/logger');
const {generateCucumberOpts} = require('./cucumberOptsGenerator');
const testRunnerReporter = require('./testrunnerReporter');
const utils = require('../utils/utils');
const commandlineArgumentParser = require('./commandlineArgumentParser');
const {DeviceCapabilities} = require('./deviceCapabilities');

const {resetTmpDir} = utils;

const unlink = denodeify(fs.unlink);

const cucumberJsonReportPath = './reports/cucumber/json';

// On large test runs we have to await on a lot of child processes.  This causes node to complain
// about too many SIGINT listeners.
require('events').EventEmitter.defaultMaxListeners = 128;

const generateAllureReport = (shouldGenerate, exitCodes) => new Promise((resolve) => {
    if (shouldGenerate) {
        const generateReport = allure(['generate', 'reports/allure', '--clean', '-o', 'reports/allure-report']);
        generateReport.on('exit', (allureExitCode) => {
            logger.info(`Generated Allure reports at "reports/allure-report" and exited with ${allureExitCode}`);
            resolve(exitCodes);
        });
    } else {
        resolve(exitCodes);
    }
});

function run(argv) {
    return initialiseReporting()
        .then(() => getFeatureFiles(argv))
        .then((files) => generateCucumberOpts(argv, files))
        .then((groupedOpts) => runWdio(argv, groupedOpts))
        .then((exitCodes) => generateAllureReport(argv.allureReports, exitCodes))
        .then((exitCodes) => aggregateResults(argv, exitCodes))
        .then((exitCodes) => {
            resetTmpDir();
            return exitCodes;
        });
}

function getFeatureFiles(argv) {
    if (argv.tags === '@reRun') {
        const artifactsPresetsToFetch = ['nga-3-aft-weeknightMasterIosNonGeneric', 'nga-3-aft-weeknightMasterAndroidNonGeneric',
            'nga-3-aft-weeknightMasterAndroidGeneric', 'nga-3-aft-weeknightMasterIosGeneric'];
        return downloadArtifacts(artifactsPresetsToFetch)
            .then(() => unZipArtifacts(artifactsPresetsToFetch))
            .then(() => readdir(path.resolve('./src/features/reRun'), ['!*.feature']));
    }
    return readdir(path.resolve(`./src/features/${commandlineArgumentParser.getAppType()}`), ['!*.feature']);
}

function downloadArtifacts(artifactsPresetsToFetch) {
    return Promise.all(artifactsPresetsToFetch.map((preset) => new Promise((resolve, reject) => {
        const url = `https://jenkins.mobile.sbx.zone/job/${preset}/lastBuild/artifact/reRun.zip`;
        const zipFile = `${preset}.zip`;
        return request
            .get(url)
            .auth(process.env.JENKINS_USER, process.env.JENKINS_PWD)
            .on('error', (error) => {
                logger.error(error);
                reject(error);
            })
            .pipe(fs.createWriteStream(zipFile))
            .on('finish', () => {
                logger.info(`Artifacts Download Completed for ${preset} Preset`);
                resolve();
            });
    })));
}

function unZipArtifacts(artifactsPresetsToFetch) {
    return Promise.all(artifactsPresetsToFetch)
        .then((presets) => {
            presets.forEach((preset) => {
                const zip = new admZip(path.resolve(`./${preset}.zip`));
                zip.extractAllTo(path.resolve('./src/features/reRun'), false);
            });
            return presets;
        })
        .then((presets) => {
            presets.forEach((preset) => {
                unlink(`./${preset}.zip`);
            });
        });
}

function initialiseReporting() {
    return Promise.all([
        readdir(path.resolve(cucumberJsonReportPath), ['!*.json']),
        readdir(path.resolve(testRunnerReporter.reportDir), ['!*.json'])
    ])
        .then(([cucumberFiles, testrunnerFiles]) => cucumberFiles.concat(testrunnerFiles))
        .then((files) => Promise.all(files.map((file) => unlink(file))));
}

function runWdio(argv, groupedOpts) {
    if (groupedOpts.length === 0) {
        if (argv.runModifiedNga3Features) {
            logger.error('There are no tests to run which match the criteria.  Usually this would '
                + 'fail the test run, but this is expected sometimes with --runModifiedNga3Features');
        } else {
            throw new Error('Failing because there are no tests to run which match the criteria, perhaps you '
                + 'specified an invalid tag combination?');
        }
    }

    const configFile = `./src/wdio_config/${argv.provider}.config.js`;

    const launch = (opt) => {
        const deviceCapabilities = new DeviceCapabilities();
        logger.info(`Launching ${argv.provider} WDIO process with options: ${util.inspect(opt)}`);

        if (argv.dryRun) {
            logger.warn('Skipping wdio process launch as this is a dry run.');
            return Promise.resolve(0);
        }
        return new Promise((resolve, reject) => {
            deviceCapabilities.tagExpressionForDevice(opt.cucumberOpts.deviceName, argv.provider.includes('perfecto'))
                .then((deviceCapabilitiesTagExpression) => {
                    opt.cucumberOpts.tagExpression = `(${opt.cucumberOpts.tagExpression}) 
                    and (${deviceCapabilitiesTagExpression})`;
                    resolve(new Launcher(configFile, opt).run());
                })
                .catch((err) => reject(err));
        });
    };

    // Before we start execution, we make sure that the tmp directory is empty.
    resetTmpDir();
    let basePromise = Promise.resolve([]);
    for (const group of groupedOpts) {
        basePromise = basePromise
            .then((exitCodes) => {
                if (exitCodes.some((code) => code !== 0) && argv.failFast) {
                    return exitCodes;
                }
                return Promise.all(group.opts.map((opt) => launch(opt)))
                    .then((newExitCodes) => exitCodes.concat(newExitCodes));
            });
    }
    return basePromise;
}

function aggregateResults(argv, outputCodes) {
    if (argv.htmlReport) {
        try {
            // Generate aggregated cucumber report from existing cucumber json reports.
            cucumberHtmlReporter.generate({
                jsonDir: 'reports/cucumber/json',
                reportPath: 'reports/cucumber/html',
                saveCollectedJSON: true
            });
        } catch (err) {
            // This aggregator will throw if there are no JSON files to aggregate, which happens
            // when all the runners fail to start.
            logger.error('Cucumber report aggregation failed.', err);
        }
    }
    return testRunnerReporter.aggregate().then(() => outputCodes);
}

module.exports = {
    run,
    getFeatureFiles
};
