/**
 * Small custom reporter, as none of the existing reporters report the runner completely failing to
 * start, for example if the perfecto device is unavailable.
 *
 * This is a wdio reporter, so is called from the main process only.
 */
const denodeify = require('denodeify');
const events = require('events');
const fs = require('fs');
const path = require('path');
const readdir = denodeify(require('recursive-readdir'));
const uuid = require('uuid');

const logger = require('../utils/logger');

const readFile = denodeify(fs.readFile);
const writeFile = denodeify(fs.writeFile);
const reportDir = 'reports/test_runner_reports';

class Reporter extends events.EventEmitter {
    static log(message) {
        const logfile = `${reportDir}/test_runner_report_segment_${uuid.v4()}.json`;
        logger.info(`Writing test runner report to ${logfile}`);
        fs.writeFileSync(logfile, JSON.stringify(message));
    }

    static get reporterName() {
        return 'TestRunnerReporter';
    }

    constructor() {
        super();

        this.errors = [];
        this.totalRunners = 0;

        this.on('error', (runner) => {
            this.errors.push(runner.error.message);
            logger.error('Test runner failed to start, '
                         + `testId: ${runner.capabilities.testId}, message: ${runner.error.message}`);
        });

        this.on('runner:start', () => {
            this.totalRunners += 1;
        });

        this.on('end', () => {
            Reporter.log({
                totalRunners: this.totalRunners,
                errors: this.errors
            });
        });
    }
}

const aggregate = () => {
    try {
        const reportsP = readdir(path.resolve(reportDir), ['!test_runner_report_segment_*.json'])
            .then((files) => Promise.all(files.map((filename) => readFile(filename)
                .then(JSON.parse))));

        return reportsP.then((reports) => {
            const aggregatedReport = {
                totalRunners: 0,
                errors: []
            };
            reports.forEach((report) => {
                aggregatedReport.totalRunners += report.totalRunners;
                aggregatedReport.errors = aggregatedReport.errors.concat(report.errors);
            });
            const logfile = `${reportDir}/test_runner_report.json`;
            logger.info(`Writing aggregated report to ${logfile}`);
            return writeFile(`${logfile}`, JSON.stringify(aggregatedReport));
        });
    } catch (err) {
        logger.error(`Report Aggregator failed, thus pasing empty Report. \n${err.message}`);
        return {
            totalRunners: 0,
            errors: ['The report for this build was not generated. Exception in generating report.']
        };
    }
};

module.exports = {Reporter, aggregate, reportDir};
