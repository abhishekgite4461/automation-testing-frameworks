const deviceCapabilitiesList = require('../data/deviceCapabilities.json');
const {PerfectoRestClient} = require('../utils/perfectoRestClient');
const DeviceHelper = require('../utils/device.helper');

const getAllDeviceInfo = (device) => {
    const perfectoClient = new PerfectoRestClient();
    if (!device) return Promise.resolve('');
    return perfectoClient.getAllDeviceInfo(device);
};

const getFingerprintTags = (deviceCapabilities) => {
    const tagFingerprintEnrolled = '@fingerprintScannerAndFingerprint';
    const tagFingerprintNotEnrolled = '@fingerprintScannerNoFingerprint';
    const tagFingerprintUnavailable = '@fingerprintNoScanner';
    let fingerprintTagExpression;
    if (deviceCapabilities && deviceCapabilities.fingerprint) {
        if (!deviceCapabilities.fingerprint.enabled) {
            // No fingerprint
            fingerprintTagExpression = `not ${tagFingerprintEnrolled} and not ${tagFingerprintNotEnrolled}`;
        } else if (deviceCapabilities.fingerprint.enrolled) {
            // Fingerprint enrolled
            fingerprintTagExpression = `not ${tagFingerprintNotEnrolled} and not ${tagFingerprintUnavailable}`;
        } else {
            // Fingerprint not enrolled
            fingerprintTagExpression = `not ${tagFingerprintEnrolled} and not ${tagFingerprintUnavailable}`;
        }
    } else {
        // Fingerprint unknown. Not executing fingerprint scenarios.
        fingerprintTagExpression = `not ${tagFingerprintEnrolled} and not ${tagFingerprintNotEnrolled} and \
            not ${tagFingerprintUnavailable}`;
    }
    return fingerprintTagExpression;
};

const getTabletTags = (deviceCapabilities) => {
    const tabletTag = '@tabletOnly';
    const mobileTag = '@mobileOnly';
    let tabletTagExpression;
    if (deviceCapabilities && deviceCapabilities.isTablet) {
        tabletTagExpression = `not ${mobileTag}`;
    } else {
        tabletTagExpression = `not ${tabletTag}`;
    }
    return tabletTagExpression;
};

class DeviceCapabilities {
    constructor(deviceList = deviceCapabilitiesList) {
        this.deviceList = deviceList;
    }

    tagExpressionForDevice(device, isPerfecto = DeviceHelper.isPerfecto()) {
        const deviceCapabilities = this.deviceList[device];
        return new Promise((resolve, reject) => {
            if (isPerfecto) {
                getAllDeviceInfo(device)
                    .then((deviceInfo) => {
                        const tag = ((deviceInfo.os === 'Android' && parseFloat(deviceInfo.osVersion) < 7.1) || deviceInfo.os === 'iOS') ? ' and not @appShortcut' : '';
                        resolve(
                            `${getFingerprintTags(deviceCapabilities)} and ${getTabletTags(deviceCapabilities)} ${tag}`
                        );
                    })
                    .catch((err) => {
                        reject(err);
                    });
            } else {
                resolve(`${getFingerprintTags(deviceCapabilities)} and ${getTabletTags(deviceCapabilities)}`);
            }
        });
    }
}

module.exports = {
    DeviceCapabilities,
    getAllDeviceInfo
};
