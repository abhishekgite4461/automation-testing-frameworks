const glob = require('glob');
const path = require('path');
const uuid = require('uuid');
const {execSync} = require('child_process');
const os = require('os');
const TagExpressionParser = require('@cucumber/tag-expressions');

const {randomDevice} = require('./commandlineArgumentParser');
const Platform = require('../enums/platform.enum');
const SwitchRepository = require('../data/switch.repository');
const utils = require('../utils/utils');
const parseFeatureFiles = require('./featureFileParser');
const logger = require('../utils/logger');

const {cartesianProduct} = utils;

const parseCucumberTags = TagExpressionParser;

function generateCucumberOpts(argv, files) {
    return parseFeatureFiles(files)
        .then((parsedFiles) => groupFeatureFilesBySwitches(argv, parsedFiles))
        .then((groupedFiles) => generateOpts(argv, groupedFiles));
}

function groupFeatureFilesBySwitches(argv, files) {
    if (!argv.toggleSwitches) {
        // We don't need to group the files as we aren't toggling the switches
        return [{switches: null, files}];
    }

    const groupedFiles = {};
    for (const file of files) {
        const switches = SwitchRepository.parseSwitches(file.switchTags);

        // We need sorting, so identical arrays will stringify to the same string.
        switches.on.sort();
        switches.off.sort();
        const switchString = JSON.stringify(switches);

        if (!groupedFiles[switchString]) {
            groupedFiles[switchString] = [];
        }
        delete file.switchTags;
        groupedFiles[switchString].push(file);
    }
    return Object.keys(groupedFiles).map((key) => ({switches: JSON.parse(key), files: groupedFiles[key]}));
}

function generateOpts(argv, groupedFiles) {
    return groupCucumberOptions(argv, createBaseCucumberOptions(argv), groupedFiles);
}

function createBaseCucumberOptions(argv) {
    const opts = [];

    const allJsFiles = (dirName) => {
        const rootDir = path.join(__dirname, '..', dirName);
        return glob.sync(path.join(rootDir, '**/*.js'));
    };
    const tags = [
        'not @pending',
        'not @manual',
        argv.appium ? 'not @perfecto' : 'not @appium'
    ].join(' and ');
    const getDefaultTags = (initialTags) => {
        let defaultTags = `(${initialTags.join(' and ')}) and (${tags})`;
        if (argv.t && argv.t !== '@smokeNga3') {
            defaultTags = defaultTags.concat(` and (${argv.t})`);
        }
        return defaultTags;
    };

    const generateDefaultOpt = (tagsToRun) => ({
        cucumberOpts: {
            // TODO MOB3-4772 Reduce timeout once using accessibility identifiers
            timeout: 10 * 60 * 1000,
            require: allJsFiles('step_definitions').concat(allJsFiles('wdio_config/hooks')),
            ignoreUndefinedDefinitions: false,
            tagExpression: getDefaultTags(tagsToRun),
            specs: [],
            buildSubfolder: argv.buildSubfolder,
            useInstalledApp: argv.useInstalledApp,
            perfectoReportingJobName: argv.perfectoReportingJobName,
            perfectoReportingJobNumber: argv.perfectoReportingJobNumber,
            numberOfDaysSince: argv.numberOfDaysSince,
            perfectoEnableVideo: argv.perfectoEnableVideo,
            perfectoExtraTags: argv.perfectoExtraTags,
            perfectoScreenshotOnErrorOnly: argv.perfectoScreenshotOnErrorOnly,
            perfectoDescription: argv.perfectoDescription,
            dataRepository: argv.dataRepository,
            toggleSwitches: argv.toggleSwitches,
            maxInstances: argv.maxInstances,
            failFast: argv.failFast,
            provider: argv.provider,
            allureReports: argv.allureReports,
            preset: argv.preset,
            deviceVitals: argv.deviceVitals,
            deviceVitalsIntervals: argv.deviceVitalsIntervals,
            reportPortal: argv.reportPortal,
            ensureMobileNumber: argv.ensureMobileNumber,
            byPassEia: argv.byPassEia
        }
    });
    cartesianProduct(argv.server, argv.app, argv.type, argv.brand, argv.platform)
        .forEach((prod) => {
            const [server, app, type, brand, platform] = prod;

            for (const device of argv.deviceIds(platform, brand)) {
                const returnTagsAsArray = () => (argv.tags instanceof Array ? argv.tags : [argv.tags]);
                const passTagsToRun = argv.tags instanceof Function ? [argv.tags(brand)] : returnTagsAsArray();
                const newOpts = generateDefaultOpt(passTagsToRun);

                // Set run instance specific cucumberOpts
                newOpts.cucumberOpts.id = uuid.v4();

                const platformExpression = platform === Platform.Android.name ? 'not @ios' : 'not @android';
                const appExpression = `@${app.toString().toLowerCase()}`;
                const typeExpression = `@${type.internalType.toString().toLowerCase()}`;
                const brandTag = `@${brand.toString().toLowerCase()}`;
                const excludedBrandTags = ['@lds', '@hfx', '@bos', '@mbna'].filter((tag) => tag !== brandTag);
                const brandExpression = `(${brandTag} or (not ${excludedBrandTags.join(' and not ')}))`;

                newOpts.cucumberOpts.tagExpression = `${newOpts.cucumberOpts.tagExpression} and (${[
                    appExpression, typeExpression, brandExpression, platformExpression].join(' and ')})`;

                newOpts.cucumberOpts.parsedTags = parseCucumberTags.default(newOpts.cucumberOpts.tagExpression);
                newOpts.cucumberOpts.server = server;
                newOpts.cucumberOpts.appVersion = app;
                newOpts.cucumberOpts.appType = type.internalType;
                newOpts.cucumberOpts.isUAT = type.isUAT;
                newOpts.cucumberOpts.platform = platform;
                newOpts.cucumberOpts.brand = brand;
                if (device !== randomDevice) {
                    newOpts.cucumberOpts.deviceName = device;
                }
                opts.push(newOpts);
            }
        });
    return opts;
}

function groupCucumberOptions(argv, opts, groupedFiles) {
    const fileHasTags = (file, opt) => file.tags.find((tags) => opt.cucumberOpts.parsedTags.evaluate(tags));
    const filters = [fileHasTags];
    if (argv.runModifiedNga3Features) {
        const modifiedFeatures = execSync('git diff-tree --no-commit-id -r --name-only HEAD')
            .toString()
            .trim()
            .split(os.EOL)
            .filter((filename) => filename.startsWith('src/features/') && filename.endsWith('.feature'));
        filters.push((file) => modifiedFeatures.includes(file.name.replace(`${utils.rootDirname}/`, '')));
    }
    const fileContainsRunnableScenario = (file, opt) => filters.every((filter) => filter(file, opt));

    const groupedOpts = [];
    for (const group of groupedFiles) {
        const optsForGroup = {};
        const serversForGroup = [];
        const brandsForGroup = [];

        cartesianProduct(group.files, opts).forEach(([file, opt]) => {
            if (fileContainsRunnableScenario(file, opt)) {
                if (!optsForGroup[opt.cucumberOpts.id]) {
                    optsForGroup[opt.cucumberOpts.id] = JSON.parse(JSON.stringify(opt));
                }
                if (optsForGroup[opt.cucumberOpts.id]
                    .cucumberOpts.specs.indexOf(file.name) === -1) {
                    optsForGroup[opt.cucumberOpts.id].cucumberOpts.specs.push(file.name);
                }

                if (serversForGroup.indexOf(opt.cucumberOpts.server) === -1) {
                    serversForGroup.push(opt.cucumberOpts.server);
                }

                if (brandsForGroup.indexOf(opt.cucumberOpts.brand) === -1) {
                    brandsForGroup.push(opt.cucumberOpts.brand);
                }
            }
        });
        const optsForGroupArray = Object.keys(optsForGroup).map((key) => {
            delete optsForGroup[key].cucumberOpts.id;
            delete optsForGroup[key].cucumberOpts.parsedTags;
            return optsForGroup[key];
        });
        if (optsForGroupArray.length > 0) {
            groupedOpts.push({
                switches: group.switches,
                brands: brandsForGroup,
                servers: serversForGroup,
                opts: optsForGroupArray
            });
        }
    }
    // If there are no matching scenarios, an exception will be thrown and the execution will be
    // aborted. The error message tells you that you probably used an invalid tag combination, but
    // not what tags you actually used. In the location where the error is thrown, the tags aren't
    // available and cannot be included in the error message. That's why we print them here.
    if (groupedOpts.length === 0) {
        // The tags are the same for all grouped opts
        logger.error(`These are the tags:\n\n${opts[0].cucumberOpts.tagExpression}\n`);
    }
    return groupedOpts;
}

module.exports = {
    generateCucumberOpts,
    // For unit testing only
    groupFeatureFilesBySwitches,
    generateOpts
};
