const yargs = require('yargs');

const AppType = require('../enums/appType.enum');
const Server = require('../enums/server.enum');
const Platform = require('../enums/platform.enum');
const PresetRunTypes = require('../presets/presetRunTypes');
const logger = require('../utils/logger');

const randomDevice = Symbol('randomDevice');

function verifyDataRepositoryCompatibleWithServer(argv) {
    if (argv.server.includes('STUB') && argv.dataRepository !== 'local') {
        throw new Error('STUB execution can only use local data repository');
    }
}

function verifyAndApplyDeviceIds(argv) {
    if (argv.deviceIds !== null) {
        if (typeof argv.deviceIds === 'function') {
            if (argv.server.length !== 1 || argv.app.length !== 1 || argv.type.length !== 1) {
                throw new Error('passing --deviceIds with a function requires a single server, app, and type.');
            }
        } else {
            if (argv.server.length !== 1 || argv.app.length !== 1 || argv.type.length !== 1
                || argv.brand.length !== 1 || argv.platform.length !== 1) {
                throw new Error('--deviceIds with a list of devices requires a single server, app, type, brand, and platform.');
            }
            if (typeof argv.deviceIds === 'string') {
                const deviceIdList = argv.deviceIds.split(',').map((deviceId) => (deviceId === 'randomDevice' ? randomDevice : deviceId));
                argv.deviceIds = () => deviceIdList;
            }
        }
    } else {
        argv.deviceIds = () => [randomDevice];
    }
}

function verifyAndApplyRunModifiedNgaFeatures(argv) {
    if (argv.runModifiedNga3Features) {
        argv.tags = ['@nga3'];

        if (argv.app.length !== 1 && argv.app[0] !== 'NGA3') {
            throw new Error('runModifiedNga3Features is only valid with --app NGA3');
        }
    }
}

function applyAppType(argv) {
    argv.type = argv.type.map((type) => AppType.fromString(type));
}

function verifyToggleSwitches(argv) {
    if ((argv.server.includes(Server.STUB.toString()) || argv.appium) && argv.toggleSwitches) {
        throw new Error('--toggleSwitches is invalid with Appium or Perfecto with --server STUB.');
    }
}

function applyPresetRunTypes(argv) {
    if (argv.preset) {
        const preset = PresetRunTypes.fromString(argv.preset);
        preset.keys.forEach((key) => {
            if (key === 'tags' && !(preset[key] instanceof Function)) {
                argv[key] = preset[key].replace('{$tags}', argv[key]);
            } else {
                argv[key] = preset[key];
            }
        });
    }
}

function getAppType() {
    let appType = getDefaultArgs(process.argv).type[0];
    try {
        const argv = getArgv(process.argv);
        if (argv.preset) {
            const preset = PresetRunTypes.fromString(argv.preset);
            appType = preset.type[0];
        }
    } catch (e) {
        logger.warn('Using default appType argument.');
    }
    return appType.replace(/_UAT/i, '');
}

function getDefaultArgs(processArgv) {
    const {argv} = yargs(processArgv)
        .usage('Usage: npm run <appium|perfecto|headspin> -- [options]')
        .options({
            provider: {
                alias: 'pro',
                describe: 'Specify the device provider for tests',
                default: 'perfecto',
                choices: ['perfecto', 'local', 'headspin', 'mock'],
                array: true
            },
            brand: {
                alias: 'b',
                describe: 'Specify which brand to run against',
                array: true,
                default: ['LDS'],
                choices: ['LDS', 'HFX', 'BOS', 'MBNA']
            },
            platform: {
                alias: 'p',
                describe: 'Specify which platform to run against',
                array: true,
                default: [Platform.Android.name],
                choices: Platform.keys()
            },
            server: {
                alias: 's',
                describe: 'Specify which server to run against',
                array: true,
                default: 'STUB',
                choices: Server.keys()
            },
            app: {
                alias: 'a',
                describe: 'Specify which app to run against',
                array: true,
                default: 'NGA3',
                choices: ['NGA3']
            },
            type: {
                describe: 'Specify which app type to run against',
                array: true,
                default: 'RNGA',
                choices: AppType.keys()
            },
            tags: {
                alias: 't',
                describe: 'Specify which tags to run; See https://docs.cucumber.io/tag-expressions/ for syntax',
                string: true,
                default: '@smokeNga3'
            },
            buildSubfolder: {
                alias: 'bs',
                describe: 'Specify a subfolder for the app binaries e.g. "abc" would test "abc/RNGA_LDS.2.apk" or similar',
                string: true,
                default: null
            },
            useInstalledApp: {
                alias: 'uia',
                describe: 'Specify running against New app or the existing installed app',
                default: false,
                boolean: true
            },
            htmlReport: {
                describe: 'Produce an HTML report. This will remove all old cucumber/json reports.',
                default: false,
                boolean: true
            },
            toggleSwitches: {
                describe: 'Toggle switches to correct state on specified servers.',
                default: false,
                boolean: true
            },
            deviceIds: {
                describe: 'Comma-separated list of Perfecto Device IDs to run the tests against, where an ID can be the magic string '
                + '"randomDevice" for a random device',
                default: null,
                string: true
            },
            perfectoReportingJobName: {
                describe: 'Label used on Perfecto\'s CI Dashboard.  Should only be used by Jenkins.',
                default: null,
                string: true
            },
            perfectoReportingJobNumber: {
                describe: 'Label used on Perfecto\'s CI Dashboard.  Should only be used by Jenkins.',
                default: null,
                number: true
            },
            perfectoExtraTags: {
                describe: 'Extra tags to be passed to Perfecto.  Should only be used by Jenkins.',
                array: true,
                default: []
            },
            perfectoEnableVideo: {
                describe: 'Enable video output when using Perfecto',
                // Do not change this defaulting without reading the comment in perfecto.config.js.
                boolean: true,
                default: false
            },
            perfectoScreenshotOnErrorOnly: {
                describe: 'Take screenshots on error only, rather than all the time.',
                boolean: true,
                default: false
            },
            dryRun: {
                describe: 'Don\'t actually spawn the wdio process.',
                boolean: true,
                default: false
            },
            runModifiedNga3Features: {
                describe: 'Run NGA3 features modified in the previous commit only.  This is designed for Jenkins, but '
                + 'you can use it if you really want.',
                default: false,
                boolean: true
            },
            dataRepository: {
                describe: 'Use `remote` for the new TDaaS, and `local` for CSV file.',
                default: 'local',
                choices: ['local', 'remote'],
                string: true
            },
            failFast: {
                describe: 'Fail as soon as an error occurs',
                boolean: true,
                default: false
            },
            perfectoDescription: {
                describe: 'Description on perfecto device, used to identify team',
                string: true,
                default: null
            },
            preset: {
                describe: 'Append arguments from one of the preset argument lists.',
                default: null,
                choices: PresetRunTypes.keys().concat([null])
            },
            maxInstances: {
                describe: 'Max parallel devices to share test execution load',
                default: 1,
                number: true
            },
            reportPortal: {
                alias: 'rp',
                describe: 'flag to handle report portal',
                boolean: true,
                default: false
            },
            allureReports: {
                alias: 'ar',
                describe: 'Generate Allure reports of the test execution.',
                boolean: true,
                default: false
            },
            deviceVitals: {
                alias: 'dv',
                describe: 'Capture device vitals during test (Perfecto Only)',
                boolean: true,
                default: false
            },
            deviceVitalsIntervals: {
                describe: 'Vital collection frequency, in seconds (Perfecto Only)',
                type: 'number',
                default: 10
            },
            ensureMobileNumber: {
                describe: 'Set it to true if you want the framewrok to ensure mobile number as per device',
                type: 'boolean',
                default: false
            },
            byPassEia: {
                describe: 'Set it to true if you need to bypass eia call during enrolment process',
                type: 'boolean',
                default: true
            }
        })
        .help()
        .alias('help', 'h')
        .strict()
        .parserConfiguration({'camel-case-expansion': false, 'short-option-group': false});

    return argv;
}

function getArgv(processArgv) {
    const argv = getDefaultArgs(processArgv);
    // The .strict() above will catch unrecognised arguments which begin with - or --, but we also
    // need the below to catch other excess arguments.
    const unrecognisedArguments = argv._.slice(2);
    if (unrecognisedArguments.length > 0) {
        throw new Error(`Found unrecognised arguments: ${unrecognisedArguments.join(' ')}`);
    }

    applyPresetRunTypes(argv);
    applyAppType(argv);
    verifyAndApplyDeviceIds(argv);
    verifyToggleSwitches(argv);
    verifyAndApplyRunModifiedNgaFeatures(argv);
    verifyDataRepositoryCompatibleWithServer(argv);

    return argv;
}

module.exports = {
    getArgv,
    getAppType,
    randomDevice
};
