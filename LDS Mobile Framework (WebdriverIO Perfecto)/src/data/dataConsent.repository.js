/* eslint import/no-dynamic-require: "off" */
const DataConsent = require('../enums/dataConsent.enum');

module.exports = class DataConsentRepository {
    static parseDataConsentTags(tags) {
        const dataConsentJson = {};

        const unMunge = (tag) => tag.toUpperCase()
            .slice(4)
            .replace(/-/g, '_');
        tags.filter((t) => t.startsWith('@dc-')).forEach((tag) => {
            if (tag.endsWith('-off')) {
                const dataConsentKey = unMunge(tag.slice(0, tag.length - 4));
                const dataConsent = DataConsent.fromString(dataConsentKey);
                dataConsentJson[dataConsent.identifier] = false;
            } else if (tag.endsWith('-on')) {
                const dataConsentKey = unMunge(tag.slice(0, tag.length - 3));
                const dataConsent = DataConsent.fromString(dataConsentKey);
                dataConsentJson[dataConsent.identifier] = true;
            } else {
                throw new Error('invalid state for data consent, must be on or off');
            }
            this.isSet = true;
        });

        return dataConsentJson;
    }

    constructor() {
        this.dataConsent = null;
    }

    set(tags, isStub) {
        this.dataConsent = DataConsentRepository.parseDataConsentTags(tags, isStub);
    }

    get() {
        if (this.dataConsent === null) {
            throw new Error('Unable to retrieve data consent list before they have been set');
        }
        return this.dataConsent;
    }
};
