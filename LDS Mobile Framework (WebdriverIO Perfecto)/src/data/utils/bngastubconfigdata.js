/* eslint import/no-dynamic-require: "off" */
const userData = {};
const ngaStubBasePath = '../../../../nga-mobile-3-config/stub/scenarios';
const transformAccountName = (accountType) => accountType.match('(.*)(_ACCOUNT|_ACOUNT)')[1];
const businessFile = (filename) => `${ngaStubBasePath}/${filename}/commercial/v1/businesses/get/200.json`;
const businessesFile = (filename) => `${ngaStubBasePath}/${filename}/commercial/v1/arrangements/get/200.json`;

const parseStubConfig = (filename) => {
    const fileRead = require(businessFile(filename));
    fileRead.businesses.items.forEach((businessID) => {
        if (userData[businessID.userRole] === undefined) {
            userData[businessID.userRole] = {};
        }
        userData[businessID.userRole][businessID.businessName] = {};
        require(businessesFile(filename))
            .arrangements
            .items
            .forEach((element) => {
                if (element.businessName === businessID.businessName) {
                    // eslint-disable-next-line max-len
                    userData[businessID.userRole][businessID.businessName][transformAccountName(element.type)] = element.accountName;
                }
            });
    });
    return userData;
};

module.exports = {
    parseStubConfig
};
