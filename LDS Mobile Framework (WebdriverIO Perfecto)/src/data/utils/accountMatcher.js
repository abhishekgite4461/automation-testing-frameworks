/* eslint import/no-dynamic-require: "off" */
const _ = require('lodash');
const UserData = require('../savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const DeviceHelper = require('../../utils/device.helper');

let accountsList;

module.exports = class AccountMatcher {
    static getBNGAEnvironmentData(testEnv, userId) {
        return require(`../../../resources/bnga/env/${testEnv}/${userId}`);
    }

    /**
     * @returns first matching business based on access level and accountsRequested as set in findMatchingBusiness
     */
    static getBusiness() {
        if (DeviceHelper.isBNGA()) {
            return Object.keys(UserData.getData(SavedData.MATCHING_BUSINESS))[0];
        }
        throw new Error('Only Applicable to BNGA');
    }

    /**
     * @returns first matching business based on access level and accountsRequested
     */
    static getBusinessByAccessAndAccount(accountsRequested, accessLevel) {
        if (DeviceHelper.isBNGA()) {
            (new AccountMatcher()).findMatchingBusiness(
                UserData.getData(SavedData.BNGA_DATA).data, accessLevel, accountsRequested
            );
            return Object.keys(UserData.getData(SavedData.MATCHING_BUSINESS))[0];
        }
        throw new Error('Only Applicable to BNGA');
    }

    static isSingleBusiness() {
        // check if there is one access type and one business in it
        if (DeviceHelper.isBNGA()) {
            return (
                Object.keys(UserData.getData(SavedData.BNGA_DATA).data).length === 1
                && Object.keys(UserData.getData(SavedData.BNGA_DATA)
                    .data[Object.keys(UserData.getData(SavedData.BNGA_DATA).data)[0]]).length === 1
            );
        }
        throw new Error('Only Applicable to BNGA');
    }

    static getData(type) {
        const data = DeviceHelper.isBNGA()
            ? UserData.getData(SavedData.BNGA_DATA)[type]
            : UserData.getData(SavedData.RNGA_DATA)[type];
        if (!data) throw new Error(`data ${type} is ${data}`);
        return data;
    }

    static getAccount(type) {
        const account = DeviceHelper.isBNGA()
            ? UserData.getData(SavedData.MATCHING_BUSINESS)[Object.keys(
                UserData.getData(SavedData.MATCHING_BUSINESS)
            )[0]][type]
            : UserData.getData(SavedData.RNGA_DATA)[type.replace(/ /g, '')];
        if (!account) throw new Error(`account ${type} is ${account}`);
        return account;
    }

    /**
     * Find business matching  access level and accountsRequested and save it into UserData
     */
    static setAccountData(assignedUser, accessLevel = null, accountsRequested = null) {
        if (DeviceHelper.isBNGA()) {
            // update BNGA_DATA and MATCHING_BUSINESS
            (new AccountMatcher()).findMatchingBusiness(assignedUser.data, accessLevel, accountsRequested);
            UserData.setData(SavedData.BNGA_DATA, assignedUser);
        } else {
            UserData.setData(SavedData.RNGA_DATA, assignedUser);
        }
    }

    /**
     * Search data for different parameters and return matching business
     * @param data - contains user businesses , access levels and types of accounts
     * @param accessLevel - access level to be matched
     * @param accountsRequested - types of accounts to be searched in data ex: savings and current
     * @returns matching business object or throws error if it cannot be found
     */
    findMatchingBusiness(data, accessLevel, accountsRequested) {
        let matched = {};
        if ([data, accessLevel, accountsRequested].includes(undefined)) {
            throw new Error('undefined data');
        }

        if (accountsRequested.includes('SINGLE_BUSINESS')) {
            // single business only
            if (Object.keys(data).length === 1) {
                const filteredArray = _.filter(accountsRequested, (val) => val !== 'SINGLE_BUSINESS');
                matched = this.findMatchingBusiness(data, accessLevel, filteredArray);
            } else {
                throw new Error('expected single business, returned multiple business');
            }
        } else if (!accessLevel) {
            // When access level is not specified return first business
            const matchedBusiness = data[Object.keys(data)[0]];
            if (_.isEmpty(matched)) {
                matched[Object.keys(matchedBusiness)[0]] = matchedBusiness[Object.keys(matchedBusiness)[0]];
            }
        } else {
            // multiple business or single business
            const businessesByAccess = data[accessLevel];
            if (accountsRequested.includes('or')) {
                accountsList = accountsRequested.split('or')
                    .map((x) => x.trim());

                Object.keys(businessesByAccess)
                    .forEach((businessName) => {
                        accountsList.forEach(((accountToBeMatched) => {
                            if (Object.keys(businessesByAccess[businessName])
                                .includes(accountToBeMatched)) {
                                if (_.isEmpty(matched)) matched[businessName] = businessesByAccess[businessName];
                            }
                        }));
                    });
            } else if (accountsRequested.includes('and')) {
                // when matching multiple accounts with and operator
                accountsList = accountsRequested.split('and')
                    .map((x) => x.trim());
                _.forEach(businessesByAccess, (value, businessName) => {
                    if (_.difference(accountsList, Object.keys(value)).length === 0) {
                        if (_.isEmpty(matched)) matched[businessName] = businessesByAccess[businessName];
                    }
                });
            } else if (accountsRequested.includes('only')) {
                // When only one account must be present in business
                _.forEach(businessesByAccess, (value, businessName) => {
                    if (accountsRequested.split('only')[0].trim() === Object.keys(value)[0] && Object.keys(value).length === 1) {
                        if (_.isEmpty(matched)) matched[businessName] = businessesByAccess[businessName];
                    }
                });
            } else {
                // when there is only one account
                accountsList = accountsRequested;
                _.forEach(businessesByAccess, (value, businessName) => {
                    if (Object.keys(value)
                        .includes(accountsRequested instanceof Array ? accountsRequested[0] : accountsRequested)) {
                        if (_.isEmpty(matched)) matched[businessName] = businessesByAccess[businessName];
                    }
                });
            }
        }
        UserData.setData(SavedData.MATCHING_BUSINESS, matched);
        if (!_.isEmpty(matched)) return matched;
        throw new Error(`Unable to find matching Business for access: ${accessLevel}, accounts: ${accountsRequested}`);
    }
};
