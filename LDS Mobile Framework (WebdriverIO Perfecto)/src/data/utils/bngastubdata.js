/* eslint import/no-dynamic-require: "off" */
const fsPath = require('fs-path');
const bngaTestConfig = require('../../../resources/stubdata/config/stub_test_config');
const {parseStubConfig} = require('./bngastubconfigdata');

let stubData;

const getBngaStubData = (parsedStub) => {
    bngaTestConfig[0].environments.forEach((stubType) => {
        if (stubType.name === parsedStub.platform) {
            stubType.servers.forEach((subStub) => {
                if (subStub.name === parsedStub.appName) {
                    stubData = require(`../../../resources/stubdata/${subStub.section}.json`);
                }
            });
        }
    });
};

const getBngaData = (stub) => {
    try {
        getBngaStubData(stub);
    } catch (err) {
        const missingStubFile = err.message.match('../../../resources/stubdata/(.*).json')[1];
        const jsonData = {data: {}};
        try {
            jsonData.data = parseStubConfig(missingStubFile);
            const newStubFile = `${__dirname}/../../../resources/stubdata/${missingStubFile}.json`;
            fsPath.writeFileSync(newStubFile, JSON.stringify(jsonData), 'utf-8');
            stubData = require(newStubFile);
        } catch (errr) {
            stubData = require('../../../resources/stubdata/success/enrolled');
        }
    }
    return stubData;
};

module.exports = {getBngaData};
