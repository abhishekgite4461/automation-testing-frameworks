// To save data from the app in a particular step and retrieve in someother step
const savedData = {};

function setData(key, value) {
    if (value === undefined || value === null) {
        throw new Error(`Call to set data for ${key} is either undefined or null`);
    }
    savedData[key] = value;
}

function getData(key) {
    const value = savedData[key];
    if (value === undefined || value === null) {
        throw new Error(`Unable to retrieve value for key ${key} before it has been set`);
    }
    return value;
}

module.exports = {setData, getData};
