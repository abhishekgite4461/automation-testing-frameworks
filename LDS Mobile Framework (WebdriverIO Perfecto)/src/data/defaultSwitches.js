const RetailSwitchEnum = require('../enums/RNGA/switch.enum');
const BusinessSwitchEnum = require('../enums/BNGA/switch.enum');

module.exports = {
    RNGA: {
        on: [
            RetailSwitchEnum.NGA_RESTRICT_ACCESS,
            RetailSwitchEnum.RNGA_LT,
            RetailSwitchEnum.DATA_CONSENT
        ],
        off: [
            RetailSwitchEnum.ENABLE_RNGA_VEF_CREDIT_CARD,
            /* note :if they are on then maintenance screen would be there and all aft tests will fail */
            RetailSwitchEnum.ENABLE_PLACEMENT_RNGA_IOS,
            RetailSwitchEnum.ENABLE_PLACEMENT_RNGA_ANDROID
        ]
    },
    BNGA: {
        on: [
            BusinessSwitchEnum.TOUCH_ID,
            BusinessSwitchEnum.FINGERPRINT_ANDROID
        ],
        off: [
            /* note :if they are on then maintenance screen would be there and all aft tests will fail */
            BusinessSwitchEnum.ENABLE_PLACEMENT_BNGA_IOS,
            BusinessSwitchEnum.ENABLE_PLACEMENT_BNGA_ANDROID
        ]
    }
};
