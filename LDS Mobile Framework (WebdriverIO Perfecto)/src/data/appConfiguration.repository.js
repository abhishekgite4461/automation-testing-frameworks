module.exports = class AppConfigurationRepository {
    constructor() {
        this.appConfiguration = null;
    }

    set(tags) {
        this.appConfiguration = {};
        this.appConfiguration.instrument = tags.some((t) => t.name === '@instrument');
        this.appConfiguration.sensorInstrument = tags.some((t) => t.name === '@sensorInstrument');
    }

    get() {
        if (this.appConfiguration === null) {
            throw Error('Unable to retrieve getAppConfiguration before it has been set');
        }
        return this.appConfiguration;
    }
};
