/* eslint import/no-dynamic-require: "off" */
const AbTests = require('../enums/abTest.enum');

module.exports = class AbTestRepository {
    static parseAbTags(tags) {
        const abJson = {};
        const unMunge = (tag) => tag.slice(4)
            .split('~');
        tags.filter((t) => t.startsWith('@ab-'))
            .forEach((tag) => {
                if (!tag.includes('~')) {
                    throw new Error('option not specified for AB test');
                }
                const activityAndOption = unMunge(tag);
                const activity = activityAndOption[0].toUpperCase()
                    .replace(/-/g, '_');
                if (!AbTests[activity]) {
                    throw new Error('AB test is not supported');
                }
                const option = matchABOption(activityAndOption[1]);
                function matchABOption(ABoption) {
                    let i = 0;
                    while (i < AbTests[activity].options.length) {
                        if (AbTests[activity].options[i] === ABoption) {
                            return AbTests[activity].options[i];
                        }
                        i++;
                    }
                    throw new Error('AB test/option is not supported');
                }

                abJson.locationName = AbTests[activity].activityName;
                abJson.option = option;
            });
        return abJson;
    }

    constructor() {
        this.abTests = null;
    }

    set(tags, isStub) {
        this.abTests = AbTestRepository.parseAbTags(tags, isStub);
    }

    get() {
        if (this.abTests === null) {
            throw new Error('Unable to retrieve Abtest list before they have been set');
        }
        return this.abTests;
    }
};
