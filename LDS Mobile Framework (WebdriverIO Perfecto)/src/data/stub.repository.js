const Stub = require('../enums/stub.enum');

module.exports = class StubRepository {
    constructor() {
        this.stub = null;
    }

    static parseStubTags(tags) {
        const allStubTags = tags
            .filter((tag) => tag.startsWith('@stub-'))
            .join(' ');

        for (const stubValue of Stub.symbols()) {
            if (allStubTags.match(new RegExp(`^@stub-${stubValue.name.replace(/_/g, '-')}$`, 'gi'))) {
                return stubValue;
            }
        }

        throw new Error('Missing @stub-XXX tag!');
    }

    set(tags) {
        this.stub = StubRepository.parseStubTags(tags);
    }

    get() {
        if (this.stub === null) {
            throw new Error('Unable to retrieve stub before it has been set');
        }
        return this.stub;
    }
};
