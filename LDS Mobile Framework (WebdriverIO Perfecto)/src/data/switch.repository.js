/* eslint import/no-dynamic-require: "off" */

const commandlineArgumentParser = require('../runner/commandlineArgumentParser');

const Switch = require(`../enums/${commandlineArgumentParser.getAppType()}/switch.enum`);
const defaultSwitchesList = require('./defaultSwitches')[commandlineArgumentParser.getAppType()];

module.exports = class SwitchRepository {
    static parseSwitches(tags, isStub, brand, defaultSwitches = defaultSwitchesList) {
        const on = [];
        const off = [];
        const defaultSwithcesToToggle = !defaultSwitches[brand] ? defaultSwitches : defaultSwitches[brand];
        // Slice(4) is to remove "@sw-" from the start of the tag.
        const unMunge = (tag) => tag.toUpperCase().slice(4).replace(/-/g, '_');

        tags.filter((t) => t.startsWith('@sw-')).forEach((tag) => {
            if (tag.endsWith('-off')) {
                const switchKey = unMunge(tag.slice(0, tag.length - 4));
                const sw = Switch.fromString(switchKey);
                if (isStub) {
                    if (sw.defaultStateOn) {
                        off.push(sw);
                    }
                } else {
                    off.push(Switch[switchKey]);
                }
            } else if (tag.endsWith('-on')) {
                const switchKey = unMunge(tag.slice(0, tag.length - 3));
                const sw = Switch.fromString(switchKey);
                if (isStub) {
                    if (!sw.defaultStateOn) {
                        on.push(sw);
                    }
                } else {
                    on.push(Switch[switchKey]);
                }
            } else {
                throw new Error(`Bad switch tag: ${tag}`);
            }
        });
        const returnValue = !isStub
            ? {on: on.concat(defaultSwithcesToToggle.on), off: off.concat(defaultSwithcesToToggle.off)}
            : {on, off};
        return returnValue;
    }

    constructor() {
        this.switches = null;
    }

    set(tags, isStub, brand, defaultSwitches = defaultSwitchesList) {
        this.switches = SwitchRepository.parseSwitches(tags, isStub, brand, defaultSwitches);
    }

    get() {
        if (this.switches === null) {
            throw new Error('Unable to retrieve switches before they have been set');
        }
        return this.switches;
    }
};
