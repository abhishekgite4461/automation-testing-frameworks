/* eslint import/no-dynamic-require: "off" */
const Features = require('../enums/feature.enum');

module.exports = class FeatureRepository {
    static parseFeatureTags(tags) {
        const featuresJson = {};

        // Slice(4) is to remove "@ff-" from the start of the tag.
        const unMunge = (tag) => tag.toUpperCase()
            .slice(4)
            .replace(/-/g, '_');

        tags.filter((t) => t.startsWith('@ff-'))
            .forEach((tag) => {
                if (tag.endsWith('-off')) {
                    const featureKey = unMunge(tag.slice(0, tag.length - 4));
                    const feature = Features.fromString(featureKey);
                    if (!feature.defaultStateOn) {
                        throw new Error('Requested feature flag is already Off');
                    }
                    featuresJson[feature.identifier] = false;
                } else if (tag.endsWith('-on')) {
                    const featureKey = unMunge(tag.slice(0, tag.length - 3));
                    const feature = Features.fromString(featureKey);
                    if (feature.defaultStateOn) {
                        throw new Error('Requested feature flag is already On');
                    }
                    featuresJson[feature.identifier] = true;
                } else {
                    throw new Error('Bad feature tag');
                }
            });
        return featuresJson;
    }

    constructor() {
        this.featureFlags = null;
    }

    set(tags, isStub) {
        this.featureFlags = FeatureRepository.parseFeatureTags(tags, isStub);
    }

    get() {
        if (this.featureFlags === null) {
            throw new Error('Unable to retrieve feature flag list before they have been set');
        }
        return this.featureFlags;
    }
};
