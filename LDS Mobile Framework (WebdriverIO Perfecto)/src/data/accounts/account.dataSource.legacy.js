const environmentApi = require('../../utils/environmentApiClient');

module.exports = class RemoteAccountDataSource {
    constructor() {
        this.environmentApi = environmentApi;
    }

    initialize() {
        this.environmentApi.verifyConnected();
    }

    getAccount(server, appType, brand, desiredUserType, mobileNumber) {
        return this.environmentApi.fetchCustomer(server, appType, brand, desiredUserType, mobileNumber);
    }

    releaseAccount(account) {
        this.environmentApi.releaseCustomer(account.reference);
    }

    markDirty(account, reason) {
        this.environmentApi.markDirty(account.reference, reason);
    }
};
