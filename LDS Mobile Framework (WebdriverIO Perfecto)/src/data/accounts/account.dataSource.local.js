const fs = require('fs');
const path = require('path');
const parse = require('csv-parse/lib/sync');
const _ = require('lodash');
const UserType = require('../../enums/userType.enum');

module.exports = class LocalAccountDataSource {
    constructor(directory = __dirname) {
        this.directory = path.resolve(directory);
        this.users = this.loadData();
    }

    loadData() {
        return _.flatten([
            this.loadDataFromCSVFile('RNGA/accounts.csv'),
            this.loadDataFromCSVFile('RNGA/accounts.ibreg.csv'),
            this.loadDataFromCSVFile('BNGA/accounts.csv')
        ]);
    }

    loadDataFromCSVFile(fileName) {
        const demungeUserType = (mungedUserType) => UserType.fromString(mungedUserType.trim().replace(/ /g, '_').toUpperCase());
        const file = fs.readFileSync(path.join(this.directory, fileName));
        const users = parse(file, {columns: true, skip_empty_lines: true});
        users.map((user) => (user.userType = demungeUserType(user.userType)));
        users.forEach((user) => {
            if (user.serverName === 'STUB') {
                throw new Error('Please do not add STUB users to accounts.cvs');
            }
        });
        return users;
    }

    getAccount(server, appType, brand, desiredUserType, mobileNumber, withEnrolment) {
        return this.users.find((user) => user.brand === brand
            && user.appType === appType
            && user.serverName === server
            && user.userType.valueOf() === desiredUserType.valueOf()
            && (!withEnrolment || user.phoneNumber === mobileNumber)) || null;
    }

    // No-op methods
    initialize() {
        return this.users;
    }

    releaseAccount() {
        return this.users;
    }

    markDirty() {
        return this.users;
    }
};
