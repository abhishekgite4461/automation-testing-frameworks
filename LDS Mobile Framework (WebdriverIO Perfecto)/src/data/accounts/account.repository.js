const AccountFactory = require('./account.factory');
const LocalAccountDataSource = require('./account.dataSource.local');
const RemoteAccountDataSource = require('./account.dataSource.remote');
const environmentApiClient = require('../../utils/environmentApiClient');
const logger = require('../../utils/logger');

const nonPasswordResetUserTypes = ['SUSPENDED', 'REVOKED'];
const frontEndEiaUserType = 'EIA_USER_FOR_ENROLLMENT_WITHOUT_BYPASS';
const noRegisteredPhoneNumberUserType = ['NO_REGISTERED_PHONE_NUMBER'];
// Default phone number to avoid incoming call in the test device
const defaultEiaPhoneNumber = '+447384454665';

module.exports = class AccountRepository {
    constructor(
        accountFactory = new AccountFactory(),
        localAccountDataSource = new LocalAccountDataSource(),
        remoteAccountDataSource = new RemoteAccountDataSource()
    ) {
        this.accountFactory = accountFactory;
        this.localAccountDataSource = localAccountDataSource;
        this.remoteAccountDataSource = remoteAccountDataSource;
        // Preferably we should only have one data source, but we have three for compatibility.
        this.dataSource = localAccountDataSource;

        this.assignedUser = null;
        this.assignedUserIsStub = null;
    }

    // This method isn't really required, and is only here to provide a compatible interface
    initialize(dataSource, ensureMobileNumber, byPassEia) {
        this.ensureMobileNumber = ensureMobileNumber;
        this.byPassEia = byPassEia;
        this.dataSource = this[`${dataSource}AccountDataSource`];
        this.dataSource.initialize();
    }

    assign(server, appType, brand, desiredUserType, enrolmentRequired, mobileNumber, cardNumber) {
        if (server === 'STUB') {
            this.assignedUser = {
                serverName: server,
                appType,
                brand,
                username: '123456789',
                password: 'password1234',
                memorableInformation: 't35ting',
                userTypes: desiredUserType
            };
            if (appType === 'RNGA') {
                this.assignedUser.newPostCode = 'IG2 7ES';
                this.assignedUser.phoneNumber = '07451111111';
                this.assignedUser.workNumber = '02011111111';
            } else if (appType === 'BNGA') {
                this.assignedUser.cardReader = {pan: '1111111111112345'};
            }
            this.assignedUserIsStub = true;
            if (desiredUserType.toString()
                .startsWith('IBREG')) {
                Object.assign(this.assignedUser, {
                    firstName: 'Stephen',
                    lastName: 'James',
                    emailId: 'lloydstestinguser@lloydsbanking.com',
                    postCode: 'E112XX',
                    ibregPostCode: 'EC1Y 4XX',
                    accountType1: 'Current_Savings',
                    accountType2: 'Loan',
                    accountType3: 'Mortgage',
                    accountType4: 'Credit_Card',
                    accountNumber: '19979860',
                    loanNumber: '987654321012',
                    mortgageNumber: '98765432101234',
                    creditCardNumber: '4751290287863962',
                    sortCode: '774824',
                    userId: '756767488',
                    userPassword: 'tester1234',
                    confirmUserPassword: 'tester1234',
                    memorableInfo: 't35ting',
                    confirmMemorableInfo: 't35ting',
                    activationCode: 'actcode1',
                    dobDate: '09',
                    dobMonth: 'November',
                    dobYear: '1983',
                    dob: '09-Nov-1983',
                    blacklistedPassword: 'password1234',
                    ibregIncorrectPostcode: {
                        firstName: 'Stephen',
                        lastName: 'James',
                        emailId: '8cbkaoh4k183xg7yn6zuxr@g.com',
                        ibregPostCode: 'E7 9QL',
                        accountNumber: '06987168',
                        loanNumber: '100123427298',
                        creditCardNumber: '5521573655373913',
                        sortCode: '770110',
                        date: '20',
                        month: 'May',
                        year: '1963'
                    }
                });
            }
        } else {
            const user = this.dataSource.getAccount(
                server, appType, brand, desiredUserType, mobileNumber, enrolmentRequired
            );
            if (user !== null) {
                this.assignedUser = user;
            } else {
                // eslint-disable-next-line max-len
                throw new Error(`Data not found for ${appType}, ${server}, ${brand}, ${desiredUserType}, ${mobileNumber} , ${cardNumber}`);
            }
            this.assignedUserIsStub = false;
        }
        Object.assign(
            this.assignedUser,
            this.accountFactory.getAccount(appType, server, brand, this.assignedUser.username)
        );

        // TODO MPL - 1455;
        this.releaseAccount();
        if (this.ensureMobileNumber) {
            if (!noRegisteredPhoneNumberUserType.includes(desiredUserType.toString())) {
                if (this.byPassEia && (desiredUserType.toString() !== frontEndEiaUserType)) {
                    environmentApiClient.modifyMobileNumber(server, appType, brand,
                        this.assignedUser.username, defaultEiaPhoneNumber);
                } else {
                    environmentApiClient.modifyMobileNumber(server, appType, brand,
                        this.assignedUser.username, mobileNumber);
                }
            }
            if (!nonPasswordResetUserTypes.includes(desiredUserType.toString())) {
                try {
                    environmentApiClient.resetPassword(server, appType, brand, this.assignedUser.username,
                        this.assignedUser.password);
                } catch (e) {
                    // We don't care about this exception, still execute it. Add logic later to retry for password reset
                    logger.info('Exception while resetting password for userid(', this.assignedUser.username, ')', e);
                }
            }
        }
        return this.assignedUser;
    }

    get() {
        if (!this.assignedUser) {
            throw new Error('Unable to retrieve data which has not been assigned.');
        }
        return this.assignedUser;
    }

    unassign() {
        if (this.assignedUser !== null && !this.assignedUserIsStub) {
            this.assignedUser = null;
        }
    }

    releaseAccount() {
        this.dataSource.releaseAccount(this.assignedUser);
    }

    markDirty(reason) {
        if (this.assignedUser !== null && !this.assignedUserIsStub) {
            this.dataSource.markDirty(this.assignedUser, reason);
            this.assignedUser = null;
        }
    }
};
