const fs = require('fs');
const path = require('path');

module.exports = class AccountFactory {
    constructor(directory = __dirname) {
        this.directory = path.resolve(directory);
    }

    getAccount(appType, server, brand, username) {
        const account = this.fetchData(`${appType}/default.json`);
        if (server === 'STUB' && brand === 'MBNA') {
            Object.assign(account, this.fetchData(`${appType}/stub/mbna/default.json`));
        } else if (server === 'STUB') {
            Object.assign(account, this.fetchData(`${appType}/stub/default.json`));
        } else {
            Object.assign(account, this.fetchData(`${appType}/${brand.toLowerCase()}/default.json`));
        }
        Object.assign(account, this.fetchData(`${appType}/users/${username}.json`));
        return account;
    }

    fetchData(filePath) {
        const fullPath = path.join(this.directory, filePath);
        if (fs.existsSync(fullPath)) {
            return JSON.parse(fs.readFileSync(fullPath));
        }
        return {};
    }
};
