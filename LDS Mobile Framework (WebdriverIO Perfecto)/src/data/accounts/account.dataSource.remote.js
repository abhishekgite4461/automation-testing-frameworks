const TDaaS2Client = require('../../utils/tdaas2Client');
const sync = require('../../utils/sync');

module.exports = class RemoteAccountDataSource {
    constructor() {
        this.tdaas2Client = new TDaaS2Client();
    }

    initialize() {
        sync(this.tdaas2Client.verifyCredentials());
    }

    getAccount(server, appType, brand, desiredUserType, mobileNumber) {
        return sync(this.tdaas2Client.createReservation(server, appType, brand, desiredUserType, mobileNumber));
    }

    releaseAccount(account) {
        sync(this.tdaas2Client.deleteReservation(account));
    }

    // This is never called and not in use.
    // I would remove it, but I want a second opinion from the guy that manages the data.
    // Unforunately, he is on annual leave.
    markDirty() {
        return this;
    }
};
