const CommonPage = require('../pages/common.page');
const Morepage = require('../pages/more.page');

const commmonPage = new CommonPage();
const morePage = new Morepage();

module.exports = class logOffTask {
    static logOffFromApp() {
        commmonPage.closeModalWhenPresent();
        if (morePage.isBottomNavBarVisible()) {
            morePage.selectTab('more');
            morePage.selectLogoff();
        }
    }
};
