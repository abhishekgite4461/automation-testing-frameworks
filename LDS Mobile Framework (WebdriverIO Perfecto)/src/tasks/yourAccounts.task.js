const ReactivateIsaPage = require('../pages/paymentHub/reactivateIsa.page');
const HomePage = require('../pages/home.page');
const ActionMenuPage = require('../pages/actionMenu.page');

const reactivateIsaPage = new ReactivateIsaPage();
const homePage = new HomePage();
const actionMenuPage = new ActionMenuPage();

module.exports = class YourAccountsTask {
    static verifyEnterPasswordDialogBox(isVisiblePasswordPopUp) {
        reactivateIsaPage.verifyEnterPasswordDialogBox(isVisiblePasswordPopUp === 'be');
    }

    static selectReactivateIsaOfAccount(accountName) {
        const data = this.dataRepository.get();
        homePage.selectActionMenuOfSpecificAccount(data[accountName.replace(/ /g, '')]);
        actionMenuPage.selectActionMenuReactivateISA();
    }
};
