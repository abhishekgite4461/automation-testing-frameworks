const HomePage = require('../pages/home.page');
const PaymentHubMainPage = require('../pages/paymentHub/paymentHubMain.page');
const BeneficiaryTypePage = require('../pages/transferAndPayments/beneficiaryType.page');
const ReviewPaymentPage = require('../pages/paymentHub/reviewPayment.page');
const UkBeneficiaryDetailPage = require('../pages/transferAndPayments/ukBeneficiaryDetail.page.js');
const UkCompanySearchResultsPage = require('../pages/transferAndPayments/ukCompanySearchResults.page.js');
const UkMobileNumberDetailPage = require('../pages/transferAndPayments/ukMobileNumberDetail.page');
const ConfirmContactPage = require('../pages/transferAndPayments/confirmContact.page');
const SearchRecipientPage = require('../pages/searchAccountAndRecipient.page');
const DeviceHelper = require('../utils/device.helper');

const confirmContactPage = new ConfirmContactPage();
const ukBeneficiaryDetailPage = new UkBeneficiaryDetailPage();
const ukCompanySearchResultsPage = new UkCompanySearchResultsPage();
const homePage = new HomePage();
const paymentHubMainPage = new PaymentHubMainPage();
const reviewPaymentPage = new ReviewPaymentPage();
const searchRecipientPage = new SearchRecipientPage();
const beneficiaryTypePage = new BeneficiaryTypePage();
const ukMobileNumberDetailPage = new UkMobileNumberDetailPage();

module.exports = class PaymentTask {
    static navigateToAddNewRecipient() {
        homePage.selectPayAndTransfer();
        paymentHubMainPage.waitForPaymentHubPageLoad();
        PaymentTask.selectFromOrToAccount('recipient');
    }

    static addNewPayee() {
        searchRecipientPage.selectAddNewPayee();
    }

    static enterUkMobileNumberDetails(mobileNumber, amount, referenceText) {
        ukMobileNumberDetailPage.enterMobileNumber(mobileNumber);
        ukMobileNumberDetailPage.enterAmount(amount);
        ukMobileNumberDetailPage.enterReference(referenceText);
    }

    static navigateToPayMBeneficiaryReview() {
        const mobileNumber = '07575858520';
        const amount = '20.00';
        const referenceText = 'payM';
        PaymentTask.navigateToAddNewRecipient();
        PaymentTask.addNewPayee();
        beneficiaryTypePage.chooseUkMobileNumber();
        PaymentTask.enterUkMobileNumberDetails(mobileNumber, amount, referenceText);
        ukMobileNumberDetailPage.clickOnContinueButtonInPaymentHubPage();
        confirmContactPage.verifyConfirmContactMessage();
        confirmContactPage.clickOnContinueButtonInConfirmContactPage();
        reviewPaymentPage.verifyReviewPaymentPage();
        PaymentTask.confirmPayment();
    }

    static navigateToUkCompanyBeneficiaryReview() {
        const reference = 'test';
        PaymentTask.navigateToAddNewRecipient();
        searchRecipientPage.selectAddNewPayee();
        beneficiaryTypePage.chooseUkAccountBeneficiary();
        PaymentTask.enterWhiteListedDetails();
        ukBeneficiaryDetailPage.continueToReviewDetailsPage();
        ukCompanySearchResultsPage.selectResult();
        PaymentTask.addReference(reference, reference);
        ukCompanySearchResultsPage.confirmReviewDetailsPage();
    }

    static enterUkBeneficiaryDetails(sortCode, accountNumber, recipientName) {
        ukBeneficiaryDetailPage.enterUkBeneficiarySortCode(sortCode);
        ukBeneficiaryDetailPage.enterUkBeneficiaryAccountNumber(accountNumber);
        ukBeneficiaryDetailPage.enterUkBeneficiaryPayeeName(recipientName);
    }

    static navigateToUkBeneficiaryReview() {
        const sortCode = '774824';
        const accountNumber = '17759060';
        const recipientName = 'abcd1234';
        PaymentTask.navigateToAddNewRecipient();
        searchRecipientPage.selectAddNewPayee();
        beneficiaryTypePage.chooseUkAccountBeneficiary();
        PaymentTask.enterUkBeneficiaryDetails(sortCode, accountNumber, recipientName);
        ukBeneficiaryDetailPage.continueToReviewDetailsPage();
        ukCompanySearchResultsPage.confirmReviewDetailsPage();
    }

    static confirmPayment() {
        reviewPaymentPage.waitForPageLoad();
        reviewPaymentPage.selectConfirmButton();
    }

    static selectFromOrToAccount(type) {
        paymentHubMainPage.waitForPaymentHubPageLoad();
        paymentHubMainPage.selectPayAccount(type);
    }

    static enterWhiteListedDetails() {
        const sortCode = DeviceHelper.isStub() ? '400530' : '802045';
        const accountNumber = DeviceHelper.isStub() ? '71584685' : '00213765';
        const recipientName = 'abcd1234';
        ukBeneficiaryDetailPage.enterUkBeneficiarySortCode(sortCode);
        ukBeneficiaryDetailPage.enterUkBeneficiaryAccountNumber(accountNumber);
        ukBeneficiaryDetailPage.enterUkBeneficiaryPayeeName(recipientName);
    }

    static addReference(reference, reEnterReference) {
        const reReference = reEnterReference.replace(/ /g, '');
        ukCompanySearchResultsPage.addReference(reference, reReference);
    }

    static chooseFromOrToAccount(type) {
        homePage.selectPayAndTransfer();
        PaymentTask.selectFromOrToAccount(type);
    }
};
