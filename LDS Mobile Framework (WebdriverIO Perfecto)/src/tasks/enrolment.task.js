const _ = require('lodash');
const EnvironmentPage = require('../pages/environment.page');
const Server = require('../enums/server.enum');
const DeviceHelper = require('../utils/device.helper');
const DeviceState = require('../utils/deviceState');
const logger = require('../utils/logger');

/* eslint import/no-dynamic-require: "off" */
const Switch = require(`../enums/${DeviceHelper.getAppType()}/switch.enum`);
const WelcomePage = require('../pages/welcome.page');
const LoginPage = require('../pages/login.page');
const MemorableInformationPage = require('../pages/memorableInformation.page');
const EnrolmentPage = require('../pages/enrolment.page');
const EnrolmentSuccessPage = require('../pages/enrolmentSuccess.page');
const EnrolmentSecurityPage = require('../pages/enrolmentSecurity.page');
const HomePage = require('../pages/home.page');
const BusinessSelectionPage = require('../pages/businessSelection.page');
const CardReaderRespondPage = require('../pages/enrolmentCardReaderRespond.page');
const FingerprintInterstitialPage = require('../pages/fingerprint/fingerprintInterstitial.page');
const verifyAndTransformRemoteAccounts = require('../utils/accountSplitter.2.0');
const verifyAndTransformLocalAccounts = require('../utils/accountSplitter');
const AccountMatcher = require('../data/utils/accountMatcher');
const {getBngaData} = require('../data/utils/bngastubdata');

const cardReaderRespondPage = new CardReaderRespondPage();

const incorrectMI = 'incorre';

const appConfig = (server, switches, featureJson) => {
    const appConfigJson = {};
    appConfigJson.testEnvironment = server.testPhase;
    appConfigJson.testStub = `${server.platform}//${server.appName}`;
    appConfigJson.features = featureJson;
    appConfigJson.switches = {};

    switches.on.forEach((item) => {
        appConfigJson.switches[item.ibcName] = true;
    });
    switches.off.forEach((item) => {
        appConfigJson.switches[item.ibcName] = false;
    });

    return appConfigJson;
};

const setTestConfig = (serverName) => {
    const environmentPage = new EnvironmentPage();
    environmentPage.setTestConfig(serverName);
};

// MQE-1075 TODO Revert back changes made for App Ban/Warn app version incompatibility
const selectEnvironment = (serverName) => {
    const environmentPage = new EnvironmentPage();
    environmentPage.selectEnvironment(serverName);
};

// This class currently assumes that enrolment and light logon is on.
// It could definitely be extended in the future, but for now this is all we need.
module.exports = class EnrolmentTask {
    constructor(dataRepository, appConfigurationRepository, stubRepository, switchRepository, featureRepository,
        abTestRepository, dataConsentRepository, deviceState) {
        this.dataRepository = dataRepository;
        this.appConfigurationRepository = appConfigurationRepository;
        this.stubRepository = stubRepository;
        this.switchRepository = switchRepository;
        this.featureRepository = featureRepository;
        this.abTestRepository = abTestRepository;
        this.dataConsentRepository = dataConsentRepository;
        this.deviceState = deviceState;
    }

    isEnrolled() {
        if (DeviceHelper.isUAT()) {
            return true;
        }
        if (this.dataRepository.assignedUserIsStub) {
            return this.stubRepository.get().isEnrolled;
        }
        return this.deviceState.getEnrolledUser() === this.dataRepository.assignedUser.username;
    }

    getQuickLaunchJson() {
        let server = Server.fromString(browser.capabilities.server);
        const quickLaunchJson = {quickLaunch: '', featureFlags: {}, ibcSwitches: {}};

        if (DeviceHelper.isStub()) {
            server = this.stubRepository.get();
            quickLaunchJson.quickLaunch = `${server.platform}//${server.appName}`;
            const switchJson = {};
            quickLaunchJson.ibcSwitches = _.forEach(this.switchRepository.get(), (value, key) => {
                _.forEach(value, (val) => {
                    switchJson[Switch[val].appName] = key === 'on';
                });
            });
            quickLaunchJson.ibcSwitches = switchJson;
            if ('locationName' in this.abTestRepository.get()) {
                quickLaunchJson.abTest = this.abTestRepository.get();
            }
        } else {
            quickLaunchJson.quickLaunch = `${server.testPhase}//${server.platform}//${server.appName}`;
        }
        const dataconsent = this.dataConsentRepository.get();
        quickLaunchJson.featureFlags = this.featureRepository.get();
        if (Object.keys(dataconsent).length > 0) {
            quickLaunchJson.analyticsConsent = dataconsent;
        }
        return quickLaunchJson;
    }

    assignUser(accountTypes, access = null, businessType = null) {
        const nonCreditCardType = ['SUSPENDED', 'REVOKED', 'PERSONAL DETAIL PASSWORD', 'ONLY_ONE_CREDIT_CARD', 'MAX DEVICE ENROLLED', 'NO REGISTERED PHONE NUMBER'];
        const getAccountType = (accTypes) => accTypes.startsWith('IBREG') || nonCreditCardType.includes(accTypes);
        const creditCardIfMbnaAccountTypes = (!(browser.capabilities.brand === 'MBNA') || getAccountType(accountTypes.toString()
            .toUpperCase())) ? accountTypes : 'creditCard';
        let verifyAndTransformAccounts = verifyAndTransformLocalAccounts;
        if (browser.config.cucumberOpts.dataRepository === 'remote') {
            verifyAndTransformAccounts = verifyAndTransformRemoteAccounts;
        }

        let transformedAccountTypes = verifyAndTransformAccounts(creditCardIfMbnaAccountTypes);

        if (DeviceHelper.isBNGA()) {
            const typeArray = [businessType, access];

            if (browser.config.cucumberOpts.dataRepository === 'remote') {
                transformedAccountTypes = transformedAccountTypes.concat(typeArray.filter((val) => val !== null));
            } else {
                typeArray.forEach((val) => {
                    if (!val) {
                        transformedAccountTypes = `${val}${transformedAccountTypes}`;
                    }
                });
            }
        }

        if (DeviceHelper.isMobileNumberRequired()) {
            this.dataRepository.assign(
                browser.capabilities.server,
                browser.capabilities.appType,
                browser.capabilities.brand,
                transformedAccountTypes,
                !DeviceHelper.isStub(),
                DeviceHelper.mobileNumber()
            );
        } else {
            this.dataRepository.assign(
                browser.capabilities.server,
                browser.capabilities.appType,
                browser.capabilities.brand,
                transformedAccountTypes,
                DeviceHelper.isUAT()
            );
        }

        if (DeviceHelper.getAppType() === 'BNGA') {
            let data;
            if (DeviceHelper.isStub()) { // get bnga stub data for appropriate stub and update it
                this.dataRepository.assignedUser.data = getBngaData(this.stubRepository.get()).data;
            } else if (browser.config.cucumberOpts.dataRepository === 'local') {
                // for local execution get data from resources/bnga/env
                data = AccountMatcher.getBNGAEnvironmentData(browser.capabilities.server,
                    this.dataRepository.assignedUser.username);
                this.dataRepository.assignedUser.data = data.data;
            }
        }
        AccountMatcher.setAccountData(this.dataRepository.assignedUser, access, accountTypes);
        const maskingUserName = `XXXX${this.dataRepository.assignedUser.username.match(/[a-zA-Z0-9]{4}(.*)/)[1]}`;
        logger.info(`Executing test with user ${maskingUserName}`);
        if (DeviceHelper.isPerfecto() && !DeviceHelper.isStub()) {
            // report maskingUserName as a test step in environment execution to speed up debugging
            browser.execute('mobile:step:start', {
                name: `Executing test with user ${maskingUserName}`
            });
        }
    }

    // MQE-1075 TODO
    static getBanWarnTypes() {
        return ['app warn', 'app ban'];
    }

    static changeCapabilityForMultipleSession(appName) {
        const brand = appName.toUpperCase();
        DeviceHelper.changeCapabilities(brand, 'SIT09-W');
    }

    navigateToAccountAggregatedApp(appName, userType) {
        EnrolmentTask.changeCapabilityForMultipleSession(appName);
        const environmentPage = new EnvironmentPage();
        environmentPage.waitForPageLoad();
        environmentPage.resetFeatureConfig(EnrolmentTask.getBanWarnTypes(), userType);
        const quickLaunchJson = this.getQuickLaunchJson();
        setTestConfig(quickLaunchJson);
    }

    prepareAppForAccountTypes(accountTypes) {
        const server = Server.fromString(browser.capabilities.server);

        driver.switchContext('NATIVE_APP');
        if (browser.config.cucumberOpts.useInstalledApp) {
            const deviceState = new DeviceState(DeviceHelper.deviceId());
            deviceState.setAppInstalled(true, false, false);
            DeviceHelper.relaunchApp();
        } else {
            DeviceHelper.ensureCleanAppInstall(this.appConfigurationRepository.get(),
                this.isEnrolled(), false, accountTypes);
            // not to de-enrol the max device enrolled scenario
            if (!DeviceHelper.isStub() && !this.isEnrolled() && accountTypes !== 'max device enrolled') {
                DeviceHelper.deEnrolUser(this.dataRepository.assignedUser.username, server);
            }
        }

        const environmentPage = new EnvironmentPage();
        environmentPage.waitForPageLoad();
        if (browser.getOrientation() === 'landscape') {
            browser.setOrientation('portrait');
        }
        // MQE-1075 TODO
        environmentPage.resetFeatureConfig(EnrolmentTask.getBanWarnTypes(), accountTypes);
        logger.info(JSON.stringify(appConfig(server, this.switchRepository.get(), this.featureRepository.get())));

        // MQE-1075 TODO Revert
        if (EnrolmentTask.getBanWarnTypes().includes(accountTypes)
            && DeviceHelper.isAndroid() && DeviceHelper.isEnv()) {
            selectEnvironment(server);
        } else {
            const quickLaunchJson = this.getQuickLaunchJson();
            setTestConfig(quickLaunchJson);
            environmentPage.navigateFromDeenrolledPage(this.getQuickLaunchJson());
        }
    }

    proceedToLoginWithAccountTypes(accountTypes, access, businessType) {
        this.assignUser(accountTypes, access, businessType);
        // MQE-1075 TODO
        this.prepareAppForAccountTypes(accountTypes, EnrolmentTask.getBanWarnTypes());
        const data = this.dataRepository.get();
        if (!this.isEnrolled()) {
            const environmentPage = new EnvironmentPage();
            // when user is de-enrolled using backend, reenter test config
            environmentPage.navigateFromDeenrolledPage(this.getQuickLaunchJson());
            const welcomePage = new WelcomePage();
            welcomePage.navigateToLoginScreen();
            const loginPage = new LoginPage();
            // MQE-1075 TODO
            if (EnrolmentTask.getBanWarnTypes()
                .includes(accountTypes)) {
                loginPage.waitForOldLoginPage();
            } else {
                loginPage.waitForPageLoad();
            }
            loginPage.login(data.username, data.password);
        }
    }

    proceedToMIWithAccountTypes(accountTypes, access, businessType = null) {
        this.proceedToLoginWithAccountTypes(accountTypes, access, businessType);
        const memorableInformationPage = new MemorableInformationPage();
        memorableInformationPage.validatePageLoad();
    }

    static proceedToAppropriatePageForSingleBusiness() {
        const memorableInformationPage = new MemorableInformationPage();
        if (DeviceHelper.isRNGA() || (AccountMatcher.isSingleBusiness() && DeviceHelper.isBNGA())) {
            memorableInformationPage.proceedToPage(new HomePage());
            const homePage = new HomePage();
            homePage.dismissServerErrorIfPresent();
        } else {
            memorableInformationPage.proceedToPage(new BusinessSelectionPage());
        }
    }

    enterMI(type) {
        const memorableInformationPage = new MemorableInformationPage();
        memorableInformationPage.validatePageLoad();
        const data = this.dataRepository.get();
        if (type === 'correct') {
            memorableInformationPage.enterMemorableInformation(data.memorableInformation, 3);
        } else if (type === 'incorrect') {
            memorableInformationPage.enterMemorableInformation(incorrectMI, 3);
        } else if (type === '2 characters of') {
            memorableInformationPage.enterMemorableInformation(data.memorableInformation, 2);
        }
    }

    loginAsUserWithAccountTypes(accountTypes, access, businessType = null,
        byPassEia = browser.config.cucumberOpts.byPassEia) {
        this.proceedToMIWithAccountTypes(accountTypes, access, businessType);
        const memorableInformationPage = new MemorableInformationPage();
        const data = this.dataRepository.get();
        memorableInformationPage.enterMemorableInformation(data.memorableInformation, 3);
        const homePage = new HomePage();
        if (!this.isEnrolled() && (DeviceHelper.isRNGA()
            || (DeviceHelper.isBNGA() && this.dataRepository.assignedUser.miUser !== 'true'))) {
            if (DeviceHelper.isBNGA() && cardReaderRespondPage.shouldSeeCardReaderRespondPage()) {
                cardReaderRespondPage.waitForCardReaderRespondPage('respond');
            } else {
                const enrolmentPage = new EnrolmentPage();
                const server = Server.fromString(browser.capabilities.server);
                if (!server.isEiaBypassed && DeviceHelper.isEnv()) {
                    if (!byPassEia) {
                        enrolmentPage.selectPhoneNumber();
                    }
                }
                enrolmentPage.clickCallMeNowOption();
                if (!server.isEiaBypassed && DeviceHelper.isEnv()) {
                    if (byPassEia) {
                        enrolmentPage.updateEiaCallState(data);
                    } else {
                        enrolmentPage.attendEiaCallAndEnterFourDigitNumber();
                    }
                }
            }
            const enrolmentSuccessPage = new EnrolmentSuccessPage();
            enrolmentSuccessPage.waitForPageLoad();
            enrolmentSuccessPage.clickContinueOnEnrolmentPage();
            this.deviceState.setEnrolledUser(this.dataRepository.assignedUser.username);
            const enrolmentSecurityPage = new EnrolmentSecurityPage();
            enrolmentSecurityPage.waitForPageLoad();
            enrolmentSecurityPage.clickContinueOnSecurityEnrolmentPage();
            if (!DeviceHelper.isStub() || (DeviceHelper.isStub() && DeviceHelper.isBNGA())) {
                const fingerprintInterstitialPage = new FingerprintInterstitialPage();
                fingerprintInterstitialPage.waitForPageLoad();
                fingerprintInterstitialPage.optOut();
            }
            EnrolmentTask.proceedToAppropriatePageForSingleBusiness(accountTypes);
        } else if (DeviceHelper.isRNGA()) {
            memorableInformationPage.proceedToPage(new HomePage());
            const interstitialUsers = ['multiple', 'big', 'leads', 'fingerprint'];
            if (!_.some(interstitialUsers, ((value) => accountTypes.includes(value)))) {
                homePage.dismissServerErrorIfPresent();
                homePage.waitForPageLoad();
            }
        } else {
            EnrolmentTask.proceedToAppropriatePageForSingleBusiness(accountTypes);
        }
    }
};
