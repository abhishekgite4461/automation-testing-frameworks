const HomePage = require('../pages/home.page');
const BusinessSelectionPage = require('../pages/businessSelection.page');
const AuthPage = require('../pages/common/auth.page');
const MorePage = require('../pages/more.page');
const PersonalDetailsPage = require('../pages/settings/personalDetails/personalDetails.page.js');
const SettingsPage = require('../pages/settings/settings.page');

const businessSelectionPage = new BusinessSelectionPage();
const homePage = new HomePage();
const authPage = new AuthPage();
const morePage = new MorePage();
const personalDetailsPage = new PersonalDetailsPage();
const settingsPage = new SettingsPage();

module.exports = class NavigationTask {
    static navigateToBusiness(businessType, access) {
        if (businessType === 'MULTIPLE_BUSINESS') {
            businessSelectionPage.selectBusinessWithinAccessLevel(access);
        }
        homePage.validateHomePage();
    }

    static navigateToSecuritySettings() {
        NavigationTask.navigateToSettings();
        settingsPage.selectSecuritySettings();
    }

    static navigateToSettings() {
        homePage.selectTab('more');
        morePage.selectSettings();
    }

    static openMoreMenu() {
        authPage.selectTab('more');
    }

    static openSettingsFromMoreMenu() {
        authPage.selectTab('more');
        morePage.selectSettings();
    }

    static openYourProfileFromMoreMenu() {
        NavigationTask.openMoreMenu();
        morePage.selectYourProfile();
    }

    static navigateToProfile() {
        NavigationTask.openYourProfileFromMoreMenu();
        personalDetailsPage.waitForPageLoad();
    }

    static updatePhoneNumber() {
        NavigationTask.navigateToProfile();
        personalDetailsPage.selectMobileDetails();
    }
};
