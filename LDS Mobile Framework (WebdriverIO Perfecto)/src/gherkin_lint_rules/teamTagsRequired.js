const rule = 'team-tags-required';

const run = (feature) => {
    if (!feature.tags.some((tag) => tag.name.startsWith('@team-'))) {
        return [{
            message: 'All features must be tagged with at least one @team tag',
            rule,
            line: 0
        }];
    }
    return [];
};

module.exports = {
    name: rule,
    run
};
