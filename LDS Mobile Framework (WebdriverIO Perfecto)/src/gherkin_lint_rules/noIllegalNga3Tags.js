const Gherkin = require('@cucumber/gherkin');
const {IdGenerator, messages} = require('@cucumber/messages');
const _ = require('lodash');

const rule = 'no-illegal-nga3-tags';
let GherkinQuery;
const includesStubTag = (tagNames) => tagNames.some((tag) => tag.startsWith('@stub-'));

const checkScenario = (scenario) => {
    const tagNames = scenario.tags.map((tag) => tag.name);
    if (tagNames.includes('@nga3')
        && !(tagNames.includes('@manual')
            || tagNames.includes('@pending')
            || tagNames.includes('@environmentOnly')
            || tagNames.includes('@optFeature')
            || includesStubTag(tagNames))) {
        return [{
            message: 'All NGA3 tests must be @manual, @pending, @environmentOnly, e2eRegressionOnly or @stub-*',
            rule,
            line: GherkinQuery.getLocation(scenario.astNodeIds[0]).line
        }];
    }
    return [];
};

const run = (feature) => {
    const allEnvelopes = messages.Envelope.create({
        gherkinDocument: messages.GherkinDocument.create({feature})
    });
    GherkinQuery = new Gherkin.Query().update(allEnvelopes);
    const pickles = new Gherkin.compile({feature}, '', IdGenerator.incrementing());
    return _.flatMap(pickles, (scenario) => checkScenario(scenario));
};

module.exports = {
    name: rule,
    run
};
