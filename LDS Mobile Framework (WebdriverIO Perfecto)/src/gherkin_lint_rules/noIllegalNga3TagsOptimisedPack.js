const Gherkin = require('@cucumber/gherkin');
const {IdGenerator, messages} = require('@cucumber/messages');
const _ = require('lodash');

const rule = 'no-illegal-nga3-tags';
let GherkinQuery;

const illegalTags = [
    '@manual',
    '@manualOnly',
    '@pending',
    '@noExistingData',
    '@exhaustiveData',
    '@stubOnly',
    '@unEnrolled',
    '@genericAccount',
    '@otherAccount'
];

const checkScenario = (scenario) => {
    const tagNames = scenario.tags.map((tag) => tag.name);
    if (tagNames.includes('@optFeature') && tagNames.includes('@rnga')
        && !(illegalTags.some((tag) => tagNames.includes(tag)))) {
        return [{
            message: `All NGA3 optimised scenarios must contain @optFeature & one of the following tag ${illegalTags}`,
            rule,
            line: GherkinQuery.getLocation(scenario.astNodeIds[0]).line
        }];
    }
    return [];
};

const run = (feature) => {
    const allEnvelopes = messages.Envelope.create({
        gherkinDocument: messages.GherkinDocument.create({feature})
    });
    GherkinQuery = new Gherkin.Query().update(allEnvelopes);
    const pickles = new Gherkin.compile({feature}, '', IdGenerator.incrementing());
    return _.flatMap(pickles, (scenario) => checkScenario(scenario));
};

module.exports = {
    name: rule,
    run
};
