const _ = require('lodash');

const rule = 'no-switch-tags-on-scenarios';

const run = (feature) => {
    if (feature.children !== undefined) {
        return _.flatMap(feature.children, (child) => {
            if (!child.background) {
                if (child.scenario.tags !== undefined) {
                    return _.flatMap(child.scenario.tags, (tag) => {
                        if (tag.name.startsWith('@sw-')) {
                            return [{
                                message: `Switch tags are illegal on scenario definitions: ${tag.name}`,
                                rule,
                                line: tag.location.line
                            }];
                        }
                        return [];
                    });
                }
            }
            return [];
        });
    }
    return [];
};

module.exports = {
    name: rule,
    run
};
