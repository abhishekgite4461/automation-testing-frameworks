const Gherkin = require('@cucumber/gherkin');
const {IdGenerator, messages} = require('@cucumber/messages');
const _ = require('lodash');

const rule = 'no-illegal-tag-combinations';
let GherkinQuery;
// More than one is allowed as long as not all of the tags are present
const illegalTagCombinationsInclusive = [
    {
        combination: ['@android', '@ios'],
        reason: 'You can\'t restrict to "only ios" and "only android" - instead remove both tags.'
    },
    {
        combination: ['@lds', '@hfx', '@bos'],
        reason: 'You can\'t restrict to all three brands - instead use no brand tags.'
    },
    {
        combination: ['@manual', '@environment'],
        reason: 'A test cannot be manual and be automated against a real environment'
    },
    {
        combination: ['@environmentOnly', '@stubOnly'],
        reason: 'A test cannot be run only on stubs and only on a real environment'
    }
];

// Only one of each tag is allowed
const illegalTagCombinationsExclusive = [
    {
        combination: ['@fingerprintScannerAndFingerprint', '@fingerprintScannerNoFingerprint', '@fingerprintNoScanner'],
        reason: 'A scenario cannot have more than one fingerprint tag'
    }
];

// At least one must be present for each scenario
const necessaryTags = [
    {
        options: ['@nga3'],
        reason: 'All tests must target nga3'
    },
    {
        options: ['@bnga', '@rnga'],
        reason: 'All tests must target bnga, or rnga, or both'
    }
];

const checkScenario = (scenario) => {
    const scenarioHasTag = (tag) => scenario.tags.map((t) => t.name).includes(tag);

    return _.flatMap(illegalTagCombinationsInclusive, (illegalCombination) => {
        if (_.every(illegalCombination.combination.map((tag) => scenarioHasTag(tag)))) {
            return [
                {
                    message: 'It\'s illegal to use the following tag combination on the '
                        + `same test case: ${illegalCombination.combination.join(', ')}.\n`
                        + `This is because: ${illegalCombination.reason}`,
                    rule,
                    line: GherkinQuery.getLocation(scenario.astNodeIds[0]).line
                }];
        }
        return [];
    }).concat(_.flatMap(illegalTagCombinationsExclusive, ((illegalCombination) => {
        if (_.filter(illegalCombination.combination.map((tag) => scenarioHasTag(tag))).length > 1) {
            return [
                {
                    message: 'It\'s illegal to use more than one of the following tags on the '
                        + `same test case: ${illegalCombination.combination.join(', ')}.\n`
                        + `This is because: ${illegalCombination.reason}`,
                    rule,
                    line: GherkinQuery.getLocation(scenario.astNodeIds[0]).line
                }];
        }
        return [];
    }))).concat(_.flatMap(necessaryTags, ((necessaryCombination) => {
        if (!_.some(necessaryCombination.options.map((tag) => scenarioHasTag(tag)))) {
            return [
                {
                    message: 'At least one of the following tags is required: '
                        + `${necessaryCombination.options.join(', ')}, this is because: `
                        + `${necessaryCombination.reason}`,
                    rule,
                    line: GherkinQuery.getLocation(scenario.astNodeIds[0]).line
                }];
        }
        return [];
    })));
};

const run = (feature) => {
    const allEnvelopes = messages.Envelope.create({
        gherkinDocument: messages.GherkinDocument.create({feature})
    });
    GherkinQuery = new Gherkin.Query().update(allEnvelopes);
    const pickles = new Gherkin.compile({feature}, '', IdGenerator.incrementing());
    return _.flatMap(pickles, (scenario) => checkScenario(scenario));
};

module.exports = {
    name: rule,
    run
};
