const _ = require('lodash');

const tagList = require('./tagList');
const teams = require('../enums/teams.enum');
const RetailSwitch = require('../enums/RNGA/switch.enum');
const BusinessSwitch = require('../enums/BNGA/switch.enum');
const FeatureFlagEnum = require('../enums/feature.enum');
const AbTestEnum = require('../enums/abTest.enum');
const DataConsentEnum = require('../enums/dataConsent.enum');
const Enum = require('../enums/enum');

const Switch = new Enum({...RetailSwitch, ...BusinessSwitch});
const FeatureFlags = new Enum({...FeatureFlagEnum});
const AbTest = new Enum({...AbTestEnum});
const Stub = require('../enums/stub.enum');

const rule = 'no-unknown-tags';

const teamTags = teams.symbols().map((team) => team.tag);

/**
 * All tags must start with @sw- or @stub-, be a team tag, or be present in the tag list.
 */
const verifyNoUnknownTags = (tags) => _.flatMap(tags, (tag) => {
    if (tag.name.startsWith('@sw-')) {
        try {
            Switch.fromString(tag.name.slice(4)
                .replace('-on', '')
                .replace('-off', '')
                .toUpperCase()
                .replace(/-/g, '_'));
        } catch (err) {
            return [{
                message: 'The following switch tag is not recognised, '
                + `please correct or add to the switch list: ${tag.name}`,
                rule,
                line: tag.location.line
            }];
        }
    } else if (tag.name.startsWith('@stub-')) {
        try {
            Stub.fromString(tag.name.slice(6).toUpperCase().replace(/-/g, '_'));
        } catch (err) {
            return [{
                message: 'The following stub tag is not recognised, '
                + `please correct or add to the stubs list: ${tag.name}`,
                rule,
                line: tag.location.line
            }];
        }
    } else if (tag.name.startsWith('@ff-')) {
        try {
            FeatureFlags.fromString(tag.name.slice(4)
                .replace('-on', '')
                .replace('-off', '')
                .toUpperCase()
                .replace(/-/g, '_'));
        } catch (err) {
            return [{
                message: 'The following feature flag tag is not recognised, '
                + `please correct or add to the feature flag list: ${tag.name}`,
                rule,
                line: tag.location.line
            }];
        }
    } else if (tag.name.startsWith('@ab-')) {
        try {
            AbTest.fromString(tag.name.slice(4)
                .split('~')[0]
                .toUpperCase()
                .replace(/-/g, '_'));
        } catch (err) {
            return [{
                message: 'The following ab test tag is not recognised, '
                + `please correct or add to the ab test list: ${tag.name}`,
                rule,
                line: tag.location.line
            }];
        }
    } else if (tag.name.startsWith('@dc-')) {
        try {
            DataConsentEnum.fromString(tag.name.slice(4)
                .replace('-on', '')
                .replace('-off', '')
                .toUpperCase()
                .replace(/-/g, '_'));
        } catch (err) {
            return [{
                message: 'The following data consnet tag is not recognised, '
                + `please correct or add to the feature flag list: ${tag.name}`,
                rule,
                line: tag.location.line
            }];
        }
    } else if (!(tag.name in tagList)
        && !(teamTags.includes(tag.name))) {
        return [{
            message: `The following tag is not recognised, please correct or add to the tag list: ${tag.name}`,
            rule,
            line: tag.location.line
        }];
    }
    return [];
});

function noUnknownTags(feature) {
    const errors = [];
    if (feature.tags !== undefined) {
        errors.push(...verifyNoUnknownTags(feature.tags));
    }
    if (feature.children !== undefined) {
        errors.push(..._.flatMap(feature.children, (child) => {
            if (!child.background) {
                if (child.scenario.tags !== undefined) {
                    return verifyNoUnknownTags(child.scenario.tags);
                }
            }
            return [];
        }));
    }
    return errors;
}

module.exports = {
    name: rule,
    run: noUnknownTags
};
