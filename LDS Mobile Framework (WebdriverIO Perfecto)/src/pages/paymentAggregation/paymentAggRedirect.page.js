require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');

const expectedErrMsgForSomethingWentWrong = 'something went wrong';
const expectedMSgForAuthenticationInProgress = 'isn\'t finished yet';
const expectedErrMsgForUnsupportedProvider = 'can\'t connect you to ';

class PaymentHubPage extends AuthPage {
    get paymentAggRedirectScreen() {
        return this.getNativeScreen('paymentAggregation/redirect.screen');
    }

    waitForErrorScreenRedirectExternalBrowser() {
        if (DeviceHelper.isAndroid()) {
            Actions.pause(5000);
        }
        Actions.getText(this.paymentAggRedirectScreen.redirectScreenTitlePaymentAgg)
            .toLowerCase().should.contains(expectedErrMsgForSomethingWentWrong);
    }

    verifyAuthenticationNotCompletedMessage() {
        Actions.getText(this.paymentAggRedirectScreen.redirectScreenAuthenticationInProgressTitle)
            .toLowerCase().should.contains(expectedMSgForAuthenticationInProgress);
    }

    verifyRedirectScreenMessage(bankName) {
        Actions.isExisting(this.paymentAggRedirectScreen.redirectLoadingProgressPayAgg).should.equals(true);
        Actions.getAttributeText(this.paymentAggRedirectScreen.redirectTextPayAgg).toLowerCase()
            .should.contains(bankName.toLowerCase());
    }

    waitForNotSupportedPISPErrorScreen(bankName) {
        if (DeviceHelper.isAndroid()) {
            Actions.pause(5000);
        }
        Actions.getText(this.paymentAggRedirectScreen.redirectScreenTitlePaymentAgg)
            .should.contains(expectedErrMsgForUnsupportedProvider + bankName);
    }
}
module.exports = PaymentHubPage;
