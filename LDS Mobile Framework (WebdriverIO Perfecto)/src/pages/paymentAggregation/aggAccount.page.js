require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const CommonPage = require('../common.page');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const DeviceHelper = require('../../utils/device.helper');

const commonPage = new CommonPage();

const nonEligibleExternalFromAccountList = [];
const allAvailableExternalAccountsInAOVPage = [];

class PaymentHubPage extends AuthPage {
    get accountsOverviewScreen() {
        return this.getNativeScreen('accountsOverview.screen');
    }

    get paymentHubMainScreen() {
        return this.getNativeScreen('paymentHub/paymentHubMain.screen');
    }

    waitForPaymentHubPageLoad() {
        commonPage.waitForSpinnerToComplete();
        Actions.waitForVisible(this.paymentHubMainScreen.title);
    }

    getListOfExternalAccounts() {
        this.findAllExternalAccountsInCurrentView();
        this.findNonEligibleExternalAccountsInCurrentView();

        // Keep scrolling down and update allAvailableExternalAccountsInAOVPage and nonEligibleExternalFromAccountList
        while (!Actions.isVisible(this.accountsOverviewScreen.openBankingPromoTile)) {
            Actions.swipeByPercentage(50, 50, 30, 30, 50);
            this.findAllExternalAccountsInCurrentView();
            this.findNonEligibleExternalAccountsInCurrentView();
        }
        // Extract the account number, as the text retrieved will be in the format 'Account number. XXXXXXXX' in iOS
        for (let i = 0; i < allAvailableExternalAccountsInAOVPage.length; i++) {
            allAvailableExternalAccountsInAOVPage[i] = allAvailableExternalAccountsInAOVPage[i].replace('Account number. ', '');
        }
        for (let i = 0; i < nonEligibleExternalFromAccountList.length; i++) {
            nonEligibleExternalFromAccountList[i] = nonEligibleExternalFromAccountList[i].replace('Account number. ', '');
        }
        // Remove non eligible external accounts that are not expected in 'From' dropdown in Payment Hub page
        for (let i = 0; i < allAvailableExternalAccountsInAOVPage.length; i++) {
            if (nonEligibleExternalFromAccountList.includes(allAvailableExternalAccountsInAOVPage[i])) {
                allAvailableExternalAccountsInAOVPage.splice(i, 1);
            }
        }
        SavedAppDataRepository.setData(SavedData.EXTERNAL_ACCOUNT_LIST, allAvailableExternalAccountsInAOVPage);
    }

    findAllExternalAccountsInCurrentView() {
        if (Actions.isVisible(this.accountsOverviewScreen.externalAccountNumberList)) {
            const allAccountsInCurrentView = Actions
                .getElementsListAttributeText(this.accountsOverviewScreen.externalAccountNumberList);
            allAccountsInCurrentView.forEach((accountNumber) => {
                if (!allAvailableExternalAccountsInAOVPage.includes(accountNumber)) {
                    allAvailableExternalAccountsInAOVPage.push(accountNumber);
                }
            });
        }
    }

    findNonEligibleExternalAccountsInCurrentView() {
        if (Actions.isExisting(this.accountsOverviewScreen.nonEligibleExternalFromAccountList)) {
            const allNonEligibleAccountsInCurrentView = Actions
                .getElementsListAttributeText(this.accountsOverviewScreen.nonEligibleExternalFromAccountList);
            allNonEligibleAccountsInCurrentView.forEach((accountNumber) => {
                if (!nonEligibleExternalFromAccountList.includes(accountNumber)) {
                    nonEligibleExternalFromAccountList.push(accountNumber);
                }
            });
        }
    }

    fromAccountListValidate(isShown) {
        Actions.click(this.paymentHubMainScreen.clickFrom);
        const externalAccountList = SavedAppDataRepository.getData(SavedData.EXTERNAL_ACCOUNT_LIST);
        const fromAccounts = Actions.getElementsListAttributeText(this.paymentHubMainScreen.fromAccountList);
        Actions.swipeByPercentage(80, 80, 30, 30, 50);
        if (Actions.getElementsListAttributeText(this.paymentHubMainScreen.fromAccountList) != null) {
            Actions.getElementsListAttributeText(this.paymentHubMainScreen.fromAccountList).forEach((accountNumber) => {
                if (!fromAccounts.includes(accountNumber)) {
                    fromAccounts.push(accountNumber);
                }
            });
            if (isShown === 'not see') {
                externalAccountList.forEach((account) => {
                    if (fromAccounts.includes(account)) {
                        throw new Error('External account Visible');
                    }
                });
            } else if (isShown === 'see') {
                externalAccountList.forEach((account) => {
                    if (!fromAccounts.includes(account)) {
                        throw new Error('External account not Visible');
                    }
                });
            }
        }
    }

    selectFromExternalAccount(aggBank, accAccountNumber) {
        if (Actions.isVisible(this.paymentHubMainScreen.clickFrom)) {
            Actions.click(this.paymentHubMainScreen.clickFrom);
        }
        Actions.swipeByPercentUntilVisible(this.paymentHubMainScreen
            .fromExternalAccount(aggBank, accAccountNumber), 60, 60, 30, 30, 50, 5);
        Actions.click(this.paymentHubMainScreen.fromExternalAccount(aggBank, accAccountNumber));
    }

    selectDebitorAccountBySortCodeAndAccountNumber(accountSortCode, accountNumber) {
        if (Actions.isVisible(this.paymentHubMainScreen.clickFrom)) {
            Actions.click(this.paymentHubMainScreen.clickFrom);
        }
        Actions.swipeByPercentUntilVisible(this.paymentHubMainScreen
            .sortCodeAndAccountNumber(accountSortCode, accountNumber), 60, 60, 30, 30, 50, 5);
        Actions.click(this.paymentHubMainScreen
            .sortCodeAndAccountNumber(accountSortCode, accountNumber));
    }

    selectCreditorAccountBySortCodeAndAccountNumber(accountSortCode, accountNumber) {
        if (Actions.isVisible(this.paymentHubMainScreen.clickTo)) {
            Actions.click(this.paymentHubMainScreen.clickTo);
        }
        Actions.swipeByPercentUntilVisible(this.paymentHubMainScreen
            .sortCodeAndAccountNumber(accountSortCode, accountNumber), 60, 60, 30, 30, 50, 5);
        Actions.click(this.paymentHubMainScreen
            .sortCodeAndAccountNumber(accountSortCode, accountNumber));
    }

    shouldSeeFromExternalAccount(accountSortCode, aggAccountNumber) {
        Actions.isVisible(this.paymentHubMainScreen
            .sortCodeAndAccountNumber(accountSortCode, aggAccountNumber)).should.equals(true);
    }

    selectToAccountList() {
        Actions.click(this.paymentHubMainScreen.clickTo);
    }

    shouldNotSeeFromExternalAccount() {
        Actions.isVisible(this.paymentHubMainScreen.fromExternalAccount).should.equal(false);
    }

    shouldSeeCurrentAccount(accountType) {
        Actions.isVisible(this.paymentHubMainScreen.eligibleOwnAccount(accountType)).should.equal(true);
    }

    toExternalAccountSelect(aggBank, aggAccountNumber) {
        Actions.click(this.paymentHubMainScreen.selectToAccount(aggBank, aggAccountNumber));
    }

    selectedToAccount(accountSortCode, aggAccountNumber) {
        Actions.isVisible(this.paymentHubMainScreen
            .sortCodeAndAccountNumber(accountSortCode, aggAccountNumber)).should.equals(true);
    }

    shouldNotSeeHelpTextForAmount() {
        if (DeviceHelper.isAndroid()) {
            Actions.hideDeviceKeyboard();
        }
        Actions.isVisible(this.paymentHubMainScreen.helpTextAmount).should.equal(false);
    }

    selectEligibleOwnAccount(eligibleOwnAccount) {
        Actions.click(this.paymentHubMainScreen.eligibleOwnAccount(eligibleOwnAccount));
    }
}

module.exports = PaymentHubPage;
