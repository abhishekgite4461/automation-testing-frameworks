const _ = require('lodash');
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class searchLandingPage extends AuthPage {
    get searchLandingScreen() {
        return this.getNativeScreen('inAppSearch/searchLanding.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.searchLandingScreen.title);
    }

    selectSearchIcon() {
        Actions.click(this.searchLandingScreen.searchIcon);
    }

    selectInAppSearchBar() {
        Actions.click(this.searchLandingScreen.searchBar);
    }

    shouldSeeSearchBar() {
        Actions.isVisible(this.searchLandingScreen.searchBar)
            .should
            .equal(true);
    }

    enterKeywordInAppSearchBox(keyword) {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.searchLandingScreen.searchIcon);
            Actions.setImmediateValue(this.searchLandingScreen.searchBar, keyword);
        } else {
            Actions.setImmediateValue(this.searchLandingScreen.searchBar, keyword);
            Actions.click(this.searchLandingScreen.searchButton);
        }
    }

    shouldSeeSearchResult(searchResult) {
        const searchResultName = _.lowerFirst(_.startCase(_.toLower(searchResult))).replace(/\W+/g, '');
        Actions.swipeUntilVisible(SwipeAction.UP,
            this.searchLandingScreen[`${searchResultName}Link`]);
        Actions.isVisible(this.searchLandingScreen[`${searchResultName}Link`])
            .should
            .equal(true);
    }

    selectSearchResult(searchResult) {
        const searchResultName = _.lowerFirst(_.startCase(_.toLower(searchResult))).replace(/\W+/g, '');
        Actions.swipeUntilVisible(SwipeAction.UP,
            this.searchLandingScreen[`${searchResultName}Link`]);
        Actions.click(this.searchLandingScreen[`${searchResultName}Link`]);
    }

    shouldSeeSearchResultLandingPage(searchedPage) {
        const searchResultPage = _.lowerFirst(_.startCase(_.toLower(searchedPage))).replace(/\W+/g, '');
        Actions.isVisible(this.searchLandingScreen[`${searchResultPage}Page`])
            .should
            .equal(true);
    }

    validateSearchResultsForLoan(searchList) {
        for (let i = 0; i < searchList.length; i++) {
            Actions.sendKeys(this.searchLandingScreen.searchBar, searchList[i].fields);
            Actions.isVisible(this.searchLandingScreen.loanRepaymentHolidayLink)
                .should
                .equal(true);
            Actions.click(this.searchLandingScreen.searchClear);
        }
    }

    validateSearchResultsForMortgage(searchList) {
        for (let i = 0; i < searchList.length; i++) {
            Actions.sendKeys(this.searchLandingScreen.searchBar, searchList[i].fields);
            Actions.isVisible(this.searchLandingScreen.loanRepaymentHolidayLink)
                .should
                .equal(true);
            Actions.click(this.searchLandingScreen.searchClear);
        }
    }

    shouldSeeNoResultFoundPage() {
        Actions.isVisible(this.searchLandingScreen.searchSuggestions).should.equal(true);
        Actions.isVisible(this.searchLandingScreen.NeedMoreHelpTitle).should.equal(true);
        Actions.isVisible(this.searchLandingScreen.messageUsButton).should.equal(true);
        Actions.isVisible(this.searchLandingScreen.helpAndSupportButton).should.equal(true);
    }

    selectHelpAndSupportButton() {
        Actions.click(this.searchLandingScreen.helpAndSupportButton);
    }

    shouldSeeSupportHubPage() {
        Actions.isVisible(this.searchLandingScreen.supportHubPage).should.equal(true);
    }

    shouldSeeSuggestedResultPage() {
        Actions.isVisible(this.searchLandingScreen.makePaymentLink).should.equal(true);
    }
}

module.exports = searchLandingPage;
