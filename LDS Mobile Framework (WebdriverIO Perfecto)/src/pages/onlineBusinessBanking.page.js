const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class OnlineBusinessBankingPage extends AuthPage {
    /**
     * @returns {OnlineBusinessBankingPage}
     */
    get onlineBusinessBankingScreen() {
        return this.getWebScreen('onlineBusinessBanking.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.onlineBusinessBankingScreen.title);
    }
}

module.exports = OnlineBusinessBankingPage;
