require('chai').should();
const UnAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');

class ActivationCodePage extends UnAuthPage {
    get activationCodeScreen() {
        return this.getNativeScreen('ibRegistration/activationCode.screen');
    }

    verifyEnterActivationCodeTitle() {
        Actions.waitForVisible(this.activationCodeScreen.title);
    }

    enterActivationCode(activationCode) {
        Actions.click(this.activationCodeScreen.activationCode);
        Actions.setImmediateValueAndTapOut(this.activationCodeScreen.activationCode, activationCode);
    }

    validateActivationCode(expectedActivationCode) {
        Actions.getText(this.activationCodeScreen.activationCode)
            .should.equal(expectedActivationCode);
    }

    isEnabledConfirmCode(flag) {
        Actions.isEnabled(this.activationCodeScreen.confirmCode)
            .should.equal(flag);
    }

    clickConfirmCode() {
        const selector = this.activationCodeScreen.confirmCode;
        Actions.click(selector);
    }

    clickRequestNewCode() {
        Actions.click(this.activationCodeScreen.requestNewCode);
        Actions.hideDeviceKeyboard();
    }

    validateErrorMessage(expectedErrorMessage) {
        Actions.getText(this.activationCodeScreen.errorMessage)
            .should.include(expectedErrorMessage);
    }
}

module.exports = ActivationCodePage;
