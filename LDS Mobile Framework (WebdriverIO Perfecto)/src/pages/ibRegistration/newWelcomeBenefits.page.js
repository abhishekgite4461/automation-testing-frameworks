require('chai').should();
const unAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class NewWelcomeBenefitsPage extends unAuthPage {
    get newWelcomeBenefitsScreen() {
        return this.getNativeScreen('ibRegistration/newWelcomeBenefits.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.newWelcomeBenefitsScreen.title, 30000);
    }

    startRegistration() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.newWelcomeBenefitsScreen.registerButton, 0, false, 9);
        Actions.click(this.newWelcomeBenefitsScreen.registerButton);
    }

    startIBRegistration() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.newWelcomeBenefitsScreen.registerForIbButton, 0, false, 9);
        Actions.click(this.newWelcomeBenefitsScreen.registerForIbButton);
    }

    goToLogin() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.newWelcomeBenefitsScreen.logOnButton);
        Actions.click(this.newWelcomeBenefitsScreen.logOnButton);
    }

    selectLegalInfo() {
        Actions.swipeByPercentUntilVisible(this.newWelcomeBenefitsScreen.legalInfoButton);
        Actions.click(this.newWelcomeBenefitsScreen.legalInfoButton);
    }
}

module.exports = NewWelcomeBenefitsPage;
