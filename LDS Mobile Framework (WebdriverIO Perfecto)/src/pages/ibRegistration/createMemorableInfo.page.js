require('chai').should();
const UnAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');

const oldAndroidVersion = () => (DeviceHelper.isAndroid() && String(parseFloat(DeviceHelper.osVersion())).split('.')[0] <= 6);
class CreateMemorableInfoPage extends UnAuthPage {
    get createMemorableInfoScreen() {
        return this.getNativeScreen('ibRegistration/createMemorableInfo.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.createMemorableInfoScreen.title);
    }

    verifyCreateMemorableInfoTitle() {
        Actions.isVisible(this.createMemorableInfoScreen.title);
    }

    selectMemorableInfo() {
        Actions.click(this.createMemorableInfoScreen.memorableInfo);
    }

    enterMemorableInfo(memorableInfo) {
        Actions.click(this.createMemorableInfoScreen.memorableInfo);
        Actions.genericSendKeys(this.createMemorableInfoScreen.memorableInfo, memorableInfo);
        Actions.hideDeviceKeyboard();
    }

    selectConfirmMemorableInfo() {
        Actions.click(this.createMemorableInfoScreen.confirmMemorableInfo);
    }

    enterConfirmMemorableInfo(confirmMI) {
        Actions.click(this.createMemorableInfoScreen.confirmMemorableInfo);
        // updated this method for custom keyboard
        for (let i = 0; i < confirmMI.length; i++) {
            Actions.setMICustomKeyoard(confirmMI[i]);
        }
    }

    enterCreateMemorableInformation(memorableInfo, confirmMemorableInfo) {
        Actions.click(this.createMemorableInfoScreen.memorableInfo);
        this.enterMemorableInfo(memorableInfo);
        Actions.tapOutOfField();
        Actions.click(this.createMemorableInfoScreen.confirmMemorableInfo);
        this.enterConfirmMemorableInfo(confirmMemorableInfo);
        Actions.tapOutOfField();
        Actions.hideDeviceKeyboard();
    }

    clearMemorableInfo() {
        Actions.deleteCharacter(this.createMemorableInfoScreen.customeKeyboardDeleteButton,
            (Actions.getText(this.createMemorableInfoScreen.memorableInfo)).length);
    }

    clearConfirmMemorableInfo() {
        Actions.deleteCharacter(this.createMemorableInfoScreen.customeKeyboardDeleteButton,
            (Actions.getText(this.createMemorableInfoScreen.confirmMemorableInfo)).length);
    }

    isEnabledConfirmMemorableInfo(isEnable) {
        Actions.isEnabled(this.createMemorableInfoScreen.confirmMemorableInfo)
            .should.equal(isEnable);
    }

    // MQE-131: masked text value is not returned on android < 7
    validateMaskedMemorableInfo(isSecure) {
        if (!oldAndroidVersion()) {
            const isMasked = Actions.getAttributeText(this.createMemorableInfoScreen.memorableInfo)
                .includes('•');
            isMasked.should.equal(isSecure);
        }
    }

    // MQE-131: masked text value is not returned on android < 7
    validateMaskedConfirmMemorableInfo(isSecure) {
        if (!oldAndroidVersion()) {
            const isMasked = Actions.getAttributeText(this.createMemorableInfoScreen.confirmMemorableInfo)
                .includes('•');
            isMasked.should.equal(isSecure);
        }
    }

    selectHideShowMemorableInfo() {
        Actions.click(this.createMemorableInfoScreen.hideShowMemorableInfo);
    }

    selectHideShowConfirmMemorableInfo() {
        Actions.click(this.createMemorableInfoScreen.hideShowConfirmMemorableInfo);
    }

    selectFindOutMore() {
        Actions.hideDeviceKeyboard();
        Actions.click(this.createMemorableInfoScreen.findOutMoreMemorableInfo);
    }

    validateMemorableInfoTipDialog() {
        Actions.isVisible(this.createMemorableInfoScreen.findOutMoreTitle);
    }

    closeFindOutMoreTip() {
        Actions.click(this.createMemorableInfoScreen.findOutMoreTipClose);
    }

    validateMemorableInfo(expectedMI) {
        Actions.getText(this.createMemorableInfoScreen.memorableInfo).should.equal(expectedMI);
    }

    // MQE-131: masked text value is not returned on android < 7
    validateMaxCharactersMemorableInfo(maxMILength) {
        if (!oldAndroidVersion()) {
            Actions.getText(this.createMemorableInfoScreen.memorableInfo).length.toString()
                .should.equal(maxMILength);
        }
    }

    validateMemorableInfoTip() {
        if (!Actions.isExisting(this.createMemorableInfoScreen.memorableInfoTip)) {
            Actions.hideDeviceKeyboard();
        }
        Actions.isExisting(this.createMemorableInfoScreen.memorableInfoTip)
            .should.equal(true);
    }

    validateConfirmMemorableInfoTip() {
        Actions.hideDeviceKeyboard();
        Actions.isExisting(this.createMemorableInfoScreen.confirmMemorableInfoTip)
            .should.equal(true);
    }

    selectNext() {
        Actions.click(this.createMemorableInfoScreen.nextButton);
    }
}

module.exports = CreateMemorableInfoPage;
