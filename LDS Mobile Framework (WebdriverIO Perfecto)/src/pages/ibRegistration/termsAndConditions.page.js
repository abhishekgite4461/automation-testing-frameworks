const UnAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');

class TermsAndConditionsPage extends UnAuthPage {
    get termsAndConditionsScreen() {
        return this.getNativeScreen('ibRegistration/termsAndConditions.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.termsAndConditionsScreen.title);
    }

    agreeTermsAndConditions() {
        Actions.waitForEnabled(this.termsAndConditionsScreen.agreeButton);
        Actions.isEnabled(this.termsAndConditionsScreen.footerSection);
        Actions.click(this.termsAndConditionsScreen.agreeButton);
    }

    clickBack() {
        Actions.click(this.termsAndConditionsScreen.backButton);
    }

    verifyFooterSection() {
        Actions.isEnabled(this.termsAndConditionsScreen.footerSection);
    }
}

module.exports = TermsAndConditionsPage;
