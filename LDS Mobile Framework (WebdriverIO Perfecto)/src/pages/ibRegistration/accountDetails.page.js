require('chai').should();
const UnAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');
const AccountType = require('../../enums/accountType.enum');

const CREDIT_CARD_MAX_LENGTH = 16;
const LOAN_NUMBER_MAX_LENGTH = 12;
const MORTGAGE_NUMBER_MAX_LENGTH = 14;

class AccountDetailsPage extends UnAuthPage {
    get accountDetailsScreen() {
        return this.getNativeScreen('ibRegistration/accountDetails.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.accountDetailsScreen.title);
    }

    verifyAccountDetailsTitle() {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.accountDetailsScreen.title);
    }

    selectAccountType(accountType) {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.accountDetailsScreen.accountType);

            switch (accountType) {
            case AccountType.CURRENT_SAVINGS:
                Actions.click(this.accountDetailsScreen.savingsSelector);
                break;

            case AccountType.LOAN:
                Actions.click(this.accountDetailsScreen.loanSelector);
                break;

            case AccountType.MORTGAGE:
                Actions.click(this.accountDetailsScreen.mortgageSelector);
                break;

            case AccountType.CREDIT_CARD:
                Actions.click(this.accountDetailsScreen.creditCardsSelector);
                break;

            default:
                throw new Error(`The account ${accountType} you have provided is not valid.`);
            }
        } else {
            Actions.click(this.accountDetailsScreen.accountType);
            const array = AccountType.asArray();
            Actions.setIOSPickerWheelValue(this.accountDetailsScreen.accountTypeSelector,
                accountType.value, array);
            Actions.tapOutOfField();
        }
    }

    enterAccountNumber(accountNumber) {
        Actions.click(this.accountDetailsScreen.accountNumber);
        Actions.setImmediateValueAndTapOut(this.accountDetailsScreen.accountNumber, accountNumber);
        Actions.hideDeviceKeyboard();
    }

    clearAccountNumber() {
        Actions.clearText(this.accountDetailsScreen.accountNumber);
    }

    enterSortCodeFullDetails(sortCode) {
        const arrayedSortCode = {
            firstBox: sortCode.substring(0, 2),
            secondBox: sortCode.substring(2, 4),
            thirdBox: sortCode.substring(4)
        };
        Actions.sendKeys(this.accountDetailsScreen.sortcodeFirstSection, arrayedSortCode.firstBox);
        Actions.click(this.accountDetailsScreen.sortcodeSecondSection);
        Actions.sendKeys(this.accountDetailsScreen.sortcodeSecondSection, arrayedSortCode.secondBox);
        Actions.click(this.accountDetailsScreen.sortcodeThirdSection);
        Actions.sendKeys(this.accountDetailsScreen.sortcodeThirdSection, arrayedSortCode.thirdBox);
    }

    enterSortCode(sortCode) {
        if (!DeviceHelper.isPerfecto() || (DeviceHelper.isAndroid() && DeviceHelper.isPerfecto())) {
            this.enterSortCodeFullDetails(sortCode);
        } else {
            Actions.sendKeys(this.accountDetailsScreen.sortcodeSection, sortCode);
        }
    }

    enterLoanReferenceNumber(loanReferenceNumber) {
        Actions.setImmediateValueAndTapOut(this.accountDetailsScreen.loanReferenceNumber, loanReferenceNumber);
        Actions.hideDeviceKeyboard();
    }

    enterMortgageNumber(mortgageNumber) {
        Actions.setImmediateValueAndTapOut(this.accountDetailsScreen.mortgageNumber, mortgageNumber);
        Actions.hideDeviceKeyboard();
    }

    enterCreditCardNumber(creditCardNumber) {
        Actions.setImmediateValueAndTapOut(this.accountDetailsScreen.creditCardNumber, creditCardNumber);
        Actions.hideDeviceKeyboard();
    }

    enterAccountInformation(accountType, accountNumber, sortCode) {
        this.selectAccountType(accountType);
        this.enterDetails(accountType, accountNumber, sortCode);
    }

    enterDetails(accountType, accountNumber, sortCode) {
        switch (accountType) {
        case AccountType.CURRENT_SAVINGS:
            this.enterSortCode(sortCode);
            this.enterAccountNumber(accountNumber);
            break;

        case AccountType.LOAN:
            this.enterLoanReferenceNumber(accountNumber);
            break;

        case AccountType.MORTGAGE:
            this.enterMortgageNumber(accountNumber);
            break;

        case AccountType.CREDIT_CARD:
            this.enterCreditCardNumber(accountNumber);
            break;

        default:
            throw new Error(`The account ${accountType} you have provided is not valid.`);
        }
    }

    clickNext() {
        Actions.click(this.accountDetailsScreen.nextButton);
    }

    clickBack() {
        Actions.click(this.accountDetailsScreen.backButton);
    }

    clickAccountNumber() {
        Actions.tapOutOfField();
        Actions.click(this.accountDetailsScreen.accountNumber);
    }

    clickSortCode() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.accountDetailsScreen.sortcodeSection);
        } else {
            Actions.click(this.accountDetailsScreen.sortcodeThirdSection);
        }
    }

    clearSortCode() {
        if (DeviceHelper.isIOS()) {
            Actions.clearText(this.accountDetailsScreen.sortcodeSection);
        } else {
            Actions.clearText(this.accountDetailsScreen.sortcodeThirdSection);
            Actions.click(this.accountDetailsScreen.sortcodeSecondSection);
            Actions.clearText(this.accountDetailsScreen.sortcodeSecondSection);
            Actions.click(this.accountDetailsScreen.sortcodeFirstSection);
            Actions.clearText(this.accountDetailsScreen.sortcodeFirstSection);
        }
    }

    clickMortgageNumber() {
        Actions.click(this.accountDetailsScreen.mortgageNumber);
    }

    clearMortgageNumber() {
        Actions.clearText(this.accountDetailsScreen.mortgageNumber);
    }

    clickLoanNumber() {
        Actions.click(this.accountDetailsScreen.loanReferenceNumber);
    }

    clearLoanNumber() {
        Actions.clearText(this.accountDetailsScreen.loanReferenceNumber);
    }

    clickCreditCard() {
        Actions.click(this.accountDetailsScreen.creditCardNumber);
    }

    clearCreditCard() {
        Actions.clearText(this.accountDetailsScreen.creditCardNumber);
    }

    isEnabledNextButton(enabled) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.accountDetailsScreen.nextButton);
        Actions.isEnabled(this.accountDetailsScreen.nextButton)
            .should.equal(enabled);
    }

    validateMortgageNumber(expectedMortgageNumber) {
        AccountDetailsPage.getFieldValue(this.accountDetailsScreen.mortgageNumber)
            .should.equal(expectedMortgageNumber);
    }

    validateCreditCardNumber(expectedCreditCardNumber) {
        AccountDetailsPage.getFieldValue(this.accountDetailsScreen.creditCardNumber).replace(/\s+/g, '')
            .should.equal(expectedCreditCardNumber);
    }

    validateMortgageTip(expectedMortgageTip) {
        Actions.getText(this.accountDetailsScreen.mortgageTip)
            .should.include(expectedMortgageTip);
    }

    validateLoanNumber(expectedLoanNumber) {
        AccountDetailsPage.getFieldValue(this.accountDetailsScreen.loanReferenceNumber)
            .should.equal(expectedLoanNumber);
    }

    validateLoanTip(expectedLoanTip) {
        Actions.getText(this.accountDetailsScreen.loanTip)
            .should.include(expectedLoanTip);
    }

    validateCreditCardTip(expectedCreditCardTip) {
        Actions.getText(this.accountDetailsScreen.creditCardTip)
            .should.include(expectedCreditCardTip);
    }

    isVisibleErrorMessage(visible) {
        Actions.isVisible(this.accountDetailsScreen.errorMessage)
            .should.equal(visible);
    }

    validateErrorMessage(expectedErrorMessage) {
        Actions.getText(this.accountDetailsScreen.errorMessage)
            .should.include(expectedErrorMessage);
    }

    closeErrorMessage() {
        Actions.click(this.accountDetailsScreen.errorMessageDismiss);
    }

    validateSortCode(expectedSortCode) {
        AccountDetailsPage.getFieldValue(this.accountDetailsScreen.sortcodeFirstSection)
            .concat(AccountDetailsPage.getFieldValue(this.accountDetailsScreen.sortcodeSecondSection))
            .concat(AccountDetailsPage.getFieldValue(this.accountDetailsScreen.sortcodeThirdSection))
            .should.equal(expectedSortCode);
    }

    clearCharacterFromField(fieldType) {
        switch (fieldType) {
        case 'accountNumber':
            this.clickAccountNumber();
            this.clearAccountNumber();
            break;

        case 'sortCode':
            this.clickSortCode();
            this.clearSortCode();
            break;

        case 'loanNumber':
            this.clickLoanNumber();
            this.clearLoanNumber();
            break;

        case 'mortgageNumber':
            this.clickMortgageNumber();
            this.clearMortgageNumber();
            break;

        case 'creditCardNumber':
            this.clickCreditCard();
            this.clearCreditCard();
            break;

        default:
            throw new Error(`The field type ${fieldType} provided is not valid.`);
        }
    }

    static getFieldValue(field) {
        const value = (DeviceHelper.isAndroid()) ? Actions.getAttributeText(field) : Actions.getAttribute(field, 'value');
        return value === null || value === undefined ? '' : value;
    }

    validateAccountNumber(expectedAccountNumber) {
        AccountDetailsPage.getFieldValue(this.accountDetailsScreen.accountNumber)
            .should.equal(expectedAccountNumber);
    }

    validateAccountDetails(expectedSortCode, expectedAccountNumber) {
        this.validateSortCode(expectedSortCode);
        this.validateAccountNumber(expectedAccountNumber);
    }

    verifyMaxCharactersCreditCard() {
        Actions.getText(this.accountDetailsScreen.creditCardNumber).toString().replace(/\s+/g, '').length
            .should.equal(CREDIT_CARD_MAX_LENGTH);
    }

    verifyMaxCharactersLoanNumber() {
        Actions.getText(this.accountDetailsScreen.loanReferenceNumber).toString().length
            .should.equal(LOAN_NUMBER_MAX_LENGTH);
    }

    verifyMaxCharactersMortgageNumber() {
        Actions.getText(this.accountDetailsScreen.mortgageNumber).toString().length
            .should.equal(MORTGAGE_NUMBER_MAX_LENGTH);
    }

    deleteCharacterFromField(fieldType) {
        switch (fieldType) {
        case 'accountNumber':
            this.clickAccountNumber();
            break;

        case 'sortCode':
            this.clickSortCode();
            break;

        case 'loanNumber':
            this.clickLoanNumber();
            break;

        case 'mortgageNumber':
            this.clickMortgageNumber();
            break;

        case 'creditCardNumber':
            this.clickCreditCard();
            Actions.deleteCharacter(this.accountDetailsScreen.keyboardDeleteButton);
            break;

        case 'midSortCode':
            Actions.click(this.accountDetailsScreen.sortcodeSecondSection);
            break;

        case 'firstSortCode':
            Actions.click(this.accountDetailsScreen.sortcodeFirstSection);
            break;

        default:
            throw new Error(`The field type ${fieldType} provided is not valid.`);
        }

        Actions.deleteCharacter(this.accountDetailsScreen.keyboardDeleteButton);
        Actions.tapOutOfField();
    }

    enterGenericAccountDetails(fieldValue, fieldType) {
        switch (fieldType) {
        case 'accountNumber':
            this.enterAccountNumber(fieldValue);
            break;

        case 'sortCode':
            this.enterSortCode(fieldValue);
            break;

        case 'loanNumber':
            this.enterLoanReferenceNumber(fieldValue);
            break;

        case 'mortgageNumber':
            this.enterMortgageNumber(fieldValue);
            break;

        case 'creditCardNumber':
            this.enterCreditCardNumber(fieldValue);
            break;

        default:
            throw new Error(`The field type ${fieldType} provided is not valid.`);
        }
    }
}

module.exports = AccountDetailsPage;
