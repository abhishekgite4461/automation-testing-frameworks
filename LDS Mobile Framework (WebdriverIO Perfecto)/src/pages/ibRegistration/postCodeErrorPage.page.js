require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

const POST_CODE_MAX_LENGTH = 8;

class PostCodeErrorPage extends AuthPage {
    get postCodeErrorPageScreen() {
        return this.getNativeScreen('ibRegistration/postCodeError.screen');
    }

    verifyPostcodeTitle() {
        Actions.isExisting(this.postCodeErrorPageScreen.postCodeTitle)
            .should.equal(true);
    }

    validatePostCode(expectedPostCode) {
        Actions.waitForVisible(this.postCodeErrorPageScreen.postCode);
        Actions.getText(this.postCodeErrorPageScreen.postCode)
            .should.equal(expectedPostCode.toUpperCase());
    }

    clickNext() {
        Actions.click(this.postCodeErrorPageScreen.nextButton);
    }

    isEnabledNextOnErrorPage(flag) {
        Actions.isEnabled(this.postCodeErrorPageScreen.nextButton)
            .should.equal(flag);
    }

    setPostCode(postcode) {
        Actions.setImmediateValueAndTapOut(this.postCodeErrorPageScreen.postCode, postcode);
    }

    clearPostCode() {
        Actions.click(this.postCodeErrorPageScreen.postCode);
        Actions.clearText(this.postCodeErrorPageScreen.postCode);
        Actions.hideDeviceKeyboard();
    }

    verifyMaxCharactersPostCode() {
        Actions.getText(this.postCodeErrorPageScreen.postCode).toString().length
            .should.equal(POST_CODE_MAX_LENGTH);
    }
}

module.exports = PostCodeErrorPage;
