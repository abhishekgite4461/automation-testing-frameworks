require('chai').should();
const UnAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');
const DateHelper = require('../../utils/date.helper');

const FIRST_LAST_NAME_MAX_LENGTH = 30;
const POST_CODE_MAX_LENGTH = 8;
const EMAIL_MAX_LENGTH = 80;

class PersonalDetailsPage extends UnAuthPage {
    get personalDetailsScreen() {
        return this.getNativeScreen('ibRegistration/personalDetails.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.personalDetailsScreen.title);
    }

    verifyPersonalDetailsTitle() {
        Actions.waitForVisible(this.personalDetailsScreen.title);
    }

    clickFirstName() {
        Actions.click(this.personalDetailsScreen.firstName);
    }

    clickLastName() {
        Actions.click(this.personalDetailsScreen.lastName);
    }

    clickEmailId() {
        Actions.click(this.personalDetailsScreen.emailId);
    }

    clickPostCode() {
        Actions.click(this.personalDetailsScreen.postCode);
    }

    setFirstName(firstName) {
        Actions.click(this.personalDetailsScreen.firstName);
        Actions.setImmediateValueAndTapOut(this.personalDetailsScreen.firstName, firstName);
    }

    setLastName(lastName) {
        Actions.click(this.personalDetailsScreen.lastName);
        Actions.setImmediateValueAndTapOut(this.personalDetailsScreen.lastName, lastName);
    }

    setEmailId(email) {
        Actions.click(this.personalDetailsScreen.emailId);
        Actions.setImmediateValueAndTapOut(this.personalDetailsScreen.emailId, email);
    }

    setPostCode(postCode) {
        Actions.waitForVisible(this.personalDetailsScreen.postCode);
        Actions.setImmediateValueAndTapOut(this.personalDetailsScreen.postCode, postCode);
    }

    openDatePicker() {
        const dob = this.personalDetailsScreen.dateOfBirth;
        Actions.waitForVisible(dob);
        Actions.click(dob);
    }

    setDateOfBirth(date, month, year) {
        this.openDatePicker();
        const dateValue = new Date();
        const currentDay = DateHelper.getDayIndex(dateValue);
        const currentYear = dateValue.getFullYear() - 25;
        PersonalDetailsPage.setDateOfBirthField(this.personalDetailsScreen.yearField, year, year < currentYear ? 'DOWN' : 'UP');
        const currentValue = Actions.getAttribute(this.personalDetailsScreen.monthField, 'value').toString();
        if (PersonalDetailsPage.isMonth(currentValue)) {
            PersonalDetailsPage.setDateOfBirthField(this.personalDetailsScreen.monthField, month, 'UP');
            PersonalDetailsPage.setDateOfBirthField(this.personalDetailsScreen.dateField, Number(date).toString(), date < currentDay ? 'DOWN' : 'UP');
        } else {
            PersonalDetailsPage.setDateOfBirthField(this.personalDetailsScreen.monthField, Number(date).toString(), date < currentDay ? 'DOWN' : 'UP');
            PersonalDetailsPage.setDateOfBirthField(this.personalDetailsScreen.dateField, month, 'UP');
        }
        Actions.waitForVisible(this.personalDetailsScreen.dobOkButton);
        Actions.click(this.personalDetailsScreen.dobOkButton);
    }

    setDefaultDateOfBirth() {
        this.openDatePicker();
        Actions.waitForVisible(this.personalDetailsScreen.dobOkButton);
        Actions.click(this.personalDetailsScreen.dobOkButton);
    }

    static isMonth(month) {
        switch (month.toLowerCase()) {
        case 'january':
        case 'february':
        case 'march':
        case 'april':
        case 'may':
        case 'june':
        case 'july':
        case 'august':
        case 'september':
        case 'october':
        case 'november':
        case 'december':
            return true;
        default:
            return false;
        }
    }

    static setDateOfBirthField(element, value, swipeDirection) {
        const x = $(element).getLocation('x');
        const y = $(element).getLocation('y');
        const {height} = browser.getElementSize(browser.$(element).elementId);
        const {width} = browser.getElementSize(browser.$(element).elementId);
        const centerY = height / 2 + y;
        const centerX = width / 2 + x;
        let newY;
        if (swipeDirection.toString().toLowerCase() === 'down') {
            newY = centerY - 25;
        } else {
            newY = centerY + 25;
        }
        let currentValue = Actions.getAttribute(element, 'value');
        while (value !== currentValue) {
            browser.touchAction({
                action: 'tap', x: centerX, y: newY
            });
            currentValue = Actions.getAttribute(element, 'value');
        }
    }

    clickCancelOnDatePicker() {
        Actions.waitForVisible(this.personalDetailsScreen.dobCancelButton);
        Actions.click(this.personalDetailsScreen.dobCancelButton);
    }

    clickOkOnDatePicker() {
        Actions.waitForVisible(this.personalDetailsScreen.dobOkButton);
        Actions.click(this.personalDetailsScreen.dobOkButton);
    }

    setPersonalDetails(firstName, lastName, email, date, month, year, postCode) {
        this.setFirstName(firstName);
        this.setLastName(lastName);
        this.setEmailId(email);
        this.setDefaultDateOfBirth(date, month, year);
        this.setPostCode(postCode);
    }

    clickNext() {
        Actions.hideDeviceKeyboard();
        Actions.swipeUntilVisible(SwipeAction.UP, this.personalDetailsScreen.nextButton);
        Actions.click(this.personalDetailsScreen.nextButton);
    }

    clickBackOnDOBError() {
        Actions.click(this.personalDetailsScreen.backButtonDOBError);
    }

    clickBack() {
        Actions.hideDeviceKeyboard();
        Actions.swipeUntilVisible(SwipeAction.UP, this.personalDetailsScreen.backButton);
        Actions.click(this.personalDetailsScreen.backButton);
    }

    verifyErrorMessageVisible(flag) {
        Actions.isVisible(this.personalDetailsScreen.errorMessage)
            .should.equal(flag);
    }

    verifyYouthErrorMessageVisible(flag) {
        Actions.isVisible(this.personalDetailsScreen.youthErrorMessage)
            .should.equal(flag);
    }

    verifyErrorMessage(expectedErrorMessage) {
        this.verifyErrorMessageVisible(true);
        if (DeviceHelper.isAndroid()) {
            Actions.waitForVisible(this.personalDetailsScreen.errorMessage);
            Actions.getText(this.personalDetailsScreen.errorMessage)
                .should.equal(expectedErrorMessage);
        } else {
            Actions.waitForVisible(this.personalDetailsScreen.errorMessage);
            Actions.getText(this.personalDetailsScreen.errorMessage)
                .should.include(expectedErrorMessage);
        }
    }

    isEnabledNextButton(flag) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.personalDetailsScreen.nextButton);
        Actions.isEnabled(this.personalDetailsScreen.nextButton)
            .should.equal(flag);
    }

    verifyFirstName(expectedFirstName) {
        Actions.getText(this.personalDetailsScreen.firstName)
            .should.equal(expectedFirstName);
    }

    verifyLastName(expectedLastName) {
        Actions.getText(this.personalDetailsScreen.lastName)
            .should.equal(expectedLastName);
    }

    verifyEmail(expectedEmail) {
        Actions.getText(this.personalDetailsScreen.emailId)
            .should.equal(expectedEmail);
    }

    verifyPostCode(expectedPostCode) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.personalDetailsScreen.postCode);
        Actions.getText(this.personalDetailsScreen.postCode).toString()
            .should.include(expectedPostCode.toUpperCase());
    }

    verifyFirstCharacterFirstName(expectedFirstName) {
        PersonalDetailsPage.verifyFirstCharacterCapitalized(
            this.personalDetailsScreen.firstName,
            expectedFirstName
        );
    }

    verifyFirstCharacterLastName(expectedLastName) {
        PersonalDetailsPage.verifyFirstCharacterCapitalized(
            this.personalDetailsScreen.lastName,
            expectedLastName
        );
    }

    clearFirstName() {
        Actions.clearText(this.personalDetailsScreen.firstName);
    }

    clearLastName() {
        Actions.clearText(this.personalDetailsScreen.lastName);
    }

    closeErrorMessage() {
        Actions.waitForVisible(this.personalDetailsScreen.errorMessageDismiss);
        Actions.click(this.personalDetailsScreen.errorMessageDismiss);
    }

    verifyMaxCharactersLastName() {
        Actions.getText(this.personalDetailsScreen.lastName).toString().length
            .should.equal(FIRST_LAST_NAME_MAX_LENGTH);
    }

    verifyMaxCharactersPostCode() {
        Actions.getText(this.personalDetailsScreen.postCode).toString().length
            .should.equal(POST_CODE_MAX_LENGTH);
    }

    verifyMaxCharactersEmail() {
        Actions.getText(this.personalDetailsScreen.emailId).toString().length
            .should.equal(EMAIL_MAX_LENGTH);
    }

    verifyMaxCharactersFirstName() {
        Actions.getText(this.personalDetailsScreen.firstName).toString().length
            .should.equal(FIRST_LAST_NAME_MAX_LENGTH);
    }

    clearEmailId() {
        Actions.clearText(this.personalDetailsScreen.emailId);
    }

    clearPostCode() {
        Actions.clearText(this.personalDetailsScreen.postCode);
    }

    verifyDefaultDateOfBirth(yearDelta) {
        const dateValue = new Date();
        const day = DateHelper.getDayIndex(dateValue);
        const year = dateValue.getFullYear() - yearDelta;
        const month = DateHelper.getMonthString(dateValue.getMonth());

        Actions.waitForVisible(this.personalDetailsScreen.dateOfBirth);
        if (DeviceHelper.isAndroid()) {
            Actions.getText(this.personalDetailsScreen.dateOfBirth)
                .should.equal(`${day}-${month}-${year}`);
        } else {
            Actions.getAttribute(this.personalDetailsScreen.dateOfBirth, 'value')
                .should.equal(`${day} ${month} ${year}`);
        }
    }

    static verifyFirstCharacterCapitalized(loc, text) {
        const textValue = Actions.getText(loc).toString();
        textValue.charAt(0).should.equal(text.charAt(0).toUpperCase());
        textValue.charAt(1).should.equal(text.charAt(1).toLowerCase());
    }
}

module.exports = PersonalDetailsPage;
