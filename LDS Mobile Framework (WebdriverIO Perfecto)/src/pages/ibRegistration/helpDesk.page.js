require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class HelpDeskPage extends AuthPage {
    get helpDeskPageScreen() {
        return this.getNativeScreen('ibRegistration/helpDesk.screen');
    }

    validateHelpDeskError(expectedErrorMessage) {
        Actions.getText(this.helpDeskPageScreen.helpDeskError)
            .should.include(expectedErrorMessage);
    }

    validateHelpDeskCMSError(expectedErrorMessage) {
        Actions.getText(this.helpDeskPageScreen.helpDeskCMSError)
            .should.include(expectedErrorMessage);
    }

    verifyHelpDeskTitle() {
        Actions.isVisible(this.helpDeskPageScreen.helpCMSTitle);
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.helpDeskPageScreen.helpDeskError);
    }
}

module.exports = HelpDeskPage;
