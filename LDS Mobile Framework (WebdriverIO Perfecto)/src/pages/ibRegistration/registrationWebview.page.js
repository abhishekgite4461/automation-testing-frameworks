const UnauthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');
const FscsMobileBankingPage = require('../fscsMobileBanking.page');

const fscsMobileBankingPage = new FscsMobileBankingPage();

class RegistrationWebviewPage extends UnauthPage {
    get newWelcomeBenefitsScreen() {
        return this.getNativeScreen('ibRegistration/newWelcomeBenefits.screen');
    }

    get fraudGuaranteeWeb() {
        return this.getWebScreen('fraudGuaranteeWeb.screen');
    }

    verifyRegistrationWebViewTitle() {
        Actions.waitForVisible(this.newWelcomeBenefitsScreen.webTitle);
    }

    navigateToFraudGuarantee() {
        fscsMobileBankingPage.closeAllTabs();
        Actions.waitForVisible(this.newWelcomeBenefitsScreen.title, 30000);
        Actions.clickOnText(this.newWelcomeBenefitsScreen.fraudLink);
        this.leaveTheApp();
    }

    navigateToAppDemo() {
        Actions.clickOnText(this.newWelcomeBenefitsScreen.demoLink);
        this.leaveTheApp();
    }

    navigateToNewCustomer() {
        Actions.click(this.newWelcomeBenefitsScreen.customerLink);
        this.leaveTheApp();
    }

    canSeeFraudGuaranteeWebPage() {
        Actions.waitForVisible(this.fraudGuaranteeWeb.fraudGuaranteeTitle);
    }

    canSeeAppDemoWebPage() {
        Actions.isVisible(this.newWelcomeBenefitsScreen.appDemoTitle);
    }

    canSeeNewCustomerWebPage() {
        Actions.isVisible(this.newWelcomeBenefitsScreen.newCustomerTitle);
    }

    leaveTheApp() {
        Actions.click(this.newWelcomeBenefitsScreen.yesLeaveTheApp);
    }
}

module.exports = RegistrationWebviewPage;
