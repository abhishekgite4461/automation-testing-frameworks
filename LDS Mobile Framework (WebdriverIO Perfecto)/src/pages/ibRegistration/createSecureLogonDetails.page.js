require('chai').should();
const Moment = require('moment');
const UnAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');
const runConfig = require('../../enums/runConfig.enum');

const LDS_MAX_PASSWORD_LENGTH = 15;
const BOS_HFX_MAX_PASSWORD_LENGTH = 20;
const KEYBOARD_DELETE_KEY_CODE = 67;
const MAX_USER_ID_LENGTH = 30;
const YEAR_DELTA = 25;
const oldAndroidVersion = () => (DeviceHelper.isAndroid() && String(parseFloat(DeviceHelper.osVersion())).split('.')[0] <= 6);

class CreateSecureLogonDetailsPage extends UnAuthPage {
    get createSecureLogonDetailsScreen() {
        return this.getNativeScreen('ibRegistration/createSecureLogonDetails.screen');
    }

    verifyCreateSecureLogonDetailsTitle() {
        Actions.waitForVisible(this.createSecureLogonDetailsScreen.title);
    }

    clickUserId() {
        Actions.click(this.createSecureLogonDetailsScreen.userId);
    }

    enterUserId(userId) {
        Actions.click(this.createSecureLogonDetailsScreen.userId, userId);
        Actions.setImmediateValue(this.createSecureLogonDetailsScreen.userId, userId);
    }

    clickPassword() {
        Actions.click(this.createSecureLogonDetailsScreen.password);
        Actions.hideDeviceKeyboard();
    }

    enterPassword(password) {
        Actions.click(this.createSecureLogonDetailsScreen.password, password);
        Actions.setImmediateValue(this.createSecureLogonDetailsScreen.password, password);
    }

    clickConfirmPassword() {
        Actions.click(this.createSecureLogonDetailsScreen.confirmPassword);
    }

    enterConfirmPassword(confirmPassword) {
        Actions.click(this.createSecureLogonDetailsScreen.confirmPassword,
            confirmPassword);
        Actions.setImmediateValue(
            this.createSecureLogonDetailsScreen.confirmPassword,
            confirmPassword
        );
    }

    clearUserId() {
        Actions.clearText(this.createSecureLogonDetailsScreen.userId);
    }

    clearPassword() {
        Actions.clearText(this.createSecureLogonDetailsScreen.password);
    }

    clearConfirmPassword() {
        Actions.clearText(this.createSecureLogonDetailsScreen.confirmPassword);
    }

    enterSecureLogonDetails(userId, password) {
        this.enterUserId(userId);
        Actions.tapOutOfField();
        this.enterPassword(password);
        Actions.tapOutOfField();
        this.enterConfirmPassword(password);
        Actions.tapOutOfField();
    }

    validateCheckButtonVisible(flag) {
        Actions.isVisible(this.createSecureLogonDetailsScreen.userIdCheck)
            .should.equal(flag);
    }

    validateLoadingIndicator(flag) {
        Actions.isVisible(this.createSecureLogonDetailsScreen.loadingIndicator)
            .should.equal(flag);
    }

    clickUserIdCheckButton() {
        Actions.click(this.createSecureLogonDetailsScreen.userIdCheck);
    }

    validateUserIdOptionsVisible() {
        Actions.waitForVisible(this.createSecureLogonDetailsScreen.userIdOptionsTitle,
            runConfig.TIMEOUTS.WAIT_FOR_SERVER_RESPONSE());
    }

    selectUsername() {
        Actions.click(this.createSecureLogonDetailsScreen.userIdOptionsSuggestion);
    }

    clickBackUserIdOptions() {
        Actions.click(this.createSecureLogonDetailsScreen.userIdOptionsBack);
    }

    validateTipMessage(field) {
        let tipFieldLocator;
        switch (field) {
        case 'UserId':
            tipFieldLocator = this.createSecureLogonDetailsScreen.userIdTip;
            break;

        case 'Password':
            tipFieldLocator = this.createSecureLogonDetailsScreen.passwordTip;
            break;

        case 'ConfirmPassword':
            tipFieldLocator = this.createSecureLogonDetailsScreen.confirmPasswordTip;
            break;

        default:
            throw new Error(`The field ${field} you have provided is not valid.`);
        }
        if (!Actions.isExisting(tipFieldLocator)) {
            Actions.hideDeviceKeyboard();
        }
        Actions.isExisting(tipFieldLocator).should.equal(true);
    }

    validateUserId(expectedUserId) {
        Actions.getText(this.createSecureLogonDetailsScreen.userId)
            .should.equal(expectedUserId);
    }

    // MQE-131: masked text value is not returned on android < 7
    validateMaskedPassword(isSecure) {
        if (!oldAndroidVersion()) {
            const isMasked = DeviceHelper.isAndroid() ? Actions.getAttributeText(this.createSecureLogonDetailsScreen.password) : Actions.getAttribute(this.createSecureLogonDetailsScreen.password, 'value');
            isMasked.includes('•').should.equal(isSecure);
        }
    }

    // MQE-131: masked text value is not returned on android < 7
    validateMaskedConfirmPassword(isSecure) {
        if (!oldAndroidVersion()) {
            const password = Actions.getText(this.createSecureLogonDetailsScreen.confirmPassword);
            password.includes('•').should.equal(isSecure);
        }
    }

    validateConfirmPasswordEnabled(flag) {
        Actions.tapOutOfField();
        Actions.isEnabled(this.createSecureLogonDetailsScreen.confirmPassword)
            .should.equal(flag);
    }

    clickHideShowPassword() {
        Actions.click(this.createSecureLogonDetailsScreen.hideShowPassword);
    }

    clickHideShowConfirmPassword() {
        Actions.click(this.createSecureLogonDetailsScreen.hideShowConfirmPassword);
    }

    validateHideShowLabelPassword(expectedLabel) {
        Actions.getText(this.createSecureLogonDetailsScreen.hideShowPassword)
            .should.include(expectedLabel);
    }

    validateHideShowLabelConfirmPassword(expectedLabel) {
        Actions.getText(this.createSecureLogonDetailsScreen.hideShowConfirmPassword)
            .should.include(expectedLabel);
    }

    validateFindOutMore() {
        if (!Actions.isExisting(this.createSecureLogonDetailsScreen.passwordfindOutMore)) {
            Actions.hideDeviceKeyboard();
        }
        Actions.click(this.createSecureLogonDetailsScreen.passwordfindOutMore);
        Actions.waitForVisible(this.createSecureLogonDetailsScreen.findOutMorePopUp);
        Actions.click(this.createSecureLogonDetailsScreen.findOutMorePopUpClose);
    }

    validateCommercialAccountMessage(expectedMessage) {
        Actions.getText(this.createSecureLogonDetailsScreen.commercialAccountMessage)
            .should.equal(expectedMessage);
    }

    validateCommercialUserId(expectedUserId) {
        Actions.getText(this.createSecureLogonDetailsScreen.commercialUserId)
            .should.equal(expectedUserId);
    }

    checkUserIdMaxLength() {
        Actions.getText(this.createSecureLogonDetailsScreen.userId).length
            .should.equal(MAX_USER_ID_LENGTH);
    }

    // MQE-131: masked text value is not returned on android < 7
    checkPasswordMaxLength() {
        if (!oldAndroidVersion()) {
            let expectedLength;
            if (browser.capabilities.brand === 'LDS') {
                expectedLength = LDS_MAX_PASSWORD_LENGTH;
            } else {
                expectedLength = BOS_HFX_MAX_PASSWORD_LENGTH;
            }
            Actions.getText(this.createSecureLogonDetailsScreen.password).length
                .should.equal(expectedLength);
        }
    }

    clickKeyboardDelete() {
        if (DeviceHelper.isAndroid()) {
            Actions.pressKeyCode(KEYBOARD_DELETE_KEY_CODE);
        } else {
            Actions.click(this.createSecureLogonDetailsScreen.keyboardDeleteButton);
        }
    }

    validateNextButtonEnabled(flag) {
        Actions.isEnabled(this.createSecureLogonDetailsScreen.nextButton)
            .should.equal(flag);
    }

    clickNext() {
        Actions.click(this.createSecureLogonDetailsScreen.nextButton);
    }

    enterDefaultDateAsPassword(dateFormat) {
        const date = Moment(new Date());
        const day = date.format('DD').toString();
        const month = date.format('MM').toString();
        const fullYear = (date.format('YYYY') - YEAR_DELTA).toString();
        const shortYear = fullYear.substring(2, 4).toString();

        switch (dateFormat) {
        case 'yyyy':
            this.enterPassword(fullYear);
            break;

        case 'ddmmyy':
            this.enterPassword(day + month + shortYear);
            break;

        case 'ddmmyyyy':
            this.enterPassword(day + month + fullYear);
            break;

        default:
            throw new Error(`The date format ${dateFormat} you have provided is not valid.`);
        }
    }

    validateErrorIconVisible(isVisible) {
        Actions.isVisible(this.createSecureLogonDetailsScreen.errorIcon)
            .should.equal(isVisible);
    }
}

module.exports = CreateSecureLogonDetailsPage;
