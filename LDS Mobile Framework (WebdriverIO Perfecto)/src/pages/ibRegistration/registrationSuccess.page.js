const UnAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class RegistrationSuccessPage extends UnAuthPage {
    get registrationSuccessScreen() {
        return this.getNativeScreen('ibRegistration/registrationSuccess.screen');
    }

    verifyRegistrationSuccessTitle() {
        Actions.waitForVisible(this.registrationSuccessScreen.title);
        Actions.waitForVisible(this.registrationSuccessScreen.subTitle);
    }

    clickContinue() {
        Actions.waitForVisible(this.registrationSuccessScreen.continueButton);
        Actions.click(this.registrationSuccessScreen.continueButton);
    }

    validateSuccessMessage(expectedSuccessMessage) {
        Actions.waitForVisible(this.registrationSuccessScreen.successMessage);
        Actions.getText(this.registrationSuccessScreen.successMessage)
            .should.include(expectedSuccessMessage);
    }

    validateFooterMessage(expectedErrorMessage) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.registrationSuccessScreen.footerMessage);
        Actions.getText(this.registrationSuccessScreen.footerMessage)
            .should.include(expectedErrorMessage);
    }
}

module.exports = RegistrationSuccessPage;
