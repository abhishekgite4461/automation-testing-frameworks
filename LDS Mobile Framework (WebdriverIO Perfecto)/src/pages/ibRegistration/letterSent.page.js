const {assert} = require('chai');
const UnAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');

let actualMessage;

class LetterSentPage extends UnAuthPage {
    get letterSentScreen() {
        return this.getNativeScreen('ibRegistration/letterSent.screen');
    }

    verifyLetterSentTitle() {
        Actions.isVisible(this.letterSentScreen.letterSentTitle).should.equal(true);
    }

    validateRegisteredUsername(expectedUserName) {
        const selector = this.letterSentScreen.registeredUsername;
        const actualUserName = Actions.getText(selector);
        assert.equal(actualUserName, expectedUserName, 'Registered username displayed is NOT correct');
    }

    validateErrorMessage(expectedErrorMessage) {
        actualMessage = Actions.getText(this.letterSentScreen.letterSentText)
            .toString().replace(/\n/g, '')
            .replace(/• /g, '');
        actualMessage.should.include(expectedErrorMessage);
    }
}

module.exports = LetterSentPage;
