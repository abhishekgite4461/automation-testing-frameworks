const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class CallUsMissingMobilePopUp extends AuthPage {
    /**
     * @returns {CallUsMissingMobilePopUpScreen}
     */
    get missingMobilePopUpScreen() {
        return this.getNativeScreen('callUs/callUsMissingMobilePopUp.screen');
    }

    shouldSeeMissingNumberAlert() {
        Actions.isVisible(this.missingMobilePopUpScreen.title).should.equal(true);
    }

    shouldSeeOptionToAddNumber() {
        Actions.isVisible(this.missingMobilePopUpScreen.addPhoneNumber).should.equal(true);
    }

    shouldSeeOptionToContinueCall() {
        Actions.isVisible(this.missingMobilePopUpScreen.continueCall).should.equal(true);
    }

    selectAddPhoneNumberOption() {
        Actions.click(this.missingMobilePopUpScreen.addPhoneNumber);
    }

    selectContinueWithCallOption() {
        Actions.click(this.missingMobilePopUpScreen.continueCall);
    }
}

module.exports = CallUsMissingMobilePopUp;
