require('chai').should();
const camelCase = require('camelcase');
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class NewProductEnquiriesPage extends AuthPage {
    /**
     * @returns {callUsNewProductEnquiriesScreen}
     */
    get callUsNewProductEnquiriesScreen() {
        return this.getNativeScreen('callUs/callUsNewProductEnquiries.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.callUsNewProductEnquiriesScreen.title);
    }

    selectArrangement(arrangementType) {
        this.swipeUntilArrangementVisible(arrangementType);
        Actions.click(this.getArrangementLocatorByType(arrangementType));
    }

    swipeUntilArrangementVisible(arrangementType) {
        Actions.swipeByPercentUntilVisible(this.getArrangementLocatorByType(arrangementType),
            70, 80, 50, 50, 100, 15);
    }

    getArrangementLocatorByType(type) {
        const locator = this.callUsNewProductEnquiriesScreen[camelCase(type)];

        if (locator === undefined) {
            throw new Error(`Unknown account type: ${type}`);
        } else {
            return locator;
        }
    }
}
module.exports = NewProductEnquiriesPage;
