const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ChangeAddressCallUsPage extends AuthPage {
    get changeAddressCallUsScreen() {
        return this.getNativeScreen('callUs/changeAddressCallUs.screen');
    }

    viewLegalInformation() {
        Actions.isVisible(this.changeAddressCallUsScreen.legalInfo);
    }

    selectCallUsButton() {
        Actions.click(this.changeAddressCallUsScreen.callUsButton);
    }
}

module.exports = ChangeAddressCallUsPage;
