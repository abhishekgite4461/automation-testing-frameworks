require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');
const CallUsReason = require('../../enums/callUsReason.enum');
const ADBHelper = require('../../utils/adb.helper');

const PhoneEnum = `../../enums/${browser.capabilities.brand.toLowerCase()}SecureCallNumbers.enum`;
const PhoneNumber = require(PhoneEnum); // eslint-disable-line import/no-dynamic-require

class CallUsPage extends AuthPage {
    get callUsScreen() {
        return this.getNativeScreen('callUs/callUs.screen');
    }

    get commonScreen() {
        return this.getNativeScreen('common/common.screen');
    }

    get alertPopUpScreen() {
        return this.getNativeScreen('alertPopUp.screen');
    }

    waitForUnSecureCallPageLoad() {
        Actions.waitForVisible(this.callUsScreen.phoneNumber);
    }

    waitForSecureCallPageLoad() {
        Actions.waitForVisible(this.callUsScreen.callUsButton);
    }

    shouldSeeCallUsButton() {
        if (!Actions.isVisible(this.callUsScreen.closeButton)) {
            Actions.isVisible(this.callUsScreen.callUsButton).should.equal(true);
        } else {
            Actions.isVisible(this.callUsScreen.phoneNumber).should.equal(true);
        }
    }

    shouldSeeReferenceNumber() {
        // Arrangement detail elements are not accessible on iOS, so only validate on Android
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.callUsScreen.referenceNumber).should.equal(true);
        }
    }

    shouldSeeAccountNumber() {
        // Arrangement detail elements are not accessible on iOS, so only validate on Android
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.callUsScreen.accountNumber).should.equal(true);
        }
    }

    shouldSeeSortCode() {
        // Arrangement detail elements are not accessible on iOS, so only validate on Android
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.callUsScreen.sortCode).should.equal(true);
        }
    }

    shouldSeeLostOrStolenSelfServiceButton() {
        Actions.swipeByPercentUntilVisible(this.callUsScreen.lostOrStolenSelfServiceOption, 60, 60, 10, 10, 100);
    }

    shouldSeeSelfServiceOptions(options) {
        for (const option of options) {
            const reason = CallUsReason.fromStringByEscapingSpace(option);
            const locator = this.getSelfServiceOptionLocatorForReason(reason);
            Actions.swipeUntilVisible(SwipeAction.UP, locator);
        }
    }

    shouldNotSeeSelfServiceOptions(options) {
        for (const option of options) {
            const reason = CallUsReason.fromStringByEscapingSpace(option);
            const locator = this.getSelfServiceOptionLocatorForReason(reason);
            Actions.isVisible(locator).should.equal(false);
        }
    }

    selectSelfServiceOption(option) {
        const reason = CallUsReason.fromStringByEscapingSpace(option);
        const locator = this.getSelfServiceOptionLocatorForReason(reason);
        Actions.swipeByPercentUntilVisible(locator, 60, 60, 10, 10, 100);
        Actions.click(locator);
    }

    validateCallUsPageLabels(labels) {
        for (const label of labels) {
            const labelLocator = this.callUsScreen[label.labelName];
            Actions.swipeUntilVisible(SwipeAction.UP, labelLocator);
        }
    }

    selectCallUsButton() {
        Actions.click(this.callUsScreen.callUsButton);
    }

    shouldSeePhoneNumber() {
        Actions.isVisible(this.callUsScreen.phoneNumber).should.equal(true);
    }

    goBackToHome() {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.commonScreen.backButton);
        } else {
            Actions.click(this.commonScreen.homeButton);
        }
    }

    getSelfServiceOptionLocatorForReason(reason) {
        switch (reason) {
        case CallUsReason.RESET_YOUR_PASSWORD:
            return this.callUsScreen.resetYourPasswordSelfServiceButton;
        case CallUsReason.LOST_OR_STOLEN_CARDS:
            return this.callUsScreen.lostOrStolenSelfServiceButton;
        case CallUsReason.REPLACEMENT_CARDS_PINS:
            return this.callUsScreen.replacementCardSelfServiceButton;
        default:
            throw new Error(`Unknown self-service option : ${reason}`);
        }
    }

    selectPhoneNumber() {
        Actions.click(this.callUsScreen.phoneNumber);
    }

    validatePhoneNumber(option, callType) {
        let mobileNumber;
        const osVersion = DeviceHelper.osVersion();

        if (DeviceHelper.isIOS() && (osVersion >= '10.3')) {
            Actions.waitForVisible(this.callUsScreen.phoneNumberText);
            const text = Actions.getText(this.callUsScreen.phoneNumberText);
            mobileNumber = text.split('+').pop().split(',').shift()
                .replace(/[^\x20-\x7E]| /g, '')
                .slice(-10);

            CallUsPage.comparePhoneNumber(mobileNumber, option, callType);
            Actions.click(this.callUsScreen.cancelButton);
        } if (DeviceHelper.isIOS() && (osVersion < '10.3') && (osVersion > '10.0')) {
            /* osVersion<10 does not take you to dialler screen to get the phoneNumber object.
                 Hence restricting the OS version */
            if (DeviceHelper.mobileNumber() !== '') {
                Actions.pause(1000);
                Actions.waitForVisible(this.callUsScreen.phoneNumberOnDialler);
                const text = Actions.getText(this.callUsScreen.phoneNumberOnDialler);
                mobileNumber = text.split('+').pop().split(',').shift()
                    .replace(/[^\x20-\x7E]| /g, '')
                    .slice(-10);

                CallUsPage.comparePhoneNumber(mobileNumber, option, callType);
                Actions.click(this.callUsScreen.endCallButton);
            } else {
                Actions.pause(1000);
                if (Actions.isVisible(this.callUsScreen.phoneNumberOnDialler)) {
                    const text = Actions.getText(this.callUsScreen.phoneNumberOnDialler);
                    mobileNumber = text.split('+').pop().split(',').shift()
                        .replace(/[^\x20-\x7E]| /g, '')
                        .slice(-10);

                    CallUsPage.comparePhoneNumber(mobileNumber, option, callType);
                }
                if (Actions.isVisible(this.callUsScreen.networkPopup)) {
                    Actions.click(this.callUsScreen.networkPopup);
                }
                if (Actions.isVisible(this.callUsScreen.cancelButton)) {
                    Actions.click(this.callUsScreen.cancelButton);
                }
            }
        } else if (DeviceHelper.isAndroid()) {
            Actions.pause(2000);
            const apn = ADBHelper.getAPNState();
            let outgoingNumber = ADBHelper.getOutgoingNumber(browser.handsetInfo.deviceId);
            while (outgoingNumber === null) {
                outgoingNumber = ADBHelper.getOutgoingNumber(browser.handsetInfo.deviceId);
            }
            const jsonString = JSON.stringify(outgoingNumber);
            const secureNumber = jsonString.split('+').pop().split(',').shift()
                .slice(-10);
            const unsecureNumber = jsonString.split('+').pop().split('}').shift()
                .slice(-10);

            mobileNumber = (callType === 'secure') ? secureNumber : unsecureNumber;
            Actions.pause(2000);
            if (apn.includes('mServiceState=0') || apn.includes('vodafone')) {
                ADBHelper.hangPhoneCall(browser.handsetInfo.deviceId);
            } else if (Actions.isVisible(this.commonScreen.errorMessageOkButton)) {
                Actions.click(this.commonScreen.errorMessageOkButton);
            }
            CallUsPage.comparePhoneNumber(mobileNumber, option, callType);
        }
    }

    validateTabletPhoneNumber() {
        const text = Actions.getText(this.alertPopUpScreen.contentText);
        const mobileNumber = text.split('+').pop().slice(-10);
        mobileNumber.should.equal(PhoneNumber.TABLET_IPAD.unsecure);
    }

    static comparePhoneNumber(mobileNumber, option, callType) {
        const phoneNumber = PhoneNumber.fromStringByEscapingSpace(option);

        mobileNumber.should.equal(phoneNumber[callType]);
    }

    selectBackButton() {
        Actions.click(this.callUsScreen.modalBackButton);
    }

    selectCloseButton() {
        Actions.click(this.callUsScreen.closeButton);
    }

    validateCallUsPages(page) {
        Actions.waitForVisible(this.callUsScreen[`${page}CallUsPage`]);
    }
}

module.exports = CallUsPage;
