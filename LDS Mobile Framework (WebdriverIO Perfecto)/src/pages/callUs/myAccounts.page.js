require('chai').should();
const camelCase = require('camelcase');
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SavedAppDataRepository = require('../../data/savedAppData.repository.js');
const SavedData = require('../../enums/savedAppData.enum.js');

class MyAccountsPage extends AuthPage {
    /**
     * @returns {myAccountsScreen}
     */
    get myAccountsScreen() {
        return this.getNativeScreen('callUs/myAccounts.screen');
    }

    get callUsScreen() {
        return this.getNativeScreen('callUs/callUs.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.myAccountsScreen.title);
    }

    viewArrangements(arrangementTypes) {
        for (const arrangementType of arrangementTypes) {
            this.swipeUntilArrangementVisible(arrangementType);
        }
    }

    verifyArrangement(arrangementType) {
        const totalAccountsAvailable = SavedAppDataRepository.getData(SavedData.MORE_THAN_ONE_ACCOUNT);
        if (totalAccountsAvailable === false) {
            Actions.isVisible(this.callUsScreen.callUsButton).should.equal(true);
        } else if (totalAccountsAvailable === true) {
            Actions.waitForVisible(this.myAccountsScreen.title);
            this.selectArrangement(arrangementType);
        }
    }

    selectArrangement(arrangementType) {
        this.swipeUntilArrangementVisible(arrangementType);
        Actions.click(this.getArrangementLocatorByType(arrangementType));
    }

    swipeUntilArrangementVisible(arrangementType) {
        Actions.swipeByPercentUntilVisible(this.getArrangementLocatorByType(arrangementType),
            70, 80, 50, 50, 100, 15);
    }

    getArrangementLocatorByType(type) {
        const locator = this.myAccountsScreen[camelCase(type)];

        if (locator === undefined) {
            throw new Error(`Unknown account type: ${type}`);
        } else {
            return locator;
        }
    }

    chooseAccount(accountHeaderName) {
        const accountName = this.myAccountsScreen.accountNameLabel(accountHeaderName, true);
        Actions.swipeByPercentUntilVisible(accountName, 80, 85, 20, 25, 200);
        Actions.click(accountName);
    }
}
module.exports = MyAccountsPage;
