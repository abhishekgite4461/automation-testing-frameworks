require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class PdfStatementsPage extends AuthPage {
    get pdfStatementsScreen() {
        return this.getWebScreen('pdfStatementCreditCard.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.pdfStatementsScreen.title);
    }

    selectViewPdfButton() {
        Actions.click(this.pdfStatementsScreen.viewPDFButton);
    }
}
module.exports = PdfStatementsPage;
