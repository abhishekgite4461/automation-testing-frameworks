const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class ShareIBANPage extends AuthPage {
    get sendBankDetailsScreen() {
        return this.getNativeScreen('shareIBAN.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.sendBankDetailsScreen.shareIbanPageTitle);
    }

    shouldNotSeeTitle() {
        Actions.waitForNotVisible(this.sendBankDetailsScreen.shareIbanPageTitle, 5000);
    }

    shouldSeeWarningMessage() {
        Actions.isVisible(this.sendBankDetailsScreen.ibanwarning).should.equal(true);
    }

    shouldSeeUKDetailsPresent() {
        Actions.isVisible(this.sendBankDetailsScreen.nameUKaccount).should.equal(true);
        Actions.isVisible(this.sendBankDetailsScreen.nameUKaccountValue).should.equal(true);
        Actions.isVisible(this.sendBankDetailsScreen.sortCode).should.equal(true);
        Actions.isVisible(this.sendBankDetailsScreen.sortCodeValue).should.equal(true);
        Actions.isVisible(this.sendBankDetailsScreen.accountNumber).should.equal(true);
        Actions.isVisible(this.sendBankDetailsScreen.accountNumberValue).should.equal(true);
    }

    shouldSeeShareUKBankDetailsButton() {
        Actions.isVisible(this.sendBankDetailsScreen.sendUKDetailsButton).should.equal(true);
    }

    shouldSeeInternationalAccountDetails() {
        Actions.isVisible(this.sendBankDetailsScreen.nameIntAccount).should.equal(true);
        Actions.isVisible(this.sendBankDetailsScreen.nameIntAccountValue).should.equal(true);
        Actions.isVisible(this.sendBankDetailsScreen.iban).should.equal(true);
        Actions.isVisible(this.sendBankDetailsScreen.ibanValue).should.equal(true);
        Actions.isVisible(this.sendBankDetailsScreen.bic).should.equal(true);
        Actions.isVisible(this.sendBankDetailsScreen.bicValue).should.equal(true);
    }

    shouldSeeShareIBANDetailsButton() {
        Actions.isVisible(this.sendBankDetailsScreen.sendIntDetailsButton).should.equal(true);
    }

    selectUKBankDetailsButton() {
        if (Actions.isVisible(this.sendBankDetailsScreen.sendUKDetailsButton)) {
            Actions.click(this.sendBankDetailsScreen.sendUKDetailsButton);
        }
    }

    selectIntBankDetailsButton() {
        if (Actions.isVisible(this.sendBankDetailsScreen.sendIntDetailsButton)) {
            Actions.click(this.sendBankDetailsScreen.sendIntDetailsButton);
        }
    }

    selectCloseIcon() {
        if (Actions.isVisible(this.sendBankDetailsScreen.closeIcon)) {
            Actions.click(this.sendBankDetailsScreen.closeIcon);
        }
    }

    shouldSeeErrorMessage(errorMessage) {
        Actions.isVisible(this.sendBankDetailsScreen.ibanErrorMessage).should.equal(true);
        Actions.getText(this.sendBankDetailsScreen.ibanErrorMessage).should.contains(errorMessage);
    }

    shouldSeeBicSearchResult(bicMessage) {
        Actions.isVisible(this.sendBankDetailsScreen.bicSearchMessage).should.equal(true);
        Actions.getText(this.sendBankDetailsScreen.bicSearchMessage).should.equal(bicMessage);
    }

    shouldSeeIBANSearchResult(ibanMessage) {
        Actions.isVisible(this.sendBankDetailsScreen.ibanSearchMessage).should.equal(true);
        Actions.getText(this.sendBankDetailsScreen.ibanSearchMessage).should.equal(ibanMessage);
    }

    shouldNotSeeBicandIbanSearchResult() {
        Actions.isVisible(this.sendBankDetailsScreen.ibanSearchMessage).should.equal(false);
        Actions.isVisible(this.sendBankDetailsScreen.bicSearchMessage).should.equal(false);
    }

    selectIBANSearchResult() {
        Actions.click(this.sendBankDetailsScreen.ibanSearchMessage);
    }

    selectBicSearchResult() {
        Actions.click(this.sendBankDetailsScreen.bicSearchMessage);
    }
}

module.exports = ShareIBANPage;
