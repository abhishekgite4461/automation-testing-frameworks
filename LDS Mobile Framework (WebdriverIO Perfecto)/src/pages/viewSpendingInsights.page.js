const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class ViewSpendingInsightsWebviewPage extends AuthPage {
    /**
     * @returns {ViewSpendingInsightsScreen}
     */
    get viewSpendingInsights() {
        return this.getNativeScreen('viewSpendingInsights.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.viewSpendingInsights.title, 5000);
    }

    selectDebitCardOption() {
        Actions.waitForVisible(this.viewSpendingInsights.debitCard, 5000);
        Actions.click(this.viewSpendingInsights.debitCard);
    }

    shouldSeeDebitCardInsightPage() {
        Actions.waitForVisible(this.viewSpendingInsights.debitCardInsights, 5000);
    }
}

module.exports = ViewSpendingInsightsWebviewPage;
