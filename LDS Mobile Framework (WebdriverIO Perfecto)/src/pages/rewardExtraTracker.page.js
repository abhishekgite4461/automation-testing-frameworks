require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class RewardExtraTrackerWebviewPage extends AuthPage {
    get rewardExtraTracker() {
        return this.getWebScreen('addRewardExtra.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.rewardExtraTracker.titleRCAIneligibility, 30000);
    }

    shouldSeeIneligibilityForRCA() {
        Actions.isVisible(this.rewardExtraTracker.titleRCAIneligibility).should.equal(true);
    }

    waitForPageLoadURCA() {
        Actions.waitForVisible(this.rewardExtraTracker.titleUrcaRewardExtra, 30000);
    }

    shouldSeeRewardExtraTrackerForURCA() {
        Actions.waitForVisible(this.rewardExtraTracker.offerProgressMessage, 3000);
    }

    selectShowMyOfferProgress() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.rewardExtraTracker.showMyOfferProgress);
        Actions.isVisible(this.rewardExtraTracker.showMyOfferProgress).should.equal(true);
    }
}
module.exports = RewardExtraTrackerWebviewPage;
