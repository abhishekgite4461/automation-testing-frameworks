const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');

class FscsMobileBankingPage extends AuthPage {
    /**
     * @returns {FscsMobileBankingScreen}
     */
    get fscsMobileBankingScreen() {
        return this.getWebScreen('fscsMobileBanking.screen');
    }

    /**
     * @returns {BrowserScreen}
     */
    get browserScreen() {
        return this.getNativeScreen('browser.screen');
    }

    /**
     * @returns {FscsScreen}
     */
    get fscsScreen() {
        return this.getNativeScreen('fscs.screen');
    }

    waitForPageLoad() {
        if (DeviceHelper.isPerfecto()) {
            Actions.waitForVisible(this.fscsMobileBankingScreen.title);
        } else {
            Actions.waitForVisible(this.fscsScreen.compensationSchemeLabel, 50000);
        }
    }

    closeAllTabs() {
        if (DeviceHelper.isPerfecto()) {
            if (DeviceHelper.isAndroid()) {
                Actions.openDefaultBrowser();
                if (Actions.isVisible(this.browserScreen.chromeBrowserTabs)) {
                    Actions.click(this.browserScreen.chromeBrowserTabs);
                    Actions.click(this.browserScreen.chromeMenuButton);
                    Actions.click(this.browserScreen.chromeCloseAllTabs);
                    if (Actions.isVisible(this.browserScreen.chromeNewTabButton)) {
                        Actions.pressBackKey();
                    }
                }

                if (Actions.isVisible(this.browserScreen.androidBrowserTabs)) {
                    Actions.click(this.browserScreen.androidBrowserTabs);
                    Actions.click(this.browserScreen.androidcloseAllTabs);
                    Actions.pressBackKey();
                }
            }
        }
    }
}

module.exports = FscsMobileBankingPage;
