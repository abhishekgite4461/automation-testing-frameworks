require('chai').should();
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');

class LoginPage extends UnAuthPage {
    /**
     * @returns {LoginScreen}
     */
    get loginScreen() {
        return this.getNativeScreen('login.screen');
    }

    isPageVisible() {
        Actions.isVisible(this.loginScreen.title).should.equal(true);
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.loginScreen.title);
    }

    waitForOldLoginPage() {
        // MQE-1075 TODO
        Actions.waitForVisible(this.loginScreen.oldTitle);
    }

    login(username, password) {
        this.enterUsernameAndPassword(username, password);
        Actions.click(this.loginScreen.continueButton);
        Actions.reportTimer('Login', 'Memorable Information');
    }

    enterUsernameAndPassword(username, password) {
        this.enterUsername(username);
        this.enterPassword(password);
    }

    enterUsername(username) {
        Actions.setImmediateValue(this.loginScreen.userIdField, username);
    }

    static clickToolTipOption(toolTipOption) {
        Actions.clickOnText(toolTipOption);
    }

    copyContentFromTextField(inputField) {
        Actions.longPressed(this.loginScreen[`${inputField}Field`]);
        if (DeviceHelper.isAndroid()) {
            LoginPage.clickToolTipOption('SELECT ALL');
            LoginPage.clickToolTipOption('COPY');
        } else {
            Actions.click(this.loginScreen.selectAllOptionInToolTip);
            Actions.click(this.loginScreen.copyOptionInToolTip);
        }
    }

    pasteContentInTextField(inputField) {
        if (inputField === 'userId') {
            if (DeviceHelper.isAndroid()) {
                Actions.click(this.loginScreen.passwordField);
                Actions.click(this.loginScreen.userIdField);
                Actions.longPressed(this.loginScreen[`${inputField}Field`]);
                LoginPage.clickToolTipOption('PASTE');
            } else {
                Actions.longPressed(this.loginScreen[`${inputField}Field`]);
                Actions.click(this.loginScreen[`${inputField}Field`]);
                Actions.longPressed(this.loginScreen[`${inputField}Field`]);
                Actions.click(this.loginScreen.pasteOptionInToolTip);
            }
        } else {
            Actions.click(this.loginScreen[`${inputField}Field`]);
            if (DeviceHelper.isAndroid()) {
                Actions.longPressed(this.loginScreen[`${inputField}Field`]);
                LoginPage.clickToolTipOption('PASTE');
            } else {
                Actions.click(this.loginScreen.passwordField);
                Actions.click(this.loginScreen.pasteOptionInToolTip);
            }
        }
    }

    checkIfTextIsPresent(text, field) {
        if (field === 'username') {
            Actions.getText(this.loginScreen.userIdField).should.equal(text);
        } else {
            Actions.getText(this.loginScreen.passwordField).should.equal(text);
        }
    }

    copyOptionNotAvailableInPasswordField() {
        Actions.longPressed(this.loginScreen.passwordField);
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(() => Actions.perfectoFindText('SELECT ALL')).should.equal(false);
        } else {
            Actions.click(this.loginScreen.passwordField);
            Actions.longPressed(this.loginScreen.passwordField);
            Actions.isVisible(this.loginScreen.copyOptionInToolTip).should.equal(false);
        }
    }

    pasteOptionAvailableInUserIdField() {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.loginScreen.passwordField);
            Actions.click(this.loginScreen.userIdField);
            Actions.longPressed(this.loginScreen.userIdField);
            LoginPage.clickToolTipOption('PASTE');
        } else {
            Actions.longPressed(this.loginScreen.userIdField);
            Actions.click(this.loginScreen.userIdField);
            Actions.longPressed(this.loginScreen.userIdField);
            Actions.isVisible(this.loginScreen.pasteOptionInToolTip).should.equal(true);
        }
    }

    enterPassword(password) {
        Actions.setImmediateValue(this.loginScreen.passwordField, password);
    }

    canSeeErrorMessage() {
        Actions.waitForVisible(this.loginScreen.userIdErrorMessage);
    }

    shouldSeeContinueButtonDisabled() {
        Actions.isEnabled(this.loginScreen.continueButton).should.equal(false);
    }

    openPreAuthMenu() {
        Actions.click(this.loginScreen.preAuthMenu);
    }

    openForgotUserNameWebPage() {
        Actions.waitForVisible(this.loginScreen.loginForgotYourPassword);

        Actions.click(this.loginScreen.loginForgotYourPassword);
    }

    canSeeMandateLessErrorMessage() {
        Actions.click(this.loginScreen.mandateLessErrorMessage);
    }

    passwordFieldShouldBeMasked() {
        if (DeviceHelper.isAndroid()) {
            Actions.getAttribute(this.loginScreen.passwordField, 'password').should.equal('true');
        } else {
            Actions.getTagName(this.loginScreen.passwordField).toLowerCase().indexOf('securetext').should.not.equal(-1);
        }
    }

    checkNumberOfUsernameCharacters(noOfCharacters) {
        Actions.getText(this.loginScreen.userIdField).length.toString()
            .should.equal(noOfCharacters);
    }

    selectContinueButton() {
        Actions.click(this.loginScreen.continueButton);
    }

    checkNumberOfPasswordCharacters(noOfCharacters) {
        // below password field doesn't retrieve any values to validate in android
        if (!DeviceHelper.isAndroid()) {
            Actions.getText(this.loginScreen.passwordField).indexOf(noOfCharacters)
                .should.not.equal(-1);
        } else {
            throw new Error('Number of characters cannot be validated for password field in Android!!');
        }
    }

    selectRegisterForIB() {
        Actions.hideDeviceKeyboard();
        Actions.click(this.loginScreen.registerForIb);
    }

    canSeeRegisterForIB(flag) {
        Actions.isVisible(this.loginScreen.registerForIb)
            .should.equal(flag);
    }

    validateErrorMessage() {
        Actions.isExisting(this.loginScreen.errorMessage).should.equal(true);
    }

    clearUsernamePassword() {
        Actions.clearText(this.loginScreen.userIdField);
        Actions.clearText(this.loginScreen.passwordField);
    }

    closeErrorMessage() {
        Actions.click(this.loginScreen.errorMessageDismiss);
    }

    hardTokenErrorMessage() {
        Actions.waitForVisible(this.loginScreen.errorMessage);
    }

    specialCharactersRestrictionMessage(state) {
        Actions.isVisible(this.loginScreen.specialCharactersRestrictionMessage)
            .should.equal(state);
    }

    updateYourPasswordLink(state) {
        Actions.isVisible(this.loginScreen.updateYourPasswordLink)
            .should.equal(state);
    }
}

module.exports = LoginPage;
