const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');
const DeviceHelper = require('../utils/device.helper');
const AdbHelper = require('../utils/adb.helper');

const OPEN_BANKING = 'open banking';
const HALIFAX = 'halifax';
const SORT_CODE = 'Sort code';
const ACCOUNT_NUMBER = 'Account number';
const CARD_NUMBER = 'Card ending in';
const AVAILABLE_CREDIT = 'Available credit';
const BALANCE = 'balance.';
const BALANCE_NA = 'balance. Not available';

class AccountsOverviewPage extends AuthPage {
    get accountsOverviewScreen() {
        return this.getNativeScreen('accountsOverview.screen');
    }

    get homeScreen() {
        return this.getNativeScreen('home.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.accountsOverviewScreen.title);
    }

    validateAccounts(accountName) {
        const accountLabel = Actions.modifyAccountName(accountName);
        Actions.swipeUntilVisible(SwipeAction.UP,
            this.homeScreen.accountNameLabel(accountLabel));
    }

    validateAlert(accountName) {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.accountsOverviewScreen.alertMessage(accountName),
            0, true, 20
        );
    }

    shouldSeeAccountLink(accountName) {
        Actions.isVisible(
            this.homeScreen.accountlinkLead(accountName)
        ).should.equal(true);
    }

    shouldSeeGhostTile(isShown) {
        if (isShown) {
            const waitForMethod = () => Actions.isExisting(this.accountsOverviewScreen.accountAggregationLoadingTile);
            Actions.waitUntil(waitForMethod, 6000, 'Ghost tile was expected to be visible', 5);
        } else {
            Actions.isExisting(this.accountsOverviewScreen.accountAggregationLoadingTile).should.equal(isShown);
        }
    }

    selectGhostTile() {
        Actions.click(this.accountsOverviewScreen.accountAggregationLoadingTile);
    }

    waitForGhostTileToDisappear() {
        const waitForMethod = () => !Actions.isExisting(this.accountsOverviewScreen.accountAggregationLoadingTile);
        Actions.waitUntil(waitForMethod, 6000, 'Ghost tile was expected to be not visible', 500);
    }

    shouldSeeExternalAccountsLoaded(isShown, account) {
        if (isShown) {
            this.waitForGhostTileToDisappear();
            if (DeviceHelper.isIOS() || (DeviceHelper.isAndroid() && account === 'accounts')) {
                Actions.swipeByPercentUntilVisible(this.accountsOverviewScreen
                    .externalAccountTiles, 80, 80, 60, 60, 100, 10);
            } else if (DeviceHelper.isAndroid() && (account === 'error accounts' || account === 'expired accounts')) {
                Actions.swipeByPercentUntilVisible(this.accountsOverviewScreen
                    .externalErrorAccountTiles, 80, 80, 60, 60, 100, 10);
            }
        } else {
            for (let i = 1; i <= 4; i++) {
                if (DeviceHelper.isIOS() || (DeviceHelper.isAndroid() && account === 'accounts')) {
                    Actions.isVisible(this.accountsOverviewScreen.externalAccountTiles).should.equal(isShown);
                } else if (DeviceHelper.isAndroid() && (account === 'error accounts' || account === 'expired accounts')) {
                    Actions.isVisible(this.accountsOverviewScreen.externalErrorAccountTiles).should.equal(isShown);
                }
                Actions.swipePage(SwipeAction.UP);
            }
        }
    }

    shouldSeeAccountAggregationTile(isShown) {
        if (isShown) {
            Actions.swipeByPercentUntilVisible(this.accountsOverviewScreen
                .openBankingPromoTile, 60, 60, 20, 20, 100, 25);
        } else {
            for (let i = 1; i <= 3; i++) {
                Actions.isVisible(this.accountsOverviewScreen.openBankingPromoTile).should.equal(isShown);
                Actions.swipePage(SwipeAction.UP);
            }
            Actions.isVisible(this.accountsOverviewScreen.openBankingPromoTile).should.equal(isShown);
        }
    }

    shouldSeeAddExternalAccount() {
        Actions.swipeByPercentUntilVisible(this.accountsOverviewScreen.addExternalAccount, 60, 60, 40, 40, 100, 25);
    }

    shouldNotSeeViewExternalAccount() {
        this.shouldSeeAddExternalAccount();
        Actions.isVisible(this.accountsOverviewScreen.viewExternalAccount).should.equal(false);
    }

    selectAddExternalAccount() {
        this.waitForGhostTileToDisappear();
        this.shouldSeeAddExternalAccount();
        Actions.click(this.accountsOverviewScreen.addExternalAccount);
    }

    shouldSeeFlagOnExternalProviderAccountTile(isVisible, flag, bankName, accountType) {
        Actions.swipePage(SwipeAction.DOWN);
        Actions.swipeByPercentUntilVisible(this.homeScreen.externalProviderAccountTile(bankName, accountType),
            80, 80, 60, 60, 100, 10);

        if (DeviceHelper.isIOS()) {
            const loc = Actions.getElementsListAttributeText(this.homeScreen.externalProviderAccountTile(bankName,
                accountType))[0];
            if (isVisible) {
                loc.should.contains(flag);
            } else {
                loc.should.not.contains(flag);
            }
        } else if (isVisible) {
            Actions.swipeByPercentUntilVisible(this.accountsOverviewScreen
                .externalProviderAccountFlag(bankName, accountType), 80, 80, 60, 60, 100, 10);
            Actions.getAttributeText(this.accountsOverviewScreen.externalProviderAccountFlag(bankName, accountType), 'text')
                .should.contains(flag);
        } else {
            Actions.swipePage(SwipeAction.UP);
            Actions.isVisible(this.accountsOverviewScreen.externalProviderAccountFlag(bankName, accountType))
                .should.equal(false);
        }
    }

    shouldSeeExternalAccountTilesAbovePromoTile() {
        let arrayList = [];

        // Fetching the Elements list and storing in an array list
        if (DeviceHelper.isAndroid()) {
            this.shouldSeeRewardsTile(true);
            Actions.getElementList(this.accountsOverviewScreen.arrangementsList)
                .forEach((elementId) => arrayList.push(Actions.elementIdAttribute(elementId, 'contentDesc')));
        } else {
            arrayList = Actions.getElementsListAttributeText(this.accountsOverviewScreen.arrangementsList)
                .filter((item) => item);
        }

        // Validating that the last element in the list is OB promo tile and 2nd last element is External accounts tile.
        arrayList[DeviceHelper.isIOS() ? arrayList.length - 1 : arrayList.length - 2].toLowerCase()
            .should.contains(OPEN_BANKING);
        arrayList[DeviceHelper.isIOS() ? arrayList.length - 2 : arrayList.length - 3].toLowerCase()
            .should.contains(HALIFAX);
    }

    shouldSeeRewardsTile(isShown) {
        this.shouldSeeAddExternalAccount();
        Actions.swipePage(SwipeAction.UP);
        Actions.isVisible(this.accountsOverviewScreen.earnCashbackTile).should.equal(isShown);
    }

    shouldSeeExternalBankFullErrorTile(errorType, bankName) {
        Actions.pause(5000);
        Actions.swipeByPercentUntilVisible(this.homeScreen.externalBankAccountErrorTile(bankName),
            80, 80, 60, 60, 100, 10);
        const actualMessage = Actions
            .getElementsListAttributeText(this.homeScreen.externalBankAccountErrorTile(bankName))[0].toLowerCase();
        switch (errorType.toLowerCase()) {
        case 'full':
            actualMessage.should
                .contains(`sorry, we can't show your ${bankName.toLowerCase()} account details at the moment.`);
            break;
        case 'expired':
            actualMessage.should
                .contains(`your added account with ${bankName.toLowerCase()} has expired.`);
            break;
        default:
            throw new Error(`Error type ${errorType} not present`);
        }
    }

    shouldSeeOBPromoTileAsLastTile() {
        this.shouldSeeRewardsTile(false);
        if (DeviceHelper.isAndroid()) {
            const arrayList = [];
            Actions.getElementList(this.accountsOverviewScreen.arrangementsList)
                .forEach((elementId) => arrayList.push(Actions.elementIdAttribute(elementId,
                    DeviceHelper.isPerfecto() ? 'contentDesc' : 'content-desc')));
            arrayList[arrayList.length - 1].toLowerCase().should.contains(OPEN_BANKING);
        } else {
            Actions.getElementsListAttributeText(this.accountsOverviewScreen.arrangementsList)
                .pop().toString().toLowerCase().should.contains(OPEN_BANKING);
        }
    }

    shouldSeeDefaultExternalAccountDetails(bankName, accountType) {
        let val = null;
        Actions.swipeByPercentUntilVisible(this.homeScreen.externalProviderAccountTile(bankName, accountType),
            80, 80, 60, 60, 100, 10);
        Actions.isVisible(this.accountsOverviewScreen.externalProviderAccountActionMenu(bankName,
            Actions.modifyAccountName(accountType, DeviceHelper.isIOS())))
            .should.equal(true);

        if (DeviceHelper.isIOS()) {
            val = Actions.getElementsListAttributeText(this.homeScreen.externalProviderAccountTile(bankName,
                accountType))[0];
            val.split(',')[0].should.contains(bankName);
            val.should.contains(BALANCE);
        } else {
            Actions.isVisible(this.accountsOverviewScreen.externalProviderAccountLogo(bankName, accountType))
                .should.equal(true);
            Actions.isVisible(this.accountsOverviewScreen.externalProviderAccountName(bankName, accountType))
                .should.equal(true);
            Actions.isVisible(this.accountsOverviewScreen.externalProviderAccountBalance(bankName, accountType))
                .should.equal(true);
        }
        return val;
    }

    static shouldSeeOtherExternalAccountDetails(val, expectedText, loc) {
        if (DeviceHelper.isIOS()) {
            val.should.contains(expectedText);
        } else {
            Actions.isVisible(loc).should.equal(true);
        }
    }

    shouldSeeInformationInExternalAccountTile(bankName, accountType, accountTileFields) {
        const val = this.shouldSeeDefaultExternalAccountDetails(bankName,
            Actions.modifyAccountName(accountType, DeviceHelper.isIOS()));
        switch (accountType.toLowerCase()) {
        case 'current account':
        case 'current':
        case 'savings':
            for (let i = 0; i < accountTileFields.length; i++) {
                switch (`${accountTileFields[i]}`) {
                case 'sortCode':
                    AccountsOverviewPage.shouldSeeOtherExternalAccountDetails(val, SORT_CODE,
                        DeviceHelper.isAndroid() ? this.accountsOverviewScreen.externalProviderAccountSortCode(bankName, accountType) : '');
                    break;

                case 'accountNumber':
                    AccountsOverviewPage.shouldSeeOtherExternalAccountDetails(val, ACCOUNT_NUMBER,
                        DeviceHelper.isAndroid() ? this.accountsOverviewScreen.externalProviderAccountNumber(bankName, accountType) : '');
                    break;
                default:
                    throw new Error(`Unknown account field: ${accountTileFields[i]}`);
                }
            }
            break;
        case 'credit':
            for (let i = 0; i < accountTileFields.length; i++) {
                switch (`${accountTileFields[i]}`) {
                case 'creditCardNumber':
                    AccountsOverviewPage.shouldSeeOtherExternalAccountDetails(val, CARD_NUMBER,
                        DeviceHelper.isAndroid() ? this.accountsOverviewScreen.externalProviderCreditCardNumber(bankName, accountType) : '');
                    break;

                case 'availableCredit':
                    AccountsOverviewPage.shouldSeeOtherExternalAccountDetails(val, AVAILABLE_CREDIT,
                        DeviceHelper.isAndroid() ? this.accountsOverviewScreen.externalProviderAvailableCredit(bankName, accountType) : '');
                    break;
                default:
                    throw new Error(`Unknown account field: ${accountTileFields[i]}`);
                }
            }
            break;
        default:
            throw new Error(`Unknown account type: ${accountType}`);
        }
    }

    shouldSeeBalanceOnExternalProviderAccountTile(balance, bankName, accountType) {
        Actions.swipeByPercentUntilVisible(this.homeScreen.externalProviderAccountTile(bankName, accountType),
            80, 80, 60, 60, 100, 10);
        if (DeviceHelper.isIOS()) {
            const actualText = Actions
                .getElementsListAttributeText(this.homeScreen.externalProviderAccountTile(bankName, accountType))[0];
            if (balance === 'N/A') {
                actualText.should.contains(BALANCE_NA);
            } else {
                actualText.should.contains(`balance. ${balance}`);
            }
        } else {
            Actions.getAttributeText(this.accountsOverviewScreen.externalProviderAccountBalance(bankName, accountType))
                .should.contains(balance);
        }
    }

    shouldSeeCovid19Tile() {
        Actions.swipeByPercentUntilVisible(this.accountsOverviewScreen.covid19Tile, 60, 60, 40, 40, 100, 30);
    }

    shouldSeeRewardHubEntryTile() {
        Actions.swipeByPercentUntilVisible(this.accountsOverviewScreen.rewardHubEntryTile, 60, 60, 40, 40, 100, 40);
    }

    selectFindOutMoreOnRewardHubEntryTile() {
        Actions.swipeByPercentUntilVisible(this.accountsOverviewScreen.findOutMoreOnRewardHubEntryTile,
            60, 60, 40, 40, 100, 30);
        Actions.click(this.accountsOverviewScreen.findOutMoreOnRewardHubEntryTile);
    }

    selectFindOutMoreOnCovid19Tile() {
        Actions.swipeByPercentUntilVisible(this.accountsOverviewScreen.findOutMoreOnCovid19Tile,
            60, 60, 40, 40, 100, 30);
        Actions.click(this.accountsOverviewScreen.findOutMoreOnCovid19Tile);
    }

    // Following function is only applicable for Android as we can pnly access logs for Android devices from Perfecto
    shouldSeeAnalyticsForCovid19Tile() {
        if (DeviceHelper.isAndroid()) {
            AdbHelper.adbLogs(this).should.contains('Event Property: EVENT_NARRATIVE(EventNarrative) --> view information on covid-19');
        }
    }

    // Following function is only applicable for Android as we can pnly access logs for Android devices from Perfecto
    shouldSeeAnalyticsForHalifaxRewardHubTile() {
        if (DeviceHelper.isAndroid()) {
            AdbHelper.adbLogs(this).should.contains('Event Property: EVENT_NARRATIVE(EventNarrative) --> view reward hub');
        }
    }
}

module.exports = AccountsOverviewPage;
