require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class InterestRateSavingAccount extends AuthPage {
    /**
     * @returns {InterestRateSavingAccountScreen}
     */
    get interestRateSavingAccountScreen() {
        return this.getNativeScreen('interestRateSavingAccount.screen');
    }

    shouldSeeInterestRate() {
        Actions.isVisible(this.interestRateSavingAccountScreen.availableBalalce);
        Actions.isVisible(this.interestRateSavingAccountScreen.interestRatetText)
            .should.equal(true);
        Actions.getText(this.interestRateSavingAccountScreen.interestRatetText).should.not.equal('');
    }
}
module.exports = InterestRateSavingAccount;
