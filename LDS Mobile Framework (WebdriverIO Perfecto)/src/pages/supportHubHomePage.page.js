const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');
const DeviceHelper = require('../utils/device.helper');
require('chai').should();

const LostAndStolenCardPage = require('./lostAndStolen.page');
const ReplacementCardAndPinPage = require('./replacementCardAndPinPage.page');
const PersonalDetailsPage = require('./settings/personalDetails/personalDetails.page');
const ResetYourPasswordPage = require('./settings/resetYourPassword.page');
const CommonPage = require('./common.page');
const MobileBrowserPage = require('./mobileBrowser.page');
const Covid19MobileBrowserSupportPage = require('./covid19MobileBrowserSupport.page.js');

const personalDetailsPage = new PersonalDetailsPage();
const lostAndStolenCardPage = new LostAndStolenCardPage();
const resetYourPasswordPage = new ResetYourPasswordPage();
const replacementCardAndPinPage = new ReplacementCardAndPinPage();
const commonPage = new CommonPage();
const mobileBrowserPage = new MobileBrowserPage();
const covid19MobileBrowserSupportPage = new Covid19MobileBrowserSupportPage();

class SupportHubHomePage extends AuthPage {
    /**
     * @returns {SupportHubHomePageScreen}
     */
    get supportHubHomePageScreen() {
        return this.getNativeScreen('supportHubHomePage.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.supportHubHomePageScreen.title);
    }

    waitForOverlay() {
        Actions.waitForVisible(this.supportHubHomePageScreen.overLayTitle);
    }

    selectCallUs() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.callUsButton);
        Actions.click(this.supportHubHomePageScreen.callUsButton);
        Actions.reportTimer('Call_US', 'call about');
    }

    selectMobileChat() {
        Actions.click(this.supportHubHomePageScreen.mobileChatButton);
    }

    shouldDisplayCMASurveytile() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.cmaSurvey);
    }

    shouldDisplayATMandBranchFindertile() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.atmBranchFinder);
    }

    shouldDisplayAppFeedbacktile() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.appFeedback);
    }

    shouldNotDisplayCMASurveyLink() {
        for (let i = 0; i < 2; i++) {
            Actions.isVisible(this.supportHubHomePageScreen.cmaSurvey).should.equal(false);
            Actions.swipePage(SwipeAction.UP);
        }
    }

    selectCMASurveyLink() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.cmaSurvey);
        Actions.click(this.supportHubHomePageScreen.cmaSurvey);
    }

    verifyMobileChatVisibility(mobileChat) {
        Actions.isVisible(this.supportHubHomePageScreen.mobileChatButton).should.equal(mobileChat);
    }

    verifyMobileChatButtonName(label) {
        let chatButtonText;
        if (DeviceHelper.isIOS()) {
            chatButtonText = Actions.getText(this.supportHubHomePageScreen.mobileChatButton);
        } else {
            chatButtonText = Actions.getText(this.supportHubHomePageScreen.mobileChatButtonLabel(label));
        }
        chatButtonText.should.equal(label);
    }

    shouldDisplayCallUsButton() {
        Actions.isVisible(this.supportHubHomePageScreen.callUsButton).should.equal(true);
    }

    selectResetPassword() {
        Actions.click(this.supportHubHomePageScreen.resetPassword);
    }

    selectReplacementCardAndPin() {
        Actions.click(this.supportHubHomePageScreen.replaceCardOrPin);
    }

    selectChangeOfAddress() {
        Actions.click(this.supportHubHomePageScreen.changeOfAddress);
        Actions.reportTimer('Change_Of_Address', 'contact details');
    }

    selectLostAndStolenCard() {
        Actions.click(this.supportHubHomePageScreen.lostOrStolenCard);
    }

    selectATMBranchFinderTile() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.atmBranchFinder);
        Actions.click(this.supportHubHomePageScreen.atmBranchFinder);
    }

    selectProvideAppFeedback() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.appFeedback);
        Actions.click(this.supportHubHomePageScreen.appFeedback);
    }

    selectSuspectFraud() {
        Actions.click(this.supportHubHomePageScreen.suspectFraud);
    }

    selectDismissOption() {
        Actions.click(this.supportHubHomePageScreen.dismissOverLay);
    }

    selectMortgagePaymentHolidaysLink() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.mortgagePaymentHolidaysLink, 20, 80, 20, 70);
        Actions.click(this.supportHubHomePageScreen.mortgagePaymentHolidaysLink);
    }

    selectCreditcardPaymentHolidaysLink() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.creditcardPaymentHolidaysLink, 20, 80, 20, 70);
        Actions.click(this.supportHubHomePageScreen.creditcardPaymentHolidaysLink);
    }

    selectLoanPaymentHolidaysLink() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.loanPaymentHolidaysLink, 20, 80, 20, 70);
        Actions.click(this.supportHubHomePageScreen.loanPaymentHolidaysLink);
    }

    selectOverdraftChargesLink() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.overdraftChargesLink, 20, 80, 20, 70);
        Actions.click(this.supportHubHomePageScreen.overdraftChargesLink);
    }

    selectTravelAndDisruptionCoverLink() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.travelAndDisruptionCoverLink, 20, 80, 20, 70);
        Actions.click(this.supportHubHomePageScreen.travelAndDisruptionCoverLink);
    }

    selectMoreInfoAboutCoronaVirusLink() {
        Actions.swipeByPercentUntilVisible(this.supportHubHomePageScreen.moreInfoAboutCoronavirusLink, 20, 80, 20, 70);
        Actions.click(this.supportHubHomePageScreen.moreInfoAboutCoronavirusLink);
    }

    selectUpdateDetailsButton() {
        Actions.click(this.supportHubHomePageScreen.updateDetailsButton);
    }

    waitForEmailAccountSelectionOverlay() {
        Actions.waitForVisible(this.supportHubHomePageScreen.emailAccountSelectionOverlay);
    }

    validateCarousels(supportHubCarouselOptions) {
        for (let i = 0; i < supportHubCarouselOptions.length; i++) {
            const totalcarousels = Actions.getElementsListAttributeText(this.supportHubHomePageScreen.carousals).length;
            const tileDisplayed = this.supportHubHomePageScreen[supportHubCarouselOptions[i].carouselOptions]
                .toLowerCase();
            if (totalcarousels === 3 && tileDisplayed.includes('changeofaddress')) {
                Actions.isVisible(tileDisplayed).should.equal(false);
            } else {
                this.selectCarousel(tileDisplayed);
            }
        }
    }

    selectCarousel(options) {
        if (options.includes('replacecard')) {
            this.selectReplacementCardAndPin();
            replacementCardAndPinPage.waitForPageLoad();
            commonPage.clickBackButton();
        } else if (options.includes('resetpassword')) {
            this.selectResetPassword();
            resetYourPasswordPage.waitForPageLoad();
            commonPage.clickBackButton();
        } else if (options.includes('change')) {
            this.selectChangeOfAddress();
            personalDetailsPage.isPageVisible();
            commonPage.clickBackButton();
        } else if (options.includes('stolen')) {
            this.selectLostAndStolenCard();
            lostAndStolenCardPage.waitForPageLoad();
            commonPage.clickBackButton();
        } else {
            throw new Error(`Unknown carousel option: ${options}`);
        }
    }

    shouldDisplayUpdateDetailsButton() {
        Actions.isVisible(this.supportHubHomePageScreen.updateDetailsButton).should.equal(true);
    }

    validateCovid19AndSupportLinks(usefulSupportLinks) {
        for (let i = 0; i < usefulSupportLinks.length; i++) {
            const linkDisplayed = this.supportHubHomePageScreen[usefulSupportLinks[i].usefulLinks];
            this.selectLink(linkDisplayed);
        }
    }

    selectLink(options) {
        switch (options) {
        case this.supportHubHomePageScreen.mortgagePaymentHolidaysLink:
            this.selectMortgagePaymentHolidaysLink();
            mobileBrowserPage.waitForMobileBroswerPageLoad();
            covid19MobileBrowserSupportPage.verifyMortgagePaymentHolidayUrl();
            mobileBrowserPage.selectReturnToApp();
            break;
        case this.supportHubHomePageScreen.creditcardPaymentHolidaysLink:
            this.selectCreditcardPaymentHolidaysLink();
            mobileBrowserPage.waitForMobileBroswerPageLoad();
            covid19MobileBrowserSupportPage.verifyCreditCardPaymentHolidayUrl();
            mobileBrowserPage.selectReturnToApp();
            break;
        case this.supportHubHomePageScreen.loanPaymentHolidaysLink:
            this.selectLoanPaymentHolidaysLink();
            mobileBrowserPage.waitForMobileBroswerPageLoad();
            covid19MobileBrowserSupportPage.verifyLoanPaymentHolidayUrl();
            mobileBrowserPage.selectReturnToApp();
            break;
        case this.supportHubHomePageScreen.overdraftChargesLink:
            this.selectOverdraftChargesLink();
            mobileBrowserPage.waitForMobileBroswerPageLoad();
            covid19MobileBrowserSupportPage.verifyOverdraftChargesUrl();
            mobileBrowserPage.selectReturnToApp();
            break;
        case this.supportHubHomePageScreen.travelAndDisruptionCoverLink:
            this.selectTravelAndDisruptionCoverLink();
            mobileBrowserPage.waitForMobileBroswerPageLoad();
            covid19MobileBrowserSupportPage.verifyTravelAndDisruptionCoverUrl();
            mobileBrowserPage.selectReturnToApp();
            break;
        case this.supportHubHomePageScreen.moreInfoAboutCoronavirusLink:
            this.selectMoreInfoAboutCoronaVirusLink();
            mobileBrowserPage.waitForMobileBroswerPageLoad();
            covid19MobileBrowserSupportPage.verifyMoreInfoAboutCoronavirusUrl();
            mobileBrowserPage.selectReturnToApp();
            break;
        default:
            throw new Error(`Given option "${options}" not found`);
        }
    }

    validateUpdateDetailsButton() {
        this.selectUpdateDetailsButton();
        personalDetailsPage.isPageVisible();
        commonPage.clickBackButton();
    }

    selectViewPinOrReplaceCard() {
        Actions.click(this.supportHubHomePageScreen.viewPinOrReplaceCard);
    }

    selectViewPinOption() {
        Actions.click(this.supportHubHomePageScreen.viewPinTile);
    }

    shouldSeeViewPinEntry() {
        Actions.isVisible(this.supportHubHomePageScreen.viewPinOrReplaceCard).should.equal(true);
    }

    selectCovidHubLink() {
        Actions.click(this.supportHubHomePageScreen.covidHubLink);
    }

    shouldSeeCovidHubNativePage() {
        Actions.isVisible(this.supportHubHomePageScreen.covidHubNativePage).should.equal(true);
    }
}

module.exports = SupportHubHomePage;
