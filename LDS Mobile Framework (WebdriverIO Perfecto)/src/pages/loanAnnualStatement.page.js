const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class LoanAnnualStatementPage extends AuthPage {
    /**
     * @returns {LoanAnnualStatement}
     */
    get loanAnnualStatement() {
        return this.getNativeScreen('loanAnnualStatement.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.loanAnnualStatement.title);
    }
}

module.exports = LoanAnnualStatementPage;
