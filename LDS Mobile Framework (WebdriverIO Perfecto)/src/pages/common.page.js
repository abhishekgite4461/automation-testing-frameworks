require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');
const logger = require('../utils/logger');

class CommonPage extends AuthPage {
    /**
     * @returns {CommonScreen}
     */
    get commonScreen() {
        return this.getNativeScreen('common/common.screen');
    }

    clickBackButton() {
        this.waitForSpinnerToComplete();
        Actions.click(this.commonScreen.backButton);
    }

    navigateHomeFromPaymentSuccess() {
        Actions.waitAndClick(this.commonScreen.modalCloseIcon);
    }

    closeModalAndLeave() {
        Actions.click(this.commonScreen.modalCloseIcon);
        if (Actions.isVisible(this.commonScreen.winBackTitle)) {
            Actions.click(this.commonScreen.winBackLeaveButton);
        }
    }

    waitForSpinnerToComplete() {
        if (!DeviceHelper.isStub()) {
            Actions.pause(2000); // wait for spinner to start
        }
        // spinner can take upto 3 mins on real environment
        Actions.waitForNotVisible(this.commonScreen.loadingSpinner, 180000);
    }

    navigateHomeFromStandingOrder() {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.commonScreen.closeButton);
            Actions.isVisible(this.commonScreen.winBackTitle).should.equal(true);
            Actions.click(this.commonScreen.winBackLeaveButton);
        } else {
            Actions.click(this.commonScreen.modalCloseIcon);
            Actions.isVisible(this.commonScreen.winBackTitle).should.equal(true);
            Actions.click(this.commonScreen.winBackLeaveButton);
        }
    }

    shouldSeeOptionsInWinback() {
        Actions.isVisible(this.commonScreen.winBackTitle).should.equal(true);
        Actions.isVisible(this.commonScreen.winBackStayButton).should.equal(true);
        Actions.isVisible(this.commonScreen.winBackLeaveButton).should.equal(true);
    }

    shouldClickLeaveOptionsInWinback() {
        Actions.click(this.commonScreen.winBackLeaveButton);
    }

    selectWinbackOptions(option) {
        if (option === 'Leave') {
            this.selectWinBackLeaveButton();
        } else if (option === 'Stay') {
            this.selectWinBackStayButton();
        } else if (option === 'OK') {
            this.selectOKButton();
        } else if (option === 'Cancel') {
            this.selectCancelButton();
        } else {
            logger.error('unrecognized options');
        }
    }

    selectWinBackStayButton() {
        Actions.waitAndClick(this.commonScreen.winBackStayButton);
    }

    selectOKButton() {
        Actions.waitAndClick(this.commonScreen.winBackOkButton);
    }

    selectCancelButton() {
        Actions.waitAndClick(this.commonScreen.winBackCancelButton);
    }

    selectWinBackLeaveButton() {
        Actions.click(this.commonScreen.winBackLeaveButton);
    }

    clickHomeButton() {
        Actions.click(this.commonScreen.homeButton);
    }

    selectHeaderBackButton() {
        Actions.click(this.commonScreen.headerBackButton);
    }

    selectModalHeaderBackButton() {
        Actions.click(this.commonScreen.modalBackButton);
    }

    shouldNotSeeBackButton() {
        Actions.isVisible(this.commonScreen.modalBackButton).should.equal(false);
    }

    closeModal() {
        Actions.waitAndClick(this.commonScreen.modalCloseIcon);
    }

    closeModalWhenPresent() {
        if (Actions.isVisible(this.commonScreen.modalCloseIcon)) {
            this.closeModalAndLeave();
        }
    }

    verifyCloseModal() {
        Actions.isVisible(this.commonScreen.modalCloseIcon).should.equal(true);
    }

    shouldNotSeeHeaderBackButton() {
        Actions.isVisible(this.commonScreen.backButton).should.equal(false);
    }

    shouldSeeHeaderBackButton() {
        Actions.isVisible(this.commonScreen.backButton).should.equal(true);
    }

    static setDeviceOrientation(orientation) {
        Actions.rotateDevice(orientation);
    }

    static getDeviceOrientation(orientation) {
        Actions.getOrientation().should.equal(orientation);
    }
}

module.exports = CommonPage;
