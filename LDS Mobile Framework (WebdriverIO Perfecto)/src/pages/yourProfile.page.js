require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class YourProfilePage extends AuthPage {
    /**
     * @returns {YourProfileScreen}
     */
    get yourProfileScreen() {
        return this.getNativeScreen('yourProfile.screen');
    }

    /**
     * @returns {PersonalDetailsScreen}
     */
    get personalDetailsScreen() {
        return this.getNativeScreen('settings/personalDetails/personalDetails.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.yourProfileScreen.title);
    }

    verifyMarketPreferenceIsNotVisible() {
        Actions.swipePage(SwipeAction.UP);
        Actions.isVisible(this.yourProfileScreen.toggleButtonOne).should.equal(false);
        Actions.isVisible(this.yourProfileScreen.toggleButtonTwo).should.equal(false);
        Actions.isVisible(this.yourProfileScreen.toggleButtonThree).should.equal(false);
    }

    selectDataConsentLink() {
        Actions.swipeByPercentUntilVisible(this.personalDetailsScreen.dataConsentLink);
        Actions.click(this.personalDetailsScreen.dataConsentLink);
    }
}

module.exports = YourProfilePage;
