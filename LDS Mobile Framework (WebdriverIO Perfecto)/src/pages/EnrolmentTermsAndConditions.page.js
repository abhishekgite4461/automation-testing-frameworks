const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
require('chai').should();

class EnrolmentTermsAndConditions extends AuthPage {
    /**
     * @returns {EnrolmentTermsAndConditions}
     */
    get enrolmentTermsAndConditionsScreen() {
        return this.getNativeScreen('enrolmentTermsAndConditions.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.enrolmentTermsAndConditionsScreen.title);
    }

    acceptTermsAndConditions() {
        Actions.click(this.enrolmentTermsAndConditionsScreen.acceptButton);
    }

    declineTermsAndConditions() {
        Actions.click(this.enrolmentTermsAndConditionsScreen.declineButton);
    }

    verifyAcceptButton() {
        Actions.isVisible(this.enrolmentTermsAndConditionsScreen.acceptButton).should.equal(true);
    }

    verifyDeclineButton() {
        Actions.isVisible(this.enrolmentTermsAndConditionsScreen.declineButton).should.equal(true);
    }
}

module.exports = EnrolmentTermsAndConditions;
