require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class AddRewardExtraWebView extends AuthPage {
    get addRewardExtra() {
        return this.getWebScreen('addRewardExtra.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.addRewardExtra.titleAddRewardExtra, 30000);
    }

    shouldSeeAlreadyHaveRewardExtras() {
        Actions.isVisible(this.addRewardExtra.alreadyHaveRewardExtras).should.equal(true);
    }

    selectBackToAccountOverviewTrackURCA() {
        Actions.click(this.addRewardExtra.backToAccountOverviewTrackURCA);
    }

    selectBackToAccountOverviewTrackRCA() {
        Actions.click(this.addRewardExtra.backToAccountOverviewTrackRCA);
    }

    shouldSeeAddRewardExtraLandingTitle() {
        Actions.waitForVisible(this.addRewardExtra.pageHeaderAddRewardExtra, 3000);
    }

    shouldSeeTrackRewardExtraLandingTitle() {
        Actions.waitForVisible(this.addRewardExtra.titleTrackRewardExtra, 3000);
    }

    shouldSeeAlreadyHaveRewardTitle() {
        Actions.waitForVisible(this.addRewardExtra.titleAlreadyHaveReward, 3000);
    }

    shouldSeeAlreadyHaveRewardMessage() {
        Actions.waitForVisible(this.addRewardExtra.AlreadyHaveRewardmessage, 3000);
    }

    validateAddRewardExtraOffers() {
        Actions.isVisible(this.addRewardExtra.addRewardExtraOffers).should.equal(true);
    }

    shouldSeeLastUpdatedDate() {
        Actions.isVisible(this.addRewardExtra.pageHeadingTrackRewardExtraDate).should.equal(true);
    }

    shouldSeeQualifyingStatus() {
        Actions.isVisible(this.addRewardExtra.titleTrackRewardQualifyingStatus).should.equal(true);
    }

    shouldSeeQualificationDescription() {
        Actions.isVisible(this.addRewardExtra.trackRewardExtraQualificationDesc).should.equal(true);
    }

    selectOfferProgress() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.addRewardExtra.trackRewardExtraProgress);
        Actions.click(this.addRewardExtra.trackRewardExtraProgress);
    }

    selectChooseHowToEarn() {
        Actions.click(this.addRewardExtra.chooseHowToEarn);
    }

    selectOfferAccordion() {
        Actions.click(this.addRewardExtra.offerAccordion);
    }

    selectProposition(proposition) {
        if (proposition.includes('propRewardFilmRental')) {
            Actions.click(this.addRewardExtra.propRewardFilmRental);
        } else if (proposition.includes('propRewardCinemaTicket')) {
            Actions.click(this.addRewardExtra.propRewardCinemaTicket);
        } else if (proposition.includes('propRewardDigitalMagazine')) {
            Actions.click(this.addRewardExtra.propRewardDigitalMagazine);
        } else {
            Actions.click(this.addRewardExtra.propRewardCashBack);
        }
    }

    shouldSeeRewardCategoryTitle() {
        Actions.isVisible(this.addRewardExtra.titleCategoryPage).should.equal(true);
    }

    selectCategory(category) {
        if (category.includes('Save')) {
            Actions.click(this.addRewardExtra.categorySaveAccordion);
            Actions.click(this.addRewardExtra.chooseThisOptionSave);
        } else {
            Actions.click(this.addRewardExtra.categorySpendAccordion);
            Actions.click(this.addRewardExtra.chooseThisOptionSpend);
        }
    }

    shouldSeeLetsGetStartedTitle() {
        Actions.isVisible(this.addRewardExtra.pageHeadingLetsGetStarted).should.equal(true);
    }

    selectContinueOnLetsGetStarted() {
        Actions.click(this.addRewardExtra.continueOnLetsGetStarted);
    }

    shouldSeeYourEmailTitle() {
        Actions.isVisible(this.addRewardExtra.pageHeadingYourEmail).should.equal(true);
    }

    selectYesButton() {
        Actions.click(this.addRewardExtra.yesButtonEmail);
    }

    selectContinueOnYourEmail() {
        Actions.click(this.addRewardExtra.continueOnYourEmail);
    }

    shouldSeeTheLegalBitsTitle() {
        Actions.isVisible(this.addRewardExtra.pageHeadingTheLegalBits).should.equal(true);
    }

    selectIConfirmCheckboxLegalBits() {
        Actions.click(this.addRewardExtra.iConfirmCheckBoxLegalBits);
    }

    shouldSeeRCAIneligibility() {
        Actions.pause(5000);
        Actions.swipeUntilVisible(SwipeAction.UP, this.addRewardExtra.rcaIneligibilityMessage);
        Actions.isVisible(this.addRewardExtra.rcaIneligibilityMessage);
    }
}
module.exports = AddRewardExtraWebView;
