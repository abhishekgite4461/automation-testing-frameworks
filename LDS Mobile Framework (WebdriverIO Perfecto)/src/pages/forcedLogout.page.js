const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class ForcedLogoutPage extends AuthPage {
    /**
     * @returns {ForcedLogoutScreen}
     */
    get forcedLogoutScreen() {
        return this.getNativeScreen('forcedLogout.screen');
    }

    isPageVisible() {
        Actions.isVisible(this.forcedLogoutScreen.title).should.equal(true);
    }

    isErrorIconVisible() {
        Actions.isVisible(this.forcedLogoutScreen.errorIcon).should.equal(true);
    }

    isErrorMessageVisible() {
        Actions.isVisible(this.forcedLogoutScreen.errorMessage).should.equal(true);
    }

    isGoToLogonButtonVisible() {
        Actions.isVisible(this.forcedLogoutScreen.logonButton).should.equal(true);
    }
}

module.exports = ForcedLogoutPage;
