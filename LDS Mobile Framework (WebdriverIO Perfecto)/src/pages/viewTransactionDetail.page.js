require('chai')
    .should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');
const DeviceHelper = require('../utils/device.helper');

let transactionDate;
let transactionDescription;
let transactionAmount;

class ViewTransactionDetailPage extends AuthPage {
    /**
     * @returns {ViewTransactionDetailScreen}
     */
    get viewTransactionDetailScreen() {
        return this.getNativeScreen('viewTransactionDetail.screen');
    }

    get accountStatementScreen() {
        return this.getNativeScreen('statements/accountStatement.screen');
    }

    get paymentHubMainScreen() {
        return this.getNativeScreen('paymentHub/paymentHubMain.screen');
    }

    get commonScreen() {
        return this.getNativeScreen('common/common.screen');
    }

    get helpWithThisTransactionScreen() {
        return this.getWebScreen('helpWithThisTransaction.screen');
    }

    get mobileBrowserScreen() {
        return this.getNativeScreen('mobileBrowser.screen');
    }

    get callUsScreen() {
        return this.getNativeScreen('callUs/callUs.screen');
    }

    validateVTDPage() {
        Actions.isVisible(this.viewTransactionDetailScreen.date)
            .should
            .equal(true);
        Actions.isVisible(this.viewTransactionDetailScreen.transactionAmount)
            .should
            .equal(true);
        Actions.isVisible(this.viewTransactionDetailScreen.payeeField)
            .should
            .equal(true);
        Actions.isVisible(this.viewTransactionDetailScreen.transferPaymentsButton)
            .should
            .equal(true);
        Actions.isVisible(this.viewTransactionDetailScreen.unsureTransactionButton)
            .should
            .equal(true);
    }

    validatePendingTransactionVTDPage() {
        Actions.isVisible(this.viewTransactionDetailScreen.pendingStatus)
            .should
            .equal(true);
        Actions.isVisible(this.viewTransactionDetailScreen.dateOfPendingPayment)
            .should
            .equal(true);
        Actions.isVisible(this.viewTransactionDetailScreen.pendingTransactionDescription)
            .should
            .equal(true);
        Actions.isVisible(this.viewTransactionDetailScreen.unSureTransaction)
            .should
            .equal(true);
    }

    verifyTransactionErrorTextIsVisible() {
        Actions.isVisible(this.viewTransactionDetailScreen.errorText)
            .should
            .equal(true);
    }

    closeVTDPage() {
        Actions.click(this.viewTransactionDetailScreen.vtdCloseButton);
    }

    selectPostedTransaction() {
        Actions.iOSObjectTreeOptimizationStart();
        Actions.swipeUntilAnyVisible(SwipeAction.UP,
            this.viewTransactionDetailScreen.postedTransaction);
        Actions.click(this.viewTransactionDetailScreen.postedTransaction);
        Actions.iOSObjectTreeOptimizationStop();
    }

    checkVtdFieldsVisibility(type, vtdFields) {
        if (DeviceHelper.isPerfecto() && DeviceHelper.isAndroid()) {
            Actions.perfectoElementInfo('text', this.viewTransactionDetailScreen[vtdFields[0]
                .fieldsInDetailScreen]);
        }
        for (let iter = 0; iter < vtdFields.length; iter++) {
            Actions.swipeByPercentUntilVisible(this.viewTransactionDetailScreen[vtdFields[iter]
                .fieldsInDetailScreen], 50, 60, 50, 20);
        }
    }

    selectViewInformationLink() {
        Actions.click(this.viewTransactionDetailScreen.viewInformationLink);
    }

    viewWinBack() {
        Actions.isVisible(this.viewTransactionDetailScreen.winBackTitle)
            .should
            .equal(true);
    }

    selectStayButtonOnWinBack() {
        Actions.click(this.viewTransactionDetailScreen.stayButton);
    }

    closePostedTransactionDetails() {
        Actions.waitAndClick(this.viewTransactionDetailScreen.vtdCloseButton);
    }

    closePostedTransactionDetailsSwipeAction() {
        Actions.swipeOnElement(
            SwipeAction.DOWN,
            this.viewTransactionDetailScreen.vtdContainer
        );
    }

    checkForDueSoonAndFieldsVisibility(dueSoonData) {
        Actions.isAnyVisible(this.viewTransactionDetailScreen.dueSoon)
            .should
            .equal(true);
        for (let iter = 0; iter < dueSoonData.length; iter++) {
            Actions.isAnyVisible(this.accountStatementScreen[dueSoonData[iter]
                .fieldsToValidate])
                .should
                .equal(true);
        }
    }

    checkForDueSoonVisibility() {
        Actions.waitForVisible(this.viewTransactionDetailScreen.dueSoon);
        Actions.isVisible(this.viewTransactionDetailScreen.dueSoon)
            .should
            .equal(true);
    }

    selectPendingPaymentTransaction() {
        Actions.waitAndClick(this.viewTransactionDetailScreen.pendingPaymentTransaction);
    }

    validatePendingVtdFields(vtdFields) {
        for (let iter = 0; iter < vtdFields.length; iter++) {
            Actions.isVisible(this.viewTransactionDetailScreen[vtdFields[iter]
                .fieldsInDetailScreen])
                .should
                .equal(true);
        }
    }

    closePendingTransactionDetails() {
        Actions.click(this.viewTransactionDetailScreen.vtdCloseButton);
    }

    closePendingTransactionDetailsSwipeAction() {
        Actions.swipeOnElement(
            SwipeAction.DOWN,
            this.viewTransactionDetailScreen.vtdContainer
        );
    }

    verifyVtdPage() {
        Actions.isVisible(this.viewTransactionDetailScreen.vtdContainer)
            .should
            .equal(true);
    }

    clickPaymentsAndTransferButton() {
        Actions.click(this.viewTransactionDetailScreen.transferPaymentsButton);
    }

    verifyPaymentHubPage() {
        Actions.waitForVisible(this.paymentHubMainScreen.fromAccountName);
        Actions.isVisible(this.paymentHubMainScreen.fromAccountName)
            .should
            .equal(true);
    }

    validateVTDPageLoaded() {
        Actions.isVisible(this.viewTransactionDetailScreen.vtdContainer)
            .should
            .equal(true);
    }

    clickStandingOrDirectDebitButton(buttonType) {
        if (buttonType === 'manageStandingOrderButton') {
            Actions.click(this.viewTransactionDetailScreen.manageStandingOrderButton);
        } else if (buttonType === 'viewDirectDebit') {
            Actions.click(this.viewTransactionDetailScreen.viewDirectDebit);
        } else {
            throw new Error('Type does not match');
        }
    }

    validateVTDValues(beneficiary, amount) {
        Actions.getAttributeText(this.viewTransactionDetailScreen.transactionAmount,
            'label')
            .should
            .contains(amount);
        Actions.getAttributeText(this.viewTransactionDetailScreen.payeeField,
            'label')
            .should
            .contains(beneficiary);
    }

    selectUnSureTransaction() {
        Actions.click(this.viewTransactionDetailScreen.unSureTransaction);
    }

    selectOutgoingTransactions() {
        Actions.click(this.viewTransactionDetailScreen.outgoingTransaction);
    }

    selectIncomingTransactions() {
        Actions.click(this.viewTransactionDetailScreen.incomingTransaction);
    }

    selectSpeakToUsButton() {
        Actions.click(this.viewTransactionDetailScreen.speakToUsButton);
    }

    validateNoPaymentOption() {
        Actions.isVisible(this.viewTransactionDetailScreen.transferPaymentsButton)
            .should
            .equal(false);
    }

    clickUnsureAboutTransaction() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.viewTransactionDetailScreen.unsureTransactionButton);
        Actions.click(this.viewTransactionDetailScreen.unsureTransactionButton);
    }

    validateUnsureAboutTransactionPageIsDisplayed() {
        Actions.isVisible(this.viewTransactionDetailScreen.unsureAboutTransactionPage)
            .should
            .equal(true);
    }

    validateUserNavigatedBackToUnsureScreen() {
        Actions.isVisible(!DeviceHelper.isIOS() ? this.viewTransactionDetailScreen.selectAccountHeading
            : this.viewTransactionDetailScreen.unsureAboutTransactionPage)
            .should
            .equal(true);
    }

    shouldNotSeeAdditionalInfromationOrExchangeRate(field) {
        if (field === 'additional information') {
            Actions.isVisible(this.viewTransactionDetailScreen.payeeFieldAdditionalDetails)
                .should
                .equal(false);
        } else if (field === 'exchange rate') {
            Actions.isVisible(this.viewTransactionDetailScreen.exchangeRate)
                .should
                .equal(false);
        } else {
            throw new Error(`${field} field does not match`);
        }
    }

    shouldNotSeeAdditionalInfromationAndExchangeRate() {
        Actions.isVisible(this.viewTransactionDetailScreen.payeeFieldAdditionalDetails)
            .should
            .equal(false);
        Actions.isVisible(this.viewTransactionDetailScreen.exchangeRate)
            .should
            .equal(false);
    }

    validateViewYourOffersButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.viewTransactionDetailScreen.viewYourOffers);
    }

    registerForSpendingRewards() {
        Actions.click(this.viewTransactionDetailScreen.registerSpendingRewardsButton);
    }

    clickViewYourOffers() {
        Actions.click(this.viewTransactionDetailScreen.viewYourOffers);
    }

    noAccountCreditedOrDebitedDate() {
        Actions.isVisible(this.viewTransactionDetailScreen.accountCreditedDate)
            .should
            .equal(false);
        Actions.isVisible(this.viewTransactionDetailScreen.accountDebitedDate)
            .should
            .equal(false);
    }

    selectAndValidateHelpOptionsDetailsPage(helpOptions) {
        for (let iter = 0; iter < helpOptions.length; iter++) {
            const option = helpOptions[iter].helpPageOptions;
            switch (option) {
            case 'dontRecogniseTransaction':
                Actions.waitAndClick(this.viewTransactionDetailScreen.dontRecogniseTransaction);
                Actions.isVisible(this.viewTransactionDetailScreen.helpDetailsPage)
                    .should
                    .equal(true);
                Actions.isVisible(this.viewTransactionDetailScreen.dontRecogniseTransactionWebLink);
                break;
            case 'expectingRefund':
                Actions.waitAndClick(this.viewTransactionDetailScreen.expectingRefund);
                Actions.isVisible(this.viewTransactionDetailScreen.helpDetailsPage)
                    .should
                    .equal(true);
                Actions.isVisible(this.viewTransactionDetailScreen.expectingRefundWebLink);
                break;
            case 'multipleDates':
                Actions.waitAndClick(this.viewTransactionDetailScreen.multipleDates);
                Actions.isVisible(this.viewTransactionDetailScreen.multipleRatesDetailsPage)
                    .should
                    .equal(true);
                break;
            case 'covid19Support':
                Actions.waitAndClick(this.viewTransactionDetailScreen.covid19Support);
                Actions.isVisible(this.viewTransactionDetailScreen.helpDetailsPage)
                    .should
                    .equal(true);
                Actions.isVisible(this.viewTransactionDetailScreen.covid19SupportWebLink);
                break;
            case 'whatIsPendingTransaction':
                Actions.waitAndClick(this.viewTransactionDetailScreen.whatIsPendingTransaction);
                Actions.isVisible(this.viewTransactionDetailScreen.whatIsPendingTransactionDetailsPage)
                    .should
                    .equal(true);
                break;
            case 'cancelPendingTransaction':
                Actions.waitAndClick(this.viewTransactionDetailScreen.cancelPendingTransaction);
                Actions.isVisible(this.viewTransactionDetailScreen.cancelPendingTransactionDetailsPage)
                    .should
                    .equal(true);
                break;
            default:
                throw new Error(`help option not found: ${helpOptions}`);
            }
            Actions.click(this.commonScreen.backButton);
        }
    }

    selectAndValidateHelpPageGuidanceLinks(helpOptions) {
        for (let iter = 0; iter < helpOptions.length; iter++) {
            const option = helpOptions[iter].helpPageOptions;
            switch (option) {
            case 'disputeMessage':
                Actions.swipeUntilVisible(SwipeAction.UP, this.viewTransactionDetailScreen.disputeMessage);
                Actions.click(this.viewTransactionDetailScreen.disputeMessage);
                Actions.waitForVisible(this.helpWithThisTransactionScreen.disputeMessageWebPage);
                if (DeviceHelper.isAndroid()) {
                    Actions.pressAndroidBackKey();
                } else {
                    Actions.click(this.mobileBrowserScreen.returnToApp);
                }
                break;
            case 'contactUs':
                Actions.swipeUntilVisible(SwipeAction.UP, this.viewTransactionDetailScreen.contactUs);
                Actions.waitAndClick(this.viewTransactionDetailScreen.contactUs);
                Actions.waitForVisible(this.callUsScreen.callUsButton);
                break;
            default:
                throw new Error(`help option not found: ${helpOptions}`);
            }
        }
    }

    selectAndValidateHelpDetailsPageWebLinks(helpDetailsWebLinks) {
        for (let iter = 0; iter < helpDetailsWebLinks.length; iter++) {
            const option = helpDetailsWebLinks[iter].helpDetailsPageLinks;
            switch (option) {
            case 'dontRecogniseTransaction':
                Actions.waitAndClick(this.viewTransactionDetailScreen.dontRecogniseTransaction);
                Actions.waitAndClick(this.viewTransactionDetailScreen.dontRecogniseTransactionWebLink);
                Actions.waitForVisible(this.helpWithThisTransactionScreen.dontRecogniseTransactionWebPage);
                break;
            case 'expectingRefund':
                Actions.waitAndClick(this.viewTransactionDetailScreen.expectingRefund);
                Actions.waitAndClick(this.viewTransactionDetailScreen.expectingRefundWebLink);
                Actions.waitForVisible(this.helpWithThisTransactionScreen.expectingRefundWebPage);
                break;
            case 'covid19Support':
                Actions.waitAndClick(this.viewTransactionDetailScreen.covid19Support);
                Actions.waitAndClick(this.viewTransactionDetailScreen.covid19SupportWebLink);
                Actions.waitForVisible(this.helpWithThisTransactionScreen.covid19SupportWebPage);
                break;
            default:
                throw new Error(`help option not found: ${helpDetailsWebLinks}`);
            }
            if (DeviceHelper.isAndroid()) {
                Actions.pressAndroidBackKey();
            } else {
                Actions.click(this.mobileBrowserScreen.returnToApp);
            }
        }
    }

    validateNoTravelDisputesField() {
        Actions.isVisible(this.viewTransactionDetailScreen.travelDisputes)
            .should
            .equal(false);
    }

    validateHelpPageOptions(type, fieldsToValidate) {
        for (let iter = 0; iter < fieldsToValidate.length; iter++) {
            const field = fieldsToValidate[iter].fieldsInHelpPage;
            if (field === 'noTravelDisputes') {
                this.validateNoTravelDisputesField();
            } else {
                Actions.swipeUntilVisible(
                    SwipeAction.UP,
                    this.viewTransactionDetailScreen[field],
                    0, false, 7, 3000
                );
            }
        }
    }

    selectTravelDisputes() {
        Actions.click(this.viewTransactionDetailScreen.travelDisputes);
    }

    valdiateOptions(options) {
        for (let iter = 0; iter < options.length; iter++) {
            const field = options[iter].fields;
            Actions.swipeUntilVisible(
                SwipeAction.UP,
                this.viewTransactionDetailScreen[field],
                0, false, 7, 3000
            );
        }
    }

    selectTravelDisputeGetStartedButton() {
        Actions.click(this.viewTransactionDetailScreen.getStartedButton);
    }

    selectUpDetailsButton(button) {
        Actions.click(this.viewTransactionDetailScreen[button]);
    }

    shouldNotSeeContinueButton() {
        Actions.isVisible(this.viewTransactionDetailScreen.continueButton)
            .should
            .equal(false);
    }

    selectContinueButton() {
        Actions.click(this.viewTransactionDetailScreen.continueButton);
    }

    shouldSeeGFormPage() {
        Actions.waitForVisible(this.viewTransactionDetailScreen.gFormsPageTitle);
    }

    shouldBeOnBeforeYouBeginPage() {
        Actions.isVisible(this.viewTransactionDetailScreen.beforeYouBeginNavigationHeader).should.equal(true);
    }

    selectPostedTransactionToValidateVTDfields() {
        Actions.waitForVisible(this.accountStatementScreen.firstTranscationDate);
        transactionDate = Actions.getAttributeText(this.accountStatementScreen.firstTranscationDate).toString().replace(/ /g, '');
        transactionDescription = Actions.getAttributeText(this.accountStatementScreen.firstTranscationDescription).toString().replace(/ /g, '');
        transactionAmount = Actions.getAttributeText(this.accountStatementScreen.firstTranscationAmount).toString().replace(/ /g, '');
        this.selectPostedTransaction();
    }

    validateFieldsOnVTD() {
        Actions.waitForVisible(this.viewTransactionDetailScreen.date);
        transactionDate.includes(Actions.getAttributeText(this.viewTransactionDetailScreen.date).toString()
            .replace(/ /g, ''));
        transactionDescription.includes(Actions.getAttributeText(this.viewTransactionDetailScreen.payeeField).toString()
            .replace(/ /g, ''));
        transactionAmount.includes(Actions.getAttributeText(this.viewTransactionDetailScreen.transactionAmount).toString()
            .replace(/ /g, ''));
    }

    shouldSeeTransactionLogo() {
        Actions.waitForVisible(this.viewTransactionDetailScreen.date);
        Actions.isVisible(this.viewTransactionDetailScreen.vtdLogo).should.equal(true);
    }
}

module.exports = ViewTransactionDetailPage;
