require('chai').should();
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');
const runConfig = require('../enums/runConfig.enum');
const DataConsent = require('./settings/personalDetails/dataConsent.page');

class MemorableInformationPage extends UnAuthPage {
    /**
     * @returns {MemorableInformationScreen}
     */
    get memorableInformationScreen() {
        return this.getNativeScreen('memorableInformation.screen');
    }

    validatePageLoad() {
        // slower devices are failing on this screen, hence increasing timeout
        Actions.waitForVisible(this.memorableInformationScreen.title, 2 * runConfig.TIMEOUTS.WAIT_FOR());
    }

    validateIbRegMiPageLoad() {
        // slower devices are failing on this screen, hence increasing timeout
        Actions.waitForVisible(this.memorableInformationScreen.iBRegMiTitle, 2 * runConfig.TIMEOUTS.WAIT_FOR());
    }

    enterMemorableInformation(mi, numberOfCharacters) {
        this.enterMI(mi, numberOfCharacters);
        Actions.reportTimer('Enter_MI', 'Security Call');
    }

    canSeeThreeTextboxForMemorableInformation() {
        Actions.elements(this.memorableInformationScreen.miTextboxes).value.length.should.equal(3);
    }

    enterMI(mi, numberOfCharacters = 3) {
        const propertyValue = DeviceHelper.isPerfecto() ? 'name' : 'text';
        const attributeType = DeviceHelper.isIOS() ? 'label' : propertyValue;
        let index = 0;
        if (DeviceHelper.isAndroid() || DeviceHelper.isPerfecto()) {
            Actions.click(this.memorableInformationScreen.firstMemorableCharacter);
        }

        let element;

        for (let i = 0; i < numberOfCharacters; i++) {
            // In STUB mode, it doesn't matter what you type, so we can save time.
            if (!DeviceHelper.isStub()) {
                const getScreenElement = (!DeviceHelper.isPerfecto() && DeviceHelper.isAndroid())
                    ? this.memorableInformationScreen.memorableInformationCharacterAppium(i)
                    : this.memorableInformationScreen.memorableInformationCharacter(i);
                index = Actions.getAttribute(getScreenElement, attributeType)
                    .match(/.*(\d).*/)[1] - 1;

                // MI screen uses custom keyboard which is different than native keyboard.
                Actions.setMICustomKeyoard(mi[index]);
            } else if (DeviceHelper.isIOS() && DeviceHelper.isStub()) {
                // to speed up entering MI on stub element is found once and reused
                if (!element) {
                    element = Actions.getElementId(this.memorableInformationScreen.miCustomKey(mi[0].toUpperCase()));
                }
                Actions.elementIdClick(element);
            } else {
                Actions.setMICustomKeyoard(mi[index]);
            }
        }
        Actions.reportTimer('Payments_AOV', 'Support');
    }

    selectFscsTitle() {
        Actions.waitForVisible(this.memorableInformationScreen.fscsTitle);
        Actions.click(this.memorableInformationScreen.fscsTitle);
    }

    clickForgotLogonDetails() {
        Actions.click(this.memorableInformationScreen.miForgotYourPassword);
    }

    verifyCmsTile(cmsVisible) {
        Actions.isVisible(this.memorableInformationScreen.cmsTile).should.equal(cmsVisible);
    }

    verifyMIFieldIsMasked() {
        for (let i = 1; i < 3; i++) {
            if (DeviceHelper.isAndroid()) {
                Actions.getAttribute(this.memorableInformationScreen.memorableInformationCharacter(i), 'password')
                    .should.equal('true');
            } else {
                Actions.getTagName(this.memorableInformationScreen.memorableInformationCharacter(i - 1)).toLowerCase()
                    .indexOf('securetext').should.not.equal(-1);
            }
        }
    }

    verifyUniqueMIHeaders() {
        let firstOrdinal;
        let secondOrdinal;
        let thirdOrdinal;
        if (DeviceHelper.isAndroid()) {
            firstOrdinal = Actions.getText(this.memorableInformationScreen.firstOrdinal).match(/(\d+)/g)[0];
            secondOrdinal = Actions.getText(this.memorableInformationScreen.secondOrdinal).match(/(\d+)/g)[0];
            thirdOrdinal = Actions.getText(this.memorableInformationScreen.thirdOrdinal).match(/(\d+)/g)[0];
        } else {
            firstOrdinal = Actions.getAttribute(this.memorableInformationScreen.firstOrdinal, 'label')
                .match(/(\d+)/g)[0];
            secondOrdinal = Actions.getAttribute(this.memorableInformationScreen.secondOrdinal, 'label')
                .match(/(\d+)/g)[0];
            thirdOrdinal = Actions.getAttribute(this.memorableInformationScreen.thirdOrdinal, 'label')
                .match(/(\d+)/g)[0];
        }
        if ((firstOrdinal === secondOrdinal)
            || (secondOrdinal === thirdOrdinal) || (thirdOrdinal === firstOrdinal)) {
            throw new Error('MI Ordinals not unique');
        }
    }

    proceedToPage(page) {
        let pageIsVisible = false;
        const homePageTimeout = DeviceHelper.isStub() ? 20000 : 120000;
        const dataConsent = new DataConsent();
        while (!pageIsVisible) {
            const locator = Actions.waitUntilVisible([
                page.validationLocator,
                this.memorableInformationScreen.continueToMyAccounts,
                this.memorableInformationScreen.touchButton,
                this.memorableInformationScreen.dataConsent,
                this.memorableInformationScreen.manageDataConsent,
                this.memorableInformationScreen.keepUsingMI
            ], homePageTimeout);
            if (locator === page.validationLocator) {
                pageIsVisible = true;
            } else if (locator === this.memorableInformationScreen.dataConsent) {
                Actions.swipeByPercentUntilVisible(this.memorableInformationScreen.acceptAll, 20, 80, 20, 60, 100);
                Actions.click(this.memorableInformationScreen.acceptAll);
            } else if (locator === this.memorableInformationScreen.touchButton) {
                Actions.swipeByPercentUntilVisible(this.memorableInformationScreen.touchButton, 20, 80, 20, 60, 100);
                Actions.click(this.memorableInformationScreen.touchButton);
            } else if (locator === this.memorableInformationScreen.manageDataConsent) { // bnga
                dataConsent.acceptAndConfirmDataConsent();
            } else {
                Actions.click(locator);
                break;
            }
        }
    }

    selectTooltiplink() {
        Actions.click(this.memorableInformationScreen.tooltipLink);
    }

    shouldSeeTooltipPopup() {
        Actions.isVisible(this.memorableInformationScreen.tooltipPopup).should.equal(true);
    }

    closeToolTip() {
        Actions.click(this.memorableInformationScreen.closeToolTip);
    }

    shouldSeeExpressLogonUnavailablePopUp() {
        Actions.isVisible(this.memorableInformationScreen.expressLogonUnavailableTitle).should.equal(true);
    }

    selectOkButton() {
        Actions.click(this.memorableInformationScreen.expressLogonUnavailableOKButon);
    }
}

module.exports = MemorableInformationPage;
