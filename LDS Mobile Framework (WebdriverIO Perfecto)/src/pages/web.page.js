const AuthPage = require('./common/auth.page');
const ScreenFactory = require('../screens/screen.factory');

class WebPage extends AuthPage {
    constructor() {
        super();
        this.screens = {};
    }

    getScreen(name) {
        if (!(name in this.screens)) {
            this.screens.name = ScreenFactory.getWebScreen(name);
        }
        return this.screens.name;
    }
}

module.exports = WebPage;
