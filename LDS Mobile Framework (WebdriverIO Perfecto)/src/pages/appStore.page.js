require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class AppStorePage extends AuthPage {
    /**
     * @returns {AppStoreScreen}
     */
    get appStoreScreen() {
        return this.getNativeScreen('appStore.screen');
    }

    waitForAppStorePageLoad() {
        Actions.waitForVisible(this.appStoreScreen.title, 50000);
    }

    verifyAppTextInAppStore() {
        Actions.waitForVisible(this.appStoreScreen.appText, 10000);
        Actions.isVisible(this.appStoreScreen.appText).should.equal(true);
    }

    selectDoneOrCancelButton() {
        Actions.click(this.appStoreScreen.DoneOrCancelButton);
    }

    compareAppVersions(expectedAppVersion) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.appStoreScreen.appHeaderVersion);
        const appVersion = Actions.getText(this.appStoreScreen.appHeaderVersion);
        const updateToAppVersion = appVersion.split(',')[0].split('Version ')[1];
        expectedAppVersion.should.equal(updateToAppVersion);
    }
}

module.exports = AppStorePage;
