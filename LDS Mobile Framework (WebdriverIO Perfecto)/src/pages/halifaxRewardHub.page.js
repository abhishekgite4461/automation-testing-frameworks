const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class halifaxRewardHubPage extends AuthPage {
    /**
     * @returns {halifaxRewardHub}
     */
    get halifaxRewardHub() {
        return this.getNativeScreen('halifaxRewardHub.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.halifaxRewardHub.title);
        Actions.swipeUntilVisible(SwipeAction.UP, this.halifaxRewardHub.mortgagePrizeDrawCallToAction);
    }

    validateCashBackExtra(state) {
        switch (state) {
        case 'optIn':
            Actions.isVisible(this.halifaxRewardHub.optInView).should.equal(true);
            Actions.isVisible(this.halifaxRewardHub.cashbackExtraOptInText).should.equal(true);
            break;
        case 'NotOptIn':
            Actions.isVisible(this.halifaxRewardHub.optInView).should.equal(false);
            Actions.isVisible(this.halifaxRewardHub.cashbackExtraNotOptInText).should.equal(true);
            break;
        case 'NotEligible':
            Actions.isVisible(this.halifaxRewardHub.cashbackExtraCallToAction).should.equal(false);
            Actions.isVisible(this.halifaxRewardHub.cashbackExtraNotEligibleText).should.equal(true);
            break;
        default:
            throw new Error('unrecognised state for cashback extras');
        }
    }

    validateRewardsExtra(state) {
        switch (state) {
        case 'optInToday':
            Actions.isVisible(this.halifaxRewardHub.optInTodayView).should.equal(true);
            break;
        case 'optInOnTrack':
            Actions.isVisible(this.halifaxRewardHub.optInView).should.equal(true);
            Actions.isVisible(this.halifaxRewardHub.onTrackText).should.equal(true);
            break;
        case 'optInNotOnTrack':
            Actions.isVisible(this.halifaxRewardHub.optInView).should.equal(true);
            Actions.isVisible(this.halifaxRewardHub.notOnTrackIcon).should.equal(true);
            Actions.isVisible(this.halifaxRewardHub.notOnTrackText).should.equal(true);
            break;
        case 'NotOptIn':
            Actions.isVisible(this.halifaxRewardHub.optInView).should.equal(false);
            Actions.isVisible(this.halifaxRewardHub.rewardExtraNotOptInText).should.equal(true);
            break;
        case 'NotEligible':
            Actions.isVisible(this.halifaxRewardHub.rewardExtraNotEligibleText).should.equal(true);
            break;
        default:
            throw new Error('unrecognised state for rewards extras');
        }
    }

    validateMortgagePrizeDraw(state) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.halifaxRewardHub.mortgagePrizeDrawCallToAction);
        switch (state) {
        case 'Registered':
            Actions.isVisible(this.halifaxRewardHub.optInView).should.equal(true);
            Actions.isVisible(this.halifaxRewardHub.mortgageRegisteredText).should.equal(true);
            break;
        case 'NotRegistered':
            Actions.isVisible(this.halifaxRewardHub.optInView).should.equal(false);
            Actions.isVisible(this.halifaxRewardHub.mortgageNotRegisteredText).should.equal(true);
            break;
        default:
            throw new Error('unrecognised state for mortgage prize draw extras');
        }
    }

    validateSaversPrizeDraw(state) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.halifaxRewardHub.mortgagePrizeDrawCallToAction);
        switch (state) {
        case 'Registered':
            Actions.isVisible(this.halifaxRewardHub.optInView).should.equal(true);
            Actions.isVisible(this.halifaxRewardHub.saversRegisteredText).should.equal(true);
            break;
        case 'NotRegistered':
            Actions.isVisible(this.halifaxRewardHub.optInView).should.equal(false);
            Actions.isVisible(this.halifaxRewardHub.saversNotRegisteredText).should.equal(true);
            break;
        default:
            throw new Error('unrecognised state for savers prize draw extras');
        }
    }

    selectCallToAction(reward) {
        Actions.click(this.halifaxRewardHub[`${reward}CallToAction`]);
    }

    validateRewardsWebJourney(reward) {
        Actions.click(this.halifaxRewardHub[`${reward}WebPage`]);
    }
}

module.exports = halifaxRewardHubPage;
