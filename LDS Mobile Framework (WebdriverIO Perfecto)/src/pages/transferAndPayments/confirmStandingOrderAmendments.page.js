const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class ConfirmStandingOrderAmendmentsPage extends AuthPage {
    /**
     * @returns {ConfirmStandingOrderAmendScreen}
     */
    get confirmStandingOrderAmendScreen() {
        return this.getNativeScreen('transferAndPayments/confirmStandingOrderAmendments.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.confirmStandingOrderAmendScreen.title);
    }

    enterAmendPassword(amendPassword) {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.confirmStandingOrderAmendScreen.confirmAmendPassword
        );
        Actions.setImmediateValue(
            this.confirmStandingOrderAmendScreen.confirmAmendPassword,
            amendPassword
        );
    }

    confirmAmendStandingOrder() {
        Actions.click(this.confirmStandingOrderAmendScreen.confirmAmendStandingOrder);
    }
}

module.exports = ConfirmStandingOrderAmendmentsPage;
