require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class TransferSuccessPage extends AuthPage {
    /**
     * @returns {TransferSuccessScreen}
     */
    get transferSuccessScreen() {
        return this.getNativeScreen('transferAndPayments/transferSuccess.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.transferSuccessScreen.title);
    }

    verifyButtonsAvailable() {
        Actions.isVisible(this.transferSuccessScreen.viewTransactionButton).should.equal(true);
        Actions.isVisible(this.transferSuccessScreen.transferPaymentButton).should.equal(true);
    }

    selectViewTransaction() {
        Actions.click(this.transferSuccessScreen.viewTransactionButton);
    }

    viewBannerPlacement() {
        Actions.waitForVisible(this.transferSuccessScreen.bannerLeadPlacement);
        Actions.isVisible(this.transferSuccessScreen.bannerLeadPlacement).should.equal(true);
    }
}

module.exports = TransferSuccessPage;
