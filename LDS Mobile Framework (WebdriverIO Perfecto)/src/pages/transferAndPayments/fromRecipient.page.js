const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class FromRecipientPage extends AuthPage {
    /**
     * @returns {FromRecipientScreen}
     */
    get fromRecipientScreen() {
        return this.getNativeScreen('transferAndPayments/fromRecipient.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.fromRecipientScreen.title);
    }
}
module.exports = FromRecipientPage;
