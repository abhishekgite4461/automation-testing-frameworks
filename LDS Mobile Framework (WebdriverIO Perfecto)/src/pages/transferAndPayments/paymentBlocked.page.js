const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class PaymentBlockedPage extends AuthPage {
    /**
     * @returns {PaymentBlockedScreen}
     */
    get paymentBlockedScreen() {
        return this.getNativeScreen('transferAndPayments/paymentBlocked.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.paymentBlockedScreen.title);
    }
}

module.exports = PaymentBlockedPage;
