const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ViewTransactionPage extends AuthPage {
    /**
     * @returns {ViewTransactionScreen}
     */

    get viewTransactionScreen() {
        return this.getNativeScreen('transferAndPayments/viewTransaction.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.viewTransactionScreen.title);
    }

    verifyRemainingAllowanceInTransactionPage() {
        const remainingAllowanceAmountFromISA = this.viewTransactionScreen.remainingAllowanceAmount;
        return Actions.getText(remainingAllowanceAmountFromISA).split('£')[1].trim();
    }
}

module.exports = ViewTransactionPage;
