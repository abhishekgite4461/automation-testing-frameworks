require('chai').should();

const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const SwipeAction = require('../../enums/swipeAction.enum');
const UkBeneficiaryDetailPage = require('./ukBeneficiaryDetail.page');

const ukBeneficiaryDetailPage = new UkBeneficiaryDetailPage();

class ConfirmationOfPayeePage extends AuthPage {
    /**
     * @returns {ConfirmationOfPayeeScreen}
     */
    get confirmationOfPayeeScreen() {
        return this.getNativeScreen('transferAndPayments/confirmationOfPayee.screen');
    }

    waitForCOPResponsePage() {
        Actions.waitForVisible(this.confirmationOfPayeeScreen.paymentHubCopNotMatchTitle);
    }

    verifyBAMMNamePlayBack(type, recipientName) {
        if (type === 'without') {
            Actions.isVisible(this.confirmationOfPayeeScreen.paymentHubCopBAMMMessage).should.equal(true);
        } else if (type === 'with') {
            Actions.isVisible(this.confirmationOfPayeeScreen.paymentHubCopBAMMNamePlayBack(recipientName))
                .should.equal(true);
        } else {
            throw new Error(`ERROR: Unknown ${type} Type`);
        }
    }

    shouldSeeCOPDetailsEditOption() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.confirmationOfPayeeScreen.paymentHubCopNotMatchEdit);
    }

    shouldSeeCOPDetailsContinueOption() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.confirmationOfPayeeScreen.paymentHubCopNotMatchContinue);
    }

    verifyAccountNameNotMatchMessage(type) {
        if (type === 'not') {
            Actions.isVisible(this.confirmationOfPayeeScreen.paymentHubCopANNMMessageOne).should.equal(true);
        } else if (type === 'still not') {
            Actions.isVisible(this.confirmationOfPayeeScreen.paymentHubCopANNMMessageTwo).should.equal(true);
        } else {
            throw new Error(`ERROR: Unknown ${type} Type`);
        }
    }

    navigateToPayeeDetailsPage() {
        Actions.click(this.commonScreen.modalBackButton);
    }

    verifyToolTipWithSortCode(bankDetails) {
        this.bankDetails = bankDetails;
        bankDetails.forEach((bank) => {
            ukBeneficiaryDetailPage.clearSortCodeField();
            ukBeneficiaryDetailPage.enterUkBeneficiarySortCode(bank.searchedSortCode);
            ukBeneficiaryDetailPage.verifyToolTipResults(
                bank.searchedSortCode, bank.bankName
            );
        });
    }

    shouldSeePleaseWaitSpinner() {
        Actions.isVisible(this.commonScreen.pleaseWaitSpinner).should.equal(true);
    }

    shouldSeeCheckingDetailsSpinner() {
        Actions.isVisible(this.commonScreen.checkingDetailsSpinner).should.equal(true);
    }

    shouldSeeAccountNameMatch() {
        Actions.isVisible(this.commonScreen.accountNameMatch).should.equal(true);
    }

    shouldSeePersonalDetailsMatch(paymentHubCopNotMatchTitle) {
        Actions.waitForVisible(this.confirmationOfPayeeScreen.personalAccountMatch);
        Actions.getAttributeText(this.confirmationOfPayeeScreen.title, 'value').should.contains(paymentHubCopNotMatchTitle);
        Actions.isVisible(this.confirmationOfPayeeScreen.panMbanMContent).should.equal(true);
    }

    shouldSeeBusinessDetailsMatch(paymentHubCopNotMatchTitle) {
        Actions.waitForVisible(this.confirmationOfPayeeScreen.businessNameMatch);
        Actions.getAttributeText(this.confirmationOfPayeeScreen.paymentHubCopNotMatchTitle, 'value').should.contains(paymentHubCopNotMatchTitle);
        Actions.isVisible(this.confirmationOfPayeeScreen.panMbanMContent).should.equal(true);
    }

    shouldSeePartialMatch() {
        Actions.isVisible(this.confirmationOfPayeeScreen.partialNameMatch).should.equal(true);
    }

    shouldSeeFullError() {
        Actions.isVisible(this.confirmationOfPayeeScreen.serviceError).should.equal(true);
    }

    selectCOPDetailsContinueOption() {
        Actions.waitAndClick(this.confirmationOfPayeeScreen.paymentHubCopNotMatchContinue);
    }

    shouldSeeCOPShareDetails() {
        Actions.swipeUntilVisible(SwipeAction.UP,
            this.confirmationOfPayeeScreen.paymentHubCopNotMatchShareDetailsButton);
        Actions.isVisible(this.confirmationOfPayeeScreen.paymentHubCopNotMatchShareDetailsButton).should.equal(true);
    }

    selectCOPShareDetailsOption() {
        Actions.swipeUntilVisible(SwipeAction.UP,
            this.confirmationOfPayeeScreen.paymentHubCopNotMatchShareDetailsButton);
        Actions.click(this.confirmationOfPayeeScreen.paymentHubCopNotMatchShareDetailsButton);
    }
}

module.exports = ConfirmationOfPayeePage;
