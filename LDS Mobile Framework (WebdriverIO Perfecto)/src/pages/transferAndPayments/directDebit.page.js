const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class DirectDebitPage extends AuthPage {
    /**
     * @returns {DirectDebitScreen}
     */
    get directDebitScreen() {
        return this.getNativeScreen('transferAndPayments/directDebit.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.directDebitScreen.title);
    }

    selectDirectDebit() {
        Actions.click(this.directDebitScreen.selectDirectDebitTab);
    }

    clickCancelDirectDebit() {
        Actions.click(this.directDebitScreen.cancelButton);
    }

    verifyCancelDirectDebitNotVisible() {
        Actions.isVisible(this.directDebitScreen.cancelButton).should.equal(false);
    }

    verifyDirectDebitCancelSuccessMsg() {
        Actions.isVisible(this.directDebitScreen.directDebitCancelled).should.equal(true);
    }
}

module.exports = DirectDebitPage;
