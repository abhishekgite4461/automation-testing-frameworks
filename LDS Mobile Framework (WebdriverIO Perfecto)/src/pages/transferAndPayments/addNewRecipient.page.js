const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class AddNewRecipientPage extends AuthPage {
    /**
     * @returns {AddNewRecipientScreen}
     */
    get addNewRecipientScreen() {
        return this.getNativeScreen('transferAndPayments/addNewRecipient.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.addNewRecipientScreen.title);
    }
}

module.exports = AddNewRecipientPage;
