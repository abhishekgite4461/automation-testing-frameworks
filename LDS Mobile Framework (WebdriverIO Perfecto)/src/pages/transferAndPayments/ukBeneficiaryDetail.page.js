const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const DeviceHelper = require('../../utils/device.helper');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const SwipeAction = require('../../enums/swipeAction.enum');

const copUser = ['Hughie Omai', 'XYZ Developments'];

class UkBeneficiaryDetailPage extends AuthPage {
    /**
     * @returns {ukBeneficiaryDetail}
     */
    get ukBeneficiaryDetailScreen() {
        return this.getNativeScreen('transferAndPayments/ukBeneficiaryDetail.screen');
    }

    /**
     * @returns {beneficiaryTypeScreen}
     */
    get beneficiaryTypeScreen() {
        return this.getNativeScreen('transferAndPayments/beneficiaryType.screen');
    }

    /**
     * @returns {reactivateIsaScreen}
     */
    get reactivateIsaScreen() {
        return this.getNativeScreen('paymentHub/reactivateIsa.screen');
    }

    /**
     * @returns {confirmationOfPayeeScreen}
     */
    get confirmationOfPayeeScreen() {
        return this.getNativeScreen('transferAndPayments/confirmationOfPayee.screen');
    }

    /**
     * @returns {paymentHubMainScreen}
     */
    get paymentHubMainScreen() {
        return this.getNativeScreen('native/paymentHubMain.screen');
    }

    verifyNoDebitCardOption() {
        Actions.isExisting(this.ukBeneficiaryDetailScreen.debitCardTile).should.equal(false);
    }

    shouldSeeNewUkBeneficiaryDetailPage() {
        Actions.isExisting(this
            .ukBeneficiaryDetailScreen.paymentHubAddUkAccountAdditionalContextCopy).should.equal(true);
    }

    shouldSeeCOPUkBeneficiaryDetailPage() {
        Actions.isVisible(this
            .ukBeneficiaryDetailScreen.paymentHubAddUkAccountSortCodeLabel).should.equal(true);
    }

    verifyUkCompanyDetails(ukBeneficiaryDetail) {
        for (let i = 0; i < ukBeneficiaryDetail.length; i++) {
            const individualBeneficiaryDetail = this.ukBeneficiaryDetailScreen[ukBeneficiaryDetail[i].details];
            Actions.isExisting(individualBeneficiaryDetail).should.equal(true);
        }
    }

    cancelWinBack() {
        Actions.click(this.ukBeneficiaryDetailScreen.paymentHubAddUkAccountCancelButton);
    }

    viewWinBackModal() {
        Actions.isExisting(this.reactivateIsaScreen.dialogPositiveAction).should.equal(true);
        Actions.isExisting(this.reactivateIsaScreen.dialogNegativeAction).should.equal(true);
    }

    verifyWinBackOnNegativeAction() {
        Actions.click(this.reactivateIsaScreen.dialogNegativeAction);
        Actions.isExisting(this
            .ukBeneficiaryDetailScreen.paymentHubAddUkAccountCancelButton).should.equal(true);
    }

    verifyWinBackOnPositiveAction() {
        Actions.click(this.ukBeneficiaryDetailScreen.paymentHubAddUkAccountCancelButton);
        Actions.click(this.reactivateIsaScreen.dialogPositiveAction);
        Actions.isExisting(this.beneficiaryTypeScreen.ukAccount).should.equal(true);
    }

    verifyBackJourney() {
        Actions.click(this.commonScreen.modalBackButton);
    }

    verifyBeneficiaryType() {
        Actions.isExisting(this.beneficiaryTypeScreen.ukAccount).should.equal(true);
    }

    selectRemittingAccount() {
        Actions.click(this.ukBeneficiaryDetailScreen.beneficiaryTileIcon);
    }

    verifyEligibleRemittingAccounts() {
        Actions.isExisting(this
            .ukBeneficiaryDetailScreen.arrangementSummaryListFromAccount).should.equal(true);
    }

    enterUkBeneficiaryPayeeName(recipientName) {
        let recipientNameWithTimeStamp;
        if (!DeviceHelper.isStub() && !(copUser.includes(recipientName))) {
            const transArray = Array.from('abcdefghijklmnopqrstuvwxyz');
            const dateTime = new Date();
            const timeStamp = `${transArray[dateTime.getMonth()]}${transArray[dateTime.getDate() % 26]}
                 ${transArray[dateTime.getHours()]}${transArray[dateTime.getMinutes() % 26]}`;
            recipientNameWithTimeStamp = `${recipientName}${timeStamp}`;
        } else {
            recipientNameWithTimeStamp = recipientName;
        }
        if (recipientNameWithTimeStamp.length >= 19) {
            recipientNameWithTimeStamp = recipientNameWithTimeStamp.substring(0, 18);
        }
        SavedAppDataRepository.setData(SavedData.NEWLY_ADDED_BENEFICIARY, recipientNameWithTimeStamp);
        Actions.setImmediateValueAndTapOut(this.ukBeneficiaryDetailScreen.payeeNameUk, recipientNameWithTimeStamp);
        // Payee or recipient name is mandatory field, We check for length because the fields
        // are dynamically validated and if an empty string is passed in, an error occurs.
        if (recipientName.length < 1) {
            Actions.isExisting(this
                .ukBeneficiaryDetailScreen.errorMessageUkBeneficiary).should.equal(true);
            Actions.isExisting(this
                .ukBeneficiaryDetailScreen.errorMessageExclamatory).should.equal(true);
        }
    }

    enterUkBeneficiarySortCode(sortCode) {
        Actions.setImmediateValueAndTapOut(this.ukBeneficiaryDetailScreen.sortCodeField, sortCode);
    }

    enterUkBeneficiaryAccountNumber(accountNumber) {
        Actions.setImmediateValueAndTapOut(this.ukBeneficiaryDetailScreen.accountNumberUk, accountNumber);
        // Account Number is dynamically validated and an error will be shown for any
        // input whose length is less than 8.
        if (accountNumber.length < 8) {
            Actions.isExisting(this
                .ukBeneficiaryDetailScreen.errorMessageUkBeneficiary).should.equal(true);
            Actions.isExisting(this
                .ukBeneficiaryDetailScreen.errorMessageExclamatory).should.equal(true);
        }
    }

    continueToReviewDetailsPage() {
        Actions.waitAndClick(this.ukBeneficiaryDetailScreen.paymentHubAddUkAccountContinueButton);
    }

    verifyErrorMessage() {
        Actions.isExisting(this
            .ukBeneficiaryDetailScreen.errorMessageUkBeneficiary).should.equal(true);
    }

    unableToContinueToReviewDetailsPage() {
        Actions.isEnabled(this.ukBeneficiaryDetailScreen.paymentHubAddUkAccountContinueButton)
            .should.equal(false);
    }

    verifyBusinessConfirmationCheckBox(check) {
        if (check === 'choose') {
            Actions.isChecked(this.ukBeneficiaryDetailScreen.paymentHubAddUkAccountTypeCheckBox).should.equal(false);
            Actions.click(this.ukBeneficiaryDetailScreen.paymentHubAddUkAccountTypeCheckBox);
        } else if (check === 'dont choose') {
            Actions.isChecked(this.ukBeneficiaryDetailScreen.paymentHubAddUkAccountTypeCheckBox).should.equal(false);
        } else {
            throw new Error(`ERROR: Unknown ${check} Type`);
        }
    }

    shouldSeeInvalidAccountNumberErrorMessage() {
        Actions.isVisible(this.ukBeneficiaryDetailScreen.invalidAccountNumberErrorMessage).should.equal(true);
    }

    closeErrorMessage() {
        Actions.click(this.ukBeneficiaryDetailScreen.closeInvalidAccountNumberErrorMessage);
    }

    shouldSeeBankAccountPage() {
        Actions.isVisible(this.ukBeneficiaryDetailScreen.yourUKBankAccount).should.equal(true);
        Actions.isVisible(this.ukBeneficiaryDetailScreen.pickBankPayForm).should.equal(true);
    }

    selectAnotherUKBankAccount(bankAccount) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.ukBeneficiaryDetailScreen.anotherUKBankAccount(bankAccount));
        Actions.click(this.ukBeneficiaryDetailScreen.anotherUKBankAccount(bankAccount));
    }

    shouldSeeConfirmPaymentDetails(confirmPreviewDetails) {
        for (let i = 0; i < confirmPreviewDetails.length; i++) {
            const confirmReceiptDetails = this.ukBeneficiaryDetailScreen[
                confirmPreviewDetails[i].confirmPreviewDetails];
            Actions.isVisible(confirmReceiptDetails).should.equal(true);
        }
    }

    selectConfirmPaymentCheckBox() {
        Actions.click(this.ukBeneficiaryDetailScreen.confirmPaymentCheckBox);
    }

    selectConfirmPaymentButton() {
        Actions.click(this.ukBeneficiaryDetailScreen.confirmButton);
    }

    selectContinueButton() {
        Actions.click(this.ukBeneficiaryDetailScreen.cotinueButton);
    }

    shouldSeeDetailsOnApproveYourPaymentPage(approveYourPayment) {
        Actions.isVisible(this.ukBeneficiaryDetailScreen.approveYourPaymentPage).should.equal(true);
        for (let i = 0; i < approveYourPayment.length; i++) {
            const hyperLinks = this.ukBeneficiaryDetailScreen[approveYourPayment[i].approveYourPayment];
            Actions.isVisible(hyperLinks).should.equal(true);
        }
    }

    selectContinueButtonApproveYourPayment() {
        Actions.click(this.ukBeneficiaryDetailScreen.approveYourPaymentCotinueButton);
    }

    shouldSeeGoToYourBankDialogBox() {
        Actions.isVisible(this.ukBeneficiaryDetailScreen.goToYourBankDialogBox).should.equal(true);
    }

    selectDialogBoxContinueButton() {
        Actions.click(this.ukBeneficiaryDetailScreen.dialogBoxContinueButton);
    }

    shouldSeeUnfinishedPaymentPage() {
        Actions.isVisible(this.ukBeneficiaryDetailScreen.unfinishedPaymentPage).should.equal(true);
    }

    shouldSeeYourBankAccountDetails(yourBankAccountDetails) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.ukBeneficiaryDetailScreen.payUKDebitCard);
        for (let i = 0; i < yourBankAccountDetails.length; i++) {
            const bankAccountDetailsPage = this.ukBeneficiaryDetailScreen[yourBankAccountDetails[i].yourBankAccount];
            Actions.isVisible(bankAccountDetailsPage).should.equal(true);
        }
    }

    selectAmountInputField() {
        Actions.click(this.ukBeneficiaryDetailScreen.amountInputField);
    }

    shouldSeeAmountToPayOptions(amountPayOptions) {
        for (let i = 0; i < amountPayOptions.length; i++) {
            const amountOptions = this.ukBeneficiaryDetailScreen[amountPayOptions[i].amountPayOptions];
            Actions.isVisible(amountOptions).should.equal(true);
        }
    }

    selectCancelButton() {
        Actions.click(this.ukBeneficiaryDetailScreen.cancelButton);
    }
}

module.exports = UkBeneficiaryDetailPage;
