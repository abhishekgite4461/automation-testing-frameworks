const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class IsaWarningPage extends AuthPage {
    /**
     * @returns {IsaWarningScreen}
     */
    get isaWarningScreen() {
        return this.getNativeScreen('transferAndPayments/isaWarning.screen');
    }

    verifyRenewWarning() {
        Actions.waitForVisible(this.isaWarningScreen.renewWarningMessage);
    }
}

module.exports = IsaWarningPage;
