const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class RateAppPaymentHubPage extends AuthPage {
    /**
     * @returns {RateAppPaymentHubScreen}
     */
    get rateAppPaymentHubScreen() {
        return this.getNativeScreen('transferAndPayments/rateAppPaymentHub.screen');
    }

    closeRateAppPopUp() {
        if (Actions.isVisible(this.rateAppPaymentHubScreen.rateApp)) {
            Actions.click(this.rateAppPaymentHubScreen.closeRateApp);
        }
    }
}

module.exports = RateAppPaymentHubPage;
