const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const SwipeAction = require('../../enums/swipeAction.enum');

class StandingOrderSuccessfulPage extends AuthPage {
    /**
     * @returns {StandingOrderSuccessfulScreen}
     */
    get StandingOrderSuccessfulScreen() {
        return this.getNativeScreen('paymentHub/standingOrderSuccessful.screen');
    }

    get successTransferScreen() {
        return this.getNativeScreen('paymentHub/successTransfer.screen');
    }

    waitForPageLoad() {
        const locator = Actions.waitUntilVisible([
            this.StandingOrderSuccessfulScreen.standingOrderSuccessHeader,
            this.successTransferScreen.notNow
        ], 15000);

        if (locator === this.successTransferScreen.notNow) {
            Actions.click(this.successTransferScreen.notNow);
            this.viewSuccessSOMessage();
        }
    }

    viewSuccessSOMessage() {
        Actions.waitForVisible(this.StandingOrderSuccessfulScreen.standingOrderSuccessHeader);
    }

    viewErrorSOMessage() {
        Actions.waitForVisible(this.StandingOrderSuccessfulScreen.soNotSetupErrorMessage);
    }

    clickViewStandingOrder() {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.StandingOrderSuccessfulScreen.viewStandingOrdersButton
        );
        Actions.click(this.StandingOrderSuccessfulScreen.viewStandingOrdersButton);
    }
}

module.exports = StandingOrderSuccessfulPage;
