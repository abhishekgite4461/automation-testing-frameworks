require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class DirectDebitNonCancellablePage extends AuthPage {
    /**
     * @returns {DirectDebitNonCancellableScreen}
     */
    get DirectDebitNonCancellableScreen() {
        return this.getNativeScreen('transferAndPayments/directDebitNonCancellable.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.DirectDebitNonCancellableScreen.title);
    }

    verifyDirectDebitNotCancelledMsg() {
        Actions.isVisible(this.DirectDebitNonCancellableScreen.nonCancellableMessage)
            .should.equal(true);
    }
}
module.exports = DirectDebitNonCancellablePage;
