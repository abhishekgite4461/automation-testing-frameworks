require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

// This page is within Standing Order Page to select the account where standing order is to be set

class AccountOptionsPage extends AuthPage {
    /**
     * @returns {AccountOptionsScreen}
     */
    get accountOptionsScreen() {
        return this.getNativeScreen('transferAndPayments/accountOptions.screen');
    }

    selectOneOfYourAccountsTab() {
        Actions.click(this.accountOptionsScreen.oneOfYourAccountsTab);
    }

    verifyOneOfYourAccountsTab() {
        Actions.isVisible(this.accountOptionsScreen.oneOfYourAccountsTab)
            .should.equal(true);
    }
}

module.exports = AccountOptionsPage;
