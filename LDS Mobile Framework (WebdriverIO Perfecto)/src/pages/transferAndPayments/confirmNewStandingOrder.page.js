const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class ConfirmNewStandingOrderPage extends AuthPage {
    /**
     * @returns {ConfirmStandingOrderScreen}
     */
    get confirmStandingOrderScreen() {
        return this.getNativeScreen('transferAndPayments/confirmStandingOrder.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.confirmStandingOrderScreen.title);
    }

    enterPassword(password) {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.confirmStandingOrderScreen.confirmPassword
        );
        Actions.setImmediateValue(this.confirmStandingOrderScreen.confirmPassword, password);
    }

    confirmStandingOrder() {
        Actions.click(this.confirmStandingOrderScreen.confirmButton);
    }
}

module.exports = ConfirmNewStandingOrderPage;
