const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class AmendStandingOrderPage extends AuthPage {
    /**
     * @returns {AmendStandingOrderScreen}
     */
    get amendStandingOrderScreen() {
        return this.getNativeScreen('transferAndPayments/amendStandingOrder.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.amendStandingOrderScreen.title);
    }

    amendStandingOrderAmount(amendAmount) {
        Actions.click(this.amendStandingOrderScreen.amendStandingOrderAmount);
        Actions.clearText(this.amendStandingOrderScreen.amendStandingOrderAmount);
        Actions.setImmediateValue(this.amendStandingOrderScreen.amendStandingOrderAmount, amendAmount);
        Actions.swipeUntilVisible(SwipeAction.UP, this.amendStandingOrderScreen.amendButton);
        Actions.click(this.amendStandingOrderScreen.amendButton);
    }
}

module.exports = AmendStandingOrderPage;
