const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const SwipeAction = require('../../enums/swipeAction.enum');

class StandingOrderSetupSuccessPage extends AuthPage {
    /**
     * @returns {StandingOrderSetupSuccessScreen}
     */
    get standingOrderSetupSuccessScreen() {
        return this.getNativeScreen('transferAndPayments/standingOrderSetupSuccess.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.standingOrderSetupSuccessScreen.title);
    }

    clickViewStandingOrder() {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.standingOrderSetupSuccessScreen.viewStandingOrdersButton
        );
        Actions.click(this.standingOrderSetupSuccessScreen.viewStandingOrdersButton);
    }
}

module.exports = StandingOrderSetupSuccessPage;
