const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class PaymentReviewPage extends AuthPage {
    /**
     * @returns {PaymentReviewScreen}
     */
    get paymentReviewScreen() {
        return this.getNativeScreen('transferAndPayments/paymentReview.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.paymentReviewScreen.title);
    }

    confirmPayment() {
        Actions.click(this.paymentReviewScreen.confirmButton);
    }
}

module.exports = PaymentReviewPage;
