const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class ConfirmDirectDebitCancelPage extends AuthPage {
    /**
     * @returns {cancelDirectDebitScreen}
     */
    get confirmDirectDebitCancelScreen() {
        return this.getNativeScreen('transferAndPayments/confirmDirectDebitCancel.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.confirmDirectDebitCancelScreen.title);
    }

    confirmDirectDebitCancel(password) {
        Actions.waitForVisible(this.confirmDirectDebitCancelScreen.cancelPassword);
        Actions.setImmediateValue(this.confirmDirectDebitCancelScreen.cancelPassword, password);
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.confirmDirectDebitCancelScreen.cancelDirectDebit
        );
        Actions.click(this.confirmDirectDebitCancelScreen.cancelDirectDebit);
    }
}

module.exports = ConfirmDirectDebitCancelPage;
