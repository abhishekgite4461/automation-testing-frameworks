const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const DeviceHelper = require('../../utils/device.helper');

class UkCompanySearchResultsPage extends AuthPage {
    /**
     * @returns {UkCompanySearchResultsScreen}
     */
    get ukCompanySearchResultsScreen() {
        return this.getNativeScreen('transferAndPayments/ukCompanySearchResults.screen');
    }

    /**
     * @returns {ukBeneficiaryDetail}
     */
    get ukBeneficiaryDetailScreen() {
        return this.getNativeScreen('transferAndPayments/ukBeneficiaryDetail.screen');
    }

    /**
     * @returns {reactivateIsaScreen}
     */
    get reactivateIsaScreen() {
        return this.getNativeScreen('paymentHub/reactivateIsa.screen');
    }

    /**
     * @returns {beneficiaryTypeScreen}
     */
    get beneficiaryTypeScreen() {
        return this.getNativeScreen('transferAndPayments/beneficiaryType.screen');
    }

    isUKCompanyReviewDetailPageVisible() {
        Actions.isVisible(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewReferenceInput).should.equal(true);
    }

    isUKReviewPageDetailVisible() {
        Actions.isVisible(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewReferenceInput).should.equal(true);
    }

    validateCompanyBasicDetails(companyList) {
        for (let i = 0; i < companyList.length; i++) {
            const individualDetail = this.ukCompanySearchResultsScreen[companyList[i].fields];
            Actions.isVisible(individualDetail).should.equal(true);
        }
    }

    verifyMatchingList(matchingList) {
        for (let i = 0; i < matchingList.length; i++) {
            const individualDetail = this.ukCompanySearchResultsScreen[matchingList[i].results];
            Actions.isExisting(individualDetail).should.equal(true);
        }
    }

    addReference(reference, reEnterReference) {
        Actions.setImmediateValueAndTapOut(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewReferenceInput, reference);
        Actions.setImmediateValueAndTapOut(
            this
                .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewReferenceConfirmInput,
            reEnterReference
        );
    }

    verifyUkCompanySearchResultsPage() {
        Actions.isVisible(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountSelectionTitle).should.equal(true);
    }

    selectResult() {
        Actions.click(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountSelectionCompanyList);
    }

    verifyUkCompanyReviewDetailPage() {
        Actions.waitForVisible(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewPayeeNameValue);
    }

    verifyBasicDetailsOnReviewDetailsPage() {
        Actions.isExisting(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewPayeeNameValue).should.equal(true);
        Actions.isExisting(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewSortCodeValue).should.equal(true);
        Actions.isExisting(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewAccountNumberValue).should.equal(true);
    }

    navigateBack() {
        Actions.click(this.commonScreen.modalBackButton);
    }

    unableToContinueToAddRecipient() {
        Actions.isEnabled(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewContinueButton)
            .should.equal(false);
    }

    verifyWinBackOnNegativeAction() {
        Actions.click(this.reactivateIsaScreen.dialogNegativeAction);
        Actions.isVisible(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewContinueButton);
    }

    enterReference(reference) {
        Actions.setImmediateValue(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewReferenceInput, reference);
        Actions.tapOutOfField();
    }

    selectSecurityLink() {
        Actions.click(this
            .ukBeneficiaryDetailScreen.paymentHubAddUkAccountSecurityInfoText);
    }

    waitForWarningOverlay() {
        Actions.waitForVisible(this.reactivateIsaScreen.dialogPositiveAction);
    }

    closeWarningOverlay() {
        Actions.click(this.reactivateIsaScreen.dialogPositiveAction);
    }

    confirmReviewDetailsPage() {
        if (DeviceHelper.isAndroid()) {
            Actions.hideDeviceKeyboard();
        }
        Actions.waitForVisible(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewContinueButton);
        Actions.click(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewContinueButton);
    }

    cancelWinBack() {
        Actions.click(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewCancelButton);
    }

    verifyWinBackOnPositiveAction() {
        Actions.click(this
            .ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewCancelButton);
        Actions.click(this.reactivateIsaScreen.dialogPositiveAction);
        Actions.isExisting(this.beneficiaryTypeScreen.ukAccount).should.equal(true);
    }

    ClearReferenceFields() {
        Actions.clearText(this.ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewReferenceInput);
        Actions.clearText(this.ukCompanySearchResultsScreen.paymentHubAddUkAccountReviewReferenceConfirmInput);
    }
}

module.exports = UkCompanySearchResultsPage;
