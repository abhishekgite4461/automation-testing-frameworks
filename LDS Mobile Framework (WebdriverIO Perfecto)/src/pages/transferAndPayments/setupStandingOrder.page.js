const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const SwipeAction = require('../../enums/swipeAction.enum');

class SetupStandingOrderPage extends AuthPage {
    /**
     * @returns {SetupStandingOrderScreen}
     */
    get setupStandingOrderScreen() {
        return this.getNativeScreen('transferAndPayments/setupStandingOrder.screen');
    }

    selectStandingOrderAccount() {
        Actions.click(this.setupStandingOrderScreen.selectAccountDropdown);
        Actions.click(this.setupStandingOrderScreen.selectAccountFromList);
    }

    standingOrderReference(reference) {
        Actions.setImmediateValue(this.setupStandingOrderScreen.standingOrderReferenceText, reference);
    }

    enterStandingOrderAmount(amount) {
        Actions.setImmediateValue(this.setupStandingOrderScreen.standingOrderAmount, amount);
    }

    selectFrequency() {
        Actions.click(this.setupStandingOrderScreen.selectFrequencyDropdown);
        Actions.click(this.setupStandingOrderScreen.standingOrderFrequency);
    }

    selectContinueStandingOrder() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.setupStandingOrderScreen.continueButton);
        Actions.click(this.setupStandingOrderScreen.continueButton);
    }

    transactionLimitExceededIsVisible() {
        Actions.swipeUntilVisible(
            SwipeAction.DOWN,
            this.setupStandingOrderScreen.transactionLimitExceeded
        );
    }
}

module.exports = SetupStandingOrderPage;
