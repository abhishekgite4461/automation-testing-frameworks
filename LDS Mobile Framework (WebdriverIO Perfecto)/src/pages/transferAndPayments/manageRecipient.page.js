const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');

class ManageRecipientPage extends AuthPage {
    /**
     * @returns {ManageRecipientScreen}
     */
    get manageRecipientScreen() {
        return this.getNativeScreen('transferAndPayments/manageRecipient.screen');
    }

    get standingOrderScreen() {
        return this.getNativeScreen('standingOrder.screen');
    }

    get actionMenuScreen() {
        return this.getNativeScreen('actionMenu.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.manageRecipientScreen.title);
    }

    verifyDeleteButton() {
        Actions.isVisible(this.manageRecipientScreen.deleteRecipientButton).should.equal(true);
    }

    verifyMakePaymentButton() {
        Actions.isVisible(this.manageRecipientScreen.makePaymentButton).should.equal(true);
    }

    verifyCancelButton() {
        Actions.isVisible(this.manageRecipientScreen.closeButton).should.equal(true);
    }

    clickCancelButton() {
        Actions.click(this.manageRecipientScreen.closeButton);
    }

    verifyHeader() {
        Actions.isVisible(this.manageRecipientScreen.actionMenuHeader).should.equal(true);
    }

    getAccountTitle() {
        return Actions.getText(this.manageRecipientScreen.actionMenuHeader);
    }

    verifyPayRecipientIsNotVisible() {
        Actions.waitForNotVisible(this.manageRecipientScreen.makePaymentButton);
    }

    verifyDeleteRecipientIsNotVisible() {
        Actions.isVisible(this.manageRecipientScreen.deleteRecipientButton).should.equal(false);
    }

    closePendingPaymentPopup() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.actionMenuScreen.closeButton);
        } else {
            Actions.pressAndroidBackKey();
        }
    }

    cancelModalAndLeave() {
        if (DeviceHelper.isIOS()) {
            Actions.waitForVisible(this.commonScreen.cancelLink);
            Actions.click(this.commonScreen.cancelLink);
        }
        Actions.click(this.commonScreen.modalCloseIcon);
        if (Actions.isVisible(this.commonScreen.winBackTitle)) {
            Actions.click(this.commonScreen.winBackLeaveButton);
        }
    }

    verifyViewPendingPaymentsIsVisible() {
        Actions.swipeByPercentUntilVisible(this.manageRecipientScreen.viewPendingPayment);
        Actions.isVisible(this.manageRecipientScreen.viewPendingPayment).should.equal(true);
    }

    selectDeleteOnAccount() {
        Actions.click(this.manageRecipientScreen.deleteRecipientButton);
    }

    selectKeepAccount() {
        Actions.click(this.manageRecipientScreen.dialogNegativeAction);
    }

    verifyConfirmationDiagueBoxIsVisible() {
        Actions.click(this.manageRecipientScreen.dialogueBoxTitle);
    }

    selectConfirmDeleteOnAccount() {
        Actions.click(this.manageRecipientScreen.dialogPositiveAction);
    }

    clickCancelSearch() {
        Actions.click(this.manageRecipientScreen.cancelSearch);
    }

    verifyRecipientCouldNotBeDeletedDueToStandingOrder() {
        Actions.isVisible(this.manageRecipientScreen.standingOrderDialogBox).should.equal(true);
    }

    verifyOptionToViewStandingOrderIsSeen() {
        Actions.isVisible(this.manageRecipientScreen.viewButton).should.equal(true);
    }

    verifyOptionCloseOrderIsSeen() {
        Actions.isVisible(this.manageRecipientScreen.closeButtonStandingOrder).should.equal(true);
    }

    clickViewOption() {
        Actions.click(this.manageRecipientScreen.viewButton);
    }

    clickCloseButton() {
        Actions.click(this.manageRecipientScreen.closeButtonStandingOrder);
    }

    verifyStandingOrderPageIsSeen() {
        Actions.isVisible(this.standingOrderScreen.title).should.equal(true);
    }

    clickViewPendingPaymentsOption() {
        Actions.waitAndClick(this.manageRecipientScreen.viewPendingPayment);
    }
}

module.exports = ManageRecipientPage;
