const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class AmendStandingOrderSuccessPage extends AuthPage {
    /**
     * @returns {AmendStandingOrderSuccessScreen}
     */
    get amendStandingOrderSuccessScreen() {
        return this.getNativeScreen('transferAndPayments/amendStandingOrderSuccess.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.amendStandingOrderSuccessScreen.title);
    }

    clickViewStandingOrders() {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.amendStandingOrderSuccessScreen.viewStandingOrders
        );
        Actions.click(this.amendStandingOrderSuccessScreen.viewStandingOrders);
    }
}

module.exports = AmendStandingOrderSuccessPage;
