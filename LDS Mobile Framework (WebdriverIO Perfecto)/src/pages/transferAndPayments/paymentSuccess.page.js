require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const AccountMatcher = require('../../data/utils/accountMatcher');

class PaymentSuccessPage extends AuthPage {
    /**
     * @returns {PaymentSuccessScreen}
     */
    get successPaymentScreen() {
        return this.getNativeScreen('paymentHub/successPayment.screen');
    }

    waitForPageLoad() {
        const locator = Actions.waitUntilVisible([
            this.successPaymentScreen.title,
            this.successPaymentScreen.notNow
        ], 15000);

        if (locator === this.successPaymentScreen.notNow) {
            Actions.click(this.successPaymentScreen.notNow);
        }
    }

    validateAccountDetails(type) {
        const toAccount = type === 'payment'
            ? (SavedAppDataRepository.getData(SavedData.PAYMENT_TO_ACCOUNT).toLowerCase()).match(/(\w+)/g)[3]
            : AccountMatcher.getAccount(type).toLowerCase();
        const amount = SavedAppDataRepository.getData(SavedData.PAYMENT_AMOUNT);
        Actions.getText(this.successPaymentScreen.paymentProcessed).toLowerCase().should.contains(toAccount);
        Actions.getText(this.successPaymentScreen.paymentProcessed).toLowerCase().should.contains(amount);
    }

    viewBannerPlacement() {
        Actions.waitForVisible(this.successPaymentScreen.bannerLeadPlacement);
        Actions.isVisible(this.successPaymentScreen.bannerLeadPlacement).should.equal(true);
    }
}

module.exports = PaymentSuccessPage;
