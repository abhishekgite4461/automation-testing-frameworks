const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class DeleteStandingOrderPage extends AuthPage {
    /**
     * @returns {DeleteStandingOrderScreen}
     */
    get deleteStandingOrderScreen() {
        return this.getNativeScreen('transferAndPayments/deleteStandingOrder.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.deleteStandingOrderScreen.title);
    }

    enterPassword(password) {
        Actions.swipeUntilVisible(SwipeAction.UP,
            this.confirmStandingOrderScreen.confirmDeletePassword);
        Actions.setImmediateValue(this.confirmStandingOrderScreen.confirmDeletePassword, password);
    }

    confirmDeleteStandingOrder() {
        Actions.click(this.deleteStandingOrderScreen.confirmDeleteButton);
    }
}

module.exports = DeleteStandingOrderPage;
