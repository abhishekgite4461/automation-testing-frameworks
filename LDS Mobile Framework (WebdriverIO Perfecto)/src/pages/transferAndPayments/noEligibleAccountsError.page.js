require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class NoEligibleAccountsError extends AuthPage {
    /**
     * @returns {noEligibleAccountsErrorScreen}
     */
    get noEligibleAccountsErrorScreen() {
        return this.getNativeScreen('transferAndPayments/noEligibleAccountsError.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.noEligibleAccountsErrorScreen.title);
    }

    verifyApplyForAccountsButton() {
        Actions.isVisible(this.noEligibleAccountsErrorScreen.applyForAccountsButton)
            .should.equal(true);
    }

    clickOnHomeButton() {
        Actions.click(this.noEligibleAccountsErrorScreen.homeButon);
    }

    clickOnApplyForAccountsButton() {
        Actions.click((this.noEligibleAccountsErrorScreen.applyForAccountsButton));
    }
}

module.exports = NoEligibleAccountsError;
