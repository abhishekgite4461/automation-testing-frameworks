const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class PendingPaymentPage extends AuthPage {
    /**
     * @returns {PendingPaymentScreen}
     */
    get pendingPaymentScreen() {
        return this.getNativeScreen('transferAndPayments/pendingPayments.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.pendingPaymentScreen.title);
    }

    clickBackButton() {
        Actions.click(this.pendingPaymentScreen.backButton);
    }
}

module.exports = PendingPaymentPage;
