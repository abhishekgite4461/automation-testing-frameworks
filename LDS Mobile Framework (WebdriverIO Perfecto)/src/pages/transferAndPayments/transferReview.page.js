const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const DeviceHelper = require('../../utils/device.helper');

class TransferReviewPage extends AuthPage {
    /**
     * @returns {TransferReviewScreen}
     */
    get transferReviewScreen() {
        return this.getNativeScreen('transferAndPayments/transferReview.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.transferReviewScreen.title);
    }

    reviewTransfer() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.transferReviewScreen.confirmButton);
            Actions.click(this.transferReviewScreen.cancelButton);
        } else {
            Actions.click(this.transferReviewScreen.confirmButton);
        }
    }
}

module.exports = TransferReviewPage;
