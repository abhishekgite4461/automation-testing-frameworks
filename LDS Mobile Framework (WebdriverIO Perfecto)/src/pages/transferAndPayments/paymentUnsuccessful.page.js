require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');

class PaymentUnsuccessfulPage extends AuthPage {
    /**
     * @returns {PaymentUnsuccessfulScreen}
     */
    get paymentUnsuccessfulScreen() {
        return this.getNativeScreen('transferAndPayments/paymentUnsuccessful.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.paymentUnsuccessfulScreen.title);
    }

    viewBannerPlacement() {
        Actions.waitForVisible(this.paymentUnsuccessfulScreen.bannerLeadPlacement);
        Actions.isVisible(this.paymentUnsuccessfulScreen.bannerLeadPlacement).should.equal(true);
    }

    shouldSeeCbsProhibitiveMessage() {
        if (DeviceHelper.isRNGA()) {
            Actions.waitForVisible(this.paymentUnsuccessfulScreen.cbsProhibitiveMessage);
        } else {
            Actions.waitForVisible(this.paymentUnsuccessfulScreen.BngacbsProhibitiveMessage);
        }
    }
}

module.exports = PaymentUnsuccessfulPage;
