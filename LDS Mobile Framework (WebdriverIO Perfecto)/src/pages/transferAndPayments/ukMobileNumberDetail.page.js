const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class UkMobileNumberDetailPage extends AuthPage {
    /**
     * @returns {ukMobileNumberDetailScreen}
     */
    get ukMobileNumberDetailScreen() {
        return this.getNativeScreen('transferAndPayments/ukMobileNumberDetail.screen');
    }

    verifyUkMobileNumberDetails(cutomerMobileDetails) {
        for (let i = 0; i < cutomerMobileDetails.length; i++) {
            const mobileDetails = this.ukMobileNumberDetailScreen[cutomerMobileDetails[i].detail];
            Actions.isVisible(mobileDetails);
        }
    }

    waitForUKMobileDetailsPageLoad() {
        Actions.waitForVisible(this.ukMobileNumberDetailScreen.title);
    }

    enterMobileNumber(mobileNumber) {
        Actions.setImmediateValueAndTapOut(this.ukMobileNumberDetailScreen.mobileNumber, mobileNumber);
    }

    enterAmount(amount) {
        Actions.setImmediateValueAndTapOut(this.ukMobileNumberDetailScreen.amount, amount);
    }

    enterReference(reference) {
        Actions.setImmediateValueAndTapOut(this.ukMobileNumberDetailScreen.reference, reference);
    }

    clickOnContinueButtonInPaymentHubPage() {
        Actions.click(this.ukMobileNumberDetailScreen.continueButtonInPaymentHubPage);
    }

    verifyErrorMessage() {
        Actions.isExisting(this.ukMobileNumberDetailScreen.errorMessageForAddPaym).should.equal(true);
    }

    verifyAddPayMPage() {
        Actions.isVisible(this.ukMobileNumberDetailScreen.verifyingaddPayMPage).should.equal(true);
    }

    verifyUKMobileDetailPage() {
        Actions.isExisting(this.ukMobileNumberDetailScreen.title).should.equal(true);
    }
}

module.exports = UkMobileNumberDetailPage;
