const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class BeneficiaryTypePage extends AuthPage {
    /**
     * @returns {BeneficiaryTypeScreen}
     */
    get beneficiaryTypeScreen() {
        return this.getNativeScreen('transferAndPayments/beneficiaryType.screen');
    }

    verifyBeneficiaryCategory(categoryDetails) {
        for (let i = 0; i < categoryDetails.length; i++) {
            const individualCategoryDetail = this.beneficiaryTypeScreen[categoryDetails[i].detail];
            Actions.isVisible(individualCategoryDetail).should.equal(true);
        }
    }

    chooseUkAccountBeneficiary() {
        Actions.click(this.beneficiaryTypeScreen.ukAccount);
    }

    verifyPayMisDisabled() {
        Actions.isEnabled(this.beneficiaryTypeScreen.ukMobileNumber).should.equal(false);
    }

    chooseUkMobileNumber() {
        Actions.click(this.beneficiaryTypeScreen.ukMobileNumber);
    }

    clickPayUkNumberLink() {
        Actions.click(this.beneficiaryTypeScreen.payingUkMobileNumberLink);
    }

    viewUkAccountVisible() {
        Actions.isExisting(this.beneficiaryTypeScreen.ukAccount).should.equal(true);
    }

    selectInternationalBankAccount() {
        Actions.click(this.beneficiaryTypeScreen.internationalBankAccount);
    }

    verifyUkMobileNumber() {
        Actions.click(this.beneficiaryTypeScreen.ukMobileNumber);
    }

    viewPayUkNumberLinkVisible() {
        Actions.isExisting(this.beneficiaryTypeScreen.payingUkMobileNumberLink).should.equal(true);
    }
}

module.exports = BeneficiaryTypePage;
