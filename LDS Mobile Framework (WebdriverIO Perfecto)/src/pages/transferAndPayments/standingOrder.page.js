require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const SwipeAction = require('../../enums/swipeAction.enum');
const FrequencyType = require('../../enums/frequencyType.enum');
const DeviceHelper = require('../../utils/device.helper');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum.js');
const DateHelper = require('../../utils/date.helper');

class StandingOrderPage extends AuthPage {
    /**
     * @returns {StandingOrderScreen}
     */
    get standingOrderScreen() {
        return this.getNativeScreen('paymentHub/standingOrder.screen');
    }

    /**
     * @returns {PaymentHubMainScreen}
     */
    get paymentHubMainScreen() {
        return this.getNativeScreen('paymentHub/paymentHubMain.screen');
    }

    /**
     * @returns {StandingOrderReviewScreen}
     */
    get standingOrderReviewScreen() {
        return this.getNativeScreen('paymentHub/standingOrderReview.screen');
    }

    /**
     * @returns {CommonScreen}
     */
    get commonScreen() {
        return this.getNativeScreen('common/common.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.standingOrderScreen.title);
    }

    selectStandingOrder() {
        Actions.click(this.standingOrderScreen.title);
    }

    selectSetupStandingOrder() {
        Actions.click(this.standingOrderScreen.setupStandingOrderButton);
    }

    selectAmendStandingOrder() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.standingOrderScreen.amendButton);
        Actions.click(this.standingOrderScreen.amendButton);
    }

    selectDeleteStandingOrder() {
        Actions.click(this.standingOrderScreen.deleteButton);
    }

    verifyNewStandingOrder(reference) {
        Actions.swipeUntilAnyVisible(
            SwipeAction.UP, this.standingOrderScreen.newStandingOrder(reference)
        );
    }

    verifyDeleteStandingOrderSuccessMsg() {
        Actions.isVisible(this.standingOrderScreen.deleteSuccess).should.equal(true);
    }

    verifyDeleteStandingOrderNotVisible() {
        Actions.isVisible(this.standingOrderScreen.deleteButton).should.equal(false);
    }

    verifyStandingOrderNotCancelledMsg() {
        Actions.isVisible(this.standingOrderScreen.nonCancellableSoMessage).should.equal(true);
    }

    toggleSODismissAlertISA(toAccount) {
        Actions.click(this.paymentHubMainScreen.standingOrderToggleSwitch);
        if (toAccount === 'fallowIsa') {
            Actions.click(this.commonScreen.winBackOkButton);
        }
    }

    toggleStandingOrder() {
        Actions.click(this.paymentHubMainScreen.standingOrderToggleSwitch);
    }

    selectStandingOrderFrequencyDropDown() {
        Actions.click(this.paymentHubMainScreen.selectFrequencyDropdown);
    }

    selectStandingOrderFrequency(frequency) {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.paymentHubMainScreen.standingOrderFrequency(frequency));
        } else {
            const array = FrequencyType.asArray();
            Actions.setIOSPickerWheelValue(this.paymentHubMainScreen.selectFrequencyDropdown,
                frequency, array);
            Actions.tapOutOfField();
        }
    }

    selectStandingOrderSwitch() {
        Actions.click(this.paymentHubMainScreen.standingOrderToggleSwitch);
    }

    warningMessageISA() {
        Actions.isExisting(this.paymentHubMainScreen.warningMessageTitle).should.equal(true);
    }

    validateISAWarningMessageForSO(toAccount) {
        if (toAccount === 'htbIsa') {
            Actions.isExisting(this.paymentHubMainScreen.warningMessageContentHTB).should.equal(true);
        } else {
            Actions.isExisting(this.paymentHubMainScreen.warningMessageContent).should.equal(true);
        }
    }

    selectEditStandingOrder() {
        Actions.click(this.standingOrderReviewScreen.reviewPaymentEditButton);
    }

    prePopulatedSO() {
        Actions.isExisting(this.paymentHubMainScreen.amount).should.equal(true);
        Actions.isExisting(this.paymentHubMainScreen.standingOrderToggleSwitch).should.equal(true);
        Actions.isExisting(this.paymentHubMainScreen.frequencyPicker).should.equal(true);
        Actions.isExisting(this.paymentHubMainScreen.paymentHubStandingOrderFirstPaymentDate).should.equal(true);
        Actions.isExisting(this.paymentHubMainScreen.paymentHubStandingOrderLastPaymentDate).should.equal(true);
    }

    selectBackFromReviewSO() {
        Actions.click(this.commonScreen.backButton);
    }

    static daysInMonth(month, year) {
        return new Date(year, month, 0).getDate();
    }

    selectStandingOrderStartDate() {
        const today = new Date();
        const selectedDate = new Date();
        selectedDate.setDate(1);
        selectedDate.setMonth(today.getMonth() + 1);

        SavedAppDataRepository.setData(SavedData.STANDING_ORDER_START_DATE, selectedDate);

        // start date is set to first day of next month, to simplify date selection logic
        Actions.click(this.paymentHubMainScreen.paymentHubStandingOrderFirstPaymentDate);
        if (StandingOrderPage.daysInMonth(today.getMonth() + 1, today.getFullYear()) !== today.getDate()) {
            Actions.click(this.paymentHubMainScreen.nextMonthButton);
        }
        Actions.click(this.paymentHubMainScreen.dateSelector('1'));
    }

    selectStandingOrderEndDate(frequency) {
        Actions.swipeByPercentUntilVisible(this.paymentHubMainScreen.paymentHubStandingOrderLastPaymentDate);
        Actions.click(this.paymentHubMainScreen.paymentHubStandingOrderLastPaymentDate);
        const selectedDate = SavedAppDataRepository.getData(SavedData.STANDING_ORDER_START_DATE);
        const returnDate = StandingOrderPage.nextActiveEndDate(selectedDate, frequency);
        Actions.click(this.paymentHubMainScreen.dateSelector(returnDate.getDate()));
    }

    selectFrequency(frequencyType) {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.paymentHubMainScreen.frequencyPicker);
            const array = FrequencyType.asArray();
            Actions.setIOSPickerWheelValue(this.paymentHubMainScreen.frequencySelector,
                frequencyType, array);
            Actions.tapOutOfField();
        } else {
            Actions.click(this.paymentHubMainScreen.frequencyPicker);
            Actions.click(this.paymentHubMainScreen.frequencySelector(frequencyType));
        }
    }

    selectMonthAndYear(year, frequency) {
        let availableYears = [];
        const selectedDate = SavedAppDataRepository.getData(SavedData.STANDING_ORDER_START_DATE);
        const returnDate = StandingOrderPage.nextActiveEndDate(selectedDate, frequency);
        const nextActiveYear = parseInt(returnDate.getFullYear().toString(), 10);

        if (DeviceHelper.isIOS()) {
            availableYears = StandingOrderPage.createListOfAvailableYears(nextActiveYear, year);
            Actions.setIOSPickerWheelValue(this.paymentHubMainScreen.yearPickerWheel,
                parseInt(year, 10), availableYears);
            Actions.tapOutOfField();
        } else {
            // selecting next active year on android devices
            availableYears = StandingOrderPage.createListOfAvailableYears(nextActiveYear + 1, year);
            StandingOrderPage.selectYearOnAndroid(this.paymentHubMainScreen.yearPickerWheelOnly, availableYears);
            Actions.click(this.paymentHubMainScreen.monthYearSelectButton);
        }
    }

    selectYear(year, frequency) {
        let availableYears = [];
        const selectedDate = SavedAppDataRepository.getData(SavedData.STANDING_ORDER_START_DATE);
        const returnDate = StandingOrderPage.nextActiveEndDate(selectedDate, frequency);
        const nextActiveYear = parseInt(returnDate.getFullYear().toString(), 10);

        if (DeviceHelper.isIOS()) {
            availableYears = StandingOrderPage.createListOfAvailableYears(nextActiveYear, year);
            Actions.setIOSPickerWheelValue(this.paymentHubMainScreen.yearPickerWheelOnly,
                year, availableYears);
            Actions.tapOutOfField();
        } else {
            // selecting next active year on android devices
            availableYears = StandingOrderPage.createListOfAvailableYears(nextActiveYear + 1, year);
            StandingOrderPage.selectYearOnAndroid(this.paymentHubMainScreen.yearPickerWheelOnly, availableYears);
            Actions.click(this.paymentHubMainScreen.monthYearSelectButton);
        }
    }

    static selectDateOffset(days) {
        const date = new Date();
        return new Date(date.setTime(date.getTime() + days * 86400000));
    }

    chooseStandingOrderStartDate() {
        Actions.click(this.paymentHubMainScreen.paymentHubStandingOrderFirstPaymentDate);
    }

    chooseStandingOrderEndDate() {
        Actions.swipeByPercentUntilVisible(this.paymentHubMainScreen.paymentHubStandingOrderLastPaymentDate);
        Actions.click(this.paymentHubMainScreen.paymentHubStandingOrderLastPaymentDate);
    }

    closeCalendar() {
        if (Actions.isVisible(this.paymentHubMainScreen.cancelCalendar)) {
            Actions.click(this.paymentHubMainScreen.cancelCalendar);
        }
    }

    selectMonthYearPicker() {
        Actions.click(this.paymentHubMainScreen.monthYearPicker);
    }

    yearPickerEnabled() {
        Actions.isExisting(this.paymentHubMainScreen.yearPickerWheel).should.equal(true);
    }

    monthPickerEnabled() {
        Actions.isExisting(this.paymentHubMainScreen.monthPickerWheel).should.equal(true);
    }

    selectDateAfter31Days() {
        const today = new Date();
        const nextDay = new Date();
        nextDay.setDate(today.getDate() + 1);
        const presentMonth = nextDay.getMonth().toString();
        const futureDate = StandingOrderPage.selectDateOffset(31);
        const futureMonth = futureDate.getMonth().toString();
        const differenceInMonths = (futureMonth === '0' && presentMonth === '11') ? 1 : futureMonth - presentMonth;
        for (let i = 0; i < differenceInMonths; i++) {
            Actions.click(this.paymentHubMainScreen.nextMonthButton);
        }
        Actions.click(this.paymentHubMainScreen.dateSelector(futureDate.getDate()));
        this.closeCalendar();
    }

    enterAmount(amount) {
        Actions.setImmediateValueAndTapOut(this.paymentHubMainScreen.amount, amount.toString());
    }

    enterReference(reference) {
        Actions.swipeByPercentUntilVisible(this.paymentHubMainScreen.standingOrderReference,
            20, 80, 20, 70, 100, 3);
        Actions.setImmediateValue(this.paymentHubMainScreen.standingOrderReference, reference.toString());
        Actions.tapOutOfField();
    }

    reviewStandingOrder() {
        Actions.swipeByPercentUntilVisible(this.paymentHubMainScreen.continueButton);
        Actions.click(this.paymentHubMainScreen.continueButton);
    }

    static nextActiveEndDate(selectedDate, frequencyType) {
        const returnDate = new Date(selectedDate.toDateString());
        switch (frequencyType) {
        case 'Weekly':
            returnDate.setDate(selectedDate.getDate() + 7);
            break;
        case 'Every four weeks':
            returnDate.setDate(selectedDate.getDate() + 28);
            break;
        case 'Every two months':
            returnDate.setMonth(selectedDate.getMonth() + 2);
            break;
        case 'Quarterly':
            returnDate.setMonth(selectedDate.getMonth() + 3);
            break;
        case 'Half yearly':
            returnDate.setMonth(selectedDate.getMonth() + 6);
            break;
        case 'Yearly':
            returnDate.setMonth(selectedDate.getMonth() + 12);
            break;
        case 'Monthly':
            returnDate.setMonth(selectedDate.getMonth() + 1);
            break;
        default:
            throw new Error('choose frequency');
        }
        return returnDate;
    }

    static selectYearOnAndroid(locator, availableYears) {
        for (const x of availableYears) {
            Actions.waitAndClick(locator(x));
        }
    }

    static createListOfAvailableYears(fromYear, toYear) {
        const availableYears = [];
        for (let i = fromYear; i <= parseInt(toYear, 10); i++) {
            availableYears.push(i);
        }
        return availableYears;
    }

    verifyStandingOrderDeclineMsg() {
        Actions.waitForVisible(this.standingOrderScreen.declineSoPage);
    }

    verifyStandingOrderUnderReviewMsg() {
        Actions.waitForVisible(this.standingOrderScreen.underReviewSoPage);
    }

    shouldSeeStandingOrderOption() {
        Actions.waitForVisible(this.standingOrderScreen.selectViewStandingOrderOption);
    }

    viewBackButton() {
        Actions.waitForVisible(this.commonScreen.backButton);
    }

    viewFieldsOnStandingOrderPage(fieldsToValidate) {
        for (const field of fieldsToValidate) {
            Actions.isVisible(this.standingOrderScreen[field.fieldsOnStandingOrderScreen]).should.equal(true);
        }
    }

    shouldSeeExitInternetBanking() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.standingOrderScreen.exitInternetBanking);
        Actions.isVisible(this.standingOrderScreen.exitInternetBanking).should.equal(true);
    }

    selectStandingOrderOption() {
        Actions.click(this.standingOrderScreen.selectViewStandingOrderOption);
    }

    updateNextDueDate(frequency) {
        const nextActiveDate = StandingOrderPage.getNextAvailablePaymentDate(frequency, new Date());
        Actions.click(this.paymentHubMainScreen.paymentHubStandingOrderFirstPaymentDate);
        this.scrollToAndSelectDate(nextActiveDate);
    }

    updatePaymentEndDate(frequency) {
        let nextActiveDate;
        if (DeviceHelper.isIOS()) {
            const nextDueDate = new Date(Actions
                .getAttributeText(this.paymentHubMainScreen.paymentHubStandingOrderFirstPaymentDate));
            nextActiveDate = StandingOrderPage.getNextAvailablePaymentDate(frequency, nextDueDate);
        } else {
            nextActiveDate = StandingOrderPage.getNextAvailablePaymentDate(frequency, new Date());
        }
        Actions.click(this.paymentHubMainScreen.paymentHubStandingOrderLastPaymentDate);
        this.scrollToAndSelectDate(nextActiveDate);
    }

    scrollToAndSelectDate(nextActiveDate) {
        try {
            for (let i = 0; i < 12; i++) {
                if (!Actions.isVisible(this.paymentHubMainScreen.monthInCalendar(DateHelper
                    .getFullMonthString(nextActiveDate.getMonth()), nextActiveDate.getFullYear()))) {
                    Actions.click(this.paymentHubMainScreen.nextMonthButton);
                } else {
                    break;
                }
            }
            Actions.click(this.paymentHubMainScreen.dateSelector(nextActiveDate.getDate()));
        } catch (e) {
            throw new Error('If failing, please validate the Perfecto device date is correct.');
        }
    }

    static getNextAvailablePaymentDate(frequency, date) {
        const nextActiveDate = StandingOrderPage.nextActiveEndDate(date, frequency);
        SavedAppDataRepository.setData(SavedData.STANDING_ORDER_START_DATE, nextActiveDate);
        return nextActiveDate;
    }

    isStandingOrderFieldEditable(isEditable, fields) {
        for (let i = 0; i < fields.length; i++) {
            switch (`${fields[i]}`) {
            case 'fromAccount':
                Actions.click(this.paymentHubMainScreen.fromAccountPane);
                if (isEditable) {
                    Actions.waitForVisible(this.paymentHubMainScreen.paymentHubRemitanceList);
                    Actions.click(this.paymentHubMainScreen.headerHomeButton);
                }
                Actions.waitForVisible(this.paymentHubMainScreen.title);
                break;

            case 'toAccount':
                Actions.click(this.paymentHubMainScreen.toAccountPane);
                if (isEditable) {
                    Actions.waitForVisible(this.paymentHubMainScreen.paymentHubRecipientsList);
                    Actions.click(this.paymentHubMainScreen.headerHomeButton);
                }
                Actions.waitForVisible(this.paymentHubMainScreen.title);
                break;

            case 'reference':
                Actions.getAttribute(this.paymentHubMainScreen.reference, 'enabled').should.include(isEditable);
                break;

            case 'amount':
                Actions.getAttribute(this.paymentHubMainScreen.amount, 'enabled').should.include(isEditable);
                break;

            case 'nextPaymentDate':
                Actions.getAttribute(this.paymentHubMainScreen.paymentHubStandingOrderFirstPaymentDate, 'enabled').should.include(isEditable);
                break;

            case 'paymentEndDate':
                Actions.getAttribute(this.paymentHubMainScreen.paymentHubStandingOrderLastPaymentDate, 'enabled').should.include(isEditable);
                break;

            case 'frequency':
                Actions.getAttribute(this.paymentHubMainScreen.selectFrequencyDropdown, 'enabled').should.include(isEditable);
                break;

            default:
                throw new Error(`The ${fields[i]} field provided is not valid.`);
            }
        }
    }

    updateFieldValue(field, fieldValue) {
        switch (field) {
        case 'amount':
            this.enterAmount(fieldValue);
            break;

        case 'nextPaymentDate':
            this.updateNextDueDate(fieldValue);
            break;

        case 'paymentEndDate':
            this.updatePaymentEndDate(fieldValue);
            break;

        case 'frequency':
            this.selectFrequency(fieldValue);
            break;

        default:
            throw new Error(`The ${field} field provided is not valid.`);
        }
    }

    getStandingOrderValues() {
        SavedAppDataRepository.setData(SavedData.FROM_ACCOUNT_NAME,
            Actions.getAttributeText(this.paymentHubMainScreen.fromAccountName));
        SavedAppDataRepository.setData(SavedData.SO_FROM_ACCOUNT_SORT_CODE,
            Actions.getAttributeText(this.paymentHubMainScreen.fromSortCodeDetail));
        SavedAppDataRepository.setData(SavedData.SO_FROM_ACCOUNT_NUMBER,
            Actions.getAttributeText(this.paymentHubMainScreen.fromAccountNumberDetail));
        SavedAppDataRepository.setData(SavedData.PAYMENT_TO_ACCOUNT,
            Actions.getAttributeText(this.paymentHubMainScreen.paymentAccountNameDetail));
        SavedAppDataRepository.setData(SavedData.SO_TO_ACCOUNT_SORT_CODE,
            Actions.getAttributeText(this.paymentHubMainScreen.paymentSortCodeDetail));
        SavedAppDataRepository.setData(SavedData.SO_TO_ACCOUNT_NUMBER,
            Actions.getAttributeText(this.paymentHubMainScreen.paymentAccountNumberDetail));
        SavedAppDataRepository.setData(SavedData.PAYMENT_AMOUNT,
            Actions.getAttributeText(this.paymentHubMainScreen.amount));
        SavedAppDataRepository.setData(SavedData.FREQUENCY,
            Actions.getAttributeText(this.paymentHubMainScreen.frequencyPicker));
        SavedAppDataRepository.setData(SavedData.REFERENCE,
            Actions.getAttributeText(this.paymentHubMainScreen.standingOrderReference));
        SavedAppDataRepository.setData(SavedData.STANDING_ORDER_START_DATE,
            Actions.getAttributeText(this.paymentHubMainScreen.paymentHubStandingOrderFirstPaymentDate));
        SavedAppDataRepository.setData(SavedData.STANDING_ORDER_END_DATE,
            Actions.getAttributeText(this.paymentHubMainScreen.paymentHubStandingOrderLastPaymentDate));
    }
}

module.exports = StandingOrderPage;
