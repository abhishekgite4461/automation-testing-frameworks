const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum.js');
const DeviceHelper = require('../../utils/device.helper');

class StandingOrderReviewPage extends AuthPage {
    /**
     * @returns {StandingOrderReviewScreen}
     */
    get StandingOrderReviewScreen() {
        return this.getNativeScreen('paymentHub/standingOrderReview.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.StandingOrderReviewScreen.title);
    }

    clickConfirmButton() {
        Actions.click(this.StandingOrderReviewScreen.confirmStandingOrderButton);
    }

    selectSOReviewConfirmButton() {
        Actions.waitAndClick(this.StandingOrderReviewScreen.reviewPaymentConfirmButton);
    }

    validateFieldValuesOnSOReviewPage(field) {
        for (let i = 0; i < field.length; i++) {
            switch (`${field[i]}`) {
            case 'remitterName':
                Actions.getAttributeText(this.StandingOrderReviewScreen.remitterName).should
                    .equal(SavedAppDataRepository.getData(SavedData.FROM_ACCOUNT_NAME));
                break;

            case 'remitterSortCode':
                Actions.getAttributeText(this.StandingOrderReviewScreen.remitterSortCode).should
                    .equal(SavedAppDataRepository.getData(SavedData.SO_FROM_ACCOUNT_SORT_CODE));
                break;

            case 'remitterAccountNumber':
                Actions.getAttributeText(this.StandingOrderReviewScreen.remitterAccountNumber).should
                    .equal(SavedAppDataRepository.getData(SavedData.SO_FROM_ACCOUNT_NUMBER));
                break;

            case 'benefeciaryName':
                Actions.getAttributeText(this.StandingOrderReviewScreen.benefeciaryName).should
                    .equal(SavedAppDataRepository.getData(SavedData.PAYMENT_TO_ACCOUNT));
                break;

            case 'benefeciarySortCode':
                Actions.getAttributeText(this.StandingOrderReviewScreen.benefeciarySortCode).should
                    .equal(SavedAppDataRepository.getData(SavedData.SO_TO_ACCOUNT_SORT_CODE));
                break;

            case 'benefeciaryAccountNumber':
                Actions.getAttributeText(this.StandingOrderReviewScreen.benefeciaryAccountNumber).should
                    .equal(SavedAppDataRepository.getData(SavedData.SO_TO_ACCOUNT_NUMBER));
                break;

            case 'amount':
                Actions.getAttributeText(this.StandingOrderReviewScreen.amount).should
                    .include(SavedAppDataRepository.getData(SavedData.PAYMENT_AMOUNT));
                break;

            case 'reference':
                Actions.getAttributeText(this.StandingOrderReviewScreen.reference).should
                    .equal(SavedAppDataRepository.getData(SavedData.REFERENCE));
                break;

            case 'standingOrderDates':
                if (DeviceHelper.isAndroid()) {
                    Actions.getAttributeText(this.StandingOrderReviewScreen.standingOrderDates).should
                        .equal('From '.concat(SavedAppDataRepository.getData(SavedData.STANDING_ORDER_START_DATE))
                            .concat('\nUntil ').concat(SavedAppDataRepository.getData(SavedData.STANDING_ORDER_END_DATE)));
                } else {
                    Actions.getAttributeText(this.StandingOrderReviewScreen.standingOrderStartDate).should
                        .equal('From '.concat(SavedAppDataRepository.getData(SavedData.STANDING_ORDER_START_DATE)));
                    Actions.getAttributeText(this.StandingOrderReviewScreen.standingOrderEndDate).should
                        .equal('Until '.concat(SavedAppDataRepository.getData(SavedData.STANDING_ORDER_END_DATE).toLowerCase()));
                }

                break;

            case 'frequency':
                if (DeviceHelper.isAndroid()) {
                    Actions.getAttributeText(this.StandingOrderReviewScreen.frequency).should
                        .equal(SavedAppDataRepository.getData(SavedData.FREQUENCY));
                } else {
                    Actions.getAttributeText(this.StandingOrderReviewScreen.amount).should
                        .include(SavedAppDataRepository.getData(SavedData.FREQUENCY));
                }
                break;

            default:
                throw new Error(`The field ${field} is not correct.`);
            }
        }
    }
}

module.exports = StandingOrderReviewPage;
