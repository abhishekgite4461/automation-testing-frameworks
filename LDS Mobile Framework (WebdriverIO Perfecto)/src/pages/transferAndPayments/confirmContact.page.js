const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ConfirmContactPage extends AuthPage {
    /**
     * @returns {ConfirmContactScreen}
     */
    get confirmContactScreen() {
        return this.getNativeScreen('transferAndPayments/confirmContact.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.confirmContactScreen.title);
    }

    clickOnContinueButtonInConfirmContactPage() {
        Actions.click(this.confirmContactScreen.confirmContinueButton);
    }

    verifyConfirmContactMessage() {
        Actions.isVisible(this.confirmContactScreen.title).should.equal(true);
    }

    verifyingDetailsInConfirmContactPage(mobileDetails) {
        for (let i = 0; i < mobileDetails.length; i++) {
            const confirmingDetail = this.confirmContactScreen[mobileDetails[i].detail];
            Actions.isVisible(confirmingDetail).should.equal(true);
        }
    }

    clickOnCancelButton() {
        Actions.click(this.confirmContactScreen.confirmCancelButton);
    }
}

module.exports = ConfirmContactPage;
