require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class TransferUnsuccessfulPage extends AuthPage {
    /**
     * @returns {TransferUnsuccessfulScreen}
     */
    get transferUnsuccessfulScreen() {
        return this.getNativeScreen('transferAndPayments/transferUnsuccessful.screen');
    }

    viewBannerPlacement() {
        Actions.waitForVisible(this.transferUnsuccessfulScreen.bannerLeadPlacement);
        Actions.isVisible(this.transferUnsuccessfulScreen.bannerLeadPlacement).should.equal(true);
    }
}

module.exports = TransferUnsuccessfulPage;
