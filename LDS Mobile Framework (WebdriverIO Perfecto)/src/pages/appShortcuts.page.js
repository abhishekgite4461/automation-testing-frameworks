require('chai').should();
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');
const AppInfoFactory = require('../apps/appInfo.factory');

class appShortcutsPage extends UnAuthPage {
    get appShortcutsScreen() {
        return this.getNativeScreen('appShortcuts.screen');
    }

    get supportHubHomePageScreen() {
        return this.getNativeScreen('supportHubHomePage.screen');
    }

    get paymentHubMainScreen() {
        return this.getNativeScreen('paymentHub/paymentHubMain.screen');
    }

    get cardManagementScreen() {
        return this.getNativeScreen('cardManagement.screen');
    }

    longPressTheApp() {
        const appName = AppInfoFactory.getAppInfo(browser.capabilities).name;
        Actions.swipeByPercentUntilVisible(this.appShortcutsScreen.appByName(appName), 50, 90, 50, 30);
        Actions.longPressed(this.appShortcutsScreen.appByName(appName));
    }

    shouldSeeAppShortcuts() {
        const brand = browser.capabilities.brand.toLowerCase();
        if (!(brand === 'mbna')) {
            Actions.isVisible(this.appShortcutsScreen.branchFinder).should.equal(true);
            Actions.isVisible(this.appShortcutsScreen.payAndTransfer).should.equal(true);
        }
        Actions.isVisible(this.appShortcutsScreen.contactus).should.equal(true);
        Actions.isVisible(this.appShortcutsScreen.manageCards).should.equal(true);
    }

    shouldSeethreeAppShortcuts() {
        const brand = browser.capabilities.brand.toLowerCase();
        if (!(brand === 'mbna')) {
            Actions.isVisible(this.appShortcutsScreen.branchFinder).should.equal(true);
        }
        Actions.isVisible(this.appShortcutsScreen.contactus).should.equal(true);
        Actions.isVisible(this.appShortcutsScreen.ourWebsite).should.equal(true);
    }

    selectBranchFinder() {
        const brand = browser.capabilities.brand.toLowerCase();
        if (!(brand === 'mbna')) {
            Actions.click(this.appShortcutsScreen.branchFinder);
        }
    }

    shouldSeeBranchFinder() {
        const getBrand = browser.capabilities.brand.toLowerCase();
        Actions.waitForVisible(this.appShortcutsScreen[`${getBrand}BranchInMaps`], 50000);
    }

    selectOurWebsite() {
        Actions.click(this.appShortcutsScreen.ourWebsite);
    }

    selectContactUs() {
        Actions.click(this.appShortcutsScreen.contactus);
    }

    selectPayAndTransfer() {
        Actions.click(this.appShortcutsScreen.payAndTransfer);
    }

    selectManageCards() {
        Actions.click(this.appShortcutsScreen.manageCards);
    }

    shouldSeeContactUs() {
        Actions.waitForVisible(this.supportHubHomePageScreen.title);
    }

    shouldSeePayAndTransfer() {
        Actions.waitForVisible(this.paymentHubMainScreen.title);
    }

    shouldSeeCardManagement() {
        Actions.waitForVisible(this.cardManagementScreen.title);
    }

    shouldSeeOurWebsite() {
        const getBrand = browser.capabilities.brand.toLowerCase();
        Actions.waitForVisible(this.appShortcutsScreen[`${getBrand}Website`], 50000);
    }
}

module.exports = appShortcutsPage;
