const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class LegalAndPrivacyPage extends AuthPage {
    /**
     * @returns {LegalAndPrivacyScreen}
     */
    get legalAndPrivacyScreen() {
        return this.getNativeScreen('helpAndInfo/legalAndPrivacy.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.legalAndPrivacyScreen.title);
    }

    clickBackButton() {
        Actions.click(this.legalAndPrivacyScreen.backButton);
    }
}

module.exports = LegalAndPrivacyPage;
