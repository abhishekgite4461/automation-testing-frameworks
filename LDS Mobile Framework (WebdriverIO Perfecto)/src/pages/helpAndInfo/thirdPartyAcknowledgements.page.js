const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ThirdPartyAcknowledgementsPage extends AuthPage {
    /**
     * @returns {ThirdPartyAcknowledgementsScreen}
     */
    get thirdPartyAcknowledgementsScreen() {
        return this.getNativeScreen('helpAndInfo/thirdPartyAcknowledgements.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.thirdPartyAcknowledgementsScreen.title);
    }

    clickBackButton() {
        Actions.click(this.thirdPartyAcknowledgementsScreen.backButton);
    }
}

module.exports = ThirdPartyAcknowledgementsPage;
