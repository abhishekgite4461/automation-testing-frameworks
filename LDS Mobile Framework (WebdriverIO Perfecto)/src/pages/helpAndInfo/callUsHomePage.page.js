require('chai').should();
const UnAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');

const SwipeAction = require('../../enums/swipeAction.enum');
const ADBHelper = require('../../utils/adb.helper');

const CallUsReason = require('../../enums/callUsReason.enum');

class CallUsHomePage extends UnAuthPage {
    /**
     * @returns {CallUsHomePageScreen}
     */
    get callUsHomePageScreen() {
        return this.getNativeScreen('helpAndInfo/callUsHomePage.screen');
    }

    get callUsNewProductScreen() {
        return this.getNativeScreen('callUs/callUsNewProductEnquiries.screen');
    }

    get callUsScreen() {
        return this.getNativeScreen('callUs/callUs.screen');
    }

    get cmaSurveyScreen() {
        return this.getWebScreen('cmaSurvey.screen');
    }

    waitForPageLoad() {
        if (DeviceHelper.isPerfecto() && DeviceHelper.isAndroid()) {
            Actions.swipePage(SwipeAction.DOWN);
            Actions.perfectoElementInfo('text', this.callUsHomePageScreen.title);
        }
        Actions.waitForVisible(this.callUsHomePageScreen.title);
    }

    shouldNotDisplayNewProductEnquireTile() {
        Actions.isVisible(this.callUsHomePageScreen.newProductEnquireTile).should.equal(false);
    }

    shouldDisplaySecurityAndTravelLabel() {
        if (DeviceHelper.isRNGA()) {
            Actions.isVisible(this.callUsHomePageScreen.securityTravelLabel).should.equal(true);
        } else {
            Actions.isVisible(this.callUsHomePageScreen.securityTravelLabel).should.equal(false);
        }
    }

    shouldNotSeeYourAccountsTile() {
        Actions.isVisible(this.callUsHomePageScreen.personalAccountsTile).should.equal(false);
    }

    clickOnYourAccountsTile() {
        Actions.click(this.callUsHomePageScreen.personalAccountsTile);
    }

    clickOnNewEnquiriesTile() {
        Actions.click(this.callUsHomePageScreen.newProductEnquireTile);
    }

    shouldDisplayBackButton() {
        Actions.isVisible(this.commonScreen.backButton).should.equal(true);
    }

    shouldDisplayCMASurveyLink() {
        Actions.swipeByPercentUntilVisible(this.callUsHomePageScreen.cmaSurveyLink);
    }

    shouldNotDisplayCMASurveyLink() {
        for (let i = 0; i < 2; i++) {
            Actions.swipePage(SwipeAction.UP);
            Actions.isVisible(this.callUsHomePageScreen.cmaSurveyLink).should.equal(false);
        }
    }

    validateTiles(tileOptions) {
        for (let i = 0; i < tileOptions.length; i++) {
            const tileDisplayed = this.callUsHomePageScreen[tileOptions[i].callUsTiles];
            Actions.isVisible(tileDisplayed).should.equal(true);
        }
    }

    validateNewProductOptions(newProducts) {
        for (let i = 0; i < newProducts.length; i++) {
            const servicesDisplayed = this.callUsNewProductScreen[newProducts[i].salesOptions];
            Actions.isVisible(servicesDisplayed).should.equal(true);
        }
    }

    validateNewProductCallusPage(newProducts) {
        for (let i = 0; i < newProducts.length; i++) {
            const reasonToCallOption = this.callUsNewProductScreen[newProducts[i].salesOptions];
            Actions.click(reasonToCallOption);
            Actions.isVisible(this.callUsScreen.callTimeLabel).should.equal(true);
            Actions.isVisible(this.callUsScreen.textPhoneLabel).should.equal(true);
            Actions.click(this.commonScreen.backButton);
        }
    }

    validateReasonToCall(reasonForCallOptions) {
        for (let i = 0; i < reasonForCallOptions.length; i++) {
            const reasonToCallOption = this.callUsHomePageScreen[reasonForCallOptions[i].reasonsToCall];
            if (DeviceHelper.isBNGA() || (DeviceHelper.getBrand() === 'MBNA'
                && reasonToCallOption.includes(this.callUsHomePageScreen.medicalAssistAbroadTab))) {
                Actions.isVisible(reasonToCallOption).should.equal(false);
            } else {
                Actions.swipeByPercentUntilVisible(reasonToCallOption);
            }
        }
    }

    selectReasonForCall(reason, type) {
        this.swipeUntilTheReasonVisible(reason);
        Actions.click(this.getLocatorForReason(reason));
        Actions.reportTimer(type.replace(' ', '_'), 'call us');
    }

    swipeUntilTheReasonVisible(reason) {
        Actions.swipeUntilAnyVisible(
            SwipeAction.UP,
            this.getLocatorForReason(reason)
        );
    }

    shouldSeeCMASurveyResult() {
        if (DeviceHelper.isAndroid() && DeviceHelper.getBrand() === 'HFX') {
            Actions.pause(1000);
            if (Actions.isVisible(this.callUsHomePageScreen.downloadOption)) {
                Actions.click(this.callUsHomePageScreen.downloadOption);
            }
            const notification = ADBHelper.getNotification('service-results');
            notification.slice(22, 37).should.equals('service-results');
        } else {
            Actions.waitForVisible(this.cmaSurveyScreen.cmaSurveyResult);
        }
    }

    getLocatorForReason(reason) {
        switch (reason) {
        case CallUsReason.PERSONAL_ACCOUNTS:
            return this.callUsHomePageScreen.personalAccountsTile;
        case CallUsReason.BUSINESS_ACCOUNTS:
            return this.callUsHomePageScreen.businessAccountsTile;
        case CallUsReason.INTERNET_BANKING:
            return this.callUsHomePageScreen.internetBankingTile;
        case CallUsReason.OTHER_BANKING_QUERY:
            return this.callUsHomePageScreen.otherBankingQueryTile;
        case CallUsReason.SUSPECTED_FRAUD:
            return this.callUsHomePageScreen.suspectedFraudTab;
        case CallUsReason.LOST_OR_STOLEN_CARDS:
            return this.callUsHomePageScreen.lostOrStolenCardTab;
        case CallUsReason.EMERGENCY_CASH_ABROAD:
            return this.callUsHomePageScreen.emergencyCashAbroadTab;
        case CallUsReason.MEDICAL_ASSISTANCE_ABROAD:
            return this.callUsHomePageScreen.medicalAssistAbroadTab;
        default:
            throw new Error(`Unknown reason for call : ${reason}`);
        }
    }
}

module.exports = CallUsHomePage;
