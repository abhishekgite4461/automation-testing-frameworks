const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class CookieUseAndPermissionsPage extends AuthPage {
    /**
     * @returns {CookieUseAndPermissionsScreen}
     */
    get cookieUseAndPermissionsScreen() {
        return this.getNativeScreen('helpAndInfo/cookieUseAndPermissions.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.cookieUseAndPermissionsScreen.title);
    }

    clickBackButton() {
        Actions.click(this.cookieUseAndPermissionsScreen.backButton);
    }
}

module.exports = CookieUseAndPermissionsPage;
