const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');
const runConfig = require('../enums/runConfig.enum');

const EIA_CALL_TIMEOUT = runConfig.TIMEOUTS.EIA_CALL;

class EnrolmentSuccessPage extends UnAuthPage {
    /**
     * @returns {EnrolmentSuccessScreen}
     */
    get enrolmentSuccessScreen() {
        return this.getNativeScreen('enrolmentSuccess.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.enrolmentSuccessScreen.title, EIA_CALL_TIMEOUT());
    }

    clickContinueOnEnrolmentPage() {
        Actions.swipeByPercentUntilVisible(this.enrolmentSuccessScreen.continueButton);
        Actions.click(this.enrolmentSuccessScreen.continueButton);
        Actions.reportTimer('Enrolment_Click_Continue', 'Your security settings');
    }
}

module.exports = EnrolmentSuccessPage;
