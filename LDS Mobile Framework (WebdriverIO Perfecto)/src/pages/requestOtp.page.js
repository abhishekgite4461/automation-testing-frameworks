const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');
require('chai').should();

class RequestOtpPage extends UnAuthPage {
    /**
     * @returns {RequestOtpScreen}
     */
    get requestOtpScreen() {
        return this.getNativeScreen('requestOtp.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.requestOtpScreen.title);
    }

    verifyRequestOtpScreenIsVisible() {
        Actions.isVisible(this.requestOtpScreen.title).should.equal(true);
    }

    selectCancelButton() {
        Actions.click(this.requestOtpScreen.cancelButton);
    }

    selectConfirmButton() {
        Actions.click(this.requestOtpScreen.confirmButton);
    }
}

module.exports = RequestOtpPage;
