const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
require('chai').should();

class AboutPendingTransactionsOverlayPage extends AuthPage {
    /**
     * @returns {AboutPendingTransactionsOverlayScreen}
     */
    get aboutPendingTransactionsOverlayScreen() {
        return this.getNativeScreen('statements/aboutPendingTransactionsOverlay.screen');
    }

    verifyAboutPendingTransactionsOverlay() {
        Actions.isVisible(this.aboutPendingTransactionsOverlayScreen.alertTitle)
            .should.equal(true);
        Actions.isVisible(this.aboutPendingTransactionsOverlayScreen.closeAlert)
            .should.equal(true);
    }

    clickOnCloseButton() {
        Actions.click(this.aboutPendingTransactionsOverlayScreen.closeAlert);
    }
}

module.exports = AboutPendingTransactionsOverlayPage;
