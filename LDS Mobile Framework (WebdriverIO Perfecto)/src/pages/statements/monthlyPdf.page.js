require('chai')
    .should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

const pickerMonthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

class MonthlyPdfPage extends AuthPage {
    /**
     * @returns {MonthlyPdfStatementScreen}
     */
    get monthlyPdfStatementScreen() {
        return this.getNativeScreen('statements/monthlyPdf.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.monthlyPdfStatementScreen.title);
    }

    selectCloseButton() {
        Actions.click(this.monthlyPdfStatementScreen.monthSelectionCloseButton);
    }

    selectViewButton() {
        Actions.click(this.monthlyPdfStatementScreen.monthSelectionViewButton);
    }

    allowPopupForPdf() {
        if (Actions.isVisible(this.monthlyPdfStatementScreen.allowPdf)) {
            Actions.click(this.monthlyPdfStatementScreen.allowPdf);
        }
    }

    waitForPdfPreview() {
        Actions.waitForVisible(this.monthlyPdfStatementScreen.pdfPreviewScreen);
    }

    enterMonthAndYear() {
        Actions.setImmediateValueAndTapOut(this.monthlyPdfStatementScreen.monthField, pickerMonthNames[8]);
        Actions.setImmediateValue(this.monthlyPdfStatementScreen.yearField, (new Date().getFullYear() - 1).toString());
    }

    validateMonthAndYear() {
        Actions.getText(this.monthlyPdfStatementScreen.monthField).should.equal(pickerMonthNames[8]);
        Actions.getText(this.monthlyPdfStatementScreen.yearField).should.equal((new Date().getFullYear() - 1).toString());
    }

    validateDefaultMonthAndYear() {
        Actions.getText(this.monthlyPdfStatementScreen.monthField)
            .should.equal(pickerMonthNames[new Date().getMonth() - 1]);
        Actions.getText(this.monthlyPdfStatementScreen.yearField).should.equal(new Date().getFullYear().toString());
    }

    waitForDigitalInbox() {
        Actions.waitForVisible(this.monthlyPdfStatementScreen.digitalInboxScreen);
    }

    selectInboxBackButton() {
        Actions.click(this.monthlyPdfStatementScreen.inboxBackButton);
    }

    selectPdfViewer() {
        if (Actions.isVisible(this.monthlyPdfStatementScreen.pdfViewer)) {
            Actions.click(this.monthlyPdfStatementScreen.pdfViewer);
        }
    }
}

module.exports = MonthlyPdfPage;
