require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class SearchTransactionsPage extends AuthPage {
    /**
     * @returns {SearchTransactionsScreen}
     */
    get searchTransactionsScreen() {
        return this.getNativeScreen('statements/searchTransactions.screen');
    }

    /**
     * @returns {SearchTransactionsScreen}
     */
    get accountStatementScreen() {
        return this.getNativeScreen('statements/accountStatement.screen');
    }

    validateSearchPageFields(searchFields) {
        Actions.waitForVisible(this.searchTransactionsScreen.searchHeader);
        for (let iter = 0; iter < searchFields.length; iter++) {
            Actions.swipeUntilAnyVisible(SwipeAction.UP, this.searchTransactionsScreen[searchFields[iter]
                .fieldsInSearchPage]);
        }
    }

    enterKeywordInSearchBox(keyword) {
        Actions.setImmediateValueAndTapOut(this.searchTransactionsScreen.searchDialogBox, keyword);
    }

    validateClearOption() {
        Actions.isVisible(this.searchTransactionsScreen.clearSearch).should.equal(true);
    }

    tapOnSearchBox() {
        Actions.click(this.searchTransactionsScreen.searchDialogBox);
    }

    clearSearchResults() {
        Actions.click(this.searchTransactionsScreen.clearSearch);
    }

    validateSearchFurtherBack() {
        Actions.isVisible(this.searchTransactionsScreen.searchFurtherBack).should.equal(true);
    }

    clickSearchFurtherBack() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.searchTransactionsScreen.searchFurtherBack);
        Actions.click(this.searchTransactionsScreen.searchFurtherBack);
    }

    validateSearchResultsCleared() {
        Actions.isVisible(this.searchTransactionsScreen.transactionListEmpty).should.equal(true);
        Actions.isVisible(this.searchTransactionsScreen.searchInfo).should.equal(false);
        Actions.isVisible(this.searchTransactionsScreen.searchFurtherBack).should.equal(false);
    }

    validateSearchWordIsCleared(keyword) {
        Actions.getAttributeText(this.searchTransactionsScreen.searchDialogBox, 'text').should.not.equal(keyword);
    }

    validateNoMoreTransactionsMessage() {
        Actions.isVisible(this.searchTransactionsScreen.noMoreTransactionsMessage).should.equal(true);
    }

    validateNoSearchFurtherBack() {
        Actions.swipePage(SwipeAction.UP);
        Actions.isVisible(this.searchTransactionsScreen.searchFurtherBack).should.equal(false);
    }

    validateNoTransactionsToSearch() {
        Actions.isVisible(this.searchTransactionsScreen.noTransactionsToSearch).should.equal(true);
    }

    validateNoSearchResults() {
        Actions.isVisible(this.searchTransactionsScreen.transactionListEmpty).should.equal(true);
    }

    validateSearchResults(keyword) {
        if (Number(keyword)) {
            Actions.getAttributeText(this.accountStatementScreen.firstTranscationAmount, 'text').toString().should.contains(keyword);
        } else {
            Actions.getAttributeText(this.accountStatementScreen.firstTranscationDescription, 'text').toString().toLowerCase().should.contains(keyword);
        }
    }

    swipeDownInSearchPage() {
        Actions.swipeOnElement(SwipeAction.UP, this.accountStatementScreen.searchResultsTransactionsList);
    }

    searchAllTransactions() {
        while (Actions.isVisible(this.searchTransactionsScreen.searchFurtherBack)) {
            Actions.swipeUntilVisible(SwipeAction.UP, this.searchTransactionsScreen.searchFurtherBack);
            Actions.click(this.searchTransactionsScreen.searchFurtherBack);
        }
        this.validateNoSearchFurtherBack();
    }

    validateZeroResultsInDateRangeMessage() {
        Actions.waitForVisible(this.searchTransactionsScreen.ZeroResultsDateRangeMessage);
        Actions.isVisible(this.searchTransactionsScreen.ZeroResultsDateRangeMessage).should.equal(true);
    }

    validateSearchPageDisplayed() {
        Actions.isVisible(this.searchTransactionsScreen.searchHeader).should.equal(true);
        Actions.isVisible(this.accountStatementScreen.firstTranscationDescription).should.equal(true);
    }
}

module.exports = SearchTransactionsPage;
