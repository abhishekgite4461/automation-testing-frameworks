require('chai')
    .should();
const assert = require('assert');
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');
const AccountHelper = require('../../utils/account.helper');
const HomePage = require('../home.page');

const homePage = new HomePage();
const LOCALE = 'en-uk';
let accountNumber;
let accountBalance;
let balanceAfterPending;
let overDraftLimit;
const currentAccount = ['classic', 'reward current account', 'islamic current account', 'current account'];

class AccountStatementPage extends AuthPage {
    /**
     * @returns {AccountStatementScreen}
     */
    get accountStatementScreen() {
        return this.getNativeScreen('statements/accountStatement.screen');
    }

    /**
     * @returns {ActionMenuScreen}
     */
    get actionMenuScreen() {
        return this.getNativeScreen('actionMenu.screen');
    }

    /**
     * @returns {HomeScreen}
     */
    get homeScreen() {
        return this.getNativeScreen('home.screen');
    }

    /**
     * @returns {ViewTransactionDetailScreen}
     */
    get viewTransactionDetailScreen() {
        return this.getNativeScreen('viewTransactionDetail.screen');
    }

    static getCurrentMonth() {
        const date = new Date();
        return AccountStatementPage.getMonthName(date);
    }

    static getPreviousMonth() {
        const date = new Date();
        date.setMonth(date.getMonth(), 0);
        return AccountStatementPage.getMonthName(date);
    }

    static getNextMonth() {
        const date = new Date();
        return date.setMonth(date.getMonth() + 1) % 12;
    }

    static getCurrentYear() {
        const date = new Date();
        return (date.getFullYear());
    }

    static getYear(year) {
        const date = new Date();
        return (date.getFullYear() + year);
    }

    static getMonthName(date, locale = LOCALE) {
        return date.toLocaleString(locale, {
            month: 'long'
        });
    }

    verifyOverdraftLimitProgressBar(type) {
        if (type === 'should') {
            Actions.isVisible(this.accountStatementScreen.overdraftLimitProgressBar)
                .should
                .equal(true);
        } else {
            Actions.isVisible(this.accountStatementScreen.overdraftLimitProgressBar)
                .should
                .equal(false);
        }
    }

    selectFirstTransaction() {
        Actions.waitAndClick(this.accountStatementScreen.firstStatement);
    }

    verifyFirstTransactionVisible() {
        Actions.isVisible(this.accountStatementScreen.firstStatement)
            .should
            .equal(true);
    }

    verifyCreditCardFirstTransactionVisible() {
        Actions.swipeUntilAnyVisible(SwipeAction.RIGHT,
            this.accountStatementScreen.creditCardFirstStatement);
        Actions.isVisible(this.accountStatementScreen.creditCardFirstStatement)
            .should
            .equal(true);
    }

    clickPendingTransactions() {
        Actions.reportTimer('Pending_Transactions', 'pending transactions');
        Actions.waitAndClick(this.accountStatementScreen.pendingTransactionsAccordionOpenCloseIcon);
        Actions.waitForVisible(this.accountStatementScreen.pendingLabel);
    }

    swipeToPreviousMonthTransaction() {
        Actions.swipeUntilAnyVisible(SwipeAction.RIGHT,
            this.accountStatementScreen.statementsDescription);
    }

    selectPendingTransactionTab() {
        Actions.waitAndClick(this.accountStatementScreen.pendingTransactionTab);
    }

    verifyChequeTransactions() {
        Actions.swipeUntilAnyVisible(SwipeAction.UP,
            this.accountStatementScreen.chequeTransaction);
        Actions.isAnyVisible(this.accountStatementScreen.chequeTransaction)
            .should
            .equal(true);
    }

    selectPendingTransaction() {
        Actions.click(this.accountStatementScreen.clickablePendingTransaction);
    }

    viewMonthTab() {
        const previousMonth = AccountStatementPage.getCurrentMonth();
        Actions.waitForVisible(this.accountStatementScreen.monthFormat(previousMonth));
    }

    validateNoCalendarMonth() {
        const futureMonth = AccountStatementPage.getNextMonth();
        Actions.isVisible(this.accountStatementScreen.monthFormat(futureMonth))
            .should
            .equal(false);
    }

    swipeToPreviousMonth(swipes = 1) {
        Actions.iOSObjectTreeOptimizationStart();
        this.swipeTransactionList('previous', swipes);
        Actions.iOSObjectTreeOptimizationStop();
    }

    swipeTransactionList(swipeDirection, numberOfMonths) {
        Actions.iOSObjectTreeOptimizationStart();
        for (let i = 1; i <= numberOfMonths; i++) {
            if (swipeDirection === 'previous') {
                if (DeviceHelper.isAndroid()) {
                    Actions.swipeByPercentage(10, 70, 90, 70, 500,
                        this.accountStatementScreen.statementsDescription(i));
                } else {
                    Actions.swipeByPercentage(20, 70, 80, 70, 100);
                    Actions.isVisible(this.accountStatementScreen.statementsDescription(i + 1));
                }
            } else if (swipeDirection === 'future') {
                if (DeviceHelper.isAndroid()) {
                    Actions.swipeOnElement(SwipeAction.LEFT, this.accountStatementScreen.statementsDescription(i));
                } else {
                    Actions.swipeByPercentage(80, 80, 20, 80, 100);
                    Actions.isVisible(this.accountStatementScreen.statementsDescription(i));
                }
            } else {
                throw new Error(`Unknown swipe direction: ${swipeDirection}`);
            }
        }

        Actions.waitForVisible(this.accountStatementScreen.statementsDescription(swipeDirection === 'previous'
            ? Number(numberOfMonths) + 1 : Number(numberOfMonths)));
        Actions.iOSObjectTreeOptimizationStop();
    }

    clickBackButton() {
        Actions.click(this.accountStatementScreen.backButton);
    }

    selectActionMenu() {
        Actions.click(this.accountStatementScreen.actionMenuCurrentAccount);
    }

    viewRecentsTab() {
        Actions.iOSObjectTreeOptimizationStart();
        Actions.waitForVisible(this.accountStatementScreen.recentsTab);
        Actions.iOSObjectTreeOptimizationStop();
    }

    verifyTransactionSinceMessage() {
        Actions.iOSObjectTreeOptimizationStart();
        Actions.waitForVisible(this.accountStatementScreen.transactionsSinceMessage);
        Actions.iOSObjectTreeOptimizationStop();
    }

    validateCurrentMonthName() {
        Actions.waitForVisible(this.accountStatementScreen.currentMonthIndicator);
    }

    shouldNotSwipeBetweenAccounts() {
        Actions.waitForVisible(this.accountStatementScreen.accountTile);
        Actions.swipeByPercentage(80, 25, 20, 25, 100);
        Actions.isVisible(this.accountStatementScreen.accountTile)
            .should
            .equal(true);
    }

    validateStatementDate() {
        Actions.waitForVisible(this.accountStatementScreen.firstTranscationDate);
        Actions.isVisible(this.accountStatementScreen.firstTranscationDate)
            .should
            .equal(true);
    }

    // TODO : Need to remove the contains once we get the unique id for android
    validateStatementDescription() {
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.accountStatementScreen.firstTranscationDescription)
                .should
                .contains(true);
        } else {
            Actions.isVisible(this.accountStatementScreen.firstTranscationDescription)
                .should
                .equal(true);
        }
    }

    // TODO : Need to remove the contains once we get the unique id for android
    validateStatementRunningBalance() {
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.accountStatementScreen.firstTransactionRunningBalance)
                .should
                .contains(true);
        } else {
            Actions.isVisible(this.accountStatementScreen.firstTransactionRunningBalance)
                .should
                .equal(true);
        }
    }

    // TODO : Need to remove the contains once we get the unique id for android
    validateStatementAmount() {
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.accountStatementScreen.firstTranscationAmount)
                .should
                .contains(true);
        } else {
            Actions.isVisible(this.accountStatementScreen.firstTranscationAmount)
                .should
                .equal(true);
        }
    }

    swipeToSeeNextSetOfTransaction(type) {
        let visible = false;
        let count = 0;
        while (!visible && count++ <= 10) {
            if (type === 'current') {
                Actions.swipeOnElement(
                    SwipeAction.UP,
                    this.accountStatementScreen.accountDetailContainer
                );
                visible = Actions.isVisible(this.accountStatementScreen.totalAmountField)
                    .should
                    .equal(true);
            } else if (type === 'saving') {
                Actions.swipeOnElement(
                    SwipeAction.UP,
                    this.accountStatementScreen.accountDetailContainer
                );
                visible = Actions.isVisible(this.accountStatementScreen.savingEndOfTransactionMessage)
                    .should
                    .equal(true);
            } else if (type === 'personal Loan') {
                Actions.swipeOnElement(
                    SwipeAction.UP,
                    this.accountStatementScreen.accountDetailContainer
                );
                visible = Actions.isVisible(this.accountStatementScreen.loanEndOfTransactionMessage)
                    .should
                    .equal(true);
            } else {
                throw new Error('Type does not match');
            }
        }
        if (count === 10 && !visible) {
            throw new Error(`Unable to locate element ${type} in ${count} attempts`);
        }
    }

    verifyEndOfTransactionMessage() {
        let visible = false;
        let count = 0;
        while (!visible && count++ <= 10) {
            Actions.swipeOnElement(
                SwipeAction.UP,
                this.accountStatementScreen.accountDetailContainer
            );
            visible = Actions.isVisible(this.accountStatementScreen.endOfTranscationMessage)
                .should
                .equal(true);
        }
        if (count === 10 && !visible) {
            throw new Error(`Unable to locate the element after ${count} attempts`);
        }
    }

    verifyBilledInfoPanel() {
        Actions.iOSObjectTreeOptimizationStart();
        if (DeviceHelper.isAndroid()) {
            Actions.isExisting(this.accountStatementScreen.statementBalanceDescription)
                .should
                .equal(true);
            AccountHelper.isValidStatementAmount(Actions.getText(this.accountStatementScreen.statementBalance));
            Actions.isExisting(this.accountStatementScreen.minimumPaymentDescription)
                .should
                .equal(true);
            AccountHelper.isValidStatementAmount(Actions.getText(this.accountStatementScreen.minimumPayment));
            Actions.isExisting(this.accountStatementScreen.dueDateDescription)
                .should
                .equal(true);
            AccountHelper.isValidStatementDate(Actions.getText(this.accountStatementScreen.dueDate));
            Actions.isExisting(this.accountStatementScreen.statementDateDescription)
                .should
                .equal(true);
            AccountHelper.isValidStatementDate(Actions.getText(this.accountStatementScreen.statementDate));
        } else {
            Actions.isExisting(this.accountStatementScreen.statementBalance)
                .should
                .equal(true);
            Actions.isExisting(this.accountStatementScreen.minimumPayment)
                .should
                .equal(true);
            Actions.isExisting(this.accountStatementScreen.dueDate)
                .should
                .equal(true);
            Actions.isExisting(this.accountStatementScreen.statementDate)
                .should
                .equal(true);
        }
        Actions.iOSObjectTreeOptimizationStop();
    }

    validateTransactionDetails(transactionDetail) {
        for (let i = 0; i < transactionDetail.length; i++) {
            const individualtransactionDetail = this.accountStatementScreen[transactionDetail[i].detail];
            Actions.swipeUntilVisible(SwipeAction.UP, individualtransactionDetail);
        }
    }

    verifyPendingTransactionType(type) {
        Actions.isExisting(this.accountStatementScreen.pendingTransactionType(type))
            .should
            .equal(true);
    }

    shouldSeeNoTransactionsMessage() {
        Actions.iOSObjectTreeOptimizationStart();
        Actions.isVisible(this.accountStatementScreen.creditCardNoTransactionMessage)
            .should
            .equal(true);
        Actions.iOSObjectTreeOptimizationStop();
    }

    verifyTransactionsHeaderIsNotPresent() {
        Actions.isExisting(this.accountStatementScreen.transactionsHeader)
            .should
            .equal(false);
    }

    selectActionMenuOnAccountStatementScreen() {
        Actions.iOSObjectTreeOptimizationStart();
        Actions.waitAndClick(this.accountStatementScreen.actionMenu);
        Actions.iOSObjectTreeOptimizationStop();
    }

    selectPostedTransaction(transactionType) {
        Actions.iOSObjectTreeOptimizationStart();
        const transactionTypeName = transactionType.replace(/ /g, ''); // remove whitespaces
        if (transactionTypeName === 'exposedPendingCheque' || transactionTypeName === 'exposedPendingDirectDebit') {
            Actions.swipeUntilVisible(SwipeAction.DOWN, this.accountStatementScreen.chequeNoteText);
        }
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen[`${transactionTypeName}TransactionType`],
            80, 80, 50, 50, 500, 30);
        Actions.click(this.accountStatementScreen[`${transactionTypeName}TransactionType`]);
        Actions.iOSObjectTreeOptimizationStop();
    }

    validateNoTransactionMessage(noTransaction, tab) {
        if (tab === 'allTab') {
            Actions.swipeOnElement(SwipeAction.UP, this.accountStatementScreen.accountDetailContainer);
            Actions.isVisible(this.accountStatementScreen.currentNoTransactionMessage)
                .should
                .equal(true);
            Actions.isVisible(this.accountStatementScreen.noCurrentInOutsTransactionMessage)
                .should
                .equal(false);
        } else {
            Actions.swipeUntilVisible(
                SwipeAction.UP,
                this.accountStatementScreen[`${noTransaction}NoTransactionMessage`], 0, false, 3
            );
        }
    }

    validateEndOfTransactionMessage(endOfTransaction, tab) {
        if (tab === 'allTab') {
            Actions.swipeOnElement(SwipeAction.UP, this.accountStatementScreen.accountDetailContainer);
            Actions.isVisible(this.accountStatementScreen.currentEndOfTransactionMessage)
                .should
                .equal(false);
            Actions.isVisible(this.accountStatementScreen.noCurrentInOutsTransactionMessage)
                .should
                .equal(false);
        } else {
            Actions.swipeByPercentUntilVisible(this.accountStatementScreen[`${endOfTransaction}TransactionMessage`],
                80, 80, 80, 20, 100, 25);
        }
    }

    verifyPendingTransactionsAccordion(state) {
        if (state === 'expanded') {
            Actions.waitForVisible(this.accountStatementScreen.pendingTransactionPanelOpen);
            Actions.swipeByPercentUntilVisible(this.accountStatementScreen.pendingLabel);
        } else if (state === 'minimised') {
            Actions.isVisible(this.accountStatementScreen.pendingLabel)
                .should
                .equal(false);
        } else {
            throw new Error(`${state} is an unrecognized accordion state`);
        }
    }

    verifyPendingTransactionsArePresent() {
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen.pendingTransactionAmount);
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen.pendingTransactionDescription);
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen.pendingLabel);
    }

    verifyPendingTransactionsHeaderIsPresent() {
        Actions.isExisting(this.accountStatementScreen.pendingTransactionTab)
            .should
            .equal(true);
    }

    verifyPendingTransactionsAccordionIsNotVisible() {
        Actions.isExisting(this.accountStatementScreen.pendingTransactionTab)
            .should
            .equal(false);
        Actions.isExisting(this.accountStatementScreen.pendingTransactionsAccordionOpenCloseIcon)
            .should
            .equal(false);
    }

    verifyTransactionsArePresent() {
        Actions.isAnyVisible(this.accountStatementScreen.firstTranscationDate)
            .should
            .equal(true);
        Actions.isAnyVisible(this.accountStatementScreen.firstTranscationDescription)
            .should
            .equal(true);
        Actions.isAnyVisible(this.accountStatementScreen.firstTranscationAmount)
            .should
            .equal(true);
    }

    validateStatementsListFields(pageFields) {
        let pageField;
        Actions.iOSObjectTreeOptimizationStart();
        for (let iter = 0; iter < pageFields.length; iter++) {
            pageField = pageFields[iter]
                .fieldsInStatementsList;
            if (pageField === 'noChevron' || pageField === 'chevron') {
                Actions.isVisible(this.accountStatementScreen.chevron)
                    .should
                    .equal(pageField === 'chevron');
            } else if (DeviceHelper.isSmallScreenSize()) {
                Actions.swipeUntilAnyVisible(SwipeAction.UP, this.accountStatementScreen[pageField]);
            } else {
                Actions.isVisible(this.accountStatementScreen[pageField])
                    .should
                    .equal(true);
            }
        }
        Actions.iOSObjectTreeOptimizationStop();
    }

    swipeToSeeNextSetOfTransactionForPreviousMonth(type) {
        let visible = false;
        while (!visible) {
            if (type === 'current') {
                Actions.swipeByPercentUntilVisible(
                    this.accountStatementScreen.totalAmountField
                );
                visible = Actions.isVisible(this.accountStatementScreen.totalAmountField)
                    .should
                    .equal(true);
            } else if (type === 'saving') {
                Actions.swipeByPercentUntilVisible(
                    this.accountStatementScreen.savingEndOfTransactionMessage
                );
                visible = Actions.isVisible(this.accountStatementScreen.savingEndOfTransactionMessage)
                    .should
                    .equal(true);
            } else if (type === 'personal Loan') {
                Actions.swipeByPercentUntilVisible(
                    this.accountStatementScreen.loanEndOfTransactionMessage, 80, 80, 20, 20, 100, 10
                );
                visible = Actions.isVisible(this.accountStatementScreen.loanEndOfTransactionMessage)
                    .should
                    .equal(true);
            } else {
                throw new Error('Type does not match');
            }
        }
    }

    validateNoPendingPaymentsMessage() {
        Actions.isVisible(this.accountStatementScreen.noPendingPaymentsMessage)
            .should
            .equal(true);
    }

    verifyStatementLimitMessage() {
        Actions.waitForVisible(this.accountStatementScreen.onlineCreditCartStatementLimitText);
    }

    verifyAboutPendingTransactionsHeaderIsPresent() {
        Actions.isVisible(this.accountStatementScreen.pendingTransactionAboutIcon)
            .should
            .equal(true);
        Actions.isVisible(this.accountStatementScreen.aboutPendingTransactionsTitle)
            .should
            .equal(true);
    }

    clickOnAboutPendingTransactionsHeader() {
        Actions.click(this.accountStatementScreen.aboutPendingTransactionsTitle);
    }

    verifyLoanFieldsInActionMenu(loanFields) {
        for (let iter = 0; iter < loanFields.length; iter++) {
            Actions.isVisible(this.actionMenuScreen[loanFields[iter]
                .fieldsInLoansActionMenu])
                .should
                .equal(true);
        }
    }

    validateCurrentYear() {
        Actions.waitForVisible(this.accountStatementScreen.currentYearIndicator);
        Actions.isVisible(this.accountStatementScreen.currentYearIndicator)
            .should
            .equal(true);
    }

    verifyLoanFieldsStatementsPage(loanFields) {
        for (let iter = 0; iter < loanFields.length; iter++) {
            Actions.swipeByPercentUntilVisible(this.accountStatementScreen[loanFields[iter]
                .fieldsForLoanAccount], 80, 80, 80, 60, 500);
        }
    }

    clickPlusAccordionLoans() {
        Actions.waitForVisible(this.accountStatementScreen.plusAccordion);
        Actions.isVisible(this.accountStatementScreen.plusAccordion)
            .should
            .equal(true);
        Actions.click(this.accountStatementScreen.plusAccordion);
    }

    clickMinusAccordionLoans() {
        Actions.isVisible(this.accountStatementScreen.minusAccordion)
            .should
            .equal(true);
        Actions.click(this.accountStatementScreen.minusAccordion);
    }

    validateCbsLoanMessage() {
        Actions.isVisible(this.accountStatementScreen.cbsLoanMessage)
            .should
            .equal(true);
    }

    noActionMenuCbsLoan() {
        Actions.isVisible(this.homeScreen.actionMenu)
            .should
            .equal(false);
    }

    swipeToPreviousYear() {
        this.swipeTransactionListLoan('previous', 1);
    }

    validateNextYearNotDisplayed(Year, boolean) {
        const yearFormat = AccountStatementPage.getYear(Year);
        Actions.isVisible(this.accountStatementScreen.yearFormat(yearFormat))
            .should
            .equal(boolean);
    }

    validateTheFutureYearToRight(Year, boolean) {
        const currentyear = AccountStatementPage.getYear(Year);
        Actions.isVisible(this.accountStatementScreen.yearFormat(currentyear))
            .should
            .equal(boolean);
    }

    validateThePreviousYearsloanStarted() {
        this.swipeTransactionListLoan('previous', 6);
    }

    validateTheYearTab(Year, boolean) {
        const currentYear = AccountStatementPage.getYear(Year);
        Actions.isVisible(this.accountStatementScreen.yearFormat(currentYear))
            .should
            .equal(boolean);
    }

    validateThePreviousYearTab(Year, boolean) {
        const previousYear = AccountStatementPage.getYear(Year);
        Actions.isVisible(this.accountStatementScreen.yearFormat(previousYear))
            .should
            .equal(boolean);
    }

    swipeTransactionListLoan(swipeDirection, numberOfYears) {
        for (let i = 1; i <= numberOfYears; i++) {
            if (swipeDirection === 'previous') {
                if (DeviceHelper.isAndroid()) {
                    Actions.swipeOnElement(SwipeAction.RIGHT, this.accountStatementScreen.statementsDescription(i));
                } else {
                    Actions.swipeByPercentUntilVisible(this.accountStatementScreen.statementsDescription(i + 1),
                        20, 70, 80, 70);
                }
            } else if (swipeDirection === 'future') {
                Actions.swipeByPercentUntilVisible(this.accountStatementScreen.statementsDescription(i - 1),
                    80, 70, 20, 70);
            } else {
                throw new Error(`Unknown swipe direction: ${swipeDirection}`);
            }
        }
    }

    viewPreviousYearTab() {
        if (DeviceHelper.isAndroid()) {
            const previousYear = AccountStatementPage.getCurrentYear() - 1;
            Actions.isVisible(this.accountStatementScreen.yearFormat(previousYear))
                .should
                .equal(true);
        } else {
            Actions.waitForVisible(this.accountStatementScreen.previousMonthStatementHolder);
            Actions.isAnyVisible(this.accountStatementScreen.previousMonthStatementHolder)
                .should
                .equal(true);
        }
    }

    viewPreviousMonthTab() {
        if (DeviceHelper.isAndroid()) {
            const previousmonth = AccountStatementPage.getPreviousMonth();
            const previousMonthValue = previousmonth.substring(0, 3);
            const actualPreviousMonth = this.accountStatementScreen.monthFormat(previousmonth);
            const actualPreviousMonthSubString = actualPreviousMonth.substring(33, 36);
            if (previousMonthValue === actualPreviousMonthSubString) {
                Actions.isAnyVisible(this.accountStatementScreen.statementsDescription(0))
                    .should
                    .equal(true);
            } else {
                throw new Error('Not in previous month');
            }
        } else {
            Actions.isAnyVisible(this.accountStatementScreen.previousMonthStatementHolder)
                .should
                .equal(true);
        }
    }

    validateStatementsPage(statementsPage) {
        Actions.iOSObjectTreeOptimizationStart();
        Actions.waitForVisible(this.accountStatementScreen[`${statementsPage}StatementsPage`]);
        Actions.isVisible(this.accountStatementScreen[`${statementsPage}StatementsPage`])
            .should
            .equal(true);
        Actions.iOSObjectTreeOptimizationStop();
    }

    validateAmountDeducted(amount) {
        const amountText = (amount.replace(/([-+ ])/g, ''));
        const amountDesc = (parseFloat(amount.replace(/([£ ])/g, '')) > 0 ? 'Credit of ' : 'Debit of ')
            + amountText;
        Actions.isAnyVisible(this.accountStatementScreen.deductedTransactionAmount(amountDesc))
            .should
            .equal(true);
    }

    swipeToCurrentMonth() {
        Actions.waitForVisible(this.viewTransactionDetailScreen.dueSoon);
        Actions.swipeOnElement(SwipeAction.LEFT,
            this.accountStatementScreen.dueSoonTransactionList);
    }

    navigateBackToPreviousAccount(accountName) {
        Actions.swipeByPercentUntilVisible(this.homeScreen.accountNameLabel(accountName),
            20, 30, 80, 30, 100, 10);
    }

    verifyMonthTabWithoutYear() {
        const previousMonth = AccountStatementPage.getPreviousMonth();
        Actions.swipeUntilVisible(SwipeAction.RIGHT, this.accountStatementScreen.monthFormat(previousMonth));
        Actions.isVisible(this.accountStatementScreen.monthFormat(previousMonth))
            .should
            .equal(true);
    }

    validateMortgageTab(mortgageTab) {
        if (mortgageTab === 'mortgageSummaryTab') {
            Actions.isVisible(this.accountStatementScreen.mortgageSummaryTab)
                .should
                .equal(true);
        } else if (mortgageTab === 'subAccountDetailsTab') {
            Actions.isVisible(this.accountStatementScreen.subAccountDetailsTab)
                .should
                .equal(true);
        } else {
            throw new Error('mortgage tab does not match');
        }
    }

    validateSubAccountListFields(pageFields) {
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.swipeUntilVisible(
                SwipeAction.UP,
                this.accountStatementScreen[pageFields[iter].fieldsInSubAccountsList],
                0,
                false,
                2,
                3000,
                35000
            );
        }
    }

    selectSubAccountTab() {
        if (DeviceHelper.isAndroid()) {
            Actions.waitForVisible(this.accountStatementScreen.mortgageSubAccountTab);
            Actions.isVisible(this.accountStatementScreen.mortgageSubAccountTab)
                .should
                .equal(true);
            Actions.click(this.accountStatementScreen.mortgageSubAccountTab);
        } else {
            Actions.swipeByPercentage(80, 30, 20, 30, 1000,
                this.accountStatementScreen.accountDetailContainerPreviousMonth);
            Actions.waitForVisible(this.accountStatementScreen.mortgageSubAccountTab);
        }
    }

    validateMortgageDetailsFields(option, pageFields) {
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.swipeByPercentUntilVisible(
                this.accountStatementScreen[pageFields[iter].fieldsInMortgageDetails], 80, 50,
                20, 20, 100, 10
            );
        }
    }

    clickMortgageAccordion() {
        Actions.waitForVisible(this.accountStatementScreen.mortgageAccordion);
        Actions.isVisible(this.accountStatementScreen.mortgageAccordion)
            .should
            .equal(true);
        Actions.click(this.accountStatementScreen.mortgageAccordion);
    }

    tapOnSubAccountFromList() {
        Actions.waitForVisible(this.accountStatementScreen.subAccount);
        Actions.isVisible(this.accountStatementScreen.subAccount)
            .should
            .equal(true);
        Actions.click(this.accountStatementScreen.subAccount);
    }

    validateDetailsTab() {
        Actions.isVisible(this.accountStatementScreen.subAccountDetailsTab)
            .should
            .equal(true);
    }

    chooseInfoLink(type, infoLink) {
        if (type === 'mortgageSubAccountTab') {
            if (infoLink === 'currentBalanceInfo') {
                Actions.swipeByPercentUntilVisible(!DeviceHelper.isIOS()
                    ? this.accountStatementScreen.currentBalanceInfoLink
                    : this.accountStatementScreen.monthlyPaymentsInfoLink, 80, 80, 20, 20, 500, 5);
                Actions.click(!DeviceHelper.isIOS() ? this.accountStatementScreen.currentBalanceInfoLink
                    : this.accountStatementScreen.monthlyPaymentsInfoLink);
            } else if (infoLink === 'monthlyPaymentsInfo') {
                Actions.swipeByPercentUntilVisible(!DeviceHelper.isIOS()
                    ? this.accountStatementScreen.monthlyPaymentsInfoLink
                    : this.accountStatementScreen.currentBalanceInfoLink, 80, 80, 20, 20, 500, 5);
                Actions.click(!DeviceHelper.isIOS() ? this.accountStatementScreen.monthlyPaymentsInfoLink
                    : this.accountStatementScreen.currentBalanceInfoLink);
            } else {
                throw new Error('Info Link does not match');
            }
        } else {
            Actions.swipeByPercentUntilVisible(this.accountStatementScreen[`${infoLink}Link`], 80, 80,
                20, 20, 500, 5);
            Actions.click(this.accountStatementScreen[`${infoLink}Link`]);
        }
    }

    validateInfoLayover(infoLink) {
        if (infoLink === 'currentBalanceInfo') {
            Actions.isVisible(this.accountStatementScreen.currentBalanceInfoPage)
                .should
                .equal(true);
        } else if (infoLink === 'monthlyPaymentInfo') {
            Actions.isVisible(this.accountStatementScreen.monthlyPaymentInfoPage)
                .should
                .equal(true);
        } else {
            throw new Error('Info Link does not match');
        }
    }

    validateMortgageAccordionState() {
        Actions.isVisible(this.accountStatementScreen.currentBalanceInfoLink)
            .should
            .equal(true);
    }

    validateOffSetRepossessedAccountMessage() {
        Actions.isVisible(this.accountStatementScreen.offSetRepossessedAccountMessage)
            .should
            .equal(true);
    }

    clickCloseButtonInInfoPage() {
        Actions.swipeByPercentUntilVisible(
            this.accountStatementScreen.closeInfoPage, 80, 80,
            20, 20, 100, 10
        );
        Actions.click(this.accountStatementScreen.closeInfoPage);
    }

    clickMortgageAccordionForSummaryAndSubAccount(type) {
        if (type === 'mortgageSummaryTab') {
            this.clickMortgageAccordion();
        } else if (type === 'mortgageSubAccountTab') {
            this.selectSubAccountTab();
            this.tapOnSubAccountFromList();
            this.clickMortgageAccordion();
        } else {
            throw new Error('type does not match');
        }
    }

    clickCallUs() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.accountStatementScreen.callUs);
        Actions.click(this.accountStatementScreen.callUs);
    }

    validateMortgageSummaryTab() {
        Actions.waitForVisible(this.accountStatementScreen.mortgageSummaryTab);
    }

    selectPolicyLink(policyLinkText) {
        const policyLink = policyLinkText.replace(/ /g, ''); // remove whitespaces
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen[`${policyLink}Link`]);
        Actions.click(this.accountStatementScreen[`${policyLink}Link`]);
        if (policyLink === 'whatsIncludedInPolicy') {
            Actions.waitForVisible(this.accountStatementScreen.coreCoverName);
        } else if (policyLink === 'optionalCoverYouCouldAdd') {
            Actions.waitForVisible(this.accountStatementScreen.optionalCoverName);
        } else if (policyLink === 'costAndPaymentDetails') {
            Actions.waitForVisible(this.accountStatementScreen.homeInsuranceTitle);
        }
    }

    verifyPendingTransactionsWhenLayoverClosed(type) {
        if (type === 'pending Direct Debit') {
            Actions.waitForVisible(this.accountStatementScreen.pendingDirectDebitTransactionType);
            Actions.isVisible(this.accountStatementScreen.pendingDirectDebitTransactionType)
                .should
                .equal(true);
        } else if (type === 'pending Credit Card') {
            Actions.waitForVisible(this.accountStatementScreen.pendingCreditCardTransactionType);
            Actions.isVisible(this.accountStatementScreen.pendingCreditCardTransactionType)
                .should
                .equal(true);
        } else {
            throw new Error('Pending Transactions are not displayed when overlay is closed');
        }
    }

    checkPolicyCoverageFieldsVisibility(type, policyFields) {
        for (let iter = 0; iter < policyFields.length; iter++) {
            if (DeviceHelper.isAndroid()) {
                Actions.swipeByPercentUntilVisible(this.accountStatementScreen[policyFields[iter].fieldsInDetails],
                    80, 80, 20, 20, 300, 5, this.accountStatementScreen.statementLayout);
            } else {
                Actions.swipeByPercentUntilVisible(this.accountStatementScreen[policyFields[iter].fieldsInDetails],
                    80, 80, 20, 20, 300, 5);
            }
        }
    }

    verifyNoSummaryAndSubaccountTabs() {
        Actions.isVisible(this.accountStatementScreen.mortgageSummaryTab)
            .should
            .equal(false);
        Actions.isVisible(this.accountStatementScreen.mortgageSubAccountTab)
            .should
            .equal(false);
    }

    validateOffsetRepossessed(message) {
        Actions.isVisible(this.accountStatementScreen[`${message}Warning`])
            .should
            .equal(true);
    }

    verifyWarningAlert() {
        Actions.isVisible(this.accountStatementScreen.warningAlert)
            .should
            .equal(true);
    }

    clickWarningAlert() {
        Actions.isVisible(this.accountStatementScreen.warningAlert)
            .should
            .equal(true);
        Actions.click(this.accountStatementScreen.warningAlert);
    }

    validateWarningMessage(message) {
        Actions.isVisible(this.accountStatementScreen.alertOverlay)
            .should
            .equal(true);
        Actions.isVisible(this.accountStatementScreen[`${message}WarningAlert`])
            .should
            .equal(true);
    }

    clickCloseButtonInAlertPage() {
        Actions.click(this.accountStatementScreen.closeInfoPage);
    }

    verifyInAndOutTotalWithoutBar() {
        Actions.isVisible(this.accountStatementScreen.inAndOutWithoutBar)
            .should
            .equal(true);
    }

    navigateToCurrentAndPreviousMonth(month, type) {
        if (month.includes('current') && type.includes('current')) {
            Actions.reportTimer('Current_Month_Statements', 'Account balance');
            this.swipeToPreviousMonth();
            this.validateCurrentMonthName();
        } else if (month.includes('previous') && type.includes('current')) {
            Actions.reportTimer('Previous_Month_Statements', 'Account balance');
            this.swipeToPreviousMonth(2);
        } else if (month.includes('current') && !type.includes('current')) {
            this.validateCurrentMonthName();
        } else if (month.includes('previous') && !type.includes('current')) {
            this.swipeToPreviousMonth();
        } else if (month === 'allTab') {
            if (DeviceHelper.isBNGA()) {
                Actions.isVisible(this.accountStatementScreen.searchIcon)
                    .should
                    .equal(false);
            }
            this.validateAllTab();
        }
    }

    validateCurrentAndPreviousMonthTransactions(month) {
        if (month === 'current') {
            this.validateCurrentMonthName();
        } else if (month === 'previous') {
            this.viewPreviousMonthTab();
        } else {
            this.validateAllTab();
        }
    }

    validatePolicyDetailsPageLoaded() {
        Actions.isVisible(this.accountStatementScreen.policyNumber)
            .should
            .equal(true);
    }

    validateRecentTransactionFields(pageFields) {
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.swipeByPercentUntilVisible(this.accountStatementScreen[pageFields[iter]
                .fieldsInRecentTransaction], 50, 80, 50, 70);
        }
    }

    validateCreditCardStatementSummary(pageFields) {
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.swipeUntilAnyVisible(SwipeAction.UP, this.accountStatementScreen[pageFields[iter]
                .fieldsInStatementsSummary]);
        }
    }

    validateStatementSummaryFieldsInFilterTransactions(type, pageFields) {
        Actions.waitForVisible(this.accountStatementScreen.clearFilter);
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.swipeUntilAnyVisible(SwipeAction.UP, this.accountStatementScreen[pageFields[iter]
                .fieldsInStatementsSummary]);
        }
    }

    clickClearFilter() {
        Actions.waitAndClick(this.accountStatementScreen.clearFilter);
    }

    validateFixedBondMessage() {
        Actions.isVisible(this.accountStatementScreen.fixedBondMessage)
            .should
            .equal(true);
    }

    validateDormantAccountMessage() {
        Actions.isVisible(this.accountStatementScreen.dormantMessage)
            .should
            .equal(true);
    }

    validateNoTransactions() {
        Actions.isVisible(this.accountStatementScreen.transactionList)
            .should
            .equal(false);
    }

    validateNoTransactionsForCreditCard() {
        Actions.isVisible(this.accountStatementScreen.firstTranscationDate)
            .should
            .equal(false);
    }

    validateNoFilterButton() {
        Actions.isVisible(this.accountStatementScreen.optionToFilterTransactions)
            .should
            .equal(false);
    }

    clickFilterButton() {
        Actions.isVisible(this.accountStatementScreen.optionToFilterTransactions);
        Actions.click(this.accountStatementScreen.optionToFilterTransactions);
    }

    validateFilterOverlayDisplayed() {
        Actions.isVisible(this.accountStatementScreen.filterOverlay)
            .should
            .equal(true);
    }

    validateFilterOverlayFields(pageFields) {
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.swipeUntilAnyVisible(SwipeAction.UP, this.accountStatementScreen[pageFields[iter]
                .filterOptions]);
        }
    }

    closeFilterOverlay() {
        Actions.click(this.accountStatementScreen.cancelButton);
    }

    validateFilterOverlayClosed() {
        Actions.isVisible(this.accountStatementScreen.transactionsSinceMessage)
            .should
            .equal(true);
    }

    validateFilterErrorAccountLevelTransactions() {
        Actions.isVisible(this.accountStatementScreen.filterErrorAccountlevel)
            .should
            .equal(true);
    }

    selectFilterTransactions(typeFilter, month) {
        if (month === 'recent') {
            this.clickFilterButton();
            Actions.waitForVisible(this.accountStatementScreen.filterOverlay);
            if (typeFilter === 'account') {
                Actions.swipeUntilAnyVisible(SwipeAction.UP, this.accountStatementScreen.accountTransactions);
                Actions.click(this.accountStatementScreen.accountTransactions);
            } else if (typeFilter === 'cardEnding') {
                Actions.click(this.accountStatementScreen.cardEnding);
            } else {
                throw new Error('filter type is not found');
            }
        } else if (month === 'previous') {
            this.swipeToPreviousMonth();
            this.clickFilterButton();
            Actions.waitForVisible(this.accountStatementScreen.filterOverlay);
            if (typeFilter === 'account') {
                Actions.swipeUntilAnyVisible(SwipeAction.UP, this.accountStatementScreen.accountTransactions);
                Actions.click(this.accountStatementScreen.accountTransactions);
            } else if (typeFilter === 'cardEnding') {
                Actions.click(this.accountStatementScreen.cardEnding);
            } else {
                throw new Error('filter type is not found');
            }
        } else {
            throw new Error('month is not found');
        }
    }

    validateFilterTransactions(typeFilter) {
        for (let i = 0; i < 2; i++) {
            if (typeFilter === 'account') {
                Actions.isVisible(this.accountStatementScreen.cardEndingDescription)
                    .should
                    .equal(false);
                Actions.isVisible(this.accountStatementScreen.cardEndingTransactionTotal)
                    .should
                    .equal(false);
            } else if (typeFilter === 'cardEnding') {
                Actions.swipePage(SwipeAction.UP);
                Actions.isVisible(this.accountStatementScreen.accountTransactionDescription)
                    .should
                    .equal(false);
            } else {
                throw new Error('Transactions not filtered');
            }
        }
    }

    validateNotAbleToNavigateBetweenMonthsAndAccounts() {
        if (DeviceHelper.isAndroid()) {
            Actions.swipeOnElement(SwipeAction.RIGHT, this.accountStatementScreen.statementsSection);
        } else {
            Actions.swipeByPercentage(20, 70, 80, 70, 100,
                this.accountStatementScreen.statementsSection);
        }
    }

    validateClearFilter() {
        Actions.waitForVisible(this.accountStatementScreen.optionToFilterTransactions);
        Actions.isVisible(this.accountStatementScreen.typeFilter)
            .should
            .equal(false);
    }

    viewTransactionsHeader() {
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen.transactionsHeader,
            80, 80, 50, 50, 1000, 5);
    }

    verifyFieldsInPendingTransactionAccordion(fieldsToValidate) {
        for (let iter = 0; iter < fieldsToValidate.length; iter++) {
            Actions.swipeByPercentUntilVisible(
                this.accountStatementScreen[fieldsToValidate[iter].fieldsInPendingTransactionAccordion],
                80, 80, 50, 50, 700, 5
            );
        }
    }

    validateNoTransactionsHeader() {
        Actions.isVisible(this.accountStatementScreen.transactionsHeader)
            .should
            .equal(false);
    }

    validateFilterErrorTooManyTransactions() {
        Actions.isVisible(this.accountStatementScreen.filterErrorTooManyTransactions)
            .should
            .equal(true);
    }

    selectAERLink(link) {
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen[`${link}InfoLink`], 80, 80,
            20, 20, 500, 5);
        Actions.click(this.accountStatementScreen[`${link}InfoLink`]);
    }

    validateTreasuryInfoOverlay(type) {
        if (type === 'aer') {
            Actions.isVisible(this.accountStatementScreen.aerInfoPage)
                .should
                .equal(true);
            Actions.isVisible(this.accountStatementScreen.aerInfo)
                .should
                .equal(true);
        } else if (type === 'nominatedAccount') {
            Actions.isVisible(this.accountStatementScreen.nominatedAccountInfoPage)
                .should
                .equal(true);
            Actions.isVisible(this.accountStatementScreen.nominatedAccountInfo)
                .should
                .equal(true);
        } else {
            throw new Error('Info Link does not match');
        }
    }

    treasuryAccountPageDisplayed() {
        Actions.waitForVisible(this.accountStatementScreen.aerInfoLink);
    }

    productNotAvailableMessage() {
        Actions.isVisible(this.accountStatementScreen.productNotAvailable)
            .should
            .equal(true);
    }

    treasuryAccountFieldsValidation(fieldsToValidate) {
        for (let iter = 0; iter < fieldsToValidate.length; iter++) {
            Actions.swipeByPercentUntilVisible(
                this.accountStatementScreen[fieldsToValidate[iter].fieldsInTreasuryAccount],
                80, 80, 50, 50, 700, 5
            );
        }
    }

    displayUpcomingPaymentWebViewPage(option) {
        if (option === 'upcomingPayments') {
            Actions.waitForVisible(this.accountStatementScreen.upcomingPaymentPage);
        } else {
            Actions.waitForVisible(this.accountStatementScreen.spendingInsightsPage);
        }
        Actions.iOSObjectTreeOptimizationStop();
    }

    verifyScottishWidowsStatementTextIsPresent() {
        Actions.waitForVisible(this.accountStatementScreen.scottishWidowsStatementText);
        Actions.isVisible(this.accountStatementScreen.scottishWidowsStatementText)
            .should
            .equal(true);
    }

    shouldSeePensionPolicyNumber() {
        Actions.isVisible(this.accountStatementScreen.scottishWidowsPolicyNumber)
            .should
            .equal(true);
    }

    selectScottishWidowsFindOutMoreButton() {
        Actions.click(this.accountStatementScreen.scottishWidowsFindOUtMoreButton);
    }

    verifyScottishWidowsFirstFAQIsPresent() {
        Actions.waitForVisible(this.accountStatementScreen.scottishWidowsfirstFAQ);
    }

    displayStatementsPage() {
        Actions.isVisible(!DeviceHelper.isIOS() ? this.accountStatementScreen.accountDetailContainer
            : this.viewTransactionDetailScreen.vtdContainer)
            .should
            .equal(true);
    }

    clickPendingTransactionToOpenorClose(state) {
        if (state === 'expanded') {
            Actions.isVisible(this.accountStatementScreen.pendingTransactionPanelOpen)
                .should
                .equal(true);
            Actions.click(this.accountStatementScreen.pendingTransactionPanelOpen);
        } else if (state === 'minimised') {
            Actions.isVisible(this.accountStatementScreen.pendingTransactionPanelClosed)
                .should
                .equal(true);
            Actions.click(this.accountStatementScreen.pendingTransactionPanelClosed);
        } else {
            throw new Error(`${state} is an unrecognized accordion state`);
        }
    }

    validateStatementFields(field, type) {
        if (type === 'CHARGECARD') {
            Actions.isVisible(this.accountStatementScreen.directDebitAmountDescription)
                .should
                .equal(true);
        } else {
            Actions.isVisible(this.accountStatementScreen.directDebitAmountDescription)
                .should
                .equal(false);
        }
    }

    validatePendingTransactionErrorMessages(errorMessage) {
        Actions.waitForVisible(this.accountStatementScreen.pendingTransactionPanelOpen);
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen[`${errorMessage}PendingTransactionErrorMessage`],
            80, 80, 50, 50, 500, 4);
    }

    validateOptInForSpendingRewardsVTD() {
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.accountStatementScreen.spendingRewardsHubSTUBPage)
                .should
                .equal(true);
            Actions.click(this.accountStatementScreen.cancelButton);
        } else {
            Actions.waitForVisible(this.homeScreen.title);
        }
    }

    validateDebitOnlyChequeOnlyPendingTransactions(type) {
        if (type === 'debitOnly') {
            Actions.isVisible(this.accountStatementScreen.noteText)
                .should
                .equal(false);
        } else {
            Actions.swipeUntilVisible(SwipeAction.UP, this.accountStatementScreen.pendingChequeTransactionType);
        }
    }

    verifyAccountTileFields(accountFields) {
        Actions.iOSObjectTreeOptimizationStart();
        for (let count = 0; count < accountFields.length; count++) {
            const field = accountFields[count].fieldsInAccountTile;
            if (field === 'noWarningAlert') {
                Actions.isVisible(this.accountStatementScreen.warningAlert)
                    .should
                    .equal(false);
            } else if (field === 'noOverDraftMessage') {
                Actions.isVisible(this.accountStatementScreen.overDraftMessage)
                    .should
                    .equal(false);
            } else {
                Actions.swipeByPercentUntilVisible(this.accountStatementScreen[field], 80, 80, 20, 20, 500, 2);
            }
        }
        Actions.iOSObjectTreeOptimizationStop();
    }

    verifyCondensedAccountTile(type) {
        if (type === 'personalLoan') {
            Actions.isVisible(this.accountStatementScreen.availableFunds)
                .should
                .equal(true);
        } else {
            Actions.isVisible(this.accountStatementScreen.availableFunds)
                .should
                .equal(false);
            Actions.isVisible(this.accountStatementScreen.accountNumber)
                .should
                .equal(true);
        }
    }

    verifyZeroRemaining() {
        if (DeviceHelper.isAndroid()) {
            Actions.getAttributeText(this.accountStatementScreen.remainingAllowanceISA)
                .should
                .equal('£0.00');
        } else {
            Actions.getAttributeText(this.accountStatementScreen.remainingAllowanceISA)
                .should
                .equal('NIL');
        }
    }

    swipeDownInStatementsPage() {
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen.availableFunds, 50, 50, 50, 30, 500, 10);
    }

    verifyCollapsedStatementsAccountTile(type) {
        if (type === 'personalLoan') {
            Actions.isVisible(this.accountStatementScreen.availableFunds)
                .should
                .equal(true);
        } else {
            Actions.isVisible(this.accountStatementScreen.availableFunds)
                .should
                .equal(true);
            Actions.isVisible(this.accountStatementScreen.accountNumber)
                .should
                .equal(true);
        }
    }

    validateAllTab() {
        Actions.isVisible(this.accountStatementScreen.allTabStatementsPage)
            .should
            .equal(true);
    }

    validatePendingTransactionDetails(type, transactionDetail) {
        if (type !== 'Credit' && DeviceHelper.isAndroid()) {
            Actions.swipeByPercentUntilVisible(this.accountStatementScreen.noteText,
                90, 70, 85, 40, 10000, 8);
        }
        for (let i = 0; i < transactionDetail.length; i++) {
            const individualtransactionDetail = this.accountStatementScreen[transactionDetail[i].detail];
            Actions.swipeUntilVisible(SwipeAction.UP, individualtransactionDetail);
        }
    }

    validateSpendingRewardsHubSTUBPage() {
        Actions.isVisible(this.accountStatementScreen.spendingRewardsHubSTUBPage)
            .should
            .equal(true);
    }

    isSelectAccountPageDisplayed() {
        Actions.isVisible(this.viewTransactionDetailScreen.selectAccountHeading)
            .should
            .equal(true);
    }

    validateSearchIcon() {
        Actions.waitForVisible(this.accountStatementScreen.searchIcon);
    }

    clickSearchIcon() {
        Actions.iOSObjectTreeOptimizationStart();
        Actions.waitAndClick(this.accountStatementScreen.searchIcon);
        Actions.iOSObjectTreeOptimizationStop();
    }

    validateNoSearchIcon() {
        Actions.isVisible(this.accountStatementScreen.searchIcon)
            .should
            .equal(false);
    }

    validateErrorMessage() {
        Actions.waitForVisible(this.accountStatementScreen.errorMessageRedBanner);
        Actions.isVisible(this.accountStatementScreen.errorMessage)
            .should
            .equal(true);
        Actions.isVisible(this.accountStatementScreen.tryAgainLink)
            .should
            .equal(true);
    }

    validateTotalFieldForFilters(total, type) {
        if (type === 'cardEnding') {
            Actions.isVisible(this.accountStatementScreen.cardEndingTransactionTotal)
                .should
                .equal(true);
        } else {
            Actions.isVisible(this.accountStatementScreen.cardEndingTransactionTotal)
                .should
                .equal(false);
        }
    }

    validateTreasuryFields(field) {
        Actions.isVisible(this.accountStatementScreen[field])
            .should
            .equal(true);
    }

    getAccountBalanceFromTile(accountName) {
        const pattern = /[- ][£][0-9]+(\.[0-9]{1,2})?/gm;
        if (DeviceHelper.isAndroid()) {
            if (currentAccount.includes(accountName)) {
                accountBalance = Actions.getAttributeText(this.accountStatementScreen.balanceAfterPendingValue);
            } else {
                accountBalance = Actions.getAttributeText(this.accountStatementScreen.accountBalance)
                    .toString();
            }
        } else if (currentAccount.includes(accountName)) {
            accountBalance = Actions.getAttributeText(this.accountStatementScreen.balanceAfterPending)
                .toString()
                .match(pattern)[0].trim();
        } else {
            accountBalance = Actions.getAttributeText(this.accountStatementScreen.accountTile)
                .toString()
                .match(pattern)[0].trim();
        }
    }

    validateStatementsPageHeader(accountName) {
        Actions.waitForVisible(this.commonScreen.backButton);
        Actions.getAttributeText(this.homeScreen.headerToolbar)
            .should
            .contains(accountName);
        Actions.getAttributeText(this.homeScreen.headerToolbar)
            .toString()
            .replace(/ /g, '')
            .should
            .contains(accountNumber.toString()
                .replace(/ /g, ''));
        Actions.isVisible(this.accountStatementScreen.actionMenu)
            .should
            .equal(true);
        this.getAccountBalanceFromTile(accountName.toLowerCase());
    }

    validateStatementsPageNoActionMenu(accountName, accountNum, type) {
        Actions.waitForVisible(this.commonScreen.backButton);
        if (type === 'homeInsurance') {
            Actions.getAttributeText(this.homeScreen.headerToolbar)
                .should
                .contains(accountName);
            const policyNumber = Actions.getAttributeText(this.accountStatementScreen.homeInsurancePolicynumber)
                .toString()
                .slice(-4);
            Actions.getAttributeText(this.homeScreen.headerToolbar)
                .toString()
                .should
                .contains(policyNumber);
        } else {
            Actions.getAttributeText(this.homeScreen.headerToolbar)
                .should
                .contains(accountName);
            Actions.getAttributeText(this.homeScreen.headerToolbar)
                .toString()
                .should
                .contains(accountNumber);
        }
        Actions.isVisible(this.accountStatementScreen.actionMenu)
            .should
            .equal(false);
    }

    scrollDownInTheTransactions() {
        Actions.waitForVisible(this.accountStatementScreen.statementLayout);
        Actions.swipeOnElement(SwipeAction.UP, this.accountStatementScreen.statementLayout);
    }

    validateAccountBalanceInHeader() {
        Actions.getAttributeText(this.accountStatementScreen.headerAccountBalance)
            .toString()
            .should
            .contains(accountBalance);
    }

    validateNoAccountTile() {
        Actions.isVisible(this.homeScreen.accountContainer)
            .should
            .equal(false);
    }

    validateMonthsTabSection() {
        Actions.isVisible(this.accountStatementScreen.title)
            .should
            .equal(true);
    }

    scrollUpOnTheTransactions() {
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen.accountTile, 50, 20, 50, 80);
    }

    validateCurrentBalanceAndAccountTileBackToPlace() {
        this.validateAccountTileNotHidden();
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.accountStatementScreen.headerAccountBalance)
                .should
                .equal(false);
        } else {
            assert.equal(Actions.getAttributeText(this.accountStatementScreen.headerAccountBalance)
                .includes(accountBalance), false);
        }
    }

    validateAccountTileNotHidden() {
        Actions.isVisible(this.homeScreen.accountContainer)
            .should
            .equal(true);
    }

    navigateAndGetAccountNumber(accountHeaderName, value, type) {
        const index = homePage.navigateToAccount(accountHeaderName);
        const accountsWithAccountNumber = ['current', 'islamicCurrent', 'saving', 'isa', 'cashIsa', 'htbIsa', 'personal Cbs Loan', 'current Account With View Transactions', 'cbsLoan'];
        if (value === 'accountNumber') {
            if (accountsWithAccountNumber.includes(type)) {
                if (DeviceHelper.isAndroid()) {
                    Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.accountNumber(index));
                    accountNumber = Actions.getAttributeText(this.homeScreen.accountNumber(index))
                        .toString()
                        .replace(/ /g, '')
                        .slice(-4);
                } else {
                    accountNumber = Actions.getAttributeText(this.homeScreen.accountNumber(index))
                        .toString()
                        .replace(/ /g, '')
                        .match(/[0-9]+(\.)/)[0].slice(4, 8);
                }
            } else if (type === 'scottishWidows') {
                if (DeviceHelper.isAndroid()) {
                    Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.scottishWidowsPolicyNumber(index));
                    accountNumber = Actions.getAttributeText(this.homeScreen.scottishWidowsPolicyNumber(index))
                        .toString()
                        .replace(/ /g, '')
                        .slice(-4);
                } else {
                    accountNumber = Actions.getAttributeText(this.homeScreen.scottishWidowsPolicyNumber(index))
                        .toString()
                        .replace(/ /g, '')
                        .match(/[0-9]+(\.)/)[0].slice(4, 8);
                }
            } else if (DeviceHelper.isAndroid()) {
                Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.creditCardAccountNumber(index));
                accountNumber = Actions.getAttributeText(this.homeScreen.creditCardAccountNumber(index))
                    .toString()
                    .replace(/ /g, '')
                    .slice(-4);
            } else {
                accountNumber = Actions.getAttributeText(this.homeScreen.creditCardAccountNumber(index))
                    .toString()
                    .replace(/ /g, '')
                    .match(/\d+/g)[0].slice(12);
            }
        } else {
            if (DeviceHelper.isAndroid()) {
                Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.accountBalance(index));
            }
            balanceAfterPending = Actions.getAttributeText(this.homeScreen.accountBalance(index))
                .toString();
            if (type !== 'currentAccountWithNoOverDraft') {
                overDraftLimit = Actions.getAttributeText(this.homeScreen.accountInfoBalance(index))
                    .toString();
            }
        }
    }

    validateNoBusinessName() {
        Actions.isVisible(this.accountStatementScreen.accountTileBusinessName)
            .should
            .equal(false);
    }

    shouldSeeAccountLink(accountName) {
        Actions.isVisible(
            this.accountStatementScreen.accountlinkLead(accountName)
        )
            .should
            .equal(true);
    }

    shouldSeeViewRatesLink() {
        Actions.isVisible(this.accountStatementScreen.viewRatesLink)
            .should
            .equal(true);
    }

    selectViewRatesLink() {
        Actions.click(this.accountStatementScreen.viewRatesLink);
    }

    shouldNotSeeViewRatesLink() {
        Actions.isVisible(this.accountStatementScreen.viewRatesLink)
            .should
            .equal(false);
    }

    shouldSeeInterestRate() {
        Actions.isVisible(this.accountStatementScreen.interestRate)
            .should
            .equal(true);
        Actions.isVisible(this.accountStatementScreen.rateOfInterest)
            .should
            .equal(true);
    }

    tapAndHoldOnAccountTile() {
        Actions.longPressed(this.accountStatementScreen.accountTile);
    }

    minimiseMortgageAccordion() {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.accountStatementScreen.mortgageAccordion);
        Actions.click(this.accountStatementScreen.mortgageAccordion);
    }

    compareStatementsValueWithAOV(aovValues, statementsPageValues, elementAt) {
        this.aovValues = aovValues;
        return aovValues[elementAt].trim() === statementsPageValues[elementAt].trim();
    }

    validateBalanceOrOverdraft(aovValue, statementsPageValue, type) {
        let flag = false;
        const pattern = /[- ][£][0-9]+(\.[0-9]{1,2})?/gm;
        const aovValues = aovValue.match(pattern);
        const statementsPageValues = statementsPageValue.match(pattern);

        if (type === 'balanceAfterPending') {
            flag = this.compareStatementsValueWithAOV(aovValues, statementsPageValues, 0);
        } else if (type === 'overDraftLimit') {
            flag = this.compareStatementsValueWithAOV(aovValues, statementsPageValues, 1);
        } else {
            throw new Error(`${type} type does not match`);
        }
        return flag;
    }

    compareStatementsAccountTileValueWithAOVPage(type) {
        let aovValue;
        if (type === 'balanceAfterPending' || type === 'overDraftLimit') {
            aovValue = (type === 'balanceAfterPending') ? balanceAfterPending : overDraftLimit;
            if (DeviceHelper.isIOS()) {
                assert.equal(this.validateBalanceOrOverdraft(aovValue,
                    Actions.getText(this.accountStatementScreen[`${type}Value`]), type), true);
            } else {
                Actions.getAttributeText(this.accountStatementScreen[`${type}Value`])
                    .toString()
                    .should
                    .contains(balanceAfterPending);
            }
        } else if (type === 'noOverDraftLimit') {
            Actions.isVisible(this.accountStatementScreen.noOverDraft)
                .should
                .equal(true);
        } else {
            throw new Error(`${type} type does not match`);
        }
    }

    viewActionMenuOptions() {
        Actions.waitForVisible(this.accountStatementScreen.actionMenuOptions);
    }

    validateExposedPendingErrorMessage(errorMessage) {
        Actions.swipeByPercentUntilVisible(this.accountStatementScreen[`${errorMessage}ErrorMessage`],
            80, 80, 50, 50, 500, 4);
    }

    validateExposedPendingDebitOnlyChequeOnlyTransactions(type) {
        if (type === 'chequeOnly') {
            Actions.isVisible(this.accountStatementScreen.chequesProcessingHeader)
                .should
                .equal(true);
            Actions.swipeByPercentUntilVisible(this.accountStatementScreen.chequeNoteText,
                80, 80, 50, 50, 500, 4);
            Actions.swipeByPercentUntilVisible(this.accountStatementScreen.exposedPendingChequeTransactionType,
                80, 80, 50, 50, 500, 4);
        } else {
            Actions.isVisible(this.accountStatementScreen.chequesProcessingHeader)
                .should
                .equal(false);
            Actions.swipeByPercentUntilVisible(this.accountStatementScreen.pendingHeader,
                80, 80, 50, 50, 500, 4);
            Actions.swipeByPercentUntilVisible(this.accountStatementScreen.exposedPendingDirectDebitTransactionType,
                80, 80, 50, 50, 500, 4);
        }
    }

    validateNoCreditIndicator() {
        Actions.isVisible(this.accountStatementScreen.firstCreditIndicator)
            .should
            .equal(false);
    }

    validateChevron() {
        Actions.isVisible(this.accountStatementScreen.chevron)
            .should
            .equal(DeviceHelper.isIOS());
    }

    shouldSeeTransactionLogo() {
        Actions.isVisible(this.accountStatementScreen.transactionLogo).should.equal(true);
    }
}

module.exports = AccountStatementPage;
