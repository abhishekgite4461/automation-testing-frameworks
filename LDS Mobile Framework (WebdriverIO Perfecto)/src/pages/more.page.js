require('chai').should();
const AuthPage = require('./common/auth.page');
const DeviceHelper = require('../utils/device.helper');
const Actions = require('./common/actions');
const SavedAppDataRepository = require('../data/savedAppData.repository');
const SavedData = require('../enums/savedAppData.enum');

class MorePage extends AuthPage {
    get moreScreen() {
        return this.getNativeScreen('more.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.moreScreen.title);
    }

    validateOptionsAvailable(moreMenuOptions, isVisible = true) {
        let actualExist = true;
        for (let i = 0; i < moreMenuOptions.length; i++) {
            const moreMenuOptionLocator = this.moreScreen[moreMenuOptions[i].optionsInMorePage];
            if (!Actions.isExisting(moreMenuOptionLocator)) {
                Actions.swipeByPercentUntilVisible(this.moreScreen
                    .logout, 70, 80, 70, 30, 1000);
                if (!Actions.isExisting(moreMenuOptionLocator)) {
                    actualExist = false;
                }
            }
            actualExist.should.equal(isVisible);
        }
    }

    verifySwitchBusinessIconIsPresent() {
        Actions.isVisible(this.moreScreen.moreSwitchBusiness).should.equal(true);
    }

    verifySwitchBusinessIconIsNotPresent() {
        Actions.isVisible(this.moreScreen.moreSwitchBusiness).should.equal(false);
    }

    selectSwitchBusinessIcon() {
        Actions.click(this.moreScreen.moreSwitchBusiness);
    }

    selectYourProfile() {
        Actions.click(this.moreScreen.yourProfile);
    }

    selectLogoff() {
        Actions.swipeByPercentUntilVisible(this.moreScreen.logout);
        Actions.click(this.moreScreen.logout);
        Actions.reportTimer('Logoff', 'logged out');
        Actions.iOSObjectTreeOptimizationStop();
    }

    verifyChequeSubOptionsAreNotVisible() {
        Actions.swipeByPercentUntilVisible(this.moreScreen.logout);
        Actions.waitForNotVisible(this.moreScreen.depositCheque);
        Actions.waitForNotVisible(this.moreScreen.viewDepositHistory);
    }

    verifyChequeDepositEntryPointOn() {
        Actions.waitForVisible(this.moreScreen.depositCheque);
    }

    verifyChequeDepositEntryPointOff() {
        Actions.waitForNotVisible(this.moreScreen.depositCheque);
    }

    selectChequeDeposit() {
        Actions.click(this.moreScreen.depositCheque);
    }

    selectDepositHistory() {
        Actions.click(this.moreScreen.viewDepositHistory);
    }

    selectInbox() {
        Actions.click(this.moreScreen.inbox);
    }

    selectSettings() {
        Actions.click(this.moreScreen.settings);
    }

    selectCardManagement() {
        Actions.click(this.moreScreen.cardManagement);
    }

    selectDarkURL() {
        Actions.swipeByPercentUntilVisible(this.moreScreen.darkURL);
        Actions.click(this.moreScreen.darkURL);
    }

    verifyVisibilityOfDarkUrl(type) {
        Actions.isVisible(this.moreScreen.darkURL).should.equal(type);
    }

    validateUserFirstName() {
        const userName = Actions.getText(this.moreScreen.yourProfile);
        SavedAppDataRepository.getData(SavedData.FIRST_NAME).should.equal(userName.split(' ').shift());
    }

    validateUserName() {
        const userName = Actions.getText(this.moreScreen.yourProfile);
        SavedAppDataRepository.getData(SavedData.FROM_ACCOUNT_NAME).should.equal(userName);
    }

    shouldNotShowChequeHistoryEntryPoint() {
        Actions.isVisible(this.moreScreen.viewDepositHistory).should.equal(false);
    }

    shouldShowChequeHistoryEntryPoint() {
        Actions.isVisible(this.moreScreen.viewDepositHistory).should.equal(true);
    }

    verifyBusinessName(buisness) {
        if (DeviceHelper.isAndroid()) {
            Actions.getText(this.moreScreen.businessName).should.equal(buisness);
        } else {
            Actions.getText(this.moreScreen.businessName(buisness)).should.equal(buisness);
        }
    }
}

module.exports = MorePage;
