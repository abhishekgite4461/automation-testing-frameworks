require('chai').should();
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class SoftTokenPage extends UnAuthPage {
    /**
     * @returns {SoftTokenScreen}
     */
    get softTokenScreen() {
        return this.getNativeScreen('softToken.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.softTokenScreen.title);
    }

    shouldSeeSoftTokenScreen() {
        Actions.isVisible(this.softTokenScreen.title).should.equal(true);
    }

    enterPasscode(passCode) {
        Actions.setImmediateValueAndTapOut(this.softTokenScreen.passCode, passCode);
    }

    selectContinueButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.softTokenScreen.continueButton);
        Actions.click(this.softTokenScreen.continueButton);
    }

    selectSoftTokenLink() {
        Actions.click(this.softTokenScreen.softTokenInfoLink);
    }

    isSoftTokenOverlayVisible() {
        Actions.isVisible(this.softTokenScreen.whatisSoftTokenOverlay).should.equal(true);
        Actions.isVisible(this.softTokenScreen.closeButton).should.equal(true);
    }

    selectCloseButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.softTokenScreen.closeButton);
        Actions.click(this.softTokenScreen.closeButton);
    }

    verifyIncorrectPasscodeErrorBanner() {
        Actions.isVisible(this.softTokenScreen.incorrectPasscodeErrorMessage).should.equal(true);
    }
}

module.exports = SoftTokenPage;
