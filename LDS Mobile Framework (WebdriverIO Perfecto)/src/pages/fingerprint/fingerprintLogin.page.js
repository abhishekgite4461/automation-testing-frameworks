require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class FingerprintLoginPage extends AuthPage {
    /**
     * @returns {FingerprintLoginScreen}
     */
    get fingerprintLoginScreen() {
        return this.getNativeScreen('fingerprint/fingerprintLogin.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.fingerprintLoginScreen.title);
    }

    cancelFingerprintLogin() {
        Actions.click(this.fingerprintLoginScreen.cancel);
    }
}

module.exports = FingerprintLoginPage;
