const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class FingerprintEnrolmentPage extends AuthPage {
    /**
     * @returns {FingerprintFraudScreen}
     */
    get fingerprintFraudScreen() {
        return this.getNativeScreen('fingerprint/fingerprintFraud.screen');
    }

    /**
     * @returns {FingerprintSuccessScreen}
     */
    get fingerprintSuccessScreen() {
        return this.getNativeScreen('fingerprint/fingerprintSuccess.screen');
    }

    /**
     * @returns {FingerprintFailureScreen}
     */
    get fingerprintFailureScreen() {
        return this.getNativeScreen('fingerprint/fingerprintFailure.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.fingerprintFraudScreen.title);
    }

    acceptFraudMessage() {
        Actions.click(this.fingerprintFraudScreen.yesButton);
    }

    declineFraudMessage() {
        Actions.click(this.fingerprintFraudScreen.noButton);
    }

    dismissSuccessMessage() {
        Actions.waitForVisible(this.fingerprintSuccessScreen.title);
        Actions.click(this.fingerprintSuccessScreen.okButton);
    }

    dismissFailureMessage() {
        Actions.waitForVisible(this.fingerprintFailureScreen.title);
        Actions.click(this.fingerprintFailureScreen.okButton);
    }

    fraudMessage() {
        Actions.waitForVisible(this.fingerprintFraudScreen.title);
    }
}

module.exports = FingerprintEnrolmentPage;
