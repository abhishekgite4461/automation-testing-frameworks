const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class FingerprintNotRegisteredPage extends AuthPage {
    /**
     * @returns {FingerprintNoFingerprintsScreen}
     */
    get fingerprintNoFingerprintsScreen() {
        return this.getNativeScreen('fingerprint/fingerprintNoFingerprints.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.fingerprintNoFingerprintsScreen.title);
    }

    validateNoFingerprintMessage() {
        Actions.isVisible(this.fingerprintNoFingerprintsScreen.title).should.equal(true);
    }

    dismissNoFingerprintsMessage() {
        Actions.click(this.fingerprintNoFingerprintsScreen.okButton);
    }
}

module.exports = FingerprintNotRegisteredPage;
