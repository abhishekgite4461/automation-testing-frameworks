require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class FingerprintInterstitialPage extends AuthPage {
    /**
     * @returns {FingerprintInterstitialScreen}
     */
    get fingerprintInterstitialScreen() {
        return this.getNativeScreen('fingerprint/fingerprintInterstitial.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.fingerprintInterstitialScreen.title);
    }

    optIn() {
        Actions.click(this.fingerprintInterstitialScreen.yesButton);
    }

    optOut() {
        Actions.click(this.fingerprintInterstitialScreen.noButton);
        Actions.reportTimer('Enrolment_Success', 'Home');
    }

    noInterstitial() {
        Actions.isVisible(this.fingerprintInterstitialScreen.title).should.equal(false);
    }

    title() {
        Actions.isVisible(this.fingerprintInterstitialScreen.title);
    }
}

module.exports = FingerprintInterstitialPage;
