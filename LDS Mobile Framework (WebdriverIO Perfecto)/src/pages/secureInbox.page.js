const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');
require('chai').should();

class SecureInboxpage extends AuthPage {
    /**
     * @returns {SecureInboxScreen}
     */
    get secureInboxScreen() {
        return this.getNativeScreen('secureInbox.screen');
    }

    get secureInboxWebpageScreen() {
        return this.getWebScreen('secureInboxWebpage.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.secureInboxScreen.title);
    }

    clickOnFirstMessage() {
        Actions.waitForVisible(this.secureInboxScreen.inboxMessage);
        Actions.click(this.secureInboxScreen.inboxMessage);
    }

    clickViewPDF() {
        Actions.waitForVisible(this.secureInboxScreen.pdfMessage);
        Actions.click(this.secureInboxScreen.pdfMessage);
        if (DeviceHelper.isIOS()) {
            Actions.click(this.secureInboxScreen.stopView);
        }
    }

    clickArchive() {
        Actions.waitForVisible(this.secureInboxScreen.archiveButton);
        Actions.click(this.secureInboxScreen.archiveButton);
    }

    clickUndoArchive() {
        Actions.waitForVisible(this.secureInboxScreen.undoArchiveButton);
        Actions.click(this.secureInboxScreen.undoArchiveButton);
    }

    verifyUndoArchive() {
        Actions.isVisible(this.secureInboxWebpageScreen.verifyUndoArchiveMessage);
    }

    selectInboxFolders() {
        if (DeviceHelper.isIOS()) {
            const size = Actions.getElementRect(this.secureInboxScreen.inbox);
            const percentage = parseInt(size.y / DeviceHelper.windowHandleSize().height * 100, 10);
            Actions.tapByLocation(`85%,${percentage}%`);
        } else {
            Actions.click(this.secureInboxScreen.inbox);
        }
    }

    selectFolders(accountName) {
        Actions.click(this.secureInboxScreen.folderName(accountName));
    }

    verifyFilteredMessages(accountName) {
        Actions.waitForVisible(this.secureInboxScreen.accountLabel(accountName));
        if (!Actions.isVisible(this.secureInboxScreen.noMessagesLabel)) {
            Actions.waitForVisible(this.secureInboxScreen.filteredMessage(accountName));
        }
    }
}

module.exports = SecureInboxpage;
