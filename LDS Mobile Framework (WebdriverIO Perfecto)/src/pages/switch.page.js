const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');
const runConfig = require('../enums/runConfig.enum');

class SwitchesPage extends AuthPage {
    /**
     * @returns {SwitchScreen}
     */
    get switchScreen() {
        return this.getNativeScreen('switch.screen');
    }

    setSwitches(switches) {
        if (DeviceHelper.isAndroid()) {
            Actions.waitForEnabled(this.switchScreen.title, 2 * runConfig.TIMEOUTS.WAIT_FOR());
        }
        switches.on.map((sw) => ({sw, checked: true}))
            .concat(switches.off.map((sw) => ({sw, checked: false})))
            // Switches are displayed in alphabetical order
            .sort((sw) => sw.sw.appName)
            .forEach((sw) => {
                const switchLoc = this.switchScreen.switchToggle(sw.sw.appName);
                Actions.swipeByPercentUntilVisible(switchLoc, 80, 80, 30, 30, 180, 11);
                if (Actions.isChecked(switchLoc) !== sw.checked) {
                    Actions.click(switchLoc);
                }
            });
    }
}

module.exports = SwitchesPage;
