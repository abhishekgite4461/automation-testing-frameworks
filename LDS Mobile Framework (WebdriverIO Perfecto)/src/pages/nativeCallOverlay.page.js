const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class NativeCallOverlayPage extends AuthPage {
    /**
     * @returns {NativeCallOverlayScreen}
     */
    get nativeCallOverlayScreen() {
        return this.getNativeScreen('nativeCallOverlay.screen');
    }

    shouldSeeNativeCallOverlay() {
        Actions.waitForVisible(this.nativeCallOverlayScreen.title);
    }
}

module.exports = NativeCallOverlayPage;
