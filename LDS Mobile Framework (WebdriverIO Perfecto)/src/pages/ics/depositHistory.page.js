require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class DepositHistoryPage extends AuthPage {
    /**
     * @returns {DepositHistoryScreen}
     */
    get depositHistoryScreen() {
        return this.getNativeScreen('ics/depositHistory.screen');
    }

    get chequeDepositScreen() {
        return this.getNativeScreen('ics/chequeDeposit.screen');
    }

    verifyHistoryDisabledErrors(errorMessages) {
        for (const errorMessage of errorMessages) {
            const displayedError = this.depositHistoryScreen[errorMessage.errorMsg];
            Actions.isVisible(displayedError).should.equal(true);
        }
    }

    verifyHelpandInfottext() {
        Actions.isVisible(this.depositHistoryScreen.helpandInfoNavigationTile).should.equal(false);
    }

    clickAccountNavigation() {
        Actions.click(this.depositHistoryScreen.accountNavigationImage);
    }

    checkAndSelectAccount() {
        const accountListSize = browser.elements(this.depositHistoryScreen.accountList).value.length;
        if (accountListSize > 1) {
            Actions.click(this.depositHistoryScreen.changeAccount);
        }
    }

    verifyChangedAccount() {
        Actions.isVisible(this.depositHistoryScreen.depositHistoryList).should.equal(true);
    }

    verifyDepositHistoryPage() {
        Actions.isVisible(this.chequeDepositScreen.chequeAmountField).should.equal(false);
        Actions.isVisible(this.depositHistoryScreen.depositHistoryTitle).should.equal(true);
    }

    selectDepositCheque() {
        Actions.click(this.depositHistoryScreen.depositChequeText);
    }

    selectDepositItem() {
        Actions.click(this.depositHistoryScreen.depositHistoryList);
    }

    verifyDepositHistoryField(depositHistoryFields) {
        for (let i = 0; i < depositHistoryFields.length; i++) {
            const depositHistoryPageDetails = this.depositHistoryScreen[depositHistoryFields[i].historyPageDetails];
            Actions.isVisible(depositHistoryPageDetails).should.equal(true);
        }
    }

    verifyDepositHistoryAccountPicker(accountPickerFields) {
        if (DeviceHelper.isAndroid()) {
            for (const accountPickerField of accountPickerFields) {
                const accountPickerFieldsPageDetails = this.depositHistoryScreen[accountPickerField.accountPickerList];
                Actions.getAttributeText(accountPickerFieldsPageDetails).should.not.equal('');
            }
        } else {
            Actions.getAttributeText(this.depositHistoryScreen.accountPicker, 'label')
                .split('.')[1].replace(/,/g, '').should.not.equal('');
            Actions.getAttributeText(this.depositHistoryScreen.accountPicker, 'label')
                .split('.')[3].replace(/,/g, '').trim().should.equal('Type');
            Number(Actions.getAttributeText(this.depositHistoryScreen.accountPicker, 'label')
                .split('Sort code.')[1].split('.')[0].replace(/,/g, '')).should.gt(0);
            Number(Actions.getAttributeText(this.depositHistoryScreen.accountPicker, 'label')
                .split('Account number.')[1].split('.')[0].replace(/,/g, '')).should.gt(0);
            parseFloat(Actions.getAttributeText(this.depositHistoryScreen.accountPicker, 'label')
                .split('Account balance. £')[1].split('.')[0].replace(/,/g, '')).should.gte(0.00);
            parseFloat(Actions.getAttributeText(this.depositHistoryScreen.accountPicker, 'label')
                .split('Available funds. £')[1].split('.')[0].replace(/,/g, '')).should.gte(0.00);
        }
    }

    scrollToLastTransactionDetail() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.depositHistoryScreen.noMoretransactionMessage, 10);
    }

    verifyEndOfTransactionMessage() {
        Actions.isVisible(this.depositHistoryScreen.noMoretransactionMessage).should.equal(true);
    }

    verifyNoTransactionMessage() {
        Actions.isVisible(this.depositHistoryScreen.noTransactionMessage).should.equal(true);
    }

    verifyReferenceText(reference) {
        Actions.isVisible(this.depositHistoryScreen.referenceTextVisibility(reference)).should.equal(true);
        Actions.click(this.depositHistoryScreen.referenceTextVisibility(reference));
        Actions.isVisible(this.depositHistoryScreen.referenceOnTransactionDetailPage(reference)).should.equal(true);
    }

    verifyReferenceTextIsNotVisible(reference) {
        Actions.isVisible(this.depositHistoryScreen.referenceTextVisibility(reference)).should.equal(false);
        Actions.click(this.depositHistoryScreen.selectReferenceOnTransactionList);
        Actions.isVisible(this.depositHistoryScreen.referenceOnTransactionDetailPage(reference)).should.equal(false);
    }

    selectPendingTransaction() {
        Actions.click(this.depositHistoryScreen.selectPendingOption);
    }

    selectFundAvailableTransaction() {
        Actions.click(this.depositHistoryScreen.selectFundsAvailableOption);
    }

    selectRejectedTransaction() {
        Actions.click(this.depositHistoryScreen.selectRejectedOption);
    }
}

module.exports = DepositHistoryPage;
