require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const SwipeAction = require('../../enums/swipeAction.enum');

class ChequeDepositStatusPage extends AuthPage {
    /**
     * @returns {ChequeDepositStatusScreen}
     */

    get chequeDepositStatusScreen() {
        return this.getNativeScreen('ics/chequeDepositStatus.screen');
    }

    verifyDepositSuccessPage() {
        Actions.getAttributeText(this.chequeDepositStatusScreen.title, 'label').toString().toLowerCase().should.equal('cheque deposited');
        Actions.isExisting(this.chequeDepositStatusScreen.fastChequeIcon).should.equal(true);
    }

    verifyChequeDepositSuccessField(successDepositFields) {
        for (let i = 0; i < successDepositFields.length; i++) {
            const successDepositPageOptions = this.chequeDepositStatusScreen[
                successDepositFields[i].successDepositOptions];
            Actions.swipeUntilVisible(SwipeAction.UP, successDepositPageOptions);
        }
    }

    selectDepositHistoryButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.chequeDepositStatusScreen.viewDepositHistoryButton);
        Actions.click(this.chequeDepositStatusScreen.viewDepositHistoryButton);
    }

    verifySortCodeErrorMessage() {
        Actions.isVisible(this.chequeDepositStatusScreen.sortCodeError).should.equal(true);
    }

    verifyDuplicateErrors(chequeOrder) {
        if (chequeOrder === '2') {
            Actions.isVisible(this.chequeDepositStatusScreen.duplicateChequeMessage).should.equal(true);
        }
    }

    verifyDailyLimitError(chequeOrder) {
        if (chequeOrder === '3') {
            Actions.isVisible(this.chequeDepositStatusScreen.dailyLimitError).should.equal(true);
        }
    }

    setDepositedAmount() {
        const amountDeposited = parseFloat(Actions.getAttributeText(this.chequeDepositStatusScreen.amountDeposited, 'label').toString().split('£')[1].replace(/,/g, ''));
        SavedAppDataRepository.setData(SavedData.CHEQUE_DEPOSITED_AMOUNT, amountDeposited);
    }

    selectDepositAnotherCheque() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.chequeDepositStatusScreen.depositAnotherCheque);
        Actions.click(this.chequeDepositStatusScreen.depositAnotherCheque);
    }

    verifyDepositSuccessOrFailurePage() {
        if (Actions.isVisible(this.chequeDepositStatusScreen.duplicateChequeMessage)) {
            Actions.isVisible(this.chequeDepositStatusScreen.duplicateChequeMessage).should.equal(true);
        } else {
            Actions.getAttributeText(this.chequeDepositStatusScreen.title, 'label').toString().toLowerCase().should.equal('cheque deposited');
            Actions.isExisting(this.chequeDepositStatusScreen.fastChequeIcon).should.equal(true);
        }
    }
}

module.exports = ChequeDepositStatusPage;
