require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class IcsMoreInformationPage extends AuthPage {
    /**
     * @returns {IcsMoreInformationScreen}
     */
    get icsMoreInformationScreen() {
        return this.getNativeScreen('ics/icsMoreInformation.screen');
    }

    viewMoreInformationText(visibility) {
        Actions.isVisible(this.icsMoreInformationScreen.moreInformationText).should.equal(visibility);
    }

    verifyChequeBoxSelectivity(visibility) {
        Actions.isChecked(this.icsMoreInformationScreen.moreInformationCheckbox).should.equal(visibility);
    }

    shouldSeeInformationDialogBox(visibility) {
        Actions.isVisible(this.icsMoreInformationScreen.moreInformationDialogbox).should.equal(visibility);
        Actions.isVisible(this.icsMoreInformationScreen.moreInformationContent).should.equal(visibility);
        Actions.isVisible(this.icsMoreInformationScreen.moreInformationOkButton).should.equal(visibility);
    }

    selectOkButton() {
        Actions.click(this.icsMoreInformationScreen.moreInformationOkButton);
    }

    selectCheckBox() {
        Actions.waitForVisible(this.icsMoreInformationScreen.moreInformationCheckbox);
        Actions.click(this.icsMoreInformationScreen.moreInformationCheckbox);
    }

    openMoreInformationDialogBox() {
        Actions.isVisible(this.icsMoreInformationScreen.moreInformationDialogbox).should.equal(true);
    }

    selectMoreInformationText() {
        Actions.waitForVisible(this.icsMoreInformationScreen.moreInformationText);
        Actions.click(this.icsMoreInformationScreen.moreInformationText);
    }
}

module.exports = IcsMoreInformationPage;
