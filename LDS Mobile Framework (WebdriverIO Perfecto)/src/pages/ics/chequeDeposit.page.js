require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');
const AppPackageName = require('../../apps/appInfo.factory');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');

class ChequeDepositPage extends AuthPage {
    /**
     * @returns {ChequeDepositScreen}
     */
    get chequeDepositScreen() {
        return this.getNativeScreen('ics/chequeDeposit.screen');
    }

    verifyChequeDepositPage(visibility) {
        Actions.isVisible(this.chequeDepositScreen.title).should.equal(visibility);
    }

    waitForChequeDepositTabEnabled() {
        Actions.waitForEnabled(this.chequeDepositScreen.title, 30000);
    }

    verifyChequeDepositField(chequeDepositFields) {
        for (let i = 0; i < chequeDepositFields.length; i++) {
            const chequeDepositPageOptions = this.chequeDepositScreen[chequeDepositFields[i].chequeDepositOptions];
            Actions.swipeUntilVisible(SwipeAction.UP, chequeDepositPageOptions);
        }
    }

    verifyChequeLimitService() {
        Actions.isVisible(this.chequeDepositScreen.chequeLimitService).should.equal(true);
    }

    enterChequeAmount(amount) {
        if (DeviceHelper.isIOS()) {
            Actions.setImmediateValueAndTapOut(this.chequeDepositScreen.chequeAmountField, amount);
        } else {
            Actions.setImmediateValue(this.chequeDepositScreen.chequeAmountField, amount);
        }
    }

    limitErrorMessageValidation(errorVisibility) {
        Actions.isVisible(this.chequeDepositScreen.chequeErrorMsg).should.equal(errorVisibility);
    }

    invalidChequeAmountValidation(invalidAmount) {
        Actions.getText(this.chequeDepositScreen.chequeAmountField).toString().replace('£', '').should.not.equal(invalidAmount);
    }

    selectChequeAmountFeild() {
        Actions.click(this.chequeDepositScreen.chequeAmountField);
    }

    selectReferenceField() {
        Actions.click(this.chequeDepositScreen.chequeReferenceField);
    }

    verifyChequeAmountFeildHelpText() {
        Actions.isVisible(this.chequeDepositScreen.chequeAmountFeildHelpText).should.equal(true);
    }

    verifyReferenceFeildHelpText() {
        Actions.isVisible(this.chequeDepositScreen.chequeReferenceFieldHelpText).should.equal(true);
    }

    enterChequeReference(chequeReference) {
        Actions.setImmediateValueAndTapOut(this.chequeDepositScreen.chequeReferenceField, chequeReference);
    }

    checkMoreInformationAlert() {
        if (Actions.isVisible(this.chequeDepositScreen.moreInformationDialogbox)) {
            Actions.click(this.chequeDepositScreen.moreInformationOkButton);
        }
    }

    selectAccountNavigationIcon() {
        Actions.click(this.chequeDepositScreen.depositAccount);
    }

    selectDepositHistory() {
        Actions.click(this.chequeDepositScreen.depositHistoryText);
    }

    verifyDepositDisabledErrors(errorMessages) {
        for (const errorMessage of errorMessages) {
            const displayedError = this.chequeDepositScreen[errorMessage.errorMsg];
            Actions.isVisible(displayedError).should.equal(true);
        }
    }

    verifyDepositChequeDisabledPage() {
        Actions.isVisible(this.chequeDepositScreen.depositChequeDisabled).should.equal(true);
    }

    selectFrontOfCheque() {
        Actions.click(this.chequeDepositScreen.captureFrontOfChequeButton);
        if (Actions.isVisible(this.chequeDepositScreen.allowCamera)) {
            Actions.click(this.chequeDepositScreen.allowCamera);
        }
        if (Actions.isVisible(this.chequeDepositScreen.closeDemobutton)) {
            Actions.click(this.chequeDepositScreen.closeDemobutton);
        }
    }

    verifyAlertMessage() {
        if (Actions.isVisible(this.chequeDepositScreen.problemScanningAlertPopupMessage)) {
            Actions.click(this.chequeDepositScreen.problemScanningAlertPopupMessage);
        }
        if (Actions.isVisible(this.chequeDepositScreen.errorMessageOkButton)) {
            Actions.click(this.chequeDepositScreen.errorMessageOkButton);
        }
    }

    startInjectImage(chequeImage) {
        const front = {};
        const back = {};
        const identifier = AppPackageName.getAppInfo(browser.capabilities).appPackage;
        if (DeviceHelper.isAndroid()) {
            front.repositoryFile = 'PUBLIC:ICSImageInjection/Android/LDS/'.concat(chequeImage).concat('.jpg');
            front.identifier = identifier;
            back.repositoryFile = 'PUBLIC:ICSImageInjection/Android/LDS/BackOfCheque2.jpg';
            back.identifier = identifier;
            Actions.stopImageInjection();
            Actions.startImageInjection(front);
            Actions.waitForVisible(this.chequeDepositScreen.useChequeImageButton);
            Actions.click(this.chequeDepositScreen.useChequeImageButton);
            Actions.startImageInjection(back);
            if (Actions.isVisible(this.chequeDepositScreen.errorMessageOkButton)) {
                Actions.click(this.chequeDepositScreen.errorMessageOkButton);
            }
            Actions.waitForVisible(this.chequeDepositScreen.useChequeImageButton);
            Actions.click(this.chequeDepositScreen.useChequeImageButton);
            Actions.stopImageInjection();
        } else {
            front.repositoryFile = 'PUBLIC:ICSImageInjection/iOS/'.concat(chequeImage).concat('.png');
            front.identifier = identifier;
            back.repositoryFile = 'PUBLIC:ICSImageInjection/iOS/BackOfCheque.jpg';
            back.identifier = identifier;
            Actions.stopImageInjection();
            Actions.startImageInjection(front);
            this.verifyAlertMessage();
            Actions.waitForVisible(this.chequeDepositScreen.useChequeImageButton);
            Actions.click(this.chequeDepositScreen.useChequeImageButton);
            Actions.stopImageInjection();
            Actions.startImageInjection(back);
            this.verifyAlertMessage();
            Actions.waitForVisible(this.chequeDepositScreen.useChequeImageButton);
            Actions.click(this.chequeDepositScreen.useChequeImageButton);
            Actions.stopImageInjection();
        }
    }

    verifyThumbnailImages() {
        Actions.isVisible(this.chequeDepositScreen.frontOfChequeThumbnail).should.equal(true);
        Actions.isVisible(this.chequeDepositScreen.backOfChequeThumbnail).should.equal(true);
    }

    selectReviewDepositButton() {
        Actions.click(this.chequeDepositScreen.reviewDepositButton);
    }

    selectFrontCamera() {
        Actions.click(this.chequeDepositScreen.captureFrontOfChequeButton);
    }

    allowCameraPermission() {
        if (DeviceHelper.osVersion() !== 'O') {
            const osVersion = parseFloat(DeviceHelper.osVersion());
            if (DeviceHelper.isIOS() || (DeviceHelper.isAndroid() && osVersion >= 6.0)) {
                Actions.click(this.chequeDepositScreen.allowCamera);
            }
        } else {
            Actions.click(this.chequeDepositScreen.allowCamera);
        }
    }

    closeDemo() {
        Actions.click(this.chequeDepositScreen.closeDemobutton);
    }

    verifyCameraEnabled() {
        Actions.waitForVisible(this.chequeDepositScreen.closeCamerabutton);
        Actions.isVisible(this.chequeDepositScreen.closeCamerabutton).should.equal(true);
    }

    denyCameraPermission() {
        if (DeviceHelper.osVersion() !== 'O') {
            const osVersion = parseFloat(DeviceHelper.osVersion());
            if (DeviceHelper.isIOS() || (DeviceHelper.isAndroid() && osVersion >= 6.0)) {
                Actions.click(this.chequeDepositScreen.denyPermission);
                Actions.click(this.chequeDepositScreen.captureFrontOfChequeButton);
            }
        } else {
            Actions.click(this.chequeDepositScreen.denyPermission);
            Actions.click(this.chequeDepositScreen.captureFrontOfChequeButton);
        }
    }

    selectCheckBoxCameraPermission() {
        if (DeviceHelper.osVersion() !== 'O') {
            const osVersion = parseFloat(DeviceHelper.osVersion());
            if (osVersion >= 6.0) {
                Actions.click(this.chequeDepositScreen.selectCheckBox);
                Actions.click(this.chequeDepositScreen.denyPermission);
            }
        } else {
            Actions.click(this.chequeDepositScreen.selectCheckBox);
            Actions.click(this.chequeDepositScreen.denyPermission);
        }
    }

    verifySettingCameraPermission() {
        if (DeviceHelper.osVersion() !== 'O') {
            const osVersion = parseFloat(DeviceHelper.osVersion());
            if (osVersion >= 6.0) {
                Actions.isVisible(this.chequeDepositScreen.settingAlert).should.equal(true);
            }
        } else {
            Actions.isVisible(this.chequeDepositScreen.settingAlert).should.equal(true);
        }
    }

    selectSettings() {
        Actions.isVisible(this.chequeDepositScreen.settingButton).should.equal(true);
    }

    selectHomeBackButton() {
        Actions.click(this.chequeDepositScreen.depositHomeBackButton);
    }

    shouldSeeOptionsInWinback() {
        Actions.isVisible(this.chequeDepositScreen.winBackTitle).should.equal(true);
        Actions.isVisible(this.commonScreen.winBackStayButton).should.equal(true);
        Actions.isVisible(this.commonScreen.winBackLeaveButton).should.equal(true);
    }

    verifyWinBackOnNegativeAction() {
        Actions.click(this.chequeDepositScreen.stayButton);
        Actions.isVisible(this.chequeDepositScreen.frontOfChequeThumbnail).should.equal(true);
    }

    verifyWinBackOnPositiveAction() {
        Actions.click(this.chequeDepositScreen.leaveButton);
        Actions.isVisible(this.chequeDepositScreen.homeAccountActionButton).should.equal(true);
    }

    closeCameraButton() {
        Actions.click(this.chequeDepositScreen.closeCamerabutton);
    }

    verifyFastChequeLogo() {
        if (DeviceHelper.isIOS()) {
            Actions.isVisible(this.chequeDepositScreen.fastChequeIcon).should.equal(true);
        }
        return true;
    }

    static autoLogOffPauseTimer(autoLogOffTime) {
        const time = autoLogOffTime.split('Minute')[0].trim();
        let waitTime = (time * 60 * 1000) - 15000;
        if (DeviceHelper.isAndroid()) {
            waitTime = (time * 60 * 1000);
        }
        Actions.pause(waitTime);
    }

    verifyReferenceMaxLength() {
        if (DeviceHelper.isAndroid()) {
            Actions.getAttribute(this.chequeDepositScreen.chequeReferenceField, 'text').concat('').replace('Ref: ', '').length.should.equal(18);
        } else {
            Actions.getAttribute(this.chequeDepositScreen.chequeReferenceField, 'value').replace('Ref: ', '').length.should.equal(18);
        }
    }

    getMainBalance() {
        let mainBalance;
        if (DeviceHelper.isAndroid()) {
            mainBalance = parseFloat(Actions.getAttribute(this.chequeDepositScreen.currentMainBalance, 'text')[0].split('£')[1].replace(/,/g, ''));
        } else {
            mainBalance = parseFloat(Actions.getAttributeText(this.chequeDepositScreen.currentMainBalance, 'label')
                .split('Account balance. £')[1].split('(')[0].replace(/,/g, ''));
        }
        SavedAppDataRepository.setData(SavedData.CHEQUE_MAIN_BALANCE,
            mainBalance);
    }

    verifyUpdatedDeferredBalance() {
        let updatedMainBalance;
        if (DeviceHelper.isAndroid()) {
            updatedMainBalance = parseFloat(Actions.getAttribute(this.chequeDepositScreen.currentMainBalance, 'text')[0].split('£')[1].replace(/,/g, ''));
        } else {
            updatedMainBalance = parseFloat(Actions.getAttributeText(this.chequeDepositScreen.currentMainBalance, 'label')
                .split('Account balance. £')[1].replace(/,/g, ''));
        }
        const amountDeposited = SavedAppDataRepository.getData(SavedData.CHEQUE_DEPOSITED_AMOUNT);
        const mainBalance = SavedAppDataRepository.getData(SavedData.CHEQUE_MAIN_BALANCE);
        let balanceAfterChequeDeposit = amountDeposited + mainBalance;
        balanceAfterChequeDeposit = parseFloat(balanceAfterChequeDeposit.toFixed(2));
        updatedMainBalance.should.equal(balanceAfterChequeDeposit);
    }

    selectSettingsOnCameraDialog() {
        Actions.click(this.chequeDepositScreen.navigateSettings);
    }

    enableCameraPermissionOnSettings() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.chequeDepositScreen.navigatePermissionSettings);
        Actions.click(this.chequeDepositScreen.navigatePermissionSettings);
        Actions.click(this.chequeDepositScreen.enableCameraPermissions);
    }

    shouldNotDisplayCameraPermissionDialog() {
        Actions.isVisible(this.chequeDepositScreen.cameraPopupAlert).should.equal(false);
    }

    navigateChequeScreenFromSettings() {
        Actions.click(this.chequeDepositScreen.selectBackOnSettings);
        Actions.pressAndroidBackKey();
    }

    verifyReviewDepositButtonisNotEnabled() {
        Actions.isEnabled(this.chequeDepositScreen.reviewDepositButton).should.equal(false);
    }

    verifyReviewDepositButtonisEnabled() {
        Actions.isEnabled(this.chequeDepositScreen.reviewDepositButton).should.equal(true);
    }

    verifyChequeThumbnailImages() {
        Actions.isVisible(this.chequeDepositScreen.frontOfChequeImageThumbnail).should.equal(true);
        Actions.isVisible(this.chequeDepositScreen.backOfChequeImageThumbnail).should.equal(true);
    }
}

module.exports = ChequeDepositPage;
