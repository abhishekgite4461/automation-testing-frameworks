require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class DepositChequeImagePage extends AuthPage {
    get depositChequeImageScreen() {
        return this.getNativeScreen('ics/depositChequeImage.screen');
    }

    verifyEnlargedImage() {
        Actions.isVisible(this.depositChequeImageScreen.imageCloseButton).should.equal(true);
    }

    closeChequeScreen() {
        Actions.click(this.depositChequeImageScreen.imageCloseButton);
    }
}

module.exports = DepositChequeImagePage;
