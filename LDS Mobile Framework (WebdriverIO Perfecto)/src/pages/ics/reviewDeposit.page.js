require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class ReviewDepositPage extends AuthPage {
    /**
     * @returns {ReviewDepositScreen}
     */
    get reviewDepositScreen() {
        return this.getNativeScreen('ics/reviewDeposit.screen');
    }

    verifyReviewDepositPage() {
        Actions.isVisible(this.reviewDepositScreen.title).should.equal(true);
    }

    selectThumbnailImageFront() {
        Actions.click(this.reviewDepositScreen.frontOfChequeHint);
    }

    selectThumbnailImageBack() {
        Actions.click(this.reviewDepositScreen.BackOfChequeHint);
    }

    selectReviewConfirmButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.reviewDepositScreen.reviewDepositConfirmButton);
        Actions.click(this.reviewDepositScreen.reviewDepositConfirmButton);
    }

    selectReviewCancelButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.reviewDepositScreen.reviewCancelButton);
        Actions.click(this.reviewDepositScreen.reviewCancelButton);
    }

    verifyReviewDepositField(reviewDepositFields) {
        for (let i = 0; i < reviewDepositFields.length; i++) {
            const reviewDepositPageOptions = this.reviewDepositScreen[reviewDepositFields[i].reviewDepositOptions];
            Actions.swipeUntilVisible(SwipeAction.UP, reviewDepositPageOptions);
        }
    }

    checkForPasswordConfirmation(password) {
        if (Actions.isVisible(this.reviewDepositScreen.passwordConfirmationText)) {
            Actions.setImmediateValue(this.reviewDepositScreen.passwordConfirmationText, password);
            Actions.click(this.reviewDepositScreen.passwordConfirmButton);
        }
    }

    checkForAmountMismatchConfirmation() {
        if (Actions.isExisting(this.reviewDepositScreen.amountMismatchConfirm)) {
            if (!Actions.isVisible(this.reviewDepositScreen.amountMismatchConfirm)) {
                Actions.tapOutOfField();
            }
            Actions.click(this.reviewDepositScreen.amountMismatchConfirm);
        }
    }

    verifyAmountMismatchPopUp() {
        Actions.isVisible(this.reviewDepositScreen.amountConfirmAlert).should.equal(true);
        if (!Actions.isVisible(this.reviewDepositScreen.amountMismatchConfirm)) {
            Actions.tapOutOfField();
        }
    }

    confirmAmountMismatch() {
        if (Actions.isVisible(this.reviewDepositScreen.amountMismatchConfirm)) {
            Actions.click(this.reviewDepositScreen.amountMismatchConfirm);
        }
    }

    cancelAmountMismatch() {
        Actions.click(this.reviewDepositScreen.amountMismatchCancel);
    }
}

module.exports = ReviewDepositPage;
