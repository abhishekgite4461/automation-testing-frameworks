require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class DepositHistoryReviewPage extends AuthPage {
    get depositHistoryReviewScreen() {
        return this.getNativeScreen('ics/depositHistoryReview.screen');
    }

    verifyDepositReviewFields(reviewFieldValues) {
        for (let i = 0; i < reviewFieldValues.length; i++) {
            const depositReviewField = this.depositHistoryReviewScreen[reviewFieldValues[i].depositReviewFields];
            Actions.swipeUntilVisible(SwipeAction.UP, depositReviewField);
        }
    }

    verifyHintTapImage() {
        Actions.isVisible(this.depositHistoryReviewScreen.tapHintFrontImage).should.equal(true);
    }

    selectFrontImage() {
        Actions.click(this.depositHistoryReviewScreen.tapHintFrontImage);
    }

    transactionReasonForStatusPendingCheque() {
        Actions.waitForVisible(this.depositHistoryReviewScreen.statusTitleOnReviewPage);
        Actions.getText(this.depositHistoryReviewScreen.statusTitleOnReviewPage).should.not.equal('');
    }

    transactionReasonForStatusFundsAvailableCheque() {
        Actions.waitForVisible(this.depositHistoryReviewScreen.statusTitleOnReviewPage);
        Actions.getText(this.depositHistoryReviewScreen.statusTitleOnReviewPage).should.not.equal('');
    }

    transactionReasonForStatusRejectedCheque() {
        Actions.waitForVisible(this.depositHistoryReviewScreen.statusTitleOnReviewPage);
        Actions.getText(this.depositHistoryReviewScreen.statusTitleOnReviewPage).should.not.equal('');
    }

    clickDepositHistoryButton() {
        Actions.click(this.depositHistoryReviewScreen.depositHistorybutton);
    }
}

module.exports = DepositHistoryReviewPage;
