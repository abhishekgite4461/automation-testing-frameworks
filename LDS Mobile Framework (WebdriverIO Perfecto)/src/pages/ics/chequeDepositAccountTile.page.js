const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
require('chai').should();

class ChequeDepositAccountTilePage extends AuthPage {
    get chequeDepositAccountTileScreen() {
        return this.getNativeScreen('ics/chequeDepositAccountTile.screen');
    }

    verifyEligibleAccountsInAccountTile() {
        const accountsDisplayedLocator = this.chequeDepositAccountTileScreen.accountsDisplayed;
        Actions.waitForVisible(accountsDisplayedLocator);
        const noOfAccounts = Actions.elements(accountsDisplayedLocator).value.length;
        noOfAccounts.should.not.equal(0);
    }
}

module.exports = ChequeDepositAccountTilePage;
