const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class CreditCardPdfStatementsWebViewPage extends AuthPage {
    /**
     * @returns {CreditCardPdfStatementsScreen}
     */
    get creditCardPdfStatementsScreen() {
        return this.getNativeScreen('creditCardPdfStatementsWebViewPage.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.creditCardPdfStatementsScreen.title);
    }
}

module.exports = CreditCardPdfStatementsWebViewPage;
