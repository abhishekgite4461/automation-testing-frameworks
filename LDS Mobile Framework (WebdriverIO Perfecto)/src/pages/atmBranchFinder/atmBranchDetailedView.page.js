require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class AtmBranchDetailViewPage extends AuthPage {
    /**
     * @returns {AtmBranchDetailViewScreen}
     */
    get atmBranchDetailViewScreen() {
        return this.getNativeScreen('atmBranchFinder/detailedView.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.atmBranchDetailViewScreen.title);
    }

    validateDetailedOptions() {
        Actions.isVisible(this.atmBranchDetailViewScreen.icon).should.equal(true);
        Actions.isVisible(this.atmBranchDetailViewScreen.mapAddress).should.equal(true);
        Actions.isVisible(this.atmBranchDetailViewScreen.openingHours).should.equal(true);
        Actions.isVisible(this.atmBranchDetailViewScreen.facilities).should.equal(true);
        Actions.isVisible(this.atmBranchDetailViewScreen.facilitiesTitle).should.equal(true);
        Actions.isVisible(this.atmBranchDetailViewScreen.getDirectionButton).should.equal(true);
    }

    clickMapViewIcon() {
        Actions.click(this.atmBranchDetailViewScreen.icon);
    }

    clickBackButton() {
        Actions.click(this.atmBranchDetailViewScreen.backButton);
    }
}

module.exports = AtmBranchDetailViewPage;
