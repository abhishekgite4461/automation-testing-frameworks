const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class DetailedMapViewPage extends AuthPage {
    /**
     * @returns {DetailedMapViewScreen}
     */
    get detailedMapViewScreen() {
        return this.getNativeScreen('atmBranchFinder/detailedMapView.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.detailedMapViewScreen.getDirectionButton);
    }

    clickGetDirection() {
        Actions.click(this.detailedMapViewScreen.getDirectionButton);
    }

    clickBackButton() {
        Actions.click(this.detailedMapViewScreen.backButton);
    }
}

module.exports = DetailedMapViewPage;
