require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const DeviceHelper = require('../../utils/device.helper');

// const mapSearchBoxText;

class AtmBranchFinderPage extends AuthPage {
    /**
     * @returns {AtmBranchFinderScreen}
     */
    get atmBranchFinderScreen() {
        return this.getNativeScreen('atmBranchFinder/atmBranchFinder.screen');
    }

    get atmBranchMapScreen() {
        return this.getWebScreen('atmBranchMap.screen');
    }

    isPageVisible() {
        Actions.isVisible(this.atmBranchFinderScreen.title).should.equal(true);
    }

    validateAtmBranchFinderPageOptions(tileOptions) {
        for (const element of tileOptions) {
            const tilesDisplayed = this.atmBranchFinderScreen[element];
            Actions.isVisible(tilesDisplayed).should.equal(true);
        }
    }

    selectSearchBranches() {
        Actions.click(this.atmBranchFinderScreen.searchBranchesTile);
    }

    selectFindNearbyATM() {
        Actions.click(this.atmBranchFinderScreen.findNearbyAtmTile);
    }

    verifyNoteMessage() {
        Actions.isVisible(this.atmBranchFinderScreen.noteMessage)
            .should.equal(true);
    }

    shouldSeeATMTile() {
        Actions.isVisible(this.atmBranchFinderScreen.findNearbyAtmTile).should.equal(true);
    }

    shouldNotSeeBranchTile() {
        Actions.isVisible(this.atmBranchFinderScreen.searchBranchesTile).should.equal(false);
    }

    validateMapATMBranch(mapSearch) {
        const screen = DeviceHelper.isAndroid() ? 'atmBranchFinderScreen' : 'atmBranchMapScreen';

        Actions.isVisible(this[screen].googleMapContainer);
        Actions.isVisible(this[screen].mapSearchBox);

        const mapSearchBoxText = Actions.getText(this[screen].mapSearchBox);

        if (DeviceHelper.getBrand() === 'HFX' && mapSearch !== 'ATM') {
            const validateBranch = (DeviceHelper.isAndroid() ? mapSearch.android : mapSearch.ios);
            validateBranch.should.equal(mapSearchBoxText);
        } else {
            mapSearch.should.equal(mapSearchBoxText);
        }
    }

    selectMapBackButton() {
        Actions.click(this.atmBranchFinderScreen.mapBackButton);
    }
}

module.exports = AtmBranchFinderPage;
