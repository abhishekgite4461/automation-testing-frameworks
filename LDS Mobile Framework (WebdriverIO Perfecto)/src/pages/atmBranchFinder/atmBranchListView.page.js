require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class AtmBranchListViewPage extends AuthPage {
    /**
     * @returns {AtmBranchListViewScreen}
     */
    get atmBranchListViewScreen() {
        return this.getNativeScreen('atmBranchFinder/listView.screen');
    }

    /**
     * @returns {AtmBranchScreen}
     */
    get atmBranchScreen() {
        return this.getNativeScreen('atmBranchFinder/atmBranch.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.atmBranchScreen.title);
    }

    navigateAndValidateBranchListView() {
        Actions.click(this.atmBranchScreen.branchButton);
        Actions.isVisible(this.atmBranchListViewScreen.firstList).should.equal(true);
    }

    navigateAndValidateAtmListView() {
        Actions.click(this.atmBranchScreen.atmButton);
        Actions.isVisible(this.atmBranchListViewScreen.firstList).should.equal(true);
    }

    navigateAndValidateBothListView() {
        Actions.click(this.atmBranchScreen.bothButton);
        Actions.isVisible(this.atmBranchListViewScreen.firstList).should.equal(true);
    }

    selectFirstBranchInListView() {
        Actions.click(this.atmBranchListViewScreen.firstList);
    }
}

module.exports = AtmBranchListViewPage;
