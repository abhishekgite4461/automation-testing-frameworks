const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class LeaveMobileBankingPopUpPage extends AuthPage {
    /**
     * @returns {LeaveMobileBankingPopUpScreen}
     */
    get leaveMobileBankingPopUpScreen() {
        return this.getNativeScreen('atmBranchFinder/leaveMobileBankingPopUp.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.leaveMobileBankingPopUpScreen.title);
    }

    clickOkButton() {
        Actions.click(this.leaveMobileBankingPopUpScreen.okButton);
    }
}

module.exports = LeaveMobileBankingPopUpPage;
