const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class AtmBranchFilterPage extends AuthPage {
    /**
     * @returns {AtmBranchFilterScreen}
     */
    get atmBranchFilterScreen() {
        return this.getNativeScreen('atmBranchFinder/filter.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.atmBranchFilterScreen.title);
    }

    makeFilterInBothTab() {
        Actions.click(this.atmBranchFilterScreen.title);
        Actions.click(this.atmBranchFilterScreen.openNow);
        Actions.click(this.atmBranchFilterScreen.openWeekends);
        Actions.click(this.atmBranchFilterScreen.okButton);
    }
}

module.exports = AtmBranchFilterPage;
