const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class GoogleMapPage extends AuthPage {
    /**
     * @returns {GoogleMapScreen}
     */
    get googleMapScreen() {
        return this.getNativeScreen('atmBranchFinder/googleMap.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.googleMapScreen.title);
    }

    clickBackButton() {
        Actions.click(this.googleMapScreen.backButton);
    }
}

module.exports = GoogleMapPage;
