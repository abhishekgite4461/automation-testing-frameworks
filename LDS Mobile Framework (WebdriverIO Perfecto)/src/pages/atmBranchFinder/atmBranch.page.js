require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class AtmBranchPage extends AuthPage {
    /**
     * @returns {AtmBranchScreen}
     */
    get atmBranchScreen() {
        return this.getNativeScreen('atmBranchFinder/atmBranch.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.atmBranchScreen.title);
    }

    searchSpecificLocation() {
        Actions.setImmediateValue(
            this.atmBranchScreen.searchBranch,
            this.atmBranchScreen.searchBranchLocation
        );
    }

    selectListViewButton() {
        Actions.click(this.atmBranchScreen.listViewButton);
    }

    selectFilterButton() {
        Actions.click(this.atmBranchScreen.filterButton);
    }

    enterPostCode(postCode) {
        Actions.setImmediateValue(this.atmBranchScreen.searchBranch, postCode);
    }

    validateCurrentLocation() {
        Actions.isVisible(this.atmBranchScreen.currentLocation).should.equal(true);
    }

    clickMapViewButton() {
        Actions.click(this.atmBranchScreen.mapViewButton);
    }

    clickATMBranchInMap() {
        Actions.click(this.atmBranchScreen.atmBranchInMap);
        Actions.click(this.atmBranchScreen.atmInfoIcon);
    }
}

module.exports = AtmBranchPage;
