require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class AtmBranchMapViewPage extends AuthPage {
    /**
     * @returns {AtmBranchMapViewScreen}
     */
    get atmBranchMapViewScreen() {
        return this.getNativeScreen('atmBranchFinder/mapView.screen');
    }

    /**
     * @returns {AtmBranchScreen}
     */
    get atmBranchScreen() {
        return this.getNativeScreen('atmBranchFinder/atmBranch.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.atmBranchScreen.title);
    }

    navigateAndValidateBranchMapView() {
        Actions.click(this.atmBranchScreen.branchButton);
        Actions.isVisible(this.atmBranchMapViewScreen.firstMapView).should.equal(true);
    }

    navigateAndValidateAtmMapView() {
        Actions.click(this.atmBranchScreen.atmButton);
        Actions.isVisible(this.atmBranchMapViewScreen.firstMapView).should.equal(true);
    }

    navigateAndValidateBothMapView() {
        Actions.click(this.atmBranchScreen.bothButton);
        Actions.isVisible(this.atmBranchMapViewScreen.firstMapView).should.equal(true);
    }
}

module.exports = AtmBranchMapViewPage;
