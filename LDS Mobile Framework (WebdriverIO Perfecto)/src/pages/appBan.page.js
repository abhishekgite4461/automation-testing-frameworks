const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
require('chai').should();

class AppBanPage extends AuthPage {
    /**
     * @returns {AppBanScreen}
     */
    get appBanScreen() {
        return this.getNativeScreen('appBan.screen');
    }

    waitForAppBanPageLoad() {
        Actions.waitForVisible(this.appBanScreen.title, 30000);
    }

    verifyUpdateAppButton() {
        Actions.isVisible(this.appBanScreen.updateAppButton).should.equal(true);
    }

    verifyGoToOurMobileSiteButton() {
        Actions.isVisible(this.appBanScreen.goToOurMobileSiteButton).should.equal(true);
    }

    clickUpdateAppButton() {
        Actions.click(this.appBanScreen.updateAppButton);
    }

    clickGoToOurMobileSiteButton() {
        Actions.click(this.appBanScreen.goToOurMobileSiteButton);
    }
}

module.exports = AppBanPage;
