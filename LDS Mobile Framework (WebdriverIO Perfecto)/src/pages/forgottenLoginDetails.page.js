const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class ForgottenLoginDetailsPage extends AuthPage {
    /**
     * @returns {ForgottenLoginDetailsScreen}
     */
    get forgottenLoginDetailsScreen() {
        return this.getWebScreen('preForgottenLoginDetails.screen');
    }

    waitForPageLoad() {
        Actions.waitUntilVisible([
            this.forgottenLoginDetailsScreen.title,
            this.forgottenLoginDetailsScreen.pageTitle
        ]);
    }
}

module.exports = ForgottenLoginDetailsPage;
