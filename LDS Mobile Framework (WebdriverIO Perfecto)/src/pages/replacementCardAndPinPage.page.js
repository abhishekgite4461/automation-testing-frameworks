const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');
const DeviceHelper = require('../utils/device.helper');

class ReplacementCardAndPinPage extends AuthPage {
    /**
     * @returns {ReplacementCardAndPin}
     */
    get replacementCardAndPin() {
        return this.getNativeScreen('replacementCardAndPin.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.replacementCardAndPin.title);
    }

    selectCardsAndPinOption() {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);

        if (!DeviceHelper.isMBNA()) {
            Actions.click(this.replacementCardAndPin.replaceCard);
            Actions.click(this.replacementCardAndPin.pin);
        }
        Actions.click(this.replacementCardAndPin.replaceCardCredit);
        Actions.click(this.replacementCardAndPin.pinCredit);
    }

    selectContinue() {
        Actions.click(this.replacementCardAndPin.continue);
    }

    verifyTitle() {
        Actions.isVisible(this.replacementCardAndPin.confirmTitle);
    }

    validateConfirmationOptions(pageFields) {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.isVisible(this.replacementCardAndPin[pageFields[iter]
                .fieldsInDetailScreen]);
        }
    }
}

module.exports = ReplacementCardAndPinPage;
