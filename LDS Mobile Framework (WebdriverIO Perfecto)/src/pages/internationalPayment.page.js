const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class InternationalPaymentPage extends AuthPage {
    /**
     * @returns {InternationalPaymentScreen}
     */
    get internationalPaymentScreen() {
        return this.getNativeScreen('internationalPayment.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.internationalPaymentScreen.title);
    }

    selectPaymentRecipients() {
        Actions.click(this.internationalPaymentScreen.recipients);
    }

    selectMakeIPPayment() {
        Actions.click(this.internationalPaymentScreen.makePayment);
    }

    enterIPAmount(amount) {
        Actions.swipePage(SwipeAction.UP);
        for (let i = 0; i < 10; i++) {
            Actions.swipeDownOnSmallScreen(50, 50, 20, 20);
            if (Actions.isVisible(this.internationalPaymentScreen.ipAmount)) {
                break;
            }
        }
        Actions.click(this.internationalPaymentScreen.ipAmount);
        Actions.swipePage(SwipeAction.UP);
        Actions.setValue(this.internationalPaymentScreen.ipAmountField, amount);
        Actions.tapOutOfField();
    }

    selectIPContinue() {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.internationalPaymentScreen.ipContinue);
    }

    verifyIPPaymentConfirmation() {
        Actions.waitForVisible(this.internationalPaymentScreen.confirmIPPayment, 50000);
    }

    selectTermsAndConditions() {
        Actions.swipePage(SwipeAction.UP);
        for (let i = 0; i < 10; i++) {
            Actions.swipeDownOnSmallScreen(50, 50, 20, 20);
            if (Actions.isVisible(this.internationalPaymentScreen.ipTermsAndConditions)) {
                break;
            }
        }
        Actions.click(this.internationalPaymentScreen.ipTermsAndConditions);
        Actions.swipePage(SwipeAction.UP);
    }

    enterIPPassword(password) {
        Actions.setValue(this.internationalPaymentScreen.ipPassword, password);
        Actions.tapOutOfField();
    }

    confirmIPPayment() {
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.internationalPaymentScreen.ipConfirm);
    }

    checkIPPayment() {
        Actions.waitForVisible(this.internationalPaymentScreen.ipPaymentConfirmation);
    }

    enterBICCode(BICcode) {
        Actions.swipePage(SwipeAction.UP);
        Actions.setValue(this.internationalPaymentScreen.txtBicSwift, BICcode);
        Actions.tapOutOfField();
    }

    selectContinueButton() {
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.internationalPaymentScreen.contuneButton);
    }

    enterRecipientBankAddress(recipientBankAddress) {
        Actions.waitForVisible(this.internationalPaymentScreen.recipientBankAddr);
        Actions.pause(1000);
        Actions.setValue(this.internationalPaymentScreen.recipientBankAddr, recipientBankAddress);
    }

    enterIBAN(IBAN) {
        Actions.swipePage(SwipeAction.UP);
        Actions.pause(1000);
        Actions.setValue(this.internationalPaymentScreen.IBAN, IBAN);
    }

    enterRecipientName(recipientName) {
        Actions.swipePage(SwipeAction.UP);
        Actions.pause(1000);
        Actions.setValue(this.internationalPaymentScreen.recipientName, recipientName);
    }

    enterRecipientAddress(recipientAddress) {
        Actions.swipePage(SwipeAction.UP);
        Actions.pause(1000);
        Actions.setValue(this.internationalPaymentScreen.recipientAddr, recipientAddress);
    }

    continueRecipientDetailsButton() {
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.internationalPaymentScreen.continueRecipientDetailsButton);
    }

    selectNumber() {
        Actions.waitForVisible(this.internationalPaymentScreen.authenticationPage);
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.pause(10000);
        Actions.click(this.internationalPaymentScreen.radioButton);
    }

    selectConfirmButton() {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.internationalPaymentScreen.confirmButton);
    }

    verifySuccessMessage() {
        Actions.pause(25000);
        Actions.isVisible(this.internationalPaymentScreen.successPage);
    }
}

module.exports = InternationalPaymentPage;
