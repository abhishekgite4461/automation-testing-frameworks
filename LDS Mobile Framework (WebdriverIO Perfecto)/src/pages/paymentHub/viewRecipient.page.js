const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');

class ViewRecipientPage extends AuthPage {
    /**
     * @returns {ViewRecipientScreen}
     */
    get viewRecipientScreen() {
        return this.getNativeScreen('paymentHub/viewRecipient.screen');
    }

    get searchRecipientScreen() {
        return this.getNativeScreen('transferAndPayments/searchRecipient.screen');
    }

    waitForViewRecipientPageLoad() {
        Actions.waitForVisible(this.viewRecipientScreen.title);
    }

    selectPendingPayment() {
        Actions.swipeByPercentUntilVisible(this.viewRecipientScreen.pendingPayment, 80, 80, 20, 20, 100, 10);
        Actions.waitAndClick(this.viewRecipientScreen.pendingPayment);
    }

    selectManageOnPendingPayment() {
        Actions.swipeByPercentUntilVisible(this.viewRecipientScreen.manageButtonPendingPayment,
            80, 80, 20, 20, 100, 10);
        Actions.waitUntilWithoutAssertion([this.viewRecipientScreen.manageButtonPendingPayment]);
        Actions.waitAndClick(this.viewRecipientScreen.manageButtonPendingPayment);
    }

    selectManageOnUkAccountNumber() {
        let condition;
        if (DeviceHelper.isAndroid()) {
            condition = () => Actions.isEnabled(this.viewRecipientScreen.manageButtonUKAccountNumber);
        } else {
            condition = () => Actions.isVisible(this.viewRecipientScreen.manageButtonUKAccountNumber);
        }
        Actions.swipeByPercentUntilCondition(condition, 70, 70, 30, 30, 500, 10);
        Actions.waitAndClick(this.viewRecipientScreen.manageButtonUKAccountNumber);
    }

    selectManageOnAccount() {
        if (DeviceHelper.isAndroid()) {
            Actions.waitAndClick(this.viewRecipientScreen.manageButton);
        } else {
            Actions.waitAndClick(this.viewRecipientScreen.manageButtonUKAccountNumber);
        }
    }

    selectManageOnUkMobileNumber() {
        Actions.swipeByPercentUntilVisible(this.viewRecipientScreen.manageButtonMobileNumber,
            70, 70, 30, 30, 500, 15);
        Actions.click(this.viewRecipientScreen.manageButtonMobileNumber);
    }

    verifyRecipientList() {
        Actions.waitForVisible(this.viewRecipientScreen.externalBeneficiaryAccounts);
    }

    shouldNotSeeInternationalPayment() {
        for (let i = 0; i < 3; i++) {
            Actions.swipePage(SwipeAction.UP);
            Actions.isVisible(this.viewRecipientScreen.internationalPaymentWithField)
                .should.equal(false);
        }
    }

    verifyInternationalPaymentWithIBANVisible() {
        if (this.getInternationalPaymentType('iban') === 'ELEMENT_NOT_FOUND') {
            throw new Error('International IBAN payee not found!');
        }
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.viewRecipientScreen.internationalPayment(this.getInternationalPaymentType('iban')), 0, false, 10
        );
    }

    verifyInternationalPaymentWithAccountNumberVisible() {
        if (this.getInternationalPaymentType('accountNumber') === 'ELEMENT_NOT_FOUND') {
            throw new Error('International payee with account number not found!');
        }
        Actions.swipeByPercentUntilVisible(
            this.viewRecipientScreen.internationalPayment(this.getInternationalPaymentType('accountNumber'),
                70, 70, 20, 20, 100, 15)
        );
    }

    getObjectForInternationalPayment() {
        return this.viewRecipientScreen.internationalPaymentWithField;
    }

    getInternationalPaymentType(type) {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.getObjectForInternationalPayment(), 0, false, 7
        );
        const internationalPayment = Actions.getAttributeText(this.getObjectForInternationalPayment());
        let regex;
        if (type === 'iban') {
            regex = /[a-z]/i;
        } else if (type === 'accountNumber') {
            regex = /^[0-9]+$/g;
        }
        for (const element of internationalPayment) {
            const accNum = (DeviceHelper.isAndroid() && (element.indexOf('|') !== -1)) ? element.split('| ')[1] : element;
            if (regex.test(accNum)) {
                return element;
            }
        }
        return 'ELEMENT_NOT_FOUND'; // returns if the international payment is not available for either iban and account number
    }

    selectAccount(accountName) {
        this.searchAccount(accountName);
        Actions.waitAndClick(this.searchRecipientScreen.searchedAccount(Actions.modifyAccountName(accountName, true)));
        Actions.waitForVisible(this.searchRecipientScreen.searchedAccount(Actions.modifyAccountName(accountName, true)));
    }

    selectAggAccount(toSortCode, toAggAccount) {
        Actions.waitAndClick(this.searchRecipientScreen.aggAccount(toSortCode, toAggAccount));
    }

    searchAccount(accountName) {
        const accName = Actions.modifyAccountName(accountName, true);
        Actions.waitAndClick(this.searchRecipientScreen.searchEditText);
        Actions.setImmediateValue(this.searchRecipientScreen.searchEditText, accName);
    }

    searchAccountToDelete(accountName) {
        Actions.modifyAccountName(accountName, true);
        Actions.click(this.searchRecipientScreen.searchEditText);
        Actions.setImmediateValue(this.searchRecipientScreen.searchEditText,
            SavedAppDataRepository.getData(SavedData.NEWLY_ADDED_BENEFICIARY).toUpperCase());
    }

    selectBusinessAccount(accountName, businessName) {
        const accName = Actions.modifyAccountName(accountName, true);
        Actions.click(this.searchRecipientScreen.searchEditText);
        Actions.setImmediateValue(this.searchRecipientScreen.searchEditText, accName);
        Actions.click(this.searchRecipientScreen.searchedBusinessAccount(businessName, accName));
    }

    selectPaymentRecipient(accountName) {
        this.searchAccount(accountName);
        try {
            Actions.waitAndClick(this.searchRecipientScreen.searchedAccountPayee(
                DeviceHelper.isStub() ? accountName : accountName.toUpperCase()
            ));
        } catch (e) {
            if (DeviceHelper.isEnv() && e.toString().indexOf(accountName) >= 0) {
                throw Error('TEST_DATA_ERROR');
            }
        }
    }

    selectFirstAccount() {
        Actions.waitAndClick(this.viewRecipientScreen.firstRecipientTransferCell);
    }

    static removeDuplicatesInArray(arr) {
        const uniqueArray = [];
        for (let i = 0; i < arr.length; i++) {
            if (uniqueArray.indexOf(arr[i]) === -1) {
                uniqueArray.push(arr[i]);
            }
        }
        return uniqueArray;
    }

    shouldSeePayeeInAlphabeticOrder() {
        Actions.swipeByPercentUntilVisible(
            this.viewRecipientScreen.externalBeneficiaryAccounts, 50, 80, 50, 20, 100, 8
        );
        const allPayees = Actions.getAttributeText(this.viewRecipientScreen.externalBeneficiaryAccounts, 'label');
        let actualPayees = [];
        Actions.swipePage(SwipeAction.UP);
        if (Array.isArray(allPayees)) {
            actualPayees = allPayees;
        } else {
            actualPayees = actualPayees.push(allPayees);
        }
        actualPayees = actualPayees.concat(Actions.getAttributeText(this.viewRecipientScreen.externalBeneficiaryAccounts, 'label'));
        actualPayees = ViewRecipientPage.removeDuplicatesInArray(actualPayees);
        const expectedPayees = actualPayees.sort();
        for (let i = 0; i < actualPayees.length; i++) {
            actualPayees[i].should.equal(expectedPayees[i]);
        }
    }

    static scrollToEndOfPage() {
        for (let i = 0; i < 3; i++) {
            Actions.swipePage(SwipeAction.UP);
        }
    }

    canSeeTopOfPage() {
        Actions.isExisting(this.viewRecipientScreen.myAccounts).should.equal(true);
    }

    shouldNotSeeSameRemitterAccount(businessName) {
        Actions.isExisting(this.searchRecipientScreen.searchedBusinessAccount(businessName,
            Actions.modifyAccountName(SavedAppDataRepository.getData(SavedData.DEFAULT_PRIMARY_ACCOUNT))))
            .should.equal(false);
    }

    selectIsaWithRemainingAllowanceZero(accName) {
        const accountName = this.viewRecipientScreen.zeroRemainingAllowance(
            Actions.modifyAccountName(accName, true)
        );
        Actions.click(this.searchRecipientScreen.searchEditText);
        Actions.setImmediateValue(this.searchRecipientScreen.searchEditText, accName);
        Actions.click(accountName);
    }

    verifyRecipientIsPresentInRecipientList(title) {
        const accountTitle = title.replace('Options for ', '');
        Actions.isExisting(this.viewRecipientScreen.accountNameLabel(
            Actions.modifyAccountName(accountTitle)
        )).should.equal(true);
    }

    verifyRecipientIsNotPresentInRecipientList(title) {
        const accountTitle = title.replace('Options for ', '');
        Actions.isExisting(this.viewRecipientScreen.accountNameLabel(
            Actions.modifyAccountName(accountTitle)
        )).should.equal(false);
    }

    selectManageOnRecipientName() {
        Actions.swipeByPercentUntilVisible(this.viewRecipientScreen.manageButtonRecipientName);
        Actions.click(this.viewRecipientScreen.manageButtonRecipientName);
    }
}

module.exports = ViewRecipientPage;
