const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const SwipeAction = require('../../enums/swipeAction.enum');

class ReviewPaymentPage extends AuthPage {
    /**
     * @returns {ReviewPaymentScreen}
     */
    get reviewPaymentScreen() {
        return this.getNativeScreen('paymentHub/reviewPayment.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.reviewPaymentScreen.title);
    }

    selectEditButton() {
        Actions.waitForVisible(this.reviewPaymentScreen.editButton, 3000);
        Actions.click(this.reviewPaymentScreen.editButton);
    }

    selectConfirmButton() {
        Actions.click(this.reviewPaymentScreen.confirmButton);
    }

    getFromDetail() {
        const fromAccountName = Actions.getText(this
            .reviewPaymentScreen.fromAccountNameDetail);
        const fromSortCode = Actions.getText(this.reviewPaymentScreen.fromSortCodeDetail);
        const fromAccountNumber = Actions.getText(this
            .reviewPaymentScreen.fromAccountNumberDetail);
        return `Account name is ${fromAccountName} with sort code `
            + `${fromSortCode} and account number ${fromAccountNumber}`;
    }

    getPaymentToDetail() {
        const toAccountName = Actions.getText(this.reviewPaymentScreen.toAccountNameDetail);
        const toSortCode = Actions.getText(this.reviewPaymentScreen.toSortCodeDetail);
        const toAccountNumber = Actions.getText(this
            .reviewPaymentScreen.toAccountNumberDetail);
        return `Account name is ${toAccountName} with sort code `
            + `${toSortCode} and account number ${toAccountNumber}`;
    }

    getCreditCardToDetail() {
        return `Credit card number is ${Actions.getText(this.reviewPaymentScreen.toCreditCardNumberDetail)}`;
    }

    getMortgageToDetail() {
        return Actions.getText(this.reviewPaymentScreen.toMortgageAccountDetail);
    }

    getAmountDetail(type) {
        if (DeviceHelper.isIOS()) {
            Actions.swipeUntilVisible(SwipeAction.UP, this.reviewPaymentScreen.amountDetail);
        } else {
            Actions.swipeByPercentUntilVisible(
                this.reviewPaymentScreen.amountDetail, 50, 80, 50, 30, 100, 5,
                this.reviewPaymentScreen.detailContainer
            );
        }
        if ((type === 'payment' || type === 'mortgage') && DeviceHelper.isAndroid()) {
            return Actions.getText(this.reviewPaymentScreen.amountDetail).replace(/ /g, '');
        }
        return Actions.getAttributeText(this.reviewPaymentScreen.amountDetail, 'label');
    }

    getReferenceDetail() {
        return Actions.getAttributeText(this.reviewPaymentScreen.reference, 'value');
    }

    validateAccountDetails(type) {
        const fromField = this.getFromDetail().toLowerCase();
        let toField;
        const amountField = this.getAmountDetail(type).toLowerCase();

        if (type === 'creditCard') {
            toField = this.getCreditCardToDetail().split('. .')[0];
        } else if (type === 'mortgage') {
            toField = this.getMortgageToDetail();
        } else if (type === 'paymobile') {
            toField = this.geP2PToDetails();
        } else {
            toField = this.getPaymentToDetail();
        }

        toField = toField.toLowerCase();

        SavedAppDataRepository.getData(SavedData.TRANSFER_FROM_ACCOUNT)
            .toLowerCase().should.contains(fromField);
        SavedAppDataRepository.getData(SavedData.PAYMENT_TO_ACCOUNT)
            .toLowerCase().should.contains(toField);
        SavedAppDataRepository.getData(SavedData.PAYMENT_AMOUNT)
            .toLowerCase().should.contains(amountField);
    }

    validateMortgageReferenceNumber() {
        const ReferenceNumber = this.getReferenceDetail();
        SavedAppDataRepository.getData(SavedData.ACCOUNT_REFERENCE)
            .toLowerCase().should.contains(ReferenceNumber);
    }

    setReference() {
        Actions.waitForVisible(this.reviewPaymentScreen.reference);
        const referenceValue = Actions.getText(this.reviewPaymentScreen.reference);
        SavedAppDataRepository.setData(SavedData.REFERENCE, referenceValue);
    }

    shouldSeeSubaccountInfo() {
        Actions.isVisible(this.reviewPaymentScreen.subAccountInfo).should.equal(true);
        const referenceDetail = this.getReferenceDetail();
        referenceDetail.substr(12, 15);
        const subAccountNumber = Actions.getAttribute(this.reviewPaymentScreen.subAccountInfo, 'name').substr(12, 15);
        referenceDetail.should.equal(subAccountNumber);
    }

    verifyReviewPaymentPage() {
        Actions.waitForVisible(this.reviewPaymentScreen.title);
    }

    shouldContainAmountValue(amount) {
        let obtainedAmount = Actions.getAttributeText(this.reviewPaymentScreen.amountDetail, 'value');
        if (obtainedAmount.includes(',')) {
            obtainedAmount = obtainedAmount.replace(/,/g, '');
        }
        obtainedAmount.should.contains(amount);
    }

    shouldSeeWithdrawFromIsaToNonLbgAccountWarningMessage() {
        Actions.isExisting(this.reviewPaymentScreen.withdrawFromIsaToNonLbgAccountWarningMessage).should.equal(true);
    }

    selectISATransferAgreementCheckBox() {
        Actions.click(this.reviewPaymentScreen.isaTransferAgreementCheckBox);
    }

    scrollToISATransferCheckBox() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.reviewPaymentScreen.isaTransferAgreementCheckBox);
    }

    shouldSeeDateValue(date) {
        const fullDate = `${date}`;
        Actions.isExisting(this.reviewPaymentScreen.dateValue(fullDate)).should.equal(true);
    }

    shouldSeeFuturePaymentMessage() {
        Actions.isExisting(this.reviewPaymentScreen.futurePaymentMessage).should.equal(true);
    }

    geP2PToDetails() {
        const toAccountName = Actions.getText(this.reviewPaymentScreen.toP2PAccountNameDetail);
        const toMobileNumber = Actions.getText(this.reviewPaymentScreen.toMobileNum);
        return `Account name is ${toAccountName} and Mobile number `
            + `${toMobileNumber}`;
    }

    verifyMPAPaymentMessage() {
        Actions.isExisting(this.reviewPaymentScreen.mpaPaymentApprovalRequiredMessage).should.equal(true);
    }

    verifyNoMPAPaymentMessage() {
        Actions.isExisting(this.reviewPaymentScreen.mpaPaymentApprovalRequiredMessage).should.equal(false);
    }
}

module.exports = ReviewPaymentPage;
