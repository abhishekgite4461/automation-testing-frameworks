const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');

class ViewRemitterPage extends AuthPage {
    /**
     * @returns {ViewRemitterScreen}
     */
    get viewRemitterScreen() {
        return this.getNativeScreen('paymentHub/viewRemitter.screen');
    }

    waitForViewRemitterPageLoad() {
        if (DeviceHelper.isBNGA() && DeviceHelper.isAndroid()) {
            Actions.waitForVisible(this.viewRemitterScreen.titleBNGA);
        } else {
            Actions.waitForVisible(this.viewRemitterScreen.title);
        }
    }

    viewListOfAccounts(type, listOfAllAccounts) {
        const allAvailableAccountsInViewRemitterPage = Actions.getAttributeText(this.viewRemitterScreen.accountName, 'label');
        const ListOfAllAccounts = listOfAllAccounts.map((account) => Actions.modifyAccountName(account, true));

        for (const account of ListOfAllAccounts) {
            if (account === undefined) throw new Error('undefined parameter');
            if (type === 'see') {
                Actions.swipeByPercentUntilVisible(this.viewRemitterScreen.accountNameLabel(account), 80, 70, 20, 30);
            } else {
                allAvailableAccountsInViewRemitterPage.indexOf(account).should.equal(-1);
            }
        }
    }

    selectAccount(accountHeaderName) {
        const accountName = this.viewRemitterScreen.accountNameLabel(
            Actions.modifyAccountName(accountHeaderName, true)
        );
        Actions.swipeByPercentUntilVisible(accountName, 80, 85, 20, 25, 200);
        Actions.waitAndClick(accountName);
    }

    selectAggAccount(sortCode, aggAccountNumber) {
        const aggregatedAccount = this.viewRemitterScreen.aggAccount(sortCode, aggAccountNumber);
        Actions.swipeByPercentUntilVisible(aggregatedAccount, 80, 85, 20, 25, 200);
        Actions.waitAndClick(aggregatedAccount);
    }

    selectedAggAccount(fromExtProvider, fromAggAccount) {
        const aggAccountName = this.viewRemitterScreen.aggAccount(fromExtProvider, fromAggAccount);
        Actions.isVisible(aggAccountName).should.equal(true);
    }

    selectBusinessAccount(accountHeaderName, businessName) {
        const accountName = Actions.modifyAccountName(accountHeaderName, true);
        Actions.click(this.viewRemitterScreen.searchedBusinessAccount(businessName, accountName));
    }
}

module.exports = ViewRemitterPage;
