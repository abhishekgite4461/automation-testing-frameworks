// This is the landing page after selecting the transfer and payment option
// from global/home/action menu
require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SavedAppDataRepository = require('../../data/savedAppData.repository.js');
const SavedData = require('../../enums/savedAppData.enum.js');

class ViewPaymentLimitsPage extends AuthPage {
    /**
     * @returns {ViewPaymentLimitsScreen}
     */
    get viewPaymentLimitsScreen() {
        return this.getNativeScreen('paymentHub/viewPaymentLimits.screen');
    }

    validateAccountDailyLimits(accountDailyLimits) {
        Actions.waitForVisible(this.viewPaymentLimitsScreen.onlineMobileBanking);
        for (let i = 0; i < accountDailyLimits.length; i++) {
            const individualAccountLimits = this.viewPaymentLimitsScreen[accountDailyLimits[i].detail];
            Actions.isVisible(individualAccountLimits).should.equal(true);
        }
        const paymentLimitLeft = Actions.getAttributeText(this.viewPaymentLimitsScreen.paymentLimitLeftToday, 'label')
            .match(/£(.+).\d{2}/g)[0].slice(1).replace(/,/g, '');
        SavedAppDataRepository.setData(SavedData.DAILY_PAYMENT_LIMIT_BEFORE_TRANSACTION, paymentLimitLeft);
    }

    validateDailyPaymentLimitAfterTransaction(amount) {
        const amountAfterTransaction = Actions.getAttributeText(this.viewPaymentLimitsScreen.paymentLimitLeftToday, 'label')
            .match(/£(.+).\d{2}/g)[0].slice(1).replace(/,/g, '');
        const amountBeforeTransaction = (
            SavedAppDataRepository.getData(SavedData.DAILY_PAYMENT_LIMIT_BEFORE_TRANSACTION));
        String(amountAfterTransaction).should
            .equal(String(amountBeforeTransaction - parseFloat(amount)));
    }
}

module.exports = ViewPaymentLimitsPage;
