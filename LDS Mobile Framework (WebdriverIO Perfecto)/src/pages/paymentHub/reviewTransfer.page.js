const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');

class ReviewTransferPage extends AuthPage {
    /**
     * @returns {ReviewTransferScreen}
     */
    get reviewTransferScreen() {
        return this.getNativeScreen('paymentHub/reviewTransfer.screen');
    }

    waitForPageLoad() {
        Actions.waitUntilVisible([this.reviewTransferScreen.title]);
    }

    selectEditButton() {
        Actions.click(this.reviewTransferScreen.editButton);
    }

    selectConfirmButton() {
        Actions.click(this.reviewTransferScreen.confirmButton);
        Actions.reportTimer('Payment_Success', 'Success');
    }

    getFromDetail() {
        if (DeviceHelper.isAndroid()) {
            const fromAccountName = Actions.getText(this
                .reviewTransferScreen.fromAccountNameDetail);
            const fromSortCode = Actions.getText(this.reviewTransferScreen.fromSortCodeDetail);
            const fromAccountNumber = Actions.getText(this
                .reviewTransferScreen.fromAccountNumberDetail);
            return `Account name is ${fromAccountName} with sort code `
                + `${fromSortCode} and account number ${fromAccountNumber}`;
        }
        return Actions.getAttributeText(this.reviewTransferScreen.fromDetail, 'label');
    }

    getToDetail() {
        if (DeviceHelper.isAndroid()) {
            const toAccountName = Actions.getText(this.reviewTransferScreen.toAccountNameDetail);
            const toSortCode = Actions.getText(this.reviewTransferScreen.toSortCodeDetail);
            const toAccountNumber = Actions.getText(this
                .reviewTransferScreen.toAccountNumberDetail);
            return `Account name is ${toAccountName} with sort code `
                + `${toSortCode} and account number ${toAccountNumber}`;
        }
        return Actions.getAttributeText(this.reviewTransferScreen.toDetail, 'label');
    }

    getAmountDetail() {
        return Actions.getAttributeText(this.reviewTransferScreen.amountDetail, 'label');
    }

    validateAccountDetails() {
        const regex = 'Details for this account are.'; // to extract only the account details from the accessibility text
        let fromField;
        let toField;
        const amountField = this.getAmountDetail().toLowerCase();

        if (DeviceHelper.isIOS()) {
            fromField = this.getFromDetail().split(regex)[1].split('. . Available funds.')[0].toLowerCase();
            toField = this.getToDetail().split(regex)[1].split('. .')[0].toLowerCase();
        } else {
            fromField = this.getFromDetail().toLowerCase();
            toField = this.getToDetail().toLowerCase();
        }

        SavedAppDataRepository.getData(SavedData.TRANSFER_FROM_ACCOUNT)
            .toLowerCase().should.contains(fromField);
        SavedAppDataRepository.getData(SavedData.TRANSFER_TO_ACCOUNT)
            .toLowerCase().should.contains(toField);
        SavedAppDataRepository.getData(SavedData.TRANSFER_AMOUNT)
            .toLowerCase().should.contains(amountField);
    }

    verifyReferenceFieldInReviewTransferPage() {
        Actions.isVisible(this.reviewTransferScreen.reference).should.equal(true);
    }

    shouldSeeHtbIsaWarningMessage() {
        Actions.isExisting(this.reviewTransferScreen.htbIsaWarningMessage).should.equal(true);
    }

    shouldSeeTopupIsaWarningMessage() {
        Actions.isExisting(this.reviewTransferScreen.topupIsaWarningMessage).should.equal(true);
    }

    shouldSeeWithdrawFromIsaToNonIsaAccountWarningMessage() {
        Actions.isExisting(
            this.reviewTransferScreen.withdrawFromIsaToNonIsaAccountWarningMessage
        ).should.equal(true);
    }

    shouldSeeWithdrawFromIsaToIsaAccountWarningMessage() {
        Actions.isExisting(
            this.reviewTransferScreen.withdrawFromIsaToIsaAccountWarningMessage
        ).should.equal(true);
    }

    shouldSeeConfirmButtonDisabled() {
        Actions.isEnabled(this.reviewTransferScreen.confirmButton).should.equal(false);
    }

    verifyMPATransferMessage() {
        Actions.isExisting(this.reviewTransferScreen.mpaTransferApprovalRequiredMessage).should.equal(true);
    }
}

module.exports = ReviewTransferPage;
