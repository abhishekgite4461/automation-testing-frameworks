// This is the landing page after selecting the transfer and payment option
// from global/home/action menu
require('chai').should();
const DeviceHelper = require('../../utils/device.helper');
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const SwipeAction = require('../../enums/swipeAction.enum');
const CommonPage = require('../common.page');

const commonPage = new CommonPage();

class PaymentHubMainPage extends AuthPage {
    /**
     * @returns {PaymentHubMainScreen}
     */
    get paymentHubMainScreen() {
        return this.getNativeScreen('paymentHub/paymentHubMain.screen');
    }

    /**
     * @returns {ReviewPaymentScreen}
     */
    get reviewPaymentScreen() {
        return this.getNativeScreen('paymentHub/reviewPayment.screen');
    }

    waitForPaymentHubPageLoad() {
        Actions.iOSObjectTreeOptimizationStart();
        commonPage.waitForSpinnerToComplete();
        Actions.waitForVisible(this.paymentHubMainScreen.title);
        Actions.iOSObjectTreeOptimizationStop();
    }

    verifyPaymentHubUnavailable() {
        Actions.waitForVisible(this.paymentHubMainScreen.paymentsHubUnavailable);
    }

    checkRecipientPrePopulation(recipientType, accountHeaderName) {
        let accountName = accountHeaderName;

        if (accountHeaderName === 'defaultPrimary') {
            accountName = SavedAppDataRepository.getData(SavedData.DEFAULT_PRIMARY_ACCOUNT).toLowerCase();
        }

        if (recipientType === 'from') {
            accountName.toLowerCase().should.contains(Actions.getAttributeText(
                this.paymentHubMainScreen.fromAccountName
            ).toLowerCase());
        } else if (recipientType === 'to' && accountHeaderName === 'noAccount') {
            Actions.isVisible(this.paymentHubMainScreen.toAccount)
                .should.equal(true);
        } else {
            Actions.getAttributeText(this.paymentHubMainScreen.toAccountName).toLowerCase()
                .should.contains(accountName.toLowerCase());
        }
    }

    checkRecipientPrePopulationForP2p(recipientType, accountName) {
        Actions.getAttributeText(this.paymentHubMainScreen.p2pToAccountNameDetail)
            .toLowerCase().should.contains(accountName.toLowerCase());
    }

    selectPayAccount(type) {
        Actions.iOSObjectTreeOptimizationStart();
        if (type === 'remitter') {
            Actions.waitAndClick(this.paymentHubMainScreen.fromAccountPane);
        } else {
            Actions.waitAndClick(this.paymentHubMainScreen.toAccountPane);
        }
        if (DeviceHelper.isAndroid() && DeviceHelper.osVersion().includes('5.0')) {
            Actions.tapByLocation('50%,1%');
        }
        Actions.pause(4000); // Adding wait for payment hub to load
        Actions.iOSObjectTreeOptimizationStop();
    }

    chooseRemitterAccount(accountName) {
        const modifiedAccountName = Actions.modifyAccountName(accountName, true);
        Actions.swipeByPercentUntilVisible(
            this.paymentHubMainScreen.paymentAccountLocator(modifiedAccountName), 50, 80, 50, 20, 100, 25
        );
        Actions.click(this.paymentHubMainScreen.paymentAccountLocator(modifiedAccountName));
    }

    enterAmount(amount) {
        Actions.waitForVisible(this.paymentHubMainScreen.amount, 30000);
        if (!DeviceHelper.isPerfecto() && DeviceHelper.isAndroid()) {
            Actions.click(this.paymentHubMainScreen.amount);
            Actions.setMICustomKeyoard(amount);
            Actions.tapOutOfField();
        } else if (!DeviceHelper.isPerfecto() && DeviceHelper.isIOS()) {
            Actions.genericSendKeys(this.paymentHubMainScreen.amount, amount);
        } else if (amount.includes('.')) {
            if (DeviceHelper.isAndroid()) {
                Actions.genericSendKeys(this.paymentHubMainScreen.amount, amount);
            } else {
                Actions.setValue(this.paymentHubMainScreen.amount, amount);
            }
        } else {
            Actions.setValue(this.paymentHubMainScreen.amount, amount);
        }
        PaymentHubMainPage.hidePayKeyboard();
    }

    static hidePayKeyboard() {
        if (DeviceHelper.isAndroid()) {
            Actions.hideDeviceKeyboard();
        } else {
            Actions.tapOutOfField();
        }
    }

    enterReference(reference) {
        Actions.clearText(this.paymentHubMainScreen.reference);
        Actions.sendKeysAndTapOut(this.paymentHubMainScreen.reference, reference);
        PaymentHubMainPage.hidePayKeyboard();
    }

    clearReference() {
        Actions.clearText(this.paymentHubMainScreen.reference);
    }

    verifyFasterPaymentWarning() {
        Actions.isVisible(this.paymentHubMainScreen.fasterPaymetWarning);
    }

    verifyFasterPaymentWarningPopUp() {
        Actions.isVisible(this.paymentHubMainScreen.fasterPaymetWarningPopUp);
    }

    verifyFasterPaymentWarningStatusLink() {
        Actions.isVisible(this.paymentHubMainScreen.statusLink);
    }

    selectFasterPaymentWarningStatusLink() {
        Actions.click(this.paymentHubMainScreen.statusLink);
    }

    selectContinueButton() {
        Actions.swipeByPercentUntilVisible(this.paymentHubMainScreen.continueButton);
        Actions.click(this.paymentHubMainScreen.continueButton);
        Actions.reportTimer('Transfer_Success', 'Confirm');
    }

    selectViewPaymentLimitsLink() {
        Actions.click(this.paymentHubMainScreen.viewPaymentLimits);
    }

    amountFieldVisibility(visibility) {
        Actions.isVisible(this.paymentHubMainScreen.amount).should.equal(visibility);
    }

    paymentAmountFieldVisibility(visibility) {
        Actions.isVisible(this.paymentHubMainScreen.amount).should.equal(visibility);
    }

    paymentDateVisibility(visibility) {
        Actions.isVisible(this.paymentHubMainScreen.date).should.equal(visibility);
    }

    statementBalanceCreditCardVisibility(visibility) {
        Actions.isVisible(this.paymentHubMainScreen.statementBalanceCreditCard).should.equal(visibility);
    }

    minimumAmountCreditCardVisibility(visibility) {
        Actions.isVisible(this.paymentHubMainScreen.minimumAmountCreditCard).should.equal(visibility);
    }

    standingOrderSwitchVisibility(visibility) {
        Actions.isVisible(this.paymentHubMainScreen.standingOrderToggleSwitch)
            .should.equal(visibility);
    }

    validateReference() {
        const referenceValue = Actions.getText(this.paymentHubMainScreen.reference);
        SavedAppDataRepository.getData(SavedData.REFERENCE).should.equal(referenceValue);
    }

    selectAmountField() {
        Actions.click(this.paymentHubMainScreen.amount);
    }

    selectPaymentAmountField() {
        Actions.click(this.paymentHubMainScreen.amount);
    }

    helpTextForAmountField() {
        if (DeviceHelper.isAndroid()) {
            Actions.hideDeviceKeyboard();
        }
        Actions.isVisible(this.paymentHubMainScreen.helpTextAmount).should.equal(true);
    }

    additionalOptionCreditCard() {
        Actions.click(this.paymentHubMainScreen.additionalOptionCreditCard);
    }

    continueButtonEnability(enability) {
        if (DeviceHelper.isAndroid()) {
            Actions.hideDeviceKeyboard();
        }
        Actions.swipeUntilVisible(SwipeAction.UP, this.paymentHubMainScreen.continueButton);
        Actions.isEnabled(this.paymentHubMainScreen.continueButton).should.equal(enability);
    }

    shouldSeeOnlyTwoDecimalPlacesForAmount() {
        Actions.getAttributeText(this.paymentHubMainScreen.amount, 'value').split('.')[1].length.should.equal(2);
    }

    shouldSeeOnlyTwoDecimalPlacesForPaymentAmount() {
        Actions.getAttributeText(this.paymentHubMainScreen.amount, 'value').split('.')[1].length.should.equal(2);
    }

    shouldSeeOnlyEighteenCharForReference() {
        Actions.getAttributeText(this.paymentHubMainScreen.reference, 'value').length.should.equal(18);
    }

    shouldSeeReferenceFieldWithText(reference) {
        Actions.getAttributeText(this.paymentHubMainScreen.reference, 'value').should.equal(reference);
    }

    referenceField() {
        Actions.isVisible(this.paymentHubMainScreen.reference).should.equal(true);
    }

    selectLeaveInWinback() {
        Actions.click(this.commonScreen.winBackLeaveButton);
    }

    selectStayInWinback() {
        Actions.click(this.commonScreen.winBackStayButton);
    }

    getRemitterAmount() {
        const locator = this.paymentHubMainScreen.remitterAmount(DeviceHelper.getAppType());
        const amount = DeviceHelper.isIOS()
            ? Actions.getAttributeText(locator, 'value')
            : Actions.getText(locator);
        if (amount.includes('N/A')) {
            throw Error('Not_Available_Balance');
        } else {
            return amount.match(/£(.+).\d{2}/g)[0].slice(1).replace(/,/g, '');
        }
    }

    getOverDraftAmountIfPresent() {
        let overdraft = 0;
        const overdraftLocator = this.paymentHubMainScreen.overDraftAmount(DeviceHelper.getAppType());
        const remitterLocator = this.paymentHubMainScreen.remitterAmount(DeviceHelper.getAppType());
        if (DeviceHelper.isAndroid() && Actions.isVisible(overdraftLocator)) {
            overdraft = Actions.getText(overdraftLocator).match(/£(.+).\d{2}/g)[0].slice(1).replace(/,/g, '');
        } else if (DeviceHelper.isIOS() && Actions.getAttribute(remitterLocator, 'value').includes('Overdraft')) {
            overdraft = Actions.getAttribute(remitterLocator, 'value').match(/£[0-9.,]*\d{2}/g)[1].slice(1).replace(',', '');
        }
        return overdraft;
    }

    getAggRemitterAmount() {
        const amount = DeviceHelper.isIOS()
            ? Actions.getAttributeText(this.paymentHubMainScreen.remitterAmount, 'value')
            : Actions.getText(this.paymentHubMainScreen.aggRemitterAmount);
        return amount.match('£(([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9]))')[1].replace(/,/g, '');
    }

    getRecipientAmount() {
        const locator = this.paymentHubMainScreen.recipientAmount(DeviceHelper.getAppType());
        const amount = DeviceHelper.isIOS()
            ? Actions.getAttributeText(locator, 'value')
            : Actions.getText(locator);
        if (amount.includes('N/A')) {
            throw Error('Not_Available_Balance');
        } else {
            return amount.match(/£(.+).\d{2}/g)[0].slice(1).replace(/,/g, '');
        }
    }

    checkAvailableBalance() {
        const AvailableBalance = DeviceHelper.isIOS()
            ? Actions.getAttributeText(this.paymentHubMainScreen.remitterAmount(), 'value').match(/\(([^)]+)\)/)[0]
            : Actions.getText(this.reviewPaymentScreen.remitterAmount);
        return AvailableBalance.match(/Available:\s£(.*)\)/i)[1].replace(/,/g, '');
    }

    getCreditCardAmount() {
        let amount;
        if (DeviceHelper.isIOS()) {
            amount = Actions.getAttributeText(this.paymentHubMainScreen.recipientAmount(), 'value')
                .split('£')[1].split('(Available credit:')[0].split('.')[0].replace(/,/g, '');
        } else {
            amount = Actions.getText(this.paymentHubMainScreen.creditCardAmount)
                .split('£')[1].split('.')[0].replace(/,/g, '');
        }
        return amount;
    }

    shouldSeeNoticeWarningMessage() {
        Actions.isVisible(this.paymentHubMainScreen.noticeWarningMessage).should.equal(true);
    }

    shouldSeeInsufficientMoneyErrorMessage() {
        Actions.isVisible(this.paymentHubMainScreen.insufficientMoneyErrorMessage)
            .should.equal(true);
    }

    waitForFraudsterWarningMessage() {
        Actions.waitForVisible(this.paymentHubMainScreen.fraudsterWarning);
    }

    validate60MinutesLapseErrorMessage() {
        Actions.isVisible(this.paymentHubMainScreen.timeLapseWarningErrorMessage);
    }

    validate60MinutesLapseWithOverPaymentErrorMessage() {
        Actions.isVisible(this.paymentHubMainScreen.timeLapseWithOverPaymentWarningErrorMessage);
    }

    continueFraudsterWarningIfPresent() {
        if (DeviceHelper.isRNGA()) {
            if (Actions.isVisible(this.paymentHubMainScreen.fraudsterWarning)) {
                Actions.click(this.commonScreen.winBackLeaveButton);
            }
        }
    }

    closeErrorMessage() {
        if (Actions.isVisible(this.paymentHubMainScreen.closeErrorMessage)) {
            Actions.click(this.paymentHubMainScreen.closeErrorMessage);
        }
    }

    getFromDetail() {
        const fromAccountName = Actions.getAttributeText(this
            .paymentHubMainScreen.fromAccountNameDetail, 'label');
        const fromSortCode = Actions.getAttributeText(this.paymentHubMainScreen.fromSortCodeDetail, 'label');
        const fromAccountNumber = Actions.getAttributeText(this
            .paymentHubMainScreen.fromAccountNumberDetail, 'label');
        return `Account name is ${fromAccountName} with sort code `
            + `${fromSortCode} and account number ${fromAccountNumber}`;
    }

    getToDetail() {
        const toAccountName = Actions.getAttributeText(this.paymentHubMainScreen.toAccountNameDetail, 'label');
        const toSortCode = Actions.getAttributeText(this.paymentHubMainScreen.toSortCodeDetail, 'label');
        const toAccountNumber = Actions.getAttributeText(this
            .paymentHubMainScreen.toAccountNumberDetail, 'label');
        return `Account name is ${toAccountName} with sort code `
            + `${toSortCode} and account number ${toAccountNumber}`;
    }

    getPaymentToDetail() {
        const toAccountName = Actions.getText(this.paymentHubMainScreen.paymentAccountNameDetail);
        const toSortCode = Actions.getText(this.paymentHubMainScreen.paymentSortCodeDetail);
        const toAccountNumber = Actions.getText(this
            .paymentHubMainScreen.paymentAccountNumberDetail);
        return `Account name is ${toAccountName} with sort code `
            + `${toSortCode} and account number ${toAccountNumber}`;
    }

    getCreditCardToDetail() {
        return `Credit card number is ${Actions.getText(this.paymentHubMainScreen.toCreditCardNumberDetail)}`;
    }

    getMortgageToDetail() {
        return Actions.getText(this.paymentHubMainScreen.toMortgageAccountDetail);
    }

    getReferenceDetail() {
        return Actions.getAttributeText(this.paymentHubMainScreen.reference, 'value');
    }

    getAmountDetail() {
        return Actions.getAttributeText(this.paymentHubMainScreen.amount, 'value');
    }

    getPaymentAmountDetail() {
        return Actions.getAttributeText(this.paymentHubMainScreen.amount, 'value');
    }

    getRemainingAllowanceForToIsa() {
        if (DeviceHelper.isAndroid()) {
            return Actions.getText(this.paymentHubMainScreen.recipientAmount()).replace(/\s/g, '').split('(Remainingallowance:£')[1].split(')')[0].replace(/,/g, '');
        }
        return Actions.getText(this.paymentHubMainScreen.recipientAmount()).replace(/\s/g, '').split('(Remainingallowance:£')[1].split(')')[0].replace(/,/g, '');
    }

    getRemainingAllowanceForFromIsa() {
        if (DeviceHelper.isAndroid()) {
            return (Actions.getText(this.paymentHubMainScreen.remitterAmount())).replace(/\s/g, '').split('(Remainingallowance:£')[1].split(')')[0].replace(/,/g, '');
        }
        return Actions.getText(this.paymentHubMainScreen.remitterAmount()).replace(/\s/g, '').split('(Remainingallowance:£')[1].split(')')[0].replace(/,/g, '');
    }

    setAccountDetailsForTransfer() {
        this.setRemitterDetails();
        this.setRecipientDetails();
        this.setTransferAmount();
    }

    setTransferAmount() {
        SavedAppDataRepository.setData(SavedData.TRANSFER_AMOUNT,
            this.getAmountDetail());
    }

    setRemitterDetails() {
        SavedAppDataRepository.setData(SavedData.TRANSFER_REMITTER_AMOUNT,
            this.getRemitterAmount());
        SavedAppDataRepository.setData(SavedData.TRANSFER_FROM_ACCOUNT,
            this.getFromDetail());
    }

    setRecipientDetails() {
        SavedAppDataRepository.setData(SavedData.TRANSFER_RECIPIENT_AMOUNT,
            this.getRecipientAmount());
        SavedAppDataRepository.setData(SavedData.TRANSFER_TO_ACCOUNT,
            this.getToDetail());
    }

    setRemainingAllowanceForToIsa() {
        SavedAppDataRepository.setData(SavedData.REMAINING_ALLOWANCE_TO_ACCOUNT,
            this.getRemainingAllowanceForToIsa());
    }

    setRemainingAllowanceForFromIsa() {
        SavedAppDataRepository.setData(SavedData.REMAINING_ALLOWANCE_FROM_ACCOUNT,
            this.getRemainingAllowanceForFromIsa());
    }

    setAccountDetailsForPayment(type) {
        let toDetail;
        if (type === 'creditCard') {
            toDetail = this.getCreditCardToDetail();
        } else if (type === 'paymobile') {
            toDetail = this.getP2PToDetail();
        } else if (type === 'mortgage') {
            toDetail = this.getMortgageToDetail();
        } else {
            toDetail = this.getPaymentToDetail();
        }
        SavedAppDataRepository.setData(SavedData.TRANSFER_FROM_ACCOUNT,
            this.getFromDetail());
        SavedAppDataRepository.setData(SavedData.PAYMENT_TO_ACCOUNT,
            toDetail);
        SavedAppDataRepository.setData(SavedData.PAYMENT_AMOUNT,
            this.getPaymentAmountDetail());
    }

    setMortgageReferenceNumber() {
        Actions.swipeByPercentUntilVisible(this.paymentHubMainScreen.reference);
        SavedAppDataRepository.setData(SavedData.ACCOUNT_REFERENCE,
            this.getReferenceDetail());
    }

    verifyAmountValue(amount, type = 'see') {
        let obtainedAmount = Actions.getAttributeText(this.paymentHubMainScreen.amount, 'value');
        if (obtainedAmount.includes(',')) {
            obtainedAmount = obtainedAmount.replace(/,/g, '');
        }
        if (type === 'see') {
            obtainedAmount.should.contains(amount);
        } else {
            obtainedAmount.should.not.contains(amount);
        }
    }

    shouldContainAmountValueForPayments(amount) {
        Actions.getAttributeText(this.paymentHubMainScreen.amount, 'value').should.contains(amount);
    }

    referenceFieldNotEnabled() {
        Actions.isEnabled(this.paymentHubMainScreen.reference).should.equal(false);
    }

    selectReferenceToolTip() {
        Actions.click(this.paymentHubMainScreen.toolTipForCorporatePayee);
    }

    closeReferenceToolTip() {
        Actions.click(this.commonScreen.winBackCloseButton);
    }

    shouldSeeInternetTransactionLimitMessage() {
        if (Actions.isVisible(this.paymentHubMainScreen.fraudsterWarning)) {
            Actions.click(this.commonScreen.winBackOkButton);
        }
        Actions.waitForVisible(this.paymentHubMainScreen.internetTransactionLimitMessage);
        Actions.click(this.paymentHubMainScreen.closeErrorMessage);
    }

    shouldSeeAccountDailyLimitMessage() {
        Actions.isVisible(this.paymentHubMainScreen.accountDailyLimitMessage).should.equal(true);
        Actions.click(this.paymentHubMainScreen.closeErrorMessage);
    }

    shouldSeeCbsProhibitiveMessage() {
        Actions.waitForVisible(this.paymentHubMainScreen.cbsProhibitiveMessage);
        Actions.click(this.paymentHubMainScreen.closeErrorMessage);
    }

    shouldSeeMonthlyCapLimitMessage() {
        Actions.isVisible(this.paymentHubMainScreen.monthlyCapLimitMessage).should.equal(true);
        Actions.click(this.paymentHubMainScreen.closeErrorMessage);
    }

    selectHeaderHomeButton() {
        Actions.click(this.paymentHubMainScreen.headerHomeButton);
    }

    verifyAddBeneficiary() {
        Actions.waitForVisible(this.paymentHubMainScreen.toAccountPane);
    }

    verifyErrorMessage() {
        Actions.isExisting(this.paymentHubMainScreen.errorMessage).should.equal(true);
    }

    verifyPaymentHubMainPage() {
        Actions.isVisible(this.paymentHubMainScreen.title).should.equal(true);
    }

    shouldSeePaymentHubMainPageWithSCA() {
        Actions.isVisible(this.paymentHubMainScreen.amount).should.equal(true);
    }

    navigateBackToHomePage() {
        Actions.click(this.paymentHubMainScreen.headerHomeButton);
    }

    verifyMinimumAndMaximumLimtText() {
        Actions.isVisible(this.paymentHubMainScreen.toolTipForMinimumAndMaximumDailyLimit).should.equal(true);
    }

    verifyErrorMessageForMinimumAndMaxmimumDailyLimit() {
        Actions.isVisible(this.paymentHubMainScreen.errorMessageForMinimumAndMaximumDailyLimit).should.equal(true);
    }

    selectReferenceField() {
        Actions.click(this.paymentHubMainScreen.reference);
    }

    verifyTextAdvisingReferenceWillAppearOnStatements() {
        Actions.isVisible(this.paymentHubMainScreen.toolTipForReferenceOnPaymentHub).should.equal(true);
    }

    verifyMinISATransferErrorMessage() {
        Actions.isVisible(this.paymentHubMainScreen.minISATransferErrorMessage).should.equal(true);
        Actions.click(this.paymentHubMainScreen.closeErrorMessage);
    }

    verifyMaxISATransferErrorMessage() {
        Actions.isVisible(this.paymentHubMainScreen.maxISATransferErrorMessage).should.equal(true);
        Actions.click(this.paymentHubMainScreen.closeErrorMessage);
    }

    clearAmountField() {
        Actions.clearText(this.paymentHubMainScreen.amount);
    }

    selectToAccountPane() {
        Actions.click(this.paymentHubMainScreen.toAccountPane);
    }

    selectCalendarPicker() {
        Actions.click(this.paymentHubMainScreen.date);
    }

    static getFuturePaymentDate() {
        const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
        ];

        const futurePaymentDate = SavedAppDataRepository.getData(SavedData.FUTURE_PAYMENT_DATE);
        const date = futurePaymentDate.getDate();
        const month = monthNames[futurePaymentDate.getMonth()];
        const year = futurePaymentDate.getFullYear();
        const twoDigitDate = PaymentHubMainPage.getTwoDigitDateFormat(date);

        return `${twoDigitDate} ${month} ${year}`;
    }

    static getTwoDigitDateFormat(date) {
        return (date < 10) ? `0${date}` : `${date}`;
    }

    shouldSeeDatePopulated() {
        const fullDate = PaymentHubMainPage.getFuturePaymentDate();
        Actions.isVisible(this.paymentHubMainScreen.paymentDate(fullDate)).should.equal(true);
    }

    shouldSeeTomorrowPopulated(date) {
        Actions.isVisible(this.paymentHubMainScreen.paymentDate(date)).should.equal(true);
    }

    statementBalanceWithInstalmentPlanVisibility(visibility) {
        Actions.isExisting(this.paymentHubMainScreen.statementBalanceWithInstalmentPlanCreditCard)
            .should.equal(visibility);
    }

    minimumAmountWithInstalmentPlanVisibility(visibility) {
        Actions.isExisting(this.paymentHubMainScreen.minimumAmountWithInstalmentPlanCreditCard)
            .should.equal(visibility);
    }

    verifyISARemainingAllowance(type, amtValue) {
        if (type === 'to') {
            const toRemainingAllowance = SavedAppDataRepository.getData(SavedData.REMAINING_ALLOWANCE_TO_ACCOUNT);
            SavedAppDataRepository.setData(SavedData.REMAINING_ALLOWANCE_FROM_ACCOUNT,
                this.getRemainingAllowanceForFromIsa());
            const fromRemainingAllowance = SavedAppDataRepository.getData(SavedData.REMAINING_ALLOWANCE_FROM_ACCOUNT);
            parseFloat(fromRemainingAllowance).should.be.equal(parseFloat(toRemainingAllowance) - parseFloat(amtValue));
        } else {
            SavedAppDataRepository.setData(SavedData.REMAINING_ALLOWANCE_TO_ACCOUNT,
                this.getRemainingAllowanceForToIsa());
            const toRemainingAllowance = SavedAppDataRepository.getData(SavedData.REMAINING_ALLOWANCE_TO_ACCOUNT);
            const fromRemainingAllowance = SavedAppDataRepository.getData(SavedData.REMAINING_ALLOWANCE_FROM_ACCOUNT);
            parseFloat(toRemainingAllowance).should.be.equal(parseFloat(fromRemainingAllowance) + parseFloat(amtValue));
        }
    }

    getP2PToDetail() {
        const toAccountName = Actions.getText(this.paymentHubMainScreen.toP2PAccountNameDetail);
        const toMobileNumber = Actions.getText(this.paymentHubMainScreen.toMobileNum);
        return `Account name is ${toAccountName} and Mobile number `
            + `${toMobileNumber}`;
    }

    verifyListOfEligibleRemitterAccount() {
        Actions.isExisting(this.paymentHubMainScreen.paymentHubRecipientsList).should.equal(true);
    }

    verifyListOfEligibleRemitanceAccount() {
        Actions.isExisting(this.paymentHubMainScreen.paymentHubRemitanceList).should.equal(true);
    }

    verifyInternetLostMessage() {
        Actions.isVisible(this.paymentHubMainScreen.internetLostMessage).should.equal(true);
        Actions.click(this.paymentHubMainScreen.closeErrorMessage);
    }

    verifySuccessConfirmPage() {
        Actions.waitForVisible(this.paymentHubMainScreen.verifySuccessConfirmPage);
    }

    validateCreditCardPaymentErrorMessage() {
        Actions.isVisible(this.paymentHubMainScreen.creditCardValidationErrorMessage).should.equal(true);
    }

    selectLearnAboutOverpaymentToolTip() {
        Actions.click(this.paymentHubMainScreen.learnAboutOverpaymentToolTip);
    }

    selectSubAccountLearnAboutOverpaymentToolTip() {
        Actions.click(this.paymentHubMainScreen.subAccountToolTip);
    }

    shouldSeeInformationAboutOverpayment(type) {
        if (DeviceHelper.isAndroid() && type === 'sub-accounts') {
            Actions.isVisible(this.paymentHubMainScreen.subAccountMoreInfoPopUp).should.equal(true);
        } else {
            Actions.isVisible(this.paymentHubMainScreen.informationAboutOverpaymentPopup).should.equal(true);
        }
    }

    shouldSeeSubAccountToggle() {
        Actions.waitForVisible(this.paymentHubMainScreen.subAccountToggle, 30000);
    }

    selectSubAccountToggle() {
        Actions.click(this.paymentHubMainScreen.subAccountToggle);
    }

    selectASubAccountFromSubAccountList() {
        Actions.click(this.paymentHubMainScreen.preSelectedSubAccount);
        Actions.swipeByPercentUntilVisible(this.paymentHubMainScreen.amount);
    }

    shouldSeeSubAccountsListed() {
        Actions.isVisible(this.paymentHubMainScreen.subAccountList).should.equal(true);
    }

    shouldSeeSubAccountsUnSelected() {
        Actions.isVisible(this.paymentHubMainScreen.preSelectedSubAccount).should.equal(true);
    }

    shouldSeeSubAccountsInformation() {
        Actions.isVisible(this.paymentHubMainScreen.subAccountDetails).should.equal(true);
    }

    shouldSeeMortgageAccount() {
        Actions.isVisible(this.homeScreen.accountSpecificActionMenu('mortgage')).should.equal(true);
    }

    verifyAmountValueFromIBC() {
        const ibcPaymentLimit = SavedAppDataRepository.getData(SavedData.TRANSACTION_LIMIT_EXCEEDED_AMOUNT);
        let obtainedAmount = Actions.getAttributeText(this.paymentHubMainScreen.amount, 'value');
        if (obtainedAmount.includes(',')) {
            obtainedAmount = obtainedAmount.replace(/,/g, '');
        }
        obtainedAmount.should.contains(ibcPaymentLimit);
    }
}

module.exports = PaymentHubMainPage;
