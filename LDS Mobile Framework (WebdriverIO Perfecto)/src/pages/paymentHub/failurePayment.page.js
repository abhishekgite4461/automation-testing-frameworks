require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class FailurePaymentPage extends AuthPage {
    /**
     * @returns {FailurePaymentScreen}
     */
    get failurePaymentScreen() {
        return this.getNativeScreen('paymentHub/failurePayment.screen');
    }

    waitForPageLoad() {
        Actions.getAttributeText(this.failurePaymentScreen.title, 'label').toLowerCase().should.contains('payment');
    }

    shouldSeeInternetLostErrorMessage() {
        Actions.isVisible(this.failurePaymentScreen.internetErrorMessage).should.equal(true);
    }

    shouldSeePendingPaymentErrorMessage() {
        Actions.isExisting(this.failurePaymentScreen.pendingPaymentErrorMessage).should.equal(true);
    }

    shouldSeeTransferPaymentsButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.failurePaymentScreen.transferAndPaymentButton);
        Actions.isVisible(this.failurePaymentScreen.transferAndPaymentButton).should.equal(true);
    }

    shouldSeeBlockISAPaymentErrorMessage() {
        Actions.isExisting(this.failurePaymentScreen.prevYearISAErrorMessage).should.equal(true);
    }
}

module.exports = FailurePaymentPage;
