const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const deviceHelper = require('../../utils/device.helper');

class StandingOrderAmendPage extends AuthPage {
    /**
     * @returns {StandingOrderAmendScreen}
     */
    get StandingOrderAmendScreen() {
        return this.getNativeScreen('paymentHub/standingOrderAmend.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.StandingOrderAmendScreen.title);
    }

    changeReference(reference) {
        Actions.swipeByPercentUntilVisible(this.StandingOrderAmendScreen.soReference, 20, 80, 20, 70);
        Actions.clearText(this.StandingOrderAmendScreen.soReference);
        Actions.setValue(this.StandingOrderAmendScreen.soReference, reference);
        if (deviceHelper.isIOS()) {
            Actions.tapOutOfField();
        } else {
            Actions.hideDeviceKeyboard();
        }
    }

    changeAmount(amount) {
        Actions.swipeByPercentUntilVisible(this.StandingOrderAmendScreen.soAmount, 20, 80, 20, 70);
        Actions.clearText(this.StandingOrderAmendScreen.soAmount);
        Actions.setValue(this.StandingOrderAmendScreen.soAmount, amount);
        if (deviceHelper.isIOS()) {
            Actions.tapOutOfField();
        } else {
            Actions.hideDeviceKeyboard();
        }
    }

    enterSOAmendPassword(password) {
        Actions.swipeByPercentUntilVisible(this.StandingOrderAmendScreen.soPassword, 20, 80, 20, 70);
        Actions.setImmediateValue(this.StandingOrderAmendScreen.soPassword, password);
    }

    selectAmend() {
        Actions.swipePage(SwipeAction.UP, 800);
        Actions.click(this.StandingOrderAmendScreen.soAmendButton);
    }

    selectConfirmAmendments() {
        Actions.swipePage(SwipeAction.UP, 800);
        Actions.click(this.StandingOrderAmendScreen.soConfirmAmendButton);
    }

    verifyStandingOrderAmendment() {
        Actions.waitForVisible(this.StandingOrderAmendScreen.soChangeSucessTitle);
        Actions.waitForVisible(this.StandingOrderAmendScreen.soChangeSucessMessage);
    }

    selectViewStandingOrder() {
        Actions.swipeByPercentUntilVisible(this.StandingOrderAmendScreen.soViewStandingOrdersButton, 20, 80, 20, 70);
        Actions.click(this.StandingOrderAmendScreen.soViewStandingOrdersButton);
    }
}

module.exports = StandingOrderAmendPage;
