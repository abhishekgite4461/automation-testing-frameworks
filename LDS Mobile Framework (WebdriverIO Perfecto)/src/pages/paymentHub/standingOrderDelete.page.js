const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class StandingOrderDeletePage extends AuthPage {
    /**
     * @returns {StandingOrderDeleteScreen}
     */
    get StandingOrderDeleteScreen() {
        return this.getNativeScreen('paymentHub/standingOrderDelete.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.StandingOrderDeleteScreen.soDeleteTitle);
    }

    enterPassword(password) {
        Actions.pause(200);
        Actions.swipeByPercentUntilVisible(this.StandingOrderDeleteScreen.soDeletePassword, 20, 80, 20, 70);
        Actions.pause(200);
        Actions.setImmediateValue(this.StandingOrderDeleteScreen.soDeletePassword, password);
    }

    confirmDeleteStandingOrder() {
        if (DeviceHelper.isBNGA()) {
            Actions.waitForVisible(this.StandingOrderDeleteScreen.soDeleteSOButtonBnga);
            Actions.click(this.StandingOrderDeleteScreen.soDeleteSOButtonBnga);
        } else {
            Actions.swipePage(SwipeAction.UP, 800);
            Actions.click(this.StandingOrderDeleteScreen.soDeleteSOButton);
        }
    }

    verifySODeleteSuccessMessage() {
        if (DeviceHelper.isBNGA()) {
            Actions.waitForVisible(this.StandingOrderDeleteScreen.soDeleteMessageTitleBnga);
        } else {
            Actions.waitForVisible(this.StandingOrderDeleteScreen.soDeleteMessageTitle);
        }
    }
}

module.exports = StandingOrderDeletePage;
