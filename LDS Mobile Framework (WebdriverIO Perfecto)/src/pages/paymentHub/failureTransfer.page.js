require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class FailureTransferPage extends AuthPage {
    /**
     * @returns {FailureTransferScreen}
     */
    get failureTransferScreen() {
        return this.getNativeScreen('paymentHub/failureTransfer.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.failureTransferScreen.title);
        Actions.getAttributeText(this.failureTransferScreen.title, 'label').toLowerCase().should.contains('transfer');
    }

    waitForIsaFailurePageLoad() {
        Actions.waitForVisible(this.failureTransferScreen.isaTransferTitle);
    }

    waitForMoreThanFundingLimitHTBPageLoad() {
        Actions.waitForVisible(this.failureTransferScreen.moreThanFundingLimitHTBTransferTitle);
    }

    shouldSeeInternetLostErrorMessage() {
        Actions.isVisible(this.failureTransferScreen.internetErrorMessage).should.equal(true);
    }

    shouldSeeTransferPaymentsButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.failureTransferScreen.transferAndPaymentButton);
        Actions.isVisible(this.failureTransferScreen.transferAndPaymentButton).should.equal(true);
    }

    shouldSeeIsaAlreadySubscribedErrorMessage() {
        Actions.isExisting(this.failureTransferScreen.isaAlreadySubscribedErrorMessage).should.equal(true);
    }

    canSeeHtbIsaAboveFundLimitErrorMessage() {
        Actions.isVisible(this.failureTransferScreen.htbIsaAboveFundLimitErrorMessage).should.equal(true);
    }
}

module.exports = FailureTransferPage;
