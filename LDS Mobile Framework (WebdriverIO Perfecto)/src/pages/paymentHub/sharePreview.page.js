require('chai')
    .should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const DeviceHelper = require('../../utils/device.helper');

class SharePreviewPage extends AuthPage {
    get sharePreviewScreen() {
        return this.getNativeScreen('paymentHub/sharePreview.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.sharePreviewScreen.title);
    }

    verifyShareOption() {
        Actions.isVisible(this.sharePreviewScreen.sharePreviewShareButton).should.equal(true);
    }

    selectShareOption() {
        Actions.click(this.sharePreviewScreen.sharePreviewShareButton);
    }

    shouldSeeNativeSharePanel() {
        Actions.isVisible(this.sharePreviewScreen.nativeSharePanel).should.equal(true);
    }

    shouldSeeEmailAppIcon() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.sharePreviewScreen.emailAppIcon);
    }

    selectEmailApp() {
        Actions.click(this.sharePreviewScreen.emailAppIcon);
    }

    shouldSeeAccountDetails() {
        Actions.isVisible(this.sharePreviewScreen.displayAccountDetails).should.equal(true);
    }

    getFromAccountName() {
        const fromAccountName = Actions.getText(this.sharePreviewScreen.payerName);
        SavedAppDataRepository.setData(SavedData.FROM_ACCOUNT_NAME, fromAccountName);
    }

    shouldSeePayeeAccountNumberMasked() {
        const accountNumber = Actions.getText(this.sharePreviewScreen.beneficiaryAccountNumber);
        if (!(accountNumber.match(/^[*]{4}[0-9]{4}$/).index === 0)) {
            throw new Error(`${accountNumber} should be masked`);
        }
    }

    shouldSeeBankIcon() {
        Actions.isVisible(this.sharePreviewScreen.bankIcon).should.equal(true);
    }

    shouldSeeDateValue(date) {
        Actions.isVisible(this.sharePreviewScreen.paymentReceiptDateValue(date)).should.equal(true);
    }

    verifySharePreviewScreenDetails(sharePreviewDetails) {
        for (let i = 0; i < sharePreviewDetails.length; i++) {
            const shareReceiptDetails = this.sharePreviewScreen[sharePreviewDetails[i].detail];
            Actions.isVisible(shareReceiptDetails).should.equal(true);
        }
    }

    selectCancelButton() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.sharePreviewScreen.nativeShareCancelButton);
            Actions.click(this.sharePreviewScreen.emailAppDeleteDraftButton);
        } else {
            Actions.pressAndroidBackKey();
            Actions.pressAndroidBackKey();
        }
    }
}

module.exports = SharePreviewPage;
