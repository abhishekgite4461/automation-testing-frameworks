const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class ReactivateIsaPage extends AuthPage {
    /**
     * @returns {HomeScreen}
     */
    get homeScreen() {
        return this.getNativeScreen('home.screen');
    }

    /**
     * @returns {ReactivateIsaScreen}
     */
    get reactivateIsaScreen() {
        return this.getNativeScreen('paymentHub/reactivateIsa.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.reactivateIsaScreen.title);
    }

    verifyDeclarationDetails(declarationDetails) {
        for (let i = 0; i < declarationDetails.length; i++) {
            const condition = () => Actions.isEnabled(this.reactivateIsaScreen[declarationDetails[i].details]);
            Actions.swipeByPercentUntilCondition(condition, 90, 80, 10, 10, 200, 5);
        }
    }

    verifyReactivateIsaUpdateNiNumberTextView() {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.reactivateIsaScreen.reactivateIsaUpdateNiNumberLinkText
        );
        Actions.click(this.reactivateIsaScreen.reactivateIsaUpdateNiNumberLinkText);
        Actions.waitForVisible(this.reactivateIsaScreen.reactivateIsaUpdateNiModal);
        Actions.click(this.reactivateIsaScreen.reactivateCloseButton);
    }

    selectReactivateIsaUpdateMyAddress() {
        Actions.click(this.reactivateIsaScreen.reactivateIsaUpdateAddressLinkText);
    }

    verifyReactivateIsaUpdateMyAddress(updateAddress) {
        for (let i = 0; i < updateAddress.length; i++) {
            Actions.isVisible(this.reactivateIsaScreen[updateAddress[i].address]);
        }
    }

    selectCalltoChangeAddress() {
        Actions.click(this.reactivateIsaScreen.settingsPersonalDetailsChangeAddressButton);
    }

    selectAgreeConfirmation() {
        Actions.swipeByPercentUntilVisible(this.reactivateIsaScreen.reactivateAgreementCheckBox);
        Actions.click(this.reactivateIsaScreen.reactivateAgreementCheckBox);
        Actions.isVisible(this.reactivateIsaScreen.reactivateIsaCancelButton);
        Actions.swipeByPercentUntilVisible(this.reactivateIsaScreen.reactivateIsaConfirmButton);
        Actions.waitForEnabled(this.reactivateIsaScreen.reactivateIsaConfirmButton);
        Actions.click(this.reactivateIsaScreen.reactivateIsaConfirmButton);
    }

    verifyEnterPasswordDialogBox(isVisiblePasswordPopUp) {
        if (isVisiblePasswordPopUp) {
            Actions.waitForVisible(this.reactivateIsaScreen.passwordConfirmationDialogPanel);
        } else {
            Actions.isVisible(this.reactivateIsaScreen.passwordConfirmationDialogPanel)
                .should.equal(isVisiblePasswordPopUp);
        }
    }

    enterPasswordOnDialogBox(password) {
        Actions.waitForVisible(this.reactivateIsaScreen.passwordConfirmationDialogPasswordBox);
        Actions.setImmediateValue(this.reactivateIsaScreen.passwordConfirmationDialogPasswordBox, password);
        Actions.waitForEnabled(this.reactivateIsaScreen.dialogPositiveAction);
        Actions.click(this.reactivateIsaScreen.dialogPositiveAction);
    }

    verifyReactivateIsaPage() {
        Actions.swipeByPercentUntilVisible(
            this.reactivateIsaScreen.reactivateIsaConfirmButton
        );
    }
}

module.exports = ReactivateIsaPage;
