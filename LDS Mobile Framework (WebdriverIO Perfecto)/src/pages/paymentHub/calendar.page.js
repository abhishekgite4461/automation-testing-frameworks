require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');

const monthNames = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'
];

class CalendarPage extends AuthPage {
    /**
     * @returns {CalendarScreen}
     */
    get calendarScreen() {
        return this.getNativeScreen('paymentHub/calendar.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.calendarScreen.title);
    }

    static selectDateOffset(days) {
        const date = new Date();
        return new Date(date.setTime(date.getTime() + days * 86400000));
    }

    getFuturePaymentDates() {
        const date = new Date();
        const presentMonth = date.getMonth().toString();
        const futureDate = CalendarPage.selectDateOffset(31);
        const futureMonth = futureDate.getMonth().toString();
        const offsetMonths = (futureDate.getYear() - date.getYear()) * 12;
        const differenceInMonths = (futureMonth + offsetMonths) - presentMonth;
        for (let i = 0; i < differenceInMonths; i++) {
            Actions.click(this.calendarScreen.nextMonthButton);
        }
        return futureDate.getDate();
    }

    canSelectPaymentUptoMonth() {
        const futureDate = this.getFuturePaymentDates();
        Actions.waitForVisible(this.calendarScreen.nextMonthButton);
        Actions.isEnabled(this.calendarScreen.lastDayOfPayment(futureDate)).should.equal(true);
    }

    cantSelectPaymentMoreThanMonth() {
        const futureDate = this.getFuturePaymentDates();
        Actions.isEnabled(this.calendarScreen.lastDayOfPayment(futureDate + 1)).should.equal(false);
        Actions.click(this.calendarScreen.cancel);
    }

    cancelCalendarPopUP() {
        Actions.click(this.calendarScreen.cancel);
    }

    shouldSeeNextMonth() {
        const date = new Date();
        let nextMonth = monthNames[(date.getMonth() + 1) % 12];
        nextMonth = browser.capabilities.brand === 'HFX' ? nextMonth.toUpperCase() : nextMonth;
        let year = date.getFullYear();
        if (date.getMonth() === 11) {
            year += 1;
        }
        const fullMonth = `${nextMonth} ${year}`;
        Actions.isVisible(this.calendarScreen.monthName(fullMonth)).should.equal(true);
    }

    shouldSeePrevMonth() {
        const date = new Date();
        const month = monthNames[date.getMonth()];
        const year = date.getFullYear();
        const fullMonth = `${month} ${year}`;
        Actions.isVisible(this.calendarScreen.monthName(fullMonth)).should.equal(true);
    }

    selectNextMonth() {
        Actions.click(this.calendarScreen.nextMonthButton);
    }

    selectPreviousMonth() {
        Actions.click(this.calendarScreen.prevMonthButton);
    }

    selectDate(dateIndex) {
        Actions.click(this.calendarScreen.lastDayOfPayment(dateIndex));
    }

    selectFuturePaymentDate(numOfDays) {
        const today = new Date();
        const nextDate = new Date();
        nextDate.setDate(today.getDate() + numOfDays);

        if (today.getMonth() !== nextDate.getMonth()) {
            this.selectNextMonth();
        }
        this.selectDate(nextDate.getDate());
        SavedAppDataRepository.setData(SavedData.FUTURE_PAYMENT_DATE, nextDate);
    }
}

module.exports = CalendarPage;
