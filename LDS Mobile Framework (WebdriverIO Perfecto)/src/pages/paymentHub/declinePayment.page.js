require('chai').should();
const unAuthPage = require('../common/unAuth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class DeclinePaymentPage extends unAuthPage {
    /**
     * @returns {DeclinePaymentScreen}
     */
    get declinePaymentScreen() {
        return this.getNativeScreen('paymentHub/declinePayment.screen');
    }

    waitForDeclinedPageLoad() {
        Actions.getAttributeText(this.declinePaymentScreen.title, 'value').toLowerCase().should.contains(this.declinePaymentScreen.pageHeader);
    }

    shouldSeeDeclinedErrorMessage() {
        const declinedErrorMessage = 'spotted unusual activity on your account and have blocked this payment';
        Actions.getAttributeText(this.declinePaymentScreen.errorMessage, 'value').toLowerCase().should.contains(declinedErrorMessage);
    }

    shouldSeeBackToLoginButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.declinePaymentScreen.logonButton);
        Actions.isVisible(this.declinePaymentScreen.logonButton).should.equal(true);
    }
}

module.exports = DeclinePaymentPage;
