require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class HoldPaymentPage extends AuthPage {
    /**
     * @returns {HoldPaymentScreen}
     */
    get holdPaymentScreen() {
        return this.getNativeScreen('paymentHub/holdPayment.screen');
    }

    waitForPaymentHoldPageLoad() {
        Actions.waitForVisible(this.holdPaymentScreen.title);
        Actions.getAttributeText(this.holdPaymentScreen.title, 'label').toLowerCase().should.contains('payment on hold');
    }

    shouldSeeHoldPaymentErrorMessage() {
        if (DeviceHelper.isRNGA()) {
            Actions.isExisting(this.holdPaymentScreen.errorMessage).should.equal(true);
        } else {
            Actions.isExisting(this.holdPaymentScreen.paymentHoldErrorMessage).should.equal(true);
        }
    }

    shouldSeeTransferPaymentButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.holdPaymentScreen.transferAndPaymentButton);
        Actions.isVisible(this.holdPaymentScreen.transferAndPaymentButton).should.equal(true);
    }

    selectTransferPaymentButton() {
        Actions.click(this.holdPaymentScreen.transferAndPaymentButton);
    }

    selectCallUsOnPaymentPage() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.holdPaymentScreen.callUsOption);
        Actions.click(this.holdPaymentScreen.callUsOption);
    }
}

module.exports = HoldPaymentPage;
