require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ChangeAddressModalPage extends AuthPage {
    /**
     * @returns {ChangeAddressModal}
     */
    get changeAddressModalScreen() {
        return this.getNativeScreen('paymentHub/changeAddressModal.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.changeAddressModalScreen.title);
    }
}

module.exports = ChangeAddressModalPage;
