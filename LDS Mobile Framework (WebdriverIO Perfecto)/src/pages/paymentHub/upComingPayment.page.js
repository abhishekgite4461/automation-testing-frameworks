const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class UpComingPayment extends AuthPage {
    /**
     * @returns {UpComingPaymentScreen}
     */
    get upComingPaymentScreen() {
        return this.getNativeScreen('paymentHub/upComingPayment.screen');
    }

    waitForPageLoad() {
        Actions.waitUntilVisible(this.upComingPaymentScreen.title, 50000);
    }

    verifyUpcomingPayment(transactionType, account, amount) {
        Actions.swipeUntilVisible(SwipeAction.UP,
            this.upComingPaymentScreen.upComingPayment(transactionType, account, amount));
    }
}

module.exports = UpComingPayment;
