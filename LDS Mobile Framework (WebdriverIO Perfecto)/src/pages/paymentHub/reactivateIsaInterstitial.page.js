const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ReactivateIsaInterstitialPage extends AuthPage {
    /**
     * @returns {ReactivateIsaInterstitialScreen}
     */
    get reactivateIsaInterstitialScreen() {
        return this.getNativeScreen('paymentHub/reactivateIsaInterstitial.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.reactivateIsaInterstitialScreen.title);
    }

    selectReactivateIsaButton() {
        Actions.click(this.reactivateIsaInterstitialScreen.reactivateIsaButton);
    }
}

module.exports = ReactivateIsaInterstitialPage;
