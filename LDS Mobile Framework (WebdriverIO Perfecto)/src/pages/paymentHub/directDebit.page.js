const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class DirectDebitPage extends AuthPage {
    /**
     * @returns {DirectDebit}
     */
    get directDebit() {
        return this.getNativeScreen('directDebit.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.directDebit.title);
    }
}

module.exports = DirectDebitPage;
