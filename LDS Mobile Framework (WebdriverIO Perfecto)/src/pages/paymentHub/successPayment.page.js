const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');

class SuccessPaymentPage extends AuthPage {
    /**
     * @returns {SuccessPaymentScreen}
     */
    get successPaymentScreen() {
        return this.getNativeScreen('paymentHub/successPayment.screen');
    }

    /**
     * @returns {PaymentUnsuccessfulScreen}
     */
    get paymentUnsuccessfulScreen() {
        return this.getNativeScreen('transferAndPayments/paymentUnsuccessful.screen');
    }

    waitForPageLoad() {
        try {
            const locator = Actions.waitUntilVisible([
                this.successPaymentScreen.title,
                this.successPaymentScreen.notNow
            ], 20000);

            if (locator === this.successPaymentScreen.notNow) {
                Actions.click(this.successPaymentScreen.notNow);
            }
        } catch (e) {
            if (DeviceHelper.isEnv() && Actions.isVisible(this.paymentUnsuccessfulScreen.title)) {
                throw Error('TEST_DATA_ERROR');
            }
        }
    }

    selectViewTransaction() {
        Actions.click(this.successPaymentScreen.viewTransactionButton);
    }

    selectMakeAnotherPaymentButton() {
        Actions.click(this.successPaymentScreen.makeAnotherPaymentButton);
    }

    static selectHardCoreBackButton() {
        if (DeviceHelper.isAndroid()) {
            Actions.pressAndroidBackKey();
        }
    }

    verifySuccessPaymentPage() {
        Actions.isVisible(this.successPaymentScreen.title).should.equal(true);
    }

    verifyViewTransactionButtonInPaymentSuccessPage() {
        Actions.isVisible(this.successPaymentScreen.viewTransactionButton);
    }

    verifyMakeAnotherTransferButtonInPaymentSuccessPage() {
        Actions.isVisible(this.successPaymentScreen.makeAnotherPaymentButton).should.equal(true);
    }

    shouldSeeDateValue(date) {
        const fullDate = `${date}`;
        Actions.swipeByPercentUntilVisible(this.successPaymentScreen.dateValue(fullDate), 80, 60, 20, 20);
    }

    selectViewStandingOrders() {
        Actions.click(this.successPaymentScreen.viewStandingOrders);
    }

    verifyMPAPaymentApprovalSuccessMessage() {
        Actions.isVisible(this.successPaymentScreen.mpaPaymentApprovalSuccessMessage).should.equal(true);
    }

    verifyShareReceiptOption() {
        Actions.isVisible(this.successPaymentScreen.shareReceiptButton).should.equal(true);
    }

    shouldNotSeeShareRecipient() {
        Actions.isVisible(this.successPaymentScreen.shareReceiptButton).should.equal(false);
    }

    selectShareReceiptOption() {
        Actions.click(this.successPaymentScreen.shareReceiptButton);
    }

    shouldSeeReferenceFieldInSuccessPage() {
        Actions.isVisible(this.successPaymentScreen.reference).should.equal(true);
    }

    shouldSeeScheduledSuccessPaymentPage() {
        Actions.isVisible(this.successPaymentScreen.paymentScheduledTitle).should.equal(true);
    }
}

module.exports = SuccessPaymentPage;
