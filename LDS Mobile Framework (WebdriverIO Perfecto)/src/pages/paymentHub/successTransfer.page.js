const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const DeviceHelper = require('../../utils/device.helper');

class SuccessTransferPage extends AuthPage {
    /**
     * @returns {SuccessTransferScreen}
     */
    get successTransferScreen() {
        return this.getNativeScreen('paymentHub/successTransfer.screen');
    }

    /**
     * @returns {SuccessPaymentScreen}
     */
    get successPaymentScreen() {
        return this.getNativeScreen('paymentHub/successPayment.screen');
    }

    waitForPageLoad() {
        const locator = Actions.waitUntilVisible([
            this.successTransferScreen.title,
            this.successTransferScreen.notNow
        ], 15000);

        if (locator === this.successTransferScreen.notNow) {
            Actions.click(this.successTransferScreen.notNow);
        }
    }

    getFromDetail() {
        const fromAccountName = Actions.getAttributeText(this
            .successTransferScreen.fromAccountNameDetail, 'label');
        const fromSortCode = Actions.getAttributeText(this.successTransferScreen.fromSortCodeDetail, 'label');
        const fromAccountNumber = Actions.getAttributeText(this
            .successTransferScreen.fromAccountNumberDetail, 'label');
        return `Account name is ${fromAccountName} with sort code `
            + `${fromSortCode} and account number ${fromAccountNumber}`;
    }

    getToDetail() {
        const toAccountName = Actions.getAttributeText(this.successTransferScreen.toAccountNameDetail, 'label');
        const toSortCode = Actions.getAttributeText(this.successTransferScreen.toSortCodeDetail, 'label');
        const toAccountNumber = Actions.getAttributeText(this
            .successTransferScreen.toAccountNumberDetail, 'label');
        return `Account name is ${toAccountName} with sort code `
            + `${toSortCode} and account number ${toAccountNumber}`;
    }

    getAmountDetail() {
        // TODO MOB3-13954 add the summary container object for ios once the refactoring is resolved
        if (DeviceHelper.isAndroid()) {
            Actions.swipeByPercentUntilVisible(this.successTransferScreen.amountDetail, 20, 60, 20, 20, 50,
                2, this.successTransferScreen.summaryContainer);
        } else {
            Actions.swipeByPercentage(20, 80, 20, 20, 50);
        }
        return Actions.getAttributeText(this.successTransferScreen.amountDetail, 'label');
    }

    validateAccountDetails() {
        const toAccount = (SavedAppDataRepository.getData(SavedData.TRANSFER_TO_ACCOUNT).toLowerCase()).match(/(\w+)/g)[3];
        const amount = SavedAppDataRepository.getData(SavedData.TRANSFER_AMOUNT).toLowerCase();
        Actions.getText(this.successPaymentScreen.paymentProcessed).toLowerCase().should.contains(toAccount);
        Actions.getText(this.successPaymentScreen.paymentProcessed).toLowerCase().should.contains(amount);
    }

    selectViewTransaction() {
        Actions.click(this.successTransferScreen.viewTransactionButton);
    }

    selectMakeAnotherTransferButton() {
        Actions.click(this.successTransferScreen.makeAnotherTransferButton);
    }

    static selectHardCoreBackButton() {
        if (DeviceHelper.isAndroid()) {
            Actions.pressAndroidBackKey();
        }
    }

    verifySuccessTransferPage() {
        Actions.isVisible(this.successTransferScreen.title).should.equal(true);
    }

    verifyTransactionButton() {
        Actions.isVisible(this.successTransferScreen.viewTransactionButton).should.equal(true);
    }

    verifyMakeAnotherTransferButton() {
        Actions.swipeDownOnSmallScreen();
        Actions.isVisible(this.successTransferScreen.makeAnotherTransferButton).should.equal(true);
    }

    verifyMPATransferApprovalSuccessMessage() {
        Actions.swipeDownOnSmallScreen();
        Actions.waitForVisible(this.successTransferScreen.mpaTransferApprovalSuccessMessage);
    }
}

module.exports = SuccessTransferPage;
