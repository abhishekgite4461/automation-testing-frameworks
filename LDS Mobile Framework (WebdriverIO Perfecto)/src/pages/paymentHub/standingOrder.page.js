const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');

class StandingOrderPage extends AuthPage {
    /**
     * @returns {StandingOrder}
     */
    get standingOrder() {
        return this.getNativeScreen('paymentHub/standingOrder.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.standingOrder.standingOrderTab);
    }

    selectSetUpNewStandingOrder() {
        Actions.click(this.standingOrder.setUpNewStandingOrderButton);
    }

    clickBackButton() {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.commonScreen.backButton);
        } else {
            Actions.click(this.commonScreen.homeButton);
        }
    }
}

module.exports = StandingOrderPage;
