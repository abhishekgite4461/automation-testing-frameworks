const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class SaveTheChangesPage extends AuthPage {
    /**
     * @returns {SaveTheChanges}
     */
    get saveTheChanges() {
        return this.getNativeScreen('transferAndPayments/saveTheChanges.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.saveTheChanges.title, 70000);
    }

    Clickhomebutton() {
        Actions.click(this.saveTheChanges.homebutton);
    }
}

module.exports = SaveTheChangesPage;
