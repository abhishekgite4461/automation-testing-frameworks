require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ReactivateIsaSuccessPage extends AuthPage {
    /**
     * @returns {reactivateIsaSuccessScreen}
     */
    get reactivateIsaSuccessScreen() {
        return this.getNativeScreen('paymentHub/reactivateIsaSuccess.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.reactivateIsaSuccessScreen.title);
    }

    verifyReactivateIsaSuccessPage(successIsaDetails) {
        for (let i = 0; i < successIsaDetails.length; i++) {
            const individualSuccessISADetail = this.reactivateIsaSuccessScreen[successIsaDetails[i].details];
            Actions.isVisible(individualSuccessISADetail).should.equal(true);
        }
    }

    verifyAddMoreMoneyButton() {
        Actions.isVisible(this.reactivateIsaSuccessScreen.reactivateIsaAddMoreMoneyButton).should.equal(true);
    }

    verifyAddMoreMoneyButtonInvisible() {
        Actions.isVisible(this.reactivateIsaSuccessScreen.reactivateIsaAddMoreMoneyButton).should.equal(false);
    }

    verifyMessageForExternalTransferIsa() {
        Actions.isVisible(this.reactivateIsaSuccessScreen.reactivateIsaSupportingCopy).should.equal(true);
    }

    verifyReactivateIsaSuccessScreen() {
        Actions.waitForVisible(this.reactivateIsaSuccessScreen.title);
    }

    verifyInstructionalMessage() {
        Actions.isVisible(this.reactivateIsaSuccessScreen.reactivateIsaInstructionalMessage).should.equal(true);
    }
}

module.exports = ReactivateIsaSuccessPage;
