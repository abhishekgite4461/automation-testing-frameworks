const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
require('chai').should();

class EnrolmentTermsAndConditionsDeclinedError extends AuthPage {
    /**
     * @returns {EnrolmentTermsAndConditionsDeclinedError}
     */
    get enrolmentTermsAndConditionsDeclinedErrorScreen() {
        return this.getNativeScreen('enrolmentTermsAndConditionsDeclinedError.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.enrolmentTermsAndConditionsDeclinedErrorScreen.title);
    }

    verifyDeclinedErrorMessage() {
        Actions.isVisible(this.enrolmentTermsAndConditionsDeclinedErrorScreen.termsAndConditionsDeclinedError)
            .should.equal(true);
    }
}

module.exports = EnrolmentTermsAndConditionsDeclinedError;
