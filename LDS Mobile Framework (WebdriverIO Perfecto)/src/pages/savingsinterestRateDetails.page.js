require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class SaveInterestDetailPage extends AuthPage {
    /**
     * @returns {saveInterestRateDetails}
     */
    get saveInterestDetailsScreen() {
        return this.getNativeScreen('saveInterestRateDetails.screen');
    }

    /**
     * @returns {HomeScreen}
     */
    get homeScreen() {
        return this.getNativeScreen('home.screen');
    }

    checkForPageVisibility() {
        Actions.isVisible(this.saveInterestDetailsScreen.accountName).should.equal(true);
    }

    holdToOpenSirDetailsScreen(accountName) {
        const accName = Actions.modifyAccountName(accountName);
        Actions.longPressed(this.homeScreen.accountByName(accName));
    }

    selectViewInterestDetailsOption() {
        Actions.swipeByPercentUntilVisible(this.saveInterestDetailsScreen.interestDetailRow);
        Actions.click(this.saveInterestDetailsScreen.interestDetailRow);
    }

    checkForFieldsVisibility(pageFields) {
        Actions.waitForVisible(this.saveInterestDetailsScreen[pageFields[0]
            .fieldsInDetailsScreen]);
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.isVisible(this.saveInterestDetailsScreen[pageFields[iter]
                .fieldsInDetailsScreen]).should.equal(true);
        }
    }

    closeViewInterestDetailsOption() {
        Actions.click(this.saveInterestDetailsScreen.closeButton);
    }

    closeInterestDetailSwipeAction() {
        Actions.swipeOnElement(
            SwipeAction.DOWN,
            this.saveInterestDetailsScreen.interestRateScreen
        );
    }

    openAndViewInterestRateDetailIsNotVisible() {
        Actions.click(this.saveInterestDetailsScreen.actionMenu);
        Actions.isVisible(this.saveInterestDetailsScreen.interestDetailRow).should.equal(false);
    }

    SelectViewInterestDetailsOption() {
        Actions.click(this.saveInterestDetailsScreen.interestDetailRow);
    }

    savingInterestRateScreenNotVisible() {
        Actions.isVisible(this.saveInterestDetailsScreen.interestDetailRow).should.equal(false);
    }

    verifySMRIsClosed() {
        Actions.isVisible(this.saveInterestDetailsScreen.accountName).should.equal(false);
    }

    validateTheScreenAfterSMRIsClosed(accountHeaderName) {
        Actions.isVisible(this.homeScreen.accountByName(accountHeaderName)).should.equal(true);
    }

    validateInterestRate() {
        Actions.swipeByPercentUntilVisible(this.saveInterestDetailsScreen.interestRateInfo);
        Actions.isVisible(this.saveInterestDetailsScreen.interestRateInfo).should.equal(true);
    }
}

module.exports = SaveInterestDetailPage;
