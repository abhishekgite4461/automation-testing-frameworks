require('chai').should();

const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DPNLeads = require('../enums/dpnLeads.enum');
const CommonPage = require('./common.page');

const commonPage = new CommonPage();

class LeadsPage extends AuthPage {
    /**
     * @returns {LeadsScreen}
     */
    get leadsScreen() {
        return this.getNativeScreen('leads.screen');
    }

    get leadsWebScreen() {
        return this.getWebScreen('leadsWeb.screen');
    }

    get homeScreen() {
        return this.getNativeScreen('home.screen');
    }

    /**
     * @returns {GlobalMenuScreen}
     */
    get globalMenuScreen() {
        return this.getNativeScreen('globalMenu.screen');
    }

    swipeToLead() {
        Actions.swipeByPercentUntilVisible(this.homeScreen.asmLeadTile, 80, 75, 60, 10, 300, 23);
    }

    verifyInterstitialLead() {
        Actions.waitForVisible(this.leadsScreen.leadsholder, 30000);
        Actions.isVisible(this.leadsScreen.leadsholder).should.equal(true);
    }

    scrollLead(type) {
        if (type === 'dpn') {
            Actions.swipeByPercentUntilVisibleWithinViewport(this.leadsWebScreen.endOfThePage,
                50, 80, 50, 20, 100, 35);
            Actions.swipeByPercentage(50, 80, 50, 50, 10);
        } else if (type === 'big prompt') {
            Actions.swipeByPercentUntilVisible(this.leadsScreen.bigPromptEndOfPage, 50, 80, 50, 20, 100, 25);
        } else if (type === 'bigprompt takeover') {
            Actions.swipeByPercentUntilVisible(this.leadsScreen.bigPromptEndOfPage, 50, 80, 50, 20, 100, 25);
        } else {
            throw new Error(`${type} is an unrecognized type of lead`);
        }
    }

    verifyButton(state) {
        const btnState = state;
        if (btnState === 'enabled') {
            Actions.isEnabled(this.leadsScreen.leadsButton).should.equal(true);
        } else if (btnState === 'disabled') {
            Actions.isEnabled(this.leadsScreen.leadsButton).should.equal(false);
        } else {
            throw new Error(`${btnState} is an unrecognized button state`);
        }
    }

    clickConfirm() {
        Actions.click(this.leadsScreen.leadsButton);
    }

    verifyNoGlobalMenu() {
        Actions.isVisible(this.globalMenuScreen.globalMenuButton).should.equal(false);
    }

    verifyNativeButton() {
        Actions.isVisible(this.leadsScreen.leadsButton).should.equal(true);
    }

    verifyAlertPopUp() {
        Actions.waitForVisible(this.leadsScreen.endOfPageNotDisplayedAlert);
    }

    clickHere() {
        Actions.click(this.leadsScreen.bigPromptLink);
    }

    selectASMLeadTile() {
        Actions.waitForVisible(this.leadsScreen.asm1Lead);
        Actions.click(this.leadsScreen.asm1Lead);
    }

    clickMarketingHubLink() {
        Actions.waitForVisible(this.leadsWebScreen.bigPromptMarketingPreferenceHubLink);
        Actions.click(this.leadsWebScreen.bigPromptMarketingPreferenceHubLink);
    }

    viewHighestPriorityLead() {
        Actions.isVisible(this.leadsScreen.highestPriorityLead).should.equal(true);
    }

    selectNativeLead(leadType) {
        // To reduce if conditions, add new DPN native leads to the ENUM
        const mapLead = DPNLeads.fromStringByEscapingSpace(leadType);
        const pickLocator = mapLead.locator;
        Actions.swipeByPercentUntilVisible(this.leadsScreen[pickLocator]);
        Actions.click(this.leadsScreen[pickLocator]);
    }

    selectChequeDepositTile() {
        Actions.click(this.leadsScreen.asmChequeTile);
    }

    selectDepositCheque() {
        Actions.click(this.leadsScreen.depositCheque);
    }

    shouldSeeDepositCheque() {
        Actions.isVisible(this.leadsScreen.chequeHeader).should.equal(true);
    }

    shouldSeeChequeHeader() {
        Actions.isVisible(this.leadsScreen.chequeHeader).should.equal(true);
    }

    shouldSeeViewdemo() {
        Actions.isVisible(this.leadsScreen.viewDemo).should.equal(true);
    }

    selectViewdemo() {
        Actions.click(this.leadsScreen.viewDemo);
    }

    validateChequePromotionPage() {
        commonPage.shouldSeeHeaderBackButton();
        this.shouldSeeChequeHeader();
        this.shouldSeePayChequeHeader();
        this.shouldSeePayChequeDesc();
        this.shouldSeeViewdemo();
        this.shouldSeeDepositCheque();
    }

    shouldSeeChequeDemoPage() {
        Actions.isVisible(this.leadsScreen.chequeDemo).should.equal(true);
        Actions.isVisible(this.leadsScreen.closeDemo).should.equal(true);
    }

    selectCloseDemo() {
        Actions.click(this.leadsScreen.closeDemo);
    }

    shouldSeePayChequeHeader() {
        Actions.isVisible(this.leadsScreen.payChequeHeader).should.equal(true);
    }

    shouldSeePayChequeDesc() {
        Actions.isVisible(this.leadsScreen.payChequeDesc).should.equal(true);
    }

    selectAddExternalAccountLink() {
        Actions.waitForVisible(this.leadsScreen.addExternalAccountLink);
        Actions.click(this.leadsScreen.addExternalAccountLink);
    }
}

module.exports = LeadsPage;
