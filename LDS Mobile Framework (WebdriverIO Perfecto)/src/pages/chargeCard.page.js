require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class ChargeCardPage extends AuthPage {
    /**
     * @returns {ChargeCardPage}
     */
    get chargeCardScreen() {
        return this.getWebScreen('chargeCard.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.chargeCardScreen.title);
    }

    chargeCardIsVisible() {
        Actions.isVisible(this.chargeCardScreen.chargeCardTitle).should.equals(true);
    }
}

module.exports = ChargeCardPage;
