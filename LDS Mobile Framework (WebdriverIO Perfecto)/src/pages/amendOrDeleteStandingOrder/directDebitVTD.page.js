require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class DirectDebitVTD extends AuthPage {
    get directDebitVTD() {
        return this.getNativeScreen('amendOrDeleteStandingOrder/directDebitVTD.screen');
    }

    get standingOrderVTD() {
        return this.getNativeScreen('amendOrDeleteStandingOrder/standingOrderVTD.screen');
    }

    get deleteDirectDebitInfoScreen() {
        return this.getNativeScreen('amendOrDeleteStandingOrder/deleteDirectDebitInfo.screen');
    }

    shouldSeeDeleteDDButton() {
        Actions.isVisible(this.directDebitVTD.directDebitDelete).should.equal(true);
    }

    selectDeleteDDButton() {
        Actions.click(this.directDebitVTD.directDebitDelete);
    }

    shouldSeeDeleteDDInfoPage() {
        Actions.isVisible(this.deleteDirectDebitInfoScreen.deleteDDInfoPage).should.equal(true);
    }

    verifyCTAButtons() {
        const directDebitName = Actions.getText(this.deleteDirectDebitInfoScreen.deleteDDInfoPage).toLowerCase();
        if (directDebitName.includes('be careful deleting your direct debit')) {
            Actions.isVisible(this.deleteDirectDebitInfoScreen.getHelpButton).should.equal(true);
            Actions.isVisible(this.deleteDirectDebitInfoScreen.deleteDDButton).should.equal(true);
        } else {
            Actions.isVisible(this.deleteDirectDebitInfoScreen.keepDDButton).should.equal(true);
            Actions.isVisible(this.deleteDirectDebitInfoScreen.deleteDDButton).should.equal(true);
        }
    }

    selectSOorDdDeleteOption(type) {
        if (type === 'amend') {
            Actions.click(this.standingOrderVTD.standingOrderAmend);
        } else {
            Actions.click(this.standingOrderVTD.standingOrderDelete);
        }
    }

    shouldSeeOptionsInVTD(type, vtdFields) {
        switch (type) {
        case 'standing order':
            for (let count = 0; count < vtdFields.length; count++) {
                const field = vtdFields[count].vtdFields;
                Actions.waitForVisible(this.standingOrderVTD[field]);
            }
            break;
        case 'direct debit':
            for (let count = 0; count < vtdFields.length; count++) {
                const field = vtdFields[count].vtdFields;
                Actions.waitForVisible(this.directDebitVTD[field]);
            }
            break;

        default:
            throw new Error(`The ${type} option provided is not valid.`);
        }
    }
}

module.exports = DirectDebitVTD;
