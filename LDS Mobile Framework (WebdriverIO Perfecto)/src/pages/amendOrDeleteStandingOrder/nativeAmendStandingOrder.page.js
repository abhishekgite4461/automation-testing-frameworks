require('chai').should();
const AuthPage = require('../common/auth.page');
const DeviceHelper = require('../../utils/device.helper');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class NativeAmendStandingOrder extends AuthPage {
    get nativeAmendStandingOrder() {
        return this.getNativeScreen('amendOrDeleteStandingOrder/nativeAmendStandingOrder.screen');
    }

    static validateLocatorVisibility(isVisible, loc) {
        if (isVisible) {
            Actions.waitForVisible(loc);
        } else {
            Actions.waitForNotVisible(loc);
        }
    }

    waitForPageToLoad(isVisible, screenName) {
        NativeAmendStandingOrder.validateLocatorVisibility(isVisible,
            this.nativeAmendStandingOrder.pageTitle(screenName));
    }

    shouldSeeSelectedAccount(accountName) {
        Actions.getAttributeText(this.nativeAmendStandingOrder.selectedStandingOrderAccount)
            .should.includes(accountName);
    }

    shouldSeeButtonForStandingOrders(isVisible, buttonName, standingOrderName) {
        switch (buttonName) {
        case 'amend':
            NativeAmendStandingOrder.validateLocatorVisibility(isVisible,
                this.nativeAmendStandingOrder.amendButtonForStandingOrder(standingOrderName));
            break;

        case 'delete':
            NativeAmendStandingOrder.validateLocatorVisibility(isVisible,
                this.nativeAmendStandingOrder.deleteButtonForStandingOrder(standingOrderName));
            break;

        default:
            throw new Error(`The ${buttonName} button provided is not valid.`);
        }
    }

    shouldSeeStandingOrdersForAccount(isVisibleSO, standingOrderName, isAmendButtonVisible, isDeleteButtonVisible) {
        NativeAmendStandingOrder.validateLocatorVisibility(isVisibleSO,
            this.nativeAmendStandingOrder.accountNameForStandingOrder(standingOrderName));
        this.shouldSeeButtonForStandingOrders(isVisibleSO ? isAmendButtonVisible : false, 'amend', standingOrderName);
        this.shouldSeeButtonForStandingOrders(isVisibleSO ? isDeleteButtonVisible : false, 'delete', standingOrderName);
    }

    selectSetUpStandingOrderButton() {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.nativeAmendStandingOrder.nativePaymentHubActionMenu);
        }
        Actions.waitAndClick(this.nativeAmendStandingOrder.setUpStandingOrderButton);
    }

    shouldSeeStandingOrdersHeading() {
        Actions.waitForVisible(this.nativeAmendStandingOrder.standingOrdesTitle);
    }

    shouldSeeNoStandingOrderInList() {
        Actions.waitForVisible(this.nativeAmendStandingOrder.noStandingOrdersTitle);
    }

    shouldSeeNoDirectDebitsInList() {
        Actions.waitForVisible(this.nativeAmendStandingOrder.noDirectDebitsHeader);
    }

    shouldSeeUnabletoLoadDirectDebitMessage() {
        Actions.waitForVisible(this.nativeAmendStandingOrder.unableToLoadDirectDebits);
    }

    shouldSeeDDOptions(accountFields) {
        for (let count = 0; count < accountFields.length; count++) {
            const field = accountFields[count].ddFields;
            Actions.isVisible(this.nativeAmendStandingOrder[field]);
        }
    }

    selectFirstDirectDebit() {
        Actions.click(this.nativeAmendStandingOrder.firstDirectDebit);
    }

    selectFirstStandingOrder() {
        Actions.click(this.nativeAmendStandingOrder.firstStandingOrder);
    }

    shouldSeeDeleteOptionForTheDirectDebit() {
        Actions.isVisible(this.nativeAmendStandingOrder.firstDirectDebit).should.equal(true);
    }

    selectDeleteOption() {
        Actions.click(this.nativeAmendStandingOrder.deleteOption);
    }

    swipeFirstDirectDebitToDelete() {
        if (Actions.isVisible(this.nativeAmendStandingOrder.firstDirectDebit)) {
            Actions.swipeOnElement(SwipeAction.LEFT, this.nativeAmendStandingOrder.firstDirectDebit);
            if (DeviceHelper.isIOS()) {
                Actions.waitAndClick(this.nativeAmendStandingOrder.deleteOption);
            }
        } else {
            throw new Error('Direct debits not available');
        }
    }

    swipeFirstStandingOrderToDelete() {
        if (Actions.isVisible(this.nativeAmendStandingOrder.firstStandingOrder)) {
            Actions.swipeOnElement(SwipeAction.LEFT, this.nativeAmendStandingOrder.firstSOAmount);
            if (DeviceHelper.isIOS()) {
                Actions.waitAndClick(this.nativeAmendStandingOrder.deleteOption);
            }
        } else {
            throw new Error('Standing orders not available');
        }
    }

    selectButtonForStandingOrder(buttonName, standingOrderName) {
        switch (buttonName) {
        case 'amend':
            Actions.click(this.nativeAmendStandingOrder.amendButtonForStandingOrder(standingOrderName));
            break;

        case 'delete':
            Actions.click(this.nativeAmendStandingOrder.deleteButtonForStandingOrder(standingOrderName));
            break;

        default:
            throw new Error(`The ${buttonName} button provided is not valid.`);
        }
    }

    shouldSeeSONameInWinBack(standingOrderName) {
        Actions.getAttributeText(this.nativeAmendStandingOrder.winbackText).should.include(standingOrderName);
    }
}

module.exports = NativeAmendStandingOrder;
