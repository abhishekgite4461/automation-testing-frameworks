require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class StandingOrderWebview extends AuthPage {
    get standingOrderWebview() {
        return this.getNativeScreen('amendOrDeleteStandingOrder/standingOrderWebview.screen');
    }

    waitForPageToLoad() {
        Actions.waitForVisible(this.standingOrderWebview.webviewTitle);
    }
}

module.exports = StandingOrderWebview;
