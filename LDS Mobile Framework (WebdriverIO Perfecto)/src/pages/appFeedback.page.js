const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
require('chai').should();

class AppFeedbackPage extends AuthPage {
    /**
     * @returns {AppFeedbackScreen}
     */
    get appFeedbackScreen() {
        return this.getNativeScreen('appFeedback.screen');
    }

    waitForAppFeedbackPageLoad() {
        Actions.waitForVisible(this.appFeedbackScreen.title);
    }

    verifyFeedbackForm() {
        Actions.isVisible(this.appFeedbackScreen.feedbackForm).should.equal(true);
    }
}

module.exports = AppFeedbackPage;
