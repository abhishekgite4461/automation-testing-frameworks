require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class SetUpIn3StepsPage extends AuthPage {
    get setUpIn3StepsScreen() {
        return this.getNativeScreen('accountAggregation/setUpIn3Steps.screen');
    }

    shouldSeeSetUpIn3StepsInformation() {
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.setUpIn3StepsScreen.setUpIn3StepsProviderImage).should.equal(true);
            Actions.isVisible(this.setUpIn3StepsScreen.setUpIn3StepsLoginProviderImage).should.equal(true);
            Actions.isVisible(this.setUpIn3StepsScreen.setUpIn3StepsProviderPermissionImage).should.equal(true);
        }
        Actions.isVisible(this.setUpIn3StepsScreen.setUpIn3StepsTitle).should.equal(true);
        Actions.isVisible(this.setUpIn3StepsScreen.setUpIn3StepsProviderDescription).should.equal(true);
        Actions.isVisible(this.setUpIn3StepsScreen.setUpIn3StepsLoginProviderDescription).should.equal(true);
        Actions.isVisible(this.setUpIn3StepsScreen.setUpIn3StepsProviderPermissionDescription)
            .should.equal(true);
    }

    shouldSeeNextButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.setUpIn3StepsScreen.setUpIn3StepsNextButton);
    }

    selectNextButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.setUpIn3StepsScreen.setUpIn3StepsNextButton);
        Actions.click(this.setUpIn3StepsScreen.setUpIn3StepsNextButton);
    }
}

module.exports = SetUpIn3StepsPage;
