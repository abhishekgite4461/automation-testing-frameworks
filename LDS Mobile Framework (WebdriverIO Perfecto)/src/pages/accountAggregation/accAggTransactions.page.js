require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class TransactionsPage extends AuthPage {
    get transactionsScreen() {
        return this.getNativeScreen('accountAggregation/transactions.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.transactionsScreen.viewTransactionsPage);
    }

    selectDeepLink(isSuccess) {
        if (isSuccess) {
            Actions.click(this.transactionsScreen.successDeepLink);
        } else {
            Actions.click(this.transactionsScreen.failureDeepLink);
        }
    }

    shouldSeeLoggedOffError() {
        Actions.waitForVisible(this.transactionsScreen.loggedOffError);
    }
}

module.exports = TransactionsPage;
