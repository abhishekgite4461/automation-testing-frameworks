require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class SecureAndProtectedPage extends AuthPage {
    get secureAndProtectedScreen() {
        return this.getNativeScreen('accountAggregation/secureAndProtected.screen');
    }

    shouldSeeSecureAndProtectedInformation() {
        Actions.isVisible(this.secureAndProtectedScreen.secureAndProtectedTitle).should.equals(true);
        Actions.isVisible(this.secureAndProtectedScreen.secureAndProtectedFirstInformationPoint).should.equals(true);
        Actions.isVisible(this.secureAndProtectedScreen.secureAndProtectedSecondInformationPoint).should.equals(true);
        Actions.isVisible(this.secureAndProtectedScreen.secureAndProtectedThirdInformationPoint).should.equals(true);
    }

    shouldSeeNextButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.secureAndProtectedScreen.secureAndProtectedNextButton);
    }

    selectNextButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.secureAndProtectedScreen.secureAndProtectedNextButton);
        Actions.click(this.secureAndProtectedScreen.secureAndProtectedNextButton);
    }
}

module.exports = SecureAndProtectedPage;
