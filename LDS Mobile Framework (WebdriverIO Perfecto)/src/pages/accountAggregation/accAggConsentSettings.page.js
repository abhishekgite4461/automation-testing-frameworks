require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ConsentSettingsPage extends AuthPage {
    get consentSettingsScreen() {
        return this.getNativeScreen('accountAggregation/consentSettings.screen');
    }

    get commonScreen() {
        return this.getNativeScreen('common/common.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.consentSettingsScreen.consentSettingsPage, 5000);
    }

    shouldSeeConsentSettingsInformation() {
        Actions.getAttributeText(this.consentSettingsScreen.mainHeaderTitle).toLowerCase().should.contains('manage accounts');
        Actions.isVisible(this.consentSettingsScreen.introductoryPara).should.equal(true);
        Actions.getAttributeText(this.consentSettingsScreen.accountsSectionHeader).toLowerCase().should.contains('accounts');
    }

    shouldNotSeeExternalAccounts() {
        Actions.getAttributeText(this.consentSettingsScreen.accountsSectionHeader).toLowerCase().should.contains('no accounts');
    }

    static getConsentMessageText(loc) {
        Actions.swipeByPercentUntilVisible(loc, 20, 80, 20, 60, 100, 10);
        return Actions.getAttributeText(loc);
    }

    shouldSeeConsentStatus(consentStatus, bankName) {
        switch (consentStatus.toLowerCase()) {
        case 'authorised':
            Actions.swipeByPercentUntilVisible(this.consentSettingsScreen.authorisedAccounts(bankName),
                20, 80, 20, 60, 100, 10);
            break;
        case 'revoked':
            ConsentSettingsPage.getConsentMessageText(this.consentSettingsScreen.consentStatusRevokedMessage(bankName))
                .should.include('removed');
            break;
        case 'expired':
            ConsentSettingsPage.getConsentMessageText(this.consentSettingsScreen.consentStatusExpiredMessage(bankName))
                .should.include('expired');
            break;
        case 'failed':
            ConsentSettingsPage.getConsentMessageText(this.consentSettingsScreen.consentStatusFailedMessage(bankName))
                .should.include('not available');
            break;
        default:
            throw new Error(`Given consent status "${consentStatus.toUpperCase()}" not found`);
        }
    }

    shouldSeeConsentExpiryMessage(expiryStatus, bankName) {
        Actions.swipeByPercentUntilVisible(this.consentSettingsScreen.consentExpiryMessage(bankName),
            20, 80, 20, 60, 100, 10);
        const expiryMessage = Actions.getText(this.consentSettingsScreen.consentExpiryMessage(bankName)).replace(/[0-9]/g, '');
        if (expiryStatus.toLowerCase() === 'expires') {
            expiryMessage.should.contains('Expires in  days');
        } else if (expiryStatus.toLowerCase() === 'expired') {
            expiryMessage.should.contains('Expired  days ago');
        }
    }

    shouldSeeManageButton(isShown, bankName) {
        if (isShown) {
            Actions.swipeByPercentUntilVisible(this.consentSettingsScreen.providerManageButton(bankName),
                20, 80, 20, 60, 100, 10);
        } else {
            Actions.swipeByPercentUntilVisible(this.consentSettingsScreen.consentProviderLabel(bankName),
                20, 80, 20, 60, 100, 10);
            Actions.isVisible(this.consentSettingsScreen.providerManageButton(bankName)).should.equal(isShown);
        }
    }

    selectManageButton(bankName) {
        this.shouldSeeManageButton(true, bankName);
        Actions.click(this.consentSettingsScreen.providerManageButton(bankName));
    }

    shouldSeeManageDrawer(bankName) {
        Actions.waitForVisible(this.consentSettingsScreen.providerManageDrawer, 5000);
        Actions.isVisible(this.consentSettingsScreen.providerManageDrawerHeader(bankName)).should.equal(true);
    }

    shouldSeeGivenOptionsOnManageDrawer(values) {
        values.forEach((value) => {
            Actions.getElementsListAttributeText(this.consentSettingsScreen.providerManageDrawerList).includes(value);
        });
    }

    selectOptionOnManageDrawer(option) {
        Actions.click(this.consentSettingsScreen.providerManageDrawerOption(option));
    }

    waitForAnAlertOnConsentSettingsPage() {
        Actions.waitForVisible(this.consentSettingsScreen.removeConsentAlert, 5000);
        Actions.getText(this.consentSettingsScreen.removeConsentAlertHeader).toLowerCase().should.contains('remove account');
    }

    shouldSeeErrorBanner(isShown) {
        Actions.waitUntilWithoutAssertion(this.consentSettingsScreen.consentSettingErrorBanner);
        Actions.isVisible(this.consentSettingsScreen.consentSettingErrorBanner).should.equal(isShown);
        if (isShown) {
            Actions.click(this.consentSettingsScreen.consentSettingErrorBannerCloseIcon);
            Actions.isVisible(this.consentSettingsScreen.consentSettingErrorBanner).should.equal(false);
        }
    }
}

module.exports = ConsentSettingsPage;
