require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const CommonPage = require('../common.page');
const DeviceHelper = require('../../utils/device.helper');

const commonPage = new CommonPage();
const DEEPLINK_URL_LDS = 'lloyds-retail://account-aggregation';
const DEEPLINK_URL_BOS = 'bos-retail://account-aggregation';
const DEEPLINK_URL_HFX = 'halifax-retail://account-aggregation';

class RedirectingPage extends AuthPage {
    get redirectingScreen() {
        return this.getNativeScreen('accountAggregation/redirecting.screen');
    }

    shouldSeeRedirectErrorScreen(reason) {
        Actions.isExisting(this.redirectingScreen.redirectExternalBrowserErrorScreen).should.equals(true);
        const text = Actions.getText(this.redirectingScreen.redirectExternalBrowserErrorScreenTitle).toLowerCase();
        switch (reason.toLowerCase()) {
        case 'incomplete journey':
            text.should.contains('you haven\'t finished renewing your account sharing');
            break;
        case 'redirect failure':
            text.should.contains('something went wrong');
            break;
        default:
            throw new Error(`Given redirect reason "${reason.toUpperCase()}" not present`);
        }
    }

    shouldSeeRedirectErrorScreenForIncompleteJourney() {
        Actions.getText(this.redirectingScreen.redirectExternalBrowserErrorScreenTitle)
            .toLowerCase().should.contains('you haven\'t finished renewing your account sharing');
    }

    shouldSeeWinBackCancelButton() {
        commonPage.waitForSpinnerToComplete();
        Actions.isVisible(this.redirectingScreen.cancelButton)
            .should.equal(true);
    }

    shouldSeeWinBackContinueButton() {
        commonPage.waitForSpinnerToComplete();
        Actions.isVisible(this.redirectingScreen.continueButton)
            .should.equal(true);
    }

    selectWinBackCancelButton() {
        Actions.click(this.redirectingScreen.cancelButton);
    }

    selectWinBackContinueButton() {
        Actions.click(this.redirectingScreen.continueButton);
    }

    selectWinBackDigitalInboxButton() {
        Actions.click(this.redirectingScreen.digitalInbox);
    }

    shouldSeeProviderNameOnRedirectingScreen(bankName) {
        Actions.isExisting(this.redirectingScreen.redirectLoadingProgress).should.equals(true);
        Actions.getAttributeText(this.redirectingScreen.redirectText).toLowerCase()
            .should.contains(bankName.toLowerCase());
    }

    shouldSeeLogOffErrorPage() {
        Actions.waitForVisible(this.redirectingScreen.loggingOffUserErrorScreen);
        if (browser.capabilities.brand === 'BOS') {
            Actions.getText(this.redirectingScreen.logOffErrorScreenTitle)
                .toLowerCase().should.contains('logged out');
        } else if (browser.capabilities.brand === 'HFX') {
            Actions.getText(this.redirectingScreen.logOffErrorScreenTitle)
                .toLowerCase().should.contains('signed out');
        } else {
            Actions.getText(this.redirectingScreen.logOffErrorScreenTitle)
                .toLowerCase().should.contains('logged off');
        }
    }

    triggerDeeplink() {
        let deepLinkURL;
        if (browser.capabilities.brand === 'HFX') {
            deepLinkURL = DEEPLINK_URL_HFX;
        } else if (browser.capabilities.brand === 'LDS') {
            deepLinkURL = DEEPLINK_URL_LDS;
        } else {
            deepLinkURL = DEEPLINK_URL_BOS;
        }

        if (DeviceHelper.isIOS()) {
            Actions.sendKeys(this.redirectingScreen.browserAddress, deepLinkURL);
            Actions.waitUntilWithoutAssertion(this.redirectingScreen.openAppButton);
            if (Actions.isVisible(this.redirectingScreen.openAppButton)) {
                Actions.click(this.redirectingScreen.openAppButton);
            }
        } else {
            Actions.waitForNotVisible(this.redirectingScreen.redirectLoadingProgress, 10000);
            Actions.pause(3000);
            Actions.launchDeeplink(deepLinkURL);
        }
    }

    shouldSeeIncompleteRenewError() {
        Actions.getAttributeText(this.redirectingScreen.redirectExternalBrowserErrorScreenTitle).toLowerCase()
            .should.include('finished renewing your account sharing');
    }

    shouldSeeRedirectHomeButton() {
        Actions.isVisible(this.redirectingScreen.redirectHomeButton).should.equal(true);
    }

    selectRedirectHomeButton() {
        Actions.click(this.redirectingScreen.redirectHomeButton);
    }

    navigateBackToApp() {
        Actions.waitUntilWithoutAssertion(this.redirectingScreen.accessLocationButton);
        if (Actions.isVisible(this.redirectingScreen.accessLocationButton)) {
            Actions.click(this.redirectingScreen.accessLocationButton);
        }
        if (DeviceHelper.isIOS()) {
            if (Actions.isVisible(this.redirectingScreen.backToAppButtonOld)) {
                Actions.click(this.redirectingScreen.backToAppButtonOld);
            } else { Actions.click(this.redirectingScreen.backToAppButtonNew); }
        } else {
            let count = 0;
            while (!Actions.isVisible(this.redirectingScreen.redirectExternalBrowserErrorScreenTitle) && count < 5) {
                Actions.pressAndroidBackKey();
                Actions.waitUntilWithoutAssertion(this.redirectingScreen.redirectExternalBrowserErrorScreenTitle);
                count++;
            }
        }
    }

    switchToApp() {
        let count = 0;
        Actions.pause(3000);
        while (!Actions.isVisible(this.redirectingScreen.redirectExternalBrowserErrorScreenTitle) && count < 3) {
            Actions.launchAppFromBackground();
            count++;
        }
    }
}

module.exports = RedirectingPage;
