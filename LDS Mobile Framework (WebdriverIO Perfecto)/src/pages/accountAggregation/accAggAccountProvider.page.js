require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class AccountProviderPage extends AuthPage {
    get accountProviderScreen() {
        return this.getNativeScreen('accountAggregation/accountProvider.screen');
    }

    shouldSeeProviderNameAndLogo() {
        Actions.isVisible(this.accountProviderScreen.accountProviderTitle).should.equals(true);
        Actions.isVisible(this.accountProviderScreen.accountProviderTextView).should.equals(true);
        Actions.isVisible(this.accountProviderScreen.accountProviderImageView).should.equals(true);
        Actions.isVisible(this.accountProviderScreen.accountProviderListView).should.equals(true);
    }

    selectAccountProvider(bankName) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.accountProviderScreen.providerBankName(bankName));
        Actions.click(this.accountProviderScreen.providerBankName(bankName));
    }

    shouldSeeFooterInformation() {
        if (DeviceHelper.isAndroid()) {
            Actions.swipeByPercentUntilVisible(this.accountProviderScreen.accountProviderFooterTitle,
                80, 80, 20, 20, 50, 5);
            Actions.swipeByPercentUntilVisible(this.accountProviderScreen.accountProviderFooterMessage,
                80, 80, 20, 20, 50, 5);
            Actions.swipeByPercentage(20, 20, 80, 80, 50);
        }
    }
}

module.exports = AccountProviderPage;
