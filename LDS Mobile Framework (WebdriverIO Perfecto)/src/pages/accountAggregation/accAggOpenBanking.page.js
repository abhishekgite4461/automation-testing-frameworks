require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class OpenBankingPage extends AuthPage {
    get openBankingScreen() {
        return this.getNativeScreen('accountAggregation/openBanking.screen');
    }

    shouldSeeScreen(isVisible, screenName) {
        if (isVisible) {
            Actions.waitForVisible(this.openBankingScreen.screenTitle(screenName));
        } else {
            Actions.waitForNotVisible(this.openBankingScreen.screenTitle(screenName));
        }
    }

    shouldSeeBackButton(isBackButtonVisible) {
        Actions.isVisible(this.openBankingScreen.backIcon)
            .should.equal(isBackButtonVisible);
    }

    selectCrossButton() {
        Actions.click(this.openBankingScreen.crossIcon);
    }

    shouldSeeOpenBankingInformation() {
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.openBankingScreen.openBankingImage).should.equal(true);
        }
        Actions.isVisible(this.openBankingScreen.openBankingInformationHeader).should.equal(true);
        Actions.isVisible(this.openBankingScreen.openBankingInformationHeaderDescription).should.equal(true);
    }

    shouldSeeNextButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.openBankingScreen.openBankingNextButton);
    }

    selectNextButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.openBankingScreen.openBankingNextButton);
        Actions.click(this.openBankingScreen.openBankingNextButton);
    }
}

module.exports = OpenBankingPage;
