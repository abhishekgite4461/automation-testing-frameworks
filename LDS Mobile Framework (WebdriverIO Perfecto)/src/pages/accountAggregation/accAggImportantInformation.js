require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class ImportantInformationPage extends AuthPage {
    get importantInformationScreen() {
        return this.getNativeScreen('accountAggregation/importantInformation.screen');
    }

    shouldSeeImportantInformation() {
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.importantInformationScreen.importantInformationBankImage).should.equals(true);
            Actions.isVisible(this.importantInformationScreen.importantInformationSelectedBankImage)
                .should.equals(true);
        }
        Actions.isVisible(this.importantInformationScreen.importantInformationGraphicImage).should.equals(true);
        Actions.isVisible(this.importantInformationScreen.importantInformationFirstBulletDescription)
            .should.equals(true);
        Actions.isVisible(this.importantInformationScreen.importantInformationSecondBulletDescription)
            .should.equals(true);
        Actions.swipePage(SwipeAction.UP);
        Actions.isVisible(this.importantInformationScreen.importantInformationThirdBulletDescription)
            .should.equals(true);
        Actions.isVisible(this.importantInformationScreen.importantInformationForthBulletDescription)
            .should.equals(true);
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.importantInformationScreen.importantInformationText);
    }

    shouldSeeRenewConsentInformation(bankName) {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.importantInformationScreen.importantInformationGraphicImage);
        Actions.getAttributeText(this.importantInformationScreen.importantInformationText, 'label').should.contains(bankName);
        Actions.getAttributeText(this.importantInformationScreen.importantInformationText, 'label').should.contains('renew');
        Actions.getAttributeText(this.importantInformationScreen.importantInformationText, 'label').should.contains('90 days');
        Actions.swipeUntilVisible(SwipeAction.UP, this.importantInformationScreen.importantInformationRenewConsent);
    }

    shouldSeeAccountProviderName(bankName) {
        Actions.getAttributeText(this.importantInformationScreen.importantInformationText)
            .should.contains(bankName);
    }

    shouldSeeTnCCheckBoxChecked() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.importantInformationScreen.importantInformationConfirmButton);
        Actions.isExisting(this.importantInformationScreen.importantInformationTnCDesciption).should.equals(true);
        Actions.isChecked(this.importantInformationScreen.importantInformationTnC).should.equals(false);
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.importantInformationScreen.importantInformationText);
    }

    shouldSeeTnCCheckbox(isVisible) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.importantInformationScreen.importantInformationConfirmButton);
        Actions.isVisible(this.importantInformationScreen.importantInformationTnC).should.equals(isVisible);
    }

    shouldSeeConsentCheckBoxChecked() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.importantInformationScreen.importantInformationConfirmButton);
        Actions.isVisible(this.importantInformationScreen.importantInformationConsentDescription).should.equals(true);
        Actions.isChecked(this.importantInformationScreen.importantInformationConsent).should.equals(false);
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.importantInformationScreen.importantInformationText);
    }

    agreeTnC(isChecked) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.importantInformationScreen.importantInformationConfirmButton);
        if (Actions.isChecked(this.importantInformationScreen.importantInformationTnC) === !isChecked) {
            Actions.click(this.importantInformationScreen.importantInformationTnC);
        }
        Actions.isChecked(this.importantInformationScreen.importantInformationTnC).should.equals(isChecked);
    }

    agreeConsent(isChecked) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.importantInformationScreen.importantInformationConfirmButton);
        if (Actions.isChecked(this.importantInformationScreen.importantInformationConsent) === !isChecked) {
            Actions.click(this.importantInformationScreen.importantInformationConsent);
        }
        Actions.isChecked(this.importantInformationScreen.importantInformationConsent).should.equals(isChecked);
    }

    shouldSeeRenewConsentCheckBoxChecked(isChecked) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.importantInformationScreen.importantInformationRenewConsent);
        Actions.isChecked(this.importantInformationScreen.importantInformationRenewConsent).should.equals(isChecked);
    }

    selectRenewConsentCheckBox() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.importantInformationScreen.importantInformationRenewConsent);
        Actions.click(this.importantInformationScreen.importantInformationRenewConsent);
    }

    shouldSeeConfirmButtonEnabled(isEnabled) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.importantInformationScreen.importantInformationConfirmButton);
        Actions.isEnabled(this.importantInformationScreen.importantInformationConfirmButton).should.equals(isEnabled);
    }

    selectConfirmButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.importantInformationScreen.importantInformationConfirmButton);
        Actions.click(this.importantInformationScreen.importantInformationConfirmButton);
    }
}

module.exports = ImportantInformationPage;
