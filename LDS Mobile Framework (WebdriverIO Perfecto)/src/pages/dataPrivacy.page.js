const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class DataPrivacyPage extends AuthPage {
    get dataPrivacyWebScreen() {
        return this.getWebScreen('dataPrivacyWeb.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.dataPrivacyWebScreen.title);
    }
}

module.exports = DataPrivacyPage;
