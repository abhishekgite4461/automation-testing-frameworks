const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class RenewSavingsAccountPage extends AuthPage {
    /**
     * @returns {RenewSavingsAccountScreen}
     */
    get renewSavingsAccountScreen() {
        return this.getNativeScreen('renewSavingsAccount.screen');
    }

    waitForPageLoad() {
        if (Actions.isVisible(this.renewSavingsAccountScreen.title)) {
            Actions.click(this.renewSavingsAccountScreen.renew);
        }
    }

    selectRenew() {
        Actions.click(this.renewSavingsAccountScreen.renew);
    }

    verifyRenewAccountPage() {
        Actions.isVisible(this.renewSavingsAccountScreen.renewAccountPage);
    }

    selectSavingAccount() {
        let i = 0;
        while (i < 5) {
            Actions.swipePage(SwipeAction.UP, 800);
            Actions.pause(1000);
            i++;
        }
        Actions.click(this.renewSavingsAccountScreen.savingAccount);
    }

    selectRenewSaving() {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.renewSavingsAccountScreen.renewSaving);
    }

    selectApply() {
        // Actions.pause(1000);
        let i = 0;
        while (i < 20) {
            Actions.swipePage(SwipeAction.UP, 800);
            Actions.pause(1000);
            i++;
        }
        Actions.click(this.renewSavingsAccountScreen.applyNow);
    }

    verifyConfirmPage() {
        Actions.isVisible(this.renewSavingsAccountScreen.confirmPage);
    }

    selectTermsAndConditions() {
        let i = 0;
        while (i < 12) {
            Actions.swipePage(SwipeAction.UP, 800);
            Actions.pause(1000);
            i++;
        }
        Actions.click(this.renewSavingsAccountScreen.termsAndConditions);
    }

    selectConfirm() {
        Actions.click(this.renewSavingsAccountScreen.confirm);
    }

    verifySuccessMessage() {
        Actions.isVisible(this.renewSavingsAccountScreen.successPage);
    }
}

module.exports = RenewSavingsAccountPage;
