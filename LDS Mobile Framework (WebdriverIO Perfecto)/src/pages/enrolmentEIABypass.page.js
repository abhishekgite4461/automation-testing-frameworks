const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class EnrolmentEIABypassPage extends AuthPage {
    /**
     * @returns {EnrolmentEIABypassScreen}
     */
    get enrolmentEIABypassScreen() {
        return this.getNativeScreen('enrolmentEIABypass.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.enrolmentEIABypassScreen.title);
    }

    clickContinueOnEIABypassPage() {
        Actions.click(this.enrolmentEIABypassScreen.continueButton);
    }
}

module.exports = EnrolmentEIABypassPage;
