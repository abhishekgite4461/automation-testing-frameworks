require('chai').should();
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');
const UnAuthPage = require('./common/unAuth.page');
const DeviceHelper = require('../utils/device.helper');

class EnrolmentCardReaderRespondPage extends UnAuthPage {
    /**
     * @returns {EnrolmentCardReaderRespondScreen}
     */
    get cardReaderRespondScreen() {
        return this.getNativeScreen('enrolmentCardReaderRespond.screen');
    }

    shouldSeeCardReaderRespondPage() {
        Actions.waitForVisible(this.cardReaderRespondScreen.title);
        return Actions.isVisible(this.cardReaderRespondScreen.title);
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.cardReaderRespondScreen.title);
    }

    waitForCardReaderRespondPage(type) {
        const passcode = '00000000';
        if (type === 'respond') {
            this.waitForPageLoad();
        } else {
            this.waitForCardReaderAuthenticationPageLoad();
        }
        this.enterPasscode(passcode);
        this.selectContinueButton();
    }

    waitForCardReaderAuthenticationPageLoad() {
        Actions.waitForVisible(this.cardReaderRespondScreen.paymentCardReaderAuthenticationTitle);
    }

    enterPasscode(passcode) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.cardReaderRespondScreen.continueButton);
        Actions.sendKeys(this.cardReaderRespondScreen.passcodeField, passcode);
    }

    shouldSeeContinueButtonDisabled() {
        Actions.isEnabled(this.cardReaderRespondScreen.continueButton).should.equal(false);
    }

    openHavingTroubleWithCardReaderLink() {
        Actions.click(this.cardReaderRespondScreen.havingTroubleWithCardReaderLink);
    }

    validateChallengeCodeLength() {
        Actions.getText(this.cardReaderRespondScreen.challengeCode, 'value').trim().length
            .should.equal(6);
    }

    validatePrePopulatedCardLength() {
        Actions.getText(this.cardReaderRespondScreen.lastFiveCardDigitField, 'value').trim().length
            .should.equal(5);
    }

    enterLastFiveCardDigits(cardNumber) {
        if (DeviceHelper.isIOS()) {
            Actions.swipeUntilVisible(SwipeAction.DOWN, this.cardReaderRespondScreen.lastFiveCardDigitField);
        }
        const cardNumbersArray = cardNumber.split('|');
        Actions.setImmediateValue(this.cardReaderRespondScreen.lastFiveCardDigitField, cardNumbersArray[0].slice(-5));
        Actions.tapOutOfField();
        if (DeviceHelper.isAndroid()) {
            Actions.pressAndroidBackKey();
        }
    }

    compareCardDetails(cardNumber) {
        const cardNumbersArray = cardNumber.split('|');
        const lastFiveCardDigit = Actions.getText(this.cardReaderRespondScreen.lastFiveCardDigitField, 'value').trim();
        const cardMatched = (() => {
            for (let count = 0; count < cardNumbersArray.length; count++) {
                if (lastFiveCardDigit === (cardNumbersArray[count].slice(-5))) {
                    return true;
                }
            }
            return false;
        })();
        if (!cardMatched) {
            throw new Error(`Card ending with "${lastFiveCardDigit}" not found for the user!`);
        }
    }

    shouldSeeEnterPasscodeField() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.cardReaderRespondScreen.passcodeField);
    }

    verifyCardDetailsEditableVisiblity(type) {
        if (type === 'editable') {
            Actions.isEnabled(this.cardReaderRespondScreen.lastFiveCardDigitField).should.equal(true);
        } else if (type === 'non-editable') {
            Actions.isEnabled(this.cardReaderRespondScreen.lastFiveCardDigitField).should.equal(false);
        }
    }

    shouldSeeProblemWithCardReaderLink() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.cardReaderRespondScreen.havingTroubleWithCardReaderLink);
    }

    verifyProblemWithCardReaderPopUp() {
        Actions.waitForVisible(this.cardReaderRespondScreen.havingTroubleWithCardReaderPopUpTitle);
        Actions.isVisible(this.cardReaderRespondScreen.havingTroublePopUpContinueButton).should.equal(true);
        Actions.isVisible(this.cardReaderRespondScreen.havingTroublePopUpCloseButton).should.equal(true);
    }

    verifyClosingProblemWithCardReaderPopUp() {
        Actions.click(this.cardReaderRespondScreen.havingTroublePopUpCloseButton);
        Actions.isVisible(this.cardReaderRespondScreen.title);
    }

    selectContinueOnProblemWithCardReaderPopUp() {
        Actions.click(this.cardReaderRespondScreen.havingTroublePopUpContinueButton);
    }

    selectContinueButton() {
        Actions.swipeByPercentUntilVisible(this.cardReaderRespondScreen.continueButton);
        Actions.isVisible(this.cardReaderRespondScreen.continueButton).should.equal(true);
        Actions.click(this.cardReaderRespondScreen.continueButton);
    }

    shouldSeeErrorBanner(errorType) {
        let errorBannerLocator;
        switch (errorType) {
        case 'invalid card':
            errorBannerLocator = this.cardReaderRespondScreen.invalidCardError;
            break;
        case 'card expired':
            errorBannerLocator = this.cardReaderRespondScreen.expiredCardError;
            break;
        case 'incorrect passcode':
            errorBannerLocator = this.cardReaderRespondScreen.incorrectPasscodeError;
            break;
        case 'card already exist':
            errorBannerLocator = this.cardReaderRespondScreen.cardAlreadyExistError;
            break;
        default:
            throw new Error(`Unknown error type: ${errorType}`);
        }
        Actions.isExisting(errorBannerLocator).should.equal(true);
    }

    verifyContinueButtonEnableVisibility(type) {
        Actions.swipeByPercentUntilVisible(this.cardReaderRespondScreen.continueButton);
        if (type === 'enabled') {
            Actions.isEnabled(this.cardReaderRespondScreen.continueButton).should.equal(true);
        } else {
            Actions.isEnabled(this.cardReaderRespondScreen.continueButton).should.equal(false);
        }
    }

    clearPasscodeField() {
        Actions.clearText(this.cardReaderRespondScreen.passcodeField);
    }

    clearCardDigitsField() {
        Actions.clearText(this.cardReaderRespondScreen.lastFiveCardDigitField);
        if (DeviceHelper.isIOS()) {
            Actions.tapOutOfField();
        }
    }

    enterInsufficientDetails(passcode, cardNumber) {
        Actions.setImmediateValueAndTapOut(this.cardReaderRespondScreen.lastFiveCardDigitField, cardNumber.split('|')[0].slice(-4));
        Actions.swipeUntilVisible(SwipeAction.UP, this.cardReaderRespondScreen.passcodeField);
        Actions.setImmediateValueAndTapOut(this.cardReaderRespondScreen.passcodeField,
            passcode.substring(0, passcode.length - 1));
    }
}

module.exports = EnrolmentCardReaderRespondPage;
