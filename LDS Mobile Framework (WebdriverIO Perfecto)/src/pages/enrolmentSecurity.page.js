require('chai').should();
const DeviceHelper = require('../utils/device.helper');
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');

class EnrolmentSecurityPage extends UnAuthPage {
    /**
     * @returns {EnrolmentSecurityScreen}
     */
    get enrolmentSecurityScreen() {
        return this.getNativeScreen('enrolmentSecurity.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.enrolmentSecurityScreen.title);
    }

    clickContinueOnSecurityEnrolmentPage() {
        Actions.click(this.enrolmentSecurityScreen.continueButton);
    }

    verifyDefaultTimeSettings() {
        if (DeviceHelper.isIOS()) {
            Actions.getAttribute(this.enrolmentSecurityScreen.defaultTime, 'label').should.contain('Currently selected');
        } else {
            Actions.getAttribute(this.enrolmentSecurityScreen.defaultTime,
                DeviceHelper.isPerfecto() ? 'contentDesc' : 'content-desc').should.contain('Currently selected');
        }
    }
}

module.exports = EnrolmentSecurityPage;
