const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class PreferenceUpdatePopUpPage extends AuthPage {
    get preferenceUpdatePopUpScreen() {
        return this.getNativeScreen('marketingPreferences/preferenceUpdatePopUp.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.preferenceUpdatePopUpScreen.title);
    }
}

module.exports = PreferenceUpdatePopUpPage;
