require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');
const SwipeAction = require('../../enums/swipeAction.enum');

class YourProfilePage extends AuthPage {
    get yourProfileScreen() {
        return this.getNativeScreen('marketingPreferences/yourProfile.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.yourProfileScreen.title);
    }

    toggleSalesMessaging() {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.yourProfileScreen.salesMessageToggleButton);
        } else {
            if (Actions.isVisible(this.yourProfileScreen.salesMessageToggleButtonOFF)) {
                Actions.click(this.yourProfileScreen.salesMessageToggleButtonOFF);
            } else if (Actions.click(this.yourProfileScreen.salesMessageToggleButtonON)) {
                Actions.click(this.yourProfileScreen.salesMessageToggleButtonON);
            }
            Actions.click(this.yourProfileScreen.okayButton);
        }
    }

    toggleEmailUpdate() {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.yourProfileScreen.emailUpdateToggleButton);
        } else if (Actions.isVisible(this.yourProfileScreen.emailUpdateToggleButtonOFF)) {
            Actions.click(this.yourProfileScreen.emailUpdateToggleButtonOFF);
        } else if (Actions.click(this.yourProfileScreen.emailUpdateToggleButtonON)) {
            Actions.click(this.yourProfileScreen.emailUpdateToggleButtonON);
        }
    }

    toggleSmsUpdate() {
        if (DeviceHelper.isAndroid()) {
            Actions.swipeUntilVisible(
                SwipeAction.UP,
                this.yourProfileScreen.smsUpdateToggleButton
            );
            Actions.click(this.yourProfileScreen.smsUpdateToggleButton);
        } else {
            Actions.swipePage(SwipeAction.UP);
            if (Actions.isVisible(this.yourProfileScreen.smsUpdateToggleButtonOFF)) {
                Actions.click(this.yourProfileScreen.smsUpdateToggleButtonOFF);
            } else if (Actions.click(this.yourProfileScreen.smsUpdateToggleButtonON)) {
                Actions.click(this.yourProfileScreen.smsUpdateToggleButtonON);
            }
        }
    }
}

module.exports = YourProfilePage;
