const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
require('chai').should();

class ExpiredOtpErrorPage extends AuthPage {
    /**
     * @returns {ExpiredOtpErrorPage}
     */
    get expiredOtpErrorScreen() {
        return this.getNativeScreen('expiredOtpError.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.expiredOtpErrorScreen.title);
    }

    verifyExpiredOtpErrorMessage() {
        Actions.isVisible(this.expiredOtpErrorScreen.expiredOtpErrorMessage).should.equal(true);
        Actions.isVisible(this.expiredOtpErrorScreen.goToLogonButton).should.equal(true);
    }
}

module.exports = ExpiredOtpErrorPage;
