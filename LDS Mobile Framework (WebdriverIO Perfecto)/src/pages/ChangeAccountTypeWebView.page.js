require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class ChangeAccountTypeWebViewPage extends AuthPage {
    /**
     * @returns {ChangeAccontTypeScreen}
     */
    get changeAccountType() {
        return this.getWebScreen('changeAccountType.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.changeAccountType.changeAccountPagetitle, 5000);
    }

    selectChangeThisAccountButton() {
        Actions.click(this.changeAccountType.changeThisAccountButton);
        browser.pause(5000);
    }

    waitForUpdateYourEmail() {
        Actions.waitForVisible(this.changeAccountType.updateYoueEmailAddressPagetitle, 5000);
    }

    selectEmailOptionButton() {
        Actions.click(this.changeAccountType.correctEmailButton);
        browser.pause(5000);
    }

    selectContinueButton() {
        Actions.waitForVisible(this.changeAccountType.continueButton, 5000);
        Actions.click(this.changeAccountType.continueButton);
        browser.pause(5000);
    }

    waitForSelectAccountYouWant() {
        Actions.waitForVisible(this.changeAccountType.selectAccountYouWantPagetitle, 5000);
    }

    selectFindOutMoreButton() {
        Actions.waitForVisible(this.changeAccountType.findOutMoreButton, 5000);
        Actions.click(this.changeAccountType.findOutMoreButton);
        browser.pause(5000);
    }

    waitYourDetailsPage() {
        Actions.waitForVisible(this.changeAccountType.yourDetailsPage, 3000);
    }

    waitForAccountYouWantPage() {
        Actions.waitForVisible(this.changeAccountType.accountYouWantPage, 5000);
    }

    waitForYourAccountOfferPage() {
        Actions.waitForVisible(this.changeAccountType.yourAccountOfferPage, 5000);
    }

    selectCompleteApplicationTickBox() {
        Actions.click(this.changeAccountType.completeApplicationTickBox);
        browser.pause(5000);
    }

    selectCompleteApplicationButton() {
        Actions.click(this.changeAccountType.completeApplicationButton);
        browser.pause(5000);
    }

    waitForBeforeYouStartPage() {
        Actions.waitForVisible(this.changeAccountType.beforeYouStartPage, 5000);
    }

    waitOpenCurrentAccountSuccessMessage() {
        Actions.waitForVisible(this.changeAccountType.openAccountSuccessMessage, 5000);
    }

    selectBeforeYouStartContinueButton() {
        Actions.click(this.changeAccountType.beforeYouStartContinueButton);
    }

    selectYourDetailsContinueButton() {
        Actions.click(this.changeAccountType.yourDetsilsContinueButtonContinueButton);
    }

    selectSelectTitle() {
        Actions.click(this.changeAccountType.selectTitle);
    }

    enterFirstName(firstName) {
        Actions.setValue(this.changeAccountType.aboutYourfirstname, firstName);
    }

    enterMiddleName(middleName) {
        Actions.setValue(this.changeAccountType.aboutYourmiddlename, middleName);
    }

    enterLastName(lastName) {
        Actions.setValue(this.changeAccountType.aboutYourlastname, lastName);
    }

    enterDOBDate(date) {
        Actions.setValue(this.changeAccountType.aboutYourDobDate, date);
    }

    enterDOBMonths(month) {
        Actions.setValue(this.changeAccountType.aboutYourDobMonths, month);
        Actions.tapOutOfField();
    }

    enterDOBYears(year) {
        Actions.setValue(this.changeAccountType.aboutYourDobYears, year);
    }

    enterAboutYouGenders() {
        Actions.click(this.changeAccountType.aboutYourGenders);
    }

    selectYourMartialStatus() {
        Actions.click(this.changeAccountType.aboutYourMartialStatus);
    }

    selectYourNationality() {
        Actions.click(this.changeAccountType.aboutYourNationality);
    }

    selectYourCountryOfBirth() {
        Actions.click(this.changeAccountType.aboutYourcountryOfBirth);
    }

    selectWhereToLive() {
        Actions.click(this.changeAccountType.whereToLive);
        Actions.click(this.changeAccountType.enterAddressYourselfurself);
    }

    enterPostCode(postCode) {
        Actions.setValue(this.changeAccountType.postCode, postCode);
    }

    enterYourAddress() {
        Actions.click(this.changeAccountType.findYourAddress);
    }

    enterHouseNumber(houseNumber) {
        Actions.setValue(this.changeAccountType.houseNumber, houseNumber);
        Actions.tapOutOfField();
    }

    enterHouseName(houseName) {
        Actions.setValue(this.changeAccountType.houseName, houseName);
        Actions.tapOutOfField();
    }

    enterStreetName(streetName) {
        Actions.setValue(this.changeAccountType.streetName, streetName);
    }

    enterCityName(cityName) {
        Actions.setValue(this.changeAccountType.cityName, cityName);
    }

    enterCountyName(countyName) {
        Actions.setValue(this.changeAccountType.countyName, countyName);
    }

    enterWhereYouLivepostCode(whereYouLivepostCode) {
        Actions.setValue(this.changeAccountType.whereYouLivepostCode, whereYouLivepostCode);
    }

    selectMoveYourCurrentAddress() {
        Actions.click(this.changeAccountType.currentAddressDurationOfStayYear);
        Actions.click(this.changeAccountType.currentAddressDurationOfStayMonth);
        Actions.click(this.changeAccountType.currentAddressResidentialStatus);
    }

    enterMobileNumber(mobile) {
        Actions.click(this.changeAccountType.contactDetailsFillDetails);
        Actions.setValue(this.changeAccountType.phoneNumber, mobile);
    }

    enterEmailAddress(email) {
        Actions.setValue(this.changeAccountType.emailAddress, email);
        Actions.setValue(this.changeAccountType.confirmEmailAddress, email);
    }

    selectMarketingChoices() {
        Actions.click(this.changeAccountType.marketingPreferencesOption);
        Actions.click(this.changeAccountType.marketingPreferencesInternetBanking);
        Actions.click(this.changeAccountType.marketingPreferencesEmail);
        Actions.click(this.changeAccountType.marketingPreferencesPost);
        Actions.click(this.changeAccountType.marketingPreferencesDeviceMessaging);
        Actions.click(this.changeAccountType.marketingPreferencesDeviceMessagingText);
        Actions.click(this.changeAccountType.marketingPreferencesDeviceMessagingPhone);
    }

    enterIncomeExpenses(monthly, mortgages, childCareCost) {
        Actions.click(this.changeAccountType.incomeAndExpenses);
        Actions.setValue(this.changeAccountType.monthlyTakeHome, monthly);
        Actions.click(this.changeAccountType.monthlyIncomeToBePaid);
        Actions.click(this.changeAccountType.payMoneyAccount);
        Actions.click(this.changeAccountType.employementStatus);
        Actions.setValue(this.changeAccountType.payMortgage, mortgages);
        Actions.setValue(this.changeAccountType.spendingChild, childCareCost);
        Actions.click(this.changeAccountType.savingsAmount);
    }

    shouldSeeReviewYourDetailsPage() {
        Actions.waitForVisible(this.changeAccountType.reviewYourDetailsPage, 3000);
    }

    shouldSeeYourAccountOfferPage() {
        Actions.waitForVisible(this.changeAccountType.reviewYourDetailsPage, 3000);
    }

    confirmCheckboxOpenAccount() {
        Actions.click(this.changeAccountType.confirmOpenAccount);
    }

    waitForReferDecisionMessagePage() {
        Actions.waitForVisible(this.changeAccountType.verifyReferDecisionMessage, 3000);
    }

    selectAnotherCurrentButton() {
        Actions.waitForVisible(this.changeAccountType.openAnotherCurrentAccount, 7000);
        Actions.click(this.changeAccountType.openAnotherCurrentAccount);
    }

    waitForBeforeYouContinue() {
        Actions.waitForVisible(this.changeAccountType.beforeYouContinuePageTitle, 5000);
    }

    selectAgeAndResident() {
        Actions.click(this.changeAccountType.ageYes);
        Actions.click(this.changeAccountType.residentYes);
    }

    selectBeforeYouContinueButton() {
        Actions.waitForVisible(this.changeAccountType.beforeYouContinueButton, 5000);
        Actions.click(this.changeAccountType.beforeYouContinueButton);
    }

    waitForYourNeedsTitle() {
        Actions.waitForVisible(this.changeAccountType.YourNeedsTitle, 5000);
    }

    waitForFamilyTravelInsuranceHeader() {
        Actions.waitForVisible(this.changeAccountType.FamilyTravelInsuranceHeader, 5000);
    }

    selectFamilyTravelOptions() {
        Actions.click(this.changeAccountType.haveSpouseYes);
        Actions.click(this.changeAccountType.haveChildNo);
        Actions.click(this.changeAccountType.similarTravelInsNo);
        Actions.click(this.changeAccountType.familyTravelAbrodYes);
        Actions.click(this.changeAccountType.wantTranvelInsYes);
    }

    selectYourNeedsContinue() {
        Actions.waitForVisible(this.changeAccountType.yourNeedsContinue, 5000);
        Actions.click(this.changeAccountType.yourNeedsContinue);
    }

    waitForVehicleBreakdownHeader() {
        Actions.waitForVisible(this.changeAccountType.vehicleBreakdownHeader, 5000);
    }

    selectVehicleBreakdownOptions() {
        Actions.click(this.changeAccountType.similarBreakCoverNo);
        Actions.click(this.changeAccountType.travelVehicleYes);
        Actions.click(this.changeAccountType.responsibleYes);
        Actions.click(this.changeAccountType.breakdownCoverYes);
    }

    waitForMobilePhoneHeader() {
        Actions.waitForVisible(this.changeAccountType.mobilePhoneHeader, 5000);
    }

    selectMobilePhoneOptions() {
        Actions.click(this.changeAccountType.alreadyHaveMobileInsNo);
        Actions.click(this.changeAccountType.ownMobilePhoneYes);
        Actions.click(this.changeAccountType.payExecessPhoneYes);
        Actions.click(this.changeAccountType.wantMobileInsYes);
    }

    waitForHomeEmergencyHeader() {
        Actions.waitForVisible(this.changeAccountType.homeEmergencyHeader, 5000);
    }

    selectHomeEmergencyOptions() {
        Actions.click(this.changeAccountType.similarCoverNo);
        Actions.click(this.changeAccountType.payEmergencyRepairYes);
        Actions.click(this.changeAccountType.wantHomeEmerCoverYes);
    }

    waitForYourNeedsSummaryHeader() {
        Actions.waitForVisible(this.changeAccountType.yourNeedsSummaryHeader, 5000);
    }

    waitForYourEligibilityTitle() {
        Actions.waitForVisible(this.changeAccountType.yourEligibilityTitle, 5000);
    }

    selectYourEligibilityContinue() {
        Actions.waitForVisible(this.changeAccountType.yourEligibilityContinue, 5000);
        Actions.click(this.changeAccountType.yourEligibilityContinue);
        browser.pause(5000);
    }

    selectDobAndResident(day, month, year) {
        Actions.setValue(this.changeAccountType.yourEligibilityDay, day);
        Actions.click(this.changeAccountType.yourEligibilityMonth, month);
        Actions.setValue(this.changeAccountType.yourEligibilityYear, year);
        Actions.click(this.changeAccountType.yourEligibilityResidentYes);
    }

    selectMedicalConditionButtons() {
        Actions.click(this.changeAccountType.medicalConditionButtonSelfNo);
        Actions.click(this.changeAccountType.medicalConditionButtonPartnerNo);
    }

    selectVehicleBreakdownFinalCheckButton() {
        Actions.click(this.changeAccountType.selectVehicleBreakdownFinalCheckNo);
    }

    selectMobileInsuranceFinalCheckButton() {
        Actions.click(this.changeAccountType.mobileInsuranceFinalCheckButton);
    }

    selectHomeEmergencyFinalCheckButton() {
        Actions.click(this.changeAccountType.homeEmergencyFinalCheckButton);
    }

    waitForYourEligibilitySummaryHeader() {
        Actions.waitForVisible(this.changeAccountType.yourEligibilitySummaryHeader, 5000);
    }

    waitForSummaryPageTitleHead() {
        Actions.waitForVisible(this.changeAccountType.summaryPageTitle, 5000);
    }

    selectSummaryPageContinueButton() {
        Actions.click(this.changeAccountType.summaryPageContinueButton);
    }

    selectAddOfferButton() {
        Actions.click(this.changeAccountType.AddOfferButton);
    }
}
module.exports = ChangeAccountTypeWebViewPage;
