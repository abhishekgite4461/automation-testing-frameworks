const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class FreezeCardTransactionsPage extends AuthPage {
    /**
     * @returns {FreezeCardTransactions}
     */
    get freezeCardTransactions() {
        return this.getNativeScreen('freezeCardTransactions.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.freezeCardTransactions.title);
    }

    selectAllOptions() {
        Actions.click(this.freezeCardTransactions.abroad);
        Actions.click(this.freezeCardTransactions.online);
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.freezeCardTransactions.terminals);
    }

    selectReportLostOrStolen() {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.freezeCardTransactions.reportLostAndStolen);
    }

    selectFreezeGambling() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.freezeCardTransactions.freezeGambling);
        Actions.click(this.freezeCardTransactions.freezeGambling);
    }

    waitForReadyToFreezePage() {
        Actions.waitForVisible(this.freezeCardTransactions.waitForReadyToFreezePage, 20000);
    }

    selectFreezeButtonGambling() {
        Actions.click(this.freezeCardTransactions.freezeButtonGambling);
    }

    selectCancelFreezeButton() {
        Actions.click(this.freezeCardTransactions.cancelFreezeButton);
    }
}

module.exports = FreezeCardTransactionsPage;
