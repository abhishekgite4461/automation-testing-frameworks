require('chai').should();
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');
const AccountMatcher = require('../data/utils/accountMatcher');

class BusinessSelectionPage extends UnAuthPage {
    /**
     * @returns {BusinessSelectionScreen}
     */
    get businessSelectionScreen() {
        return this.getNativeScreen('businessSelection.screen');
    }

    get validationLocator() {
        return this.businessSelectionScreen.title;
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.businessSelectionScreen.title, 15000);
    }

    shouldSeeSignatoryTitleIsPresent() {
        Actions.waitForVisible(this.businessSelectionScreen.businessSelectionSignatoryHeader, 15000);
    }

    shouldSeeAccessLevelTitleIsPresent(accessLevel) {
        if (accessLevel === 'FULL_SIGNATORY') {
            Actions.waitForVisible(this.businessSelectionScreen.businessSelectionSignatoryHeader, 15000);
        } else {
            Actions.waitForVisible(this.businessSelectionScreen.businessSelectionSignatoryHeader, 15000);
        }
    }

    shouldSeeDelegateTitleIsPresent() {
        Actions.waitForVisible(this.businessSelectionScreen.businessSelectionDelegateHeader, 15000);
    }

    shouldSeeBusinessSelectionPage() {
        Actions.isVisible(this.businessSelectionScreen.title).should.equal(true);
    }

    shouldNotSeeBusinessSelectionPage() {
        Actions.isVisible(this.businessSelectionScreen.title).should.equal(false);
    }

    selectBusinessWithinAccessLevel(access, businessName = null) {
        const selectBusiness = businessName || AccountMatcher.getBusiness();
        if (!AccountMatcher.isSingleBusiness()) {
            this.waitForPageLoad();
            // access is undefined when there is only one business
            if (access) {
                if (access === 'FULL_SIGNATORY') {
                    Actions.waitAndClick(this.businessSelectionScreen.signatoryBusiness(selectBusiness));
                } else {
                    Actions.waitAndClick(this.businessSelectionScreen.delegateBusiness(selectBusiness));
                }
            }
        }
    }

    selectFirstSignatoryBusiness() {
        Actions.click(this.businessSelectionScreen.firstSignatoryBusiness);
    }

    selectFirstBusinessOfAccessLevel(accessLevel) {
        if (accessLevel === 'FULL_SIGNATORY') {
            Actions.click(this.businessSelectionScreen.firstSignatoryBusiness);
        } else {
            Actions.click(this.businessSelectionScreen.firstDelegateBusiness);
        }
    }

    selectFirstBusiness() {
        Actions.waitAndClick(this.businessSelectionScreen.firstBusinessName);
    }

    shouldSeeBusinessInAlphabeticOrder(accessLevel) {
        const allBusinesses = (accessLevel === 'FULL_SIGNATORY') ? Actions.getAttributeText(this.businessSelectionScreen.signatoryBusinesses) : Actions.getAttributeText(this.businessSelectionScreen.delegateAndViewOnlyBusinesses);
        let actualBusinesses = [];
        actualBusinesses = (Array.isArray(allBusinesses) ? allBusinesses : actualBusinesses.push(allBusinesses));
        const expectedBusinesses = actualBusinesses.sort();
        for (let i = 0; i < actualBusinesses.length; i++) {
            actualBusinesses[i].should.equal(expectedBusinesses[i]);
        }
    }
}

module.exports = BusinessSelectionPage;
