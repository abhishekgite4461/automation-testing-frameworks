require('chai').should();
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');

class LogoutPage extends UnAuthPage {
    /**
     * @returns {LogoutScreen}
     */
    get logoutScreen() {
        return this.getNativeScreen('logout.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.logoutScreen.title);
    }

    navigateToLogin() {
        Actions.click(this.logoutScreen.loginButton);
    }

    shouldSeeCmsTile() {
        Actions.isVisible(this.logoutScreen.cmsTile).should.equal(true);
    }

    shouldSeeLoginButton() {
        Actions.isVisible(this.logoutScreen.loginButton).should.equal(true);
    }

    verifyFscsTile(fscsVisible) {
        Actions.isVisible(this.logoutScreen.fscsTile).should.equal(fscsVisible);
    }

    clickBackToLogonButton() {
        Actions.click(this.logoutScreen.loginButton);
    }

    verifyAppBanningMessage() {
        Actions.isVisible(this.logoutScreen.appBanningMessage).should.equal(true);
    }

    verifyLogoutCompleteMessage() {
        Actions.isVisible(this.logoutScreen.loggOffcompleteMessage).should.equal(true);
    }

    verifyAppResetCompleteMessage() {
        Actions.isVisible(this.logoutScreen.appResetcompleteMessage).should.equal(true);
    }

    shouldSeeSessionTimedOutMessage() {
        if (Actions.isVisible(this.logoutScreen.surveyButton)) {
            Actions.isVisible(this.logoutScreen.appResetcompleteMessage).should.equal(true);
        } else {
            Actions.isVisible(this.logoutScreen.sessionTimedOutMessage).should.equal(true);
        }
    }

    shouldSeeSurveyButton() {
        Actions.isVisible(this.logoutScreen.surveyButton).should.equal(true);
    }
}

module.exports = LogoutPage;
