const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class OrderPaperStatementsPage extends AuthPage {
    /**
     * @returns {OrderPaperStatementsScreen}
     */
    get orderPaperStatementsScreen() {
        return this.getNativeScreen('orderPaperStatements.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.orderPaperStatementsScreen.title);
    }

    selectStatements() {
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.orderPaperStatementsScreen.firstStatement);
    }

    SelectContinue() {
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.orderPaperStatementsScreen.continue);
    }

    verifyConfirmYourOrderPage() {
        Actions.waitForVisible(this.orderPaperStatementsScreen.titleConfirm);
    }

    validateConfirmOrderOptions(pageFields) {
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.isVisible(this.orderPaperStatementsScreen[pageFields[iter]
                .fieldsInDetailScreen]);
        }
    }

    selectChooseAccount() {
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.orderPaperStatementsScreen.chooseAccount);
    }

    validateAccountOptions(pageFields) {
        Actions.swipePage(SwipeAction.UP);
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.isVisible(this.orderPaperStatementsScreen[pageFields[iter]
                .fieldsInDetailScreen]);
        }
    }

    verifyAccountDetails(accName) {
        Actions.isVisible(this.orderPaperStatementsScreen.accountName(accName));
    }
}

module.exports = OrderPaperStatementsPage;
