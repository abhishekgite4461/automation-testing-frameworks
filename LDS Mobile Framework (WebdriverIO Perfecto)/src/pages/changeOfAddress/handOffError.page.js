require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class HandOffErrorPage extends AuthPage {
    get handOffErrorScreen() {
        return this.getNativeScreen('changeOfAddress/handOffError.screen');
    }

    verifyHandOffErrorTitle() {
        Actions.isExisting(this.handOffErrorScreen.title)
            .should.equal(true);
    }

    isVisibleHandOffErrorText() {
        Actions.isVisible(this.handOffErrorScreen.handOffErrorText)
            .should.equal(true);
    }

    isVisibleCallUsButton(isVisible) {
        Actions.isVisible(this.handOffErrorScreen.callUsButton)
            .should.equal(isVisible);
    }

    clickCallUsButton() {
        Actions.click(this.handOffErrorScreen.callUsButton);
    }

    isVisibleBackToPersonalDetailsButton(isVisible) {
        Actions.isVisible(this.handOffErrorScreen.backToPersonalDetailsButton)
            .should.equal(isVisible);
    }

    clickBackToPersonalDetailsButton() {
        Actions.click(this.handOffErrorScreen.backToPersonalDetailsButton);
    }

    isVisibleForcedLogoutPage() {
        Actions.isExisting(this.handOffErrorScreen.forcedLogoffTitle).should.equal(true);
        Actions.isExisting(this.handOffErrorScreen.forcedLogoffMessage).should.equal(true);
        Actions.isExisting(this.handOffErrorScreen.forcedLogoffLogoonButton).should.equal(true);
    }
}

module.exports = HandOffErrorPage;
