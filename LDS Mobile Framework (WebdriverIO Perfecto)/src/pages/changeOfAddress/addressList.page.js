require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const SwipeAction = require('../../enums/swipeAction.enum');

class AddressListPage extends AuthPage {
    get addressListScreen() {
        return this.getNativeScreen('changeOfAddress/addressList.screen');
    }

    verifyAddressListTitle() {
        if (!Actions.isVisible(this.addressListScreen.title)) {
            Actions.swipeUntilVisible(SwipeAction.DOWN, this.addressListScreen.title);
        }
    }

    isVisibleAddressListText() {
        Actions.isVisible(this.addressListScreen.addressListText)
            .should.equal(true);
    }

    isVisibleAddressList() {
        Actions.isVisible(this.addressListScreen.addressList)
            .should.equal(true);
    }

    isVisibleAddressNotOnListButton() {
        Actions.swipeByPercentUntilVisible(this.addressListScreen.addressNotOnListButton);
        Actions.isVisible(this.addressListScreen.addressNotOnListButton)
            .should.equal(true);
    }

    clickAddressNotOnListButton() {
        Actions.swipeByPercentUntilVisible(this.addressListScreen.addressNotOnListButton);
        Actions.click(this.addressListScreen.addressNotOnListButton);
    }

    selectAddress() {
        let postCode = null;
        if (DeviceHelper.isUAT()) {
            postCode = SavedAppDataRepository.getData(SavedData.CURRENT_POST_CODE);
            if (postCode === 'EC1Y 4XX') {
                postCode = 'SE19EQ';
            } else {
                postCode = 'EC1Y4XX';
            }
        }
        let selectedAddress = null;
        if (DeviceHelper.isUAT() && postCode === 'SE19EQ') {
            Actions.swipeByPercentUntilVisible(this.addressListScreen.addressToBeSelectedUAT);
            selectedAddress = Actions.getAttributeText(this.addressListScreen.addressToBeSelectedUAT, 'label');
        } else {
            selectedAddress = Actions.getAttributeText(this.addressListScreen.addressToBeSelected, 'label');
        }
        SavedAppDataRepository.setData(SavedData.COA_SELECTED_ADDRESS, selectedAddress);
        const addressLines = selectedAddress.split(',');
        SavedAppDataRepository.setData(SavedData.COA_ADDRESS_LIST_POSTCODE, addressLines.pop().trim());
        if (DeviceHelper.isUAT() && postCode === 'SE19EQ') {
            Actions.click(this.addressListScreen.addressToBeSelectedUAT);
        } else {
            Actions.click(this.addressListScreen.addressToBeSelected);
        }
    }

    validateAddressListTitleCase() {
        const enteredPostcode = SavedAppDataRepository.getData(SavedData.COA_POSTCODE_ENTERED);
        const elements = browser.elements(this.addressListScreen.addressListItem);
        (DeviceHelper.isAndroid()
            ? elements.value.map((element) => element.getText())
            : elements.value.map((element, index) => Actions.getAttributeText(this.addressListScreen.addressListAllItems(index), 'label')))
            .forEach((address) => {
                AddressListPage.evaluateAddressListItem(address, enteredPostcode);
            });
    }

    static evaluateAddressListItem(addressListItem, enteredPostcode) {
        const addressLines = addressListItem.split(',');
        const actualPostcode = addressLines.pop().trim().replace(' ', '');
        actualPostcode.should.equal(enteredPostcode);
        addressLines.join().split(' ').forEach((item) => {
            const startChar = item.trim().charAt(0);
            const remainingText = item.trim().substring(1);
            startChar.should.equal(startChar.toUpperCase());
            remainingText.should.equal(remainingText.toLowerCase());
        });
    }
}

module.exports = AddressListPage;
