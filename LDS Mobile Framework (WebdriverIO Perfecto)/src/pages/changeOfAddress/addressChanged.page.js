require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class AddressChangedPage extends AuthPage {
    get addressChangedScreen() {
        return this.getNativeScreen('changeOfAddress/addressChanged.screen');
    }

    verifyAddressChangedTitle() {
        Actions.waitForVisible(this.addressChangedScreen.title);
        Actions.isExisting(this.addressChangedScreen.title)
            .should.equal(true);
        Actions.isExisting(this.addressChangedScreen.addressChangeSuccessMessage)
            .should.equal(true);
        Actions.isExisting(this.addressChangedScreen.homeInsuranceLeadTitle)
            .should.equal(true);
        Actions.isExisting(this.addressChangedScreen.homeInsuranceLeadIcon)
            .should.equal(true);
        Actions.isExisting(this.addressChangedScreen.homeInsuranceLeadButton)
            .should.equal(true);
    }

    verifyBackToHomeButton() {
        Actions.isEnabled(this.addressChangedScreen.backToHomeButton)
            .should.equal(true);
    }

    selectHomeButton() {
        Actions.click(this.addressChangedScreen.backToHomeButton);
    }
}

module.exports = AddressChangedPage;
