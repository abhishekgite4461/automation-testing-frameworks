require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const DeviceHelper = require('../../utils/device.helper');

class ConfirmAddressPage extends AuthPage {
    get confirmAddressScreen() {
        return this.getNativeScreen('changeOfAddress/confirmAddress.screen');
    }

    verifyConfirmAddressTitle() {
        Actions.isExisting(this.confirmAddressScreen.title)
            .should.equal(true);
    }

    isVisibleMap() {
        Actions.isVisible(this.confirmAddressScreen.mapView)
            .should.equal(true);
    }

    isVisibleMapPlaceholderImage() {
        Actions.isVisible(this.confirmAddressScreen.mapPlaceholder)
            .should.equal(true);
    }

    validateNewAddress() {
        const expectedAddress = SavedAppDataRepository.getData(SavedData.COA_SELECTED_ADDRESS);
        const printedAddress = Actions.getAttributeText(this.confirmAddressScreen.newAddress, 'label');
        if (DeviceHelper.isAndroid()) {
            printedAddress.replace(new RegExp('\\n', 'g'), ', ').should.equal(expectedAddress);
        } else {
            printedAddress.should.equal(expectedAddress.replace(new RegExp(',', 'g'), ''));
        }
    }

    validateNewAddressTitleCase() {
        const address = Actions.getAttributeText(this.confirmAddressScreen.newAddress, 'label');
        let addressLines;
        let visiblePostcode;
        if (DeviceHelper.isAndroid()) {
            addressLines = address.split(new RegExp('\\n', 'g'));
            visiblePostcode = addressLines.pop().trim();
        } else {
            addressLines = address.split(' ');
            visiblePostcode = addressLines.pop();
            visiblePostcode = `${addressLines.pop()} ${visiblePostcode}`;
        }
        visiblePostcode.should.equal(SavedAppDataRepository.getData(SavedData.COA_ADDRESS_LIST_POSTCODE));
        addressLines.join(' ').split(' ').forEach((addressPart) => {
            const remainingText = addressPart.substring(1);
            addressPart.charAt(0).should.equal(addressPart.charAt(0).toUpperCase());
            remainingText.should.equal(remainingText.toLowerCase());
        });
    }

    isEnabledConfirmAddressButton() {
        Actions.isEnabled(this.confirmAddressScreen.confirmAddressButton)
            .should.equal(true);
    }

    clickConfirmAddressButton() {
        Actions.click(this.confirmAddressScreen.confirmAddressButton);
    }
}

module.exports = ConfirmAddressPage;
