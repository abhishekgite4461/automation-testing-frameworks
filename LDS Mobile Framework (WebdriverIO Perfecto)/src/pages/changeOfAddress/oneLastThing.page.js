require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class OneLastThingPage extends AuthPage {
    get oneLastThingScreen() {
        return this.getNativeScreen('changeOfAddress/oneLastThing.screen');
    }

    verifyOneLastThingTitle() {
        if (!Actions.isVisible(this.oneLastThingScreen.title)) {
            Actions.swipePage(SwipeAction.DOWN);
        }
        Actions.waitForVisible(this.oneLastThingScreen.title);
    }

    isVisiblePageText() {
        const titleElements = browser.elements(this.oneLastThingScreen.oneLastThingSubtitles);
        const textElements = browser.elements(this.oneLastThingScreen.oneLastThingTexts);
        for (let i = 1; i <= titleElements.value.length; i++) {
            if (i >= titleElements.value.length / 2) {
                Actions.swipePage(SwipeAction.UP);
            }
            Actions.isVisible(this.oneLastThingScreen.oneLastThingSubtitleText(i)).should.equal(true);
        }

        Actions.swipeUntilVisible(SwipeAction.DOWN, this.oneLastThingScreen.title);
        for (let i = 1; i <= textElements.value.length; i++) {
            if (i >= textElements.value.length / 2) {
                Actions.swipePage(SwipeAction.UP);
            }
            Actions.isVisible(this.oneLastThingScreen.oneLastThingText(i)).should.equal(true);
        }
    }

    isEnabledIUnderstandButton() {
        if (!Actions.isVisible(this.oneLastThingScreen.iUnderstandButton)) {
            Actions.swipePage(SwipeAction.UP);
        }
        Actions.isEnabled(this.oneLastThingScreen.iUnderstandButton)
            .should.equal(true);
    }

    selectIUnderstrandButton() {
        if (!Actions.isVisible(this.oneLastThingScreen.iUnderstandButton)) {
            Actions.swipePage(SwipeAction.UP);
        }
        Actions.click(this.oneLastThingScreen.iUnderstandButton);
    }

    isVisibleEnterPasswordDialog(isVisible) {
        Actions.isExisting(this.oneLastThingScreen.enterPasswordDialogTitle)
            .should.equal(isVisible);
    }

    enterPassword(password) {
        Actions.setImmediateValue(this.oneLastThingScreen.dialogPasswordField, password);
    }

    selectForgotPassword() {
        Actions.click(this.oneLastThingScreen.dialogForgottenPassword);
    }

    isDialogCancelButtonEnabled() {
        Actions.isEnabled(this.oneLastThingScreen.dialogCancelButton)
            .should.equal(true);
    }

    selectDialogCancelButton() {
        Actions.click(this.oneLastThingScreen.dialogCancelButton);
    }

    isDialogOkButtonEnabled(isEnabled) {
        Actions.isEnabled(this.oneLastThingScreen.dialogOkButton)
            .should.equal(isEnabled);
    }

    selectDialogOkButton() {
        Actions.click(this.oneLastThingScreen.dialogOkButton);
    }
}

module.exports = OneLastThingPage;
