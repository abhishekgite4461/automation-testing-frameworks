require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');
const DeviceHelper = require('../../utils/device.helper');

const POST_CODE_MAX_LENGTH = 7;
const POST_CODE_MAX_LENGTH_WITH_SPACE = 8;

class PostCodeEntryPage extends AuthPage {
    get postCodeEntryScreen() {
        return this.getNativeScreen('changeOfAddress/postCodeEntry.screen');
    }

    verifyPostCodeEntryTitle() {
        Actions.waitForVisible(this.postCodeEntryScreen.title, 20000);
    }

    clickPostCode() {
        Actions.waitAndClick(this.postCodeEntryScreen.postCode);
    }

    enterPostCode(postCode) {
        SavedAppDataRepository.setData(SavedData.COA_POSTCODE_ENTERED, postCode.replace(' ', ''));
        this.clickPostCode();
        Actions.clearText(this.postCodeEntryScreen.postCode);
        Actions.setImmediateValue(this.postCodeEntryScreen.postCode, postCode);
        Actions.tapOutOfField();
        Actions.hideDeviceKeyboard();
    }

    isVisiblePostCodeTip(isVisible) {
        Actions.isVisible(this.postCodeEntryScreen.postCodeTipView)
            .should.equal(isVisible);
    }

    validatePostCode(expectedPostCode) {
        PostCodeEntryPage.getFieldValue(this.postCodeEntryScreen.postCode)
            .should.equal(expectedPostCode);
    }

    deletePostCodeCharacter() {
        this.clickPostCode();
        Actions.deleteCharacter(this.postCodeEntryScreen.keyboardDeleteButton);
        Actions.tapOutOfField();
        Actions.hideDeviceKeyboard();
    }

    clearPostCode() {
        Actions.clearText(this.postCodeEntryScreen.postCode);
        Actions.tapOutOfField();
        Actions.hideDeviceKeyboard();
    }

    validateMaxLength() {
        Actions.getAttributeText(this.postCodeEntryScreen.postCode, 'value').trim().length
            .should.equal(POST_CODE_MAX_LENGTH);
    }

    validateMaxLengthWithSpace() {
        Actions.getAttributeText(this.postCodeEntryScreen.postCode, 'value').trim().length
            .should.equal(POST_CODE_MAX_LENGTH_WITH_SPACE);
    }

    validatePostCodeCapitalization(expectedPostCode) {
        Actions.getAttributeText(this.postCodeEntryScreen.postCode, 'value').toString()
            .should.equal(expectedPostCode);
    }

    isExistingErrorIcon(isExisting) {
        Actions.isExisting(this.postCodeEntryScreen.errorIcon)
            .should.equal(isExisting);
    }

    isVisibleEnterPostcodeHelpText() {
        Actions.isVisible(this.postCodeEntryScreen.enterPostcodeHelpText)
            .should.equal(true);
    }

    isVisibleEnterPostcodeCallUsText() {
        Actions.waitForVisible(this.postCodeEntryScreen.enterPostcodeCallUsText, 20000);
    }

    clickCallUsLink() {
        Actions.click(this.postCodeEntryScreen.enterPostcodeCallUsText);
    }

    clickBackArrow() {
        Actions.waitForVisible(this.postCodeEntryScreen.backArrow);
        Actions.click(this.postCodeEntryScreen.backArrow);
    }

    isEnabledFindAddress(isEnabled) {
        Actions.isEnabled(this.postCodeEntryScreen.findAddress)
            .should.equal(isEnabled);
    }

    clickFindAddress() {
        Actions.click(this.postCodeEntryScreen.findAddress);
    }

    isVisibleWebViewPage() {
        Actions.isVisible(this.postCodeEntryScreen.webviewChangeAddress);
    }

    static getFieldValue(field) {
        let value = Actions.getAttributeText(field, 'value');
        // Handling for iOS Accessibility Text
        if (DeviceHelper.isIOS()) {
            value = value === 'Enter postcode' ? '' : value;
        }
        return value === null || value === undefined ? '' : value;
    }
}

module.exports = PostCodeEntryPage;
