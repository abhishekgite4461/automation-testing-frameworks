const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class CardManagementPage extends AuthPage {
    /**
     * @returns {CardManagementScreen}
     */
    get cardManagementScreen() {
        return this.getNativeScreen('cardManagement.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.cardManagementScreen.title);
    }

    validateCardManagementOptions(pageFields) {
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.isVisible(this.cardManagementScreen[pageFields[iter]
                .fieldsInDetailScreen]);
        }
    }

    selectLostAndStolenCards() {
        Actions.waitAndClick(this.cardManagementScreen.lostAndStolenCards);
    }

    selectReplacementCardAndPin() {
        Actions.waitAndClick(this.cardManagementScreen.replaceCardAndPin);
    }

    selectFreezCardTransactions() {
        Actions.waitAndClick(this.cardManagementScreen.freezCardTransactions);
    }
}

module.exports = CardManagementPage;
