require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class GlobalMenuPage extends AuthPage {
    get globalMenuScreen() {
        return this.getNativeScreen('globalMenu.screen');
    }

    clickGlobalMenuButton(elem) {
        // The logoff button will obscure menu items at the bottom of the menu so we use an offset
        // of the height of the logoff button to mitigate this.  This ties the test rather strongly
        // to the page layout, but it shouldn't change too much.
        Actions.swipeUntilVisible(SwipeAction.UP, elem, this.getLogoffButtonHeight());
        Actions.click(elem);
    }

    openGlobalMenu() {
        Actions.waitForVisible(this.globalMenuScreen.globalMenuButton);
        Actions.click(this.globalMenuScreen.globalMenuButton);
        // Scroll to reach the top of the menu, in case it was scrolled previously.
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.globalMenuScreen.title);
    }

    navigateToSettings() {
        GlobalMenuPage.swipeOnGlobalMenu(this.globalMenuScreen.settingsButton);
        this.clickGlobalMenuButton(this.globalMenuScreen.settingsButton);
    }
}

module.exports = GlobalMenuPage;
