const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class AccountSelectionPage extends AuthPage {
    get accountSelectionScren() {
        return this.getNativeScreen('accountSelection.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.accountSelectionScren.accountSelectionAccountName);
    }

    shouldSeeAccountDetails() {
        Actions.isVisible(this.accountSelectionScren.accountSelectionAccountName).should.equal(true);
        Actions.isVisible(this.accountSelectionScren.accountSelectionSortCode).should.equal(true);
        Actions.isVisible(this.accountSelectionScren.accountSelectionAccountBalance).should.equal(true);
        Actions.isVisible(this.accountSelectionScren.accountSelectionAccountNumber).should.equal(true);
    }

    selectOnFirstSearchResult() {
        Actions.click(this.accountSelectionScren.accountSelectionAccountName);
    }

    shouldSeeWarningMessage() {
        Actions.isVisible(this.accountSelectionScren.currentAccountWarningMessage).should.equal(true);
    }
}

module.exports = AccountSelectionPage;
