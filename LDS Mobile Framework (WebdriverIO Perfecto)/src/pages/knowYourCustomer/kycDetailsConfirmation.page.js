const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class KycDetailsConfirmationPage extends AuthPage {
    /**
     * @returns {KycDetailsConfirmationPage}
     */
    get kycDetailsConfirmationScreen() {
        return this.getNativeScreen('knowYourCustomer/kycDetailsConfirmation.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.kycDetailsConfirmationScreen.title);
    }

    clickConfirmButton() {
        Actions.click(this.kycDetailsConfirmationScreen.confirmButton);
    }
}

module.exports = KycDetailsConfirmationPage;
