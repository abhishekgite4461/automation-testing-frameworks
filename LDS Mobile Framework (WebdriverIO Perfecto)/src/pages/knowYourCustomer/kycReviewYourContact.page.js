require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class KycReviewYourContactPage extends AuthPage {
    /**
     * @returns {KycReviewYourContactPage}
     */
    get kycReviewYourContactScreen() {
        return this.getNativeScreen('knowYourCustomer/kycReviewYourContact.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.kycReviewYourContactScreen.detailsCorrect);
    }

    enterMobileNumber(mobile) {
        Actions.click(this.kycReviewYourContactScreen.mobileText);
        Actions.clearText(this.kycReviewYourContactScreen.mobileText);
        Actions.setImmediateValue(this.kycReviewYourContactScreen.mobileText, mobile);
    }

    validateDetails(kycDetails) {
        for (let i = 0; i < kycDetails.length; i++) {
            const detailsDisplayed = this.kycReviewYourContactScreen[kycDetails[i].existingDetails];
            Actions.isVisible(detailsDisplayed).should.equal(true);
        }
    }

    validateOptions(kycOptions) {
        for (let i = 0; i < kycOptions.length; i++) {
            const optionssDisplayed = this.kycReviewYourContactScreen[kycOptions[i].options];
            Actions.isVisible(optionssDisplayed).should.equal(true);
        }
    }

    clickConfirmButton() {
        Actions.click(this.kycReviewYourContactScreen.detailsCorrect);
    }

    clickUpdateUpdateLaterButton() {
        if (Actions.isVisible(this.kycReviewYourContactScreen.updateLater)) {
            Actions.click(this.kycReviewYourContactScreen.updateLater);
        } else {
            Actions.swipeUntilVisible(SwipeAction.UP, this.kycReviewYourContactScreen.updateLater);
            Actions.click(this.kycReviewYourContactScreen.updateLater);
        }
    }

    clickNotNowButton() {
        Actions.click(this.kycReviewYourContactScreen.notNowButton);
    }
}

module.exports = KycReviewYourContactPage;
