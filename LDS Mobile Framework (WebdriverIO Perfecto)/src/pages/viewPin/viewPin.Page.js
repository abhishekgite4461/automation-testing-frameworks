const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class viewPinPage extends AuthPage {
    get viewPinScreen() {
        return this.getNativeScreen('viewPin/viewPin.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.viewPinScreen.title);
    }

    validatePINDisplay() {
        const PIN = Actions.getText(this.viewPinScreen.PIN).valueOf();
        if (PIN > 3) {
            return true;
        }

        return false;
    }

    selectViewPinOption() {
        Actions.waitForVisible(this.viewPinScreen.revealPinButton);
        Actions.longPressed(this.viewPinScreen.revealPinButton, 50);
    }
}
module.exports = viewPinPage;
