const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
require('chai').should();

class cardAndPinSettingsPage extends AuthPage {
    get cardAndPinSettingsScreen() {
        return this.getNativeScreen('viewPin/cardAndPinSettings.screen');
    }

    get homeScreen() {
        return this.getNativeScreen('home.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.cardAndPinSettingsScreen.title);
    }

    selectViewPinOption() {
        Actions.waitForVisible(this.cardAndPinSettingsScreen.viewPinTile);
        Actions.click(this.cardAndPinSettingsScreen.viewPinTile);
    }
}

module.exports = cardAndPinSettingsPage;
