const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class viewPinPage extends AuthPage {
    get accountsOverviewScreen() {
        return this.getNativeScreen('cardAndPinSettingsScreen.screen');
    }

    get homeScreen() {
        return this.getNativeScreen('home.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.accountsOverviewScreen.title);
    }
}

module.exports = viewPinPage;
