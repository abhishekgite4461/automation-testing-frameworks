const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class viewPincardCarouselPage extends AuthPage {
    get accountsOverviewScreen() {
        return this.getNativeScreen('viewPincardCarouselScreen.screen');
    }

    get homeScreen() {
        return this.getNativeScreen('home.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.accountsOverviewScreen.title);
    }
}

module.exports = viewPincardCarouselPage;
