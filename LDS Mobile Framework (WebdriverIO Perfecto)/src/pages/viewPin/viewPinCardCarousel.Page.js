const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
require('chai').should();

class viewPinCardCarouselPage extends AuthPage {
    get viewPinCardCarouselScreen() {
        return this.getNativeScreen('viewPin/viewPinCardCarousel.screen');
    }

    get confirmAuthenticationPasswordScreen() {
        return this.getNativeScreen('confirmAuthenticationPassword.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.viewPinCardCarouselScreen.title);
    }

    selectViewPinButton() {
        Actions.click(this.viewPinCardCarouselScreen.viewPinButton);
    }

    enterPassword(password) {
        Actions.waitForVisible(this.confirmAuthenticationPasswordScreen.passwordField);
        Actions.click(this.confirmAuthenticationPasswordScreen.passwordField);
        Actions.waitForVisible(this.confirmAuthenticationPasswordScreen.passwordField);
        Actions.genericSendKeys(this.confirmAuthenticationPasswordScreen.passwordField, password);
        Actions.click(this.viewPinCardCarouselScreen.dialogOkButton);
    }
}
module.exports = viewPinCardCarouselPage;
