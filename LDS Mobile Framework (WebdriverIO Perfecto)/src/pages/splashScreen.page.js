const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class SplashScreenPage extends AuthPage {
    /**
     * @returns {SplashScreen}
     */

    get splashScreen() {
        return this.getScreen('splashScreen.screen');
    }

    verifySplashScreen() {
        Actions.isExisting(this.splashScreen.title).should.equal(true);
    }
}

module.exports = SplashScreenPage;
