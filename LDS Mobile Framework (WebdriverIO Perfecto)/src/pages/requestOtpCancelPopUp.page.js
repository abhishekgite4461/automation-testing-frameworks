const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');

class RequestOtpCancelPopUpPage extends UnAuthPage {
    /**
     * @returns {RequestOtpCancelPopUpScreen}
     */
    get requestOtpCancelPopUpScreen() {
        return this.getNativeScreen('requestOtpCancelPopUp.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.requestOtpCancelPopUpScreen.title);
    }

    selectCancelButton() {
        Actions.click(this.requestOtpCancelPopUpScreen.cancelButton);
    }

    selectConfirmButton() {
        Actions.click(this.requestOtpCancelPopUpScreen.confirmButton);
    }
}

module.exports = RequestOtpCancelPopUpPage;
