require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class PayCreditCardbyMasterCard extends AuthPage {
    get payCreditCardbyMasterCardScreen() {
        return this.getNativeScreen('payCreditCardbyMasterCard/payCreditCardbyMasterCard.screen');
    }

    get ukBeneficiaryDetailScreen() {
        return this.getNativeScreen('transferAndPayments/ukBeneficiaryDetail.screen');
    }

    verifyAnotherUKAccountVisibility(isShown) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.ukBeneficiaryDetailScreen.debitCardTile);
        if (isShown === 'not see') {
            Actions.isVisible(this.payCreditCardbyMasterCardScreen.anotherUkBankAccount).should.equal(false);
        } else {
            Actions.isVisible(this.payCreditCardbyMasterCardScreen.anotherUkBankAccount).should.equal(true);
        }
    }

    selectDismissErrorBanner() {
        Actions.click(this.payCreditCardbyMasterCardScreen.dismissErrorBanner);
    }

    shouldSeeReviewPayment() {
        Actions.isVisible(this.payCreditCardbyMasterCardScreen.reviewPaymentPage).should.equal(true);
    }

    selectProviderButton() {
        Actions.click(this.payCreditCardbyMasterCardScreen.selectProviderButton);
    }

    selectProviderButtonEnability(enability) {
        if (DeviceHelper.isAndroid()) {
            Actions.hideDeviceKeyboard();
        }
        Actions.isEnabled(this.payCreditCardbyMasterCardScreen.selectProviderButton).should.equal(enability);
    }

    shouldSeeSelectedBank(bankName) {
        Actions.isVisible(this.payCreditCardbyMasterCardScreen.selectedBankName(bankName));
    }

    verifySelectedCCAccount(accountName) {
        Actions.isVisible(this.payCreditCardbyMasterCardScreen.selectedBankName(accountName));
    }

    shouldNotSeeCheckbox() {
        Actions.isVisible(this.payCreditCardbyMasterCardScreen.confirmPaymentCheckBox).should.equal(false);
    }

    confirmButtonEnability(enability) {
        Actions.isEnabled(this.payCreditCardbyMasterCardScreen.confirmButton).should.equal(enability);
    }

    editButtonEnability(enability) {
        Actions.isEnabled(this.payCreditCardbyMasterCardScreen.editButton).should.equal(enability);
    }

    impScreenVisibility(isVisible) {
        if (isVisible === 'see') {
            Actions.isVisible(this.payCreditCardbyMasterCardScreen.impInfoToBankLogo).should.equal(true);
            Actions.isVisible(this.payCreditCardbyMasterCardScreen.impInfoBankLogo).should.equal(true);
            Actions.isVisible(this.payCreditCardbyMasterCardScreen.impInfoScreenCheckBox).should.equal(true);
        } else {
            Actions.isVisible(this.payCreditCardbyMasterCardScreen.impInfoBankLogo).should.equal(false);
        }
    }

    selectTCCheckBox() {
        Actions.click(this.payCreditCardbyMasterCardScreen.impInfoScreenCheckBox);
    }

    selectContinueBtn() {
        Actions.click(this.payCreditCardbyMasterCardScreen.continueBtn);
    }

    shouldSeeProviderListScreen() {
        Actions.waitForVisible(this.payCreditCardbyMasterCardScreen.providerListScreen);
        Actions.isVisible(this.payCreditCardbyMasterCardScreen.providerListScreen).should.equal(true);
    }

    selectPaybyUKDebitCard() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.payCreditCardbyMasterCardScreen.paybyUKdebitCard);
        Actions.click(this.payCreditCardbyMasterCardScreen.paybyUKdebitCard);
    }
}
module.exports = PayCreditCardbyMasterCard;
