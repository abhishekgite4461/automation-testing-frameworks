require('chai').should();
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');

class ErrorPage extends UnAuthPage {
    /**
     * @returns {ErrorScreen}
     */
    get errorScreen() {
        return this.getNativeScreen('error.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.errorScreen.title);
    }

    canSeeRevokedUserErrorMessage() {
        Actions.isVisible(this.errorScreen.revokedIDErrorMessage).should.equal(true);
    }

    isSCARevokedErrorMessageDisplayed() {
        Actions.isVisible(this.errorScreen.SCARevokedErrorMessage).should.equal(true);
    }

    shouldSeeHardTokenUserErrorMessage() {
        Actions.isVisible(this.errorScreen.hardTokenUserErrorMessage).should.equal(true);
    }

    shouldSeeSoftTokenUserErrorMessage() {
        Actions.isVisible(this.errorScreen.softTokenUserErrorMessage).should.equal(true);
    }

    canSeeSuspendedUserErrorMessage() {
        Actions.isVisible(this.errorScreen.suspendedIDErrorMessage).should.equal(true);
    }

    canSeeInactiveUserErrorMessage() {
        Actions.isVisible(this.errorScreen.inactiveIDErrorMessage).should.equal(true);
    }

    clickLogonMobileBanking() {
        Actions.click(this.errorScreen.logonToMobileBanking);
    }

    canSeeCompromisedPasswordErrorMessage() {
        Actions.isVisible(this.errorScreen.compromisedPasswordErrorMessage).should.equal(true);
    }

    canSeePartiallyRegisteredErrorMessage() {
        Actions.isExisting(this.errorScreen.partiallyRegisteredErrorMessage).should.equal(true);
    }

    canSeeTwoFactorAuthErrorMessage() {
        Actions.isVisible(this.errorScreen.twoFactorAuthErrorMessage).should.equal(true);
    }

    canSeeTelephoneNumber() {
        Actions.isAnyVisible(this.errorScreen.telephoneNumber).should.equal(true);
    }

    canSeeInaccessibleErrorMessage() {
        Actions.isVisible(this.errorScreen.inaccessibleErrorMessage).should.equal(true);
    }

    selectHomeButton() {
        Actions.click(this.errorScreen.homeButton);
    }

    shouldSeeMaxDeviceEnrolErrorMessage() {
        Actions.isVisible(this.errorScreen.maxDeviceEnrolErrorMessage).should.equal(true);
    }

    shouldSeeCardExpiredAndNewCardIssuedMessage() {
        Actions.isVisible(this.errorScreen.oldCardExpiredError).should.equal(true);
    }

    shouldSeeCardSuspendedMessage() {
        Actions.isVisible(this.errorScreen.suspendedCardError).should.equal(true);
    }
}

module.exports = ErrorPage;
