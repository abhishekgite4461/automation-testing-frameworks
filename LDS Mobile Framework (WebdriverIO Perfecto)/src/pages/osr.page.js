const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class OsrPage extends AuthPage {
    /**
     * @returns {OsrScreen}
     */
    get osrScreen() {
        return this.getNativeScreen('osr.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.osrScreen.title);
    }
}

module.exports = OsrPage;
