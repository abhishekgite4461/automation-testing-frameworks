const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');
require('chai').should();

class AppLaunchErrorPage extends AuthPage {
    /**
     * @returns {AppLaunchErrorScreen}
     */
    get appLaunchErrorScreen() {
        return this.getNativeScreen('appLaunchError.screen');
    }

    get switchScreen() {
        return this.getNativeScreen('switch.screen');
    }

    get browserScreen() {
        return this.getNativeScreen('browser.screen');
    }

    waitForPageLoad() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.switchScreen.nextButton);
        }
        Actions.waitForVisible(this.appLaunchErrorScreen.title);
    }

    shouldSeeTechnicalError() {
        Actions.isVisible(this.appLaunchErrorScreen.technicalError).should.equal(true);
    }

    shouldSeeInternetConnectionError() {
        Actions.waitForVisible(this.appLaunchErrorScreen.internetConnectionError, 5000);
    }

    shouldSeeInternetConnectionInBrowser() {
        Actions.pause(5000);
        Actions.clickOnText(this.appLaunchErrorScreen.internetConnectionErrorInBrowser);
    }
}

module.exports = AppLaunchErrorPage;
