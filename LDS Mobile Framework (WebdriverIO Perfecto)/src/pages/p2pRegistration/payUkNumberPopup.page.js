const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class PayUkNumberPopupPage extends AuthPage {
    /**
     * @returns {PayUkNumberPopupScreen}
     */
    get payUkNumberPopupScreen() {
        return this.getNativeScreen('p2pRegistration/payUkNumberPopup.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.payUkNumberPopupScreen.title);
    }

    clickRegisterNow() {
        Actions.click(this.payUkNumberPopupScreen.registerNow);
    }
}

module.exports = PayUkNumberPopupPage;
