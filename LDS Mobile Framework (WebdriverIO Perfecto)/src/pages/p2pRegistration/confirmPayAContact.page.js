const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ConfirmPayAContactPage extends AuthPage {
    /**
     * @returns {ConfirmPayAContactScreen}
     */
    get confirmPayAContactScreen() {
        return this.getNativeScreen('p2pRegistration/confirmPayAContact.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.confirmPayAContactScreen.title);
    }
}

module.exports = ConfirmPayAContactPage;
