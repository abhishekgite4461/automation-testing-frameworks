require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class PayContactRegistrationPage extends AuthPage {
    /**
     * @returns {PayContactRegistrationScreen}
     */
    get payContactRegistrationScreen() {
        return this.getNativeScreen('p2pRegistration/payContactRegistration.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.payContactRegistrationScreen.title);
    }

    selectRegisterPayAContact() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.payContactRegistrationScreen.registerButton);
        Actions.click(this.payContactRegistrationScreen.registerButton);
    }

    selectDeRegisterPayAContact() {
        Actions.click(this.payContactRegistrationScreen.deRegisterButton);
    }

    verifyDeRegisterSuccessMessage() {
        Actions.isVisible(this.payContactRegistrationScreen.deRegisterSuccessMessage).should.equal(true);
    }
}

module.exports = PayContactRegistrationPage;
