const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const DeviceHelper = require('../../utils/device.helper');

class GoingAbroadPage extends AuthPage {
    /**
     * @returns {GoingAbroadScreen}
     */
    get goingAbroadScreen() {
        return this.getNativeScreen('settings/goingAbroad.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.goingAbroadScreen.title);
    }

    clickBackButton() {
        Actions.click(this.goingAbroadScreen.backButton);
        if (!DeviceHelper.isAndroid()) {
            Actions.click(this.goingAbroadScreen.okButton);
        }
    }
}

module.exports = GoingAbroadPage;
