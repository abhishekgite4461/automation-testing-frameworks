require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const SwipeAction = require('../../enums/swipeAction.enum');

class ResetPage extends AuthPage {
    /**
     * @returns {ResetScreen}
     */
    get resetScreen() {
        return this.getNativeScreen('settings/reset.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.resetScreen.title);
    }

    selectResetAppButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.resetScreen.resetButton);
        Actions.click(this.resetScreen.resetButton);
    }

    verifyResetAppInfo() {
        Actions.isVisible(this.resetScreen.resetAppInfo);
    }

    verifyResetAppWarningMessage() {
        Actions.isVisible(this.resetScreen.resetAppWarningMessage);
    }
}

module.exports = ResetPage;
