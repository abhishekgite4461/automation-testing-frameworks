const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class AutoLogOffSettingsPage extends AuthPage {
    /**
     * @returns {AutoLogOffSettingsScreen}
     */
    get autoLogOffSettingsScreen() {
        return this.getNativeScreen('settings/autoLogOffSettings.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.autoLogOffSettingsScreen.title);
    }

    selectInactivityTimeTenMinutes() {
        Actions.click(this.autoLogOffSettingsScreen.inactivityTimeTenMins);
    }

    clickConfirmButton() {
        Actions.click(this.autoLogOffSettingsScreen.confirmButton);
    }

    clickAutoLogOffTimer(autoLogOffTime) {
        Actions.swipeDownOnSmallScreen();
        Actions.click(this.locateOptionsInAutoLogOffPage(autoLogOffTime));
        Actions.click(this.autoLogOffSettingsScreen.confirmButton);
        Actions.reportTimer('Save_AutoLogOff', 'successfully');
    }

    locateOptionsInAutoLogOffPage(optionsInAutoLogOffTime) {
        switch (optionsInAutoLogOffTime) {
        case 'logoffImmediate':
            return this.autoLogOffSettingsScreen.autoLogOffImmediately;
        case '1 Minute':
            return this.autoLogOffSettingsScreen.autoLogOff1minute;
        case '2 Minute':
            return this.autoLogOffSettingsScreen.autoLogOff2minute;
        case '5 Minute':
            return this.autoLogOffSettingsScreen.autoLogOff5minute;
        case '10 Minute':
            return this.autoLogOffSettingsScreen.autoLogOff10minute;
        default:
            throw new Error(`Unknown autoLogOffTime: ${optionsInAutoLogOffTime}`);
        }
    }

    verifyConfirmButtonIsNotEnabled() {
        Actions.isEnabled(this.autoLogOffSettingsScreen.confirmButton).should.equal(false);
    }
}

module.exports = AutoLogOffSettingsPage;
