const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class BusinessDetailsPage extends AuthPage {
    /**
     * @returns {BusinessDetailsScreen}
     */
    get businessDetailsScreen() {
        return this.getNativeScreen('settings/businessDetails.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.businessDetailsScreen.title);
    }

    clickBackButton() {
        Actions.click(this.businessDetailsScreen.backButton);
    }
}

module.exports = BusinessDetailsPage;
