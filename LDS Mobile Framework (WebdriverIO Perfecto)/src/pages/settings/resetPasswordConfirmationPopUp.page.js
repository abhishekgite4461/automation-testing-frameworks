const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
require('chai').should();

class ResetPasswordConfirmationPopUpPage extends AuthPage {
    /**
     * @returns {ResetPasswordConfirmationPopUpScreen}
     */
    get resetPasswordConfirmationPopUpScreen() {
        return this.getNativeScreen('settings/resetPasswordConfirmationPopUp.screen');
    }

    waitForResetPasswordConfirmationPopUpPageLoad() {
        Actions.waitForVisible(this.resetPasswordConfirmationPopUpScreen.title);
    }

    clickOkAndLogOffButton() {
        Actions.click(this.resetPasswordConfirmationPopUpScreen.OkAndLogOffButton);
        Actions.reportTimer('Ok_LogOff', 'Mobile Banking');
    }

    resetPasswordConfirmationPopUpIsNotVisible() {
        Actions.isVisible(this.resetPasswordConfirmationPopUpScreen.title).should.equal(false);
    }
}

module.exports = ResetPasswordConfirmationPopUpPage;
