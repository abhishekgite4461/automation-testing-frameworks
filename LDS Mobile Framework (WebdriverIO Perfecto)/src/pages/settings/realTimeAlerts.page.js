require('chai').should();
const Actions = require('../common/actions');
const DeviceHelper = require('../../utils/device.helper');
const AuthPage = require('../common/auth.page');
const SwipeAction = require('../../enums/swipeAction.enum');
const SavedAppDataRepository = require('../../data/savedAppData.repository.js');
const SavedData = require('../../enums/savedAppData.enum.js');
const deviceHelper = require('../../utils/device.helper');

class RealTimeAlertsPage extends AuthPage {
    /**
     * @returns {RealTimeAlertsScreen}
     */
    get realTimeAlertsScreen() {
        return this.getNativeScreen('settings/realTimeAlerts.screen');
    }

    get logoutScreen() {
        return this.getNativeScreen('logout.screen');
    }

    get multipleDeviceManagementNotificationsScreen() {
        return this.getNativeScreen('settings/multipleDeviceManagementNotifications.screen');
    }

    selectOptInAccountAlerts() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.realTimeAlertsScreen.accountAlertsToggleSwitch);
        if (Actions.isChecked(this.realTimeAlertsScreen.accountAlertsToggleSwitch)) {
            Actions.click(this.realTimeAlertsScreen.accountAlertsToggleSwitch);
            Actions.waitForVisible(this.realTimeAlertsScreen.optOutConfirmationAlertTitle);
            Actions.click(this.realTimeAlertsScreen.okButton);
        }
        Actions.click(this.realTimeAlertsScreen.accountAlertsToggleSwitch);
    }

    selectOptInAccountAlertsMDM() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.realTimeAlertsScreen.newAccountAlertsToggleSwitch);
        if (Actions.isChecked(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch)) {
            Actions.click(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch);
            Actions.waitForVisible(this.realTimeAlertsScreen.optOutConfirmationAlertTitle);
            Actions.click(this.realTimeAlertsScreen.okButton);
        }
        Actions.click(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch);
    }

    selectOptOutAccountAlerts() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.realTimeAlertsScreen.accountAlertsToggleSwitch);
        if (Actions.isChecked(this.realTimeAlertsScreen.accountAlertsToggleSwitch)) {
            Actions.click(this.realTimeAlertsScreen.accountAlertsToggleSwitch);
        }
    }

    selectAPNSAllowButton(type) {
        if (Actions.isVisible(this.realTimeAlertsScreen.apnsTitle)) {
            if (type === 'allow') {
                Actions.click(this.realTimeAlertsScreen.apnsAllowButton);
            } else {
                Actions.click(this.realTimeAlertsScreen.apnsDontAllowButton);
            }
        }
    }

    verifyConfirmationAlert(type) {
        if (type === 'success') {
            Actions.waitForVisible(this.realTimeAlertsScreen.confirmationAlertTitle);
        } else {
            Actions.waitForVisible(this.realTimeAlertsScreen.optOutConfirmationAlertTitle);
        }
        Actions.click(this.realTimeAlertsScreen.okButton);
    }

    verifyAccountAlertsToggleValue(type) {
        if (type === 'in for') {
            Actions.isChecked(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch).should.equal(true);
        } else {
            Actions.isChecked(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch).should.equal(false);
        }
    }

    verifyPushPromptScreen(screentype) {
        if (screentype === 'A') {
            Actions.waitForVisible(this.realTimeAlertsScreen.pushPromptScreenATitle);
        } else {
            Actions.waitForVisible(this.realTimeAlertsScreen.pushPromptScreenBTitle);
        }
    }

    selectOption(type) {
        if (type === 'Not now') {
            Actions.swipeUntilAnyVisible(SwipeAction.UP, this.realTimeAlertsScreen.notNowOption);
            Actions.click(this.realTimeAlertsScreen.notNowOption);
        } else {
            Actions.swipeUntilAnyVisible(SwipeAction.UP, this.realTimeAlertsScreen.turnOnNotificationsOption);
            Actions.click(this.realTimeAlertsScreen.turnOnNotificationsOption);
        }
    }

    selectDeviceSettings() {
        browser.pause(500);
        Actions.swipePage(SwipeAction.UP);
        browser.click(this.realTimeAlertsScreen.deviceSettings);
    }

    selectDateOption() {
        if (DeviceHelper.isAndroid()) {
            this.setDateOnAndroid();
        } else {
            this.setDateOnIOS();
        }
    }

    setDateOnAndroid() {
        Actions.swipeUntilAnyVisible(SwipeAction.UP, this.realTimeAlertsScreen.generalManagement);
        Actions.click(this.realTimeAlertsScreen.generalManagement);
        Actions.swipeUntilAnyVisible(SwipeAction.UP, this.realTimeAlertsScreen.dateAndTime);
        Actions.click(this.realTimeAlertsScreen.dateAndTime);
        if (!Actions.isVisible(this.realTimeAlertsScreen.setDate)) {
            Actions.click(this.realTimeAlertsScreen.automaticDateAndTime);
        }
        browser.click(this.realTimeAlertsScreen.setDate);
    }

    setDateOnIOS() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.realTimeAlertsScreen.generalButton);
        Actions.click(this.realTimeAlertsScreen.generalButton);
        Actions.swipeUntilVisible(SwipeAction.UP, this.realTimeAlertsScreen.dateAndTimeButton);
        Actions.click(this.realTimeAlertsScreen.dateAndTimeButton);
        Actions.swipeUntilVisible(SwipeAction.UP, this.realTimeAlertsScreen.setAutomaticDateButton);
        Actions.click(this.realTimeAlertsScreen.setAutomaticDateToggle);
    }

    incrementDate(numOfDays) {
        const today = new Date();
        today.setDate(today.getDate() + numOfDays);
        const newDate = today.getDate();
        const newMonth = today.getMonth();
        if (DeviceHelper.isAndroid()) {
            while (!Actions.isVisible(this.realTimeAlertsScreen.futureDate(newDate, newMonth))) {
                Actions.waitForVisible(this.realTimeAlertsScreen.nextCalenderMonthOption);
                Actions.click(this.realTimeAlertsScreen.nextCalenderMonthOption);
            }
            this.selectDate(newDate, newMonth);
            Actions.click(this.realTimeAlertsScreen.doneOption);
        } else {
            Actions.click(this.realTimeAlertsScreen.datePickerTitle);
        }
    }

    selectDate(newDate, newMonth) {
        Actions.click(this.realTimeAlertsScreen.futureDate(newDate, newMonth));
    }

    navigatePushPromptScreenBbyNextButton() {
        for (let i = 0; i < 2; i++) {
            Actions.waitForVisible(this.realTimeAlertsScreen.nextOption);
            Actions.click(this.realTimeAlertsScreen.nextOption);
        }
    }

    navigatePushPromptScreenBbySwipe() {
        Actions.swipePage(SwipeAction.RIGHT);
        Actions.waitForVisible(this.realTimeAlertsScreen.pushPromptScreenBSecondTitle);
        Actions.swipePage(SwipeAction.LEFT);
        Actions.waitForVisible(this.realTimeAlertsScreen.pushPromptScreenBThirdTitle);
    }

    selectAppsOption() {
        Actions.swipeUntilAnyVisible(SwipeAction.UP, this.realTimeAlertsScreen.apps);
        Actions.click(this.realTimeAlertsScreen.apps);
    }

    turnOffDeviceNotifications() {
        if (DeviceHelper.isIOS()) {
            this.turnOffDeviceNotificationsIos();
        } else {
            this.turnOffDeviceNotificationsAndroid();
        }
    }

    turnOffDeviceNotificationsAndroid() {
        if (Actions.isVisible(this.realTimeAlertsScreen.deviceNotificationsStatusOn)) {
            Actions.click(this.realTimeAlertsScreen.deviceNotificationsStatusOn);
        }
        Actions.isVisible(this.realTimeAlertsScreen.deviceNotificationsStatusOff);
    }

    turnOffDeviceNotificationsIos() {
        Actions.waitForVisible(this.multipleDeviceManagementNotificationsScreen.deviceSettingsnotificationsButton);
        Actions.click(this.multipleDeviceManagementNotificationsScreen.deviceSettingsnotificationsButton);
        Actions.waitForVisible(this.multipleDeviceManagementNotificationsScreen.deviceAllowNotificationsToggleButton);
        const deviceNotificationsToggleValue = Actions.getAttribute(this.multipleDeviceManagementNotificationsScreen.deviceAllowNotificationsToggleButton, 'value');
        if (deviceNotificationsToggleValue !== '0') {
            Actions.click(this.multipleDeviceManagementNotificationsScreen.deviceAllowNotificationsToggleButton);
        }
    }

    selectAppNotifications(app) {
        this.swipeUntilBrandVisible(app);
        Actions.click(this.getArrangementLocatorByType(app));
        Actions.swipeUntilAnyVisible(SwipeAction.UP, this.realTimeAlertsScreen.deviceNotifications);
        Actions.click(this.realTimeAlertsScreen.deviceNotifications);
    }

    swipeUntilBrandVisible(app) {
        Actions.swipeUntilAnyVisible(SwipeAction.UP, this.getArrangementLocatorByType(app));
    }

    getArrangementLocatorByType(app) {
        switch (app) {
        case 'BOS':
            return this.realTimeAlertsScreen.bosDeviceSettings;
        case 'HFX':
            return this.realTimeAlertsScreen.halifaxDeviceSettings;
        case 'LDS':
            return this.realTimeAlertsScreen.lloydsDeviceSettings;
        default:
            throw new Error(`Unknown arrangement type: ${app}`);
        }
    }

    verifyConflictPromptScreen() {
        Actions.waitForVisible(this.realTimeAlertsScreen.conflictScreenTitle);
    }

    verifyDeviceSettingsPage() {
        Actions.waitForVisible(this.realTimeAlertsScreen.appNotificationsDeviceTitle);
    }

    turnOnDeviceNotifications() {
        if (deviceHelper.isIOS()) {
            Actions.waitForVisible(this.realTimeAlertsScreen.iosDeviceNotificationsButton);
            Actions.click(this.realTimeAlertsScreen.iosDeviceNotificationsButton);
        }
        Actions.click(this.realTimeAlertsScreen.deviceNotificationsStatusOff);
        Actions.waitForVisible(this.realTimeAlertsScreen.deviceNotificationsStatusOn);
    }

    verifySessionStatus(status) {
        if (status === 'still') {
            Actions.isVisible(this.logoutScreen.title).should.equal(false);
        } else {
            Actions.isVisible(this.logoutScreen.title).should.equal(true);
        }
    }

    selectTurnOffInApp() {
        Actions.click(this.realTimeAlertsScreen.turnOffInAppButton);
    }

    verifyTurnOffConfirmationPage1() {
        Actions.waitForVisible(this.realTimeAlertsScreen.turnOffConfirmationLoginPageTitle);
    }

    verifyTurnOffConfirmationPage2() {
        Actions.waitForVisible(this.realTimeAlertsScreen.optOutConfirmationAlertTitle);
    }

    verifyOptions() {
        Actions.isVisible(this.realTimeAlertsScreen.notNowOption);
        Actions.isVisible(this.realTimeAlertsScreen.turnOffInAppButton);
    }

    selectNotNowButton() {
        Actions.click(this.realTimeAlertsScreen.notNowConflictOption);
    }

    selectOptOutOkButton() {
        Actions.click(this.realTimeAlertsScreen.optOutOkButton);
    }

    selectNoThanksButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.realTimeAlertsScreen.noThanksNotificationsOption);
        Actions.click(this.realTimeAlertsScreen.noThanksNotificationsOption);
    }

    VerifyConflictAlert() {
        Actions.waitForVisible(this.realTimeAlertsScreen.conflictprompt);
    }

    ConflictPopupDeviceSettings() {
        Actions.waitForVisible(this.realTimeAlertsScreen.conflictpopup);
    }

    ClickNotNowOnConflictPopup() {
        Actions.click(this.realTimeAlertsScreen.notnowconflictpopup);
    }

    verifyIncompleteActionsOverlay() {
        Actions.waitForVisible(this.realTimeAlertsScreen.incompleteActionsOverlay);
    }

    selectIncompleteActionsOverlayOKButton() {
        Actions.click(this.realTimeAlertsScreen.incompleteActionsOverlayOKButton);
    }

    shouldSeeEnableNotificationsInstruction() {
        Actions.waitForVisible(this.realTimeAlertsScreen.enableNotificationInstruction);
    }

    verifyCorrectAlertToggleStatus() {
        Actions.pause(1000);
        const toggleValue = (SavedAppDataRepository.getData(SavedData.NOTIFICATIONS_TOGGLE_VALUE));
        Actions.getAttributeText(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch).should.equal(toggleValue);
    }

    static getActualText(locator, attribute) {
        Actions.waitForVisible(locator);
        if (DeviceHelper.isAndroid()) {
            return Actions.getAttributeText(locator);
        }
        return Actions.getAttribute(locator, attribute);
    }

    checkToggleStatus(userAbility) {
        this.saveToggleValue();
        const toggleValue = (SavedAppDataRepository.getData(SavedData.NOTIFICATIONS_TOGGLE_VALUE));
        Actions.waitForVisible(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch);
        Actions.click(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch);
        Actions.pause(10000);
        if (DeviceHelper.isIOS()
            && Actions.isVisible(this.realTimeAlertsScreen.confirmationAlertTitle)) {
            Actions.click(this.realTimeAlertsScreen.okButton);
        }
        if (userAbility === 'not') {
            RealTimeAlertsPage.getActualText(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch, 'value').should.equal(toggleValue);
        } else {
            RealTimeAlertsPage.getActualText(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch, 'value').should.not.equal(toggleValue);
            Actions.click(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch);
            if (DeviceHelper.isIOS()) {
                Actions.waitForVisible(this.realTimeAlertsScreen.okButton);
                Actions.click(this.realTimeAlertsScreen.okButton);
            }
        }
    }

    saveToggleValue() {
        if (DeviceHelper.isAndroid()) {
            SavedAppDataRepository.setData(
                SavedData.NOTIFICATIONS_TOGGLE_VALUE,
                Actions.getAttributeText(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch)
            );
        } else {
            SavedAppDataRepository.setData(
                SavedData.NOTIFICATIONS_TOGGLE_VALUE,
                Actions.getAttribute(this.realTimeAlertsScreen.newAccountAlertsToggleSwitch, 'value')
            );
        }
    }

    selectGeneralButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.realTimeAlertsScreen.generalButton);
        Actions.click(this.realTimeAlertsScreen.generalButton);
        Actions.swipeUntilVisible(SwipeAction.UP, this.realTimeAlertsScreen.dateAndTimeButton);
        Actions.click(this.realTimeAlertsScreen.dateAndTimeButton);
        Actions.swipeUntilVisible(SwipeAction.UP, this.realTimeAlertsScreen.setAutomaticDateButton);
        Actions.click(this.realTimeAlertsScreen.setAutomaticDateToggle);
    }

    resetDeviceDate() {
        if (!DeviceHelper.isAndroid()) {
            Actions.appInBackground();
            Actions.launchDeviceSettings();
            this.selectGeneralButton();
        } else {
            Actions.pressAndroidMenuKey();
            browser.pause(500);
            Actions.swipePage(SwipeAction.UP);
            browser.click(this.realTimeAlertsScreen.deviceSettings);
        }
    }

    selectGoToDeviceSettings() {
        Actions.click(this.realTimeAlertsScreen.goToDeviceSettingsButton);
    }

    selectReturnToAppButton() {
        Actions.click(this.realTimeAlertsScreen.returnToApp);
    }
}
module.exports = RealTimeAlertsPage;
