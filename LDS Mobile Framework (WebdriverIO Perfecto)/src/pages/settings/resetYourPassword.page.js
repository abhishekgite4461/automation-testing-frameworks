const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
require('chai').should();

class ResetYourPasswordPage extends AuthPage {
    /**
     * @returns {ResetYourPasswordScreen}
     */
    get resetYourPasswordScreen() {
        return this.getNativeScreen('settings/resetYourPassword.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.resetYourPasswordScreen.title);
    }

    enterNewPassword(password) {
        Actions.click(this.resetYourPasswordScreen.enterNewPasswordEditBox);
        Actions.setImmediateValueAndTapOut(this.resetYourPasswordScreen.enterNewPasswordEditBox, password);
    }

    confirmNewPassword(password) {
        Actions.click(this.resetYourPasswordScreen.confirmNewPasswordEditBox);
        Actions.setImmediateValueAndTapOut(this.resetYourPasswordScreen.confirmNewPasswordEditBox, password);
    }

    clickSubmitButton() {
        Actions.click(this.resetYourPasswordScreen.clickSubmitButton);
        Actions.reportTimer('Reset_Password_Submit', 'has been reset');
    }

    resetPasswordSuccess() {
        Actions.isVisible(this.resetYourPasswordScreen.resetSuccessfulPopupMessage).should.equal(true);
    }

    clickPasswordLogOff() {
        Actions.click(this.resetYourPasswordScreen.LogOffButton);
    }

    verifyNonIdenticalErrorMessage() {
        Actions.isVisible(this.resetYourPasswordScreen.nonIdenticalErrorMessage).should.equal(true);
    }

    verifyNoPersonalDetailsErrorMessage() {
        Actions.waitForVisible(this.resetYourPasswordScreen.noPersonalDetailsErrorMessage, 30000);
    }

    verifyNonSecureErrorMessage() {
        Actions.isVisible(this.resetYourPasswordScreen.nonSecurePasswordErrorMessage).should.equal(true);
    }

    verifyLastFivePasswordErrorMessage() {
        Actions.waitForEnabled(this.resetYourPasswordScreen.lastFivePasswordErrorMessage, 5000);
        Actions.isVisible(this.resetYourPasswordScreen.lastFivePasswordErrorMessage).should.equal(true);
    }

    selectTipsForPasswordAccordion() {
        Actions.click(this.resetYourPasswordScreen.tipsForPasswordAccordion);
    }

    selectCloseButton() {
        Actions.click(this.resetYourPasswordScreen.closeButton);
    }

    verifyPasswordSecurityTips(tipsForPasswordContentToolTip) {
        Actions.isVisible(this.resetYourPasswordScreen
            .tipsForPasswordToolTip).should.equal(tipsForPasswordContentToolTip);
    }

    verifyEasyToGuessPasswordErrorMessage() {
        Actions.waitForVisible(this
            .resetYourPasswordScreen.easyToGuessPasswordErrorMessage);
    }
}

module.exports = ResetYourPasswordPage;
