require('chai').should();

const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');

class marketingPreferencesConfirmation extends AuthPage {
    /**
     * @returns {marketingPreferencesConfirmation}
     * */

    get marketingPreferencesConfirmation() {
        return this.getNativeScreen('settings/personalDetails/marketingPreferencesConfirmation.screen');
    }

    isPageVisible() {
        Actions.waitForVisible(this.marketingPreferencesConfirmation.title);
    }

    selectReturnToAccoutns() {
        Actions.click(this.marketingPreferencesConfirmation.accountsButton);
    }

    selectYourProfile() {
        Actions.click(this.marketingPreferencesConfirmation.profileButton);
    }
}
module.exports = marketingPreferencesConfirmation;
