require('chai').should();

const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');
const PersonalDetail = require('../../../enums/personalDetail.enum');
const SwipeAction = require('../../../enums/swipeAction.enum');
const SavedAppDataRepository = require('../../../data/savedAppData.repository');
const SavedData = require('../../../enums/savedAppData.enum');
const DeviceHelper = require('../../../utils/device.helper');
const BusinessDetail = require('../../../enums/businessDetail.enum');

class PersonalDetailsPage extends AuthPage {
    static getPersonalDetailFromString(detail) {
        return PersonalDetail.fromString(detail.trim().replace(/ /g, '_').toUpperCase());
    }

    static getBusinessDetailFromString(detail) {
        return BusinessDetail.fromString(detail.trim().replace(/ /g, '_').toUpperCase());
    }

    /**
     * @returns {PersonalDetailsScreen}
     */
    get personalDetailsScreen() {
        return this.getNativeScreen('settings/personalDetails/personalDetails.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.personalDetailsScreen.title);
    }

    isPageVisible() {
        Actions.swipeUntilVisible(SwipeAction.DOWN,
            this.personalDetailsScreen.title, 0, false, 5, 100, 15000);
        Actions.isExisting(this.personalDetailsScreen.title).should.equal(true);
    }

    selectMobileDetails() {
        Actions.waitForVisible(this.personalDetailsScreen.mobileNumber, 15000);
        Actions.click(this.personalDetailsScreen.mobileNumber);
    }

    selectAddressDetails() {
        Actions.waitForVisible(this.personalDetailsScreen.addressContainer);
        Actions.click(this.personalDetailsScreen.addressContainer);
    }

    selectEmailAddressDetails() {
        Actions.click(this.personalDetailsScreen.emailContainer);
    }

    clickBackButton() {
        Actions.click(this.personalDetailsScreen.backButton);
    }

    confirmUpdatedEmailAddress(email) {
        if (!Actions.isVisible(this.personalDetailsScreen.updatedEmailAddressField(email))) {
            throw new Error('Email Address not updated in the personal details');
        }
    }

    shouldSeePersonalDetails(personalDetails) {
        for (const personalDetail of personalDetails) {
            const field = PersonalDetailsPage.getPersonalDetailFromString(personalDetail.personalDetail);
            const fieldLocator = this.getLocatorForDetail(field);
            Actions.swipeUntilVisible(SwipeAction.UP, fieldLocator);
        }
        if (DeviceHelper.isRNGA()) {
            const currentPostCode = Actions.getAttributeText(this.personalDetailsScreen.postcode);
            SavedAppDataRepository.setData(SavedData.CURRENT_POST_CODE, currentPostCode);
        }
    }

    getLocatorForDetail(field) {
        switch (field) {
        case PersonalDetail.FULL_NAME:
            return this.personalDetailsScreen.fullName;
        case PersonalDetail.USER_ID:
            return this.personalDetailsScreen.userId;
        case PersonalDetail.MOBILE_NUMBER:
            return this.personalDetailsScreen.mobileNumber;
        case PersonalDetail.HOME_NUMBER:
            return this.personalDetailsScreen.homeNumber;
        case PersonalDetail.WORK_NUMBER:
            return this.personalDetailsScreen.workNumber;
        case PersonalDetail.WORK_EXTENSION_NUMBER:
            return this.personalDetailsScreen.workExtensionNumber;
        case PersonalDetail.ADDRESS:
            return this.personalDetailsScreen.address;
        case PersonalDetail.POSTCODE:
            return this.personalDetailsScreen.postcode;
        case PersonalDetail.EMAIL_ADDRESS:
            return this.personalDetailsScreen.email;
        case BusinessDetail.BUSINESS_NAME:
            return this.personalDetailsScreen.businessName;
        case BusinessDetail.BUSINESS_REGISTEREDADDRESSTEXT:
            return this.personalDetailsScreen.businessRegisteredAddressText;
        case BusinessDetail.BUSINESS_ADDRESS:
            return this.personalDetailsScreen.businessAddress;
        case BusinessDetail.BUSINESS_POSTCODE:
            return this.personalDetailsScreen.businessPostcode;
        default:
            throw new Error(`Unknown personal detail field: ${field}`);
        }
    }

    selectPrimaryMarketingPreferencesLink() {
        Actions.swipeByPercentage(70, 80, 70, 30, 1000);
        Actions.click(this.personalDetailsScreen.primaryMarketingLink);
    }

    selectSecondaryMarketingPreferencesLink() {
        Actions.swipeByPercentage(70, 80, 70, 30, 1000);
        Actions.click(this.personalDetailsScreen.secondaryMarketingLink);
    }

    selectPrimaryOrSecondaryMarketingPreferencesLink(link) {
        if (link === 'primary') {
            this.selectPrimaryMarketingPreferencesLink();
        } else if (link === 'secondary') {
            this.selectSecondaryMarketingPreferencesLink();
        }
    }

    verifyPrimaryMarketingPreferencesLinkVisibility(visibility) {
        Actions.isVisible(this.personalDetailsScreen.primaryMarketingLink).should.equal(visibility);
    }

    verifySecondaryMarketingPreferencesLinkVisibility(visibility) {
        Actions.isVisible(this.personalDetailsScreen.secondaryMarketingLink).should.equal(visibility);
    }

    verifyPrimaryAndSecondaryMarketingPreferencesLinkIsVisible(visibility) {
        Actions.swipeByPercentage(70, 80, 70, 30, 1000);
        this.verifyPrimaryMarketingPreferencesLinkVisibility(visibility);
        this.verifySecondaryMarketingPreferencesLinkVisibility(visibility);
    }

    verifyPrimaryAndSecondaryMarketingPreferencesLinkNotVisible(visibility) {
        Actions.swipeByPercentage(70, 80, 70, 30, 1000);
        this.verifyPrimaryMarketingPreferencesLinkVisibility(visibility);
        this.verifySecondaryMarketingPreferencesLinkVisibility(visibility);
    }

    verifyEitherPrimaryOrSecondaryMarketingPreferencesLink(link) {
        Actions.swipeByPercentage(70, 80, 70, 30, 1000);
        if (link === 'primary') {
            this.verifyPrimaryMarketingPreferencesLinkVisibility(true);
            this.verifySecondaryMarketingPreferencesLinkVisibility(false);
        } else if (link === 'secondary') {
            this.verifyPrimaryMarketingPreferencesLinkVisibility(false);
            this.verifySecondaryMarketingPreferencesLinkVisibility(true);
        }
    }

    verifyUpdatedAddress() {
        const expectedPostCode = SavedAppDataRepository.getData(SavedData.COA_ADDRESS_LIST_POSTCODE);
        const expectedAddress = SavedAppDataRepository.getData(SavedData.COA_SELECTED_ADDRESS)
            .replace(`, ${expectedPostCode}`, '');

        const printedAddress = Actions.getAttributeText(this.personalDetailsScreen.address, 'label');
        if (DeviceHelper.isAndroid()) {
            printedAddress.replace(new RegExp('\\n', 'g'), ', ').should.equal(expectedAddress);
        } else {
            printedAddress.should.equal(expectedAddress.replace(new RegExp(',', 'g'), ''));
        }

        const printedPostCode = Actions.getAttributeText(this.personalDetailsScreen.postcode, 'label').trim().toUpperCase();
        printedPostCode.should.equal(expectedPostCode.toUpperCase());
    }

    verifyDataConsentLinkVisibility() {
        Actions.swipeByPercentage(70, 80, 70, 30, 1000);
        Actions.isVisible(this.personalDetailsScreen.dataConsentLink).should.equal(true);
    }

    selectDataConsentLink() {
        Actions.swipeByPercentage(70, 80, 70, 30, 1000);
        Actions.click(this.personalDetailsScreen.dataConsentLink);
    }

    shouldNotSeeTheDataConsentLink() {
        Actions.isVisible(this.personalDetailsScreen.dataConsentLink).should.equal(false);
    }

    validateKycDetails(kycDetails) {
        for (let i = 0; i < kycDetails.length; i++) {
            const updatedDetails = this.personalDetailsScreen[kycDetails[i].details];
            Actions.isVisible(updatedDetails).should.equal(true);
        }
    }

    shouldSeeBusinessDetails(businessDetails) {
        for (const businessDetail of businessDetails) {
            const field = PersonalDetailsPage.getBusinessDetailFromString(businessDetail.businessDetail);
            const fieldLocator = this.getLocatorForDetail(field);
            Actions.swipeUntilVisible(SwipeAction.UP, fieldLocator);
        }
    }
}

module.exports = PersonalDetailsPage;
