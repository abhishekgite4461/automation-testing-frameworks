const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');

class PhoneOutcomePage extends AuthPage {
    /**
     * @returns {PhoneOutcomeScreen}
     */
    get phoneOutcomeScreen() {
        return this.getNativeScreen('settings/personalDetails/phoneOutcome.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.phoneOutcomeScreen.title);
    }

    isPageVisible() {
        Actions.isVisible(this.phoneOutcomeScreen.title).should.equal(true);
    }

    isMessageVisible() {
        Actions.isVisible(this.phoneOutcomeScreen.message).should.equal(true);
    }

    isBackToContactDetailsButtonVisible() {
        Actions.isVisible(this.phoneOutcomeScreen.backToButton).should.equal(true);
    }

    clickBackToContactDetailsButton() {
        Actions.click(this.phoneOutcomeScreen.backToButton);
    }
}

module.exports = PhoneOutcomePage;
