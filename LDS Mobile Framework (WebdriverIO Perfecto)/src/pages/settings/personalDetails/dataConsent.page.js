require('chai').should();

const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');
const SwipeAction = require('../../../enums/swipeAction.enum');
const DeviceHelper = require('../../../utils/device.helper');

class DataConsent extends AuthPage {
    /**
     * @returns {DataConsentWebScreen}
     */
    get dataConsentWebScreen() {
        return this.getWebScreen('dataConsent.screen');
    }
    /**
     * @returns {DataConsentScreen}
     */

    get dataConsent() {
        return this.getNativeScreen('settings/personalDetails/dataConsent.screen');
    }

    get bngaDataConsent() {
        return this.getNativeScreen('bnga/dataConsent.screen');
    }

    /**
     * @returns {GlobalMenuScreen}
     */
    get globalMenuScreen() {
        return this.getNativeScreen('globalMenu.screen');
    }

    /**
     * @returns {HomeScreen}
     */
    get homeScreen() {
        return this.getNativeScreen('home.screen');
    }

    waitForDataConsentPageLoad() {
        Actions.waitForVisible(this.dataConsent.dataConsentInterstitialPageTitle, 20000);
    }

    waitForManageDataConsentPageLoad() {
        Actions.waitForVisible(this.dataConsent.dataConsentManageConsentPageTitle);
    }

    waitForExternalDataConsentPageLoad() {
        Actions.pause(5000);
        if (DeviceHelper.isIOS()) {
            if (Actions.isVisible(this.dataConsent.acceptCookiesPolicyPageOnWebPage)) {
                Actions.click(this.dataConsent.acceptCookiesPolicyPageOnWebPage);
            } else {
                Actions.click(this.dataConsent.KeepChoicesCookiesPolicyPageOnWebPage);
            }
        }
        Actions.isVisible(this.dataConsentWebScreen.cookiesPolicyPagetitle).should.equal(true);
    }

    shouldSeePageTitle(pageType) {
        if (pageType === 'manage consent') {
            this.waitForManageDataConsentPageLoad();
        } else if (pageType === 'data consent' && DeviceHelper.isBNGA()) {
            Actions.isVisible(this.bngaDataConsent.title).should.equal(true);
        } else {
            this.waitForDataConsentPageLoad();
        }
    }

    shouldSeeInterstitialOrHomePage() {
        const locator = Actions.waitUntilVisible([
            this.dataConsent.dataConsentInterstitialPageTitle,
            this.homeScreen.title
        ]);

        if (locator === this.dataConsent.dataConsentInterstitialPageTitle) {
            this.selectAcceptAllButton();
        }
    }

    shouldSeeSelectionOptions() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.dataConsent.dataConsentManageConsentButton);
        this.shouldSeeAcceptAllButton();
        this.shouldSeeManageConsentButton();
    }

    shouldSeeAcceptAllButton() {
        Actions.isVisible(this.dataConsent.dataConsentAcceptAllButton).should.equal(true);
    }

    shouldSeeManageConsentButton() {
        Actions.isVisible(this.dataConsent.dataConsentManageConsentButton).should.equal(true);
    }

    selectAcceptAllButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.dataConsent.dataConsentAcceptAllButton);
        Actions.click(this.dataConsent.dataConsentAcceptAllButton);
    }

    shouldNotSeeGlobalBackButton() {
        Actions.isVisible(this.dataConsent.globalBackButton).should.equal(false);
    }

    shouldNotSeeTheDataConsentInterstitial() {
        Actions.isVisible(this.dataConsent.dataConsentInterstitialPageTitle).should.equal(false);
    }

    selectButtonInSection(button, dataConsentSection) {
        Actions.waitAndClick(this.getDataConsentToggleButtonLocator(button, dataConsentSection));
    }

    assertDataConsentButtonState(expectedState, dataConsentSection) {
        this.getDataConsentPreferencesState(dataConsentSection).should.equal(expectedState);
    }

    getDataConsentToggleButtonLocator(button, dataConsentSection) {
        const buttonLocator = this.dataConsent[(button === 'consentOptedIn') ? 'consentOptedIn' : 'consentOptedOut'];
        const sectionLocator = this.dataConsent[dataConsentSection];
        return (sectionLocator + buttonLocator);
    }

    getDataConsentPreferencesState(dataConsentSection) {
        let sectionLocator;
        let optedInButtonLocator;
        let optedOutButtonLocator;
        let optedInButtonState;
        let optedOutButtonState;
        if (DeviceHelper.isAndroid()) {
            sectionLocator = this.dataConsent[dataConsentSection];
            optedInButtonLocator = sectionLocator + this.dataConsent.consentOptedIn;
            optedOutButtonLocator = sectionLocator + this.dataConsent.consentOptedOut;

            const attribute = DeviceHelper.isPerfecto() ? 'contentDesc' : 'content-desc';
            optedInButtonState = Actions.getAttribute(optedInButtonLocator, attribute).includes('Selected');
            optedOutButtonState = Actions.getAttribute(optedOutButtonLocator, attribute).includes('Selected');
        } else {
            sectionLocator = dataConsentSection === 'marketingPrefs' ? this.dataConsent.marketingPrefsText : this.dataConsent.performancePrefsText;
            optedInButtonLocator = sectionLocator;
            optedOutButtonLocator = sectionLocator;

            optedInButtonState = Actions.getAttribute(optedInButtonLocator, 'value').includes('Consent given');
            optedOutButtonState = Actions.getAttribute(optedOutButtonLocator, 'value').includes('Consent not given');
        }

        if (optedInButtonState === true && optedOutButtonState === false) {
            return 'consentOptedIn';
        } if (optedInButtonState === false && optedOutButtonState === true) {
            return 'consentOptedOut';
        } if (optedInButtonState === false && optedOutButtonState === false) {
            return 'unselected';
        }
        throw new Error('unrecognised state for data consent preference buttons');
    }

    toggleDataConsentMarketingPreferences(dataConsentSection) {
        Actions.swipeUntilVisible(SwipeAction.SHORTUP,
            this.getDataConsentToggleButtonLocator('consentOptedIn', dataConsentSection), 0, false, 5, 100, 20000);
        switch (this.getDataConsentPreferencesState(dataConsentSection)) {
        case 'consentOptedIn':
            this.selectButtonInSection('consentOptedOut', dataConsentSection);
            this.assertDataConsentButtonState('consentOptedOut', dataConsentSection);
            break;
        case 'consentOptedOut':
            this.selectButtonInSection('consentOptedIn', dataConsentSection);
            this.assertDataConsentButtonState('consentOptedIn', dataConsentSection);
            break;
        case 'unselected':
            this.selectButtonInSection('consentOptedIn', dataConsentSection);
            this.assertDataConsentButtonState('consentOptedIn', dataConsentSection);
            break;
        default:
            throw new Error('unrecognised state for marketing preference');
        }
    }

    assertDataConsentWinbackModalVisible() {
        Actions.isVisible(this.dataConsent.dataConsentWinbackModal).should.equal(true);
    }

    assertDataConsentExternalWinbackModalVisible() {
        Actions.isVisible(this.dataConsent.dataConsentExternalWinbackModal).should.equal(true);
    }

    selectStayOrLeaveButtonFromWinbackModal(winbackButton) {
        switch (winbackButton) {
        case 'stay on':
            Actions.click(this.dataConsent.dataConsentForWinbackStay);
            break;
        case 'leave':
            Actions.click(this.dataConsent.dataConsentForWinbackLeave);
            break;
        default:
            throw new Error('unrecognised winback modal button ');
        }
    }

    selectDataConsentAccordionLink() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.dataConsent.dataConsentConfirmButton);
        Actions.click(this.dataConsent.dataConsentCookiePolicyLink);
    }

    selectInterstitialDataConsentAccordionLink() {
        Actions.swipeByPercentUntilVisible(this.dataConsent.dataConsentAcceptAllButton);
        Actions.click(this.dataConsent.interstitialDataConsentCookiePolicyLink);
    }

    setStateForDataConsentPreferences(preferenceArray, preferenceStatus) {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.dataConsent.dataConsentManageConsentPageTitle);
        for (let count = 0; count < preferenceArray.length; count++) {
            Actions.swipeUntilVisible(SwipeAction.UP,
                this.getDataConsentToggleButtonLocator(preferenceStatus, [preferenceArray[count]]),
                0, false, 5, 100, 20000);
            this.selectButtonInSection(preferenceStatus, preferenceArray[count]);
            this.getDataConsentPreferencesState(preferenceArray[count]).should.equal(preferenceStatus);
        }
    }

    submitDataConsentPreferences() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.dataConsent.dataConsentConfirmButton);
        Actions.click(this.dataConsent.dataConsentConfirmButton);
    }

    shouldSeeGlobalBackButton() {
        Actions.isVisible(this.globalMenuScreen.globalBackButton).should.equal(true);
    }

    dataConsentConfirmButtonIsEnabled() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.dataConsent.dataConsentConfirmButton);
        Actions.isEnabled(this.dataConsent.dataConsentConfirmButton).should.equal(true);
    }

    dataConsentConfirmButtonIsDisabled() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.dataConsent.dataConsentConfirmButton);
        Actions.isEnabled(this.dataConsent.dataConsentConfirmButton).should.equal(false);
    }

    dataConsentPerformanceButtonsIsUnselected() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.dataConsent.performancePrefs);
        const buttonState = this.getDataConsentPreferencesState('performancePrefs');
        buttonState.should.equal('unselected');
    }

    selectManageConsentButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.dataConsent.dataConsentManageConsentButton);
        Actions.click(this.dataConsent.dataConsentManageConsentButton);
    }

    noConsentSelected() {
        Actions.waitForVisible(this.bngaDataConsent.title);
        Actions.isClickable(this.bngaDataConsent.performanceConsentToggleON).should.equal(true);
        Actions.isClickable(this.bngaDataConsent.performanceConsentToggleOFF).should.equal(true);
    }

    noBackButtonOnInterstitial() {
        Actions.isVisible(this.bngaDataConsent.title);
        Actions.isVisible(this.commonScreen.backButton).should.equal(false);
        Actions.isVisible(this.commonScreen.contactUs).should.equal(false);
    }

    acceptAndConfirmDataConsent() {
        Actions.click(this.bngaDataConsent.performanceConsentToggleON);
        Actions.swipeByPercentUntilVisible(this.bngaDataConsent.confirmConsent);
        Actions.click(this.bngaDataConsent.confirmConsent);
    }

    declineDataConsentAndConfirm() {
        Actions.click(this.bngaDataConsent.performanceConsentToggleOFF);
        Actions.click(this.bngaDataConsent.confirmConsent);
    }

    viewBNGADataConsentPage() {
        Actions.isVisible(this.bngaDataConsent.title);
    }

    viewPerformanceConsent() {
        Actions.isVisible(this.bngaDataConsent.performanceConsent).should.equal(true);
    }

    acceptDataConsent() {
        Actions.click(this.bngaDataConsent.performanceConsentToggleON);
    }

    declineDataConsent() {
        Actions.click(this.bngaDataConsent.performanceConsentToggleOFF);
    }

    confirmConsent() {
        Actions.swipeByPercentUntilVisible(this.bngaDataConsent.confirmConsent);
        Actions.click(this.bngaDataConsent.confirmConsent);
    }

    noWinbackToLeaveTheApp() {
        Actions.isVisible(this.bngaDataConsent.winBackTitle).should.equal(false);
    }

    clickBackButton() {
        Actions.click(this.commonScreen.backButton);
    }

    changeConsentPreference() {
        if (Actions.isClickable(this.bngaDataConsent.performanceConsentToggleON)) {
            Actions.click(this.bngaDataConsent.performanceConsentToggleON);
        } else {
            Actions.click(this.bngaDataConsent.performanceConsentToggleOFF);
        }
    }

    viewWinBackAlert() {
        Actions.isVisible(this.bngaDataConsent.dialogTitle).should.equal(true);
    }

    viewHeaderWinBackAlert() {
        Actions.isVisible(this.bngaDataConsent.winBackTitle).should.equal(true);
    }

    selectCookiePolicyLink() {
        Actions.swipeByPercentUntilVisible(this.bngaDataConsent.cookiePolicyLink);
        Actions.click(this.bngaDataConsent.cookiePolicyLink);
    }

    setConsentOnDevice() {
        Actions.click(this.bngaDataConsent.performanceConsentToggleON);
        Actions.click(this.bngaDataConsent.confirmConsent);
    }
}

module.exports = DataConsent;
