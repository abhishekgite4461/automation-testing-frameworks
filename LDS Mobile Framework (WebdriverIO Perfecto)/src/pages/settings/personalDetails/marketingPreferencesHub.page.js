require('chai').should();

const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');
const SwipeAction = require('../../../enums/swipeAction.enum');
const DeviceHelper = require('../../../utils/device.helper');

class MarketingPreferencesHub extends AuthPage {
    /**
     * @returns {MarketingPreferenceScreen}
     */
    get marketingPreferenceScreen() {
        return this.getWebScreen('marketingPreference.screen');
    }

    /**
     * @returns {MarketingPreferencesHubScreen}
     */

    get marketingPreferencesHub() {
        return this.getNativeScreen('settings/personalDetails/marketingPreferencesHub.screen');
    }

    waitForMarketingPreferencePageLoad() {
        Actions.waitForVisible(this.marketingPreferenceScreen.title, 30000);
    }

    waitForMarketingPreferenceHubPageLoad() {
        Actions.swipeByPercentUntilVisible(this.marketingPreferencesHub.marketingHubIntroduction,
            20, 20, 80, 80, 5000);
    }

    isCoServicingPageVisible(link) {
        if ((DeviceHelper.getBrand() === 'HFX' && link === 'primary')
            || (DeviceHelper.getBrand() === 'BOS' && link === 'secondary')) {
            Actions.waitForVisible(this.marketingPreferencesHub.halMarketingHubIntroductionText);
        } else if ((DeviceHelper.getBrand() === 'HFX' && link === 'secondary')
            || (DeviceHelper.getBrand() === 'BOS' && link === 'primary')) {
            Actions.waitForVisible(this.marketingPreferencesHub.bosMarketingHubIntroductionText);
        }
    }

    swipeToVisibleCoServicedText(link) {
        if ((DeviceHelper.getBrand() === 'HFX' && link === 'primary')
            || (DeviceHelper.getBrand() === 'BOS' && link === 'secondary')) {
            Actions.swipeUntilVisible(SwipeAction.DOWN, this.marketingPreferencesHub.halMarketingHubIntroductionText);
        } else if ((DeviceHelper.getBrand() === 'HFX' && link === 'secondary')
            || (DeviceHelper.getBrand() === 'BOS' && link === 'primary')) {
            Actions.swipeUntilVisible(SwipeAction.DOWN, this.marketingPreferencesHub.bosMarketingHubIntroductionText);
        }
    }

    isOcisErrorMessageVisible(errorMessage) {
        Actions.waitForVisible(this.marketingPreferencesHub.notification, 10000);
        Actions.getText(this.marketingPreferencesHub.notification).should.contain(errorMessage);
    }

    dismissErrorMessage() {
        Actions.click(this.marketingPreferencesHub.closeErrorMessage);
    }

    getToggleButtonLocator(button, preferencesSection) {
        const buttonLocator = this.marketingPreferencesHub[(button === 'optedIn') ? 'optedIn' : 'optedOut'];
        const sectionLocator = this.marketingPreferencesHub[preferencesSection];
        return (sectionLocator + buttonLocator);
    }

    selectButtonInSection(button, preferencesSection) {
        let preferencesState;
        const preferencesSectionState = this.getMarketingPreferencesState(preferencesSection);
        if (preferencesSectionState === 'optedIn') {
            Actions.click(this.getToggleButtonLocator('optedOut', preferencesSection));
            preferencesState = 'optedOut';
        } else {
            Actions.click(this.getToggleButtonLocator('optedIn', preferencesSection));
            preferencesState = 'optedIn';
        }
        return preferencesState;
    }

    assertButtonStateForSection(expectedState, preferenceSection) {
        this.getMarketingPreferencesState(preferenceSection).should.equal(expectedState);
    }

    continueToSecondaryPreferences() {
        if (DeviceHelper.isIOS()) {
            Actions.swipeUntilVisible(SwipeAction.UP, this.marketingPreferencesHub.continueButton);
            Actions.click(this.marketingPreferencesHub.continueButton);
        } else {
            this.submitPreferences();
        }
    }

    submitPreferences() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.marketingPreferencesHub.submitButton);
        Actions.click(this.marketingPreferencesHub.submitButton);
    }

    shouldNotSeeCoServicingPageWhenPrefsSet() {
        Actions.isVisible(this.marketingPreferencesHub.progressIndicator).should.equal(false);
    }

    getMarketingPreferencesState(preferenceSection) {
        const sectionLocator = this.marketingPreferencesHub[preferenceSection];
        const optedInButtonLocator = sectionLocator + this.marketingPreferencesHub.optedIn;
        const optedOutButtonLocator = sectionLocator + this.marketingPreferencesHub.optedOut;
        let optedInButtonState;
        let optedOutButtonState;
        if (DeviceHelper.isAndroid()) {
            const attribute = DeviceHelper.isPerfecto() ? 'contentDesc' : 'content-desc';
            optedInButtonState = Actions.getAttribute(optedInButtonLocator, attribute).includes('Selected');
            optedOutButtonState = Actions.getAttribute(optedOutButtonLocator, attribute).includes('Selected');
        } else {
            optedInButtonState = Actions.getAttribute(optedInButtonLocator, 'value') !== null;
            optedOutButtonState = Actions.getAttribute(optedOutButtonLocator, 'value') !== null;
        }

        if (optedInButtonState === true && optedOutButtonState === false) {
            return 'optedIn';
        } if (optedInButtonState === false && optedOutButtonState === true) {
            return 'optedOut';
        } if (optedInButtonState === false && optedOutButtonState === false) {
            return 'unselected';
        }
        throw new Error('unrecognised state for marketing preference buttons');
    }

    toggleMarketingPreferences(preferenceSection) {
        Actions.swipeUntilVisible(SwipeAction.SHORTUP,
            this.getToggleButtonLocator('optedIn', preferenceSection), 0, false, 20, 100, 20000);
        switch (this.getMarketingPreferencesState(preferenceSection)) {
        case 'optedIn':
            this.selectButtonInSection('optedOut', preferenceSection);
            this.assertButtonStateForSection('optedOut', preferenceSection);
            break;
        case 'optedOut':
            this.selectButtonInSection('optedIn', preferenceSection);
            this.assertButtonStateForSection('optedIn', preferenceSection);
            break;
        case 'unselected':
            this.selectButtonInSection('optedIn', preferenceSection);
            this.assertButtonStateForSection('optedIn', preferenceSection);
            break;
        default:
            throw new Error('unrecognised state for marketing preference');
        }
    }

    swipeToTopOfMarketingHub() {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.marketingPreferencesHub.marketingHubIntroduction);
    }

    checkBrandToVerifyPageIntro(link) {
        if (DeviceHelper.getBrand() === 'BOS' && link === 'primary' || DeviceHelper.getBrand() === 'HFX' && link === 'secondary') {
            return this.marketingPreferencesHub.bosMarketingHubIntroductionText;
        } if (DeviceHelper.getBrand() === 'HFX' && link === 'primary' || DeviceHelper.getBrand() === 'BOS' && link === 'secondary') {
            return this.marketingPreferencesHub.halMarketingHubIntroductionText;
        }
        return this.marketingPreferencesHub.marketingHubIntroduction;
    }

    setStateForPreferences(preferenceArray, preferenceStatus, link) {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.checkBrandToVerifyPageIntro(link));
        for (let count = 0; count < preferenceArray.length; count++) {
            Actions.swipeUntilVisible(SwipeAction.SHORTUP,
                this.getToggleButtonLocator(preferenceStatus, [preferenceArray[count]]), 0, false, 20, 100, 30000);
            const preferencesState = this.selectButtonInSection(preferenceStatus, preferenceArray[count]);
            this.getMarketingPreferencesState(preferenceArray[count]).should.equal(preferencesState);
        }
    }

    assertPreferencesState(preferenceArray, preferenceStatus) {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.marketingPreferencesHub.marketingHubIntroduction);
        for (let count = 0; count < preferenceArray.length; count++) {
            Actions.swipeUntilVisible(SwipeAction.SHORTUP,
                this.getToggleButtonLocator('optedIn', [preferenceArray[count]]), 0, false, 20, 100, 20000);
            this.getMarketingPreferencesState(preferenceArray[count]).should.equal(preferenceStatus);
        }
    }

    assertWinbackModalvisible() {
        Actions.isVisible(this.marketingPreferencesHub.winbackModal).should.equal(true);
    }

    selectButtonFromWinbackModal(winbackButton) {
        switch (winbackButton) {
        case 'stay on':
            Actions.click(this.marketingPreferencesHub.winbackStay);
            break;
        case 'leave':
            Actions.click(this.marketingPreferencesHub.winbackLeave);
            break;
        default:
            throw new Error('unrecognised winback modal button ');
        }
    }

    selectAccordionLink() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.marketingPreferencesHub.submitButton);
        Actions.click(this.marketingPreferencesHub.accordionLink);
    }

    verifyErrorMessage() {
        Actions.isVisible(this.marketingPreferencesHub.inCompleteErrorMessage).should.equal(true);
        Actions.click(this.marketingPreferencesHub.errorMessageClose);
    }

    verifyProgressBarPrimary() {
        Actions.isExisting(this.marketingPreferencesHub.progressIndicator).should.equal(true);
    }

    verifyProgressBarSecondary() {
        Actions.isExisting(this.marketingPreferencesHub.progressIndicator).should.equal(true);
    }

    bigPromptMarketingPreferenceHubLink() {
        Actions.click(this.marketingPreferencesHub.bigPromptMarketingPreferenceHubLink);
    }
}

module.exports = MarketingPreferencesHub;
