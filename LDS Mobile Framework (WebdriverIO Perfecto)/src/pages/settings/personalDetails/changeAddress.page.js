const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');

class ChangeAddressPage extends AuthPage {
    /**
     * @returns {ChangeAddressScreen}
     */
    get changeAddressScreen() {
        return this.getNativeScreen('settings/personalDetails/changeAddress.screen');
    }

    verifyTitle() {
        Actions.waitForVisible(this.changeAddressScreen.title);
    }

    shouldShowCurrentAddressAndPostcode() {
        Actions.waitForVisible(this.changeAddressScreen.addressLabel);
        Actions.waitForVisible(this.changeAddressScreen.address);
        Actions.waitForVisible(this.changeAddressScreen.postcodeLabel);
        Actions.waitForVisible(this.changeAddressScreen.postcode);
    }

    shouldShowChangeAddressButton() {
        Actions.waitForVisible(this.changeAddressScreen.changeAddressButton);
    }

    clickChangeAddressButton() {
        Actions.click(this.changeAddressScreen.changeAddressButton);
    }
}

module.exports = ChangeAddressPage;
