const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');

class ConfirmNumberPage extends AuthPage {
    /**
     * @returns {ConfirmNumberScreen}
     */
    get confirmNumberScreen() {
        return this.getNativeScreen('settings/personalDetails/confirmNumber.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.confirmNumberScreen.title);
    }

    selectOkButton() {
        Actions.click(this.confirmNumberScreen.okButton);
    }
}

module.exports = ConfirmNumberPage;
