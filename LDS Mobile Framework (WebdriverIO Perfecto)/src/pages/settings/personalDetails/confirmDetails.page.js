const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');

class ConfirmDetailsPage extends AuthPage {
    /**
     * @returns {ConfirmDetailsScreen}
     */
    get confirmDetailsScreen() {
        return this.getNativeScreen('settings/personalDetails/confirmDetails.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.confirmDetailsScreen.title);
    }

    selectConfirmContact() {
        Actions.click(this.confirmDetailsScreen.backToContactsButton);
    }
}

module.exports = ConfirmDetailsPage;
