const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');
const MobileHelper = require('../../../utils/mobile.helper');

class EditMobilePage extends AuthPage {
    /**
     * @returns {ChangePhoneNumbersScreen}
     */
    get changePhoneNumbersScreen() {
        return this.getNativeScreen('settings/personalDetails/changePhoneNumbers.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.changePhoneNumbersScreen.confirmEditButton);
    }

    validateOption(mobileNumber, workNumber, homeNumber) {
        MobileHelper.isPersonalMobile(
            Actions.getText(this.changePhoneNumbersScreen.mobileNumberLabel),
            mobileNumber
        );
        MobileHelper.isWorkMobile(
            Actions.getText(this.changePhoneNumbersScreen.workNumberLabel),
            workNumber
        );
        MobileHelper.isHomeInterestRate(
            Actions.getText(this.changePhoneNumbersScreen.homeNumberLabel),
            homeNumber
        );
    }
}

module.exports = EditMobilePage;
