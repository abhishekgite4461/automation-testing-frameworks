const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');
const PhoneNumberType = require('../../../enums/phoneNumberType.enum');

class ConfirmPhoneNumbersPage extends AuthPage {
    /**
     * @returns {ConfirmPhoneNumbersScreen}
     */
    get confirmPhoneNumbersScreen() {
        return this.getNativeScreen('settings/personalDetails/confirmPhoneNumbers.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.confirmPhoneNumbersScreen.title);
    }

    isPageVisible() {
        Actions.isVisible(this.confirmPhoneNumbersScreen.title).should.equal(true);
    }

    areNonEditedNumbersMasked(editedNumberType) {
        const mobileText = Actions.getText(this.confirmPhoneNumbersScreen.mobileNumberField);

        const homeText = Actions.getText(this.confirmPhoneNumbersScreen.homeNumberField);

        const workText = Actions.getText(this.confirmPhoneNumbersScreen.workNumberField);

        let numbers;
        switch (editedNumberType) {
        case PhoneNumberType.MOBILE:
            numbers = {
                Home: homeText,
                Work: workText
            };
            break;
        case PhoneNumberType.HOME:
            numbers = {
                Mobile: mobileText,
                Work: workText
            };
            break;
        case PhoneNumberType.WORK:
            numbers = {
                Mobile: mobileText,
                Home: homeText
            };
            break;
        case PhoneNumberType.WORK_EXTN:
            numbers = {
                Mobile: mobileText,
                Home: homeText,
                Work: workText
            };
            break;
        default:
            throw new Error(`Unknown number type : ${editedNumberType}`);
        }

        const maskedNumberRegex = /\*+/;

        for (const key in numbers) {
            if (Object.prototype.hasOwnProperty.call(numbers, key)) {
                const number = numbers[key];
                if (number.length !== 0 && !maskedNumberRegex.test(number)) {
                    throw new Error(`${number} - ${key} number should be masked!!`);
                }
            }
        }
    }

    phoneNumberFieldsIsVisible() {
        Actions.isAnyVisible(this.confirmPhoneNumbersScreen.mobileNumberField)
            .should.equal(true);
        Actions.isAnyVisible(this.confirmPhoneNumbersScreen.homeNumberField)
            .should.equal(true);
        Actions.isAnyVisible(this.confirmPhoneNumbersScreen.workNumberField)
            .should.equal(true);
        Actions.isAnyVisible(this.confirmPhoneNumbersScreen.workExtensionField)
            .should.equal(true);
    }

    editButtonIsVisible() {
        Actions.isVisible(this.confirmPhoneNumbersScreen.editButton).should.equal(true);
    }

    confirmButtonIsVisible() {
        Actions.isVisible(this.confirmPhoneNumbersScreen.confirmButton).should.equal(true);
    }

    clickEditButton() {
        Actions.click(this.confirmPhoneNumbersScreen.editButton);
    }

    clickConfirmButton() {
        Actions.waitForVisible(this.confirmPhoneNumbersScreen.confirmButton);
        Actions.click(this.confirmPhoneNumbersScreen.confirmButton);
    }

    areEditedNumbersNotMasked(editedNumberType) {
        let number;
        switch (editedNumberType) {
        case PhoneNumberType.MOBILE:
            number = Actions.getText(this.confirmPhoneNumbersScreen.mobileNumberField);
            break;
        case PhoneNumberType.HOME:
            number = Actions.getText(this.confirmPhoneNumbersScreen.homeNumberField);
            break;
        case PhoneNumberType.WORK:
            number = Actions.getText(this.confirmPhoneNumbersScreen.workNumberField);
            break;
        case PhoneNumberType.WORK_EXTN:
            number = Actions.getText(this.confirmPhoneNumbersScreen.workExtensionField);
            break;
        default:
            throw new Error(`Unknown number type : ${editedNumberType}`);
        }

        const maskedNumberRegex = /\*+/;

        if (number.length !== 0 && maskedNumberRegex.test(number)) {
            throw new Error(`${number} - is not masker`);
        }
    }
}

module.exports = ConfirmPhoneNumbersPage;
