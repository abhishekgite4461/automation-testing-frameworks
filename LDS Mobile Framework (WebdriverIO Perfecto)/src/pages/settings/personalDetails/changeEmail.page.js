const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');

class ChangeEmailPage extends AuthPage {
    /**
     * @returns {ChangeEmailScreen}
     */
    get changeEmailScreen() {
        return this.getNativeScreen('settings/personalDetails/changeEmail.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.changeEmailScreen.title);
    }

    isPageVisible() {
        Actions.isVisible(this.changeEmailScreen.confirmButton).should.equal(true);
    }

    isEmailFieldVisible() {
        Actions.isVisible(this.changeEmailScreen.enterEmailAddressField).should.equal(true);
    }

    isReenterEmailFieldVisible() {
        Actions.isVisible(this.changeEmailScreen.reenterEmailAddressField).should.equal(true);
    }

    isValidationErrorVisible() {
        Actions.isVisible(this.changeEmailScreen.errorMessage).should.equal(true);
    }

    isEmailAddressFieldPopulatedWithEmail() {
        const emailRegex = /^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$/;
        const emailAddress = Actions.getText(this.changeEmailScreen.enterEmailAddressField);
        if (!emailRegex.test(emailAddress)) {
            throw new Error(`${emailAddress} - is not an email!`);
        }
    }

    enterEmailAddress(emailAddress) {
        Actions.click(this.changeEmailScreen.enterEmailAddressField);
        Actions.clearText(this.changeEmailScreen.enterEmailAddressField);
        Actions.setImmediateValueAndTapOut(this.changeEmailScreen.enterEmailAddressField, emailAddress);
    }

    reenterEmailAddress(emailAddress) {
        Actions.click(this.changeEmailScreen.reenterEmailAddressField);
        Actions.setImmediateValueAndTapOut(this.changeEmailScreen.reenterEmailAddressField, emailAddress);
    }

    confirmEmailAddress(emailAddress) {
        Actions.setImmediateValueAndTapOut(this.changeEmailScreen.reenterEmailAddressField, emailAddress);
        Actions.click(this.changeEmailScreen.confirmButton);
    }

    dismissValidationErrorMessage() {
        Actions.click(this.changeEmailScreen.dismissErrorMessage);
        Actions.hideDeviceKeyboard();
    }

    clickConfirmButton() {
        Actions.click(this.changeEmailScreen.confirmButton);
    }

    goBack() {
        Actions.click(this.commonScreen.backButton);
    }
}

module.exports = ChangeEmailPage;
