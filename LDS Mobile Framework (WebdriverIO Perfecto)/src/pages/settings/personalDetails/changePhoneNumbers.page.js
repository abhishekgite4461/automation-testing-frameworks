const AuthPage = require('../../common/auth.page');
const Actions = require('../../common/actions');
const SwipeAction = require('../../../enums/swipeAction.enum');

class ChangePhoneNumbersPage extends AuthPage {
    /**
     * @returns {ChangePhoneNumbersScreen}
     */
    get changePhoneNumbersScreen() {
        return this.getNativeScreen('settings/personalDetails/changePhoneNumbers.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.changePhoneNumbersScreen.title);
    }

    isPageVisible() {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.changePhoneNumbersScreen.title);
        Actions.isVisible(this.changePhoneNumbersScreen.title).should.equal(true);
    }

    arePhoneNumbersVisible() {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.changePhoneNumbersScreen.mobileNumberField);
        Actions.swipeUntilVisible(SwipeAction.UP, this.changePhoneNumbersScreen.homeNumberField);
        Actions.swipeUntilVisible(SwipeAction.UP, this.changePhoneNumbersScreen.workNumberField);
        Actions.swipeUntilVisible(SwipeAction.UP, this.changePhoneNumbersScreen.workExtensionField);
    }

    arePhoneNumbersMaskedWhenVisible() {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.changePhoneNumbersScreen.mobileNumberField);
        const mobileNumber = Actions.getText(this.changePhoneNumbersScreen.mobileNumberField);

        Actions.swipeUntilVisible(SwipeAction.UP, this.changePhoneNumbersScreen.homeNumberField);
        const homeNumber = Actions.getText(this.changePhoneNumbersScreen.homeNumberField);

        Actions.swipeUntilVisible(SwipeAction.UP, this.changePhoneNumbersScreen.workNumberField);
        const workNumber = Actions.getText(this.changePhoneNumbersScreen.workNumberField);

        const maskedNumberRegex = /\*+/;
        const phoneNumbers = {
            Mobile: mobileNumber,
            Home: homeNumber,
            Work: workNumber
        };

        for (const phoneType in phoneNumbers) {
            if (Object.prototype.hasOwnProperty.call(phoneNumbers, phoneType)) {
                const phoneNumber = phoneNumbers[phoneType];
                if (phoneNumber.length !== 0 && !maskedNumberRegex.test(phoneNumber)) {
                    throw new Error(`${phoneNumber} - ${phoneType} number should be masked!!`);
                }
            }
        }
    }

    isPayAContactMessageVisible() {
        Actions.isVisible(this.changePhoneNumbersScreen.paymInfoLabel).should.equal(true);
        Actions.isVisible(this.changePhoneNumbersScreen.mobilePaymLabel).should.equal(true);
    }

    isContinueButtonEnabled() {
        Actions.isEnabled(this.changePhoneNumbersScreen.continueButton).should.equal(true);
    }

    isContinueButtonDisabled() {
        Actions.isEnabled(this.changePhoneNumbersScreen.continueButton).should.equal(false);
    }

    isPopupErrorVisible() {
        Actions.isVisible(this.changePhoneNumbersScreen.mobileNumberError).should.equal(true);
    }

    changeMobileNumber(mobileNumber) {
        Actions.swipeUntilVisible(SwipeAction.DOWN, this.changePhoneNumbersScreen.mobileNumberField);
        Actions.setImmediateValueAndTapOut(this.changePhoneNumbersScreen.mobileNumberField, mobileNumber);
    }

    changeHomeNumber(homeNumber) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.changePhoneNumbersScreen.homeNumberField);
        Actions.setImmediateValueAndTapOut(this.changePhoneNumbersScreen.homeNumberField, homeNumber);
    }

    changeWorkNumber(workNumber) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.changePhoneNumbersScreen.workNumberField);
        Actions.setImmediateValueAndTapOut(this.changePhoneNumbersScreen.workNumberField, workNumber);
    }

    changeWorkExtension(workExtension) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.changePhoneNumbersScreen.workExtensionField);
        Actions.setImmediateValueAndTapOut(this.changePhoneNumbersScreen.workExtensionField, workExtension);
    }

    clearMobileNumber() {
        Actions.click(this.changePhoneNumbersScreen.mobileNumberField);
        Actions.click(this.changePhoneNumbersScreen.keyboardDone);
    }

    clearHomeNumber() {
        Actions.click(this.changePhoneNumbersScreen.homeNumberField);
        Actions.click(this.changePhoneNumbersScreen.keyboardDone);
    }

    clickContinueButton() {
        Actions.click(this.changePhoneNumbersScreen.continueButton);
    }

    goBack() {
        Actions.click(this.commonScreen.backButton);
    }

    changeAndSubmitPhoneNumbers(phoneNumber, workNumber) {
        Actions.swipeByPercentUntilVisible(this.changePhoneNumbersScreen.mobileNumberField);
        Actions.setImmediateValueAndTapOut(this.changePhoneNumbersScreen.mobileNumberField, phoneNumber);
        Actions.setImmediateValueAndTapOut(this.changePhoneNumbersScreen.homeNumberField, phoneNumber);
        Actions.setImmediateValueAndTapOut(this.changePhoneNumbersScreen.workNumberField, workNumber);
        this.clickContinueButton();
    }

    changeHomeNumberUAT(homeNumber) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.changePhoneNumbersScreen.homeNumberField);
        Actions.sendKeys(this.changePhoneNumbersScreen.homeNumberField, homeNumber);
    }

    shouldSeeHCCText() {
        Actions.isVisible(this.changePhoneNumbersScreen.textOnEditPhoneNumber).should.equal(true);
    }

    static scrollToEndOfPage() {
        for (let i = 0; i < 8; i++) {
            Actions.swipePage(SwipeAction.UP);
        }
    }

    shouldSeeHCCConfirmationText() {
        Actions.isVisible(this.changePhoneNumbersScreen.textOnConfirmPhoneNumber).should.equal(true);
    }
}

module.exports = ChangePhoneNumbersPage;
