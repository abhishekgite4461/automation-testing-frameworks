require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class SecuritySettingsPage extends AuthPage {
    /**
     * @returns {SecuritySettingsScreen}
     */
    get securitySettingsScreen() {
        return this.getNativeScreen('settings/securitySettings.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.securitySettingsScreen.title);
    }

    toggleFingerprintOptInStatus() {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.securitySettingsScreen.fingerprintToggle
        );
        Actions.click(this.securitySettingsScreen.fingerprintToggle);
    }

    shouldSeeFingerprintEnrolled() {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.securitySettingsScreen.fingerprintToggle
        );
        Actions.isChecked(this.securitySettingsScreen.fingerprintToggle).should.equal(true);
    }

    shouldSeeFingerprintNotEnrolled() {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.securitySettingsScreen.fingerprintToggle
        );
        Actions.isChecked(this.securitySettingsScreen.fingerprintToggle).should.equal(false);
    }

    validateOptionsInTheSecuritySettingsPage(securitySettingsOptions) {
        for (let i = 0; i < securitySettingsOptions.length; i++) {
            const option = this.securitySettingsScreen[securitySettingsOptions[i].securitySettingsOptions];
            Actions.isVisible(option).should.equal(true);
        }
    }

    validateOptionsInTheSecurityDetailsPage(allSecurityDetailOptions) {
        for (const field of allSecurityDetailOptions) {
            switch (field.securityDetailsOptions) {
            case 'User ID':
                Actions.isVisible(this.securitySettingsScreen.securityDetailsUserIDLabel).should.equal(true);
                break;
            case 'App version':
                Actions.isVisible(this.securitySettingsScreen.securityDetailsAppVersionLabel).should.equal(true);
                break;
            case 'Device name':
                Actions.isVisible(this.securitySettingsScreen.securityDetailsDeviceNameLabel).should.equal(true);
                break;
            case 'Device type':
                Actions.isVisible(this.securitySettingsScreen.securityDetailsDeviceTypeLabel).should.equal(true);
                break;
            case 'Forgotten your password':
                Actions.isVisible(this.securitySettingsScreen
                    .securityDetailsForgottenPasswordButton).should.equal(true);
                break;
            case 'Auto logoff':
                Actions.isVisible(this.securitySettingsScreen.securityDetailsAutoLogOffButton).should.equal(true);
                break;
            case 'Finger Print/Touch id':
                Actions.isVisible(this.securitySettingsScreen.securityFingerprintCell).should.equal(true);
                Actions.isVisible(this.securitySettingsScreen.securityFingerPrintToggleCell).should.equal(true);
                break;
            default:
                throw new Error('Unknown Options');
            }
        }
    }

    resetYourPassword() {
        Actions.click(this.securitySettingsScreen.resetPassword);
    }

    selectAutoLogOffOption() {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.securitySettingsScreen.securityDetailsAutoLogOffButton
        );
        Actions.click(this.securitySettingsScreen.securityDetailsAutoLogOffButton);
        Actions.reportTimer('Choose_AutoLogOff', 'automatically');
    }

    verifyAppDetailsInTheSecuritySettingsPage(userId) {
        const regex = /^[a-zA-Z0-9]+$/;
        let userIdWithStubOverride;
        if (browser.capabilities.server === 'STUB' && DeviceHelper.isAndroid()) {
            userIdWithStubOverride = 'Stub UserId';
        } else {
            userIdWithStubOverride = userId;
        }

        if (browser.capabilities.server === 'STUB') {
            this.validateOptionsInSecuritySettings(userIdWithStubOverride);
        } else {
            const deviceName = Actions.getText(this.securitySettingsScreen
                .securityDetailsDeviceNameValue);
            this.validateOptionsInSecuritySettings(userIdWithStubOverride);
            if (!(regex.test(deviceName))) {
                throw new Error(`${this.securitySettingsScreen
                    .securityDetailsDeviceNameValue} is not a valid security details device name!`);
            }
        }
    }

    validateOptionsInSecuritySettings(userId) {
        if (DeviceHelper.isAndroid()) {
            Actions.getText(this.securitySettingsScreen.securityDetailsDeviceTypeValue)
                .toLowerCase()
                .replace(/\s+/g, '').should.contains((browser.handsetInfo.manufacturer).toLowerCase());
            Actions.getText(this.securitySettingsScreen.securityDetailsUserIDValue).trim()
                .should.equal(userId);
        } else {
            Actions.getText(this.securitySettingsScreen.securityDetailsDeviceTypeValue)
                .toLowerCase()
                .replace(/\s+/g, '').should.contains(((browser.handsetInfo.model).toLowerCase()).replace(/\W+/g, '').split('e')[0]);
        }
    }

    clickForgottenPasswordOption() {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.securitySettingsScreen.securityDetailsForgottenPasswordButton
        );
        Actions.click(this.securitySettingsScreen.securityDetailsForgottenPasswordButton);
        Actions.reportTimer('Forgot_Your_Password', 'Reset your password');
    }

    static autoLogOffPauseTimer(autoLogOffTime) {
        const time = autoLogOffTime.split('Minute')[0].trim();
        const waitTime = (time * 60 * 1000) - 15000;
        Actions.pause(waitTime);
    }

    static sendAppToBackground() {
        Actions.appInBackground();
    }

    static bringAppFromBackground() {
        Actions.launchAppFromBackground();
    }
}

module.exports = SecuritySettingsPage;
