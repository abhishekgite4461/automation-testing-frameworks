require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class MultipleDeviceManagementNotificationsPage extends AuthPage {
    /**
     * @returns {MultipleDeviceManagementNotificationsScreen}
     */
    get multipleDeviceManagementNotificationsScreen() {
        return this.getNativeScreen('settings/MultipleDeviceManagementNotifications.screen');
    }

    /**
     * @returns {LogoutScreen}
     */
    get logoutScreen() {
        return this.getNativeScreen('logout.screen');
    }

    waitForNotificationsPageLoad() {
        Actions.waitForVisible(this.multipleDeviceManagementNotificationsScreen.header);
    }

    waitForAccountAlertsTileLoad() {
        Actions.waitForVisible(this.multipleDeviceManagementNotificationsScreen.accountNotificationsTile);
    }

    selectAllowNotificationsButton() {
        if (Actions.isVisible(this.multipleDeviceManagementNotificationsScreen.allowNotificationsButton)) {
            Actions.click(this.multipleDeviceManagementNotificationsScreen.allowNotificationsButton);
        }
    }

    checkToggleStatusOn() {
        Actions.waitForVisible(this.multipleDeviceManagementNotificationsScreen.accountNotificationsToggleSwitch);
        Actions.isChecked(this.multipleDeviceManagementNotificationsScreen.accountNotificationsToggleSwitch)
            .should.equal(false);
    }

    waitForGoToDeviceButton() {
        Actions.waitForVisible(this.multipleDeviceManagementNotificationsScreen.goToDeviceSettingsButton);
    }

    newSelectGoToDeviceSettings() {
        Actions.click(this.multipleDeviceManagementNotificationsScreen.goToDeviceSettingsButton);
    }
}
module.exports = MultipleDeviceManagementNotificationsPage;
