const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
require('chai').should();

class HowWeContactYouPage extends AuthPage {
    /**
     * @returns {HowWeContactYou}
     */
    get howWeContactYouScreen() {
        return this.getNativeScreen('settings/howWeContactYou.screen');
    }

    validateHowWeContactYouPageOptions(contactOptions) {
        for (let i = 0; i < contactOptions.length; i++) {
            const optionsDisplayed = this.howWeContactYouScreen[contactOptions[i].howWeContactYouOptions];
            Actions.isVisible(optionsDisplayed).should.equal(true);
        }
    }

    isPageVisible() {
        Actions.isVisible(this.howWeContactYouScreen.onlinePaperPreference).should.equal(true);
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.howWeContactYouScreen.onlinePaperPreference);
    }

    selectMarketingPreference() {
        Actions.click(this.howWeContactYouScreen.marketingPreference);
    }

    selectOnlinePaperPreferenceTile() {
        Actions.click(this.howWeContactYouScreen.onlinePaperPreference);
    }
}

module.exports = HowWeContactYouPage;
