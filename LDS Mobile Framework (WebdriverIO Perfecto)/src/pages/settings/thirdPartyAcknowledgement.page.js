const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class ThirdPartyAcknowledgementPage extends AuthPage {
    /**
     * @returns {ThirdPartyAcknowledgementScreen}
     */
    get thirdPartyAcknowledgementScreen() {
        return this.getNativeScreen('settings/thirdPartyAcknowledgement.screen');
    }

    isPageVisible() {
        Actions.waitForVisible(this.thirdPartyAcknowledgementScreen.title, 10000);
    }
}

module.exports = ThirdPartyAcknowledgementPage;
