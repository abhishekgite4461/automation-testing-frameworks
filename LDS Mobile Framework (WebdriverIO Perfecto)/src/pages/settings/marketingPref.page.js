const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class MarketingPrefPage extends AuthPage {
    /**
     * @returns {MarketingPrefScreen}
     */
    get marketingPrefScreen() {
        return this.getNativeScreen('settings/marketingPref.screen');
    }

    isPageVisible() {
        Actions.isVisible(this.marketingPrefScreen.title).should.equal(true);
    }

    isHalifaxMarketingPrefVisible() {
        Actions.isVisible(this.marketingPrefScreen.halifaxMarketing).should.equal(true);
    }

    isBosMarketingPrefVisible() {
        Actions.isVisible(this.marketingPrefScreen.bosMarketing).should.equal(true);
    }
}

module.exports = MarketingPrefPage;
