const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const SwipeAction = require('../../enums/swipeAction.enum');

class CookieUseAndPermissionPage extends AuthPage {
    /**
     * @returns {CookieUseAndPermissionScreen}
     */
    get cookieUseAndPermissionScreen() {
        return this.getNativeScreen('settings/cookieUseAndPermission.screen');
    }

    isPageVisible() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.cookieUseAndPermissionScreen.title);
    }

    selectCookiePolicyLink() {
        Actions.swipeUntilAnyVisible(SwipeAction.UP, this.cookieUseAndPermissionScreen.cookiePolicyLink);
        Actions.isVisible(this.cookieUseAndPermissionScreen.cookiePolicyLink).should.equal(true);
        Actions.click(this.cookieUseAndPermissionScreen.cookiePolicyLink);
    }
}

module.exports = CookieUseAndPermissionPage;
