require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class NotificationsSettingPage extends AuthPage {
    /**
     * @returns {NotificationsSettingScreen}
     */
    get notificationsSettingScreen() {
        return this.getNativeScreen('settings/notificationsSetting.screen');
    }

    /**
     * @returns {LogoutScreen}
     */
    get logoutScreen() {
        return this.getNativeScreen('logout.screen');
    }

    waitForNotificationsPageLoad() {
        Actions.waitForVisible(this.notificationsSettingScreen.header);
    }

    enableSmartAlertToggleStatusOn() {
        Actions.waitForVisible(this.notificationsSettingScreen.smartAlertsToggleSwitch);
        Actions.click(this.notificationsSettingScreen.smartAlertsToggleSwitch);
    }

    checkToggleStatusOn() {
        Actions.isChecked(this.notificationsSettingScreen.smartAlertsToggleSwitch)
            .should.equal(true);
    }
}
module.exports = NotificationsSettingPage;
