require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const DeviceHelper = require('../../utils/device.helper');

class DarkUrl extends AuthPage {
    /**
     * @returns {DarkUrlScreen}
     */
    get darkUrlScreen() {
        return this.getNativeScreen('settings/darkUrl.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.darkUrlScreen.title);
    }

    confirmOpenButtonText(label) {
        let darkUrlButtonText;
        if (DeviceHelper.isIOS()) {
            darkUrlButtonText = Actions.getText(this.darkUrlScreen.darkUrlButton(label));
        } else {
            darkUrlButtonText = Actions.getText(this.darkUrlScreen.darkUrlButton);
        }
        darkUrlButtonText.should.equal(label);
    }
}

module.exports = DarkUrl;
