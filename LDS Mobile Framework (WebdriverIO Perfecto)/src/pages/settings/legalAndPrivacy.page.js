const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class LegalAndPrivacyPage extends AuthPage {
    /**
     * @returns {LegalAndPrivacyScreen}
     */
    get legalAndPrivacyScreen() {
        return this.getNativeScreen('settings/legalAndPrivacy.screen');
    }

    isPageVisible() {
        Actions.waitForVisible(this.legalAndPrivacyScreen.title, 10000);
    }

    selectTermsAndConditionsLink() {
        Actions.isVisible(Actions.firstInstanceOf(this.legalAndPrivacyScreen.termsAndConditionsLink))
            .should.equal(true);
        Actions.click(this.legalAndPrivacyScreen.termsAndConditionsLink);
    }

    selectDismissButton() {
        Actions.waitForVisible(this.legalAndPrivacyScreen.dismissButton);
        Actions.click(this.legalAndPrivacyScreen.dismissButton);
    }

    selectBackButton() {
        Actions.click(this.legalAndPrivacyScreen.backButton);
    }

    navigateBackToLegalInfo() {
        Actions.click(this.commonScreen.modalBackButton);
    }
}

module.exports = LegalAndPrivacyPage;
