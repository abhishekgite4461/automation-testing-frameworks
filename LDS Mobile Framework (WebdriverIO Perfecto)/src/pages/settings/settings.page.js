const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const SwipeAction = require('../../enums/swipeAction.enum');

class SettingsPage extends AuthPage {
    get settingsScreen() {
        return this.getNativeScreen('settings/settings.screen');
    }

    get realTimeAlertsScreen() {
        return this.getNativeScreen('settings/realTimeAlerts.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.settingsScreen.title);
    }

    selectSecuritySettings() {
        Actions.swipeByPercentUntilVisible(this.settingsScreen.securitySettingsButton);
        Actions.click(this.settingsScreen.securitySettingsButton);
        Actions.reportTimer('Security_Settings', 'security details');
    }

    navigateToReset() {
        Actions.swipeByPercentUntilVisible(this.settingsScreen.resetAppButton);
        Actions.click(this.settingsScreen.resetAppButton);
    }

    selectPersonalDetails() {
        Actions.click(this.settingsScreen.personalDetailsButton);
    }

    verifyPersonalDetailsIsVisible() {
        Actions.swipeUntilVisible(
            SwipeAction.DOWN,
            this.settingsScreen.personalDetailsButton
        );
    }

    verifySecuritySettingsIsVisible() {
        Actions.swipeDownOnSmallScreen();
        Actions.isVisible(this.settingsScreen.securitySettingsButton).should.equal(true);
    }

    verifyPayAContactIsVisible() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.settingsScreen.payAContact);
    }

    verifyGoingAbroadIsVisible() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.settingsScreen.goingAbroad);
    }

    selectGoingAbroad() {
        Actions.click(this.settingsScreen.goingAbroad);
    }

    clickBackButton() {
        Actions.click(this.settingsScreen.backButton);
    }

    selectLegalInfo() {
        Actions.swipeByPercentage(80, 80, 30, 30, 50);
        Actions.click(this.settingsScreen.legalInfo);
    }

    selectHowWeContactYou() {
        Actions.click(this.settingsScreen.howWeContactYou);
    }

    validateOptionsInTheSettingsPage(allOptionsInSettingsPage) {
        for (let i = 0; i < allOptionsInSettingsPage.length; i++) {
            Actions.swipeUntilVisible(
                SwipeAction.UP,
                this.settingsScreen[allOptionsInSettingsPage[i].optionsInSettingsPage]
            );
        }
    }

    selectBusinessDetails() {
        Actions.click(this.settingsScreen.businessDetailsButton);
    }

    selectSpendingRewardsButton() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.settingsScreen.everydayOfferButton);
        Actions.click(this.settingsScreen.everydayOfferButton);
    }

    selectPayAContactSettings() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.settingsScreen.payAContactButton);
        Actions.click(this.settingsScreen.payAContactButton);
    }

    selectRealTimeAlertsTile() {
        Actions.swipeByPercentUntilVisible(this.settingsScreen.realTimeAlertsButton);
        Actions.click(this.settingsScreen.realTimeAlertsButton);
    }

    shouldSeeOpenBankingTile(isShown) {
        if (isShown) {
            Actions.swipeUntilVisible(SwipeAction.UP, this.settingsScreen.settingsOpenBankingTile);
            Actions.swipePage(SwipeAction.UP);
        } else {
            for (let i = 1; i <= 2; i++) {
                Actions.isVisible(this.settingsScreen.settingsOpenBankingTile).should.equal(isShown);
                Actions.swipePage(SwipeAction.UP);
            }
        }
        Actions.isVisible(this.settingsScreen.settingsOpenBankingTile).should.equal(isShown);
    }

    validateOpenBankingTilePostition() {
        this.shouldSeeOpenBankingTile(true);
        const settingsList = Actions.getAttributeText(this.settingsScreen.settingsList).toString().split(',');
        settingsList[settingsList.length - 1].toLowerCase().should.contains('open banking');
    }

    selectOpenBankingTile() {
        this.shouldSeeOpenBankingTile(true);
        Actions.click(this.settingsScreen.settingsOpenBankingTile);
    }

    shouldSeeOpenBankingErrorBanner() {
        Actions.isVisible(this.settingsScreen.settingsOpenBankingTileErrorBanner).should.equal(true);
    }

    waitForRealTimeAlertsPageLoad() {
        Actions.waitForVisible(this.realTimeAlertsScreen.title);
    }

    waitForNotificationsPageLoad() {
        Actions.waitForVisible(this.realTimeAlertsScreen.heading, 20000);
    }

    verifyNotificationsOptionIsNotVisible(type) {
        Actions.isVisible(this.settingsScreen.realTimeAlertsButton).should.equal(type);
    }
}

module.exports = SettingsPage;
