const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');

class LegalInfoPage extends AuthPage {
    /**
     * @returns {LegalInfoScreen}
     */
    get legalInfoScreen() {
        return this.getNativeScreen('settings/legalInfo.screen');
    }

    isPageVisible() {
        Actions.isVisible(this.legalInfoScreen.title).should.equal(true);
    }

    isLegalAndPrivacyLinkVisible() {
        Actions.isVisible(this.legalInfoScreen.legalAndPrivacy).should.equal(true);
    }

    isCookieUseAndPermissionLinkVisible() {
        Actions.isVisible(this.legalInfoScreen.cookieUseAndPermissions).should.equal(true);
    }

    isThirdPartyAcknowledgementsLinkVisible() {
        Actions.isVisible(this.legalInfoScreen.thirdPartyAcknowledgements).should.equal(true);
    }

    selectLegalAndPrivacyLink() {
        Actions.click(this.legalInfoScreen.legalAndPrivacy);
    }

    selectCookieUseAndPermissionsLink() {
        Actions.click(this.legalInfoScreen.cookieUseAndPermissions);
    }

    selectThirdPartyAcknowledgementsLink() {
        Actions.click(this.legalInfoScreen.thirdPartyAcknowledgements);
    }

    selectDismissButton() {
        Actions.click(this.legalInfoScreen.dismissButton);
    }

    viewFieldsOnLegalInfoPage(fieldsToValidate) {
        Actions.waitForVisible(this.legalInfoScreen.title);
        for (const field of fieldsToValidate) {
            Actions.isVisible(this.legalInfoScreen[field.fieldsOnLegalInfoScreen]).should.equal(true);
        }
    }
}

module.exports = LegalInfoPage;
