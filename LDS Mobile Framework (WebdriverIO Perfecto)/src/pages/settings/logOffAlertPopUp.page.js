const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const DeviceHelper = require('../../utils/device.helper');

class LogOffAlertPopUpPage extends AuthPage {
    /**
     * @returns {LogOffAlertPopUpScreen}
     */
    get logOffAlertPopUpScreen() {
        return this.getNativeScreen('settings/logOffAlertPopUp.screen');
    }

    /**
     * @returns {ChequeDepositScreen}
     */
    get chequeDepositScreen() {
        return this.getNativeScreen('ics/chequeDeposit.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.logOffAlertPopUpScreen.title);
    }

    static waitLogoutPageLoad() {
        Actions.pause(15000);
    }

    clickLogOffOption() {
        Actions.click(this.logOffAlertPopUpScreen.logOffButton);
    }

    clickContinueOption() {
        Actions.click(this.logOffAlertPopUpScreen.continueButton);
    }

    shouldSeeLogoffAlertPopup() {
        Actions.waitForVisible(this.logOffAlertPopUpScreen.title, 15000);
    }

    waitForPageLoadOnChequeScanningPage() {
        if (DeviceHelper.isIOS() && Actions.isExisting(this.logOffAlertPopUpScreen.continueButton)) {
            Actions.click(this.logOffAlertPopUpScreen.logOffButton);
        } else {
            Actions.click(this.chequeDepositScreen.closeCamerabutton);
        }
    }
}

module.exports = LogOffAlertPopUpPage;
