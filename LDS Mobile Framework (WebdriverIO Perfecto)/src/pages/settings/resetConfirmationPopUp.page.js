require('chai').should();
const Actions = require('../common/actions');
const AuthPage = require('../common/auth.page');
const DeviceState = require('../../utils/deviceState');
const DeviceHelper = require('../../utils/device.helper.js');

class ResetConfirmationPopUpPage extends AuthPage {
    /**
     * @returns {ResetConfirmationScreen}
     */
    get resetConfirmationScreen() {
        return this.getNativeScreen('settings/resetConfirmation.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.resetConfirmationScreen.title);
    }

    selectContinueButton() {
        Actions.click(this.resetConfirmationScreen.goButton);
        const deviceState = new DeviceState(DeviceHelper.deviceId());
        deviceState.setEnrolledUser(null);
        Actions.pause(10000); // wait for reset to complete
    }

    selectCancelButton() {
        Actions.click(this.resetConfirmationScreen.cancelButton);
    }

    verifyResetConfirmationPopup() {
        Actions.isVisible(this.resetConfirmationScreen.goButton).should.equal(true);
        Actions.isVisible(this.resetConfirmationScreen.cancelButton).should.equal(true);
    }
}

module.exports = ResetConfirmationPopUpPage;
