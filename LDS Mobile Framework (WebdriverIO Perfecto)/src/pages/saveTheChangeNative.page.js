require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class SaveTheChangeNativePage extends AuthPage {
    get saveTheChangeNativeScreen() {
        return this.getNativeScreen('saveTheChangeNative.screen');
    }

    verifySaveTheChangeSetUp(type) {
        if (type === 'have not') {
            Actions.isVisible(this.saveTheChangeNativeScreen.setUpSTCOption).should.equal(true);
        } else {
            Actions.isVisible(this.saveTheChangeNativeScreen.saveTheChangeTurnOffButton).should.equal(true);
        }
    }

    shouldSwipeThroughSTCInfoPages(number) {
        for (let i = 1; i <= number; i++) {
            Actions.swipeOnElement(SwipeAction.LEFT, this.saveTheChangeNativeScreen.infoPageNavigator);
        }
    }

    selectSetUpSTCOption() {
        Actions.click(this.saveTheChangeNativeScreen.setUpSTCOption);
    }

    selectTurnOFFSTC() {
        Actions.click(this.saveTheChangeNativeScreen.saveTheChangeTurnOffButton);
    }

    selectSavingAccount() {
        Actions.click(this.saveTheChangeNativeScreen.stcSavingAccount);
    }

    selectAcceptTCandTurnONSTC() {
        Actions.waitAndClick(this.saveTheChangeNativeScreen.acceptTC);
        Actions.click(this.saveTheChangeNativeScreen.turnONSTC);
    }

    shouldSeeBackToAccounts() {
        Actions.isVisible(this.saveTheChangeNativeScreen.backToAccounts).should.equal(true);
    }

    selectBackToAccounts() {
        Actions.click(this.saveTheChangeNativeScreen.backToAccounts);
    }

    shouldSeeSTCOFF() {
        Actions.isVisible(this.saveTheChangeNativeScreen.SaveTheChangeDeleteSuccessHeader).should.equal(true);
    }

    shouldSeeSTCSuccessScreen() {
        Actions.isVisible(this.saveTheChangeNativeScreen.stcSuccess).should.equal(true);
    }

    shouldSeeAlreadyRegistered() {
        Actions.isVisible(this.saveTheChangeNativeScreen.alreadyRegistered).should.equal(true);
    }

    shouldSeeOpenNewSaving() {
        Actions.isVisible(this.saveTheChangeNativeScreen.openNewSavingAccount).should.equal(true);
    }

    selectOpenNewSaving() {
        Actions.click(this.saveTheChangeNativeScreen.openNewSavingAccount);
    }
}

module.exports = SaveTheChangeNativePage;
