const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');
const Timeouts = (require('../enums/runConfig.enum')).TIMEOUTS;

class WelcomePage extends AuthPage {
    /**
     * @returns {WelcomeBenefitsScreen}
     */
    get welcomeBenefitsScreen() {
        return this.getNativeScreen('welcomeBenefits.screen');
    }

    /**
     * @returns {WelcomeRegisterScreen}
     */
    get welcomeRegisterScreen() {
        return this.getNativeScreen('welcomeRegister.screen');
    }

    get dataPrivacyScreen() {
        return this.getNativeScreen('dataPrivacy.screen');
    }

    /**
     * @returns {NewWelcomeBenefitsScreen}
     */
    get newWelcomeBenefitsScreen() {
        return this.getNativeScreen('ibRegistration/newWelcomeBenefits.screen');
    }

    waitForPageLoad() {
        if (DeviceHelper.isRNGA()) {
            Actions.waitForVisible(this.newWelcomeBenefitsScreen.title);
        } else {
            Actions.waitForVisible(this.welcomeBenefitsScreen.title, Timeouts.WAIT_FOR());
        }
    }

    moveToRegisterScreen() {
        if (DeviceHelper.isRNGA()) {
            Actions.swipeDownOnSmallScreen();
            Actions.waitForVisible(this.newWelcomeBenefitsScreen.logOnButton, 20000);
            Actions.click(this.newWelcomeBenefitsScreen.logOnButton);
        } else {
            if (DeviceHelper.isIOS()) {
                Actions.click(this.welcomeBenefitsScreen.nextButton);
            } else {
                Actions.swipeByPercentage(90, 70, 20, 70, 1000);
            }
            Actions.waitForVisible(this.welcomeRegisterScreen.title);
        }
    }

    navigateToLoginScreen() {
        this.waitForPageLoad();
        this.moveToRegisterScreen();
        if (DeviceHelper.isBNGA()) {
            Actions.click(this.welcomeRegisterScreen.getStartedButton);
        }
    }

    validateWelcomeScreenOptions(welcomeOptions) {
        for (let i = 0; i < welcomeOptions.length; i++) {
            const optionsDisplayed = this.welcomeRegisterScreen[welcomeOptions[i].welcomeScreenOptions];
            Actions.isVisible(optionsDisplayed).should.equal(true);
        }
    }

    validateWelcomeBenefitsScreenOptions(welcomeOptions) {
        for (let i = 0; i < welcomeOptions.length; i++) {
            const optionsDisplayed = this.welcomeBenefitsScreen[welcomeOptions[i].welcomeScreenOptions];
            Actions.isVisible(optionsDisplayed).should.equal(true);
        }
    }

    selectDataPrivacy() {
        Actions.click(this.welcomeBenefitsScreen.dataPrivacy);
    }

    selectRegisterForOnlineBanking() {
        Actions.click(this.welcomeRegisterScreen.onlineBankingRegistrationLink);
    }

    selectMoreInformation() {
        Actions.click(this.welcomeRegisterScreen.infoAboutCardReader);
    }

    selectWhyYouNeedCard() {
        Actions.click(this.welcomeRegisterScreen.whyYouNeedACard);
    }

    waitForOverlay() {
        Actions.waitForVisible(this.welcomeRegisterScreen.dialogTitle);
    }

    waitForDataPrivacy() {
        Actions.waitForVisible(this.dataPrivacyScreen.title);
    }

    selectDataPrivacyNotice() {
        Actions.click(this.dataPrivacyScreen.dataPrivacyNotice);
    }

    selectCookiesPolicy() {
        Actions.click(this.dataPrivacyScreen.cookiePolicy);
    }
}

module.exports = WelcomePage;
