const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');
const DeviceHelper = require('../utils/device.helper');
const Server = require('../enums/server.enum');
const {iOSDialPadPercentage} = require('../utils/dialer.helper');
const adbHelper = require('../utils/adb.helper');
const logger = require('../utils/logger');
const androidCallAppInfo = require('../apps/callApp.android');
const EnvironmentApiClient = require('../utils/environmentApiClient');

const waitForEIAPIN = 10000;
const waitAfterEIADuration = 10000;
const maxCallWaitDuration = 90000;

class EnrolmentPage extends UnAuthPage {
    get enrolmentCallScreen() {
        return this.getNativeScreen('enrolmentCall.screen');
    }

    get enrolmentCallWebScreen() {
        return this.getWebScreen('enrolmentCall.screen');
    }

    get enrolmentGracePeriodScreen() {
        return this.getNativeScreen('enrolmentGracePeriod.screen');
    }

    waitForEiaCallMeNowPageLoad() {
        Actions.waitForVisible(this.enrolmentCallScreen.title);
    }

    waitForEiaCallingPageLoad() {
        Actions.waitForVisible(this.enrolmentCallScreen.eiaCallingTitle);
    }

    waitForEiaCallNotRequiredPageLoad() {
        Actions.waitForVisible(this.enrolmentGracePeriodScreen.title);
    }

    clickCallMeNowOption() {
        Actions.waitForVisible(this.enrolmentCallScreen.eiaPhoneNumbers);
        Actions.swipeByPercentUntilVisible(this.enrolmentCallScreen.callMeNowButton);
        Actions.click(this.enrolmentCallScreen.callMeNowButton);
        Actions.reportTimer('Call_Me', 'Calling now');
    }

    selectPhoneNumber() {
        if (DeviceHelper.isEnv() && DeviceHelper.isPerfecto()) {
            const deviceMobileNumber = DeviceHelper.mobileNumber();
            const deviceLastTwoDigitMobileNumber = deviceMobileNumber.substring((deviceMobileNumber.length - 2),
                deviceMobileNumber.length);
            try {
                Actions.waitForVisible(this.enrolmentCallScreen.title);
                Actions.click(this.enrolmentCallScreen.selectPhoneNumber(deviceLastTwoDigitMobileNumber));
            } catch (e) {
                throw new Error('TEST_DATA_ERROR: Incorrect ph no for EIA');
            }
        }
    }

    validateMaskedPhoneNumbers() {
        const maskedNumberRegex = /\*+/;
        const getPhoneNumber = (loc) => {
            let phoneNumber;
            if (DeviceHelper.isIOS()) {
                phoneNumber = Actions.getAttribute(loc, 'value');
            } else {
                phoneNumber = Actions.getText(loc);
            }
            return phoneNumber;
        };
        if (Actions.isVisible(this.enrolmentCallScreen.yourPhone)) {
            const yourPhoneNumber = getPhoneNumber(this.enrolmentCallScreen.yourPhone);
            if (!maskedNumberRegex.test(yourPhoneNumber)) {
                throw new Error(`${yourPhoneNumber} number should be masked!!`);
            }
        } else {
            if (Actions.isVisible(this.enrolmentCallScreen.mobilePhone)) {
                const mobilePhoneNumber = getPhoneNumber(this.enrolmentCallScreen.mobilePhone);
                if (!maskedNumberRegex.test(mobilePhoneNumber)) {
                    throw new Error(`${mobilePhoneNumber} number should be masked!!`);
                }
            }
            if (Actions.isVisible(this.enrolmentCallScreen.homePhone)) {
                const homePhoneNumber = getPhoneNumber(this.enrolmentCallScreen.homePhone);
                if (!maskedNumberRegex.test(homePhoneNumber)) {
                    throw new Error(`${homePhoneNumber} number should be masked!!`);
                }
            }
            if (Actions.isVisible(this.enrolmentCallScreen.workPhone)) {
                const workPhoneNumber = getPhoneNumber(this.enrolmentCallScreen.workPhone);
                if (!maskedNumberRegex.test(workPhoneNumber)) {
                    throw new Error(`${workPhoneNumber} number should be masked!!`);
                }
            }
        }
    }

    selectNumber(number) {
        Actions.click(this.enrolmentCallScreen.numberRadioButton(number));
    }

    clickEIACancelOption() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.enrolmentCallScreen.cancelButton);
        Actions.click(this.enrolmentCallScreen.cancelButton);
    }

    clickExitCancelOption() {
        Actions.click(this.enrolmentCallScreen.exitCancel);
    }

    clickExitYesOption() {
        Actions.click(this.enrolmentCallScreen.exitYes);
    }

    static isEIARequired() {
        const server = Server.fromString(browser.capabilities.server);
        return ['BR', 'UAT', 'FOV', 'MFIT'].includes(server.testPhase);
    }

    updateEiaCallState(data) {
        Actions.waitForVisible(this.enrolmentCallScreen.eiaPin1, waitForEIAPIN);
        EnvironmentApiClient.updateEIA(
            browser.capabilities.server,
            browser.capabilities.brand,
            browser.capabilities.appType,
            data.username
        );
    }

    attendEiaCallAndEnterFourDigitNumber() {
        if (!EnrolmentPage.isEIARequired()) return;
        Actions.waitForVisible(this.enrolmentCallScreen.eiaPin1, waitForEIAPIN);
        let pinNumber;
        if (DeviceHelper.isIOS()) {
            pinNumber = Actions.getPageSource().match(/(\d),(\d),(\d),(\d)/);
            EnrolmentPage.acceptIOScallAndOpenKeyPad();
            for (let i = 1; i < pinNumber.length; i++) {
                Actions.tapByLocation(iOSDialPadPercentage[pinNumber[i]]);
            }
        } else {
            // Capturing the 4 digit pin code
            const pin1 = Actions.getAttribute(this.enrolmentCallScreen.eiaPin1, 'text');
            const pin2 = Actions.getAttribute(this.enrolmentCallScreen.eiaPin2, 'text');
            const pin3 = Actions.getAttribute(this.enrolmentCallScreen.eiaPin3, 'text');
            const pin4 = Actions.getAttribute(this.enrolmentCallScreen.eiaPin4, 'text');
            EnrolmentPage.acceptAndroidCallAndOpenKeyPad();
            // Entering the four digit pin in the dialer pad
            pinNumber = [pin1, pin2, pin3, pin4];
            // Entering the four digit pin in the dialer pad
            for (let i = 0; i < pinNumber.length; i++) {
                Actions.click(this.enrolmentCallScreen.dialNumber(pinNumber[i]));
            }
        }
        Actions.pause(waitAfterEIADuration);
        Actions.launchAppFromBackground();
    }

    static enrolmentDuration() {
        if (browser.capabilities.brand === 'MBNA') {
            return 15000;
        }
        return 22000;
    }

    static acceptIOScallAndOpenKeyPad(eiaMessageDuration = this.enrolmentDuration()) {
        try {
            Actions.waitUntil(() => Actions.perfectoFindText('Accept'), maxCallWaitDuration,
                'Elements not visible after 90000 seconds');
            Actions.tapByLocation(iOSDialPadPercentage.Accept);
            Actions.pause(eiaMessageDuration);
            Actions.tapByLocation(iOSDialPadPercentage.Keypad);
            Actions.pause(1000);
        } catch (err) {
            logger.error('Error while Accepting iOS call, DeviceID:'
                + `${DeviceHelper.deviceId()}. Error Message: ${err.message}`);
            throw err;
        }
    }

    static acceptAndroidCallAndOpenKeyPad(eiaMessageDuration = this.enrolmentDuration()) {
        try {
            browser.waitUntil(() => adbHelper.getCallState(), maxCallWaitDuration,
                'Elements not visible after 90000 seconds');
            Actions.pause(2000);
            adbHelper.inputKeyEvent('KEYCODE_CALL');
            Actions.pause(2000);
            adbHelper.inputKeyEvent('KEYCODE_CALL');
            const test1 = new Date();
            Actions.pause(3000);
            Actions.tapByLocation('50%,1%');
            this.checkAndroidInCallAppIsInFocus();
            if (Actions.isVisible('//*[@text=\'Dial pad\' or @text=\'Keypad\' or @content-desc=\'Show Dialpad\']')) {
                Actions.click('//*[@text=\'Dial pad\' or @text=\'Keypad\' or @content-desc=\'Show Dialpad\']');
            }
            const test2 = new Date();
            Actions.pause(eiaMessageDuration - (test2 - test1));
        } catch (err) {
            logger.error('Error while Accepting Android call, DeviceID:'
                + `${DeviceHelper.deviceId()}. Error Message: ${err.message}`);
            throw err;
        }
    }

    static checkAndroidInCallAppIsInFocus() {
        const focusedApp = browser.getCurrentActivity();
        if (!androidCallAppInfo.appActivity.some((activity) => activity.indexOf(focusedApp) > -1)) {
            adbHelper.openNotificationTray();
            browser.click('//*[contains(@text, \'Ongoing call\') or contains(@text, \'On-going call\')]');
        }
    }

    webViewAttendEiaCallAndEnterFourDigitNumber(WebViewEIAMessageDuration) {
        if (!EnrolmentPage.isEIARequired()) return;
        Actions.waitForVisible(this.enrolmentCallWebScreen.eiaPin, waitForEIAPIN);
        const pinNumber = Actions.getText(this.enrolmentCallWebScreen.eiaPin);
        if (DeviceHelper.isIOS()) {
            EnrolmentPage.acceptIOScallAndOpenKeyPad(WebViewEIAMessageDuration);
        } else {
            EnrolmentPage.acceptAndroidCallAndOpenKeyPad(WebViewEIAMessageDuration);
        }
        for (let i = 0; i < pinNumber.length; i++) {
            Actions.tapByLocation(iOSDialPadPercentage[pinNumber[i]]);
        }
        // Delay to end up the call
        Actions.pause(waitAfterEIADuration);
    }

    selectEnrolmentBypassContinueButton() {
        Actions.click(this.enrolmentGracePeriodScreen.continueButton);
    }

    checkGreyedOutEnability(enability) {
        if (DeviceHelper.isIOS()) {
            Actions.isEnabled(this.enrolmentCallScreen.selectRadioButton('MOBILE')).should.equal(enability);
        } else {
            Actions.isEnabled(this.enrolmentCallScreen.mobilePhoneSection).should.equal(enability);
        }
    }
}

module.exports = EnrolmentPage;
