require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class PaymentTypePage extends AuthPage {
    /**
     * @returns {PaymentTypeScreen}
     */
    get paymentTypeScreen() {
        return this.getNativeScreen('paymentType.screen');
    }

    selectPayContact() {
        Actions.click(this.paymentTypeScreen.payContact);
    }

    verifyPayContactErrorMessage() {
        Actions.isVisible(this.paymentTypeScreen.payContactError).should.equal(true);
    }
}

module.exports = PaymentTypePage;
