const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class ModifySpendingRewardsWebView extends AuthPage {
    /**
     * @returns {ModifySpendingRewardsWebViewScreen}
     */
    get modifySpendingRewardsWebViewScreen() {
        return this.getNativeScreen('modifySpendingRewardsWebViewScreen.screen');
    }

    /**
     * @returns {SpendingRewardsScreen}
     */
    waitForPageLoad() {
        Actions.waitForVisible(this.modifySpendingRewardsWebViewScreen.title);
    }
}

module.exports = ModifySpendingRewardsWebView;
