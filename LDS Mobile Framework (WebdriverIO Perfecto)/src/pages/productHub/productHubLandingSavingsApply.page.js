require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class ProductHubLandingSavingsApplyPage extends AuthPage {
    /**
     * @returns {productHubApplySavingsScreen}
     */
    get productHubApplySavingsScreen() {
        return this.getNativeScreen('productHub/productHubApplySavings.screen');
    }

    selectApplyNow() {
        let i = 0;
        while (i < 25) {
            Actions.swipePage(SwipeAction.UP, 800);
            Actions.pause(1000);
            i++;
        }
        Actions.click(this.productHubApplySavingsScreen.applyNow);
    }

    selectTermsandConditions() {
        let i = 0;
        while (i < 14) {
            Actions.swipePage(SwipeAction.UP);
            Actions.pause(1000);
            i++;
        }
        Actions.click(this.productHubApplySavingsScreen.acceptTC);
    }

    selectOpenAccount() {
        Actions.click(this.productHubApplySavingsScreen.openAccount);
    }

    verifyAccountAndSortCode() {
        if (DeviceHelper.isIOS()) {
            if (!Actions.isVisible(this.productHubApplySavingsScreen.sortCode)) {
                Actions.swipePage(SwipeAction.UP);
                Actions.swipePage(SwipeAction.UP);
            }
            Actions.isVisible(this.productHubApplySavingsScreen.acctAccountNumber);
        } else {
            if (!Actions.isVisible(this.productHubApplySavingsScreen.acctSortCodeandNumber)) {
                Actions.swipePage(SwipeAction.UP);
                Actions.swipePage(SwipeAction.UP);
                Actions.swipePage(SwipeAction.UP);
            }
            Actions.isVisible(this.productHubApplySavingsScreen.acctSortCodeandNumber);
        }
    }

    verifyAccountOpening(accountName) {
        Actions.isVisible(this.productHubApplySavingsScreen.accountOpeningSuccessMessage(accountName))
            .should.equal(true);
    }

    getSortCodeandNumberNo() {
        return Actions.getText(this.productHubApplySavingsScreen.acctSortCodeandNumber);
    }

    getAccountNumber() {
        return Actions.getText(this.productHubApplySavingsScreen.acctAccountNumber);
    }

    verifyAccountDetails(accountNumber) {
        Actions.swipeByPercentUntilVisible(this.productHubApplySavingsScreen.accountNum(accountNumber), 80, 80, 20, 20,
            100, 15);
    }

    selectAcceptAll() {
        Actions.waitUntilWithoutAssertion(this.productHubApplySavingsScreen.acceptAll);
        if (Actions.isVisible(this.productHubApplySavingsScreen.acceptAll)) {
            Actions.click(this.productHubApplySavingsScreen.acceptAll);
        }
    }
}

module.exports = ProductHubLandingSavingsApplyPage;
