require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class ProductHubLandingPage extends AuthPage {
    /**
     * @returns {ProductHubLandingScreen}
     */
    get productHubLandingScreen() {
        return this.getNativeScreen('productHub/productHubLanding.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.productHubLandingScreen.title);
    }

    shouldBeOnOtherMortgage() {
        Actions.waitForVisible(this.productHubLandingScreen.otherMortgage);
        Actions.isVisible(this.productHubLandingScreen.otherMortgage)
            .should.equal(true);
    }

    enterLoanTermField(numberOfMonth) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubLandingScreen.loanTermField);
        Actions.setImmediateValue(this.productHubLandingScreen.loanTermField, numberOfMonth);
        Actions.isVisible(this.productHubLandingScreen.numberPad).should.equal(true);
    }

    goBack() {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.commonScreen.backButton);
        } else {
            Actions.click(this.commonScreen.homeButton);
        }
    }
}
module.exports = ProductHubLandingPage;
