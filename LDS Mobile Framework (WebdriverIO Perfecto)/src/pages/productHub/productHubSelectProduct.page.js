require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class ProductHubSelectProductPage extends AuthPage {
    /**
     * @returns {ProductHubLandingSavingsScreen}
     */
    get productHubLandingSavingsScreen() {
        return this.getNativeScreen('productHub/productHubLandingSavings.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.productHubLandingSavingsScreen.title);
    }

    selectSavingsAccount(accountName) {
        Actions.click(this.productHubLandingSavingsScreen.savingsAccountName(accountName));
    }

    selectOtherSavingsAccount() {
        Actions.swipeByPercentUntilVisible(this.productHubLandingSavingsScreen.OthersavingsAccountName,
            20, 80, 20, 60, 100);
        Actions.click(this.productHubLandingSavingsScreen.OthersavingsAccountName);
        Actions.swipeByPercentUntilVisible(this.productHubLandingSavingsScreen.Otherselect, 20, 80, 20, 60, 100);
        Actions.click(this.productHubLandingSavingsScreen.Otherselect);
    }

    startApplyingAccount() {
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.productHubLandingSavingsScreen.select);
    }
}

module.exports = ProductHubSelectProductPage;
