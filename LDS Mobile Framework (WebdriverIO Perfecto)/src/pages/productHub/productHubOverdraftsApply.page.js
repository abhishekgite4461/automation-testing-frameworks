require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const deviceHelper = require('../../utils/device.helper');
const SwipeAction = require('../../enums/swipeAction.enum');
const HelpInfo = require('../../enums/RNGA/helpAndInfoNumbers.enum');

class ProductHubOverdraftsApplyPage extends AuthPage {
    /**
     * @returns {productHubApplyOverdraftScreen}
     */
    get productHubApplyOverdraftScreen() {
        return this.getNativeScreen('productHub/productHubApplyOverdraft.screen');
    }

    verifyOverdraftsPageTitle() {
        Actions.waitForVisible(this.productHubApplyOverdraftScreen.overdraftPageTitle, 50000);
    }

    selectApplyNowButton() {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.productHubApplyOverdraftScreen.applyButton);
    }

    verifyOverdraftsPageDetails() {
        Actions.waitForVisible(this.productHubApplyOverdraftScreen.aboutYouLabel, 20000);
    }

    enterOverdraftAmount(odAmount) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.overdraftAmountFieldNew);
        Actions.clearText(this.productHubApplyOverdraftScreen.overdraftAmountFieldNew);
        Actions.setImmediateValue(this.productHubApplyOverdraftScreen.overdraftAmountFieldNew, odAmount);
    }

    selectEmploymentStatus(status) {
        if (!deviceHelper.isIOS()) {
            Actions.click(this.productHubApplyOverdraftScreen.employementStatusSelector);
            Actions.waitAndClick(this.productHubApplyOverdraftScreen.selectEmployment);
        } else {
            Actions.click(this.productHubApplyOverdraftScreen.employementStatusSelector);
            Actions.setImmediateValueAndTapOut(this.productHubApplyOverdraftScreen.employmentStatus, status);
        }
    }

    enterMonthlyIncomeAfterTax(aftertax) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.monthlyIncomeAfterTax);
        Actions.click(this.productHubApplyOverdraftScreen.monthlyIncomeAfterTax);
        Actions.setValue(this.productHubApplyOverdraftScreen.monthlyIncomeAfterTax, aftertax);
        Actions.tapOutOfField();
    }

    enterMonthlyOutgoings(outgoing) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.monthlyOutgoings);
        Actions.click(this.productHubApplyOverdraftScreen.monthlyOutgoings);
        Actions.setValue(this.productHubApplyOverdraftScreen.monthlyOutgoings, outgoing);
        Actions.tapOutOfField();
    }

    enterChildCareCost(childcare) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.childCareCost);
        Actions.click(this.productHubApplyOverdraftScreen.childCareCost);
        Actions.setValue(this.productHubApplyOverdraftScreen.childCareCost, childcare);
        Actions.tapOutOfField();
    }

    selectLifeChangingEvent() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.lifeChangingEventField);
        Actions.click(this.productHubApplyOverdraftScreen.lifeChangingEventField);
    }

    clickSubmitButton() {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.continueButton);
        Actions.click(this.productHubApplyOverdraftScreen.continueButton);
    }

    verifyOverdraftDeclinePage() {
        Actions.waitForVisible(this.productHubApplyOverdraftScreen.overdraftDeclinePageTitle, 20000);
    }

    verifyHelpAndInfoButton() {
        Actions.swipeByPercentUntilVisible(this.productHubApplyOverdraftScreen.helpButton, 50, 80, 50, 20);
        Actions.isVisible(this.productHubApplyOverdraftScreen.helpButton).should.equal(true);
    }

    selectHelpandInfoButton() {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.waitAndClick(this.productHubApplyOverdraftScreen.helpButton);
        Actions.swipePage(SwipeAction.UP);
    }

    validateHelpAndSupportSection(actionMenuOptions) {
        let individualActionMenuOption = '';
        for (let i = 0; i < actionMenuOptions.length; i++) {
            individualActionMenuOption = ['outsideUKNumber', 'ukNumber'].includes(actionMenuOptions[i].optionsinHelpandSupport)
                ? this.productHubApplyOverdraftScreen[actionMenuOptions[i]
                    .optionsinHelpandSupport](HelpInfo.fromString(deviceHelper.getBrand())[
                    actionMenuOptions[i].optionsinHelpandSupport])
                : this.productHubApplyOverdraftScreen[actionMenuOptions[i].optionsinHelpandSupport];
            Actions.waitForVisible(individualActionMenuOption);
        }
    }

    selectFindYourNearestBranch() {
        Actions.click(this.productHubApplyOverdraftScreen.findYourNearestBranchLink);
    }

    selectLeaveApp() {
        Actions.waitForVisible(this.productHubApplyOverdraftScreen.leaveAppTitle);
        Actions.click(this.productHubApplyOverdraftScreen.leaveAppOkButton);
    }

    selectPayBackNoButton() {
        Actions.waitForVisible(this.productHubApplyOverdraftScreen.overdraftPayBackButton);
        Actions.click(this.productHubApplyOverdraftScreen.overdraftPayBackButton);
    }

    selectLetMeJustTypeLink() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.letMeJustTypeLink);
        Actions.click(this.productHubApplyOverdraftScreen.letMeJustTypeLink);
    }

    enterOverdraftDays(odDays) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.overdraftDays);
        Actions.click(this.productHubApplyOverdraftScreen.overdraftDays);
        Actions.clearText(this.productHubApplyOverdraftScreen.overdraftDays);
        Actions.setImmediateValueAndTapOut(this.productHubApplyOverdraftScreen.overdraftDays, odDays);
    }

    verifyOverdraftsPartnerPageDetails() {
        Actions.waitForVisible(this.productHubApplyOverdraftScreen.aboutDigitalLabel, 20000);
    }

    enterMonthlyIncomeAfterTaxJoint(aftertax) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.monthlyIncomeAfterTaxJoint);
        Actions.click(this.productHubApplyOverdraftScreen.monthlyIncomeAfterTaxJoint);
        Actions.setImmediateValueAndTapOut(this.productHubApplyOverdraftScreen.monthlyIncomeAfterTaxJoint, aftertax);
    }

    enterMonthlyOutgoingsJoint(outgoing) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.monthlyOutgoingsJoint);
        Actions.click(this.productHubApplyOverdraftScreen.monthlyOutgoingsJoint);
        Actions.setImmediateValueAndTapOut(this.productHubApplyOverdraftScreen.monthlyOutgoingsJoint, outgoing);
    }

    enterChildCareCostJoint(childcare) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyOverdraftScreen.childCareCostJoint);
        Actions.click(this.productHubApplyOverdraftScreen.childCareCostJoint);
        Actions.setImmediateValueAndTapOut(this.productHubApplyOverdraftScreen.childCareCostJoint, childcare);
    }
}

module.exports = ProductHubOverdraftsApplyPage;
