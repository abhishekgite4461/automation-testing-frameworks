require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ProductHubLandingCurrentPage extends AuthPage {
    /**
     * @returns {ProductHubLandingCurrentScreen}
     */
    get productHubLandingCurrentScreen() {
        return this.getNativeScreen('productHub/productHubLandingCurrent.screen');
    }

    waitForPageLoad() {
        Actions.waitUntilVisible([
            this.productHubLandingCurrentScreen.currentAccountOverdraft,
            this.productHubLandingCurrentScreen.currentAccountOverdraftText
        ]);
    }

    shouldBeOnCurrentAccountOverdraft() {
        Actions.waitUntilVisible([
            this.productHubLandingCurrentScreen.currentAccountOverdraft,
            this.productHubLandingCurrentScreen.currentAccountOverdraftText
        ]);
    }
}

module.exports = ProductHubLandingCurrentPage;
