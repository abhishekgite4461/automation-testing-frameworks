const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ProductHubPage extends AuthPage {
    /**
     * @returns {ProductHubScreen}
     */
    get productHubScreen() {
        return this.getNativeScreen('productHub/productHub.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.productHubScreen.title);
    }

    selectLoanCalculator() {
        Actions.swipeByPercentUntilVisible(this.productHubScreen.loan);
        Actions.click(this.productHubScreen.loan);
        Actions.click(this.productHubScreen.calculator);
    }

    selectCurrentAccountsOverdraft() {
        Actions.click(this.productHubScreen.currentAccounts);
        Actions.click(this.productHubScreen.currentAccountOverdraft);
    }

    selectCompareCreditCard() {
        Actions.click(this.productHubScreen.creditCards);
        Actions.click(this.productHubScreen.compareCreditCards);
    }

    selectCompareSavingsAccounts() {
        Actions.click(this.productHubScreen.savings);
        Actions.click(this.productHubScreen.compareSavingsAccounts);
    }

    selectHomeInsurance() {
        Actions.click(this.productHubScreen.insurance);
        Actions.click(this.productHubScreen.homeInsurance);
    }

    selectOtherMortgageOptions() {
        Actions.click(this.productHubScreen.mortgages);
        Actions.isVisible(this.productHubScreen.otherMortgageOptions);
    }

    expandProduct(productName) {
        Actions.swipeByPercentUntilVisible(this.productHubScreen.expandIcon(productName));
        Actions.click(this.productHubScreen.expandIcon(productName));
    }

    verifyProductisNotVisible(productName) {
        Actions.waitForNotVisible(this.productHubScreen[productName]);
    }

    verifyProductisVisible(productName) {
        Actions.isVisible(this.productHubScreen[productName]).should.equal(true);
    }

    selectFeatured() {
        Actions.click(this.productHubScreen.featured);
    }

    applyForChargeCard() {
        Actions.click(this.productHubScreen.cards);
        Actions.click(this.productHubScreen.applyForChargeCard);
    }

    verifyDiscoverAndApplyTitleIsNotVisible() {
        Actions.isVisible(this.productHubScreen.discoverAndApplyTitle).should.equal(false);
    }
}

module.exports = ProductHubPage;
