const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const ProductHubProducts = require('../../enums/productHubProducts.enum');
const DeviceHelper = require('../../utils/device.helper');

class ProductHubPage extends AuthPage {
    static getProductByString(type) {
        return ProductHubProducts.fromString(type.trim().replace(/ /g, '_').toUpperCase());
    }

    get productHubHomeScreen() {
        return this.getNativeScreen('productHub/productHub.screen');
    }

    selectProductCategory(category) {
        const productCategory = this.getProductCategoryLocator(category);
        if (DeviceHelper.isAndroid()) {
            if (Actions.getAttribute(productCategory, 'text') === 'Featured') {
                Actions.swipeByPercentUntilVisible(productCategory);
            } else {
                Actions.swipeByPercentUntilVisible(productCategory);
                Actions.click(productCategory);
            }
        } else if (DeviceHelper.isIOS()) {
            if (Actions.getAttribute(productCategory, 'name') === 'Featured') {
                Actions.swipeByPercentUntilVisible(productCategory);
            } else {
                Actions.swipeByPercentUntilVisible(productCategory);
                Actions.click(productCategory);
            }
        }
    }

    shouldShowLabel(product) {
        Actions.swipeByPercentUntilVisible(this.productHubHomeScreen[product]);
    }

    getProductCategoryLocator(category) {
        switch (category) {
        case ProductHubProducts.CURRENT:
            return this.productHubHomeScreen.currentAccounts;
        case ProductHubProducts.LOAN:
            return this.productHubHomeScreen.loans;
        case ProductHubProducts.CREDIT_CARD:
            return this.productHubHomeScreen.creditCards;
        case ProductHubProducts.SAVING:
            return this.productHubHomeScreen.savings;
        case ProductHubProducts.INSURANCE:
            return this.productHubHomeScreen.insurance;
        case ProductHubProducts.MORTGAGE:
            return this.productHubHomeScreen.mortgages;
        case ProductHubProducts.FEATURED:
            return this.productHubHomeScreen.featured;
        case ProductHubProducts.SHARE_DEALING:
            return this.productHubHomeScreen.shareDealing;
        default:
            throw new Error(`Unknown category locator: ${category}`);
        }
    }

    featureExpanded() {
        Actions.isVisible(this.productHubHomeScreen.featuredExpanded).should.equal(true);
    }

    businessCurrentAccountCollapsed() {
        Actions.isVisible(this.productHubHomeScreen.businessCurrentAccountCollapsed).should.equal(true);
    }

    selectOverdraftsOptions() {
        Actions.swipeByPercentUntilVisible(this.productHubHomeScreen.overdrafts);
        Actions.click(this.productHubHomeScreen.overdrafts);
        Actions.click(this.productHubHomeScreen.overdraftsOptions);
    }

    selectBusinessLoanOption() {
        Actions.click(this.productHubHomeScreen.businessLoan);
    }

    selectShareDealing() {
        Actions.click(this.productHubHomeScreen.shareDealing);
    }
}

module.exports = ProductHubPage;
