require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ProductHubLandingCreditCardPage extends AuthPage {
    /**
     * @returns {productHubLandingCreditCardsScreen}
     */
    get productHubLandingCreditCardsScreen() {
        return this.getNativeScreen('productHub/productHubLandingCreditCards.screen');
    }

    waitForPageLoad() {
        Actions.waitUntilVisible([
            this.productHubLandingCreditCardsScreen.compareCreditCard,
            this.productHubLandingCreditCardsScreen.compareCreditCardText
        ]);
    }

    shouldBeOnCompareCreditCard() {
        Actions.waitUntilVisible([
            this.productHubLandingCreditCardsScreen.compareCreditCard,
            this.productHubLandingCreditCardsScreen.compareCreditCardText
        ]);
    }
}

module.exports = ProductHubLandingCreditCardPage;
