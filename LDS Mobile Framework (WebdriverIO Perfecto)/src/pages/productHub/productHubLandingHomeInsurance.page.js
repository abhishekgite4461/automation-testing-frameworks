require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ProductHubLandingHomeInsurancePage extends AuthPage {
    /**
     * @returns {ProductHubLandingHomeInsuranceScreen}
     */
    get productHubLandingHomeInsuranceScreen() {
        return this.getNativeScreen('productHub/productHubLandingHomeInsurance.screen');
    }

    waitForPageLoad() {
        Actions.waitUntilVisible([
            this.productHubLandingHomeInsuranceScreen.homeInsurance,
            this.productHubLandingHomeInsuranceScreen.homeInsuranceText
        ]);
    }

    shouldBeOnHomeInsurance() {
        Actions.waitUntilVisible([
            this.productHubLandingHomeInsuranceScreen.homeInsurance,
            this.productHubLandingHomeInsuranceScreen.homeInsuranceText
        ]);
    }
}

module.exports = ProductHubLandingHomeInsurancePage;
