require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');
const DeviceHelper = require('../../utils/device.helper');

class ProductHubLoanApplyPage extends AuthPage {
    /**
     * @returns {ProductHubApplyLoanScreen}
     */
    get productHubApplyLoanScreen() {
        return this.getWebScreen('productHub/productHubLoanApply.screen');
    }

    waitForPageLoad() {
        const locator = Actions.waitUntilVisible([
            this.productHubApplyLoanScreen.loanTitle,
            this.productHubApplyLoanScreen.helloTitle
        ], 30000);

        if (locator === this.productHubApplyLoanScreen.helloTitle) {
            Actions.swipePage(SwipeAction.UP);
            Actions.swipePage(SwipeAction.UP);
            Actions.click(this.productHubApplyLoanScreen.continueLoanButton);
        }
    }

    enterLoanAmountField(amount) {
        Actions.click(this.productHubApplyLoanScreen.loanAmountField);
        Actions.setValue(this.productHubApplyLoanScreen.loanAmountField, amount);
        Actions.tapOutOfField();
    }

    enterLoanTermField(term) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyLoanScreen.termField);
        Actions.click(this.productHubApplyLoanScreen.termField);
        Actions.setValue(this.productHubApplyLoanScreen.termField, term);
        Actions.tapOutOfField();
    }

    selectLoanPurpose() {
        if (!DeviceHelper.isIOS()) {
            Actions.click(this.productHubApplyLoanScreen.selectLoanPurposeDropDown);
        }
    }

    selectLoanPurposeOption(LoanPurpose) {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.productHubApplyLoanScreen.selectLoanPurposeDropDown);
            Actions.setImmediateValue(this.productHubApplyLoanScreen.selectLoanPurposeOption,
                LoanPurpose);
        } else {
            Actions.click(this.productHubApplyLoanScreen.selectLoanPurposeOption);
        }
    }

    selectFlexibleLoan() {
        Actions.click(this.productHubApplyLoanScreen.flexibleLoanButton);
    }

    verifyRepaymentOptionPage() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.repaymentOptionTitle);
    }

    selectClosestMatchOption() {
        Actions.click(this.productHubApplyLoanScreen.closestMatchOption);
    }

    verifyYourApplicationPage() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.yourApplicationTitle, 40000);
    }

    selectEmploymentStatus() {
        if (!DeviceHelper.isIOS()) {
            Actions.click(this.productHubApplyLoanScreen.selectEmploymentStatus);
        } else {
            Actions.click(this.productHubApplyLoanScreen.selectEmploymentStatus);
        }
    }

    selectEmploymentLoanPurposeDropDownOption() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.productHubApplyLoanScreen.selectEmploymentStatus);
            Actions.setImmediateValueAndTapOut(this.productHubApplyLoanScreen.selectLoanPurposeOption,
                this.productHubApplyLoanScreen.selectEmploymentOption);
        } else {
            Actions.click(this.productHubApplyLoanScreen.selectEmploymentOption);
        }
        Actions.pause(100);
    }

    enterMonthlyIncome(income) {
        Actions.click(this.productHubApplyLoanScreen.monthlyIncomeField);
        Actions.setValue(this.productHubApplyLoanScreen.monthlyIncomeField, income);
        Actions.tapOutOfField();
    }

    enterMonthlyOutgoings(outgoing) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyLoanScreen.monthlyOutgoingField);
        Actions.click(this.productHubApplyLoanScreen.monthlyOutgoingField);
        Actions.setValue(this.productHubApplyLoanScreen.monthlyOutgoingField, outgoing);
        Actions.tapOutOfField();
    }

    enterDependents(dependents) {
        Actions.click(this.productHubApplyLoanScreen.dependentsField);
        Actions.setValue(this.productHubApplyLoanScreen.dependentsField, dependents);
        Actions.tapOutOfField();
    }

    enterChildCareCosts(childCareCost) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyLoanScreen.childCareCostsField);
        Actions.click(this.productHubApplyLoanScreen.childCareCostsField);
        Actions.setValue(this.productHubApplyLoanScreen.childCareCostsField, childCareCost);
        Actions.tapOutOfField();
    }

    selectAgreementCheckboxOne() {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        for (let i = 0; i < 10; i++) {
            Actions.swipeDownOnSmallScreen(50, 50, 20, 20);
            if (Actions.isVisible(this.productHubApplyLoanScreen.agreementCheckboxOne)) {
                break;
            }
        }
        Actions.click(this.productHubApplyLoanScreen.agreementCheckboxOne);
    }

    selectAgreementCheckboxTwo() {
        for (let i = 0; i < 10; i++) {
            Actions.swipeDownOnSmallScreen(50, 50, 20, 20);
            if (Actions.isVisible(this.productHubApplyLoanScreen.agreementCheckboxTwo)) {
                break;
            }
        }
        Actions.click(this.productHubApplyLoanScreen.agreementCheckboxTwo);
        Actions.swipeDownOnSmallScreen(50, 50, 20, 20);
        Actions.click(this.productHubApplyLoanScreen.agreementForStatements);
    }

    selectCheckEligibility() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.productHubApplyLoanScreen.checkEligibilityButton);
        Actions.click(this.productHubApplyLoanScreen.checkEligibilityButton);
    }

    verifyLoanDeclinePage() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.loanDeclinePageTitle, 50000);
        Actions.isVisible(this.productHubApplyLoanScreen.loanDeclinePageTitle).should.equal(true);
    }

    selectBookAppointment() {
        let i = 0;
        while (i < 10) {
            Actions.swipePage(SwipeAction.UP, 800);
            Actions.pause(1000);
            i++;
        }
        Actions.click(this.productHubApplyLoanScreen.bookAppointmentOption);
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.productHubApplyLoanScreen.bookAppointment);
    }

    verifyFindBranchPage() {
        Actions.isVisible(this.productHubApplyLoanScreen.titleFindBranch);
    }

    enterPostCodeAndFindBranch(postCode) {
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.productHubApplyLoanScreen.postcode);
        Actions.sendKeysAndTapOut(this.productHubApplyLoanScreen.postcode, postCode);
        Actions.click(this.productHubApplyLoanScreen.findBranch);
    }

    verifyBranchResults(postcode) {
        // Added pause to load the results
        Actions.pause(10000);
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.isVisible(this.productHubApplyLoanScreen.branchResults(postcode));
    }

    selectContinueFindBranch() {
        Actions.click(this.productHubApplyLoanScreen.continueFindBranch);
    }

    verifyAppointmentTimePage() {
        Actions.isVisible(this.productHubApplyLoanScreen.titleAppointment);
    }

    selectApplyLoanContinueButton() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.applyLoanContinueLoanButton);
        Actions.click(this.productHubApplyLoanScreen.applyLoanContinueLoanButton);
    }

    shouldVerifyLoanPurposePage() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.verifyPurposeLoanPage, 40000);
    }

    selectLoanPurposeContinueButton() {
        Actions.click(this.productHubApplyLoanScreen.purposeLoanContinueButton);
    }

    shouldVerifyLoanCalculatorPage() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.loanCalculatorPageTitle);
    }

    selectApplyLoanCalculateButton() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.loanCalculateButton);
        Actions.click(this.productHubApplyLoanScreen.loanCalculateButton);
    }

    shouldVerifyFutureChangeOption() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.futureChangeOptionNoButton);
    }

    selectFutureChangeOption() {
        Actions.click(this.productHubApplyLoanScreen.futureChangeOptionNoButton);
    }

    selectContinueApplicationButton() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.continueApplicationButton);
        Actions.click(this.productHubApplyLoanScreen.continueApplicationButton);
    }

    shouldVerifyYourApplicationPage() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.yourApplicationTitlePage, 40000);
    }

    selectRegularMonthlyIncome() {
        Actions.click(this.productHubApplyLoanScreen.regularMonthlyIncomeField);
    }

    selectChildCareCosts() {
        Actions.click(this.productHubApplyLoanScreen.childCareCostsField);
    }

    selectContactDetailsClosestMatchOption() {
        Actions.click(this.productHubApplyLoanScreen.contactDetailsClosestMatchOption);
        Actions.click(this.productHubApplyLoanScreen.confirmEmailAddressCheckbox);
    }

    selectAcknowledgeCheckBox() {
        Actions.click(this.productHubApplyLoanScreen.acknowledgeCheckbox);
    }

    selectSubmitButton() {
        Actions.click(this.productHubApplyLoanScreen.submitloanButton);
    }

    shouldVerifyApplyLoanPage() {
        Actions.waitForVisible(this.productHubApplyLoanScreen.applyLoanPage, 40000);
    }
}

module.exports = ProductHubLoanApplyPage;
