require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ProductHubLandingSavingsPage extends AuthPage {
    /**
     * @returns {ProductHubLandingSavingsScreen}
     */
    get productHubLandingSavingsScreen() {
        return this.getNativeScreen('productHub/productHubLandingSavings.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.productHubLandingSavingsScreen.compareSavings);
    }

    shouldBeOnCompareSavings() {
        Actions.isVisible(this.productHubLandingSavingsScreen.compareSavings)
            .should.equal(true);
    }
}

module.exports = ProductHubLandingSavingsPage;
