const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class BusinessLoanPage extends AuthPage {
    /**
     * @returns {businessLoanScreen}
     */
    get businessLoanScreen() {
        return this.getWebScreen('businessLoan.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.businessLoanScreen.title);
    }
}

module.exports = BusinessLoanPage;
