require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class ProductHubLandingLoanPage extends AuthPage {
    /**
     * @returns {ProductHubLandingLoanScreen}
     */
    get productHubLandingLoanScreen() {
        return this.getNativeScreen('productHub/productHubLandingLoan.screen');
    }

    waitForPageLoad() {
        Actions.waitUntilVisible([
            this.productHubLandingLoanScreen.loanCalculator,
            this.productHubLandingLoanScreen.loanCalculatorText
        ]);
    }

    shouldBeOnLoanCalculator() {
        Actions.waitUntilVisible([
            this.productHubLandingLoanScreen.loanCalculator,
            this.productHubLandingLoanScreen.loanCalculatorText
        ]);
    }
}

module.exports = ProductHubLandingLoanPage;
