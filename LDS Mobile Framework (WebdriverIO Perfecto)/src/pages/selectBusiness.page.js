const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class SelectBusinessPage extends AuthPage {
    /**
     * @returns {SelectBusinessScreen}
     */
    get selectBusinessScreen() {
        return this.getNativeScreen('selectBusiness.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.selectBusinessScreen.title);
    }

    selectFirstBusiness() {
        Actions.click(this.selectBusinessScreen.business(1));
    }
}

module.exports = SelectBusinessPage;
