const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');

class RequestOtpConfirmationPage extends UnAuthPage {
    /**
     * @returns {RequestOtpConfirmationScreen}
     */
    get requestOtpConfirmationScreen() {
        return this.getNativeScreen('requestOtpConfirmation.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.requestOtpConfirmationScreen.title);
    }
}

module.exports = RequestOtpConfirmationPage;
