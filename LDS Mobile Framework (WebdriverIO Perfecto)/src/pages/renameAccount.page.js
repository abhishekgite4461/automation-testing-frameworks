const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const AccountMatcher = require('../data/utils/accountMatcher');
const DeviceHelper = require('../utils/device.helper');

class RenameAccountPage extends AuthPage {
    /**
     * @returns {RenameAccountScreen}
     */
    get renameAccountScreen() {
        return this.getNativeScreen('renameAccount.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.renameAccountScreen.title);
    }

    shouldSeeRenameButtonDisabled() {
        Actions.isEnabled(this.renameAccountScreen.renameButton).should.equal(false);
    }

    enterAccountName(accountName) {
        Actions.click(this.renameAccountScreen.textField);
        Actions.genericSendKeys(this.renameAccountScreen.textField, accountName);
    }

    selectRenameButton() {
        Actions.click(this.renameAccountScreen.renameButton);
    }

    shouldSeeConfirmationBoxEnabled() {
        Actions.isEnabled(this.renameAccountScreen.confirmationBox).should.equal(true);
    }

    selectBackToYourAccountsButton() {
        Actions.waitForEnabled(this.renameAccountScreen.backToYourAccountsButton, 5000);
        Actions.click(this.renameAccountScreen.backToYourAccountsButton);
    }

    verifyExistingAccountName(accountName) {
        let accNames;
        if (DeviceHelper.isIOS()) {
            accNames = Actions.getText(this.renameAccountScreen.textField);
        } else {
            accNames = Actions.getText(this.renameAccountScreen.ghostAccountName);
        }
        (accNames).should.equal(AccountMatcher.getAccount(accountName.replace(/ /g, '')));
    }
}

module.exports = RenameAccountPage;
