require('chai').should();
const DeviceHelper = require('../utils/device.helper');
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class CwaPage extends AuthPage {
    get cwaScreen() {
        return this.getNativeScreen('cwa.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.cwaScreen.title);
    }

    backToApplication() {
        if (DeviceHelper.isAndroid()) {
            Actions.pressAndroidBackKey();
            Actions.closeBrowser();
        } else {
            Actions.click(this.cwaScreen.backToApp);
        }
    }
}

module.exports = CwaPage;
