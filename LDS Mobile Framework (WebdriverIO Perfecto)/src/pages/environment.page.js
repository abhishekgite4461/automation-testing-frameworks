const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');
const runConfig = require('../enums/runConfig.enum');

class EnvironmentPage extends AuthPage {
    /**
     * @returns {EnvironmentScreen}
     */
    get environmentScreen() {
        return this.getNativeScreen('environment.screen');
    }

    get requestPermissionPopUpScreen() {
        return this.getNativeScreen('requestPermissionPopUp.screen');
    }

    waitForPageLoad() {
        // Fix for Android 5.0.x script issues
        if (DeviceHelper.isPerfecto() && DeviceHelper.isAndroid()) {
            Actions.perfectoElementInfo('text', this.environmentScreen.title);
        }
        const locatorArray = [this.environmentScreen.title];
        Actions.waitUntilVisible(locatorArray, runConfig.TIMEOUTS.LAUNCH_TIMEOUT());
    }

    resetFeatureConfig(banWarnTypes, accountTypes) {
        // MQE-1075 TODO
        if (accountTypes === 'app ban' && DeviceHelper.isEnv()) {
            Actions.click(this.environmentScreen.oldResetFeatureConfigButton);
        } else {
            Actions.click(this.environmentScreen.resetFeatureConfigButton);
        }
    }

    setTestConfig(testConfig) {
        if (DeviceHelper.isPerfecto()) {
            Actions.clearText(this.environmentScreen.quickStubText);
            Actions.setImmediateValue(
                this.environmentScreen.quickStubText,
                JSON.stringify(testConfig)
            );
        } else {
            Actions.setValue(
                this.environmentScreen.quickStubText,
                JSON.stringify(testConfig)
            );
        }
        Actions.click(this.environmentScreen.quickStubButton);
    }

    navigateFromDeenrolledPage(testConfig) {
        if (Actions.isVisible(this.environmentScreen.deenrolledErrorMessage)) {
            Actions.click(this.environmentScreen.deenrolledContinueButton);
            if (DeviceHelper.isIOS()) {
                this.waitForPageLoad();
                this.setTestConfig(testConfig);
            }
        }
    }

    selectEnvironment(server) {
        const phaseLoc = this.environmentScreen.server(server.testPhase);
        const platformLoc = this.environmentScreen.server(server.platform);
        const serverLoc = this.environmentScreen.server(server.appName);

        /**
         * When running tests In Stub mode enter platform and app name into the quick stub text box
         * and click Go button, For running tests Environments use environments selector
         */

        if (server.testPhase === 'STUB') {
            if (server.platform === 'Success Scenarios' && server.appName === 'Enrolled') {
                Actions.click(this.environmentScreen.quickStubButton);
            } else {
                Actions.clearText(this.environmentScreen.quickStubText);
                if (DeviceHelper.isPerfecto()) { // setImmediateValue is quick on Perfecto
                    Actions.setImmediateValue(
                        this.environmentScreen.quickStubText,
                        `${server.platform}//${server.appName}`
                    );
                } else {
                    Actions.setValue(
                        this.environmentScreen.quickStubText,
                        `${server.platform}//${server.appName}`
                    );
                }
                Actions.click(this.environmentScreen.quickStubButton);
            }
        } else if (DeviceHelper.isIOS()) {
            Actions.setValue(this.environmentScreen.quickStubText,
                `${server.testPhase}//${server.platform}//${server.appName}`);
            Actions.click(this.environmentScreen.quickStubButton);
        } else {
            Actions.hideDeviceKeyboard();
            Actions.swipeByPercentUntilVisible(phaseLoc);
            Actions.click(phaseLoc);
            Actions.swipeToLocator('//XCUIElementTypeTable', server.platform, platformLoc);
            Actions.click(platformLoc);
            Actions.swipeToLocator('//XCUIElementTypeTable', server.appName, serverLoc);
            Actions.click(serverLoc);
        }
    }

    enterAppVersion(appVersion) {
        Actions.sendKeys(this.environmentScreen.inputAppVersion, appVersion);
    }
}

module.exports = EnvironmentPage;
