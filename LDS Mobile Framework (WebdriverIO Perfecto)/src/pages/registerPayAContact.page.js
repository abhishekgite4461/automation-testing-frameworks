const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class RegisterPayAContact extends AuthPage {
    get registerPayAContactScreen() {
        return this.getNativeScreen('paymentHub/registerPayAContact.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.registerPayAContactScreen.title);
    }

    selectRegisterPayAContact() {
        Actions.click(this.registerPayAContactScreen.registerButton);
    }

    selectContinue() {
        Actions.waitForVisible(this.registerPayAContactScreen.continueButton);
        Actions.click(this.registerPayAContactScreen.continueButton);
    }

    selectAgreementAndContinue() {
        Actions.click(this.registerPayAContactScreen.agreeCheckBox);
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        Actions.click(this.registerPayAContactScreen.continuetoP2PButton);
    }

    selectContinueToP2PAuthentication() {
        Actions.click(this.registerPayAContactScreen.continueToP2PAuthentication);
    }

    verifySuccessMessage() {
        Actions.waitForVisible(this.registerPayAContactScreen.P2PsuccessMessage, 18000);
    }

    deregisterP2P() {
        Actions.click(this.registerPayAContactScreen.lnkP2PSetting);
        Actions.click(this.registerPayAContactScreen.lkDeRegisterFromP2P);
        Actions.pause(10000);
        Actions.setValue(this.registerPayAContactScreen.authPassword, 'auto24680');
        Actions.click(this.registerPayAContactScreen.continueToDereg);
        Actions.waitForVisible(this.registerPayAContactScreen.deregSuceesMsg);
    }

    selectHomeButton() {
        Actions.click(this.commonScreen.homeButton);
    }
}

module.exports = RegisterPayAContact;
