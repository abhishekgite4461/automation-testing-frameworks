require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class ViewTransactionPage extends AuthPage {
    /**
     * @returns {ViewTransactionScreen}
     */
    get viewTransactionScreen() {
        return this.getNativeScreen('viewTransaction.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.viewTransactionScreen.viewTransactionTitle);
    }

    verifyTransactionExist() {
        Actions.isVisible(this.viewTransactionScreen.noTransactionAvailable).should.equal(false);
    }

    selectActionMenu() {
        Actions.click(this.viewTransactionScreen.actionMenu);
    }
}

module.exports = ViewTransactionPage;
