const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');

class EnterPostalOtpTooltipPage extends UnAuthPage {
    /**
     * @returns {EnterPostalOtpTooltipScreen}
     */
    get enterPostalOtpTooltipScreen() {
        return this.getNativeScreen('enterPostalOtpTooltip.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.enterPostalOtpTooltipScreen.title);
    }

    selectCloseButton() {
        Actions.click(this.enterPostalOtpTooltipScreen.closeButton);
    }
}

module.exports = EnterPostalOtpTooltipPage;
