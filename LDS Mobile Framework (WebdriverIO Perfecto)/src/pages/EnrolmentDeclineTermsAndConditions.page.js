const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class EnrolmentDeclineTermsAndConditions extends AuthPage {
    /**
     * @returns {EnrolmentDeclineTermsAndConditions}
     */
    get enrolmentDeclineTermsAndConditionsScreen() {
        return this.getNativeScreen('enrolmentDeclineTermsAndConditions.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.enrolmentDeclineTermsAndConditionsScreen.title);
    }

    confirmDecliningTermsAndConditions() {
        Actions.click(this.enrolmentDeclineTermsAndConditionsScreen.confirmDeclineButton);
    }

    backToTermsAndConditionsPage() {
        Actions.click(this.enrolmentDeclineTermsAndConditionsScreen.backButton);
    }
}

module.exports = EnrolmentDeclineTermsAndConditions;
