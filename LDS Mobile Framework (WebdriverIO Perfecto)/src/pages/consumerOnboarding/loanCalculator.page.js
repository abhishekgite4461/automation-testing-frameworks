require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');

class LoanCalculatorPage extends AuthPage {
    /**
     * @returns {loanCalculatorScreen}
     */
    get loanCalculatorScreen() {
        return this.getWebScreen('consumerOnboarding/loanCalculatorScreen.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.loanCalculatorScreen.title, 30000);
    }

    selectHomeImprovementoption() {
        Actions.click(this.loanCalculatorScreen.homeImprovementOption);
    }

    selectContinueButton() {
        Actions.click(this.loanCalculatorScreen.continue);
    }

    shouldSeeLoanCalulatorPage() {
        Actions.isVisible(this.loanCalculatorScreen.loanCalculatorText).should.equal(true);
    }

    enterLoanAmount(amount) {
        Actions.sendKeys(this.loanCalculatorScreen.amountField, amount);
    }

    enterLoanTerm(amount) {
        Actions.sendKeysAndTapOut(this.loanCalculatorScreen.termField, amount);
    }

    selectCalculateButton() {
        Actions.click(this.loanCalculatorScreen.calculateButton);
    }

    selectFutureOption() {
        Actions.click(this.loanCalculatorScreen.negativeButton);
        Actions.click(this.loanCalculatorScreen.continueApplicationButton);
    }

    selectContinueApplicationButton() {
        Actions.click(this.loanCalculatorScreen.applyContinueButton);
    }

    shouldSeeApplyLoanPage() {
        Actions.isVisible(this.loanCalculatorScreen.applyLoanPageHeader).should.equal(true);
    }

    selectEmploymentStatus() {
        Actions.click(this.loanCalculatorScreen.employmentStatus);
        Actions.click(this.loanCalculatorScreen.employedPartTimeState);
    }

    enterSalaryAmount(salary) {
        Actions.sendKeys(this.loanCalculatorScreen.salaryField, salary);
    }

    selectSpendingAccordian() {
        Actions.click(this.loanCalculatorScreen.spendingExpensesAccordian);
    }

    selectNoForOtherRegularMonthlyIncome() {
        Actions.click(this.loanCalculatorScreen.regularMonthlyIncomeNoButton);
    }

    enterMortgageShare(mortgageShare) {
        Actions.sendKeys(this.loanCalculatorScreen.shareOfMortagageAmountField, mortgageShare);
    }

    enterDependentDetails(dependent) {
        Actions.sendKeys(this.loanCalculatorScreen.dependentField, dependent);
    }

    selectNoForExtraCost() {
        Actions.click(this.loanCalculatorScreen.regularMonthlyExpenseNoButton);
    }

    selectContactDetailsAccordian() {
        Actions.click(this.loanCalculatorScreen.contactDetailsAccordian);
    }

    selectConfirmEmailCheckBox() {
        Actions.click(this.loanCalculatorScreen.agreeEmailCheckBox);
    }
}

module.exports = LoanCalculatorPage;
