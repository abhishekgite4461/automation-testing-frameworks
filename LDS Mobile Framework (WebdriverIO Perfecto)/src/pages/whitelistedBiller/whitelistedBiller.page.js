require('chai').should();
const AuthPage = require('../common/auth.page');
const Actions = require('../common/actions');
const SwipeAction = require('../../enums/swipeAction.enum');

class WhitelistedBiller extends AuthPage {
    get whitelistedBillerScreen() {
        return this.getNativeScreen('whitelistedBiller/whitelistedBiller.screen');
    }

    verifyBillerVisibility(isShown, account) {
        if (isShown === 'not see') {
            Actions.isVisible(this.whitelistedBillerScreen.lloydsBiller(account)).should.equal(false);
        } else {
            Actions.isVisible(this.whitelistedBillerScreen.lloydsBiller(account)).should.equal(true);
        }
    }

    selectBiller(account) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.whitelistedBillerScreen.lloydsBiller(account));
        Actions.waitAndClick(this.whitelistedBillerScreen.lloydsBiller(account));
    }

    shouldSeeReferenceHint() {
        Actions.isVisible(this.whitelistedBillerScreen.refBiller).should.equal(true);
    }

    shouldSeeRefErrorMessage() {
        Actions.isVisible(this.whitelistedBillerScreen.refErrorBanner).should.equal(true);
    }

    shouldSeeApiFailErrorMessage() {
        Actions.isVisible(this.whitelistedBillerScreen.apiFailErrorBanner).should.equal(true);
    }
}
module.exports = WhitelistedBiller;
