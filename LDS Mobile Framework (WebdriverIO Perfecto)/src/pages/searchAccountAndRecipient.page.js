require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');

class SearchAccountAndRecipientPage extends AuthPage {
    /**
     * @returns {SearchRecipientScreen}
     */
    get searchRecipientScreen() {
        return this.getNativeScreen('transferAndPayments/searchRecipient.screen');
    }

    verifySearchRecipientPage() {
        Actions.isVisible(this.searchRecipientScreen.title).should.equal(true);
    }

    verifySearchBarPresent() {
        Actions.isVisible(this.searchRecipientScreen.searchIcon).should.equal(true);
    }

    selectSearchBar() {
        Actions.click(this.searchRecipientScreen.searchIcon);
    }

    selectResultantAccount(account) {
        Actions.click(this.searchRecipientScreen.searchResult(account));
    }

    verifyAddNewPayee(visible) {
        if (visible === 'should') {
            Actions.isVisible(this.searchRecipientScreen.addNewPayee).should.equal(true);
        } else if (visible === 'should not') {
            Actions.isVisible(this.searchRecipientScreen.addNewPayee).should.equal(false);
        } else {
            throw new Error(`ERROR: Unknown ${visible} Type`);
        }
    }

    verifyRecipientCell() {
        if (DeviceHelper.isIOS()) {
            Actions.isAnyVisible(this.searchRecipientScreen.recipientCell)
                .should.equal(true);
        } else {
            Actions.isAnyVisible(this.searchRecipientScreen.recipientCellByIndex(1))
                .should.equal(true);
        }
    }

    verifyAddNewPayeeFooter() {
        Actions.swipeByPercentUntilVisible(this.searchRecipientScreen
            .addNewPayeeFooter, 50, 80, 50, 20, 100, 10);
    }

    selectAddNewPayee() {
        Actions.waitForVisible(this.searchRecipientScreen.title);
        Actions.click(this.searchRecipientScreen.addNewPayee);
    }

    enterSearchText(searchTerm) {
        Actions.sendKeys(this.searchRecipientScreen.searchEditText, searchTerm);
    }

    selectRecipient(recipient) {
        this.enterSearchText(recipient);
        Actions.click(this.searchRecipientScreen.firstRecipientFromList);
    }

    selectTransferAccount(recipient) {
        const remitter = Actions.modifyAccountName(recipient);
        this.enterSearchText(remitter);
        Actions.click(this.searchRecipientScreen.transferToList(remitter));
    }

    verifySearchResults(searchTerm, searchCategory, result) {
        const modifiedAccountName = Actions.modifyAccountName(result, true);
        switch (searchCategory) {
        case 'account':
            if (result === 'no results') {
                Actions.isVisible(this.searchRecipientScreen.noResultMessage).should.equal(true);
            } else {
                Actions.waitForVisible(this.searchRecipientScreen.accountResultByName(modifiedAccountName));
            }
            break;
        case 'recipient':
            if (result === 'no results') {
                Actions.isVisible(this.searchRecipientScreen.noResultMessage).should.equal(true);
            } else {
                Actions.waitForVisible(this.searchRecipientScreen.benificiaryResultByName(modifiedAccountName));
            }
            break;
        default:
            throw new Error(`Search result not found ${searchTerm}`);
        }
    }

    cancelSearch() {
        Actions.click(this.searchRecipientScreen.cancelSearch);
    }

    clearSearch() {
        Actions.clearText(this.searchRecipientScreen.searchEditText);
        if (DeviceHelper.isAndroid()) {
            Actions.tapOutOfField();
        }
    }

    verifyNoResultsMessage() {
        Actions.isVisible(this.searchRecipientScreen.noResultMessage).should.equal(true);
    }

    verifyAddNewPayeeLink(visible) {
        Actions.swipeByPercentUntilVisible(this.searchRecipientScreen.addNewPayeeFooter);
        if (visible === 'should') {
            Actions.isVisible(this.searchRecipientScreen.partialAddNewPayeeFooterText).should.equal(true);
        } else if (visible === 'should not') {
            Actions.isVisible(this.searchRecipientScreen.partialAddNewPayeeFooterText).should.equal(false);
        } else {
            throw new Error(`ERROR: Unknown ${visible} Type`);
        }
    }

    selectBannerAccountToTransfer() {
        Actions.click(this.searchRecipientScreen.bannerAccount);
    }

    selectBannerRecipientForPayment() {
        Actions.click(this.searchRecipientScreen.bannerRecipient);
    }

    selectManageRecipient(recipient) {
        this.enterSearchText(recipient);
        Actions.click(this.searchRecipientScreen.firstRecipientFromListManageRecipient);
    }

    deleteRecipient(number, recipient) {
        this.enterSearchText(recipient);
        if (number === 'second') {
            if (Actions.isVisible(this.searchRecipientScreen.secondRecipientFromListManageRecipient)) {
                Actions.click(this.searchRecipientScreen.secondRecipientFromListManageRecipient);
                Actions.click(this.searchRecipientScreen.deleteRecipient);
                Actions.click(this.searchRecipientScreen.deleteConfirm);
                Actions.pause(10000);
                this.enterSearchText(recipient);
                Actions.isVisible(this.searchRecipientScreen.secondRecipientFromListManageRecipient)
                    .should.equal(false);
            }
        } else if (number === 'first') {
            if (Actions.isVisible(this.searchRecipientScreen.firstRecipientFromListManageRecipient)) {
                Actions.click(this.searchRecipientScreen.firstRecipientFromListManageRecipient);
                Actions.click(this.searchRecipientScreen.deleteRecipient);
                Actions.click(this.searchRecipientScreen.deleteConfirm);
                Actions.pause(10000);
                this.enterSearchText(recipient);
                Actions.isVisible(this.searchRecipientScreen.firstRecipientFromListManageRecipient)
                    .should.equal(false);
            }
        }
    }
}

module.exports = SearchAccountAndRecipientPage;
