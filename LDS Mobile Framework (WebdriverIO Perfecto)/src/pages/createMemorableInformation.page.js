require('chai').should();
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');

class CreateMemorableInformationPage extends UnAuthPage {
    /**
     * @returns {CreateMemorableInformationScreen}
     */
    get createMemorableInformationScreen() {
        return this.getNativeScreen('createMemorableInformation.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.createMemorableInformationScreen.title);
    }

    enterMemorableInformation(memorableInformation) {
        Actions.click(this.createMemorableInformationScreen.enterMI);
        Actions.genericSendKeys(this.createMemorableInformationScreen.enterMI, memorableInformation);
        Actions.tapOutOfField();
    }

    reEnterMemorableInformation(memorableInformation) {
        Actions.click(this.createMemorableInformationScreen.reEnterMI);
        Actions.genericSendKeys(this.createMemorableInformationScreen.reEnterMI, memorableInformation);
        Actions.tapOutOfField();
    }

    selectNextButton() {
        Actions.click(this.createMemorableInformationScreen.nextButton);
    }
}

module.exports = CreateMemorableInformationPage;
