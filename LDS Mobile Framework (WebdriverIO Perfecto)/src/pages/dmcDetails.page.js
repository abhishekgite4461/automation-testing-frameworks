const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class DmcDetailsPage extends AuthPage {
    /**
     * @returns {DmcDetailsScreen}
     */
    get dmcDetailsScreen() {
        return this.getWebScreen('dmcDetails.screen');
    }

    waitForDmcPageLoad() {
        Actions.waitForVisible(this.dmcDetailsScreen.title);
    }

    selectWebCloseButton() {
        Actions.click(this.dmcDetailsScreen.webCloseButton);
    }
}

module.exports = DmcDetailsPage;
