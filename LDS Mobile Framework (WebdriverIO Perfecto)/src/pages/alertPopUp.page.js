const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class AlertPopUpPage extends AuthPage {
    /**
     * @returns {AlertPopUpScreen}
     */
    //
    get alertPopUpScreen() {
        return this.getNativeScreen(this.screenName);
    }

    // This Constructor overrides phonenumber and email screens according to the Popup displayed.
    // So donot remove this constructor.
    constructor(screenName = 'alertPopUp.screen') {
        super();
        this.screenName = screenName;
    }

    shouldSeeAlert() {
        Actions.isVisible(this.alertPopUpScreen.title).should.equal(true);
    }

    shouldSeeAlertContentText() {
        Actions.isVisible(this.alertPopUpScreen.contentText).should.equal(true);
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.alertPopUpScreen.title, 9500);
    }

    isConfirmButtonVisible() {
        Actions.isVisible(this.alertPopUpScreen.confirmButton).should.equal(true);
    }

    isCancelButtonVisible() {
        Actions.isVisible(this.alertPopUpScreen.cancelButton).should.equal(true);
    }

    selectConfirmButton() {
        Actions.click(this.alertPopUpScreen.confirmButton);
    }

    selectCancelButton() {
        Actions.click(this.alertPopUpScreen.cancelButton);
    }
}

module.exports = AlertPopUpPage;
