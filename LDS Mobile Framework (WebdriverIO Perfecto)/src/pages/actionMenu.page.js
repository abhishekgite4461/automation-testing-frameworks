require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');
const DeviceHelper = require('../utils/device.helper');

class ActionMenuPage extends AuthPage {
    /**
     * @returns {ActionMenuScreen}
     */
    get actionMenuScreen() {
        return this.getNativeScreen('actionMenu.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.actionMenuScreen.title);
    }

    waitForExternalAccountActionMenuPageLoad() {
        Actions.waitForVisible(this.actionMenuScreen.externalAccountActionMenu);
    }

    selectViewTransactionsOption() {
        Actions.click(this.actionMenuScreen.viewTransactionsOption);
    }

    selectTransferAndPaymentOption() {
        Actions.click(this.actionMenuScreen.transferAndPaymentOption);
    }

    selectInternationalPayment() {
        Actions.click(this.actionMenuScreen.sendMoneyOutsideTheUKOption);
    }

    selectReplacementCardAndPinOption() {
        Actions.click(this.actionMenuScreen.replacementCardAndPinOption);
    }

    selectStandingOrderOption() {
        Actions.click(this.actionMenuScreen.standingOrderOption);
    }

    selectBorrowMoreOption() {
        Actions.click(this.actionMenuScreen.loanBorrowMoreOption);
    }

    selectDownloadTransactionsOption() {
        Actions.click(this.actionMenuScreen.downloadTransactionsOption);
    }

    selectPensionTransferOption() {
        Actions.click(this.actionMenuScreen.pensionTransferOption);
    }

    selectSaveTheChangeOption() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.saveTheChangeOption);
        Actions.click(this.actionMenuScreen.saveTheChangeOption);
    }

    selectOverdraftOption() {
        Actions.click(this.actionMenuScreen.applyForOverdraftOption);
    }

    selectSendBankDetailsOption() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.sendBankDetailsOption);
        Actions.click(this.actionMenuScreen.sendBankDetailsOption);
    }

    selectStandingOrderAndDirectDebitOption() {
        Actions.click(this.actionMenuScreen.standingOrderAndDirectDebitOption);
    }

    selectRenameAccountOption() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.renameAccountOption);
        Actions.click(this.actionMenuScreen.renameAccountOption);
    }

    selectChangeAccountTypeOption() {
        Actions.click(this.actionMenuScreen.changeAccountTypeOption);
    }

    validateActionMenuOptions(actionMenuOptions, isVisible = true) {
        let actualExist = true;
        for (let i = 0; i < actionMenuOptions.length; i++) {
            const individualActionMenuOption = this.actionMenuScreen[actionMenuOptions[i].optionsInActionMenu];
            if (isVisible) {
                Actions.swipeByPercentUntilVisible(individualActionMenuOption, 50, 80, 50, 20);
            } else {
                if (!Actions.isExisting(individualActionMenuOption)) {
                    Actions.swipePage(SwipeAction.UP);
                    if (!Actions.isExisting(individualActionMenuOption)) {
                        actualExist = false;
                    }
                }
                actualExist.should.equal(isVisible);
                Actions.swipePage(SwipeAction.DOWN);
            }
        }
    }

    selectActionMenuOptions(actionMenuType) {
        const actionMenuOption = this.actionMenuScreen[actionMenuType];
        if (Actions.isVisible(actionMenuOption)) {
            Actions.click(actionMenuOption);
        } else {
            Actions.swipeUntilVisible(SwipeAction.UP, actionMenuOption);
            Actions.click(actionMenuOption);
        }
    }

    closeActionMenu() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.actionMenuScreen.closeButton);
        } else {
            Actions.pressAndroidBackKey();
        }
    }

    selectLostAndStolenOption() {
        Actions.click(this.actionMenuScreen.lostOrStolenCardOption);
    }

    selectLoanAnnualStatementOption() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.loanAnnualStatementsOption);
        Actions.click(this.actionMenuScreen.loanAnnualStatementsOption);
    }

    verifyPendingPaymentOption(option) {
        if (option === 'viewUpcomingPayment') {
            Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.viewUpcomingPayment);
        } else if (option === 'viewPaymentsDueSoon') {
            Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.viewPendingPaymentOption);
        } else if (option === 'viewSpendingInsights') {
            Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.viewSpendingInsights);
        } else {
            throw new Error('option does not match');
        }
    }

    verifyNoPendingPaymentOption() {
        Actions.isVisible(this.actionMenuScreen.viewPendingPaymentOption).should.equal(false);
    }

    selectPendingPaymentOption(option) {
        if (option === 'viewUpcomingPayment') {
            Actions.waitAndClick(this.actionMenuScreen.viewUpcomingPayment);
        } else if (option === 'viewPaymentsDueSoon') {
            Actions.waitAndClick(this.actionMenuScreen.viewPendingPaymentOption);
        } else if (option === 'viewSpendingInsights') {
            Actions.waitAndClick(this.actionMenuScreen.viewSpendingInsights);
        } else {
            throw new Error('option does not match');
        }
    }

    selectPayCreditCard() {
        Actions.click(this.actionMenuScreen.payCreditCardOption);
    }

    verifyNoPayCreditCard() {
        Actions.isVisible(this.actionMenuScreen.payCreditCardOption).should.equal(false);
    }

    selectActionMenuTopUpIsa() {
        Actions.waitForVisible(this.actionMenuScreen.topupIsaOption);
        Actions.click(this.actionMenuScreen.topupIsaOption);
    }

    shouldSeeTopUpIsa() {
        Actions.isExisting(this.actionMenuScreen.topupIsaOption).should.equal(true);
    }

    clickViewPendingPayment() {
        Actions.click(this.actionMenuScreen.viewPendingPayment);
    }

    verifyNoViewTransactionsOption() {
        Actions.isVisible(this.actionMenuScreen.viewTransactionsOption).should.equal(false);
    }

    shouldSeeDownloadTransactionsOption() {
        Actions.isVisible(this.actionMenuScreen.downloadTransactionsOption).should.equal(true);
    }

    viewInterestDetailsOptionIsNotVisible() {
        Actions.isVisible(this.actionMenuScreen.interestDetailRow).should.equal(false);
    }

    selectViewInterestDetails() {
        Actions.click(this.actionMenuScreen.interestDetailRow);
    }

    selectDirectDebitOption() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.directDebitOption);
        Actions.click(this.actionMenuScreen.directDebitOption);
    }

    selectActionMenuReactivateISA() {
        Actions.waitForVisible(this.actionMenuScreen.reactivateIsaOption);
        Actions.click(this.actionMenuScreen.reactivateIsaOption);
    }

    verifyChequeDepositOptionIsNotVisible() {
        Actions.isVisible(this.actionMenuScreen.chequeDepositOption).should.equal(false);
    }

    verifyTransferAndPaymentOptionIsNotVisible() {
        Actions.isVisible(this.actionMenuScreen.transferAndPaymentOption).should.equal(false);
    }

    validateActionMenuOption(actionMenuOptions) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen[actionMenuOptions]);
    }

    clickOnActionMenuOption(actionMenuOptions) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen[actionMenuOptions]);
        Actions.click(this.actionMenuScreen[actionMenuOptions]);
    }

    selectPdfStatements() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.pdfStatementsOption);
        Actions.click(this.actionMenuScreen.pdfStatementsOption);
    }

    selectOrderPaperStatements() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.orderPaperStatementOption);
        Actions.click(this.actionMenuScreen.orderPaperStatementOption);
    }

    selectChequeDepositOption() {
        Actions.click(this.actionMenuScreen.chequeDepositOption);
    }

    selectGivenActionMenuOption(option) {
        Actions.click(this.actionMenuScreen.externalAccountMenuOptionsLabel(option));
    }

    verifyExternalBankNameOnActionMenuHeader(bankName) {
        Actions.getText(this.actionMenuScreen.actionMenuHeader).toLowerCase().should.contains(bankName.toLowerCase());
    }

    verifyExternalBankAccountActionMenuPopupOptions(values) {
        const numberOfActionMenuOptions = browser.elements(this.actionMenuScreen
            .externalBankAccountNumberOfActionMenuOptions).value.length;
        for (let i = 0; i < values.length; i++) {
            let valueFound = false;
            for (let j = 1; j <= numberOfActionMenuOptions; j++) {
                if (DeviceHelper.isIOS()) {
                    if (values[i].toString().toLowerCase()
                        === (Actions.getAttributeText(this.actionMenuScreen.actionMenuOption(j), 'name').toString().toLowerCase())) {
                        valueFound = true;
                        break;
                    }
                } else if (values[i].toString().toLowerCase()
                    === (Actions.getText(this.actionMenuScreen.actionMenuOption(j)).toString().toLowerCase())) {
                    valueFound = true;
                    break;
                }
            }
            if (valueFound === false) {
                throw new Error(`Action Menu option ${values[i]} not present`);
            }
        }
    }

    verifyCloseButtonPresent() {
        if (DeviceHelper.isIOS()) {
            Actions.isVisible(this.actionMenuScreen.closeButton).should.equals(true);
        }
    }

    verifyTopUpISANotPresent() {
        Actions.isVisible(this.actionMenuScreen.topupIsaOption).should.equals(false);
    }

    selectCardManagement() {
        Actions.click(this.actionMenuScreen.cardManagementOption);
    }

    selectMakeAnOverpaymentOption() {
        Actions.click(this.actionMenuScreen.makeAnOverpaymentOption);
    }

    swipeAndSelectSendBankDetailsOption() {
        Actions.swipeByPercentage(80, 25, 20, 20, 100);
        Actions.click(this.actionMenuScreen.sendBankDetailsOption);
    }

    shouldNotSeeSendBankDetailsOption() {
        Actions.isVisible(this.actionMenuScreen.sendBankDetailsOption).should.equal(false);
    }

    selectViewSpendingInsights() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.actionMenuScreen.viewSpendingInsightsOption);
        Actions.click(this.actionMenuScreen.viewSpendingInsightsOption);
    }
}

module.exports = ActionMenuPage;
