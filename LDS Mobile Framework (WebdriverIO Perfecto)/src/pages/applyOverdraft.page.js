const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class ApplyOverdraftPage extends AuthPage {
    /**
     * @returns {ApplyOverdraftScreen}
     */
    get applyOverdraftScreen() {
        return this.getWebScreen('applyOverdraft.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.applyOverdraftScreen.title);
    }

    selectAccount() {
        if (Actions.isVisible(this.applyOverdraftScreen.selectAccount)) {
            Actions.click(this.applyOverdraftScreen.selectAccount);
        }
    }

    closeOverdraftInfoModule() {
        Actions.waitForVisible(this.applyOverdraftScreen.infoModalTitle);
        Actions.click(this.applyOverdraftScreen.infoModalContinueButton);
    }

    enterOverdraftAmount(overdraft) {
        Actions.waitForVisible(this.applyOverdraftScreen.letmetypeLink);
        Actions.click(this.applyOverdraftScreen.letmetypeLink);
        Actions.waitForVisible(this.applyOverdraftScreen.overdraftInputBox);
        Actions.setImmediateValue(this.applyOverdraftScreen.overdraftInputBox, overdraft);
    }

    waitForFeeCalculator() {
        Actions.waitForVisible(this.applyOverdraftScreen.feeCalculator);
    }

    selectOverdraftContinueButton() {
        Actions.waitForVisible(this.applyOverdraftScreen.overdraftContinueButton);
        Actions.click(this.applyOverdraftScreen.overdraftContinueButton);
    }

    waitForINEPage() {
        Actions.waitForVisible(this.applyOverdraftScreen.ineScreenHeader, 5000);
    }

    selectEmploymentStatusFullTime() {
        Actions.click(this.applyOverdraftScreen.employmentStatus);
        Actions.waitForVisible(this.applyOverdraftScreen.employmentStatusFullTime);
        Actions.click(this.applyOverdraftScreen.employmentStatusFullTime);
    }

    enterIncomeAmount(income) {
        Actions.waitForVisible(this.applyOverdraftScreen.income, 3000);
        Actions.setImmediateValue(this.applyOverdraftScreen.income, income);
    }

    enterMortgageFee(mortgage) {
        Actions.waitForVisible(this.applyOverdraftScreen.mortagage, 3000);
        Actions.setImmediateValue(this.applyOverdraftScreen.mortagage, mortgage);
    }

    enterMaintenanceFee(maintenance) {
        Actions.waitForVisible(this.applyOverdraftScreen.maintenance, 3000);
        Actions.setImmediateValue(this.applyOverdraftScreen.maintenance, maintenance);
    }

    selectNoChangeOption() {
        Actions.waitForVisible(this.applyOverdraftScreen.somethingAboutToChangeNo, 3000);
        Actions.waitAndClick(this.applyOverdraftScreen.somethingAboutToChangeNo);
    }

    selectINEContinueButton() {
        Actions.waitAndClick(this.applyOverdraftScreen.ineContinueButton);
    }

    selectConfirmEmail() {
        Actions.waitForVisible(this.applyOverdraftScreen.emailCorrectYes);
        Actions.click(this.applyOverdraftScreen.emailCorrectYes);
        Actions.waitAndClick(this.applyOverdraftScreen.emailScreenContinueButton);
    }

    selectConfirmPCCI() {
        Actions.isVisible(this.applyOverdraftScreen.pcciScreenHeader).should.equal(true);
        Actions.waitAndClick(this.applyOverdraftScreen.pcciAgreementCheckbox);
        Actions.waitAndClick(this.applyOverdraftScreen.pcciContinueButton);
    }

    verifyOverdraftSuccessPage(overdraft) {
        const od = `£${overdraft}`;
        Actions.isVisible(this.applyOverdraftScreen.successScreenHeader).should.equal(true);
        Actions.isVisible(this.applyOverdraftScreen.overdraftValue).should.equal(true);
        Actions.getText(this.applyOverdraftScreen.overdraftValue).should.equal(od);
        Actions.isVisible(this.applyOverdraftScreen.surveyButton).should.equal(true);
        Actions.isVisible(this.applyOverdraftScreen.aovButton).should.equal(true);
    }

    selectOverdraftRemoveButton() {
        Actions.waitForVisible(this.applyOverdraftScreen.overdraftRemoveButton);
        Actions.click(this.applyOverdraftScreen.overdraftRemoveButton);
    }

    shouldSeeRemoveOverdraftSuccessPage() {
        Actions.isVisible(this.applyOverdraftScreen.successScreenHeader).should.equal(true);
        Actions.isVisible(this.applyOverdraftScreen.aovButton).should.equal(true);
    }

    verifyExistingOverdraft() {
        const od = '0';
        Actions.getText(this.applyOverdraftScreen.overdraftInputBox).should.not.equal(od);
    }
}
module.exports = ApplyOverdraftPage;
