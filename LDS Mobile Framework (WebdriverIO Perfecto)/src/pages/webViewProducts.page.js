require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');

class WebViewProductsPage extends AuthPage {
    /**
     * @returns {WebViewProductsScreen}
     */
    get webViewProductsScreen() {
        return this.getNativeScreen('webViewProducts.screen');
    }

    /**
     * @returns {ProductHubOverdraftsApplyScreen}
     */
    get productHubOverdraftsApplyScreen() {
        return this.getWebScreen('productHub/productHubOverdraftsApply.screen');
    }

    /**
     * @returns {ProductHubChangeAccountTypeScreen}
     */
    get productHubChangeAccountTypeScreen() {
        return this.getWebScreen('productHub/productHubChangeAccountType.screen');
    }

    isManageCreditLimitPageVisible() {
        Actions.waitUntilVisible([
            this.webViewProductsScreen.manageCreditLimitTitle,
            this.webViewProductsScreen.manageCreditLimitTitleText
        ]);
    }

    isRepaymentHolidayPageVisible() {
        Actions.waitUntilVisible([
            this.webViewProductsScreen.repaymentHolidayTitle,
            this.webViewProductsScreen.repaymentHolidayTitleText
        ]);
    }

    isAdditionalPaymentPageVisible() {
        Actions.waitUntilVisible([
            this.webViewProductsScreen.additionalPaymentTitle,
            this.webViewProductsScreen.additionalPaymentTitleText
        ]);
    }

    isBalanceTransferVisible() {
        Actions.waitUntilVisible([
            this.webViewProductsScreen.balanceTransferTitle,
            this.webViewProductsScreen.balanceTransferTitleText
        ]);
    }

    isBorrowMoreVisible() {
        Actions.waitUntilVisible([
            this.webViewProductsScreen.borrowMoreTitle,
            this.webViewProductsScreen.borrowMoreTitleText
        ]);
    }

    isLoanClosureVisible() {
        Actions.waitUntilVisible([
            this.webViewProductsScreen.loanClosureTitle,
            this.webViewProductsScreen.loanClosureTitleText
        ]);
    }

    isRenewYourSavingsAccountVisible() {
        if (DeviceHelper.isStub()) {
            Actions.waitUntilVisible([
                this.webViewProductsScreen.renewYourSavingsAccountTitle,
                this.webViewProductsScreen.renewYourSavingsAccountTitleText
            ]);
        } else {
            Actions.waitUntilVisible([
                this.webViewProductsScreen.selectSavingsAccountToRenew,
                this.webViewProductsScreen.selectSavingsAccountToRenewText
            ]);
        }
    }

    waitForPensionTransferTitle() {
        Actions.waitUntilVisible([
            this.webViewProductsScreen.pensionTransferTitle,
            this.webViewProductsScreen.pensionTransferTitleText
        ]);
    }

    shouldSeeOverdraftVisible() {
        if (DeviceHelper.isStub()) {
            Actions.waitUntilVisible([
                this.webViewProductsScreen.overdraftPopUpTitle,
                this.webViewProductsScreen.overdraftPopUpText
            ]);
        } else {
            Actions.waitUntilVisible([
                this.productHubOverdraftsApplyScreen.overdraftPopUpTitle
            ], 60000);
            Actions.click(this.productHubOverdraftsApplyScreen.continueButton);
            Actions.isVisible(this.productHubOverdraftsApplyScreen.overdraftPageTitle).should.equal(true);
        }
    }

    shouldSeeChangeAccountTypeVisible() {
        if (DeviceHelper.isStub()) {
            Actions.waitUntilVisible([
                this.webViewProductsScreen.changeAccountTypeTitle,
                this.webViewProductsScreen.changeAccountTypeText
            ]);
        } else {
            Actions.waitUntilVisible([
                this.productHubChangeAccountTypeScreen.changeAccountTypeTitle
            ], 60000);
        }
    }
}

module.exports = WebViewProductsPage;
