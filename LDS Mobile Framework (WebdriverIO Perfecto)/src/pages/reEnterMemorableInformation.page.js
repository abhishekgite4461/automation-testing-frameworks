const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const MemorableInformationPage = require('./memorableInformation.page');

class ReEnterMemorableInformationPage extends AuthPage {
    /**
     * @returns {ReEnterMemorableInformationScreen}
     */
    get reEnterMIScreen() {
        return this.getNativeScreen('reEnterMemorableInformation.screen');
    }

    waitForPageLoad() {
        Actions.isExisting(this.reEnterMIScreen.title).should.equal(true);
    }

    verifyAndCloseErrorMsg(authType) {
        if (authType === 'MI') {
            Actions.isExisting(this.reEnterMIScreen.wrongMIError).should.equal(true);
            Actions.click(this.reEnterMIScreen.closeButton);
        }

        if (authType === 'password') {
            Actions.isExisting(this.reEnterMIScreen.wrongPasswordError).should.equal(true);
            Actions.click(this.reEnterMIScreen.closeButton);
        }
    }

    reEnterMemorableInformation(mi) {
        const memorableInformationPage = new MemorableInformationPage();
        memorableInformationPage.enterMI(mi, 3, this.reEnterMIScreen.memorableCharacter);
    }

    clickForgotLogonDetails() {
        Actions.click(this.reEnterMIScreen.miForgotYourPassword);
    }

    shouldSeeReenterMIPageTitle() {
        Actions.waitForVisible(this.reEnterMIScreen.title);
    }

    selectFscsTitle() {
        Actions.click(this.reEnterMIScreen.fscsTitle);
    }
}

module.exports = ReEnterMemorableInformationPage;
