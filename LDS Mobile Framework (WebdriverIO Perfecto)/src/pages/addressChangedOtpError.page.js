const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
require('chai').should();

class AddressChangedOtpErrorPage extends AuthPage {
    /**
     * @returns {AddressChangedOtpErrorPage}
     */
    get addressChangedOtpErrorScreen() {
        return this.getNativeScreen('addressChangedOtpError.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.addressChangedOtpErrorScreen.title);
    }

    verifyAddressChangedErrorMessage() {
        Actions.isVisible(this.addressChangedOtpErrorScreen.addressChangedOtpErrorMessage).should.equal(true);
        Actions.isVisible(this.addressChangedOtpErrorScreen.goToLogonButton).should.equal(true);
    }
}

module.exports = AddressChangedOtpErrorPage;
