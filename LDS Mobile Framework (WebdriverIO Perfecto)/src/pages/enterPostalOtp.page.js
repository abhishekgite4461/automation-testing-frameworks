require('chai').should();
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');

class EnterPostalOtpPage extends UnAuthPage {
    /**
     * @returns {EnterPostalOtpScreen}
     */
    get enterPostalOtpScreen() {
        return this.getNativeScreen('enterPostalOtp.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.enterPostalOtpScreen.title);
    }

    selectNotReceivedTooltip() {
        Actions.click(this.enterPostalOtpScreen.otpNotRecievedToolTip);
    }

    shouldSeeLogoffOption() {
        Actions.isVisible(this.enterPostalOtpScreen.logoffOption).should.equal(true);
    }

    enterOTP(otp) {
        Actions.click(this.enterPostalOtpScreen.enterOTPField);
        Actions.setImmediateValue(this.enterPostalOtpScreen.enterOTPField, otp);
        if (DeviceHelper.isIOS()) {
            Actions.tapOutOfField();
        } else {
            Actions.click(this.enterPostalOtpScreen.continueButton);
        }
    }

    verifyIncorrectOTPMessage() {
        Actions.isVisible(this.enterPostalOtpScreen.invalidOTPErrorMessage).should.equal(true);
    }
}

module.exports = EnterPostalOtpPage;
