require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class OnlinePaperPreferenceWebViewpage extends AuthPage {
    get globalMenuScreen() {
        return this.getNativeScreen('globalMenu.screen');
    }

    /**
     * @returns {OnlinePaperPreferenceScreen}
     */
    get onlinePaperPreferenceScreen() {
        return this.getWebScreen('onlinePaperPreference.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.onlinePaperPreferenceScreen.title, 30000);
    }

    dismissCookieConsentIfPresent() {
        if (Actions.isVisible(this.onlinePaperPreferenceScreen.acceptCookie)) {
            Actions.click(this.onlinePaperPreferenceScreen.acceptCookie);
        }
    }

    selectGlobalBackButton() {
        Actions.click(this.globalMenuScreen.globalBackButton);
    }

    isCustomPreferenceChecked() {
        return Actions.getAttribute(this.onlinePaperPreferenceScreen.customisePreferences, 'class').indexOf('checked') > 0;
    }

    modifyPreferences() {
        if (!this.isCustomPreferenceChecked()) {
            Actions.click(this.onlinePaperPreferenceScreen.customisePreferences);
        }
        this.togglePreferences();
    }

    togglePreferences() {
        Actions.click(this.onlinePaperPreferenceScreen.togglePreference);
    }

    updatePreference() {
        Actions.click(this.onlinePaperPreferenceScreen.updatePreferences);
    }

    selectForgetPassword() {
        Actions.click(this.OnlinePaperPreferenceScreen.forgetPassword);
    }

    waitForConfirmPreferencePageLoad() {
        Actions.waitForVisible(this.onlinePaperPreferenceScreen.confirmPreferenceHeader);
    }

    acceptTermsAndCondition() {
        Actions.click(this.onlinePaperPreferenceScreen.termsAndCondition);
    }

    enterPassword(password) {
        Actions.sendKeys(this.onlinePaperPreferenceScreen.password, password);
    }

    submitPreference() {
        Actions.click(this.onlinePaperPreferenceScreen.confirmPreferenceButton);
    }

    waitForPreferenceSuccessPageLoad() {
        Actions.waitForVisible(this.onlinePaperPreferenceScreen.onlinePaperPreferenceSuccessHeader);
    }

    static scrollToEndOfPage() {
        for (let i = 0; i < 8; i++) {
            Actions.swipePage(SwipeAction.UP);
        }
    }

    verifyWinbackOption() {
        Actions.waitForVisible(this.onlinePaperPreferenceScreen.winback);
        Actions.waitForVisible(this.onlinePaperPreferenceScreen.leaveButton);
        Actions.waitForVisible(this.onlinePaperPreferenceScreen.stayButton);
    }

    verifyConfirmPreferencePage() {
        if (!(Actions.isVisible(this.onlinePaperPreferenceScreen.confirmPreferencesTitle))) {
            Actions.isVisible(this.onlinePaperPreferenceScreen.forgetPassword);
        }
    }

    selectStayButton() {
        Actions.click(this.onlinePaperPreferenceWebViewPageScreen.stayButton);
    }

    selectLeaveOption() {
        Actions.click(this.onlinePaperPreferenceWebViewPageScreen.leaveButton);
    }

    verifyForgottenPasswordWinbackOption() {
        Actions.waitForVisible(this.onlinePaperPreferenceWebViewPageScreen.forgetPasswordWinback);
        Actions.waitForVisible(this.onlinePaperPreferenceWebViewPageScreen.leaveButton);
        Actions.waitForVisible(this.onlinePaperPreferenceWebViewPageScreen.stayButton);
    }
}
module.exports = OnlinePaperPreferenceWebViewpage;
