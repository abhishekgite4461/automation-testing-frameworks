const AuthPage = require('./common/auth.page'); const Actions = require('./common/actions');

class PayAContactSettings extends AuthPage {
    /**
     * @returns {PayAContactSettingsScreen}
     */
    get payAContactSettingsScreen() {
        return this.getNativeScreen('PayAContactSettings.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.payAContactSettingsScreen.title);
    }
}

module.exports = PayAContactSettings;
