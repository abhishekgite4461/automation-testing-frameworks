const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class RequestPermissionPopUpPage extends AuthPage {
    /**
     * @returns {RequestPermissionPopUpScreen}
     */
    get requestPermissionPopUpScreen() {
        return this.getNativeScreen('requestPermissionPopUp.screen');
    }

    allowNativeAppCall() {
        if (Actions.isVisible(this.requestPermissionPopUpScreen.title)) {
            Actions.click(this.requestPermissionPopUpScreen.permissionAcceptButton);
        }
        Actions.pause(1000); // wait for pop up incase it appears
        if (Actions.isVisible(this.requestPermissionPopUpScreen.setPhoneDefaultOption)) {
            Actions.click(this.requestPermissionPopUpScreen.setPhoneDefaultOption);
        }
    }

    acceptGpsPopup() {
        // Pop up title changes with OS version hence using 'Allow' Button to check
        Actions.pause(1000); // wait for pop up incase it appears
        if (Actions.isVisible(this.requestPermissionPopUpScreen.permissionAcceptButton)) {
            Actions.click(this.requestPermissionPopUpScreen.permissionAcceptButton);
        }
    }
}

module.exports = RequestPermissionPopUpPage;
