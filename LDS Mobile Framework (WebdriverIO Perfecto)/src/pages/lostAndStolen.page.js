const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class LostAndStolenCardPage extends AuthPage {
    /**
     * @returns {LostAndStolenCard}
     */
    get lostAndStolenCard() {
        return this.getNativeScreen('lostAndStolenCard.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.lostAndStolenCard.title);
    }

    selectBackButton() {
        Actions.click(this.lostAndStolenCard.backButton);
    }

    selectLostOption() {
        Actions.click(this.lostAndStolenCard.lost);
    }

    selectCardsOption() {
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        if (Actions.isVisible(this.lostAndStolenCard.cancelCardCredit)) {
            Actions.click(this.lostAndStolenCard.cancelCardCredit);
        }
        if (Actions.isVisible(this.lostAndStolenCard.cancelCardDebit)) {
            Actions.click(this.lostAndStolenCard.cancelCardDebit);
        }
        if (Actions.isVisible(this.lostAndStolenCard.newPin)) {
            Actions.click(this.lostAndStolenCard.newPin);
        }
    }

    selectContinue() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.lostAndStolenCard.continue);
        Actions.click(this.lostAndStolenCard.continue);
    }

    verifyTitle() {
        Actions.isVisible(this.lostAndStolenCard.confirmTitle);
    }

    validateConfirmationOptions(pageFields) {
        Actions.pause(1000);
        Actions.swipePage(SwipeAction.UP);
        Actions.swipePage(SwipeAction.UP);
        for (let iter = 0; iter < pageFields.length; iter++) {
            Actions.isVisible(this.lostAndStolenCard[pageFields[iter]
                .fieldsInDetailScreen]);
        }
    }
}

module.exports = LostAndStolenCardPage;
