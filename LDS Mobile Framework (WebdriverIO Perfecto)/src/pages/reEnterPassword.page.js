require('chai').should();
const UnAuthPage = require('./common/unAuth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');

class ReEnterPasswordPage extends UnAuthPage {
    /**
     * @returns {ReEnterPasswordScreen}
     */
    get reEnterPasswordScreen() {
        return this.getNativeScreen('reEnterPassword.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.reEnterPasswordScreen.title);
    }

    reEnterPassword(password) {
        Actions.click(this.reEnterPasswordScreen.reEnterPasswordField);
        Actions.setImmediateValue(this.reEnterPasswordScreen.reEnterPasswordField, password);
        Actions.click(this.reEnterPasswordScreen.continueButton);
    }

    verifyCmsTile(cmsVisible) {
        Actions.isVisible(this.reEnterPasswordScreen.cmsTile).should.equal(cmsVisible);
    }

    verifyFscsTile(fscsVisible) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.reEnterPasswordScreen.fscsTile);
        Actions.isVisible(this.reEnterPasswordScreen.fscsTile).should.equal(fscsVisible);
    }

    shouldNotSeeReEnterPasswordScreen() {
        Actions.isVisible(this.reEnterPasswordScreen.title).should.equal(false);
    }
}

module.exports = ReEnterPasswordPage;
