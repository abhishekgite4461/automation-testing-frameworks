require('chai').should();
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');
const UnAuthPage = require('./common/unAuth.page');

class EnrolmentCardReaderIdentifyPage extends UnAuthPage {
    /**
     * @returns {CardReaderIdentifyScreen}
     */
    get cardReaderIdentifyScreen() {
        return this.getNativeScreen('enrolmentCardReaderIdentify.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.cardReaderIdentifyScreen.title);
    }

    waitForCardReaderAuthenticationPageLoad() {
        Actions.waitForVisible(this.cardReaderIdentifyScreen.paymentCardReaderAuthenticationTitle);
    }

    selectContinueButton() {
        Actions.swipeByPercentUntilVisible(this.cardReaderIdentifyScreen.continue,
            20, 80, 20, 60, 100);
        Actions.click(this.cardReaderIdentifyScreen.continue);
    }

    selectTroubleCardReader() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.cardReaderIdentifyScreen.troubleCardReader);
        Actions.click(this.cardReaderIdentifyScreen.troubleCardReader);
    }

    waitForDialog() {
        Actions.isVisible(this.cardReaderIdentifyScreen.dialogTitle).should.equal(true);
    }

    shouldSeeHelpPopUpContinueAndCloseButton() {
        Actions.isVisible(this.cardReaderIdentifyScreen.helpPopUpContinueButton).should.equal(true);
        Actions.isVisible(this.cardReaderIdentifyScreen.helpPopUpCancelButton).should.equal(true);
    }

    selectHelpPopContinueButton() {
        Actions.click(this.cardReaderIdentifyScreen.helpPopUpContinueButton);
    }

    selectHelpPopCloseButton() {
        Actions.click(this.cardReaderIdentifyScreen.helpPopUpCancelButton);
    }

    enterLastFiveCardDigits(cardNumber) {
        Actions.setImmediateValueAndTapOut(this.cardReaderIdentifyScreen.lastFiveCardDigitField,
            cardNumber.split('|')[0].slice(-5));
    }

    enterPasscode(passcode) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.cardReaderIdentifyScreen.passcodeField);
        Actions.setImmediateValueAndTapOut(this.cardReaderIdentifyScreen.passcodeField, passcode);
    }

    shouldSeeErrorBanner(errorType) {
        let errorBannerLocator;
        switch (errorType) {
        case 'invalid card':
            errorBannerLocator = this.cardReaderIdentifyScreen.invalidCardError;
            break;
        case 'card expired':
            errorBannerLocator = this.cardReaderIdentifyScreen.expiredCardError;
            break;
        case 'card suspended':
            errorBannerLocator = this.cardReaderIdentifyScreen.suspendedCardError;
            break;
        case 'card expired new card issued':
            errorBannerLocator = this.cardReaderIdentifyScreen.oldCardExpiredError;
            break;
        case 'card already exist':
            errorBannerLocator = this.cardReaderIdentifyScreen.cardAlreadyExistError;
            break;
        case 'incorrect passcode':
            errorBannerLocator = this.cardReaderIdentifyScreen.incorrectPasscodeError;
            break;
        default:
            throw new Error(`Unknown error type: ${errorType}`);
        }
        Actions.isExisting(errorBannerLocator).should.equal(true);
    }

    verifyContinueButtonEnableVisibility(type) {
        if (type === 'enabled') {
            Actions.isEnabled(this.cardReaderIdentifyScreen.continueButton).should.equal(true);
        } else {
            Actions.isEnabled(this.cardReaderIdentifyScreen.continueButton).should.equal(false);
        }
    }

    selectCloseButton() {
        Actions.click(this.cardReaderIdentifyScreen.cancelButton);
    }

    closeInvalidCardBanner() {
        Actions.waitForVisible(this.cardReaderIdentifyScreen.closeErrorBanner);
        Actions.click(this.cardReaderIdentifyScreen.closeErrorBanner);
    }
}

module.exports = EnrolmentCardReaderIdentifyPage;
