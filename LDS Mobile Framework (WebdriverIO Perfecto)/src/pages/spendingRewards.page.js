const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const SwipeAction = require('../enums/swipeAction.enum');
require('chai').should();

class SpendingRewardsPage extends AuthPage {
    get spendingRewardsScreen() {
        return this.getNativeScreen('spendingRewards.screen');
    }

    get spendingRewardsCongratulationPopUpScreen() {
        return this.getNativeScreen('spendingRewardsCongratulationPopUp.screen');
    }

    get viewOffersSpendingRewardsScreen() {
        return this.getNativeScreen('spendingRewardsViewOffer.screen');
    }

    get spendingRewardsWebScreen() {
        return this.getWebScreen('spendingRewardsOptIn.screen');
    }

    waitForSpendingRewardsPageLoad() {
        Actions.isVisible(this.spendingRewardsScreen.title);
    }

    waitForSpendingRewardsViewOfferPageLoad() {
        Actions.waitForVisible(this.viewOffersSpendingRewardsScreen.title);
    }

    clickYesForEverydayOfferButton() {
        Actions.swipeUntilVisible(SwipeAction.UP,
            this.spendingRewardsScreen.optInEverydayOfferButton);
        Actions.click(this.spendingRewardsScreen.optInEverydayOfferButton);
    }

    clickRegisterPopUpConfirmButton() {
        Actions.click(this.spendingRewardsCongratulationPopUpScreen.okButton);
    }

    clickDoNotRegisterButton() {
        Actions.swipeUntilVisible(SwipeAction.UP,
            this.spendingRewardsScreen.noOptInEverydayOfferButton);
        Actions.click(this.spendingRewardsScreen.noOptInEverydayOfferButton);
    }

    selectActiveOffers() {
        Actions.click(this.spendingRewardsScreen.activeOffers);
    }

    selectNewOffers() {
        Actions.click(this.spendingRewardsScreen.NewOffers);
    }

    selectExpiredOffers() {
        Actions.click(this.spendingRewardsScreen.expiredOffers);
    }

    validateFields(tableData) {
        for (let iter = 0; iter < tableData.length; iter++) {
            const optionsDisplayed = this.spendingRewardsScreen[tableData[iter].options];
            Actions.swipeByPercentUntilVisible(optionsDisplayed);
        }
    }

    validateOtions(tableData) {
        for (let iter = 0; iter < tableData.length; iter++) {
            const optionsDisplayed = this.spendingRewardsWebScreen[tableData[iter].options];
            Actions.swipeByPercentUntilVisible(optionsDisplayed);
        }
    }

    verifyNewTab() {
        Actions.isVisible(this.spendingRewardsWebScreen.newTab).should.equal(true);
    }

    verifyActiveTab() {
        Actions.isVisible(this.spendingRewardsScreen.activeTab).should.equal(true);
    }

    selectEverydayOffersLink() {
        Actions.click(this.spendingRewardsScreen.selectEverydayOffersLink);
    }

    verifyExpiredOffersPage() {
        Actions.isVisible(this.spendingRewardsScreen.expiredOffersPageTitle);
    }

    selectSettings() {
        Actions.click(this.spendingRewardsScreen.settings);
    }

    verifySettingPageTitle() {
        Actions.isVisible(this.spendingRewardsScreen.settingPageTitle);
    }

    selectFindOutMore() {
        Actions.click(this.spendingRewardsScreen.findOutMore);
    }

    verifyFindOutMorePageTitle() {
        Actions.isVisible(this.spendingRewardsScreen.findOutMorePageTitle);
    }
}

module.exports = SpendingRewardsPage;
