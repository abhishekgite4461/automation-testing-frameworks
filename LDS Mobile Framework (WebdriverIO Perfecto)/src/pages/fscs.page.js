const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class FscsPage extends AuthPage {
    /**
     * @returns {FscsScreen}
     */
    get fscsScreen() {
        return this.getNativeScreen('fscs.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.fscsScreen.title);
    }

    selectLeaveApp() {
        Actions.click(this.fscsScreen.leaveButton);
    }

    selectStayButton() {
        Actions.click(this.fscsScreen.stayButton);
    }
}

module.exports = FscsPage;
