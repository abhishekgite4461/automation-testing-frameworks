const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
require('chai').should();

class ScottishWidowsFAQpage extends AuthPage {
    /**
     * @returns {ScottishWidowsFAQScreen}
     */
    get scottishWidowsFAQScreen() {
        return this.getNativeScreen('scottishWidowsFAQ/scottishWidowsFAQ.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.scottishWidowsFAQScreen.title);
    }

    shouldSeeScottishWidowsHelpdeskNumber() {
        Actions.click(this.scottishWidowsFAQScreen.whatcanIdonextOption);
        Actions.isVisible(this.scottishWidowsFAQScreen.scottishWidowHelpDeskNumber).should.equal(true);
    }
}

module.exports = ScottishWidowsFAQpage;
