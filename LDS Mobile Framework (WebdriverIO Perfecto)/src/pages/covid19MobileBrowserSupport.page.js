require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');

class Covid19MobileBrowserSupportPage extends AuthPage {
    /**
     * @returns {MobileBrowserScreen}
     */
    get mobileBrowserScreen() {
        return this.getNativeScreen('mobileBrowser.screen');
    }

    /**
     * @returns {Covid19MobileBrowserSupportScreen}
     */
    get covid19MobileBrowserSupportScreen() {
        return this.getNativeScreen('covid19MobileBrowserSupport.screen');
    }

    verifyMortgagePaymentHolidayUrl() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.mobileBrowserScreen.browserUrl);
        }
        Actions.getText(this.mobileBrowserScreen.browserUrl)
            .should.contains(this.covid19MobileBrowserSupportScreen.mortgagePaymentHolidayUrl);
    }

    verifyCreditCardPaymentHolidayUrl() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.mobileBrowserScreen.browserUrl);
        }
        Actions.getText(this.mobileBrowserScreen.browserUrl)
            .should.contains(this.covid19MobileBrowserSupportScreen.creditcardPaymentHolidayUrl);
    }

    verifyLoanPaymentHolidayUrl() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.mobileBrowserScreen.browserUrl);
        }
        Actions.getText(this.mobileBrowserScreen.browserUrl)
            .should.contains(this.covid19MobileBrowserSupportScreen.loanPaymentHolidayUrl);
    }

    verifyOverdraftChargesUrl() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.mobileBrowserScreen.browserUrl);
        }
        Actions.getText(this.mobileBrowserScreen.browserUrl)
            .should.contains(this.covid19MobileBrowserSupportScreen.overdraftChargesUrl);
    }

    verifyTravelAndDisruptionCoverUrl() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.mobileBrowserScreen.browserUrl);
        }
        Actions.getText(this.mobileBrowserScreen.browserUrl)
            .should.contains(this.covid19MobileBrowserSupportScreen.travelAndDisruptionCoverUrl);
    }

    verifyMoreInfoAboutCoronavirusUrl() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.mobileBrowserScreen.browserUrl);
        }
        Actions.getText(this.mobileBrowserScreen.browserUrl)
            .should.contains(this.covid19MobileBrowserSupportScreen.moreInfoAboutCoronavirusUrl);
    }
}

module.exports = Covid19MobileBrowserSupportPage;
