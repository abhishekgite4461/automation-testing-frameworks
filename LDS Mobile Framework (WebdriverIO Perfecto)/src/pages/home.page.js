require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const AccountHelper = require('../utils/account.helper');
const SwipeAction = require('../enums/swipeAction.enum');
const DeviceHelper = require('../utils/device.helper');
const SavedAppDataRepository = require('../data/savedAppData.repository.js');
const SavedData = require('../enums/savedAppData.enum.js');
const AccountTileField = require('../enums/accountTileField.enum.js');
const ActionMenuPage = require('./actionMenu.page');

const actionMenuPage = new ActionMenuPage();

class HomePage extends AuthPage {
    /**
     * @returns {HomeScreen}
     */

    get homeScreen() {
        return this.getNativeScreen('home.screen');
    }

    get appShortcutsScreen() {
        return this.getNativeScreen('appShortcuts.screen');
    }

    get validationLocator() {
        return this.homeScreen.title;
    }

    get commonScreen() {
        return this.getNativeScreen('common/common.screen');
    }

    get accountStatementScreen() {
        return this.getNativeScreen('statements/accountStatement.screen');
    }

    get bottomNavigationBarScreen() {
        return this.getNativeScreen('common/bottomNavigationBar.screen');
    }

    get validateOptionAccountOverview() {
        return this.getNativeScreen('home.screen');
    }

    verifyAccountTileByIndexIsNotVisible(index) {
        Actions.waitForNotVisible(this.homeScreen.accountDetailTile(index));
    }

    verifyAccountTileByIndexIsVisible(index) {
        const currentIndex = (DeviceHelper.isAndroid() && Actions.isVisible(this.homeScreen.priorityMessageNormal))
            ? index + 1 : index;
        Actions.waitForVisible(this.homeScreen.accountDetailTile(currentIndex.toString()));
    }

    scrollToAccountTileByIndex(index) {
        Actions.swipeByPercentUntilVisible(this.homeScreen.accountDetailTile(index));
    }

    verifyTitle() {
        Actions.isVisible(this.homeScreen.title).should.equal(true);
    }

    dismissServerErrorIfPresent() {
        let count = 0;
        while (count < 2) {
            if (!DeviceHelper.isStub()) {
                Actions.setTimeout({implicit: 1000});
                if (Actions.isVisible(this.homeScreen.dismissErrorMessage)) {
                    Actions.click(this.homeScreen.dismissErrorMessage);
                    Actions.pause(1000);
                } else {
                    break;
                }
            }
            count++;
        }
        Actions.setTimeout();
        if (!DeviceHelper.isStub() && DeviceHelper.isAndroid()
            && Actions.isVisible(this.homeScreen.dismissErrorMessage)) {
            if (Actions.isVisible(this.commonScreen.backButton)) {
                Actions.click(this.commonScreen.backButton);
            }
        }
    }

    clickErrorMessage() {
        Actions.click(this.homeScreen.dismissErrorMessage);
    }

    getPrimaryAccountName() {
        this.waitForPageLoad();
        SavedAppDataRepository.setData(
            SavedData.DEFAULT_PRIMARY_ACCOUNT,
            Actions.getText(this.homeScreen.primaryAccount)
        );
    }

    validateHomePageOptions(homePageOptions, isVisible = true) {
        let actualExist = true;
        for (let i = 0; i < homePageOptions.length; i++) {
            const individualHomePageOption = this.homeScreen[homePageOptions[i].optionsInHomePage];
            if (isVisible) {
                Actions.swipeByPercentUntilVisible(individualHomePageOption, 50, 95, 50, 45, 100, 5);
            } else {
                if (!Actions.isExisting(individualHomePageOption)) {
                    Actions.swipePage(SwipeAction.UP);
                    if (!Actions.isExisting(individualHomePageOption)) {
                        actualExist = false;
                    }
                }
                actualExist.should.equal(isVisible);
                Actions.swipePage(SwipeAction.DOWN);
            }
        }
    }

    navigateAndValidateOptionInAccountTile(accountHeaderName, allAccountField, accountKeyName) {
        const indices = this.navigateToAccount(accountHeaderName, accountKeyName);
        try {
            if (DeviceHelper.isAndroid()) {
                Actions.swipeByPercentUntilVisible(this.homeScreen.accountTileFooter(indices), 80, 80, 80, 40, 300, 6);
            }
        } catch (e) {
            if (/^N\/A|Not Available/i.test(Actions.getText(this.homeScreen.balanceField)) && DeviceHelper.isEnv()) {
                throw new Error('ENVIRONMENT_FAILURE');
            }
        }
        for (let i = 0; i < allAccountField.length; i++) {
            this.validateOptionInAccountTile(
                indices, accountHeaderName,
                allAccountField[i].accountFields
            );
        }
        SavedAppDataRepository.setData(
            SavedData.ACCOUNT_ITERATOR_E2E,
            indices
        );
    }

    validateBalanceTypeInAccountTile(accountHeaderName, balanceType) {
        const indices = this.navigateToAccount(accountHeaderName);
        this.validateOptionInAccountTile(indices, accountHeaderName, balanceType);
    }

    navigateAndValidateOptionInAccountTileRecursively(accountHeaderName, allAccountField) {
        const indices = this.navigateToAccount(accountHeaderName);
        if (DeviceHelper.isAndroid()) {
            Actions.swipeByPercentUntilVisible(this.homeScreen.accountTileFooter(indices), 80, 80, 40, 65, 300, 1);
        }
        for (let i = 0; i < allAccountField.length; i++) {
            this.validateOptionInAccountTile(
                indices, accountHeaderName,
                allAccountField[i].accountFields
            );
        }
        SavedAppDataRepository.setData(
            SavedData.ACCOUNT_ITERATOR_E2E,
            indices
        );
    }

    navigateToAccount(accountName, accountKeyName = '') {
        Actions.validateUndefinedArgs(accountName);
        this.swipeToAccount(accountName, accountKeyName);
        const accName = Actions.modifyAccountName(accountName, DeviceHelper.isIOS());
        let iteratorLoc;
        if (accountName === 'Workplace Pension' && DeviceHelper.isIOS()) {
            iteratorLoc = this.homeScreen.pensionAccountIterator(accountName);
        } else {
            iteratorLoc = this.homeScreen.accountIterator(accName);
        }
        const attribute = (!DeviceHelper.isPerfecto() && DeviceHelper.isAndroid()) ? 'content-desc' : 'name';
        let attributeValue;
        if (DeviceHelper.isIOS()) {
            attributeValue = Actions.getAttribute(iteratorLoc, attribute);
        } else {
            attributeValue = Actions.getAttribute(iteratorLoc, attribute)
                .split('accountTile')[1];
        }
        return attributeValue;
    }

    chooseAccount(accountName, accountKeyName = '') {
        this.waitForPageLoad();
        this.swipeToAccount(accountName, accountKeyName);
        this.selectAccount(accountName, accountKeyName);
    }

    verifyNoScottishWidowsAccountTiles(accountName) {
        Actions.isVisible(DeviceHelper.isIOS() ? this.homeScreen.pensionAccount : this.homeScreen.account(accountName))
            .should.equal(false);
    }

    selectAccount(accName, accountKeyName = '') {
        const accountName = Actions.modifyAccountName(accName);
        const noActionMenuBosCurrentAccounts = ['Current', 'Classic', 'Reward Current Account', 'Islamic Current Account', 'Workplace Pension', 'Formal Loan', 'Classic Account', 'Unused OD Current Account', 'No OD Current Account', 'Used OD Current Account'];
        let accountLocator;
        if ((['Repossessed mortgage', 'Offset mortgage', 'Personal Loan (CBS)', 'Home Insurance', 'Workplace Pension', 'Formal Loan'].includes(accName)
            || ['rePossessedMortgage', 'offSetMortgage'].includes(accountKeyName)
            || (noActionMenuBosCurrentAccounts.includes(accName) && browser.capabilities.brand === 'BOS')) && DeviceHelper.isIOS()) {
            accountLocator = this.homeScreen.noActionMenuAccounts(accountName);
        } else if (accName === 'balance after pending is negative with arranged overdraft') {
            accountLocator = this.homeScreen.accountWithNegativeOverdraft;
        } else {
            accountLocator = this.homeScreen.account(accountName);
        }
        Actions.click(accountLocator);
    }

    static isAccountVisible(element, accountLocator) {
        let accountName;
        if (DeviceHelper.isAndroid()) {
            accountName = element.getText();
        } else {
            accountName = element.getAttribute('label');
        }

        // returns as Account name. <name of the account> for iOS

        if (DeviceHelper.isIOS() && accountName.includes('.')) {
            accountName = accountName.split('.')[1];
        }
        return accountName.trim().toLowerCase() === accountLocator.trim().toLowerCase();
    }

    // swiping vertical on home
    swipeToAccount(accName, accountKeyName = '') {
        const accountName = Actions.modifyAccountName(accName);
        const noActionMenuAccounts = ['Repossessed mortgage', 'Offset mortgage', 'Personal Loan (CBS)', 'Home Insurance', 'Workplace Pension', 'Formal Loan'];
        const noActionMenuBosCurrentAccounts = ['Current', 'Classic', 'Reward Current Account', 'Islamic Current Account', 'Workplace Pension', 'Formal Loan', 'Classic Account'];
        let loc;

        if ((DeviceHelper.isIOS()) && (noActionMenuAccounts.includes(accName)
        || ['rePossessedMortgage', 'offSetMortgage'].includes(accountKeyName))) {
            loc = this.homeScreen.noActionMenuAccounts(accountName);
        } else if ((DeviceHelper.isIOS()) && (noActionMenuBosCurrentAccounts.includes(accName) && browser.capabilities.brand === 'BOS')) {
            if (Actions.isVisible(this.homeScreen.noActionMenuBOSAccounts(accountName))) {
                loc = this.homeScreen.noActionMenuBOSAccounts(accountName);
            } else {
                loc = this.homeScreen.noActionMenuAccounts(accountName);
            }
        } else {
            loc = this.homeScreen.account(accountName);
        }
        try {
            if (DeviceHelper.isPerfecto() && DeviceHelper.isAndroid()) {
                // Android swipe is not smooth on perfecto devices
                Actions.swipeByPercentUntilVisible(
                    loc, 80, 90, 80, 10, 300, 23,
                    this.homeScreen.homeContainer
                );
            } else {
                Actions.swipeByPercentUntilVisible(loc, 70, 80, 10, 10, 200, 18);
            }
        } catch (e) {
            if (DeviceHelper.isEnv()) {
                throw Error('TEST_DATA_ERROR');
            }
        }
    }

    // swiping horizontal after selecting accounts from home
    swipeToAccountTile(accountName, currentIterator = 0) {
        const totalAccounts = 31;
        let iterator = currentIterator;
        let leadCount = 0;
        let AccountNameElement = this.homeScreen.accountNameElement(iterator);
        let isAccountVisibleVar = HomePage.isAccountVisible(AccountNameElement, accountName);
        while (!isAccountVisibleVar && iterator < totalAccounts - 1) {
            Actions.swipeByPercentage(80, 25, 20, 25, 100);
            iterator++;
            while (!Actions.isVisible(this.homeScreen.firstClickableAccountTile(iterator))) {
                leadCount++;
                if (leadCount > 3) {
                    throw new Error('Too many Leads configured');
                }
                Actions.swipeByPercentage(80, 25, 20, 25, 100);
                iterator++;
            }
            AccountNameElement = this.homeScreen.accountNameElement(iterator);
            isAccountVisibleVar = HomePage.isAccountVisible(AccountNameElement, accountName);
        }
        return iterator;
    }

    validateOptionInAccountTile(index, accName, accountField) {
        switch (accountField) {
        case 'accountNumber': {
            let accountNumber = Actions.getAttributeText(this.homeScreen.accountNumber(index));
            accountNumber = DeviceHelper.isIOS() ? accountNumber.match(AccountTileField
                .BANK_ACCOUNT_NUMBER.value)[0] : accountNumber;
            AccountHelper.isValidAccountNumber(accountNumber.trim());
            break;
        }

        case 'sortCode': {
            let sortCode = Actions.getAttributeText(this.homeScreen.sortCode(index));
            sortCode = DeviceHelper.isIOS() ? sortCode.match(AccountTileField
                .SORT_CODE_NUMBER.value)[0] : sortCode;
            AccountHelper.isValidSortCode(sortCode);
            break;
        }

        case 'accountBalance': {
            let accountBalance = Actions.getAttributeText(this.homeScreen.accountBalance(index));
            if (accountBalance.includes('N/A')) {
                throw Error('Not_Available_Balance');
            }
            accountBalance = DeviceHelper.isIOS() ? accountBalance.match(AccountTileField
                .ACCOUNT_ACCOUNT_INFO_BALANCE.value)[0] : accountBalance;
            AccountHelper.isValidAmount(accountBalance);
            break;
        }

        case 'negativeBalance': {
            let accountBalance = Actions.getAttributeText(this.homeScreen.accountBalance(index));
            accountBalance = DeviceHelper.isIOS() ? accountBalance.match(AccountTileField
                .ACCOUNT_ACCOUNT_INFO_BALANCE.value)[0] : accountBalance;
            AccountHelper.isNegativeAmount(accountBalance);
            break;
        }

        case 'nilBalance': {
            let accountBalance = Actions.getAttributeText(this.homeScreen.accountBalance(index));
            accountBalance = DeviceHelper.isIOS() ? accountBalance.match(AccountTileField
                .NIL_BALANCE.value)[0] : accountBalance;
            AccountHelper.isNilAmount(accountBalance);
            break;
        }

        case 'NABalance': {
            if (DeviceHelper.isAndroid()) {
                Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.accountBalance(index));
            }
            let accountBalance = Actions.getAttributeText(this.homeScreen.accountBalance(index));
            accountBalance = DeviceHelper.isIOS() ? accountBalance.match(AccountTileField
                .NA_BALANCE.value)[0] : accountBalance;
            AccountHelper.isNotApplicableAmount(accountBalance);
            break;
        }

        case 'mortgageAccountBalance': {
            let accountBalance = Actions.getAttributeText(this.homeScreen.accountBalance(index));
            accountBalance = DeviceHelper.isIOS() ? accountBalance.match(AccountTileField
                .MORTGAGE_INFO_BALANCE.value)[0] : accountBalance;
            AccountHelper.isValidAmount(accountBalance);
            break;
        }

        case 'accountInfoBalance': {
            // TODO DPL-3269 Remove swipe method once this issue is resolved
            if (DeviceHelper.isRNGA()) {
                Actions.swipeByPercentUntilVisible(this.homeScreen.accountInfoBalance(index), 80, 80, 80, 60, 50, 2);
            }
            let availableBalance = Actions.getAttributeText(this.homeScreen.accountInfoBalance(index));
            availableBalance = DeviceHelper.isIOS() ? availableBalance.match(AccountTileField
                .ACCOUNT_ACCOUNT_INFO_BALANCE.value)[1] : availableBalance;
            AccountHelper.isValidAmount(availableBalance);
            break;
        }

        case 'interestRate': {
            let interestRate = Actions.getAttributeText(this.homeScreen.interestRate(index));
            interestRate = DeviceHelper.isIOS() ? interestRate.split('Interest rate.,')[1] : interestRate;
            AccountHelper.isValidInterestRate(interestRate);
            break;
        }

        case 'creditCardAccountNumber': {
            let creditCardAccountNumber = Actions.getAttributeText(this.homeScreen.creditCardAccountNumber(index));
            creditCardAccountNumber = DeviceHelper.isIOS() ? creditCardAccountNumber.match(AccountTileField
                .LOAN_ACCOUNT_NUMBER.value)[0] : creditCardAccountNumber;
            AccountHelper.isValidCreditCardAccountNumber(creditCardAccountNumber);
            break;
        }

        case 'creditCardAccountBalance': {
            let accountBalance = Actions.getAttributeText(this.homeScreen.accountBalance(index));
            if (accountBalance.includes('N/A')) {
                throw Error('Not_Available_Balance');
            }
            accountBalance = DeviceHelper.isIOS() ? accountBalance.match(AccountTileField
                .AVAILABLE_CREDIT_CARD_LIMIT.value)[0] : accountBalance;
            AccountHelper.isValidCreditCardAvailableBalance(accountBalance);
            break;
        }

        case 'availableCredit': {
            if (DeviceHelper.isAndroid()) {
                // TODO DPL-3269 Remove swipe method once this issue is resolved
                Actions.swipeByPercentUntilVisible(this.homeScreen.availableCredit(index), 80, 80, 80, 60, 50, 2);
            }
            let availableCredit = Actions.getAttributeText(this.homeScreen.availableCredit(index));
            availableCredit = DeviceHelper.isIOS() ? availableCredit.match(AccountTileField
                .AVAILABLE_CREDIT_CARD_LIMIT.value)[1] : availableCredit;
            AccountHelper.isValidCreditCardAvailableBalance(availableCredit);
            break;
        }

        case 'negativeAvailableCredit': {
            if (DeviceHelper.isAndroid()) {
                Actions.swipeByPercentUntilVisible(this.homeScreen.availableCredit(index), 80, 80, 80, 60, 50, 2);
            }
            let availableCredit = Actions.getAttributeText(this.homeScreen.availableCredit(index));
            availableCredit = DeviceHelper.isIOS() ? availableCredit.match(AccountTileField
                .AVAILABLE_CREDIT_CARD_LIMIT.value)[1] : availableCredit;
            AccountHelper.isnegativeCreditCardAvailableBalance(availableCredit);
            break;
        }

        case 'accountTileLogo': {
            if (DeviceHelper.isAndroid()) {
                Actions.isExisting(this.homeScreen.accountTileLogo(index)).should.equal(true);
            } else {
                let accountTileLogoDetails = Actions.getAttributeText(this.homeScreen.accountTileLogo(index));
                accountTileLogoDetails = accountTileLogoDetails.match(AccountTileField.BANK_LOGO.value)[0];
                AccountHelper.isLogoDetailsDisplayed(accountTileLogoDetails);
            }
            break;
        }

        case 'overdueAmount': {
            const overDueAmt = Actions.getAttributeText(this.homeScreen.overdueAmount(index));
            AccountHelper.isValidAmount(overDueAmt);
            break;
        }

        case 'overdueAmountForAdditionalCredit': {
            let overDueAmtForAddCredit = Actions.getAttributeText(this.homeScreen.overdueAmount(index));
            overDueAmtForAddCredit = DeviceHelper.isIOS() ? overDueAmtForAddCredit.split('Overdue amount.,')[1] : overDueAmtForAddCredit;
            AccountHelper.isNotApplicableAmount(overDueAmtForAddCredit);
            break;
        }

        case 'mortgageAccountNumber':
            AccountHelper.isValidMortgageAccountNumber(
                Actions.getAttributeText(this.homeScreen.mortgageAccountNumber(index))
            );
            break;

        case 'mortgageMonthlyPayment': {
            let mortgageMonthlyPayment = Actions.getAttributeText(this.homeScreen.mortgageMonthlyPayment(index));
            mortgageMonthlyPayment = DeviceHelper.isIOS() ? mortgageMonthlyPayment.split('monthly payment.,')[1].split(',Balance')[0] : mortgageMonthlyPayment;
            AccountHelper.isValidAmount(mortgageMonthlyPayment);
            break;
        }

        case 'mortgageBalanceOnDate': {
            let balanceOnDate = Actions.getAttributeText(this.homeScreen.mortgageBalanceOnDate(index));
            balanceOnDate = DeviceHelper.isIOS() ? balanceOnDate.split('Balance as of.,')[1].split(',')[0] : balanceOnDate;
            AccountHelper.isValidDate(balanceOnDate);
            break;
        }

        case 'loanAccountNumber': {
            let loanReferenceNumber = Actions.getAttributeText(this.homeScreen.loanAccountNumber(index));
            loanReferenceNumber = DeviceHelper.isIOS() ? loanReferenceNumber.match(
                AccountTileField.LOAN_ACCOUNT_NUMBER.value
            ) : loanReferenceNumber;
            AccountHelper.isValidLoanAccountNumber(
                loanReferenceNumber
            );
            break;
        }
        case 'loanCurrentBalance': {
            let loanCurrBal = Actions.getAttributeText(this.homeScreen.loanCurrentBalance(index));
            loanCurrBal = DeviceHelper.isIOS() ? loanCurrBal.match(
                AccountTileField.CURRENT_BALANCE.value
            ) : loanCurrBal;
            AccountHelper.isValidAmount(loanCurrBal);
            break;
        }
        case 'cbsLoanAccountNumber':
            AccountHelper.isValidAccountNumber(Actions.getAttributeText(this.homeScreen.cbsLoanAccountNumber(index)));
            break;

        case 'cbsCurrentBalance': {
            let cbsCurrentBalanceAmt = Actions.getAttributeText(this.homeScreen.cbsCurrentBalance(index));
            cbsCurrentBalanceAmt = DeviceHelper.isIOS() ? cbsCurrentBalanceAmt.split('Current balance. ')[1] : cbsCurrentBalanceAmt;
            AccountHelper.isValidAmount(cbsCurrentBalanceAmt, 'loan');
            break;
        }

        case 'cbsLoanSortCode':
            AccountHelper.isValidSortCode(Actions.getAttributeText(this.homeScreen.cbsLoanSortCode(index)));
            break;

        case 'cbsLoanCurrentBalance':
            AccountHelper.isValidAmount(Actions.getAttributeText(this.homeScreen.cbsLoanCurrentBalance(index)), 'loan');
            break;

        case 'maturityDate': {
            let maturityDate = Actions.getAttributeText(this.homeScreen.maturityDate(index));
            maturityDate = DeviceHelper.isIOS() ? maturityDate.split('Maturity date.,')[1].split(',')[0] : maturityDate;
            AccountHelper.isValidDate(maturityDate);
            break;
        }

        case 'businessName': {
            AccountHelper.isValidTextField(Actions.getAttributeText(this.homeScreen.businessName(index)));
            break;
        }

        case 'progressBar': {
            Actions.isExisting(this.homeScreen.creditCardAccountTileProgressBar(index)).should.equal(true);
            break;
        }

        case 'progressAvailableCredit': {
            AccountHelper.isValidTextField(
                Actions.getAttributeText(this.homeScreen.accountTileProgressAvailableCreditLabel(index))
            );
            AccountHelper.isValidAmount(
                Actions.getAttributeText(this.homeScreen.accountTileProgressAvailableCreditValue(index))
            );
            break;
        }

        case 'progressCreditLimit': {
            AccountHelper.isValidTextField(
                Actions.getAttributeText(this.homeScreen.accountTileProgressCreditLimitLabel(index))
            );
            AccountHelper.isValidAmount(
                Actions.getAttributeText(this.homeScreen.accountTileProgressCreditLimitValue(index))
            );
            break;
        }

        case 'minimumPayment': {
            const minimumPayment = Actions.getAttributeText(this.homeScreen.accountTileMinimumPaymentValue(index));
            AccountHelper.isValidAmount(minimumPayment);
            break;
        }

        case 'dueDate': {
            const dueDate = Actions.getAttributeText(this.homeScreen.accountTilePaymentDueDateValue(index));
            AccountHelper.isValidDate(dueDate);
            break;
        }

        case 'statementBalance': {
            const statementBalance = Actions.getAttributeText(this.homeScreen.accountTileStatementBalanceValue(index));
            AccountHelper.isValidAmount(statementBalance);
            break;
        }

        case 'overDraftLimit': {
            if (DeviceHelper.isAndroid()) {
                AccountHelper.isValidTextField(Actions.getAttributeText(this.homeScreen.accountTileOverdraftLimitLabel(index)).replace(':', ''));
                AccountHelper.isValidAmount(
                    Actions.getAttributeText(this.homeScreen.accountTileOverdraftLimitValue(index))
                );
            } else {
                const overDraftLimitDetails = Actions
                    .getAttributeText(this.homeScreen.accountTileOverdraftLimitDetails(index));
                const overDraftLimitLabel = overDraftLimitDetails
                    .match(AccountTileField.OVERDRAFT_LIMIT_LABEL.value)[0];
                const overDraftLimitValue = overDraftLimitDetails
                    .match(AccountTileField.OVERDRAFT_LIMIT_VALUE.value)[0];
                AccountHelper.isOverdraftLimitLabelDisplayed(overDraftLimitLabel);
                AccountHelper.isValidAmount(overDraftLimitValue);
            }
            break;
        }

        case 'RemainingOverdraft': {
            if (DeviceHelper.isAndroid()) {
                AccountHelper.isValidTextField(Actions
                    .getAttributeText(this.homeScreen.accountTileRemainingOverdraftLabel(index)).replace(':', ''));
                AccountHelper
                    .isValidAmount(Actions.getAttributeText(this.homeScreen.accountTileRemainingOverdraftValue(index)));
            } else {
                const remainingOverdraftDetails = Actions
                    .getAttributeText(this.homeScreen.accountTileRemainingOverdraftDetails(index));
                const remainingOverDraftLabel = remainingOverdraftDetails
                    .match(AccountTileField.REMAINING_OVERDRAFT_LABEL.value)[0];
                const remainingOverDraftValue = remainingOverdraftDetails
                    .match(AccountTileField.REMAINING_OVERDRAFT_VALUE.value)[0];
                AccountHelper.isRemainingOverdraftLabelDisplayed(remainingOverDraftLabel);
                AccountHelper.isValidRemainingOverdraftValue(remainingOverDraftValue);
            }
            break;
        }

        case 'thirtyTwoDayNoticeAccountNumber':
            AccountHelper.isValidThirtyTwoDayNoticeAccountNumber(
                Actions.getAttributeText(this.homeScreen.thirtyTwoDayNoticeAccountNumber(index))
            );
            break;

        case 'upcomingPayments': {
            Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.upcomingPaymentsQuickLink(index));
            const upcomingPaymentQuickLinkText = DeviceHelper.isIOS() ? 'Upcoming payments' : 'Upcoming\npayments';
            Actions.getAttributeText(this.homeScreen.upcomingPaymentsQuickLink(index))
                .should.contains(upcomingPaymentQuickLinkText);
            break;
        }

        case 'freezeCard': {
            const freezeCardQuickLinkText = DeviceHelper.isIOS() ? 'Freeze card' : 'Freeze\ncard';
            Actions.getAttributeText(this.homeScreen.freezeCardQuickLink(index)).should.equal(freezeCardQuickLinkText);
            break;
        }

        case 'notifications': {
            Actions.getAttributeText(this.homeScreen.notificationsQuickLink(index)).should.equal('Notifications');
            break;
        }

        case 'more': {
            Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.moreQuickLink(index));
            const moreQuickLinkText = DeviceHelper.isIOS() ? 'View more options for this account' : 'More';
            Actions.getAttributeText(this.homeScreen.moreQuickLink(index)).should.equal(moreQuickLinkText);
            break;
        }

        case 'payAndTransfer': {
            Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.payAndTransferQuickLink(index));
            Actions.getAttributeText(this.homeScreen.payAndTransferQuickLink(index))
                .should.contains(this.homeScreen.payAndTransferQuickLinkText);
            break;
        }

        case 'manageCard': {
            Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.manageCardQuickLink(index));
            Actions.getAttributeText(this.homeScreen.manageCardQuickLink(index))
                .should.contains(this.homeScreen.manageCardQuickLinkText);
            break;
        }

        default:
            throw new Error(`Unknown accountField: ${accountField}`);
        }
    }

    selectCallUs() {
        Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.callUsTile);
        Actions.click(this.homeScreen.callUsTile);
    }

    verifyReportIssueIsVisible() {
        Actions.waitForVisible(this.homeScreen.reportIssueButton);
    }

    selectActionMenu() {
        Actions.click(this.homeScreen.actionMenu);
    }

    selectActionMenuOfSpecificAccount(account) {
        const accName = Actions.modifyAccountName(account);
        // Following If statement is added to address the addition of QuickLinks for CurrentAccount for BOS brand
        if (browser.capabilities.brand === 'BOS' && account === 'Reward Current Account') {
            this.selectQuicklinkOfSpecificAccount('More', account);
        } else if (DeviceHelper.getBrand() === 'BOS' && account === 'Islamic Current Account') {
            this.selectQuicklinkOfSpecificAccount('More', account);
        } else {
            Actions.swipeUntilVisible(
                SwipeAction.UP, Actions.firstInstanceOf(this.homeScreen.accountSpecificActionMenu(accName))
            );
            Actions.waitAndClick(Actions.firstInstanceOf(this.homeScreen.accountSpecificActionMenu(accName)));
        }
    }

    checkAccountNameVisibility(accountName) {
        if (DeviceHelper.isAndroid()) {
            (Actions.getText(this.homeScreen.accountNameLabel(accountName))).should.contains(accountName);
        } else {
            const accNames = Actions
                .getAttributeText(this.homeScreen.accountNameLabelAfterRename(accountName));
            (accNames).should.contains(accountName);
        }
    }

    verifyInterestRateValues(interestRatesValues) {
        Actions.swipeByPercentUntilVisible(this.homeScreen[`${interestRatesValues}InterestRateValue`],
            70, 70, 70, 60, 500, 2);
    }

    selectRegisterForEverydayOffersTile() {
        Actions.swipeByPercentUntilVisible(
            this.homeScreen.registerForEverydayOffer, 80, 80, 40, 64, 300, 100
        );
        Actions.click(this.homeScreen.registerForEverydayOffer);
    }

    selectViewEverydayOffersTile() {
        Actions.swipeByPercentUntilVisible(
            this.homeScreen.viewEverydayOffer, 80, 90, 40, 10, 300, 23
        );
        Actions.click(this.homeScreen.viewEverydayOffer);
    }

    shouldSeeTechnicalErrorForEverydayOffer() {
        Actions.waitForVisible(this.homeScreen.technicalErrorEverydayOffer);
    }

    verifyEverydayOfferOptedIn() {
        Actions.swipeUntilVisible(
            SwipeAction.UP,
            this.homeScreen.viewEverydayOffer
        );
    }

    verifyEverydayOfferNotOptedIn() {
        Actions.swipeByPercentUntilVisible(
            this.homeScreen.registerForEverydayOffer, 80, 80, 80, 30, 300, 30
        );
    }

    verifyAccountTileFields(accountFields) {
        for (let count = 0; count < accountFields.length; count++) {
            Actions.swipeByPercentUntilVisible(this.homeScreen[accountFields[count]
                .fieldsInAccountTile], 80, 80, 20, 20, 500, 2);
        }
    }

    verifyPensionPolicyNumberIsVisible() {
        Actions.isVisible(this.homeScreen.scottishWidowsPolicyNumber).should.equal(true);
    }

    verifyPensionBalanceIsVisible() {
        Actions.isVisible(this.homeScreen.scottishWidowsBalance).should.equal(true);
    }

    noActionMenuCbsLoan(accountName) {
        Actions.waitForNotVisible(this.homeScreen.accountSpecificActionMenu(accountName));
    }

    static extractBeforeAmount(type) {
        let storedData;
        if (DeviceHelper.isIOS()) {
            if (type === 'from') {
                storedData = SavedData.TRANSFER_REMITTER_AMOUNT;
            } else if (type === 'to') {
                storedData = SavedData.TRANSFER_RECIPIENT_AMOUNT;
            }
            return SavedAppDataRepository.getData(storedData)
                .match(/([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])/g)[0].replace(/,/g, '');
        }
        if (type === 'from') {
            storedData = SavedData.TRANSFER_REMITTER_AMOUNT;
        } else if (type === 'to') {
            storedData = SavedData.TRANSFER_RECIPIENT_AMOUNT;
        }
        return SavedAppDataRepository.getData(storedData);
    }

    static extractAfterAmount(amount) {
        if (DeviceHelper.isAndroid()) {
            return amount.split('£')[1].replace(/,/g, '');
        }
        return amount.match(/([0-9]{1,3},([0-9]{3},)*[0-9]{3}|[0-9]+)(.[0-9][0-9])/g)[0].replace(/,/g, '');
    }

    static extractRemainingAllowanceBeforeTransaction(type) {
        if (type === 'from') {
            return SavedAppDataRepository.getData(SavedData.REMAINING_ALLOWANCE_FROM_ACCOUNT);
        }
        return SavedAppDataRepository.getData(SavedData.REMAINING_ALLOWANCE_TO_ACCOUNT);
    }

    static extractRemainingAllowanceAfterTransaction(amount) {
        if (DeviceHelper.isAndroid()) {
            return amount.split('£')[1].replace(/,/g, '');
        }
        return amount.split('Remaining allowance. £')[1].replace(/,/g, '');
    }

    navigateAndValidateAmountAfterTransaction(type, accountName, amount) {
        if (DeviceHelper.isEnv()) {
            this.navigateToAccount(accountName);
            Actions.click(this.homeScreen.account(Actions.modifyAccountName(accountName)));
            const amountAfterTransaction = parseFloat(HomePage.extractAfterAmount(Actions.getAttributeText(this.accountStatementScreen.accountInfoBalance, 'label')));
            const amountBeforeTransaction = parseFloat(HomePage.extractBeforeAmount(type));
            if (type === 'from') {
                String(amountAfterTransaction).should
                    .equal(String(amountBeforeTransaction - parseFloat(amount)));
            } else if (type === 'to') {
                String(amountAfterTransaction).should
                    .equal(String(amountBeforeTransaction + parseFloat(amount)));
            }
            Actions.click(this.bottomNavigationBarScreen.homeTab);
        }
    }

    navigateAndValidateNoAmountDeductionAfterTransaction(type, accountName) {
        const index = this.navigateToAccount(accountName);
        const amountAfterTransaction = parseFloat(HomePage.extractAfterAmount(Actions.getAttributeText(this.homeScreen.accountInfoBalance(index, accountName), 'label')));
        const amountBeforeTransaction = parseFloat(HomePage.extractBeforeAmount(type));
        if (type === 'from') {
            String(amountAfterTransaction).should
                .equal(String(amountBeforeTransaction));
        }
    }

    navigateAndValidateRemainingAllowanceAfterTransaction(type, accountHeaderName, amount) {
        if (!DeviceHelper.isStub()) {
            const accountName = this.homeScreen.accountNameLabel(accountHeaderName);
            const index = this.navigateToAccount(accountHeaderName);
            const remainingAllowanceAfterTransaction = parseFloat(HomePage.extractRemainingAllowanceAfterTransaction(Actions.getAttributeText(this.homeScreen.accountInfoBalance(index, accountName), 'label')));
            const remainingAllowanceBeforeTransaction = parseFloat(
                HomePage.extractRemainingAllowanceBeforeTransaction(type)
            );
            if (type === 'to') {
                String(remainingAllowanceBeforeTransaction).should
                    .equal(String(remainingAllowanceAfterTransaction + parseFloat(amount)));
            } else if (type === 'from') {
                String(remainingAllowanceBeforeTransaction).should
                    .equal(String(remainingAllowanceAfterTransaction - parseFloat(amount)));
            }
        }
    }

    dismissPendingPaymentErrorMessage() {
        Actions.isVisible(this.homeScreen.pendingPaymentErrorMessage).should.equal(true);
        Actions.click(this.homeScreen.dismissErrorMessage);
    }

    shouldNotSeePendingPaymentErrorMessage() {
        Actions.isVisible(this.homeScreen.pendingPaymentErrorMessage).should.equal(false);
    }

    shouldSeeNABalance() {
        let accountBalanceNAValue = 'Not available';
        if (DeviceHelper.isAndroid()) {
            accountBalanceNAValue = 'N/A';
        }
        let availableBalanceNAValue = 'N/A';
        if (DeviceHelper.isIOS()) {
            availableBalanceNAValue = 'Not available';
        }
        Actions.getAttributeText(this.homeScreen.firstAccountBalance, 'label').should.contains(accountBalanceNAValue);
        Actions.getAttributeText(this.homeScreen.firstAvailableBalance, 'label').should.contains(availableBalanceNAValue);
    }

    selectCurrentAccount(accountName) {
        this.swipeToAccount(accountName);
        Actions.click(this.homeScreen.actionMenu);
    }

    selectActionMenuOfThatAccount(type) {
        if (type === 'current Account With View Transactions') {
            Actions.click(this.homeScreen.actionMenuOfCurrentAccount);
        } else if (type === 'saving') {
            Actions.click(this.homeScreen.actionMenuOfSavingAccount);
        } else if (type === 'isa') {
            Actions.click(this.homeScreen.actionMenuOfisaAccount);
        } else if (type === 'credit Card Account With View Transactions') {
            Actions.click(this.homeScreen.actionMenuOfCreditCardAccount);
        } else if (type === 'personal Loan') {
            Actions.click(this.homeScreen.actionMenuOfLoanAccount);
        } else if (type === 'mortgage') {
            Actions.click(this.homeScreen.actionMenuOfMortgageAccount);
        } else if (type === 'homeInsurance') {
            Actions.click(this.homeScreen.actionMenuOfHomeInsuranceAccount);
        } else if (type === 'credit Card Account') {
            Actions.click(this.homeScreen.actionMenuOfCreditCardAccount);
        } else {
            throw new Error('Type does not match');
        }
    }

    verifyEverydayOffersTileIsNotVisible() {
        for (let i = 0; i < 23; i++) {
            Actions.isVisible(this.homeScreen.everydayOfferTile).should.equal(false);
            Actions.swipeByPercentage(80, 90, 40, 10, 300);
        }
    }

    shouldSeeAlertOfSpecificAccount(account) {
        Actions.swipeByPercentUntilVisible(this.homeScreen.alertMessage(account), 80, 80, 80, 20, 500, 2);
    }

    selectAlertNotification(account) {
        const acc = Actions.modifyAccountName(account, DeviceHelper.isIOS());
        Actions.swipeByPercentUntilVisible(this.homeScreen.alertMessage(acc), 80, 80, 20, 20, 300, 18);
        Actions.click(this.homeScreen.alertMessage(acc));
    }

    verifyAlertMessageForDormantAccount() {
        Actions.isVisible(this.homeScreen.alertMessageDormantAccount).should.equal(true);
    }

    verifyInvalidAccountError() {
        Actions.isVisible(this.homeScreen.errorMessageInvalidAccount).should.equal(true);
    }

    verifyInvalidPostcode() {
        Actions.isVisible(this.homeScreen.errorMessageInvalidPostcode).should.equal(true);
    }

    shouldSeeNonAccessibleByNgaErrorMessage() {
        Actions.isExisting(this.homeScreen.nonAccessibleNga).should.equal(true);
    }

    validatePolicyFieldsOnAccountTile(policyFields) {
        Actions.waitForVisible(this.homeScreen[policyFields[0]
            .fieldsInHomeInsuranceSummary]);
        for (let count = 0; count < policyFields.length; count++) {
            Actions.isVisible(this.homeScreen[policyFields[count]
                .fieldsInHomeInsuranceSummary]).should.equal(true);
        }
    }

    shouldSeeAccountLink(accountName) {
        Actions.isVisible(
            this.homeScreen.accountlinkLead(accountName)
        ).should.equal(true);
    }

    shouldSeeAccountName(accountName) {
        Actions.isVisible(
            this.homeScreen.accountByName(Actions.modifyAccountName(accountName))
        ).should.equal(true);
    }

    verifyMortgageFields(accountName, mortgageFields) {
        for (let count = 0; count < mortgageFields.length; count++) {
            Actions.isVisible(this.homeScreen[mortgageFields[count]
                .fieldsForMortgage](accountName)).should.equal(true);
        }
    }

    verifySwitchBusinessIconIsPresent() {
        Actions.isVisible(this.homeScreen.switchBusinessButton).should.equal(true);
    }

    verifySwitchBusinessIconIsNotPresent() {
        Actions.isVisible(this.homeScreen.switchBusinessButton).should.equal(false);
    }

    selectSwitchBusinessIcon() {
        Actions.click(this.homeScreen.switchBusinessButton);
    }

    verifyBusinessNameIsCorrectForAllAccounts(expectedBusiness) {
        const allAvailableBusinesses = Actions.getAttributeText(this.homeScreen.allBusinessName, 'label');
        for (const business in allAvailableBusinesses) {
            if (business === undefined) throw new Error('undefined parameter');
            allAvailableBusinesses[business].should.equal(expectedBusiness);
        }
    }

    shouldSeeNotificationsOnTheNotificationTile(notification) {
        if (notification === 'only 1' || notification === 'single') {
            Actions.isVisible(this.homeScreen.singleNotification).should.equal(true);
        } else {
            Actions.isVisible(this.homeScreen.multipleNotification).should.equal(true);
        }
    }

    selectTheMultipleNotificationCentrePlacement() {
        Actions.click(this.homeScreen.multipleNotification);
    }

    shouldSeeAllAvailableNotification() {
        Actions.isVisible(this.homeScreen.multipleNotificationView).should.equal(true);
        Actions.isVisible(this.homeScreen.notificationCentreCloseButton).should.equal(true);
    }

    selectOneOfTheAvailableNotifications() {
        Actions.click(this.homeScreen.multipleNotificationCentreHolder);
    }

    selectTheSingleNotificationCentrePlacement() {
        Actions.click(this.homeScreen.singleNotification);
    }

    shouldBeOnTheNotificationJourney() {
        Actions.waitForVisible(this.homeScreen.notificationWebViewJourney);
    }

    selectCloseButton() {
        Actions.click(this.homeScreen.notificationCentreCloseButton);
    }

    shouldNotSeeTheNotificationCentrePlacement() {
        Actions.isVisible(this.homeScreen.singleNotification).should.equal(false);
    }

    validateAlertRepossessedMortgage(type) {
        if (type === 'rePossessedMortgage') {
            Actions.isVisible(this.homeScreen.alertMessage).should.equal(true);
        } else {
            Actions.isVisible(this.homeScreen.alertMessage).should.equal(false);
        }
    }

    selectPayAndTransfer() {
        Actions.iOSObjectTreeOptimizationStart();
        this.selectTab('payAndTransfer');
        Actions.iOSObjectTreeOptimizationStop();
    }

    shouldSeeCantApplyErrorMessage() {
        if (DeviceHelper.isAndroid()) {
            Actions.isVisible(this.homeScreen.errorMessageFeatureUnavailable).should.equal(true);
        } else {
            Actions.isVisible(this.homeScreen.cantApplyErrorMessage).should.equal(true);
        }
    }

    shouldSeeFeatureNotAvailable() {
        Actions.isVisible(this.homeScreen.cantApplyErrorMessage).should.equal(true);
    }

    shouldSeeCantMakePaymentErrorMessage() {
        Actions.isVisible(this.homeScreen.cantMakePaymentErrorMessage).should.equal(true);
    }

    static pressAndroidBackKey() {
        Actions.pressAndroidBackKey();
    }

    navigateToHomePage() {
        Actions.click(this.bottomNavigationBarScreen.homeTab);
    }

    verifyHomeScreenErrorBanner() {
        Actions.waitForVisible(this.homeScreen.homeScreenErrorBanner);
    }

    verifyNoTabMenu() {
        Actions.isVisible(this.isBottomNavBarVisible()).should.equal(false);
    }

    externalProviderBankRemoved(bankName, accountType) {
        const accTypeList = accountType.includes(',') ? accountType.split(',') : accountType;
        for (let i = 1; i <= 5; i++) {
            for (let j = 0; j < accTypeList.length; j++) {
                if (accTypeList[j] === 'Error' || accTypeList[j] === 'Expired') {
                    Actions.isVisible(this.homeScreen.externalBankAccountErrorTile(bankName))
                        .should.equal(false);
                } else {
                    Actions.isVisible(this.homeScreen.externalProviderAccountTile(bankName, accTypeList[j]))
                        .should.equal(false);
                }
            }
            Actions.swipeByPercentage(80, 80, 60, 60, 50);
        }
    }

    selectExternalAccountTile(bankName, accountType) {
        Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.externalProviderAccountTile(bankName, accountType));
        Actions.click(this.homeScreen.externalProviderAccountTile(bankName, accountType));
    }

    navigateToHomeFromNotification() {
        // TODO MORE-2228
        if (DeviceHelper.isIOS()) {
            Actions.click(this.commonScreen.backButton);
        } else {
            Actions.click(this.bottomNavigationBarScreen.homeTab);
        }
    }

    navigateToExternalBankAccountTile(bankName, accountType) {
        Actions.swipeByPercentUntilVisible(this.homeScreen.externalProviderAccountTile(bankName, accountType),
            80, 80, 60, 60, 100, 10);
    }

    navigateToExternalBankAccountErrorTile(bankName) {
        Actions.swipeByPercentUntilVisible(this.homeScreen.externalBankAccountErrorTile(bankName),
            80, 80, 60, 60, 100, 10);
    }

    shouldSeeRenewConsentButtonOnExternalTile(bankName, isShown) {
        if (isShown) {
            Actions.swipeUntilVisible(SwipeAction.UP, this.homeScreen.renewAccountSharingButtonOnErrorTile(bankName));
        }
        Actions.isVisible(this.homeScreen.renewAccountSharingButtonOnErrorTile(bankName))
            .should.equals(isShown);
    }

    verifyPriorityMessageVisibility(shouldSeePriorityMessage) {
        Actions.isVisible(this.homeScreen.priorityMessageNormal).should.equal(shouldSeePriorityMessage);
    }

    shouldSeeDismissPriority() {
        Actions.isVisible(this.homeScreen.priorityDismissMessage).should.equal(true);
    }

    selectDismissPriority() {
        Actions.click(this.homeScreen.priorityDismissMessage);
    }

    shouldSeeUpdateNow() {
        Actions.isVisible(this.homeScreen.updateNowMessage).should.equal(true);
    }

    shouldSeeGroupBPriorityMessage() {
        Actions.isVisible(this.homeScreen.priorityMessageCallAction).should.equal(true);
    }

    shouldNotSeeCallActionPriority() {
        Actions.isVisible(this.homeScreen.priorityMessageCallAction).should.equal(false);
    }

    selectButtonOnExternalBankAccountTile(buttonName, bankName, accountType) {
        switch (buttonName.toLowerCase()) {
        case 'action menu':
            Actions.swipeByPercentUntilVisible(this.homeScreen.externalBankActionMenuButton(bankName, accountType),
                80, 80, 60, 60, 100, 10);
            Actions.click(this.homeScreen.externalBankActionMenuButton(bankName, accountType));
            actionMenuPage.waitForExternalAccountActionMenuPageLoad();
            break;
        case 'renew account sharing':
            Actions.swipeByPercentUntilVisible(this.homeScreen.renewAccountSharingButtonOnErrorTile(bankName),
                80, 80, 60, 60, 100, 10);
            Actions.click(this.homeScreen.renewAccountSharingButtonOnErrorTile(bankName));
            break;
        case 'settings':
            Actions.swipeByPercentUntilVisible(this.homeScreen.settingsButtonOnErrorTile(bankName),
                80, 80, 60, 60, 100, 10);
            Actions.click(this.homeScreen.settingsButtonOnErrorTile(bankName));
            break;
        default:
            throw new Error(`Given button name ${buttonName} not present`);
        }
    }

    getFirstName() {
        const homeHeaderToolBarText = Actions.getText(this.homeScreen.headerToolbar).split('.')[0];
        const firstName = homeHeaderToolBarText.split(' ').pop();
        SavedAppDataRepository.setData(SavedData.FIRST_NAME, firstName);
    }

    verifyGreetingMessages() {
        const time = new Date().getHours();

        const homeHeaderToolBarText = DeviceHelper.isIOS() ? Actions.getText(this.homeScreen.headerToolbar).split('.')[1]
            : Actions.getText(this.homeScreen.headerToolbar).split('.')[0];

        const greetMessage = homeHeaderToolBarText.match(AccountTileField.GREETING_MESSAGE.value)[0].trim();

        if (time >= 5 && time < 12) {
            greetMessage.should.equal('Good Morning');
        } else if (time >= 12 && time < 16) {
            greetMessage.should.equal('Good Afternoon');
        } else if (time >= 17 && time < 21) {
            greetMessage.should.equal('Good Evening');
        } else {
            greetMessage.should.equal('Welcome');
        }
    }

    shouldSeeWelcomeMessage() {
        Actions.isVisible(this.homeScreen.welcomeMessage).should.equal(true);
    }

    shouldSeeHomeMessageInSecondLogin() {
        Actions.isVisible(this.homeScreen.welcomeMessage).should.equal(false);
        Actions.isVisible(this.homeScreen.homeMessage).should.equal(true);
    }

    shouldSeeVoiceIdScreen() {
        Actions.isVisible(this.homeScreen.voiceIDScreen).should.equal(true);
    }

    selectAsmVoiceIdTile() {
        Actions.click(this.homeScreen.asmVoiceIDScreen);
    }

    validateVoiceIdScreen() {
        Actions.isVisible(!DeviceHelper.isIOS() ? this.homeScreen.headerToolbar
            : this.homeScreen.voiceIDScreen).should.equal(true);
        Actions.isVisible(this.homeScreen.voiceIDHeading);
        Actions.isVisible(this.homeScreen.registerVoiceIDButton);
        Actions.swipeByPercentUntilVisible(!DeviceHelper.isIOS()
            ? this.homeScreen.title : this.homeScreen.notNowButton);
    }

    selectRegisterVoiceID() {
        Actions.swipeByPercentUntilVisible(this.homeScreen.registerVoiceIDButton);
        Actions.click(this.homeScreen.registerVoiceIDButton);
    }

    selectBottomNavOrNotNow() {
        if (DeviceHelper.isAndroid()) {
            Actions.click(this.homeScreen.title);
        } else {
            Actions.swipeByPercentUntilVisible(this.homeScreen.notNowButton);
            Actions.click(this.homeScreen.notNowButton);
        }
    }

    shouldNotSeePriorityAfterScrolldown() {
        Actions.swipeByPercentage(80, 80, 20, 20, 100);
        this.verifyPriorityMessageVisibility(false);
    }

    shouldSeePriorityAfterScrollUp() {
        Actions.swipeByPercentage(20, 20, 80, 80, 100);
        this.verifyPriorityMessageVisibility(true);
    }

    isMoreThanOneAccount() {
        const totalAccounts = DeviceHelper.isAndroid()
            ? Actions.getElementsListAttributeText(this.homeScreen.accountContainer).length
            : Actions.getElementsListAttributeText(this.homeScreen.accountTileContainer('voiceOverView')).length;

        if (totalAccounts > 1) {
            SavedAppDataRepository.setData(SavedData.MORE_THAN_ONE_ACCOUNT, true);
        } else {
            SavedAppDataRepository.setData(SavedData.MORE_THAN_ONE_ACCOUNT, false);
        }
    }

    selectHomePage() {
        Actions.click(this.homeScreen.title);
    }

    verifyBalanceOrMoneyTransferTile() {
        if (DeviceHelper.isAndroid()) {
            Actions.swipeByPercentUntilVisible(this.homeScreen.balanceOrMoneyTransferTile, 80, 80, 20, 20, 100, 10);
            Actions.isExisting(this.homeScreen.balanceOrMoneyTransferTile).should.equal(true);
        } else {
            Actions.isExisting(this.homeScreen.balanceTransferTile).should.equal(true);
            Actions.isExisting(this.homeScreen.moneyTransferTile).should.equal(true);
        }
    }

    validateHomePage() {
        this.waitForPageLoad();
        if (!DeviceHelper.isStub()) {
            this.dismissServerErrorIfPresent();
        }
    }

    waitForPageLoad() {
        // Increasing timeout for slower devices
        Actions.waitForVisible(this.homeScreen.title, 50000);
    }

    shouldNotSeeRemainingOverdraft() {
        Actions.isVisible(this.homeScreen.accountTileRemainingOverdraftLabel).should.equal(false);
        Actions.isVisible(this.homeScreen.accountTileRemainingOverdraftValue).should.equal(false);
    }

    selectMoreQuikLink() {
        Actions.click(this.homeScreen.moreQuickLink(0));
    }

    shouldSeeOptionsInAccountOverviewScreen(accountOverviewScreenOptions) {
        for (let i = 0; i < accountOverviewScreenOptions.length; i++) {
            const optionsDisplayed = this.validateOptionAccountOverview[
                accountOverviewScreenOptions[i].optionsInAccountOverviewScreen];
            Actions.isVisible(optionsDisplayed).should.equal(true);
        }
    }

    selectQuicklinkOfSpecificAccount(quickLink, account) {
        const accName = Actions.modifyAccountName(account);
        switch (quickLink.toLowerCase()) {
        case 'upcoming payments':
            Actions.click(Actions.firstInstanceOf(this.homeScreen.accountSpecificUpcomingPaymentsQuicklink(accName)));
            break;
        case 'freeze card':
            Actions.click(Actions.firstInstanceOf(this.homeScreen.accountSpecificFreezeCardQuicklink(accName)));
            break;
        case 'notifications':
            Actions.click(Actions.firstInstanceOf(this.homeScreen.accountSpecificNotificationsQuicklink(accName)));
            break;
        case 'pay & transfer':
            Actions.click(Actions.firstInstanceOf(this.homeScreen.accountSpecificPayAndTransferQuicklink(accName)));
            break;
        case 'manage card':
            Actions.click(Actions.firstInstanceOf(this.homeScreen.accountSpecificManageCardQuicklink(accName)));
            break;
        case 'more':
            Actions.click(Actions.firstInstanceOf(this.homeScreen.accountSpecificMoreQuicklink(accName)));
            break;
        default:
            throw new Error(`Given quicklink ${quickLink} not present`);
        }
    }
}

module.exports = HomePage;
