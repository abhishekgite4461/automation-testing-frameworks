const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');
require('chai').should();

class MessageUsHomePage extends AuthPage {
    /**
     * @returns {MessageUsHomePageScreen}
     */
    get messageUsHomePageScreen() {
        return this.getNativeScreen('messageUsHomePage.screen');
    }

    waitForPageLoad() {
        if (DeviceHelper.isStub()) {
            Actions.waitForVisible(this.messageUsHomePageScreen.errorMessage, 2000);
            Actions.isVisible(this.messageUsHomePageScreen.errorMessage).should.equal(true);
        } else {
            Actions.waitForNotVisible(this.commonScreen.winBackOkButton, 3000);
            Actions.isVisible(!DeviceHelper.isIOS() ? this.messageUsHomePageScreen.title
                : this.messageUsHomePageScreen.typeYourQuestionField).should.equal(true);

            try {
                Actions.isVisible(this.messageUsHomePageScreen.errorMessage).should.equal(true);
            } catch (e) {
                throw Error('Mobile chat not working in this environment, check manually');
            }
        }
    }

    validateChatWindow() {
        if (DeviceHelper.isStub()) {
        // No further validation can be performed in stub
            Actions.waitForVisible(this.messageUsHomePageScreen.errorMessage);
            Actions.isVisible(this.messageUsHomePageScreen.errorMessage).should.equal(true);
        } else {
            Actions.waitForNotVisible(this.commonScreen.winBackOkButton);
            Actions.isVisible(this.messageUsHomePageScreen.title).should.equal(true);
            Actions.isVisible(this.messageUsHomePageScreen.chatCloseButton).should.equal(true);
            Actions.isEnabled(this.messageUsHomePageScreen.chatSendButton).should.equal(false);
            Actions.isVisible(this.messageUsHomePageScreen.typeYourQuestionField).should.equal(true);
        }
    }

    shouldSeeChatSendButtonEnabled() {
        Actions.isEnabled(this.messageUsHomePageScreen.chatSendButton).should.equal(true);
    }

    sendChatMessage(question) {
        Actions.setImmediateValue(this.messageUsHomePageScreen.typeYourQuestionField, question);
        Actions.waitForEnabled(this.messageUsHomePageScreen.chatSendButton);
        Actions.click(this.messageUsHomePageScreen.chatSendButton);
    }

    closeChatWindow() {
        if (DeviceHelper.isStub()) {
            Actions.waitForVisible(this.messageUsHomePageScreen.errorMessage, 2000);
            Actions.click(this.commonScreen.winBackOkButton);
        } else {
            Actions.waitForNotVisible(this.commonScreen.winBackOkButton, 2000);
            Actions.isVisible(this.messageUsHomePageScreen.chatCloseButton).should.equal(true);
            Actions.click(this.messageUsHomePageScreen.chatCloseButton);
            if (DeviceHelper.isIOS() && Actions.isVisible(this.messageUsHomePageScreen.pushOptInHeader)) {
                Actions.isVisible(this.messageUsHomePageScreen.pushOptInHeader).should.equal(true);
                Actions.click(this.messageUsHomePageScreen.notNowButton);
            }
        }
    }
}

module.exports = MessageUsHomePage;
