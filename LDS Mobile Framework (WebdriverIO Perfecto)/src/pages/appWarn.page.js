require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');

class AppWarnPage extends AuthPage {
    /**
     * @returns {AppWarnScreen}
     */
    get appWarnScreen() {
        return this.getNativeScreen('appWarn.screen');
    }

    waitForAppWarnPageLoad() {
        Actions.waitForVisible(this.appWarnScreen.title, 30000);
    }

    verifyUpdateAppButton() {
        Actions.isVisible(this.appWarnScreen.updateAppButton).should.equal(true);
    }

    verifyContinueWithoutUpdatingButton() {
        Actions.isVisible(this.appWarnScreen.continueWithoutUpdating).should.equal(true);
    }

    clickContinueWithoutUpdatingButton() {
        Actions.click(this.appWarnScreen.continueWithoutUpdating);
    }

    clickUpdateAppButton() {
        Actions.click(this.appWarnScreen.updateAppButton);
    }
}

module.exports = AppWarnPage;
