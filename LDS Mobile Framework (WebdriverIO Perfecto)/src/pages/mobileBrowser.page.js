require('chai').should();
const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');

class MobileBrowserPage extends AuthPage {
    /**
     * @returns {MobileBrowserScreen}
     */
    get mobileBrowserScreen() {
        return this.getNativeScreen('mobileBrowser.screen');
    }

    waitForMobileBroswerPageLoad() {
        Actions.waitForVisible(this.mobileBrowserScreen.title, 10000);
    }

    verifyBrowserUrl() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.mobileBrowserScreen.browserUrl);
        }
        Actions.getText(this.mobileBrowserScreen.browserUrl).should.contains(this.mobileBrowserScreen.brandUrl);
    }

    selectReturnToApp() {
        if (DeviceHelper.isIOS()) {
            Actions.click(this.mobileBrowserScreen.returnToApp);
        } else {
            Actions.pressAndroidBackKey();
        }
    }
}

module.exports = MobileBrowserPage;
