const AuthPage = require('./common/auth.page');
const Actions = require('./common/actions');
const DeviceHelper = require('../utils/device.helper');

class ConfirmAuthenticationPasswordPage extends AuthPage {
    /**
     * @returns {ConfirmAuthenticationPasswordScreen}
     */
    get confirmAuthenticationPasswordScreen() {
        return this.getNativeScreen('confirmAuthenticationPassword.screen');
    }

    waitForPageLoad() {
        Actions.waitForVisible(this.confirmAuthenticationPasswordScreen.title);
    }

    isPageVisible() {
        Actions.isExisting(this.confirmAuthenticationPasswordScreen.title).should.equal(true);
    }

    enterPasswordAndConfirm(password) {
        Actions.waitForVisible(this.confirmAuthenticationPasswordScreen.passwordField);
        Actions.click(this.confirmAuthenticationPasswordScreen.passwordField);
        this.enterPassword(password);
        Actions.click(this.confirmAuthenticationPasswordScreen.confirmButton);
        Actions.reportTimer('Manage_Standing_Order', 'Success');
    }

    enterPassword(password) {
        Actions.waitForVisible(this.confirmAuthenticationPasswordScreen.passwordField);
        Actions.genericSendKeys(this.confirmAuthenticationPasswordScreen.passwordField, password);
    }

    isPasswordFieldVisible() {
        Actions.isVisible(this.confirmAuthenticationPasswordScreen.passwordField)
            .should.equal(true);
    }

    isCancelButtonVisible() {
        Actions.isVisible(this.confirmAuthenticationPasswordScreen.cancelButton)
            .should.equal(true);
    }

    isConfirmButtonVisible() {
        Actions.isVisible(this.confirmAuthenticationPasswordScreen.confirmButton)
            .should.equal(true);
    }

    isForgottenPasswordLinkVisible() {
        Actions.waitForVisible(this.confirmAuthenticationPasswordScreen.forgottenPasswordLabel);
    }

    checkNumberOfPasswordCharacters(numberOfCharacters) {
        const password = Actions.getText(this.confirmAuthenticationPasswordScreen.passwordField);
        if (DeviceHelper.isIOS()) {
            // On iOS, the text of a secure input field is returned as "X Characters",
            // so we extract the password length (X) from that string.
            const passwordLength = parseInt(password.split(' ')[0], 10);
            passwordLength.should.equal(numberOfCharacters);
        } else {
            password.length.should.equal(numberOfCharacters);
        }
    }

    confirmButtonIsEnabled() {
        Actions.isEnabled(this.confirmAuthenticationPasswordScreen.confirmButton)
            .should.equal(true);
    }

    validateMaskedConfirmPassword(isSecure) {
        const password = Actions.getText(this.confirmAuthenticationPasswordScreen.passwordField);
        password.includes('•').should.equal(isSecure);
    }

    confirmButtonIsDisabled() {
        Actions.isEnabled(this.confirmAuthenticationPasswordScreen.confirmButton)
            .should.equal(false);
    }

    clickConfirmButton() {
        Actions.click(this.confirmAuthenticationPasswordScreen.confirmButton);
    }

    clickCancelButton() {
        Actions.click(this.confirmAuthenticationPasswordScreen.cancelButton);
    }

    clickForgottenPasswordLink() {
        Actions.click(this.confirmAuthenticationPasswordScreen.forgottenPasswordLabel);
    }

    enterWrongPassword() {
        this.enterPassword('Abc12345');
        this.clickConfirmButton();
    }

    isErrorMessageDisplayed() {
        Actions.waitForVisible(this.confirmAuthenticationPasswordScreen.errorMessage);
    }

    isSCAIncorrectPasswordDisplayed() {
        Actions.isVisible(this.confirmAuthenticationPasswordScreen.scaErrorMessage);
    }

    isSCAFinalAttemptWarningDisplayed() {
        Actions.isVisible(this.confirmAuthenticationPasswordScreen.scaFinalAttemptWarning);
    }
}

module.exports = ConfirmAuthenticationPasswordPage;
