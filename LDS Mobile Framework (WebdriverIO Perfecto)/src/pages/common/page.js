const ScreenFactory = require('../../screens/screen.factory');
const DeviceHelper = require('../../utils/device.helper');
const Actions = require('./actions');

class Page {
    constructor() {
        this.screens = {
            web: {},
            native: {}
        };
    }

    get commonScreen() {
        return this.getNativeScreen('common/common.screen');
    }

    getNativeScreen(name) {
        if (DeviceHelper.isPerfecto()) {
            Actions.switchContext('NATIVE_APP');
        }
        if (!(name in this.screens.native)) {
            this.screens.native[name] = ScreenFactory.getNativeScreen(name);
        }
        return this.screens.native[name];
    }

    getWebScreen(name) {
        browser.pause(10000);
        Actions.switchContext('WEBVIEW');
        if (!(name in this.screens.web)) {
            this.screens.web[name] = ScreenFactory.getWebScreen(name);
        }
        return this.screens.web[name];
    }
}

module.exports = Page;
