const _ = require('lodash');
const DeviceHelper = require('../../utils/device.helper');
const SwipeAction = require('../../enums/swipeAction.enum');
const AppInfoFactory = require('../../apps/appInfo.factory');
const helper = require('../../screens/native/ios.helper');
const adbHelper = require('../../utils/adb.helper');
const runConfig = require('../../enums/runConfig.enum');
const rotate = require('../../enums/rotation.enum');
const logger = require('../../utils/logger');
const SavedAppDataRepository = require('../../data/savedAppData.repository');
const SavedData = require('../../enums/savedAppData.enum');

const TAP_OUT_KEY_CODE = 61;
const WAITFOR_TIMEOUT = runConfig.TIMEOUTS.WAIT_FOR;
const WAITFORNOTVISIBLE_TIMEOUT = runConfig.TIMEOUTS.WAIT_FOR_NOT_VISIBLE;
const IMPLICIT_TIMEOUT = runConfig.TIMEOUTS.IMPLICIT;
const DELETE_KEY_CODE = 67;

class Actions {
    static validateUndefinedArgs(...args) {
        if (_.some(args, ((value) => value === undefined))) {
            logger.info(args);
            throw Error('some args are undefined');
        }
    }

    static getPageSource() {
        return browser.getPageSource();
    }

    static clickBackButton() {
        Actions.click(this.commonScreen.backButton);
    }

    static element(loc) {
        Actions.validateUndefinedArgs(loc);
        return $(loc);
    }

    static elements(loc) {
        Actions.validateUndefinedArgs(loc);
        return browser.$$(loc);
    }

    static perfectoFindText(text) {
        return browser.execute('mobile:screen:text', {usecache: 'nouse'}).includes(text);
    }

    static ismodifyCaseRequired(state = false) {
        if (browser.capabilities.brand === 'HFX') {
            if (!DeviceHelper.isPerfecto() && DeviceHelper.isAndroid()) {
                return false;
            } if ((DeviceHelper.isAndroid() && String(parseFloat(DeviceHelper.osVersion())).split('.')[0] >= 7) || state) {
                return false;
            }
        }
        return false;
    }

    static modifyAccountName(accountName, state = false) {
        if (DeviceHelper.isBNGA()) return accountName;
        Actions.validateUndefinedArgs(accountName);
        return Actions.ismodifyCaseRequired(state) ? accountName.toUpperCase() : accountName;
    }

    static turnWifiOff() {
        if (DeviceHelper.isAndroid() && DeviceHelper.isPerfecto()) {
            browser.execute('mobile:network.settings:set', {wifi: 'disabled'});
        } else {
            throw new Error('this function is tested only for android on perfecto');
        }
    }

    static waitAndClick(loc) {
        Actions.waitForElementIdVisible(loc);
        $(loc).click();
    }

    static click(loc) {
        Actions.validateUndefinedArgs(loc);
        try {
            $(loc).click();
        } catch (err) {
            throw new Error(err);
        }
    }

    static waitUntilWithoutAssertion(locArray, waitInMS = WAITFOR_TIMEOUT()) {
        // waitUntil any selector of locArray is visible and return the visible selector
        let foundLocator = null;
        Actions.setTimeout({implicit: 0});

        try {
            Actions.waitUntil(
                () => locArray.some(
                    (loc) => {
                        if (Actions.isVisible(loc)) {
                            foundLocator = loc;
                            return true;
                        }
                        return false;
                    }
                ),
                waitInMS,
                `Elements not visible after ${waitInMS} seconds`
            );
        } catch (err) {
            return false;
        }
        Actions.setTimeout();
        return foundLocator;
    }

    static setTimeout(timeout = {implicit: IMPLICIT_TIMEOUT()}) {
        browser.setTimeout(timeout);
    }

    static waitUntilVisible(locArray, waitInMS = WAITFOR_TIMEOUT()) {
        // waitUntil any selector of locArray is visible and return the visible selector
        let foundLocator = null;
        Actions.setTimeout({implicit: 0});
        Actions.waitUntil(
            () => locArray.some(
                (loc) => {
                    if (Actions.isVisible(loc)) {
                        foundLocator = loc;
                        return true;
                    }
                    return false;
                }
            ),
            waitInMS,
            `Elements not visible after ${waitInMS} seconds`
        );
        Actions.setTimeout();
        return foundLocator;
    }

    /**
     * @param {Function} condition  condition to wait on
     * @param {Number=}  timeout    timeout in ms (default: 500)
     * @param {String=}  timeoutMsg error message to throw when waitUntil times out
     * @param {Number=}  interval   interval between condition checks (default: 500)
     */
    static waitUntil(condition, timeout = WAITFOR_TIMEOUT(), timeoutMsg, interval = 1000) {
        const startTime = new Date();
        let returnValue = false;
        const retry = (time = 0) => {
            if (time < timeout) {
                try {
                    returnValue = condition();
                    if (!returnValue) {
                        throw new Error('Condition is false');
                    }
                } catch (err) {
                    this.pause(interval);
                    return retry(new Date() - startTime);
                }
            } else {
                return false;
            }
            return true;
        };
        if (!retry()) {
            throw new Error(timeoutMsg);
        }
        return returnValue;
    }

    static waitForNotVisible(loc, waitInMS = WAITFORNOTVISIBLE_TIMEOUT()) {
        Actions.validateUndefinedArgs(loc);
        const waitForMethod = () => !Actions.isVisible(loc);
        Actions.waitUntil(waitForMethod, waitInMS, `Element ${loc} was expected not to be visible`);
    }

    static waitForVisible(loc, waitInMS = WAITFOR_TIMEOUT()) {
        Actions.validateUndefinedArgs(loc);
        const waitForMethod = () => Actions.isVisible(loc);
        Actions.waitUntil(waitForMethod, waitInMS, `Element ${loc} was expected to be visible$`);
    }

    static waitForElementIdVisible(selector, waitInMS = WAITFOR_TIMEOUT()) {
        Actions.validateUndefinedArgs(selector);
        const waitForMethod = () => $(selector).isDisplayed();
        return Actions.waitUntil(waitForMethod, waitInMS, `Element ${selector} was expected to be visible$`);
    }

    static isExisting(loc) {
        Actions.validateUndefinedArgs(loc);
        let returnValue = false;
        try {
            const element = $(loc);
            returnValue = element.isExisting();
        } catch (error) {
            returnValue = false;
        }
        return returnValue;
    }

    /**
     * Swipe until an element with the given selector is visible, failing if none becomes visible
     * after numerous attempts. This method will return successfully if more than one is visible.
     *
     * @param swipeAction Swipe action to take
     * @param loc selector to search for
     * @param thresholdOffset Only used if swiping up. See comment on isOverThresholdHeight
     */
    static swipeUntilAnyVisible(swipeAction, loc, thresholdOffset = 0) {
        Actions.validateUndefinedArgs(loc);
        return this.swipeUntilVisible(swipeAction, loc, thresholdOffset, true);
    }

    /**
     * Swipe until an element with the given selector is visible, failing if none becomes visible
     * after specified number of attempts.
     *
     * @param loc selector to search for
     * @param maxSwipes Maximum number of times to swipe, by default its 5
     * @param startXPercent - Starting X coordinates percentage
     * @param swipeDuration - swipe duration in milliseconds
     * @param startYPercent - Starting Y coordinates percentage
     * @param endXPercent - ending X coordinates percentage
     * @param endYPercent - ending Y coordinates percentage
     */
    static swipeByPercentUntilVisible(loc, startXPercent = 80, startYPercent = 80,
        endXPercent = 20, endYPercent = 20, swipeDuration = 100, maxSwipes = 5, containerLocator = false) {
        const foundElement = (() => {
            Actions.setTimeout({implicit: 500});
            for (let count = 0; count < maxSwipes; count++) {
                if (this.isVisible(loc)) {
                    return true;
                }
                this.swipeByPercentage(startXPercent, startYPercent, endXPercent, endYPercent,
                    swipeDuration, containerLocator);
            }
            Actions.setTimeout();
            return false;
        })();

        Actions.validateUndefinedArgs(loc);
        if (!foundElement) {
            throw new Error(`Element "${loc}" not found whilst scrolling!`);
        }
    }

    static swipeByPercentUntilCondition(condition, startXPercent = 80, startYPercent = 80,
        endXPercent = 20, endYPercent = 20, swipeDuration = 100, maxSwipes = 5) {
        if (typeof condition !== 'function') {
            throw new Error(`Condition ${condition} should be a function`);
        }
        const foundElement = (() => {
            for (let count = 0; count < maxSwipes; count++) {
                if (condition()) {
                    return true;
                }
                this.swipeByPercentage(startXPercent, startYPercent, endXPercent, endYPercent,
                    swipeDuration);
            }
            return false;
        })();

        if (!foundElement) {
            throw new Error(`Condition "${condition.name}" not fulfilled whilst scrolling!`);
        }
    }

    /**
     * Swipe until an element with the given selector is visible, failing if none becomes visible
     * after specified number of attempts.
     *
     * @param loc selector to search for
     * @param maxSwipes Maximum number of times to swipe, by default its 5
     * @param startXPercent - Starting X coordinates percentage
     * @param swipeDuration - swipe duration in milliseconds
     * @param startYPercent - Starting Y coordinates percentage
     * @param endXPercent - ending X coordinates percentage
     * @param endYPercent - ending Y coordinates percentage
     */
    static swipeByPercentUntilVisibleWithinViewport(loc, startXPercent = 80, startYPercent = 80,
        endXPercent = 20, endYPercent = 20, swipeDuration = 100,
        maxSwipes = 5, containerLocator = false) {
        const isElementVisible = () => Actions.isVisibleWithinViewport(loc);
        const foundElement = (() => {
            for (let count = 0; count < maxSwipes; count++) {
                if (isElementVisible()) {
                    return true;
                }
                Actions.swipeByPercentage(startXPercent, startYPercent, endXPercent, endYPercent,
                    swipeDuration, containerLocator);
            }
            return false;
        })();

        Actions.validateUndefinedArgs(loc);
        if (!foundElement) {
            throw new Error(`Element "${loc}" not found whilst scrolling!`);
        }
    }

    /**
     * Swipe until an element with the given selector is visible, failing if none becomes visible
     * after specified number of attempts.
     *
     * @param swipeAction Swipe action to take
     * @param loc selector to search for
     * @param thresholdOffset Only used if swiping up. See comment on isOverThresholdHeight
     * @param allowMultiple If true, will swipe until first matching element is visible,
     *                      otherwise will fail if more than one matching element becomes visible.
     * @param maxSwipes Maximum number of times to swipe, by default its 5
     */
    static swipeUntilVisible(swipeAction, loc, thresholdOffset = 0, allowMultiple = false, maxSwipes = 20,
        swipeDuration = 100) {
        const isSwipeUp = swipeAction === SwipeAction.UP;
        const isElementVisible = () => (allowMultiple ? Actions.isAnyVisible(loc) : Actions.isVisible(loc))
        && (isSwipeUp ? Actions.isOverThresholdHeight(loc, thresholdOffset) : true);
        const foundElement = (() => {
            for (let count = 0; count < maxSwipes; count++) {
                if (isElementVisible()) {
                    return true;
                }
                Actions.swipePage(swipeAction, swipeDuration);
            }
            return false;
        })();

        Actions.validateUndefinedArgs(loc);
        if (!foundElement) {
            throw new Error(`Element "${loc}" not found whilst scrolling!`);
        }
    }

    static swipeDownOnSmallScreen(startXPercent = 80, startYPercent = 80, endXPercent = 20, endYPercent = 20,
        swipeDuration = 100, containerLocator = false) {
        if (DeviceHelper.isSmallScreenSize()) {
            Actions.swipeByPercentage(startXPercent, startYPercent, endXPercent, endYPercent,
                swipeDuration, containerLocator);
        }
    }

    /**
     * swipeToLocator uses the "mobile: scroll" method for XCUITest. For UIAutomation and Android it
     * uses the legacy swipeUntilVisible, since "mobile: scroll" is implemented differently for
     * UIAutomation.
     *
     * @param parentLocator Only used for XCUITest. The selector for the element to swipe on.
     * @param name Only used for XCUITest. The name of the element to swipe to.
     * @param selector Only used for UIAutomation. The selector for the element to swipe to.
     */
    static swipeToLocator(parentLocator, name, selector) {
        if (DeviceHelper.isXCUITest()) {
            const parentElementId = browser.getElementId(parentLocator);
            browser.execute('mobile: scroll', {element: parentElementId, name});
        } else {
            Actions.swipeUntilVisible(SwipeAction.SHORTUP, selector, 0, false, 25);
        }
    }

    static swipeOnElement(swipeAction, selector) {
        swipeAction.perform($(selector)
            .getSize(), $(selector)
            .getLocation());
    }

    static swipePage(swipeAction, swipeDuration = 100) {
        swipeAction.perform(DeviceHelper.windowHandleSize(), {x: 0, y: 0}, swipeDuration);
    }

    /**
     * Check that more than 70% of the item is visible when measured from the bottom of the screen
     * minus the offset. If there are multiple matching elements, only the first will be checked.
     *
     * If required in the future, we could check for elements obscured by the top / sides of the
     * screen, or obscured by other elements on the screen in a cleverer way than via the offset.
     */
    static isOverThresholdHeight(selector, offset) {
        if (offset === 0) {
            return true; // Dont check threshold if its not required explicitly
        }
        const elementRectangle = browser.getElementRect(Actions.getElementId(selector));
        const elementYPos = elementRectangle.y;
        const elementHeight = elementRectangle.height;
        const thresholdHeight = DeviceHelper.windowHandleSize().height;

        return elementYPos + (elementHeight * 0.7) < thresholdHeight - offset;
    }

    /**
     * Check if element is visible using selector.
     * if there are multiple elements check visibility of first element.
     *
     * @param loc selector to search for
     * @returns true if an item matching the selector is visible, false otherwise
     */
    static isVisible(loc) {
        Actions.validateUndefinedArgs(loc);
        // if there are multiple elements check visibility of first element
        let returnVal = false;
        try {
            returnVal = $(loc).isDisplayed();
        } catch (error) {
            returnVal = false;
        }
        return returnVal;
    }

    static isVisibleWithinViewport(loc) {
        Actions.validateUndefinedArgs(loc);
        // if there are multiple elements check visibility of first element
        return $(loc).isDisplayedInViewport();
    }

    static sendKeysAndTapOut(loc, text) {
        Actions.validateUndefinedArgs(loc, text);
        Actions.genericSendKeys(loc, text);
        Actions.tapOutOfField();
    }

    static clearText(loc) {
        Actions.validateUndefinedArgs(loc);
        $(loc).clearValue();
    }

    static pressAndroidBackKey() {
        driver.pressKeyCode(4);
    }

    static pressAndroidMenuKey() {
        driver.pressKeyCode(3);
    }

    static pressKeyCode(keyCode) {
        Actions.validateUndefinedArgs(keyCode);
        driver.pressKeyCode(keyCode);
    }

    /**
     * Check if one or more elements matching the given selector is visible.
     *
     * @param loc selector to search for
     * @returns true if one or more items matching the selector is visible, false otherwise
     */
    static isAnyVisible(loc) {
        Actions.validateUndefinedArgs(loc);
        return Actions.isVisible(loc);
    }

    static pause(ms) {
        browser.pause(ms);
    }

    static firstInstanceOf(possibleArray) {
        if (Array.isArray(possibleArray)) {
            return possibleArray[0];
        }
        return possibleArray;
    }

    static isEnabled(loc) {
        Actions.validateUndefinedArgs(loc);
        // if there are multiple elements check visibility of first element
        let returnValue = false;
        try {
            returnValue = $(loc).isEnabled();
        } catch (error) {
            returnValue = false;
        }
        return returnValue;
    }

    static waitForEnabled(loc, waitInMS = WAITFOR_TIMEOUT()) {
        Actions.validateUndefinedArgs(loc);
        const waitForMethod = () => Actions.isEnabled(loc);
        Actions.waitUntil(waitForMethod, waitInMS, `Element ${loc} was expected to be visible$`);
    }

    static longPressed(selector, pressDuration = 1000) {
        Actions.validateUndefinedArgs(selector);
        browser.touchAction([
            {action: 'longPress', element: Actions.element(selector)},
            {action: 'wait', ms: pressDuration},
            'release'
        ]);
    }

    static isChecked(loc) {
        Actions.validateUndefinedArgs(loc);
        if (DeviceHelper.isAndroid()) {
            return Actions.getAttribute(loc, 'checked') === 'true';
        }

        // To determine if the switch is enabled on iOS
        return Actions.getAttribute(loc, 'value') === true
            || Actions.getAttribute(loc, 'value') === 'true'
            || Actions.getAttribute(loc, 'value') === '1';
    }

    // yet to implement for iOS
    static isClickable(selector) {
        Actions.validateUndefinedArgs(selector);
        if (DeviceHelper.isIOS()) {
            return $(selector).getAttribute('hittable') === 'true';
        }
        return $(selector).getAttribute('clickable') === 'true';
    }

    static getAttribute(selector, attr) {
        Actions.validateUndefinedArgs(selector, attr);
        return $(selector).getAttribute(attr);
    }

    static elementIdAttribute(elementId, attr) {
        Actions.validateUndefinedArgs(elementId, attr);
        return browser.getElementAttribute(elementId, attr);
    }

    static getAttributeText(loc) {
        const attributeArray = this.getElementsListAttributeText(loc);
        if (attributeArray.length === 1) {
            return attributeArray[0];
        }
        return attributeArray;
    }

    static getElementsListAttributeText(loc) {
        Actions.validateUndefinedArgs(loc);
        const attributeArray = [];
        Actions.getElementList(loc)
            .forEach((x) => attributeArray.push(x.getText()));
        if (attributeArray.length === 0) {
            throw new Error(`No Elements founds for selector ${loc}`);
        }
        return attributeArray;
    }

    static getElementRect(selector) {
        Actions.validateUndefinedArgs(selector);
        return browser.getElementRect(Actions.getElementId(selector));
    }

    static genericSendKeys(loc, text) {
        Actions.validateUndefinedArgs(loc, text);
        Actions.waitAndClick(loc);
        $(loc).setValue(text);
    }

    static sendKeys(loc, text) {
        Actions.validateUndefinedArgs(loc, text);
        Actions.pause(500);
        Actions.genericSendKeys(loc, text);
        if (DeviceHelper.isAndroid()) {
            Actions.hideDeviceKeyboard();
        } else {
            Actions.tapOutOfField();
        }
    }

    static getElementId(selector) {
        return $(selector).elementId;
    }

    static getText(selector) {
        Actions.validateUndefinedArgs(selector);
        return $(selector).getText();
    }

    static getTagName(loc) {
        Actions.validateUndefinedArgs(loc);
        return $(loc).getTagName();
    }

    static tapOutOfField() {
        if (DeviceHelper.isAndroid()) {
            driver.pressKeyCode(TAP_OUT_KEY_CODE);
        } else {
            const doneButtonLoc = ['~Done', '~Search', '~done', '~return', '~Return', '~Go'];
            const selector = Actions.waitUntilWithoutAssertion(doneButtonLoc);
            if (selector) {
                Actions.click(selector);
            }
        }
    }

    static getElementList(loc) {
        Actions.validateUndefinedArgs(loc);
        return $$(loc);
    }

    static deleteCharacter(iosDeleteButtonLoc, noOfCharactersToDelete = 1) {
        for (let i = 1; i <= noOfCharactersToDelete; i++) {
            if (DeviceHelper.isAndroid()) {
                driver.pressKeyCode(DELETE_KEY_CODE);
            } else {
                Actions.click(iosDeleteButtonLoc);
            }
        }
    }

    static swipeByPercentage(startXPercent, startYPercent, endXPercent, endYPercent,
        swipeDuration, containerLocator = false) {
        const deviceDimension = DeviceHelper.windowHandleSize();
        let elementHeight;
        let elementWidth;
        let elementXOffset = 0;
        let elementYOffset = 0;

        if (containerLocator) {
            const elementRectangle = Actions.getElementRect(containerLocator);
            elementXOffset = elementRectangle.x;
            elementYOffset = elementRectangle.y;
            elementWidth = elementRectangle.width;
            elementHeight = elementRectangle.height;
        } else {
            elementHeight = deviceDimension.height;
            elementWidth = deviceDimension.width;
        }
        const startX = elementXOffset + elementWidth * (startXPercent / 100);
        const startY = elementYOffset + elementHeight * (startYPercent / 100);
        const endX = elementXOffset + elementWidth * (endXPercent / 100);
        const endY = elementYOffset + elementHeight * (endYPercent / 100);
        // Swipe on XCUITest supported iOS devices is done using dragFromToForDuration,
        // for Android devices swipe is done using touchAction
        if (DeviceHelper.isPerfecto() && DeviceHelper.isIOS() && DeviceHelper.isIOS9()) {
            browser.touchAction([
                {action: 'press', x: startX, y: startY - 2 * endY},
                {action: 'wait', ms: 100},
                {action: 'moveTo', x: endX, y: endY},
                'release'
            ]);
        } else if (DeviceHelper.isIOS() && driver.getContext() === 'NATIVE_APP') {
            // dragFromToForDuration is only supported on devices
            browser.execute('mobile: dragFromToForDuration', {
                duration: swipeDuration / 1000,
                fromX: startX,
                fromY: startY,
                toX: endX,
                toY: endY
            });
        } else {
            browser.touchAction([
                {action: 'press', x: startX, y: startY},
                {action: 'wait', ms: !DeviceHelper.isPerfecto() ? 1200 : swipeDuration},
                {action: 'moveTo', x: endX, y: endY},
                'release'
            ]);
        }
    }

    static setValue(selector, value) {
        Actions.validateUndefinedArgs(selector, value);
        $(selector).clearValue();
        $(selector).addValue(value);
    }

    static setValueImmediate(selector, value) {
        Actions.validateUndefinedArgs(selector, value);
        browser.setValueImmediate(Actions.getElementId(selector), value);
    }

    static setImmediateValue(selector, value) {
        Actions.validateUndefinedArgs(selector, value);
        Actions.waitAndClick(selector);
        browser.setValueImmediate(Actions.getElementId(selector), value);
    }

    static setImmediateValueAndTapOut(selector, value) {
        Actions.validateUndefinedArgs(selector, value);
        Actions.setImmediateValue(selector, value);
        Actions.tapOutOfField();
        this.hideDeviceKeyboard();
    }

    static keysAndTapOut(selector, value) {
        Actions.validateUndefinedArgs(selector, value);
        Actions.waitAndClick(selector);
        browser.keys(value);
        Actions.tapOutOfField();
        this.hideDeviceKeyboard();
    }

    static hideDeviceKeyboard() {
        // TODO: MOB3-1517 Fix soft Keyboard exception for Android appium.
        try {
            if (DeviceHelper.isAndroid()) {
                driver.hideKeyboard();
            }
        } catch (e) {
            logger.error(`Error in hideKeyboard ${e.message}`);
        }
    }

    static addValue(selector, value) {
        $(selector)
            .addValue(value);
    }

    static closeBrowser() {
        browser.close();
    }

    static setMICustomKeyoard(value) {
        Actions.validateUndefinedArgs(value);
        if (DeviceHelper.isIOS()) {
            Actions.click(helper.byName(value.toUpperCase()));
        } else {
            adbHelper.inputText(value);
        }
    }

    static elementIdClick(elementId) {
        browser.elementClick(elementId);
    }

    static setIOSPickerWheelValue(selector, expectedValue, array) {
        // Get Expected Index
        const expectedIndex = array.indexOf(expectedValue);

        // Get Current index and value
        let currentValue = Actions.getAttribute(selector, 'value')
            .split(',')[0].trim();
        // Integer value will be type casted here
        currentValue = Number.isNaN(parseInt(currentValue, 10)) ? currentValue : parseInt(currentValue, 10);
        let index = array.indexOf(currentValue);

        // Click on picker value
        let count = 0;
        while (currentValue !== expectedValue && count < 50) {
            if (DeviceHelper.isPerfecto()) {
                if (expectedIndex - index > 2) {
                    index += 2;
                    browser.execute('mobile:text:select', {content: array[index]});
                } else if (index - expectedIndex > 2) {
                    index -= 2;
                    browser.execute('mobile:text:select', {content: array[index]});
                } else {
                    browser.execute('mobile:text:select', {content: expectedValue});
                }
            } else {
                browser.execute('mobile:selectPickerWheelValue', {
                    order: (index < expectedIndex) ? 'next' : 'previous',
                    element: Actions.getElementId(selector),
                    offset: 0.15
                });
            }
            currentValue = Actions.getAttribute(selector, 'value').split(',')[0].trim();
            currentValue = Number.isNaN(parseInt(currentValue, 10)) ? currentValue : parseInt(currentValue, 10);
            index = array.indexOf(currentValue);
            count++;
        }

        [currentValue].should.include(expectedValue);
    }

    static appInBackground() {
        if (DeviceHelper.isAndroid()) {
            Actions.pressKeyCode(3);
        } else {
            browser.background(10);
        }
        browser.pause(2000);
    }

    static launchAppFromBackground() {
        const appInfo = AppInfoFactory.getAppInfo(browser.capabilities);
        browser.execute('mobile:application:open', {identifier: appInfo.appPackage});
        browser.pause(5000);
    }

    static tapByLocation(label) {
        Actions.validateUndefinedArgs(label);
        /*
         TODO: MPL-1514 Modify as per appium syntax
         Need to change it in tapLocationByPercentage(xPercen, ypercent)
         and change the implementation for Perfecto.
         */
        if (!DeviceHelper.isPerfecto()) {
            const size = DeviceHelper.windowHandleSize();
            const parseNumber = (val) => (Number.isNaN(parseInt(val, 10)) ? val : parseInt(val, 10));
            const x = size.width * (parseNumber(label.split(',')[0].replace(/%/i, '')) / 100);
            const y = size.height * (parseNumber(label.split(',')[1].replace(/%/i, '')) / 100);
            if (DeviceHelper.isIOS()) {
                browser.execute('mobile:tap', {x, y});
            } else {
                browser.touchAction({
                    action: 'tap',
                    x,
                    y
                });
            }
        } else {
            browser.execute('mobile:touch:tap', {location: `${label}`});
        }
    }

    static startImageInjection(imageDetails) {
        browser.execute('mobile:image.injection:start', imageDetails);
    }

    static stopImageInjection() {
        browser.execute('mobile:image.injection:stop');
    }

    static openDefaultBrowser() {
        if (!DeviceHelper.isPerfecto()) {
            throw new Error('Its Perfecto supported command only');
        }
        browser.execute('mobile:browser:open', {automation: 'simulated'});
    }

    static pressBackKey() {
        if (!DeviceHelper.isPerfecto()) {
            throw new Error('Its Perfecto supported command only');
        }
        browser.execute('mobile:presskey', {keySequence: 'BACK'});
    }

    static turnWifiOn() {
        if (DeviceHelper.isAndroid() && DeviceHelper.isPerfecto()) {
            browser.execute('mobile:network.settings:set', {wifi: 'enabled'});
        } else {
            throw new Error('this function is tested only for android on perfecto');
        }
    }

    static clickOnText(expectedValue) {
        if (!DeviceHelper.isPerfecto()) {
            throw new Error('clickOnText is only meant for Perfecto, not Appium.');
        }
        browser.execute('mobile:text:select', {content: expectedValue});
    }

    static perfectoElementInfo(property, loc, framework = 'appium-1.3.4', timeout = 5) {
        if (!DeviceHelper.isPerfecto()) {
            throw new Error('perfectoElementInfo is only meant for Perfecto, not Appium.');
        }
        browser.execute('mobile:application.element:info',
            {property, value: loc, framework, timeout});
    }

    static enterPasswordInWebPageAndroid(loc, text) { // will add platform logic for both ios and android
        Actions.validateUndefinedArgs(loc, text);
        Actions.click(loc);
        adbHelper.inputText(text);
        Actions.hideDeviceKeyboard();
    }

    static launchDeeplink(deepLinkUrl) {
        if (!DeviceHelper.isPerfecto()) {
            throw new Error('Its Perfecto supported command only');
        }
        browser.execute('mobile:browser:goto', {url: deepLinkUrl});
    }

    static setLocation(addressVal) {
        if (DeviceHelper.isAndroid() && DeviceHelper.isPerfecto()) {
            browser.execute('mobile:location:set', {address: addressVal});
        } else {
            throw new Error('this function is tested only for android on perfecto');
        }
    }

    static resetDeviceLocation() {
        if (DeviceHelper.isAndroid() && DeviceHelper.isPerfecto()) {
            browser.execute('mobile:location:reset');
        } else {
            throw new Error('this function is tested only for android on perfecto');
        }
    }

    static startDeviceVitals(interval = 10, sources = 'Devices') {
        if (DeviceHelper.isPerfecto()) {
            browser.execute('mobile:monitor:start', {
                sources,
                interval
            });
        } else {
            logger.info('startDeviceVitals implementation to be added.');
        }
    }

    static stopDeviceVitals() {
        if (DeviceHelper.isPerfecto()) {
            browser.execute('mobile:monitor:stop', {});
        } else {
            logger.info('stopDeviceVitals implementation to be added.');
        }
    }

    static launchDeviceSettings() {
        if (DeviceHelper.isIOS()) {
            browser.execute('mobile:launchApp', {bundleId: 'com.apple.Preferences'});
        } else {
            throw new Error('this function is tested only for ios');
        }
    }

    static rotateDevice(orientation) {
        const mapRotate = rotate.fromStringByEscapingSpace(orientation);
        if (DeviceHelper.isAndroid()) {
            adbHelper.deviceRotation(mapRotate.value);
        } else {
            throw new Error('Not implemented for IOS');
        }
    }

    static setOrientation(orientation) {
        browser.setOrientation(orientation);
    }

    static getOrientation() {
        return browser.getOrientation();
    }

    static reportTimer(timerID, title) {
        if (SavedAppDataRepository.getData(SavedData.IS_NFT_TIMER_ENABLED)) {
            browser.execute('mobile:checkpoint:text', {
                content: title,
                threshold: 90,
                measurement: 'accurate',
                timeout: 30,
                source: 'camera',
                analysis: 'automatic'
            });
            const timeEx = (browser.execute('mobile:timer:info', {
                timerId: timerID,
                timerType: 'ux',
                units: 'milliseconds'
            }) || 0);
            browser.execute('mobile:status:timer', {name: timerID, result: timeEx});
        }
    }

    static iOSObjectTreeOptimizationStart(childNodes = 6) {
        if (DeviceHelper.isPerfecto() && DeviceHelper.isIOS()) {
            browser.execute('mobile:objects.optimization:start', {children: childNodes});
        }
    }

    static iOSObjectTreeOptimizationStop() {
        if (DeviceHelper.isPerfecto() && DeviceHelper.isIOS()) {
            browser.execute('mobile:objects.optimization:stop', {});
        }
    }

    static switchContext(context) {
        driver.switchContext(context);
    }
}

module.exports = Actions;
