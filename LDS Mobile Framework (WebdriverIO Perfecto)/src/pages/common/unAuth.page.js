const Page = require('./page');
const Actions = require('./actions.js');

class UnAuthPage extends Page {
    selectContactUs() {
        Actions.waitAndClick(this.commonScreen.contactUs);
    }

    shouldNotSeeContactUsButton() {
        Actions.isVisible(this.commonScreen.contactUs).should.equal(false);
    }

    shouldSeeContactUsButton() {
        Actions.isVisible(this.commonScreen.contactUs).should.equal(true);
    }
}

module.exports = UnAuthPage;
