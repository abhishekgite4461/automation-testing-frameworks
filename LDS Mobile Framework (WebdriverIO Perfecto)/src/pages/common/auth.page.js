require('chai').should();
const page = require('./page');
const Actions = require('./actions');
const DeviceHelper = require('../../utils/device.helper');

class AuthPage extends page {
    get bottomNavigationBarScreen() {
        return this.getNativeScreen('common/bottomNavigationBar.screen');
    }

    tabLocator(locator) {
        return this.bottomNavigationBarScreen[`${locator}Tab`];
    }

    selectTab(tabName) {
        Actions.waitForVisible(this.tabLocator(tabName));
        Actions.waitAndClick(this.tabLocator(tabName));
    }

    verifyBottomNavBar() {
        Actions.waitForVisible(this.tabLocator('home'));
    }

    isBottomNavBarVisible() {
        return Actions.isVisible(this.tabLocator('home'));
    }

    shouldNotSeeBottomNavBar() {
        Actions.isVisible(this.tabLocator('home')).should.equal(false);
    }

    shouldSeeOptionsInBottomNavBar(bottomNavOptions) {
        for (const individualOptions of bottomNavOptions) {
            Actions.isVisible(this.tabLocator(individualOptions.optionsInBottomNav)).should.equal(true);
        }
    }

    verifySelectedTabIsHighlighted(bottomNavOptions) {
        for (const individualOptions of bottomNavOptions) {
            this.selectTab(individualOptions.optionsInBottomNav);
            this.shouldSeeTabHighlighted(individualOptions.optionsInBottomNav);
        }
    }

    shouldSeeTabHighlighted(tabName) {
        if (DeviceHelper.isIOS()) {
            Actions.getAttribute(this.tabLocator(tabName), 'value').should.equal('1');
        } else {
            Actions.getAttribute(this.tabLocator(tabName), 'selected').should.equal('true');
        }
    }

    static swipeOnScreen() {
        Actions.swipeByPercentage(80, 80, 20, 20, 100);
    }
}

module.exports = AuthPage;
