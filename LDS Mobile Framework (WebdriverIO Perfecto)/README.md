# NGA AFT

This repository contains automated integration tests for LBG's mobile apps.

The tests are run against physical devices (either locally through Appium or hosted by Perfecto).

# Requirements

* Git.
* Node.js >= 12 (recommendation is Node.js 12.18.1).
* Java 8 or higher (required for Allure reports)
* Access to Sandbox.
* Webstorm (or whatever IDE you're comfortable with).
* For local Android testing, Android SDK is required (with an `ANDROID_HOME` environment variable
  pointing to the SDK folder and `$ANDROID_HOME/platform-tools` on your `PATH`).
* For local iOS testing, XCode 8 or above is required (although it may work with lower versions if
  you remove `caps['automationName'] = 'XCUITest';` in `appium.config.js`).  There are extra
  dependencies for iOS, libimobiledevice and ios-deploy.  These can be installed using Homebrew on
  a Mac:  For libimobiledevice, `brew install --HEAD libimobiledevice` and for ios-deploy,
  `brew install ios-deploy`.  For Lloyds MacBooks, see
   https://confluence.devops.lloydsbanking.com/display/MOB/1.+Development+how-tos#id-1.Developmenthow-tos-FixHomebrewpermissionsonaLloydsMBP)
* For perfecto testing, you need Perfecto credentials - ask the QA lead or, ask on the slack channel
  `#grp-quality-engineers`.
* This project is not routinely tested on Lloyds Windows Machines, so instructions for installing
  on these machines are not given here.

# Setup

1. Open a terminal.
2. Clone the repo from mobile's gitlab in sandbox
   `git clone ssh://git@gitlab.mobile.sbx.zone:29418/nga/nga-mobile-3-qa.git`.
3. Navigate to the repository folder `cd nga-mobile-3-qa`.
4. Install all the npm dependencies `npm install`.
   If you're planning to never run against Appium (e.g. if you're on a Windows machine),
   use `npm i --no-optional` instead.

# Running the tests

## Getting the APK/IPA to test against

Regardless of which device provider you choose, you will need the APK/IPA of the app that you want
to test. You can always build it yourself, especially if you need to test a custom change or debug
an issue, but if you only need to run tests against the latest master or a release candidate, there 
is a gradle script for getting those builds.

1. To find out which builds are available for download, run:

       ./gradlew tasks --all

   Under "Other tasks" you will find all the options. The upload tasks are for uploading to Perfecto
   and require additional configuration (see [Running against Perfecto](#running-against-perfecto)).
   You can ignore the upload tasks for now. All you need to know is that they work the same as the
   download tasks, except they also upload to Perfecto.

2. Execute the task for the builds that you need. Example:

       ./gradlew downloadALL

   The above will download all brands for both iOS and Android. This would take a long time, since
   the apps are big and the network is slow, so it is recommended that you only download the apps
   that you need. It will only perform the download if you don't already have the latest build, so
   don't be afraid of running it over and over again.

   By default you will get the latest master build, but if you need a release candidate there's an
   option you can provide:

       ./gradlew downloadRNGA -Pbranch=release_candidate_sweden

   If there was a release candidate called Sweden, the above would download all retail brands for
   iOS and Android that have been built from the `release_candidate_sweden` branch.

3. The next step depends on which device provider you choose. See below.

## Running against Perfecto

1. Set the following additional environment variables.
    * `PERFECTO_USER`=`<Perfecto username>`
    * `PERFECTO_PWD`=`<Perfecto password>`
2. Upload the APK/IPA to Perfecto. Unless you have a custom build, you are adviced to use the
   appropriate upload task of the gradle script. The upload tasks depend on their respective
   download tasks, and will first download the latest build if you don't already have it. Example:

       ./gradlew uploadALL

   For custom builds, please use the following command to upload the .apk or .ipa you want to test:

       ./uploadAppToPerfecto.sh --user $PERFECTO_USER --pwd $PERFECTO_PWD --file build-store/RNGA_LDS.3.apk

   They must be named `{X}NGA_{BRAND}.3.{ext}`, where:
    * `{X}` should be replaced with `R` for retail and `B` for business
    * `{BRAND}` should be replaced with `LDS`, `HFX`, `BOS`, or `MBNA`
    * `{ext}` should be replaced with `apk` for Android and `ipa` for iOS.
    * The '3' is there for historical reasons and indicates the version of NGA. Before NGA 3 was
      released, we were maintaining NGA 2 in parallel. Today there is no reason to test anything
      other than NGA 3.

4. Run the tests, eg.

       npm run perfecto -- --tag @environmentSmoke --server STUB --platform Android --type RNGA --app NGA3 --brand LDS

5. Run the tests with already installed app, eg.

       npm run perfecto -- --tag @environmentSmoke --server STUB --platform Android --uia true --b BOS

If you need to run against Perfecto via a proxy, you must set the following additional environment
variables beforehand:

* `PROXY_HOST`=`<host>`
* `PROXY_PORT`=`<port>`

If the proxy requires authentication, you also need:

* `PROXY_USER`=`<username>`
* `PROXY_PWD`=`<password>`

For example if you are using an LBG Windows laptop, you will need:

 * `PROXY_HOST=proxyarray.service.group`
 * `PROXY_PORT=8080`
 * `PROXY_USER=<global domain login>`
 * `PROXY_PWD=<global domain password>`.

## Running against a local Appium instance

1. Add the .apk's and .ipa's to `build-store/`
    * They should be named RNGA_LDS.apk, RNGA_HFX.apk and RNGA_BOS.apk (and similar for the .ipa's)
2. Plug in an iPhone and/or Android phone.
3. Run `npm run appium`.

## Running against Headspin Devices

> This is still a POC and hasn't been rolled out yet. The below instructions are likely to be
incomplete.

1. Add the Headspin authentication token to environment variables
    * `HEADSPIN_AUTH_TOKEN`=`<HS_AUTH_TOKEN>`
2. Run the test with the following command:
        npm run headspin -- --tag @primary --server STUB --platform Android --type RNGA --app NGA3 --brand LDS --deviceIds <UDID from HS>

## Command line arguments

View all available arguments by running `npm run appium -- --help`

Some example configurations:
* `npm run appium -- --app NGA3` - run default NGA 3 tests
* `npm run appium -- --brand LDS --brand HFX --brand BOS` - run default tests on all three brands
* `npm run appium -- --server PUT2.2-GLZ` - run default tests against PUT2.2
* `npm run appium -- --tag @t1,@t2` - run tests tagged with `@t1` or `@t2`
* `npm run appium -- --tag @t1 --tag @t2` - run tests tagged with `@t1` AND `@t2`

# Writing new tests

1. Add a new .feature file for the story you are testing in the correct subfolder of
   `src/features/`.
2. Name the feature (using `Feature:`) and add some Scenarios to your new .feature file (look at
   the code style guide https://confluence.devops.lloydsbanking.com/pages/viewpage.action?pageId=74368612,
   read https://github.com/cucumber/cucumber/wiki/Gherkin, or look at other .feature files for
   examples.)
3. You can add as many tags as you like to distinguish different groups of tests.  There is a list
   of tags and their meanings in the `tagList.js`.  Some commonly used tags are:

   * `@nga3` used to run tests against NGA3.
   * `@appium` and `@perfecto` are used to distinguish between tests that will only run against
      Appium or Perfecto respectively.
   * Tags beginning `@stub-` indicate which stubbed responses should be used when running against
      the NGA3 app in stub mode.
   * Tags begninning `@sw-` indicate that certain switches must be on or off for this feature.

   In general, we want tests to run on as wide a range of apps, platforms, etc as possible, so tags
   should only restrict this when necessary.

4. Add step definitions for any undefined steps in `src/step_definitions/`
   * The step definitions should follow the same convention as the existing step definitions, i.e.
       * Create the relevant Page object, eg. `const loginPage = new LoginPage();`.
       * Perform an action on that Page, eg. `loginPage.login();`
   * Reuse existing pages as much as possible.
   * Steps should be roughly grouped by journey, e.g. Android Fingerprint steps in
     in `fingerprint.steps.js`.
5. Add any undefined page objects and their actions in `src/pages/`
   * The page objects should follow the same convention as the existing page objects, i.e.
       * They should define a `get xyzScreen()` method which is used to provide the locators, e.g.
       `get loginScreen() { return this.getScreen('login.screen'); }`
       * The actions should perform events (e.g. Click, Wait, Scroll) on Screen elements, e.g.
         `Page.click(this.loginScreen.continueButton());`
   * Reuse existing screens as much as possible.
6. Add any undefined native screens and their elements in the correct subfolder of
   `src/screens/`.
7. Add the platform specific screens:
   * For NGA3 Android:
       * The screens should be in `src/screens/native/android`.
       * The elements should preferably be located by using the element class and it's resource-id.
       * You can use UIAutomatorViewer (in `$ANDROID_HOME/tools`) or Perfecto to find these details.
   * For NGA3 iOS:
       * The screens should be in `src/screens/native/ios`
       * The elements are located by using the label or value of the elements.
       * You can use Perfecto Object Spy or temporarily add `driver.getPageSource()` to the relevant
        page object to find these details.
   * If there are differences in the locators between brands, create another version of the screen
     in `/src/screens/native/<app>/<platform>/<brand>`, e.g.
     `src/screens/native/nga3/android/hfx/login.screen.js`.
8. *Run your tests locally before pushing your changes to source control*.  See instructions in
   `Running the tests`.

# Code Style

Coding style is checked via `eslint` for js and `gherkin-lint` for feature files.  We use a number
of custom rules for `gherkin-lint` to enforce some very specific rules which we need, for example
the rule that switches must be at feature level, not scenario level.

# Architecture

## Technologies

The following technologies form the core of the framework:

* WebdriverIO `http://webdriver.io/` - 'WebDriver bindings for Node.js'.  Provides a test runner and
 an interface to Appium and Cucumber.
* Appium `http://appium.io/` - 'Appium is an open source test automation framework for use with
 native, hybrid and mobile web apps.  It drives iOS, Android, and Windows apps using the WebDriver
 protocol.'  We use Appium to run against local devices via a local Appium instance.  When using
 Perfecto, we use a local Appium client to connect to Perfecto and utilize the Appium extensions to
 the WebDriver JSON Wire Protocol.
* Cucumber `https://cucumber.io/` - 'Cucumber executes your .feature files, and those files contain
 executable specifications written in a language called Gherkin.'

## Code structure


                                      +-------------------+
                                      |                   |
                                      | ./main            |
                                      |                   |
                                      +---------+---------+
                                                |
        +-------------------+         +---------v---------+        +-------------------+
        |                   |         |                   |        |                   |
        |  ./wdio_config    |         |  ./runner         |        |  ./features       |
        |                   |         |                   |        |                   |
        +---------+---------+         +---------+---------+        +---------^---------+
                  ^                             |                            |
                  |                   +---------v---------+        +---------+---------+
                  |                   |                   |        |                   |
                  +-------------------+  WebdriverIO      +-------->  Cucumber         |
                                      |                   |        |                   |
                                      +---------+---------+        +-------------------+
                                                |
                                                |
                                      +---------v---------+
                                      |                   |
                                      |  Appium Client    |
                                      |                   |
                                      +---------+---------+
                                                |
                                                |
                                      +---------v---------+
                                      |                   |
                                      |  Perfecto         |
                                      |                   |
                                      +-------------------+


Arrows are roughly the direction of function calls.  The entry point is `./main.js`, this starts the
`./runner` which is reponsible for converting the high-level command line arguments into the
 specific wdio configuration which describes which tests we want to run.  We then delegate to WDIO
 (WebdriverIO) fairly quickly.  WDIO retrieves yet more configuration from `./wdio_config` and is
 responsible for actually running the cucumber tests.  Reporting is also implemented via callbacks
 from both WDIO and cucumber, this is described further in the reporting section.

## Process hierachy

WDIO uses a multi-process architecture, which can be the source of subtle bugs if you do not share
state between these processes in the correct way.  The diagram below shows a typical test run where
the runner has launched a single `WDIO Launcher` which then launches two `WDIO Runner` processes.

For example, this could happen when running against two perfecto devices simultaneously, in which
case a `WDIO Runner` would be created for each device.

        +---------------------------------------------------+
        |                                                   |
        | +-------------------+                             |
        | |                   |                             |
        | |   ./runner        |                             |
        | |                   |                             |
        | +---------+---------+                             |
        |           |                                       |
        |           |                                       |
        |           |                                       |
        | +---------v---------+        +------------------+ |
        | |                   |        |                  | |
        | |  WDIO Launcher    +-------->  ./wdio_config   | |
        | |                   |        |                  | |
        | +-----+-------+-----+        +------------------+ |
        |       |       |                                   |
        +---------------------------------------------------+
                |       |
                |       +----------------------------------------------------+
                |                                                            |
        +---------------------------------------------------+     +------------------------------------------------+
        |       |                                           |     |          |                                     |
        | +-----v-------------+        +------------------+ |     | +--------v---------+       +-----------------+ |
        | |                   |        |                  | |     | |                  |       |                 | |
        | |  WDIO Runner      +-------->  ./wdio_config   | |     | |  WDIO Runner     +------->  ./wdio_config  | |
        | |                   |        |                  | |     | |                  |       |                 | |
        | +-------------------+        +------------------+ |     | +------------------+       +-----------------+ |
        |                                                   |     |                                                |
        +---------------------------------------------------+     +------------------------------------------------+


Note that `./wdio_config` is hooked into multiple times from multiple processes!  So if you set some
global state in there then it will only exist for the process you are in at the time.

The `WDIO Launcher` process runs as a normal asynchronous NodeJS app, but the `WDIO Runner`
processes are a different story.  These are written as _synchronous_ scripts, which are then
converted to be asynchronous via the `fibers` library.  One must avoid using promises etc in these
scripts, similarly in the step definitions.

## Runner

The main job of the runner is to convert high-level command line arguments into the specific wdio
configuration which describes which tests we want to run.  It is also responsible for:

* Initialising reporting before calling WDIO.
* Aggregating reports once WDIO has completed.
* When running against real environments, the runner is responsible for changing switches via the
  IBC Proxy before running batches of tests which require the same switch configuration.

## Reporting

We expose a bunch of seperate reports, all of which are saved to various subfolders of `./reports`:

* WDIO JSON reports and the console `spec` report.
* Cucumber Reports.
* Aggregated `multiple-cucumber-html-reporter` reports, which combine cucumber multiple cucumber
  reports together.  These are the most useful reports for most users and are saved to
  `reports/cucumber/html`.  This report can be enabled with the `--htmlReport` flag.  This is also
  the report which is exposed from the CI jobs.
* Test Runner Reporter - This is a small custom reporter which logs any failures to start the
  `WDIO Runner`, in practise this usually signifies that a device is unavailable or in a bad state.
  This reporter is necessary because none of the reports above expose this specific information.
* Allure Reports - Improved HTML reporting that gives us a lot of the features that we have in the
  Perfecto reports. This reporting is enabled by default and can be toggled by the `--allureReports`
  flag.

The output of many of these reports is read by the report aggregtor (see section below).

## Logging

We use the `winston` logging library, and each log message is prefaced by the process ID.  WDIO
doesn't provide very helpful logging, instead logging everything to the `console`, so we also
redirect `console` calls through `winston`.

The WDIO logging level can be increased via the `config.logLevel` setting in
`src/wdio_config/config.js`.

## Unit Tests

           npm run test

There are a small quantity of unit tests for the runner and related areas, but the coverage is not
very good.

## Lesser Known Entrypoints

As well as the `npm run perfecto` and `npm run appium` entrypoints which call through to `./main.js`,
there are a number of lesser known entrypoints which were designed to be used by Jenkins alone.

### Report Aggregrator

           npm run report-aggregator

Aggregates the results of several test runs into a pretty html report.  This is designed to be
run from a jenkins 'overseer' job which schedules a number of downstream jobs, then aggregates
their results and sends out a pretty email.  For example we might use separate jobs for Android and
iOS over the weekend, but then aggregate the results into a single 'weekend' report.

The runner expects a `reportPrimer.json` which contains links to some of the reports from the
test run, for example the wdio report.  Currently, this primer is created by Jenkins after it
has run the necessary downstream jobs.

### Refresh Devices

           npm run refresh-devices

Runs `reboot` on all devices in the `perfectoDeviceList.js`.  Designed to be run
every morning from Jenkins, as devices are known to get into into a bad state if they are not
rebooted every so often.

### Dump Reservations

           npm run dump-reservations

Queries perfecto for all device reservations and dumps them.  Designed to be run from Jenkins so
that anyone can see who has reserved which devices.

### Force Device Reservations

          npm run forceDeviceReservations

Creates reservations for any devices in the `perfectoDeviceList`, deleting existing reservations if
they interfere.  Designed to be run every morning, in order to create reservations for the next
evenings run.

# Technical Debt

The framework is built on a number of technologies which all contribute to it's instability.

## Perfecto

The main sources of instability are:

1. We are running with a network in between us and the device.
2. Although Perfecto provides what looks like an Appium endpoint, it is in fact a very heavily
 modified out-of-date verion of Appium.  This means commands do not behave exactly the same when run
 against a local Appium instance and against Perfecto.  This leads to many bugs, and a lot of
 `if (isPerfecto()))` statements around key pieces of functionality.  To compound this problem, the
 solutions which are suggested by Perfecto usually involve using their own custom Appium extensions,
 which contributes to vendor lock-in.
3. Real devices necessarily means some instability, but on top of this perfecto devices often go
 into a permanent bad state, which is then almost impossible to detect before running the test and
 requires manual intervention through support tickets to fix.  There is also no good way to
 completely reset the device state after a test, meaning that some who has used the phone for manual
 testing can ruin a subsequent test run by setting an obscure config setting which you have never
 heard of, and hence do not check, eg. force RTL alignment (switching alignment from right-to-left
 on the page, as opposed to left-to-right.)

Other services, for example AWS device cloud, avoid issue 1 by running the code themselves - you
upload a binary to their servers which contains the tests to run, then they report the result to
you.  Issue 3 is resolved by these sorts of services by providing better guarantees of device state
and issue 2 by actually supporting Appium natively.

## WebdriverIO

1. Insufficient control over how tests are run.  This is the reason why we needed to create our own
 `runner` - we could not exert enough control over WDIO using it's own config.
2. Use of multiple processes and `fibers` to enable the writing of synchronous code in some of these
 processes makes debugging and even reasoning about how the framework works difficult (see Process
 Hierachy section).  Using multiple processes in this way is not a recommended technique for nodeJS.
 Running multiple sessions in the same process is on the wishlist of a future version of WDIO, but
 is not yet available.
3. Designed for web browser testing with WebDriver, not for Appium.  There is little documentation
 on how to integrate with Appium, though it has been improving recently.

## Misc

1. The feedback loop for seeing test failures is very long (>24 hours).  We once had smoke tests
 which reduced the feedback loop considerably, and hence pushed up the test pass rate, but these
 were switched off due to issues with Perfecto (see earlier section).
2. Lack of types seems to cause a large number of bugs in the scripts.  There is some consensus that
 Java would be less painful to work with.
3. Low unit test coverage.  There have been quite a few regressions in, for example, the `runner`,
 and the repository objects in `./data` which could've been prevented by better unit test coverage.
