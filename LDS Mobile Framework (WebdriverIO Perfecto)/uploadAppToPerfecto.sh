#!/bin/bash

set -e

usage()
{
cat <<EOF
Usage: $(basename $0) [--user yourusername --pwd yourpassword --file yourpackage] [options] 
 
Upload an app to your Perfecto account private repository.

  --user         Perfecto account username
  --pwd          Perfecto account password
  --file         Path to package to upload         
 
Options:
 
  --host         Optional perfecto url (default https://lloyds.perfectomobile.com)
  --proxy        Enable connect via proxy (default false)
  --proxy-host   Proxy host (default proxyarray.service.group)
  --proxy-port   Proxy port (default 8080)
  --proxy-auth   Specify if the proxy is authenticated (default false)
  --proxy-user   Proxy username (only needed for authenticated proxy)
  --proxy-pwd    Proxy password (only needed for authenticated proxy)
  --repo         Remote repository (defaut PRIVATE:applications)
  --app-name     Set name of the uploaded package (default filename of --file)
  --sha1         Enable sha1 validation (default false)
 
 Example 

EOF
}

# defaults
HOST="https://lloyds.perfectomobile.com"
USE_PROXY="false"
REMOTE_REPO="PRIVATE:applications"
PROXY_HOST="proxyarray.service.group"
PROXY_PORT="8080"
PROXY_AUTH="false"
USE_SHA1="false"
# Perfecto doesn't accept the sha1 file extension, so we need to use another one.
SHA1_FILE_EXTENSION="txt"

while [ "$1" ]; do
  case "$1" in
        --host)
            shift
            HOST="$1"
            ;;
        --proxy)
            USE_PROXY="true"
            ;;
        --proxy-host)
            shift
            PROXY_HOST="$1"
            ;;
        --proxy-port)
            shift
            PROXY_PORT="$1"
            ;;
        --proxy-auth)
            PROXY_AUTH="true"
            ;;
        --proxy-user)
            shift
            proxyUser="$1"
            ;;
        --proxy-pwd)
            shift
            proxyPwd="$1"
            ;;
         --user)
            shift
            perfectoUser="$1"
            ;;
         --pwd)
            shift
            perfectoPwd="$1"
            ;;
         --file)
            shift
            pkg="$1"
            ;;
         --repo)
            shift
            REMOTE_REPO="$1"
            ;;
         --app-name)
            shift
            pkgName="$1"
            ;;
        --sha1)
            USE_SHA1="true"
            ;;
        --help)
            usage
            exit 0
            ;;
        *)
            echo "$(basename $0): invalid option $1" >&2
            echo "see --help for usage"
            exit 1
                  ;;
  esac
  shift
done

if [[ -z "${perfectoUser}" ||  -z "${perfectoPwd}" ||  -z "${pkg}" ]]; then
    echo "Missing required arguments"
    usage
    exit 1
fi

if [ ! -f "${pkg}" ]; then
    echo "ERROR: Package ${pkg} not found for upload, aborting"
    exit 1
fi

if [ -z "${pkgName}" ]; then
   pkgName=$(basename $pkg)
   echo "Using app name: ${pkgName}"
fi

if [ "${USE_PROXY}" == "true" ]; then
    echo "Setting proxy"
    if [ "${PROXY_AUTH}" == "true" ]; then
        export https_proxy="http://${proxyUser}:${proxyPwd}@${PROXY_HOST}:${PROXY_PORT}"
        export http_proxy="http://${proxyUser}:${proxyPwd}@${PROXY_HOST}:${PROXY_PORT}"
    else
        export https_proxy="http://${PROXY_HOST}:${PROXY_PORT}"
        export http_proxy="http://${PROXY_HOST}:${PROXY_PORT}"
    fi
fi

if [ "${USE_SHA1}" == "true" ]; then
    # Generate local sha1
    SHA1="$(shasum ${pkg} | cut -d " " -f 1 | tr -d '\n')"
    # Download sha1 file from Perfecto
    PERFECTO_SHA1="$(curl -s --http1.1 -X GET "${HOST}/services/repositories/media/${REMOTE_REPO}/${pkgName}.${SHA1_FILE_EXTENSION}?operation=download&user=${perfectoUser}&password=${perfectoPwd}")"
    if [ "${SHA1}" == "${PERFECTO_SHA1}" ]; then
        echo ""
        echo "${pkgName} is already up to date"
        exit 0
    else
        # Delete the sha1 file in Perfecto if it doesn't match.
        echo "Latest version of ${pkgName} is not in Perfecto"
        curl -s -o /dev/null --http1.1 -X GET "${HOST}/services/repositories/media/${REMOTE_REPO}/${pkgName}.${SHA1_FILE_EXTENSION}?operation=delete&user=${perfectoUser}&password=${perfectoPwd}"
    fi
fi

echo ""
echo "Uploading app"
curl --http1.1 -X POST --data-binary @"${pkg}" "${HOST}/services/repositories/media/${REMOTE_REPO}/${pkgName}?operation=upload&overwrite=true&user=${perfectoUser}&password=${perfectoPwd}"

if [ "${USE_SHA1}" == "true" ]; then
    # Upload sha1 file
    echo ""
    echo "Uploading sha1 file"
    curl -s -o /dev/null --http1.1 -X POST -d "${SHA1}" "${HOST}/services/repositories/media/${REMOTE_REPO}/${pkgName}.${SHA1_FILE_EXTENSION}?operation=upload&overwrite=true&user=${perfectoUser}&password=${perfectoPwd}"
fi

echo ""
echo "Upload complete"
curl -s --http1.1 -X GET "${HOST}/services/repositories/media/${REMOTE_REPO}?operation=list&user=${perfectoUser}&password=${perfectoPwd}&responseFormat=json"
