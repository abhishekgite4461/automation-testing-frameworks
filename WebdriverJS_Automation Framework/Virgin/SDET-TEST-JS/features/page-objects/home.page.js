var webdriver = require('selenium-webdriver');
var until = webdriver.until;
var by = webdriver.By;
var Key = webdriver.Key;
var chai = require('chai')
var expect = chai.expect;


module.exports = class HomePage {

    /**
     * ELEMENTS ******************************************************************
     */

    get url() {
        return 'https://www.virginholidays.co.uk/'
    }

    get destinationBoxCloseIcon() {
        return '.demo-icon.sp-icon-close'
    }

    get destinationBox() {
        return '#holiday-typeahead-typeahead'
    }

    get destinationBoxHotel() {
        return '#hotel-typeahead-typeahead'
    }

    get destinationAutoFillResultsSection() {
        return '#holiday-typeahead-typeahead-results'
    }

    get hotelAutoFillResultsSection() {
        return '#hotel-typeahead-typeahead-results'
    }

    get findHolidaysButton() {
        return '//*[@id="search-panel"]/div/div/div[1]/form/div[2]/div[1]/div[5]/button/i'
    }

    get findHotelsButton() {
        return '//*[@id="search-panel"]/div/div/div[@booking-tab="hotel"]/form/div[2]/div/div[4]/button'
    }

    get gridButtonSearchPage() {
        return '.button.view'
    }

    get addToHotListIcon() {
        return '.hbc-add-content'
    }

    get hotListEnabledIcon() {
        return '.hotlist-link.loading-cover-sea.full-mode'
    }

    get navMenuHotelSection() {
        return '#search-panel-nav > li > button[booking-tab-target="hotel"]'
    }

    get hotelDestinationBoxCloseIcon() {
        return '//input[@id="hotel-typeahead-typeahead"]'
    }

    get hotelResultsPageSummaryPanel() {
        return '.summary-panel.hotel-panel.current.active'
    }

    get hotelOptionsSection() {
        return 'div[option-name="hotel options"]'
    }

    get boardBasisSection() {
        return 'li[label="board basis"]'
    }

    
    /**
     * ELEMENT ACTIONS ***********************************************************
     */


    async getVirginHomePage(driver) {
        await driver.get(this.url);
        await expect(await driver.getCurrentUrl()).to.be.eq(this.url)
    }

    async pressEnterKey(driver) {
        const textboxResults = await driver.findElement(by.css(this.destinationAutoFillResultsSection));
        await driver.wait(until.elementIsVisible(textboxResults), 10000);
        await driver.actions().sendKeys(Key.ENTER).perform();
    }

    async clickFindHolidaysButton(driver) {
        const findHolidays = await driver.findElement({xpath: this.findHolidaysButton});
        await findHolidays.click();
        await driver.wait(until.elementLocated(by.css(this.gridButtonSearchPage)), 30 * 1000);
    }

    async typeInDestinationBox(driver, location) {
        // const closeIcon = await driver.findElement({css: this.destinationBoxCloseIcon});
        // await closeIcon.click();
        const textBox = await driver.findElement({css: this.destinationBox});
        await textBox.click();
        await textBox.sendKeys(location);
        await this.pressEnterKey(driver);
    }

    async holidaySearch(driver, location) {
        await this.typeInDestinationBox(driver, location);
        await this.clickFindHolidaysButton(driver);
   }

    async clickOnNavMenuHotelsSection(driver) {
        await driver.wait(until.elementLocated(by.css(this.navMenuHotelSection)), 30 * 1000).click();
    }

    async clickFindHotelsButton(driver) {
        const findHotels = await driver.findElement({xpath: this.findHotelsButton});
        await findHotels.click();
        await driver.wait(until.elementLocated(by.css(this.hotelResultsPageSummaryPanel)), 30 * 1000);
    }

    async pressEnterKeyInHotelsSection(driver) {
        const textboxResults = await driver.findElement(by.css(this.hotelAutoFillResultsSection));
        await driver.wait(until.elementIsVisible(textboxResults), 10000);
        await driver.actions().sendKeys(Key.ENTER).perform();
    }

    async typeInHotelsDestinationBox(driver, location) {
        await driver.wait(until.elementLocated(by.xpath(this.hotelDestinationBoxCloseIcon)), 10 * 1000).click();
        await driver.actions().sendKeys(Key.CONTROL + "a").perform();
        await driver.actions().sendKeys(Key.DELETE).perform();
        const textBox = await driver.wait(until.elementLocated(by.css(this.destinationBoxHotel)), 10 * 1000);
        await textBox.click();
        await textBox.sendKeys(location);
        await this.pressEnterKeyInHotelsSection(driver);
    }   

   async hotelSearch(driver, location) {
        await this.clickOnNavMenuHotelsSection(driver);
        await this.typeInHotelsDestinationBox(driver, location);
        await this.clickFindHotelsButton(driver);
    }

   async addToHotList(driver) {
        const addToHotlist = await driver.wait(until.elementLocated(by.css(this.addToHotListIcon)), 10 * 1000);
        await addToHotlist.click();
        await driver.wait(until.elementLocated(by.css(this.hotListEnabledIcon)), 10 * 1000);
   }

   async hotListFullModeIsDisplayed(driver) {
        await driver.findElement(by.css(this.hotListEnabledIcon)).isDisplayed();
   }

   async hotelOptionPageIsDisplayed(driver) {
        await driver.findElement(by.css(this.hotelOptionsSection)).isDisplayed(); 
   }

   async boardBasisSectionIsDisplayed(driver) {
        await driver.findElement(by.css(this.boardBasisSection)).isDisplayed();
   }
   
}
