require('chromedriver');
const {After, Status, setWorldConstructor, setDefaultTimeout} = require('cucumber');
const seleniumWebdriver = require('selenium-webdriver');
const fs = require('fs');
const { Options } = require('selenium-webdriver/chrome');

// const breakpoint = process.env.BREAKPOINT || 'mobile';
console.log(`[SETUP] - BREAKPOINT: <${process.env.BREAKPOINT}>`);


After(function (testCase) {
    if (testCase.result.status === Status.FAILED) {
        return this.driver.takeScreenshot().then(function(image, err) {
                    fs.writeFile('screenshot.png', image, 'base64', function(error) {
                    if(error!=null)
                    console.log("Error occured while saving screenshot" + error);
                    });
         });
    }
    this.driver.quit();
});

function World() {
    if (process.env.BREAKPOINT == "desktop") {
        this.driver = new seleniumWebdriver.Builder()
                .forBrowser('chrome')
                .build();
    } else {
        this.driver = new seleniumWebdriver.Builder()
        .forBrowser('chrome')
        .setChromeOptions(new Options().addArguments(['--window-size=200,900', 'disable-infobars']))
        .build();
    }
}

setDefaultTimeout(60 * 1000);
  
setWorldConstructor(World);