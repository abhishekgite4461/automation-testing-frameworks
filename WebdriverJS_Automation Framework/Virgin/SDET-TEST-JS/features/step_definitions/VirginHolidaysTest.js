const { Given, When, Then, setDefaultTimeout} = require('cucumber');
require('chromedriver');
const HomePage = require('../page-objects/home.page');
const homepage = new HomePage();
const seleniumWebdrivers = require('selenium-webdriver');
require('selenium-webdriver/chrome');

    Given('I am on virgin holidays home page', async function () {
        await homepage.getVirginHomePage(this.driver);
        await this.driver.executeScript(function() {
            return [window.innerWidth, window.innerHeight];
        }).then(console.log);
    });

    Given('I do a {string} search', async function (string) {
        if (string == "hotel") {
            await homepage.hotelSearch(this.driver, "Mayfair");
        } else if (string == "holiday") {
            await homepage.holidaySearch(this.driver, "Manchester");
        }
    });

    When('I add a holiday to a hotlist', async function () {
        await homepage.addToHotList(this.driver); 
    });
    
    Then('I can see that a holiday added to the hotlist on top of the page', async function () {
        await homepage.hotListFullModeIsDisplayed(this.driver);
    });
      
    When('I proceed to hotel options page', async function () {
        await homepage.hotelOptionPageIsDisplayed(this.driver);
    });
    
    Then('I can see my board basis', async function () {
        await homepage.boardBasisSectionIsDisplayed(this.driver);
    });