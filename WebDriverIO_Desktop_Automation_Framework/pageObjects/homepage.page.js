class homepage  {

    /* page elements */

    get brandLogo()   { return browser.element('.BrandLogo-img'); }
    get searchIcon()  { return browser.element('.Header-searchIcon')}
    get searchTextField() {return browser.element('#searchTermInput') }

    /* page methods */

    // This method open the base URL
    goToPage () {
        browser.url('/')
    }

    // This method will return a bollean value by checking if the Brand logo is visible or not
    hasLogo () {
        return this.brandLogo.isVisible();
    }

    // This method will search for a product. Currently we have kept it static "Earrings" but it can be changed to search any other products.
    searchProduct() {
        this.searchIcon.waitForVisible(5000);
        this.searchIcon.click();
        this.searchTextField.setValue("Earrings");
        browser.keys('Enter');
    }
}

export default new homepage();