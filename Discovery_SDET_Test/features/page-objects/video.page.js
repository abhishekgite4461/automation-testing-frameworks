import {expect} from "chai";
module.exports = class VideoPage {

    /**
     * ELEMENTS ******************************************************************
     */

    get gdprIAcceptButton() {
        return '.qc-cmp-button'
    }

    get tennisMoreVideosButton() {
        return '.video-section__button-more-content[href="/tennis/"]'
    }

    get footballMoreVideosButton() {
        return '.video-section__button-more-content[href="/football/"]'
    }

    get cyclingMoreVideosButton() {
        return '.video-section__button-more-content[href="/cycling/"]'
    }

    get snookerMoreVideosButton() {
        return '.video-section__button-more-content[href="/snooker/"]'
    }

    get tennisFirstVideo() {
        return '.secondary-video > .video_thumbnail[href*="/tennis/"]'
    }

    get footballFirstVideo() {
        return '.secondary-video > .video_thumbnail[href*="/football/"]'
    }

    get cyclingFirstVideo() {
        // return '.video_thumbnail[href*="/cycling/"]'
        return '.secondary-video > .video_thumbnail[href*="/cycling/"]'
    }

    get snookerFirstVideo() {
        return '.secondary-video > .video_thumbnail[href*="/snooker/"]'
    }

    get categoryListItems() {
        return '.categorylist__item a'
    }


    /**
     * ELEMENT ACTIONS ***********************************************************
     */

    isVisibleGdprIAcceptButton() {
        return browser.isVisible(this.gdprIAcceptButton);
    }

    clickgdprIAcceptButton() {
        browser.click(this.gdprIAcceptButton)
    }

    clickMoreVideosButton(sport) {
        switch (sport) {
            case "Tennis":
                browser.scrollIntoView(this.tennisMoreVideosButton, false);
                browser.scrollIntoView(this.tennisFirstVideo, false);
                browser.click(this.tennisMoreVideosButton);
                break;
            case "Football":
                browser.scrollIntoView(this.footballMoreVideosButton, false);
                browser.scrollIntoView(this.footballFirstVideo, false);
                browser.click(this.footballMoreVideosButton);
                break;
            case "Cycling":
                browser.scrollIntoView(this.cyclingMoreVideosButton, false);
                browser.scrollIntoView(this.cyclingFirstVideo, false);
                browser.click(this.cyclingMoreVideosButton);
                break;
            case "Snooker":
                browser.scrollIntoView(this.snookerMoreVideosButton, false);
                browser.scrollIntoView(this.snookerFirstVideo, false);
                browser.click(this.snookerMoreVideosButton);
                break;
            default:
                console.log('Sorry, unknown section ' + sport + '.');
        }
    }

    clickFirstVideo() {
        browser.click('.thumb-title');
    }

    verifyVideoisPlaying() {
        browser.waitUntil(() => (browser.waitForVisible('.video__skip.video__skip--skippable')), 90000);
        browser.click('.video__skip.video__skip--skippable');
        return browser.isVisible('.vjs-play-control.vjs-control.vjs-button.vjs-playing')[0];
    }

    videoPlayerButton(button) {
        if (button == "Pause") {
            return browser.isVisible('.vjs-play-control.vjs-control.vjs-button.vjs-playing')[0];
        } else if (button == "Play") {
            if (browser.isVisible('.vjs-play-control.vjs-control.vjs-button.vjs-playing')[0] == true) {
                browser.click('.vjs-play-control.vjs-control.vjs-button.vjs-playing')[0];
                return browser.isVisible('.vjs-play-control.vjs-control.vjs-button.vjs-paused');
            } else {
                return browser.isVisible('.vjs-play-control.vjs-control.vjs-button.vjs-paused');
            }
        } else if (button == "Maximize") {
            return browser.isVisible('.vjs-fullscreen-control.vjs-control.vjs-button')[0];
        }
    }

    allNavMenuItems() {
        return $$(this.categoryListItems)
    }
}
