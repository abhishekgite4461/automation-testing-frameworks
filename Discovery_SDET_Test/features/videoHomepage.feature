Feature: User able to watch the Videos from Eurosport

  As Eurosport Customer
  I want to watch my favourite sports videos streams
  So that I can enjoy and follow my favourite sports

  # Scenario Outline: able to watch sports streaming
  #   Given I am Eurosport Customer
  #   And On Videos Hub Page
    # When I select to watch the videos from <Sport> Section
    # Then the selected video is playing
    # And the following player controls are displayed
    #   |Pause|
    #   |Play|
    #   |Maximize|
    # Examples:
    #   | Sport |
    #   | Tennis |
    #   | Football |
    #   | Cycling |
    #   | Snooker |

  Scenario: able to see all the menu items are displayed on Videos homepage
    Given I am Eurosport Customer
    And On Videos Hub Page
    Then I can see the menu items
