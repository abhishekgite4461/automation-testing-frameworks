import {Given, When, Then} from 'cucumber'
import {expect} from 'chai'
import VideoPage from '../page-objects/video.page'

Given(/^I am Eurosport Customer$/, () => {
    browser.url('/')
    const homepage = new VideoPage();
    if (homepage.isVisibleGdprIAcceptButton() == true) {
        homepage.clickgdprIAcceptButton();
    }
})

Given(/^On Videos Hub Page$/, () => {
    expect(browser.getUrl()).to.equal(browser.options.baseUrl)
})

When(/^I select to watch the videos from (.*) Section$/, (sport) => {
    const homepage = new VideoPage();
    homepage.clickMoreVideosButton(sport);
    homepage.clickFirstVideo();
})

Then(/^I can see the menu items$/, () => {
    const labelArr = [ [ 'Video Home' ],
        [ 'News Videos' ],
        [ 'Eurosport Events' ],
        [ 'Eurosport Original' ] ]

    const homepage = new VideoPage()
    let  allMenuItems = homepage.allNavMenuItems()
    expect(allMenuItems.map(function(el) { return [el.getText()]})).to.deep.equal(labelArr)
})

