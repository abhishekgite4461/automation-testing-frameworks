import {Given, When, Then} from 'cucumber'
import {expect} from 'chai'
import VideoPage from '../page-objects/video.page'

Then(/^the selected video is playing$/, () => {
    const homepage = new VideoPage();
    expect(homepage.verifyVideoisPlaying()).to.equal(true);
})

Then(/^the following player controls are displayed$/, (table) => {
    const homepage = new VideoPage();
    const buttons = table.raw();
    for (const button of buttons) {
        expect(homepage.videoPlayerButton(button)).to.equal(true);
    }
})
