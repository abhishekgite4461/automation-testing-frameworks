package pageObject;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import pageObject.Home_Page;
import pageObject.SignUp_Page;
import pageObject.AccountCreated_Page;
import pageObject.Home_LoggedIn_Page;
import pageObject.NewSnippet_Page;

public class Functions {
	
	public static boolean isLoggedIn=false;
	 
	 //SignUp method
	 public static void SignUp(String userID, String password, WebDriver driver) {
	 
		 
			 	Home_Page.lnk_SignUp(driver).click();
				
				SignUp_Page.txtUserName(driver).sendKeys(userID);
				
				SignUp_Page.txtPassword(driver).sendKeys(password);
				
				SignUp_Page.btnCreateAccount(driver).click();
				
				String accountCreated_Header = AccountCreated_Page.msg_AccountCreated(driver).getText();
				
				if (accountCreated_Header.equals("Account created."))
				{
					Assert.assertTrue(accountCreated_Header.equals("Account created."));
			         
					System.out.println("Account created successfully.");
					
					isLoggedIn = true;
					
				}
				else if (accountCreated_Header.equals("User already exists."))
				{
					System.out.println("Account already exists.");
				}
				
				
				
				Home_Page.lnk_Home(driver).click();
		 
	 }
	 
	 //NewSnippet method
	 public static void NewSnippet(String snippet, WebDriver driver) {
		 
		 if(isLoggedIn) {
		 	Home_LoggedIn_Page.lnk_NewSnippet(driver).click();
			
			NewSnippet_Page.txt_AddSnippet(driver).sendKeys(snippet);
			NewSnippet_Page.btn_Submit(driver).click();
			
			System.out.println("New Snippet added successfully.");
		 }
		 
		 else
		 {
			 System.out.println("User should be logged in to create a new snippet.");
		 }
	 }
	 
	 //SignIn method
	 public static void SignIn(String userID, String password, WebDriver driver) {
		 
		 if(!isLoggedIn) {
		 	
				Home_Page.lnk_SignIn(driver).click();
				
				SignUp_Page.txtUserName(driver).sendKeys(userID);
				
				SignUp_Page.txtPassword(driver).sendKeys(password);
				
				SignUp_Page.btnCreateAccount(driver).click();
				
				String accountCreated_Header = Home_Page.header_HomePage(driver).getText();
				
				Assert.assertTrue(accountCreated_Header.equals("Gruyere: Home"));
	      
				isLoggedIn = true;
				System.out.println("Logged in successfully.");
						
		  }
	 
	 }
	 
	 //SignOut method
	 public static void SignOut(WebDriver driver) {
		 
		 if(isLoggedIn) {
		 		Home_LoggedIn_Page.lnk_SignOut(driver).click();
				
				isLoggedIn = false;
				System.out.println("Logged out successfully.");
	 			}
				else
				 {
					 System.out.println("User not logged in.");
				 }
							 
	 }	

}
