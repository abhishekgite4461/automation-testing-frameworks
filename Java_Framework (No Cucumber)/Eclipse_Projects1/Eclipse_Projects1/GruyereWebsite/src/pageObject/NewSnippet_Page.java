package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class NewSnippet_Page {

	private static WebElement element = null;
	 
	 public static WebElement txt_AddSnippet(WebDriver driver){
	 
	    element = driver.findElement(By.name("snippet"));
	 
	    return element;
	 
	    }
	 
	 public static WebElement btn_Submit(WebDriver driver){
		 
		    element = driver.findElement(By.xpath("//tbody/tr/td[2]/input"));
		 
		    return element;
		 
		    }
}
