package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Home_LoggedIn_Page {

	private static WebElement element = null;
	 
	 public static WebElement lnk_NewSnippet(WebDriver driver){
	 
	    element = driver.findElement(By.linkText("New\u00A0Snippet"));
	 
	    return element;
	 
	    }
	 
	 public static WebElement lnk_Upload(WebDriver driver){
		 
		    element = driver.findElement(By.linkText("Upload"));
		 
		    return element;
		 
		    }
	 
	 public static WebElement lnk_SignOut(WebDriver driver){
		 
		 element = driver.findElement(By.linkText("Sign out"));
	 
		 return element;
	 
	    }
}
