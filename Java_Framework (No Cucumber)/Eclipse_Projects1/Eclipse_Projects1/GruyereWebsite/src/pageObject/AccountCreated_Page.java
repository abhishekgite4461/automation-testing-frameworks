package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AccountCreated_Page {
	
	 private static WebElement element = null;
	 
	 public static WebElement msg_AccountCreated(WebDriver driver){
	 
	    element = driver.findElement(By.className("message"));
	 
	    return element;
	 
	    }
	 

}
