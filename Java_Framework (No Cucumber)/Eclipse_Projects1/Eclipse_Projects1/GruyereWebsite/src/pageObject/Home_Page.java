package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Home_Page {
	
	private static WebElement element = null;
	 
	 public static WebElement lnk_SignUp(WebDriver driver){
	 
	    element = driver.findElement(By.linkText("Sign up"));
	 
	    return element;
	 
	    }
	 
	 public static WebElement lnk_SignIn(WebDriver driver){
	 
		 element = driver.findElement(By.linkText("Sign in"));
	 
		 return element;
	 
	    }
	 
	 public static WebElement lnk_Home(WebDriver driver){
		 
		 element = driver.findElement(By.linkText("Home"));
	 
		 return element;
	 
	    }
	 
	 public static WebElement header_HomePage(WebDriver driver){
		 
		 element = driver.findElement(By.className("has-refresh"));
	 
		 return element;
	 
	    }
	 
}
