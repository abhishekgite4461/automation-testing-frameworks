package pageObject;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class SignUp_Page {
	
	private static WebElement element = null;
	 
    public static WebElement txtUserName(WebDriver driver){
 
         element = driver.findElement(By.name("uid"));
 
         return element;
 
         }
 
     public static WebElement txtPassword(WebDriver driver){
 
         element = driver.findElement(By.name("pw"));
 
         return element;
 
         }
 
     public static WebElement btnCreateAccount(WebDriver driver){
 
         element = driver.findElement(By.xpath("//tbody/tr[3]/td[2]/input"));
 
         return element;
 
         }

}
