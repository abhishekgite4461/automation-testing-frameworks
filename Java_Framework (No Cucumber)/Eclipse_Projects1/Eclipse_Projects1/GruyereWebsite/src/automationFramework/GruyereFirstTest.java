package automationFramework;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import pageObject.Functions;

public class GruyereFirstTest {

	public static void main(String[] args) {
		// Create a new instance of the Firefox driver
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", "C:\\geckodriver-v0.11.1-win64\\geckodriver.exe");
		driver =new FirefoxDriver();
		
		//Implicit wait of 10 seconds is applied
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		//Launch the Gruyere Website in the browser
		driver.get("https://google-gruyere.appspot.com/543012646686/");
 
        //Print a Log In message to the screen
        System.out.println("Successfully opened the website.");
        
        //Call the SignUp method from 'Functions' and sign up with the username - '�bhishek' and password 'test'
        Functions.SignUp("h", "h", driver);
        
        //Call the NewSnippet method from 'Functions' and add a new snippet to the account created above 
        Functions.NewSnippet("I love Cheese", driver);
        
        //Call the SignOut method from 'Functions' in order to Sign-Out from the currently logged in account
        Functions.SignOut(driver);
        
        //Call the SignIn method from 'Functions' with the username - '�bhishek' and password 'test'
        Functions.SignIn("m", "m", driver);
 
		//Close the driver
        driver.quit();

	}

}
