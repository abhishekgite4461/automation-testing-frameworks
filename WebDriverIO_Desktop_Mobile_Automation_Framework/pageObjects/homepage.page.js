class homepage  {

    /* Mobile site page elements */
    get brandLogo()   { return browser.element('.BrandLogo-img'); }
    get searchIcon()  { return browser.element('.Header-searchIcon')}
    get searchTextField() {return browser.element('#searchTermInput') }

    /* Desktop site page elements */
    get desktopBrandLogo()   { return browser.element('#store_logo'); }
    get dektopSearchTextField() {return browser.element('#inp_search_text') }


    /* page methods */

    // This method open the base URL
    goToPage () {
        browser.url('/')
    }

    // This method will return a bollean value by checking if the Brand logo is visible or not. The if statement will check if it is desktop site or mobile site.
    hasLogo () {
        if(this.dektopSearchTextField.isVisible()){
            return this.desktopBrandLogo.isVisible();
        }
        else{
            return this.brandLogo.isVisible();
        }
    }

    // This method will search for a product. Currently we have kept it static "Earrings" but it can be changed to search any other products. The if statement will check if it is desktop site or mobile site.
    searchProduct(product_name) {

        if(this.dektopSearchTextField.isVisible()){
            this.dektopSearchTextField.waitForVisible(5000);
            this.dektopSearchTextField.setValue(product_name);
            browser.keys('Enter');

        }
        else{
            this.searchIcon.waitForVisible(5000);
            this.searchIcon.click();
            this.searchTextField.setValue(product_name);
            browser.keys('Enter');

        }
    }
}

export default new homepage();